package com.gdslinkasia.onboarding;

import android.Manifest;
import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.os.Build;
import android.os.Bundle;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.annotation.RequiresApi;
import androidx.appcompat.app.AlertDialog;
import androidx.appcompat.app.AppCompatActivity;
import androidx.core.app.ActivityCompat;

import com.gdslinkasia.base.utils.ActivityIntent;

@RequiresApi(api = Build.VERSION_CODES.M)
public class SplashActivity extends AppCompatActivity {

    Thread splashThread;
    public static final int REQUEST_CODE = 1;
    public static String[] MY_PERMISSIONS = {
            android.Manifest.permission.READ_EXTERNAL_STORAGE, //RUNTIME PERMISSION : LEVEL SECURITY
            android.Manifest.permission.WRITE_EXTERNAL_STORAGE, //RUNTIME PERMISSION : LEVEL SECURITY
            android.Manifest.permission.CAMERA,                 //RUNTIME PERMISSION : LEVEL SECURITY
            android.Manifest.permission.CALL_PHONE,             //RUNTIME PERMISSION : LEVEL SECURITY
            /* android.Manifest.permission.SEND_SMS,               //RUNTIME PERMISSION : LEVEL SECURITY
             android.Manifest.permission.ACCESS_COARSE_LOCATION, //RUNTIME PERMISSION : LEVEL SECURITY
             android.Manifest.permission.ACCESS_FINE_LOCATION,   //RUNTIME PERMISSION : LEVEL SECURITY*/
            android.Manifest.permission.ACCESS_NETWORK_STATE,
            android.Manifest.permission.ACCESS_WIFI_STATE,
            android.Manifest.permission.INTERNET,
            Manifest.permission.READ_PHONE_STATE,
            Manifest.permission.REQUEST_INSTALL_PACKAGES,
            Manifest.permission.MODIFY_PHONE_STATE
    };

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_splashscreen);

        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {

            if (!hasPermissions(this, MY_PERMISSIONS)) {
                ActivityCompat.requestPermissions(this, MY_PERMISSIONS, REQUEST_CODE);
                //Without the permission to Write, to apply for the permission to Read and Write, the system will pop up the permission dialog
            } else {
                startSplashThread();
            }
        } else {
            startSplashThread();
        }
    }

    void startSplashThread() {
        splashThread = new Thread() {
            @Override
            public void run() {
                super.run();
                try {
                    int waited = 0;

                    while (waited < 1000) {
                        sleep(100);
                        waited += 100;
                    }

                    try {
                        Intent intent = new Intent(SplashActivity.this, Class.forName(ActivityIntent.LOGIN_ACTIVITY));
                        startActivity(intent);
                    } catch (ClassNotFoundException e) {
                        e.printStackTrace();
                    }


                } catch (InterruptedException ignored) {

                }
//                finally {
//                    SplashActivity.this.finish();
//                }
            }
        };
        splashThread.start();
    }


    public boolean hasPermissions(Context context, String... permissions) {
        if (context != null && permissions != null) {
            for (String permission : permissions) {
                if (ActivityCompat.checkSelfPermission(context, permission) != PackageManager.PERMISSION_GRANTED) {
                    return false;
                }
            }
        }
        return true;
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults);
        if (requestCode == REQUEST_CODE) {

            //check if all permissions are granted
            boolean allgranted = false;
            for (int grantResult : grantResults) {
                if (grantResult == PackageManager.PERMISSION_GRANTED) {
                    allgranted = true;
                } else {
                    allgranted = false;
                    break;
                }
            }

            if (allgranted) {

                startSplashThread();
            } else if (
                    ActivityCompat.shouldShowRequestPermissionRationale(this, MY_PERMISSIONS[0])
                            || ActivityCompat.shouldShowRequestPermissionRationale(this, MY_PERMISSIONS[1])
                            || ActivityCompat.shouldShowRequestPermissionRationale(this, MY_PERMISSIONS[2])
                            || ActivityCompat.shouldShowRequestPermissionRationale(this, MY_PERMISSIONS[3])
                            || ActivityCompat.shouldShowRequestPermissionRationale(this, MY_PERMISSIONS[4])
                            || ActivityCompat.shouldShowRequestPermissionRationale(this, MY_PERMISSIONS[5])
                            || ActivityCompat.shouldShowRequestPermissionRationale(this, MY_PERMISSIONS[6])
                            || ActivityCompat.shouldShowRequestPermissionRationale(this, MY_PERMISSIONS[7])
                            || ActivityCompat.shouldShowRequestPermissionRationale(this, MY_PERMISSIONS[8])
                            || ActivityCompat.shouldShowRequestPermissionRationale(this, MY_PERMISSIONS[9])) {
                Toast.makeText(getBaseContext(), "Please Enable all permissions first.", Toast.LENGTH_LONG).show();
                new AlertDialog.Builder(this)
                        .setCancelable(false)
                        .setTitle("Permissions")
                        .setMessage("Please Enable all permissions first.")
                        .setNegativeButton("Okay", (dialogInterface, i) -> finishAffinity())
                        .create()
                        .show();


            } else {
                startSplashThread();

            }
        }
    }


    @Override
    public void onBackPressed() {
        splashThread.interrupt();
        super.onBackPressed();
        finish();
    }
}
