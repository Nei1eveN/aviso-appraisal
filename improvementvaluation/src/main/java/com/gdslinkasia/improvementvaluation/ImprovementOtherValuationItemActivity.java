package com.gdslinkasia.improvementvaluation;

import android.app.ProgressDialog;
import android.content.Intent;
import android.os.Bundle;
import android.view.MenuItem;
import android.widget.AutoCompleteTextView;

import androidx.appcompat.app.AlertDialog;
import androidx.appcompat.app.AppCompatActivity;

import com.gdslinkasia.base.model.details.ValrepLandimpOtherLandImpValue;
import com.gdslinkasia.commonresources.R2;
import com.gdslinkasia.improvementvaluation.contract.land_imp_other_valuation_item.LandImpOtherValuationItemContract;
import com.gdslinkasia.improvementvaluation.contract.land_imp_other_valuation_item.LandImpOtherValuationItemPresenterImpl;

import java.util.Objects;

import butterknife.BindView;
import butterknife.ButterKnife;

import static com.gdslinkasia.base.database.DatabaseUtils.*;
import static com.gdslinkasia.base.utils.computation.ImprovementOtherValuationComputation.AC_UNIT_COST;

public class ImprovementOtherValuationItemActivity extends AppCompatActivity implements LandImpOtherValuationItemContract.View {

    public final String TAG = this.getClass().getSimpleName();

    private long uniqueId;

    @BindView(R2.id.acTypeOtherImp) AutoCompleteTextView acTypeOtherImp;
    @BindView(R2.id.acDescription) AutoCompleteTextView acDescription;
    @BindView(R2.id.acEcoLife) AutoCompleteTextView acEcoLife;
    @BindView(R2.id.acEffectiveAge) AutoCompleteTextView acEffectiveAge;
    @BindView(R2.id.acRemLife) AutoCompleteTextView acRemLife;
    @BindView(R2.id.acArea) AutoCompleteTextView acArea;
    @BindView(R2.id.acUnit) AutoCompleteTextView acUnit;
    @BindView(R2.id.acUnitCost) AutoCompleteTextView acUnitCost;
    @BindView(R2.id.acRCN) AutoCompleteTextView acRCN;
    @BindView(R2.id.acPercentage) AutoCompleteTextView acPercentage;
    @BindView(R2.id.acTotalDeduct) AutoCompleteTextView acTotalDepreciation;
    @BindView(R2.id.acDepreciatedRCN) AutoCompleteTextView acDepreciatedRCN;

    private ProgressDialog progressDialog;

    private LandImpOtherValuationItemContract.Presenter presenter;
    private String appraisalType;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.content_imp_val_other_cost_approach);
        ButterKnife.bind(this);

        Intent intent = getIntent();
        uniqueId = intent.getLongExtra(UNIQUE_ID, 0);
        appraisalType = intent.getStringExtra(APPRAISAL_TYPE);

        Objects.requireNonNull(getSupportActionBar()).setTitle("Property Details");
        getSupportActionBar().setSubtitle("Description of Other Improvements (Cost Approach)"+uniqueId);
        getSupportActionBar().setIcon(R.drawable.ic_close_white_24dp);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);

        progressDialog = new ProgressDialog(this);
        presenter = new LandImpOtherValuationItemPresenterImpl(this, getApplication());
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        if (item.getItemId() == android.R.id.home) {
            finish();
        }
        return super.onOptionsItemSelected(item);
    }

    @Override
    protected void onResume() {
        super.onResume();
        presenter.onStart(uniqueId);
    }



    @Override
    protected void onPause() {
        super.onPause();
        presenter.updateItemDetails(
                uniqueId,
                Objects.requireNonNull(acTypeOtherImp.getText()).toString(),
                Objects.requireNonNull(acDescription.getText()).toString(),
                Objects.requireNonNull(acEcoLife.getText()).toString(),
                Objects.requireNonNull(acEffectiveAge.getText()).toString(),
                Objects.requireNonNull(acRemLife.getText()).toString(),
                Objects.requireNonNull(acArea.getText()).toString(),
                Objects.requireNonNull(acUnit.getText()).toString(),
                Objects.requireNonNull(acUnitCost.getText()).toString(),
                Objects.requireNonNull(acRCN.getText()).toString(),
                Objects.requireNonNull(acPercentage.getText()).toString(),
                Objects.requireNonNull(acTotalDepreciation.getText()).toString(),
                Objects.requireNonNull(acDepreciatedRCN.getText()).toString()
        );


    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        presenter.onDestroy();
        progressDialog.dismiss();
    }

    @Override
    public void showProgress(String title, String message) {
        progressDialog.setCancelable(false);
        progressDialog.setIndeterminate(false);
        progressDialog.setTitle(title);
        progressDialog.setMessage(message);
        progressDialog.show();
    }

    @Override
    public void hideProgress() {
        progressDialog.hide();
    }

    @Override
    public void showDetails(ValrepLandimpOtherLandImpValue detail) {

        acTypeOtherImp.setText(detail.getValrepLandimpOtherLandImpValueType());
        acDescription.setText(detail.getValrepLandimpOtherLandImpValueDesc());
        acEcoLife.setText(detail.getValrepLandimpOtherLandImpValueEconomicLife());
        acEffectiveAge.setText(detail.getValrepLandimpOtherLandImpValueEffectiveAge());
        acRemLife.setText(detail.getValrepLandimpOtherLandImpValueRemainLife());
        acArea.setText(detail.getValrepLandimpOtherLandImpValueArea());
        acUnit.setText(detail.getValrepLandimpOtherLandImpValueAreaUnit());
        acUnit.setEnabled(false);

        acUnitCost.setText(detail.getValrepLandimpOtherLandImpValueUnitValue());
        acRCN.setText(detail.getValrepLandimpOtherLandImpValueRcn());
        acPercentage.setText(detail.getValrepLandimpOtherLandImpValuePercentage());
        acTotalDepreciation.setText(detail.getValrepLandimpOtherLandImpValueDepreciation());
        acDepreciatedRCN.setText(detail.getValrepLandimpOtherLandImpValueDepreciatedValue());

        acUnitCost.addTextChangedListener(AC_UNIT_COST(acArea, acUnitCost, acEcoLife, acEffectiveAge, acRCN, acPercentage, acTotalDepreciation, acDepreciatedRCN));
    }

    @Override
    public void showExitDialog(String title, String message) {
        new AlertDialog.Builder(this)
                .setCancelable(false)
                .setTitle(title)
                .setMessage(message)
                .setPositiveButton("CLOSE", (dialog, which) -> finish())
                .create()
                .show();
    }
}
