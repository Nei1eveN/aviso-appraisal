package com.gdslinkasia.improvementvaluation;

import android.app.Dialog;
import android.app.ProgressDialog;
import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.appcompat.app.AlertDialog;
import androidx.appcompat.widget.Toolbar;
import androidx.coordinatorlayout.widget.CoordinatorLayout;
import androidx.fragment.app.DialogFragment;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.gdslinkasia.base.adapter.LandImpOtherValuationAdapter;
import com.gdslinkasia.base.adapter.click_listener.ViewListClickListener;
import com.gdslinkasia.base.model.details.ValrepLandimpOtherLandImpValue;
import com.gdslinkasia.commonresources.R2;
import com.gdslinkasia.improvementvaluation.contract.land_imp_other_valuation_list.LandImpOtherValuationContract;
import com.gdslinkasia.improvementvaluation.contract.land_imp_other_valuation_list.LandImpOtherValuationPresenterImpl;

import java.util.List;
import java.util.Objects;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.Unbinder;

import static com.gdslinkasia.base.database.DatabaseUtils.*;

public class ImprovementOtherValuationFragment extends DialogFragment
        implements LandImpOtherValuationContract.View, ViewListClickListener {

    public static final String TAG = "ImprovementDescFragment";

    private String recordId;
    private String appraisalType;

    private Unbinder unbinder;

    @BindView(R2.id.coordinatorLayout)
    CoordinatorLayout coordinatorLayout;
    @BindView(R2.id.toolbar)
    Toolbar toolbar;
    @BindView(R2.id.frameContainer)
    FrameLayout frameLayout;

    //    @BindView(R.id.coordinatorLayout)
//    CoordinatorLayout coordinatorLayout;
//    @BindView(R.id.swipeRefresh)
//    private SwipeRefreshLayout swipeRefreshLayout;
    //    @BindView(R.id.recyclerView)
    private RecyclerView recyclerView;
    //    @BindView(R.id.tvList)
    private TextView emptyText;
    // @BindView(R.id.ivList)
    private ImageView emptyImage;
    // @BindView(R.id.tvNoRecord)
    private TextView tvNoRecord;

    private ProgressDialog progressDialog;

    private LandImpOtherValuationContract.Presenter presenter;

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setStyle(DialogFragment.STYLE_NORMAL, R.style.FullScreenDialog);

        recordId = Objects.requireNonNull(getArguments()).getString(RECORD_ID);
        appraisalType = Objects.requireNonNull(getArguments()).getString(APPRAISAL_TYPE);

        progressDialog = new ProgressDialog(getActivity());
        presenter = new LandImpOtherValuationPresenterImpl(this, Objects.requireNonNull(getActivity()).getApplication());
    }

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        return inflater.inflate(R.layout.layout_full_screen_dialog, container, false);
    }


    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        unbinder = ButterKnife.bind(this, view);

        toolbar.setNavigationIcon(R.drawable.ic_close_white_24dp);
        toolbar.setNavigationOnClickListener(view1 -> Objects.requireNonNull(getDialog()).dismiss());
        toolbar.setTitle("List");
        toolbar.setSubtitle("Description of Improvement");

        //TODO Form Widgets
        View contentView = LayoutInflater.from(getContext()).inflate(R.layout.content_list, frameLayout, false);

//        swipeRefreshLayout = contentView.findViewById(R.id.swipeRefresh);
        recyclerView = contentView.findViewById(R.id.recyclerView);
        tvNoRecord = contentView.findViewById(R.id.tvNoRecord);
        emptyText = contentView.findViewById(R.id.tvList);
        emptyImage = contentView.findViewById(R.id.ivList);

        frameLayout.addView(contentView);
        presenter.onStart(recordId);
    }

    @Override
    public void onStart() {
        super.onStart();
        Log.d("int--DialogOnStart", "Logic Triggered");
        Dialog dialog = getDialog();
        if (dialog != null) {
            int width = ViewGroup.LayoutParams.MATCH_PARENT;
            int height = ViewGroup.LayoutParams.MATCH_PARENT;
            Objects.requireNonNull(dialog.getWindow()).setLayout(width, height);
        }
    }

    //    @Override
//    public void onStart() {
//        super.onStart();
//
//    }

    @Override
    public void onPause() {
        super.onPause();
    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();
        unbinder.unbind();
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        presenter.onDestroy();
        progressDialog.dismiss();
    }

    @Override
    public void showProgress(String title, String message) {
//        swipeRefreshLayout.setRefreshing(true);
        progressDialog.setCancelable(false);
        progressDialog.setIndeterminate(false);
        progressDialog.setTitle(title);
        progressDialog.setMessage(message);
        progressDialog.show();
    }

    @Override
    public void hideProgress() {
//        swipeRefreshLayout.setRefreshing(false);
        progressDialog.hide();
    }

    @Override
    public void showList(List<ValrepLandimpOtherLandImpValue> valrepLandimpImpDetail) {
        emptyImage.setVisibility(View.GONE);
        emptyText.setVisibility(View.GONE);
        tvNoRecord.setVisibility(View.GONE);

        recyclerView.setVisibility(View.VISIBLE);
        recyclerView.setLayoutManager(new LinearLayoutManager(getActivity()));
        recyclerView.setHasFixedSize(true);

        LandImpOtherValuationAdapter adapter = new LandImpOtherValuationAdapter(getActivity(), this);
        adapter.submitList(valrepLandimpImpDetail);

        recyclerView.setAdapter(adapter);
    }

    @Override
    public void showEmptyState(String message) {
        recyclerView.setVisibility(View.GONE);

        tvNoRecord.setVisibility(View.VISIBLE);
        emptyText.setVisibility(View.GONE);
        emptyImage.setVisibility(View.VISIBLE);

        tvNoRecord.setText(message);
    }

    @Override
    public void showErrorDialog(String title, String message) {
        new AlertDialog.Builder(Objects.requireNonNull(getActivity()))
                .setCancelable(false)
                .setTitle(title)
                .setMessage(message)
                .setPositiveButton("CLOSE", null)
                .create()
                .show();
    }

    @Override
    public void onItemDetailClick(String recordId, long uniqueId) {
        Log.d("int--uniqueId", String.valueOf(uniqueId));
//        presenter.requestOwnershipDetails(uniqueId);
//        Bundle bundle = new Bundle();
//        bundle.putLong(UNIQUE_ID, uniqueId);

//        CondoUnitItemDialogFragment dialogFragment = new CondoUnitItemDialogFragment();
//        dialogFragment.setArguments(bundle);
//
//        FragmentTransaction fragmentTransaction = getChildFragmentManager().beginTransaction();
//        dialogFragment.show(fragmentTransaction, CondoUnitItemDialogFragment.TAG);

        Intent intent = new Intent(getActivity(), ImprovementOtherValuationItemActivity.class);
        intent.putExtra(UNIQUE_ID, uniqueId);

        Objects.requireNonNull(getActivity()).startActivity(intent);
    }
}
