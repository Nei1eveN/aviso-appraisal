package com.gdslinkasia.improvementvaluation;

import android.os.Bundle;
import android.os.Handler;
import android.widget.AutoCompleteTextView;

import androidx.appcompat.app.AppCompatActivity;
import androidx.fragment.app.FragmentTransaction;

import com.gdslinkasia.base.model.job.Record;
import com.gdslinkasia.commonresources.R2;
import com.gdslinkasia.improvementvaluation.contract.total_valuation.ImprovementTotalValuationContract;
import com.gdslinkasia.improvementvaluation.contract.total_valuation.ImprovementTotalValuationPresenterImpl;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

import static com.gdslinkasia.base.database.DatabaseUtils.*;

public class ImprovementValuationActivity extends AppCompatActivity implements ImprovementTotalValuationContract.View {

    @BindView(R2.id.acDeedRestrict) AutoCompleteTextView acDeedRestrict;
    @BindView(R2.id.acImpValuationRemark) AutoCompleteTextView acImpValuationRemark;
    @BindView(R2.id.acTotalValOtherImp) AutoCompleteTextView acTotalValOtherImp;



    public static String TAG = "ImprovementValuationActivity";

    private String recordId;
    private String appraisalType;
    private ImprovementTotalValuationContract.Presenter presenter;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.form_improvement_valuation);

        ButterKnife.bind(this);

        Bundle putExtraBundle = getIntent().getExtras();
        if(putExtraBundle == null) {
            recordId= null;
            return;
        } else {
            recordId= putExtraBundle.getString(RECORD_ID);
            appraisalType= putExtraBundle.getString(APPRAISAL_TYPE);
        }

        presenter = new ImprovementTotalValuationPresenterImpl(this, getApplication());

        presenter.onStart(recordId);

    }

    @OnClick(R2.id.llImprovementCostApproach)
    void llImprovementCostApproachOnClick() {
        Bundle bundle = new Bundle();
        bundle.putString(RECORD_ID, recordId);
        bundle.putString(APPRAISAL_TYPE, appraisalType);

        ImprovementValuationFragment dialogFragment = new ImprovementValuationFragment();
        dialogFragment.setArguments(bundle);
        FragmentTransaction fragmentTransaction = getSupportFragmentManager().beginTransaction();
        dialogFragment.show(fragmentTransaction, ImprovementValuationFragment.TAG);

    }

    @OnClick(R2.id.llImprovementOtherCostApproach)
    void onClickllImprovementCostApproach() {
        Bundle bundle = new Bundle();
        bundle.putString(RECORD_ID, recordId);
        bundle.putString(APPRAISAL_TYPE, appraisalType);

        ImprovementOtherValuationFragment dialogFragment = new ImprovementOtherValuationFragment();
        dialogFragment.setArguments(bundle);
        FragmentTransaction fragmentTransaction = getSupportFragmentManager().beginTransaction();
        dialogFragment.show(fragmentTransaction, ImprovementOtherValuationFragment.TAG);

    }






    @Override
    public void showProgress(String title, String message) {

    }

    @Override
    public void hideProgress() {

    }

    @Override
    public void showSnackMessage(String message) {



    }

    @Override
    protected void onPause() {
        super.onPause();

        new Handler().post(() -> presenter.updatePropertyDesc(
                                recordId,
                                acDeedRestrict.getText().toString(),
                                acImpValuationRemark.getText().toString(),
                                acTotalValOtherImp.getText().toString()
        ));


    }

    @Override
    public void showDetails(Record record) {

        acDeedRestrict.setText(record.getValrepLandimpIdStatutoryConstraints());
        acImpValuationRemark.setText(record.getValrepLandimpIdStatutoryConstraintsDesc());
        acTotalValOtherImp.setText(record.getValrepLandimpPhysicalLotType());

    }

    @Override
    public void showExitDialog(String title, String message) {

    }

    @Override
    public void showErrorDialog(String title, String message) {

    }
}
