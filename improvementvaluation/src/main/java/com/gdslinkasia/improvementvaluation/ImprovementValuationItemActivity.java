package com.gdslinkasia.improvementvaluation;

import android.app.ProgressDialog;
import android.content.Intent;
import android.os.Bundle;
import android.view.MenuItem;
import android.widget.AutoCompleteTextView;

import androidx.appcompat.app.AlertDialog;
import androidx.appcompat.app.AppCompatActivity;

import com.gdslinkasia.base.model.details.ValrepLandimpImpValuation;
import com.gdslinkasia.commonresources.R2;
import com.gdslinkasia.improvementvaluation.contract.land_imp_valuation_item.LandImpValuationItemContract;
import com.gdslinkasia.improvementvaluation.contract.land_imp_valuation_item.LandImpValuationItemPresenterImpl;

import java.util.Objects;

import butterknife.BindView;
import butterknife.ButterKnife;

import static com.gdslinkasia.base.database.DatabaseUtils.*;
import static com.gdslinkasia.base.utils.computation.ImprovementCostAppComputation.*;

public class ImprovementValuationItemActivity extends AppCompatActivity implements LandImpValuationItemContract.View {

    public final String TAG = this.getClass().getSimpleName();

    private long uniqueId;


    @BindView(R2.id.acDescription) AutoCompleteTextView acDescription;
    @BindView(R2.id.acFloorArea) AutoCompleteTextView acFloorArea;
    @BindView(R2.id.acRcnSqm) AutoCompleteTextView acRcnSqm;
    @BindView(R2.id.acRcn) AutoCompleteTextView acRcn;
    @BindView(R2.id.acPercent) AutoCompleteTextView acPercent;
    @BindView(R2.id.acPhysical) AutoCompleteTextView acPhysical;
    @BindView(R2.id.acFunctional) AutoCompleteTextView acFunctional;
    @BindView(R2.id.acEconomic) AutoCompleteTextView acEconomic;
    @BindView(R2.id.acDepreciate) AutoCompleteTextView acDepreciate;

    private ProgressDialog progressDialog;

    private LandImpValuationItemContract.Presenter presenter;
    private String appraisalType;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.content_imp_val_cost_approach);
        ButterKnife.bind(this);

        Intent intent = getIntent();
        uniqueId = intent.getLongExtra(UNIQUE_ID, 0);
        appraisalType = intent.getStringExtra(APPRAISAL_TYPE);

        Objects.requireNonNull(getSupportActionBar()).setTitle("Improvement Valuation");
        getSupportActionBar().setSubtitle("Improvement (Cost Approach)");
        getSupportActionBar().setIcon(R.drawable.ic_close_white_24dp);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);

        progressDialog = new ProgressDialog(this);
        presenter = new LandImpValuationItemPresenterImpl(this, getApplication());
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        if (item.getItemId() == android.R.id.home) {
            finish();
        }
        return super.onOptionsItemSelected(item);
    }

    @Override
    protected void onResume() {
        super.onResume();
        presenter.onStart(uniqueId);
    }



    @Override
    protected void onPause() {
        super.onPause();
        presenter.updateItemDetails(
                uniqueId,
                Objects.requireNonNull(acDescription.getText()).toString(),
                Objects.requireNonNull(acFloorArea.getText()).toString(),
                Objects.requireNonNull(acRcnSqm.getText()).toString(),
                Objects.requireNonNull(acRcn.getText()).toString(),
                Objects.requireNonNull(acPercent.getText()).toString(),
                Objects.requireNonNull(acPhysical.getText()).toString(),
                Objects.requireNonNull(acFunctional.getText()).toString(),
                Objects.requireNonNull(acEconomic.getText()).toString(),
                Objects.requireNonNull(acDepreciate.getText()).toString()
        );

    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        presenter.onDestroy();
        progressDialog.dismiss();
    }

    @Override
    public void showProgress(String title, String message) {
        progressDialog.setCancelable(false);
        progressDialog.setIndeterminate(false);
        progressDialog.setTitle(title);
        progressDialog.setMessage(message);
        progressDialog.show();
    }

    @Override
    public void hideProgress() {
        progressDialog.hide();
    }

    @Override
    public void showDetails(ValrepLandimpImpValuation detail) {

        acDescription.setText(detail.getValrepLandimpImpValueDescription());
        acDescription.setEnabled(false);
        acDescription.setFocusable(false);
        acFloorArea.setText(detail.getValrepLandimpImpValueTotalArea());
        acFloorArea.setEnabled(false);
        acFloorArea.setFocusable(false);

        acRcnSqm.setText(detail.getValrepLandimpImpValueTotalReproductionCost());
        acRcn.setText(detail.getValrepLandimpImpValueRcnNew());
        acPercent.setText(detail.getValrepLandimpImpValuePhysicalIncurablePer());
        acPhysical.setText(detail.getValrepLandimpImpValuePhysicalIncurableVal());
        acFunctional.setText(detail.getValrepLandimpImpValueFunctionalVal());
        acEconomic.setText(detail.getValrepLandimpImpValueEconomicVal());
        acDepreciate.setText(detail.getValrepLandimpImpValueDepreciatedValue());

        acRcnSqm.addTextChangedListener(RCN_SQM(detail.getValrepLandimpImpValueEffectiveLife(), detail.getValrepLandimpImpValueEconomicLife(), acFloorArea, acRcnSqm, acRcn, acPercent, acPhysical, acFunctional, acEconomic, acDepreciate));
        acFunctional.addTextChangedListener(FUNCTIONAL_OBSOLENCE(detail.getValrepLandimpImpValueEffectiveLife(), detail.getValrepLandimpImpValueEconomicLife(), acFloorArea, acRcnSqm, acRcn, acPercent, acPhysical, acFunctional, acEconomic, acDepreciate));
        acEconomic.addTextChangedListener(ECONOMIC_OBSOLENCE(detail.getValrepLandimpImpValueEffectiveLife(), detail.getValrepLandimpImpValueEconomicLife(), acFloorArea, acRcnSqm, acRcn, acPercent, acPhysical, acFunctional, acEconomic, acDepreciate));
    }

    @Override
    public void showExitDialog(String title, String message) {
        new AlertDialog.Builder(this)
                .setCancelable(false)
                .setTitle(title)
                .setMessage(message)
                .setPositiveButton("CLOSE", (dialog, which) -> finish())
                .create()
                .show();
    }
}
