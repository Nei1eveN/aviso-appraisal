package com.gdslinkasia.improvementvaluation.contract.land_imp_valuation_item;

import com.gdslinkasia.base.model.details.ValrepLandimpImpValuation;

import io.reactivex.Single;

public interface LandImpValuationItemContract {
    interface View {
        void showProgress(String title, String message);
        void hideProgress();
        void showDetails(ValrepLandimpImpValuation detail);
        void showExitDialog(String title, String message);
    }
    interface Presenter {
        void onStart(long uniqueId);
        void onDestroy();

        void updateItemDetails(long uniqueId,
                               String valrep_landimp_imp_value_description,
                               String valrep_landimp_imp_value_total_area,
                               String valrep_landimp_imp_value_total_reproduction_cost,
                               String valrep_landimp_imp_value_rcn_new,
                               String valrep_landimp_imp_value_physical_incurable_per,
                               String valrep_landimp_imp_value_physical_incurable_val,
                               String valrep_landimp_imp_value_functional_val,
                               String valrep_landimp_imp_value_economic_val,
                               String valrep_landimp_imp_value_depreciated_value
        );



    }
    interface Interactor {
        Single<ValrepLandimpImpValuation> getLandImpImpDetailsList(long uniqueId);

        void updateLotDetail(ValrepLandimpImpValuation detail);
    }
}
