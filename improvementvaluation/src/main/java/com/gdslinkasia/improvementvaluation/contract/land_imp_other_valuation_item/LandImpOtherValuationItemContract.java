package com.gdslinkasia.improvementvaluation.contract.land_imp_other_valuation_item;

import com.gdslinkasia.base.model.details.ValrepLandimpOtherLandImpValue;

import io.reactivex.Single;

public interface LandImpOtherValuationItemContract {
    interface View {
        void showProgress(String title, String message);
        void hideProgress();
        void showDetails(ValrepLandimpOtherLandImpValue detail);
        void showExitDialog(String title, String message);
    }
    interface Presenter {
        void onStart(long uniqueId);
        void onDestroy();

        void updateItemDetails(long uniqueId,
                               String valrep_landimp_other_land_imp_value_type,
                               String valrep_landimp_other_land_imp_value_desc,
                               String valrep_landimp_other_land_imp_value_economic_life,
                               String valrep_landimp_other_land_imp_value_effective_age,
                               String valrep_landimp_other_land_imp_value_remain_life,
                               String valrep_landimp_other_land_imp_value_area,
                               String valrep_landimp_other_land_imp_value_area_unit,
                               String valrep_landimp_other_land_imp_value_unit_value,
                               String valrep_landimp_other_land_imp_value_rcn,
                               String valrep_landimp_other_land_imp_value_percentage,
                               String valrep_landimp_other_land_imp_value_depreciation,
                               String valrep_landimp_other_land_imp_value_depreciated_value
        );



    }
    interface Interactor {
        Single<ValrepLandimpOtherLandImpValue> getLandImpImpDetailsList(long uniqueId);

        void updateLotDetail(ValrepLandimpOtherLandImpValue detail);
    }
}
