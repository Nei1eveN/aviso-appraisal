package com.gdslinkasia.improvementvaluation.contract.land_imp_valuation_list;

import android.app.Application;
import android.util.Log;

import com.gdslinkasia.base.model.details.ValrepLandimpImpValuation;

import java.util.List;

import io.reactivex.Flowable;
import io.reactivex.Single;
import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.disposables.CompositeDisposable;
import io.reactivex.functions.Consumer;
import io.reactivex.observers.DisposableSingleObserver;
import io.reactivex.schedulers.Schedulers;

public class LandImpValuationPresenterImpl implements LandImpValuationContract.Presenter {

    private LandImpValuationContract.View view;
    private LandImpValuationContract.Interactor interactor;
    private CompositeDisposable compositeDisposable = new CompositeDisposable();

    public LandImpValuationPresenterImpl(LandImpValuationContract.View view, Application application) {
        this.view = view;
        this.interactor = new LandImpValuationInteractorImpl(application);
    }

    @Override
    public void onStart(String recordId) {
//        view.showProgress();
        compositeDisposable.add(getSingleListObservable(recordId).subscribeWith(getSingleListObserver(recordId)));
    }

    @Override
    public void onDestroy() {
        view = null;
        compositeDisposable.clear();
    }

    private Single<List<ValrepLandimpImpValuation>> getSingleListObservable(String recordId) {
        return interactor.getDetailsList(recordId)
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread());
    }

    private DisposableSingleObserver<List<ValrepLandimpImpValuation>> getSingleListObserver(String recordId) {
        return new DisposableSingleObserver<List<ValrepLandimpImpValuation>>() {
            @Override
            public void onSuccess(List<ValrepLandimpImpValuation> valrepLandimpLotDetails) {
                if (!valrepLandimpLotDetails.isEmpty()) {
//                    view.showList(valrepLandimpLotDetails);
                    compositeDisposable.add(getListFlowable(recordId).subscribe(getFlowableObserver()));
                } else {
                    view.showEmptyState("There are no records available.");
                }
//                new Handler().postDelayed(() -> view.hideProgress(), 1000);
            }

            @Override
            public void onError(Throwable e) {
                view.showEmptyState(e.getMessage());
//                new Handler().postDelayed(() -> view.hideProgress(), 1000);
            }
        };
    }

    private Flowable<List<ValrepLandimpImpValuation>> getListFlowable(String recordId) {
        return interactor.getListFlowable(recordId)
                .subscribeOn(Schedulers.computation())
                .observeOn(AndroidSchedulers.mainThread());
    }

    private Consumer<List<ValrepLandimpImpValuation>> getFlowableObserver() {
        return valrepLandimpLotDetails -> {
            view.showList(valrepLandimpLotDetails);
            Log.d("int--flowableSize", String.valueOf(valrepLandimpLotDetails.size()));
        };
    }

}
