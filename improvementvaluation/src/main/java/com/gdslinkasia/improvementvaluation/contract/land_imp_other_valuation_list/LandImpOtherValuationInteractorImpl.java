package com.gdslinkasia.improvementvaluation.contract.land_imp_other_valuation_list;

import android.app.Application;

import com.gdslinkasia.base.database.repository.LandImpOtherValuationLocalRepository;
import com.gdslinkasia.base.model.details.ValrepLandimpOtherLandImpValue;

import java.util.List;

import io.reactivex.Flowable;
import io.reactivex.Single;

public class LandImpOtherValuationInteractorImpl implements LandImpOtherValuationContract.Interactor {

    private LandImpOtherValuationLocalRepository localRepository;

    public LandImpOtherValuationInteractorImpl(Application application) {
        //same table for condo Unit Desc
        localRepository = new LandImpOtherValuationLocalRepository(application);
    }

    @Override
    public Single<List<ValrepLandimpOtherLandImpValue>> getDetailsList(String recordId) {
        return localRepository.getDetailsList(recordId);
    }

    @Override
    public Flowable<List<ValrepLandimpOtherLandImpValue>> getListFlowable(String recordId) {
        return localRepository.getListFlowable(recordId);
    }
}
