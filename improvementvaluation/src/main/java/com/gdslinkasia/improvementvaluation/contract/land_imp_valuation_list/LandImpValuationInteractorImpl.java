package com.gdslinkasia.improvementvaluation.contract.land_imp_valuation_list;

import android.app.Application;

import com.gdslinkasia.base.database.repository.LandImpValuationLocalRepository;
import com.gdslinkasia.base.model.details.ValrepLandimpImpValuation;

import java.util.List;

import io.reactivex.Flowable;
import io.reactivex.Single;

public class LandImpValuationInteractorImpl implements LandImpValuationContract.Interactor {

    private LandImpValuationLocalRepository localRepository;

    public LandImpValuationInteractorImpl(Application application) {
        //same table for condo Unit Desc
        localRepository = new LandImpValuationLocalRepository(application);
    }

    @Override
    public Single<List<ValrepLandimpImpValuation>> getDetailsList(String recordId) {
        return localRepository.getDetailsList(recordId);
    }

    @Override
    public Flowable<List<ValrepLandimpImpValuation>> getListFlowable(String recordId) {
        return localRepository.getListFlowable(recordId);
    }
}
