package com.gdslinkasia.improvementvaluation.contract.land_imp_other_valuation_item;

import android.app.Application;

import com.gdslinkasia.base.database.repository.LandImpOtherValuationLocalRepository;
import com.gdslinkasia.base.model.details.ValrepLandimpOtherLandImpValue;

import io.reactivex.Single;

public class LandImpOtherValuationItemInteractorImpl implements LandImpOtherValuationItemContract.Interactor {

    private LandImpOtherValuationLocalRepository localRepository;

    public LandImpOtherValuationItemInteractorImpl(Application application) {
        this.localRepository = new LandImpOtherValuationLocalRepository(application);
    }

    @Override
    public Single<ValrepLandimpOtherLandImpValue> getLandImpImpDetailsList(long uniqueId) {
        return localRepository.getDetails(uniqueId);
    }

    @Override
    public void updateLotDetail(ValrepLandimpOtherLandImpValue detail) {
        localRepository.updateLotDetail(detail);
    }
}
