package com.gdslinkasia.improvementvaluation.contract.land_imp_valuation_item;

import android.app.Application;

import com.gdslinkasia.base.database.repository.LandImpValuationLocalRepository;
import com.gdslinkasia.base.model.details.ValrepLandimpImpValuation;

import io.reactivex.Single;

public class LandImpValuationItemInteractorImpl implements LandImpValuationItemContract.Interactor {

    private LandImpValuationLocalRepository localRepository;

    public LandImpValuationItemInteractorImpl(Application application) {
        this.localRepository = new LandImpValuationLocalRepository(application);
    }

    @Override
    public Single<ValrepLandimpImpValuation> getLandImpImpDetailsList(long uniqueId) {
        return localRepository.getDetails(uniqueId);
    }

    @Override
    public void updateLotDetail(ValrepLandimpImpValuation detail) {
        localRepository.updateLotDetail(detail);
    }
}
