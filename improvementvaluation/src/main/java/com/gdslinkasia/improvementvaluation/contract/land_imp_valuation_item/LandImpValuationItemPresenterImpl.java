package com.gdslinkasia.improvementvaluation.contract.land_imp_valuation_item;

import android.app.Application;
import android.os.Handler;
import android.util.Log;

import com.gdslinkasia.base.model.details.ValrepLandimpImpValuation;

import io.reactivex.Single;
import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.disposables.CompositeDisposable;
import io.reactivex.observers.DisposableSingleObserver;
import io.reactivex.schedulers.Schedulers;

public class LandImpValuationItemPresenterImpl implements LandImpValuationItemContract.Presenter {
    private LandImpValuationItemContract.View view;
    private LandImpValuationItemContract.Interactor interactor;
    private CompositeDisposable compositeDisposable = new CompositeDisposable();

    public LandImpValuationItemPresenterImpl(LandImpValuationItemContract.View view, Application application) {
        this.view = view;
        this.interactor = new LandImpValuationItemInteractorImpl(application);
    }

    @Override
    public void onStart(long uniqueId) {
        view.showProgress("Loading Details", "Please wait...");
        compositeDisposable.add(getSingleObservable(uniqueId).subscribeWith(getSingleObserver()));
    }

    @Override
    public void onDestroy() {
        view = null;
        compositeDisposable.clear();
    }

    @Override
    public void updateItemDetails(long uniqueId,
                                String valrep_landimp_imp_value_description,
                                String valrep_landimp_imp_value_total_area,
                                String valrep_landimp_imp_value_total_reproduction_cost,
                                String valrep_landimp_imp_value_rcn_new,
                                String valrep_landimp_imp_value_physical_incurable_per,
                                String valrep_landimp_imp_value_physical_incurable_val,
                                String valrep_landimp_imp_value_functional_val,
                                String valrep_landimp_imp_value_economic_val,
                                String valrep_landimp_imp_value_depreciated_value
    ) {

        compositeDisposable.add(getSingleObservable(uniqueId).subscribeWith(getRecordUpdate(
                 valrep_landimp_imp_value_description,
                 valrep_landimp_imp_value_total_area,
                 valrep_landimp_imp_value_total_reproduction_cost,
                 valrep_landimp_imp_value_rcn_new,
                 valrep_landimp_imp_value_physical_incurable_per,
                 valrep_landimp_imp_value_physical_incurable_val,
                 valrep_landimp_imp_value_functional_val,
                 valrep_landimp_imp_value_economic_val,
                 valrep_landimp_imp_value_depreciated_value
        )));
    }


    private Single<ValrepLandimpImpValuation> getSingleObservable(long uniqueId) {
        return interactor.getLandImpImpDetailsList(uniqueId)
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread());
    }

    private DisposableSingleObserver<ValrepLandimpImpValuation> getSingleObserver() {
        return new DisposableSingleObserver<ValrepLandimpImpValuation>() {
            @Override
            public void onSuccess(ValrepLandimpImpValuation detail) {
                view.showDetails(detail);
                new Handler().postDelayed(() -> view.hideProgress(), 2000);
            }

            @Override
            public void onError(Throwable e) {
                view.showExitDialog("Record Error", e.getMessage());
                new Handler().postDelayed(() -> view.hideProgress(), 2000);
            }
        };
    }

    private DisposableSingleObserver<ValrepLandimpImpValuation> getRecordUpdate(
            String valrep_landimp_imp_value_description,
            String valrep_landimp_imp_value_total_area,
            String valrep_landimp_imp_value_total_reproduction_cost,
            String valrep_landimp_imp_value_rcn_new,
            String valrep_landimp_imp_value_physical_incurable_per,
            String valrep_landimp_imp_value_physical_incurable_val,
            String valrep_landimp_imp_value_functional_val,
            String valrep_landimp_imp_value_economic_val,
            String valrep_landimp_imp_value_depreciated_value) {
        return new DisposableSingleObserver<ValrepLandimpImpValuation>() {
            @Override
            public void onSuccess(ValrepLandimpImpValuation detail) {

                detail.setValrepLandimpImpValueDescription(valrep_landimp_imp_value_description);
                detail.setValrepLandimpImpValueTotalArea(valrep_landimp_imp_value_total_area);
                detail.setValrepLandimpImpValueTotalReproductionCost(valrep_landimp_imp_value_total_reproduction_cost);
                detail.setValrepLandimpImpValueRcnNew(valrep_landimp_imp_value_rcn_new);
                detail.setValrepLandimpImpValuePhysicalCurablePer(valrep_landimp_imp_value_physical_incurable_per);
                detail.setValrepLandimpImpValuePhysicalIncurableVal(valrep_landimp_imp_value_physical_incurable_val);
                detail.setValrepLandimpImpValueFunctionalVal(valrep_landimp_imp_value_functional_val);
                detail.setValrepLandimpImpValueEconomicVal(valrep_landimp_imp_value_economic_val);
                detail.setValrepLandimpImpValueDepreciatedValue(valrep_landimp_imp_value_depreciated_value);

                interactor.updateLotDetail(detail);

                Log.d("int--landImpSave", "Logic triggered");
            }

            @Override
            public void onError(Throwable e) {

            }
        };
    }
}
