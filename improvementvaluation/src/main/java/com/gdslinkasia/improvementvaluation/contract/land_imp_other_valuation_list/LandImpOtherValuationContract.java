package com.gdslinkasia.improvementvaluation.contract.land_imp_other_valuation_list;

import com.gdslinkasia.base.model.details.ValrepLandimpOtherLandImpValue;

import java.util.List;

import io.reactivex.Flowable;
import io.reactivex.Single;

public interface LandImpOtherValuationContract {

    interface View {
        void showProgress(String title, String message);

        void hideProgress();

        void showList(List<ValrepLandimpOtherLandImpValue> valrepLandimpImpValuation);

        void showEmptyState(String message);

        void showErrorDialog(String title, String message);
    }

    interface Presenter {
        void onStart(String recordId);

        void onDestroy();
    }

    interface Interactor {
        Single<List<ValrepLandimpOtherLandImpValue>> getDetailsList(String recordId);

        Flowable<List<ValrepLandimpOtherLandImpValue>> getListFlowable(String recordId);
    }

}
