package com.gdslinkasia.improvementvaluation.contract.land_imp_other_valuation_item;

import android.app.Application;
import android.os.Handler;
import android.util.Log;

import com.gdslinkasia.base.model.details.ValrepLandimpOtherLandImpValue;

import io.reactivex.Single;
import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.disposables.CompositeDisposable;
import io.reactivex.observers.DisposableSingleObserver;
import io.reactivex.schedulers.Schedulers;

public class LandImpOtherValuationItemPresenterImpl implements LandImpOtherValuationItemContract.Presenter {
    private LandImpOtherValuationItemContract.View view;
    private LandImpOtherValuationItemContract.Interactor interactor;
    private CompositeDisposable compositeDisposable = new CompositeDisposable();

    public LandImpOtherValuationItemPresenterImpl(LandImpOtherValuationItemContract.View view, Application application) {
        this.view = view;
        this.interactor = new LandImpOtherValuationItemInteractorImpl(application);
    }

    @Override
    public void onStart(long uniqueId) {
        view.showProgress("Loading Details", "Please wait...");
        compositeDisposable.add(getSingleObservable(uniqueId).subscribeWith(getSingleObserver()));
    }

    @Override
    public void onDestroy() {
        view = null;
        compositeDisposable.clear();
    }

    @Override
    public void updateItemDetails(long uniqueId,
                String valrep_landimp_other_land_imp_value_type,
                String valrep_landimp_other_land_imp_value_desc,
                String valrep_landimp_other_land_imp_value_economic_life,
                String valrep_landimp_other_land_imp_value_effective_age,
                String valrep_landimp_other_land_imp_value_remain_life,
                String valrep_landimp_other_land_imp_value_area,
                String valrep_landimp_other_land_imp_value_area_unit,
                String valrep_landimp_other_land_imp_value_unit_value,
                String valrep_landimp_other_land_imp_value_rcn,
                String valrep_landimp_other_land_imp_value_percentage,
                String valrep_landimp_other_land_imp_value_depreciation,
                String valrep_landimp_other_land_imp_value_depreciated_value
    ) {

        compositeDisposable.add(getSingleObservable(uniqueId).subscribeWith(getRecordUpdate(
                 valrep_landimp_other_land_imp_value_type,
                 valrep_landimp_other_land_imp_value_desc,
                 valrep_landimp_other_land_imp_value_economic_life,
                 valrep_landimp_other_land_imp_value_effective_age,
                 valrep_landimp_other_land_imp_value_remain_life,
                 valrep_landimp_other_land_imp_value_area,
                 valrep_landimp_other_land_imp_value_area_unit,
                 valrep_landimp_other_land_imp_value_unit_value,
                 valrep_landimp_other_land_imp_value_rcn,
                 valrep_landimp_other_land_imp_value_percentage,
                 valrep_landimp_other_land_imp_value_depreciation,
                 valrep_landimp_other_land_imp_value_depreciated_value
        )));
    }


    private Single<ValrepLandimpOtherLandImpValue> getSingleObservable(long uniqueId) {
        return interactor.getLandImpImpDetailsList(uniqueId)
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread());
    }

    private DisposableSingleObserver<ValrepLandimpOtherLandImpValue> getSingleObserver() {
        return new DisposableSingleObserver<ValrepLandimpOtherLandImpValue>() {
            @Override
            public void onSuccess(ValrepLandimpOtherLandImpValue detail) {
                view.showDetails(detail);
                new Handler().postDelayed(() -> view.hideProgress(), 2000);
            }

            @Override
            public void onError(Throwable e) {
                view.showExitDialog("Record Error", e.getMessage());
                new Handler().postDelayed(() -> view.hideProgress(), 2000);
            }
        };
    }

    private DisposableSingleObserver<ValrepLandimpOtherLandImpValue> getRecordUpdate(
            String valrep_landimp_other_land_imp_value_type,
            String valrep_landimp_other_land_imp_value_desc,
            String valrep_landimp_other_land_imp_value_economic_life,
            String valrep_landimp_other_land_imp_value_effective_age,
            String valrep_landimp_other_land_imp_value_remain_life,
            String valrep_landimp_other_land_imp_value_area,
            String valrep_landimp_other_land_imp_value_area_unit,
            String valrep_landimp_other_land_imp_value_unit_value,
            String valrep_landimp_other_land_imp_value_rcn,
            String valrep_landimp_other_land_imp_value_percentage,
            String valrep_landimp_other_land_imp_value_depreciation,
            String valrep_landimp_other_land_imp_value_depreciated_value) {
        return new DisposableSingleObserver<ValrepLandimpOtherLandImpValue>() {
            @Override
            public void onSuccess(ValrepLandimpOtherLandImpValue detail) {

                detail.setValrepLandimpOtherLandImpValueType(valrep_landimp_other_land_imp_value_type);
                detail.setValrepLandimpOtherLandImpValueDesc(valrep_landimp_other_land_imp_value_desc);
                detail.setValrepLandimpOtherLandImpValueEconomicLife(valrep_landimp_other_land_imp_value_economic_life);
                detail.setValrepLandimpOtherLandImpValueEffectiveAge(valrep_landimp_other_land_imp_value_effective_age);
                detail.setValrepLandimpOtherLandImpValueRemainLife(valrep_landimp_other_land_imp_value_remain_life);
                detail.setValrepLandimpOtherLandImpValueArea(valrep_landimp_other_land_imp_value_area);
                detail.setValrepLandimpOtherLandImpValueAreaUnit(valrep_landimp_other_land_imp_value_area_unit);
                detail.setValrepLandimpOtherLandImpValueUnitValue(valrep_landimp_other_land_imp_value_unit_value);
                detail.setValrepLandimpOtherLandImpValueRcn(valrep_landimp_other_land_imp_value_rcn);
                detail.setValrepLandimpOtherLandImpValuePercentage(valrep_landimp_other_land_imp_value_percentage);
                detail.setValrepLandimpOtherLandImpValueDepreciation(valrep_landimp_other_land_imp_value_depreciation);
                detail.setValrepLandimpOtherLandImpValueDepreciatedValue(valrep_landimp_other_land_imp_value_depreciated_value);

                interactor.updateLotDetail(detail);

                Log.d("int--landImpSave", "Logic triggered");
            }

            @Override
            public void onError(Throwable e) {

            }
        };
    }
}
