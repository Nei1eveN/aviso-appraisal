package com.gdslinkasia.improvementvaluation.contract.land_imp_valuation_list;
import com.gdslinkasia.base.model.details.ValrepLandimpImpValuation;

import java.util.List;

import io.reactivex.Flowable;
import io.reactivex.Single;

public interface LandImpValuationContract {

    interface View {
        void showProgress(String title, String message);

        void hideProgress();

        void showList(List<ValrepLandimpImpValuation> valrepLandimpImpValuation);

        void showEmptyState(String message);

        void showErrorDialog(String title, String message);
    }

    interface Presenter {
        void onStart(String recordId);

        void onDestroy();
    }

    interface Interactor {
        Single<List<ValrepLandimpImpValuation>> getDetailsList(String recordId);

        Flowable<List<ValrepLandimpImpValuation>> getListFlowable(String recordId);
    }

}
