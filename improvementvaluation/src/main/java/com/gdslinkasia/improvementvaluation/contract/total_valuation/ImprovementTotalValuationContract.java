package com.gdslinkasia.improvementvaluation.contract.total_valuation;

import com.gdslinkasia.base.model.job.Record;

import io.reactivex.Single;

public interface ImprovementTotalValuationContract {
    interface View {
        void showProgress(String title, String message);
        void hideProgress();
        void showSnackMessage(String message);
        void showDetails(Record record);
        void showExitDialog(String title, String message);
        void showErrorDialog(String title, String message);
    }
    interface Presenter {
        void onStart(String recordId);
        void onDestroy();

        void updatePropertyDesc(
                String recordId,
                String valrep_landimp_imp_value_total_imp_value,
                String valrep_landimp_imp_valuation_remarks,
                String valrep_landimp_imp_value_total_other_imp_value
        );
    }
    interface Interactor {
        Single<Record> getRecordById(String recordId);
        void updatePropertyDesc(Record record);
    }
}
