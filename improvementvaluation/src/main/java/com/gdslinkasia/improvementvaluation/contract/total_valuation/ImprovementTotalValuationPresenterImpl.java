package com.gdslinkasia.improvementvaluation.contract.total_valuation;

import android.app.Application;
import android.os.Handler;
import android.util.Log;

import com.gdslinkasia.base.model.job.Record;

import io.reactivex.Single;
import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.disposables.CompositeDisposable;
import io.reactivex.observers.DisposableSingleObserver;
import io.reactivex.schedulers.Schedulers;

public class ImprovementTotalValuationPresenterImpl implements ImprovementTotalValuationContract.Presenter {

    private ImprovementTotalValuationContract.View view;
    private ImprovementTotalValuationContract.Interactor interactor;

    private CompositeDisposable compositeDisposable = new CompositeDisposable();

    public ImprovementTotalValuationPresenterImpl(ImprovementTotalValuationContract.View view, Application application) {
        this.view = view;
        this.interactor = new ImprovementTotalValuationInteractorImpl(application);
    }


    @Override
    public void onStart(String recordId) {
        view.showProgress("Loading Record", "Please wait...");
        compositeDisposable.add(getLocalSingleObserver(recordId).subscribeWith(getLocalObserver()));
    }

    @Override
    public void onDestroy() {
        view = null;
        compositeDisposable.clear();
    }

    @Override
    public void updatePropertyDesc(String recordId,
                                   String valrep_landimp_imp_value_total_imp_value,
                                   String valrep_landimp_imp_valuation_remarks,
                                   String valrep_landimp_imp_value_total_other_imp_value) {

                compositeDisposable.add(getLocalSingleObserver(recordId).subscribeWith(
                        getRecordUpdate(
                                 valrep_landimp_imp_value_total_imp_value,
                                 valrep_landimp_imp_valuation_remarks,
                                 valrep_landimp_imp_value_total_other_imp_value
                        )
                ));

    }


    //TODO Single disposable for fetching 1 Record from database based on recordId
    private Single<Record> getLocalSingleObserver(String recordId) {
        return interactor.getRecordById(recordId)
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread());
    }

    //TODO Results from disposable will be fetched here
    private DisposableSingleObserver<Record> getLocalObserver() {
        return new DisposableSingleObserver<Record>() {
            @Override
            public void onSuccess(Record record) {
                Log.d("int--LocalRepo", "Logic Triggered: UpdateAppraisalDetailsPresenter");
                view.showDetails(record);
                view.showSnackMessage("Loading Successful");
                view.hideProgress();
                new Handler().postDelayed(() -> view.hideProgress(), 1000);
            }

            @Override
            public void onError(Throwable e) {
                view.hideProgress();
                view.showExitDialog("Record Error", e.getMessage());
            }
        };
    }

    //TODO Update Record with parameters passed from method updateScopeOfWork
    private DisposableSingleObserver<Record> getRecordUpdate(
            String valrep_landimp_imp_value_total_imp_value,
            String valrep_landimp_imp_valuation_remarks,
            String valrep_landimp_imp_value_total_other_imp_value
    ) {
        return new DisposableSingleObserver<Record>() {
            @Override
            public void onSuccess(Record record) {
                Log.d("int--RecordFlowable", "Logic Triggered: ImprovementTotalValuationriptions");

                record.setValrepLandimpImpValueTotalImpValue(valrep_landimp_imp_value_total_imp_value);
                record.setValrepLandimpImpValuationRemarks(valrep_landimp_imp_valuation_remarks);
                record.setValrepLandimpImpValueTotalOtherImpValue(valrep_landimp_imp_value_total_other_imp_value);

                interactor.updatePropertyDesc(record);

                view.showSnackMessage("Saving Successful");

                new Handler().postDelayed(() -> view.hideProgress(), 2000);
            }

            @Override
            public void onError(Throwable e) {
                view.hideProgress();
                view.showExitDialog("Record Error", e.getMessage());
            }
        };
    }
}