package com.gdslinkasia.scope_of_work.contract;


import com.gdslinkasia.base.model.job.Record;

import io.reactivex.Single;

public interface UpdateScopeWorkContract {
    interface View {
        void showProgress(String title, String message);
        void hideProgress();
        void showSnackMessage(String message);
        void showDetails(Record record);
        void showExitDialog(String title, String message);
        void showErrorDialog(String title, String message);
    }
    interface Presenter {
        void onStart(String recordId);
        void onDestroy();

        void updateScopeWork(String recordId,
                             //Nature and Source of Information
                             String valrep_landimp_basic_description,
                             String bov_source_info_buyer_seller,
                             String bov_source_info_buyer_sellerbroker,
                             String bov_source_info_unaffiliated_broker,
                             String bov_source_info_publication,
                             String bov_source_info_property_manager,
                             String bov_source_info_contractor,
                             String bov_source_info_private_website,
                             String bov_source_info_government,
                             String bov_source_info_hoa_officer,
                             String bov_source_info_lgu_official,
                             String bov_source_info_simulation,
                             String bov_source_info_other,
                             // Verification (w/o Cert)
                             String valrep_landimp_id_locational_verification,
                             String valrep_landimp_id_locational_verification_subd,
                             String valrep_landimp_id_zoning_verification,
                             //TODO - Inspection (Land Only)
                             String valrep_landimp_id_ocular_inspection,
                             String valrep_landimp_id_site_assessment,
                             //TODO - Inspection (Land Imp)
                             String valrep_landimp_id_building_inspection,
                             String valrep_landimp_id_sampling_verification,
                             String valrep_landimp_id_building_components,
                             String valrep_landimp_id_utilities_services,
                             //TODO - Inspection (Condo)
                             String valrep_landimp_id_unit_inspection,
                             String valrep_landimp_id_unit_sampling_verification,
                             // Market Research
                             String valrep_landimp_id_neighborhood_research,
                             String valrep_landimp_id_local_brokers,
                             String valrep_landimp_id_mls,
                             String valrep_landimp_id_socio_physical_profile,
                             // Valuation Analysis
                             String valrep_landimp_id_comparison,
                             String valrep_landimp_id_use_analysis,
                             String valrep_landimp_id_typical_participants,
                             String valrep_landimp_id_market_analysis,
                             //TODO - Inspection (Land Imp)
                             String valrep_landimp_id_cost_estimate,
                             String valrep_landimp_id_depreciated_analysis,
                             //Eng. and other surveys
                             String valrep_landimp_id_measurement_verification,
                             String valrep_landimp_id_geodetic_survey,
                             String valrep_landimp_id_survey_hazardous,
                             String valrep_landimp_id_engineering_tests,
                             String valrep_landimp_id_quantity_survey,
                             //TODO - Inspection (Condo)
                             String valrep_landimp_id_bldg_valuation,
                             String valrep_landimp_id_land_valuation,
                             String valrep_landimp_id_natural_hazards,
                             String valrep_landimp_id_value_research,
                             String valrep_landimp_id_traffic_count,
                             String valrep_landimp_id_special_laws,
                             //Verification w/ cert
                             String valrep_landimp_id_deeds_sale,
                             String valrep_landimp_id_tax_declaration,
                             String valrep_landimp_id_tax_map,
                             String valrep_landimp_id_zoning_certification,
                             String valrep_landimp_id_zonal_value,
                             //Assumption
                             String valrep_landimp_assumptions_1,
                             String valrep_landimp_assumptions_2,
                             String valrep_landimp_assumptions_3,
                             String valrep_landimp_assumptions_4,
                             String valrep_landimp_assumptions_5,
                             String valrep_landimp_special_assumptions_check,
                             //Qualifying Statement
                             String valrep_lc_ivs_qualifying_statements_check,
                             String lv_ivs_qualifying_statements_1,
                             String lv_ivs_qualifying_statements_2,
                             String lv_ivs_qualifying_statements_3,
                             String lv_ivs_qualifying_statements_4,
                             String lv_ivs_qualifying_statements_5,
                             String lv_ivs_qualifying_statements_6,
                             String lc_ivs_qualifying_statements_desc
        );
    }
    interface Interactor {
        Single<Record> getRecordById(String recordId);
        void updateScopeWork(Record record);
    }
}
