package com.gdslinkasia.scope_of_work.contract;

import android.app.Application;
import android.os.Handler;
import android.util.Log;

import com.gdslinkasia.base.model.job.Record;

import io.reactivex.Single;
import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.disposables.CompositeDisposable;
import io.reactivex.observers.DisposableSingleObserver;
import io.reactivex.schedulers.Schedulers;

public class UpdateScopeWorkPresenterImpl implements UpdateScopeWorkContract.Presenter {

    private UpdateScopeWorkContract.View view;
    private UpdateScopeWorkContract.Interactor interactor;

    private CompositeDisposable compositeDisposable = new CompositeDisposable();

    public UpdateScopeWorkPresenterImpl(UpdateScopeWorkContract.View view, Application application) {
        this.view = view;
        this.interactor = new UpdateScopeWorkInteractorImpl(application);
    }


    @Override
    public void onStart(String recordId) {
        view.showProgress("Loading Record", "Please wait...");
        compositeDisposable.add(getLocalSingleObserver(recordId).subscribeWith(getLocalObserver()));
    }

    @Override
    public void onDestroy() {
        view = null;
        compositeDisposable.clear();
    }

    @Override
    public void updateScopeWork(
            String recordId,
            //Nature and Source of Information
            String valrep_landimp_basic_description,
            String bov_source_info_buyer_seller,
            String bov_source_info_buyer_sellerbroker,
            String bov_source_info_unaffiliated_broker,
            String bov_source_info_publication,
            String bov_source_info_property_manager,
            String bov_source_info_contractor,
            String bov_source_info_private_website,
            String bov_source_info_government,
            String bov_source_info_hoa_officer,
            String bov_source_info_lgu_official,
            String bov_source_info_simulation,
            String bov_source_info_other,
            // Verification (w/o Cert)
            String valrep_landimp_id_locational_verification,
            String valrep_landimp_id_locational_verification_subd,
            String valrep_landimp_id_zoning_verification,
            //TODO - Inspection (Land Only)
            String valrep_landimp_id_ocular_inspection,
            String valrep_landimp_id_site_assessment,
            //TODO - Inspection (Land Imp)
            String valrep_landimp_id_building_inspection,
            String valrep_landimp_id_sampling_verification,
            String valrep_landimp_id_building_components,
            String valrep_landimp_id_utilities_services,
            //TODO - Inspection (Condo)
            String valrep_landimp_id_unit_inspection,
            String valrep_landimp_id_unit_sampling_verification,
            // Market Research
            String valrep_landimp_id_neighborhood_research,
            String valrep_landimp_id_local_brokers,
            String valrep_landimp_id_mls,
            String valrep_landimp_id_socio_physical_profile,
            // Valuation Analysis
            String valrep_landimp_id_comparison,
            String valrep_landimp_id_use_analysis,
            String valrep_landimp_id_typical_participants,
            String valrep_landimp_id_market_analysis,
            //TODO - Inspection (Land Imp)
            String valrep_landimp_id_cost_estimate,
            String valrep_landimp_id_depreciated_analysis,
            //Eng. and other surveys
            String valrep_landimp_id_measurement_verification,
            String valrep_landimp_id_geodetic_survey,
            String valrep_landimp_id_survey_hazardous,
            String valrep_landimp_id_engineering_tests,
            String valrep_landimp_id_quantity_survey,
            //TODO - Inspection (Condo)
            String valrep_landimp_id_bldg_valuation,
            String valrep_landimp_id_land_valuation,
            String valrep_landimp_id_natural_hazards,
            String valrep_landimp_id_value_research,
            String valrep_landimp_id_traffic_count,
            String valrep_landimp_id_special_laws,
            //Verification w/ cert
            String valrep_landimp_id_deeds_sale,
            String valrep_landimp_id_tax_declaration,
            String valrep_landimp_id_tax_map,
            String valrep_landimp_id_zoning_certification,
            String valrep_landimp_id_zonal_value,
            //Assumption
            String valrep_landimp_assumptions_1,
            String valrep_landimp_assumptions_2,
            String valrep_landimp_assumptions_3,
            String valrep_landimp_assumptions_4,
            String valrep_landimp_assumptions_5,
            String valrep_landimp_special_assumptions_check,
            //Qualifying Statement
            String valrep_lc_ivs_qualifying_statements_check,
            String lv_ivs_qualifying_statements_1,
            String lv_ivs_qualifying_statements_2,
            String lv_ivs_qualifying_statements_3,
            String lv_ivs_qualifying_statements_4,
            String lv_ivs_qualifying_statements_5,
            String lv_ivs_qualifying_statements_6,
            String lc_ivs_qualifying_statements_desc) {
        compositeDisposable.add(getLocalSingleObserver(recordId).subscribeWith(
                getRecordUpdate(
                        //Nature and Source of Information
                         valrep_landimp_basic_description,
                         bov_source_info_buyer_seller,
                         bov_source_info_buyer_sellerbroker,
                         bov_source_info_unaffiliated_broker,
                         bov_source_info_publication,
                         bov_source_info_property_manager,
                         bov_source_info_contractor,
                         bov_source_info_private_website,
                         bov_source_info_government,
                         bov_source_info_hoa_officer,
                         bov_source_info_lgu_official,
                         bov_source_info_simulation,
                         bov_source_info_other,
                        // Verification (w/o Cert)
                         valrep_landimp_id_locational_verification,
                         valrep_landimp_id_locational_verification_subd,
                         valrep_landimp_id_zoning_verification,
                        //TODO - Inspection (Land Only)
                         valrep_landimp_id_ocular_inspection,
                         valrep_landimp_id_site_assessment,
                        //TODO - Inspection (Land Imp)
                         valrep_landimp_id_building_inspection,
                         valrep_landimp_id_sampling_verification,
                         valrep_landimp_id_building_components,
                         valrep_landimp_id_utilities_services,
                        //TODO - Inspection (Condo)
                         valrep_landimp_id_unit_inspection,
                         valrep_landimp_id_unit_sampling_verification,
                        // Market Research
                         valrep_landimp_id_neighborhood_research,
                         valrep_landimp_id_local_brokers,
                         valrep_landimp_id_mls,
                         valrep_landimp_id_socio_physical_profile,
                        // Valuation Analysis
                         valrep_landimp_id_comparison,
                         valrep_landimp_id_use_analysis,
                         valrep_landimp_id_typical_participants,
                         valrep_landimp_id_market_analysis,
                        //TODO - Inspection (Land Imp)
                         valrep_landimp_id_cost_estimate,
                         valrep_landimp_id_depreciated_analysis,
                        //Eng. and other surveys
                         valrep_landimp_id_measurement_verification,
                         valrep_landimp_id_geodetic_survey,
                         valrep_landimp_id_survey_hazardous,
                         valrep_landimp_id_engineering_tests,
                         valrep_landimp_id_quantity_survey,
                        //TODO - Inspection (Condo)
                         valrep_landimp_id_bldg_valuation,
                         valrep_landimp_id_land_valuation,
                         valrep_landimp_id_natural_hazards,
                         valrep_landimp_id_value_research,
                         valrep_landimp_id_traffic_count,
                         valrep_landimp_id_special_laws,
                        //Verification w/ cert
                         valrep_landimp_id_deeds_sale,
                         valrep_landimp_id_tax_declaration,
                         valrep_landimp_id_tax_map,
                         valrep_landimp_id_zoning_certification,
                         valrep_landimp_id_zonal_value,
                        //Assumption
                         valrep_landimp_assumptions_1,
                         valrep_landimp_assumptions_2,
                         valrep_landimp_assumptions_3,
                         valrep_landimp_assumptions_4,
                         valrep_landimp_assumptions_5,
                         valrep_landimp_special_assumptions_check,
                        //Qualifying Statement
                         valrep_lc_ivs_qualifying_statements_check,
                         lv_ivs_qualifying_statements_1,
                         lv_ivs_qualifying_statements_2,
                         lv_ivs_qualifying_statements_3,
                         lv_ivs_qualifying_statements_4,
                         lv_ivs_qualifying_statements_5,
                         lv_ivs_qualifying_statements_6,
                         lc_ivs_qualifying_statements_desc
                )
        ));
    }


    //TODO Single disposable for fetching 1 Record from database based on recordId
    private Single<Record> getLocalSingleObserver(String recordId) {
        return interactor.getRecordById(recordId)
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread());
    }

    //TODO Results from disposable will be fetched here
    private DisposableSingleObserver<Record> getLocalObserver() {
        return new DisposableSingleObserver<Record>() {
            @Override
            public void onSuccess(Record record) {
                Log.d("int--LocalRepo", "Logic Triggered: UpdateAppraisalDetailsPresenter");
                view.showDetails(record);
                view.showSnackMessage("Loading Successful");
                view.hideProgress();
                new Handler().postDelayed(() -> view.hideProgress(), 1000);
            }

            @Override
            public void onError(Throwable e) {
                view.hideProgress();
                view.showExitDialog("Record Error", e.getMessage());
            }
        };
    }

    //TODO Update Record with parameters passed from method updateScopeOfWork
    private DisposableSingleObserver<Record> getRecordUpdate(  //Nature and Source of Information
                                                               String valrep_landimp_basic_description,
                                                               String bov_source_info_buyer_seller,
                                                               String bov_source_info_buyer_sellerbroker,
                                                               String bov_source_info_unaffiliated_broker,
                                                               String bov_source_info_publication,
                                                               String bov_source_info_property_manager,
                                                               String bov_source_info_contractor,
                                                               String bov_source_info_private_website,
                                                               String bov_source_info_government,
                                                               String bov_source_info_hoa_officer,
                                                               String bov_source_info_lgu_official,
                                                               String bov_source_info_simulation,
                                                               String bov_source_info_other,
                                                               // Verification (w/o Cert)
                                                               String valrep_landimp_id_locational_verification,
                                                               String valrep_landimp_id_locational_verification_subd,
                                                               String valrep_landimp_id_zoning_verification,
                                                               //TODO - Inspection (Land Only)
                                                               String valrep_landimp_id_ocular_inspection,
                                                               String valrep_landimp_id_site_assessment,
                                                               //TODO - Inspection (Land Imp)
                                                               String valrep_landimp_id_building_inspection,
                                                               String valrep_landimp_id_sampling_verification,
                                                               String valrep_landimp_id_building_components,
                                                               String valrep_landimp_id_utilities_services,
                                                               //TODO - Inspection (Condo)
                                                               String valrep_landimp_id_unit_inspection,
                                                               String valrep_landimp_id_unit_sampling_verification,
                                                               // Market Research
                                                               String valrep_landimp_id_neighborhood_research,
                                                               String valrep_landimp_id_local_brokers,
                                                               String valrep_landimp_id_mls,
                                                               String valrep_landimp_id_socio_physical_profile,
                                                               // Valuation Analysis
                                                               String valrep_landimp_id_comparison,
                                                               String valrep_landimp_id_use_analysis,
                                                               String valrep_landimp_id_typical_participants,
                                                               String valrep_landimp_id_market_analysis,
                                                               //TODO - Inspection (Land Imp)
                                                               String valrep_landimp_id_cost_estimate,
                                                               String valrep_landimp_id_depreciated_analysis,
                                                               //Eng. and other surveys
                                                               String valrep_landimp_id_measurement_verification,
                                                               String valrep_landimp_id_geodetic_survey,
                                                               String valrep_landimp_id_survey_hazardous,
                                                               String valrep_landimp_id_engineering_tests,
                                                               String valrep_landimp_id_quantity_survey,
                                                               //TODO - Inspection (Condo)
                                                               String valrep_landimp_id_bldg_valuation,
                                                               String valrep_landimp_id_land_valuation,
                                                               String valrep_landimp_id_natural_hazards,
                                                               String valrep_landimp_id_value_research,
                                                               String valrep_landimp_id_traffic_count,
                                                               String valrep_landimp_id_special_laws,
                                                               //Verification w/ cert
                                                               String valrep_landimp_id_deeds_sale,
                                                               String valrep_landimp_id_tax_declaration,
                                                               String valrep_landimp_id_tax_map,
                                                               String valrep_landimp_id_zoning_certification,
                                                               String valrep_landimp_id_zonal_value,
                                                               //Assumption
                                                               String valrep_landimp_assumptions_1,
                                                               String valrep_landimp_assumptions_2,
                                                               String valrep_landimp_assumptions_3,
                                                               String valrep_landimp_assumptions_4,
                                                               String valrep_landimp_assumptions_5,
                                                               String valrep_landimp_special_assumptions_check,
                                                               //Qualifying Statement
                                                               String valrep_lc_ivs_qualifying_statements_check,
                                                               String lv_ivs_qualifying_statements_1,
                                                               String lv_ivs_qualifying_statements_2,
                                                               String lv_ivs_qualifying_statements_3,
                                                               String lv_ivs_qualifying_statements_4,
                                                               String lv_ivs_qualifying_statements_5,
                                                               String lv_ivs_qualifying_statements_6,
                                                               String lc_ivs_qualifying_statements_desc) {
        return new DisposableSingleObserver<Record>() {
            @Override
            public void onSuccess(Record record) {
                Log.d("int--RecordFlowable", "Logic Triggered: UpdateAppraisalDetailsPresenter");

                //Nature and Source of Information
                record.setValrepLandimpBasicDescription(valrep_landimp_basic_description);
                record.setBovSourceInfoBuyerSeller(bov_source_info_buyer_seller);
                record.setBovSourceInfoBuyerSellerbroker(bov_source_info_buyer_sellerbroker);
                record.setBovSourceInfoUnaffiliatedBroker(bov_source_info_unaffiliated_broker);
                record.setBovSourceInfoPublication(bov_source_info_publication);
                record.setBovSourceInfoPropertyManager(bov_source_info_property_manager);
                record.setBovSourceInfoContractor(bov_source_info_contractor);
                record.setBovSourceInfoPrivateWebsite(bov_source_info_private_website);
                record.setBovSourceInfoGovernment(bov_source_info_government);
                record.setBovSourceInfoHoaOfficer(bov_source_info_hoa_officer);
                record.setBovSourceInfoLguOfficial(bov_source_info_lgu_official);
                record.setBovSourceInfoSimulation(bov_source_info_simulation);
                record.setBovSourceInfoOther(bov_source_info_other);

                // Verification (w/o Cert)
                record.setValrepLandimpIdLocationalVerification(valrep_landimp_id_locational_verification);
                record.setValrepLandimpIdLocationalVerificationSubd(valrep_landimp_id_locational_verification_subd);
                record.setValrepLandimpIdZoningVerification(valrep_landimp_id_zoning_verification);

                //TODO - Inspection (Land Only)
                record.setValrepLandimpIdOcularInspection(valrep_landimp_id_ocular_inspection);
                record.setValrepLandimpIdSiteAssessment(valrep_landimp_id_site_assessment);
                //TODO Inspection Land w/ Improvements
                record.setValrepLandimpIdBuildingInspection(valrep_landimp_id_building_inspection);
                record.setValrepLandimpIdSamplingVerification(valrep_landimp_id_sampling_verification);
                record.setValrepLandimpIdBuildingComponents(valrep_landimp_id_building_components);
                record.setValrepLandimpIdUtilitiesServices(valrep_landimp_id_utilities_services);
                //TODO Inspection Condo
                record.setValrepLandimpIdUnitInspection(valrep_landimp_id_unit_inspection);
                record.setValrepLandimpIdUnitSamplingVerification(valrep_landimp_id_unit_sampling_verification);
                // Market Research
                record.setValrepLandimpIdNeighborhoodResearch(valrep_landimp_id_neighborhood_research);
                record.setValrepLandimpIdLocalBrokers(valrep_landimp_id_local_brokers);
                record.setValrepLandimpIdMls(valrep_landimp_id_mls);
                record.setValrepLandimpIdSocioPhysicalProfile(valrep_landimp_id_socio_physical_profile);

                // Valuation Analysis
                record.setValrepLandimpIdComparison(valrep_landimp_id_comparison);
                record.setValrepLandimpIdUseAnalysis(valrep_landimp_id_use_analysis);
                record.setValrepLandimpIdTypicalParticipants(valrep_landimp_id_typical_participants);
                record.setValrepLandimpIdMarketAnalysis(valrep_landimp_id_market_analysis);

                //  TODO-Land w/ Improvements NO GETTER SETTER YET
               record.setValrepLandimpIdCostEstimate(valrep_landimp_id_cost_estimate);
               record.setValrepLandimpDepreciatedAnalysis(valrep_landimp_id_depreciated_analysis);

                //Eng. and other surveys
                record.setValrepLandimpIdMeasurementVerification(valrep_landimp_id_measurement_verification);
                record.setValrepLandimpIdGeodeticSurvey(valrep_landimp_id_geodetic_survey);
                record.setValrepLandimpIdSurveyHazardous(valrep_landimp_id_survey_hazardous);
                record.setValrepLandimpIdEngineeringTests(valrep_landimp_id_engineering_tests);
                record.setValrepLandimpIdQuantitySurvey(valrep_landimp_id_quantity_survey);
                record.setValrepLandimpIdNaturalHazards(valrep_landimp_id_natural_hazards);
                record.setValrepLandimpIdValueResearch(valrep_landimp_id_value_research);
                record.setValrepLandimpIdTrafficCount(valrep_landimp_id_traffic_count);
                record.setValrepLandimpIdSpecialLaws(valrep_landimp_id_special_laws);
                //TODO ENG_OTHER_SURVEY Condo
                record.setValrepLandimpIdBldgValuation(valrep_landimp_id_bldg_valuation);
                record.setValrepLandimpIdLandValuation(valrep_landimp_id_land_valuation);

                //Verification w/ cert
                record.setValrepLandimpIdDeedsSale(valrep_landimp_id_deeds_sale);
                record.setValrepLandimpIdTaxDeclaration(valrep_landimp_id_tax_declaration);
                record.setValrepLandimpIdTaxMap(valrep_landimp_id_tax_map);
                record.setValrepLandimpIdZoningCertification(valrep_landimp_id_zoning_certification);
                record.setValrepLandimpZonalValue(valrep_landimp_id_zonal_value);

                //Assumption
                record.setValrepLandimpAssumptions1(valrep_landimp_assumptions_1);
                record.setValrepLandimpAssumptions2(valrep_landimp_assumptions_2);
                record.setValrepLandimpAssumptions3(valrep_landimp_assumptions_3);
                record.setValrepLandimpAssumptions4(valrep_landimp_assumptions_4);
                record.setValrepLandimpAssumptions5(valrep_landimp_assumptions_5);
                record.setValrepLandimpSpecialAssumptionsCheck(valrep_landimp_special_assumptions_check);

                //Qualifying Statement
                record.setValrepLcIvsQualifyingStatementsCheck(valrep_lc_ivs_qualifying_statements_check);
                record.setLvIvsQualifyingStatements1(lv_ivs_qualifying_statements_1);
                record.setLvIvsQualifyingStatements2(lv_ivs_qualifying_statements_2);
                record.setLvIvsQualifyingStatements3(lv_ivs_qualifying_statements_3);
                record.setLvIvsQualifyingStatements4(lv_ivs_qualifying_statements_4);
                record.setLvIvsQualifyingStatements5(lv_ivs_qualifying_statements_5);
                record.setLvIvsQualifyingStatements6(lv_ivs_qualifying_statements_6);
                record.setLcIvsQualifyingStatementsDesc(lc_ivs_qualifying_statements_desc);


                interactor.updateScopeWork(record);

                view.showSnackMessage("Saving Successful");

                new Handler().postDelayed(() -> view.hideProgress(), 2000);
            }

            @Override
            public void onError(Throwable e) {
                view.hideProgress();
                view.showExitDialog("Record Error", e.getMessage());
            }
        };
    }
}