package com.gdslinkasia.scope_of_work.contract;

import android.app.Application;

import com.gdslinkasia.base.database.repository.JobDetailsLocalRepository;
import com.gdslinkasia.base.model.job.Record;

import io.reactivex.Single;

class UpdateScopeWorkInteractorImpl implements UpdateScopeWorkContract.Interactor {

    private JobDetailsLocalRepository localRepository;

    public UpdateScopeWorkInteractorImpl(Application application) {
        localRepository = new JobDetailsLocalRepository(application);
    }

    @Override
    public Single<Record> getRecordById(String recordId) {
        return localRepository.getRecordById(recordId);
    }

    @Override
    public void updateScopeWork(Record record) {
        localRepository.updateRecord(record);  //update all record: should be rename to updateRecordAll
    }
}
