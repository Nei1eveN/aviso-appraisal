package com.gdslinkasia.scope_of_work;

import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;
import android.view.MenuItem;
import android.view.View;
import android.widget.AutoCompleteTextView;
import android.widget.CheckBox;
import android.widget.LinearLayout;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;

import com.gdslinkasia.base.model.job.Record;
import com.gdslinkasia.base.utils.ActivityIntent;
import com.gdslinkasia.commonresources.R2;
import com.gdslinkasia.scope_of_work.contract.UpdateScopeWorkContract;
import com.gdslinkasia.scope_of_work.contract.UpdateScopeWorkPresenterImpl;
import com.google.android.material.textfield.TextInputLayout;

import java.util.Objects;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

import static com.gdslinkasia.base.database.DatabaseUtils.APPRAISAL_TYPE;
import static com.gdslinkasia.base.database.DatabaseUtils.RECORD_ID;
import static com.gdslinkasia.base.database.DatabaseUtils.VAR_LAND_IMP;
import static com.gdslinkasia.base.database.DatabaseUtils.VAR_LAND_ONLY;
import static com.gdslinkasia.base.utils.GlobalFunctions.GET_CONTENT_ADAPTER;
import static com.gdslinkasia.base.utils.GlobalFunctions.GET_IS_CHECKED_FROM_CHECKBOX_VISIBILITY;
import static com.gdslinkasia.base.utils.GlobalFunctions.ONCHANGE_IS_CHECKED_CHECKBOX_VISIBILITY;
import static com.gdslinkasia.base.utils.GlobalFunctions.getCheckboxBoolean;
import static com.gdslinkasia.base.utils.GlobalString.CHECKBOX_TO_STRING;
import static com.gdslinkasia.base.utils.GlobalString.CONDO;


public class ScopeWorkActivity extends AppCompatActivity implements UpdateScopeWorkContract.View{

    @BindView(R2.id.txBasicDesc) TextInputLayout txBasicDesc;
    @BindView(R2.id.acBasicDesc) AutoCompleteTextView acBasicDesc;

    /*cb nature and Src Info*/
    @BindView(R2.id.cbNatureBuySell) CheckBox cbNatureBuySell;
    @BindView(R2.id.cbNatureBuySellBroker) CheckBox cbNatureBuySellBroker;
    @BindView(R2.id.cbNatureUnaffiliate) CheckBox cbNatureUnaffiliate;
    @BindView(R2.id.cbNaturePub) CheckBox cbNaturePub;
    @BindView(R2.id.cbProperty) CheckBox cbPropManage;
    @BindView(R2.id.cbNatureContract) CheckBox cbNatureContract;
    @BindView(R2.id.cbNatureWeb) CheckBox cbNatureWeb;
    @BindView(R2.id.cbNatureGov) CheckBox cbNatureGov;
    @BindView(R2.id.cbNatureHome) CheckBox cbNatureHome;
    @BindView(R2.id.cbNatureLGU) CheckBox cbNatureLGU;
    @BindView(R2.id.cbNatureSimul) CheckBox cbNatureSimul;

    @BindView(R2.id.txNatureOther) TextInputLayout txNatureOther;
    @BindView(R2.id.acNatureOther) AutoCompleteTextView acNatureOther;

    /*
     In Scope Work
     verification w/out cert**/
    @BindView(R2.id.cbVerifAssessor) CheckBox cbVerifAssessor;
    @BindView(R2.id.cbVerifSubdiv) CheckBox cbVerifSubdiv;
    @BindView(R2.id.cbVerifZoning) CheckBox cbVerifZoning;

    /*Inspection*/
    @BindView(R2.id.cbInsOccular) CheckBox cbInsOccular;
    @BindView(R2.id.cbInsSite) CheckBox cbInsSite;

    //TODO Inspection Land w/ Improvements
    @BindView(R2.id.cbInsBuilding) CheckBox cbInsBuilding;
    @BindView(R2.id.cbInsSampling) CheckBox cbInsSampling;
    @BindView(R2.id.cbInsIdBuilding) CheckBox cbInsIdBuilding;
    @BindView(R2.id.cbInsIdUtilities) CheckBox cbInsIdUtilities;

    //TODO Inspection Condo
    @BindView(R2.id.cbUnitinspection) CheckBox cbUnitinspection;
    @BindView(R2.id.cbUnitSamplingInspection) CheckBox cbUnitSamplingInspection;


    /*Market Research*/
    @BindView(R2.id.cbMarketNeighbor) CheckBox cbMarketNeighbor;
    @BindView(R2.id.cbMarketInterview) CheckBox cbMarketInterview;
    @BindView(R2.id.cbMarketMLS) CheckBox cbMarketMLS;
    @BindView(R2.id.cbMarketSocio) CheckBox cbMarketSocio;

    /*Valuation Analysis*/
    @BindView(R2.id.cbValComparison) CheckBox cbValComparison;
    @BindView(R2.id.cbValHighest) CheckBox cbValHighest;
    @BindView(R2.id.cbValPropMarket) CheckBox cbValPropMarket;
    @BindView(R2.id.cbValComparative) CheckBox cbValComparative;
    @BindView(R2.id.cbValEstCost) CheckBox cbValEstCost;
    @BindView(R2.id.cbValPhysObso) CheckBox cbValPhysObso;

    /*Out Scope Work*/
    /*Eng. and other surveys*/
    @BindView(R2.id.cbEngPropMear) CheckBox cbEngPropMear;
    @BindView(R2.id.cbEngGeo) CheckBox cbEngGeo;
    @BindView(R2.id.cbEngSoil) CheckBox cbEngSoil;
    @BindView(R2.id.cbEngStruct) CheckBox cbEngStruct;
    @BindView(R2.id.cbEngQuantity) CheckBox cbEngQuantity;
    @BindView(R2.id.cbEngRiskManage) CheckBox cbEngRiskManage;
    @BindView(R2.id.cbEngAssessor) CheckBox cbEngAssessor;
    @BindView(R2.id.cbEngTraffic) CheckBox cbEngTraffic;
    @BindView(R2.id.cbEngSurveyRight) CheckBox cbEngSurveyRight;

    //TODO Inspection Condo
    @BindView(R2.id.cbEngBuildingVal) CheckBox cbEngBuildingVal;
    @BindView(R2.id.cbEngLandVal) CheckBox cbEngLandVal;

    /*Verification w/ cert*/
    @BindView(R2.id.cbEngDeedSale) CheckBox cbEngDeedSale;
    @BindView(R2.id.cbEngTaxDec) CheckBox cbEngTaxDec;
    @BindView(R2.id.cbEngTaxMap) CheckBox cbEngTaxMap;
    @BindView(R2.id.cbEngZoning) CheckBox cbEngZoning;
    @BindView(R2.id.cbEngZonal) CheckBox cbEngZonal;

    /*Assumption*/
    @BindView(R2.id.cbAssFreeClear) CheckBox cbAssFreeClear;
    @BindView(R2.id.cbAssOwnership) CheckBox cbAssOwnership;
    @BindView(R2.id.cbAssIndicate) CheckBox cbAssIndicate;
    @BindView(R2.id.cbAssCurrently) CheckBox cbAssCurrently;
    @BindView(R2.id.cbAssThird) CheckBox cbAssThird;
    @BindView(R2.id.cbAssSpecial) CheckBox cbAssSpecial;

    @BindView(R2.id.container_specialAssumptions) LinearLayout container_specialAssumptions;


    /*Qualifying Statement*/
    @BindView(R2.id.cbQualifying) CheckBox cbQualifying;

    @BindView(R2.id.container_qualifyingStatement) LinearLayout container_qualifyingStatement;
    @BindView(R2.id.cbQualLandArea) CheckBox cbQualLandArea;
    @BindView(R2.id.cbQualImpHighest) CheckBox cbQualImpHighest;
    @BindView(R2.id.cbQualCoverage) CheckBox cbQualCoverage;
    @BindView(R2.id.cbQualPremiseAss) CheckBox cbQualPremiseAss;
    @BindView(R2.id.cbQualPremiseHigh) CheckBox cbQualPremiseHigh;
    @BindView(R2.id.cbQualAdj) CheckBox cbQualAdj;

    @BindView(R2.id.txtil_remark_qualifyingStatement) TextInputLayout txtil_remark_qualifyingStatement;
    @BindView(R2.id.acQualRemarks) AutoCompleteTextView acQualRemarks;

    private String recordId;
    private UpdateScopeWorkContract.Presenter presenter;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.form_scope_work);
        ButterKnife.bind(this);

        Bundle putExtraBundle = getIntent().getExtras();
        String appraisal_type;
        if(putExtraBundle == null) {
            recordId= null;
            return;
        } else {
            recordId= putExtraBundle.getString(RECORD_ID);
            appraisal_type = putExtraBundle.getString(APPRAISAL_TYPE);
        }

        Objects.requireNonNull(getSupportActionBar()).setTitle("Scope of Work");
        getSupportActionBar().setSubtitle(appraisal_type);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);

        presenter = new UpdateScopeWorkPresenterImpl(this, getApplication());

        if(Objects.requireNonNull(appraisal_type).equals(VAR_LAND_ONLY)){
            //land imp checkbox
            cbInsBuilding.setVisibility(View.GONE);
            cbInsSampling.setVisibility(View.GONE);
            cbInsIdBuilding.setVisibility(View.GONE);
            cbInsIdUtilities.setVisibility(View.GONE);
            cbValEstCost.setVisibility(View.GONE);
            cbValPhysObso.setVisibility(View.GONE);
            //condo checkbox
            cbUnitinspection.setVisibility(View.GONE);
            cbUnitSamplingInspection.setVisibility(View.GONE);
            cbEngLandVal.setVisibility(View.GONE);
            cbEngBuildingVal.setVisibility(View.GONE);

        }
        else if(appraisal_type.equals(VAR_LAND_IMP)){
            cbUnitinspection.setVisibility(View.GONE);
            cbUnitSamplingInspection.setVisibility(View.GONE);
            cbEngLandVal.setVisibility(View.GONE);
            cbEngBuildingVal.setVisibility(View.GONE);
        }
        else if(appraisal_type.equals(CONDO)){
            cbInsBuilding.setVisibility(View.GONE);
            cbInsSampling.setVisibility(View.GONE);
            cbValEstCost.setVisibility(View.GONE);
            cbValPhysObso.setVisibility(View.GONE);
        }
    }

    @Override
    public boolean onOptionsItemSelected(@NonNull MenuItem item) {
        if (item.getItemId() == android.R.id.home) {
            finish();
        }
        return super.onOptionsItemSelected(item);
    }

    @Override
    protected void onResume() {
        super.onResume();
        presenter.onStart(recordId);
    }

    @Override
    public void showProgress(String title, String message) {

    }

    @Override
    public void hideProgress() {

    }

    @Override
    public void showSnackMessage(String message) {

    }

    @OnClick(R2.id.btnAddAssumpt)
    void onClickUpdate() {
        try {
            Intent intent = new Intent(this, Class.forName(ActivityIntent.SPECIAL_ASSUMPTIONS_ACTIVITY));
            intent.putExtra(RECORD_ID, recordId);

            startActivity(intent);
        } catch (ClassNotFoundException e) {
            e.printStackTrace();
        }
    }

    @OnClick(R2.id.btnAddQual)
    void onClickUpdateAddQual() {
        try {
            Intent intent = new Intent(this, Class.forName(ActivityIntent.QUALIFYING_STATEMENTS_ACTIVITY));
            intent.putExtra(RECORD_ID, recordId);

            startActivity(intent);
        } catch (ClassNotFoundException e) {
            e.printStackTrace();
        }
    }


    @Override
    protected void onPause() {

        new Handler().post(() -> presenter.updateScopeWork(
                recordId,
                //Nature and Source of Information
                acBasicDesc.getText().toString(),
                CHECKBOX_TO_STRING(cbNatureBuySell),
                CHECKBOX_TO_STRING(cbNatureBuySellBroker),
                CHECKBOX_TO_STRING(cbNatureUnaffiliate),
                CHECKBOX_TO_STRING(cbNaturePub),
                CHECKBOX_TO_STRING(cbPropManage),
                CHECKBOX_TO_STRING(cbNatureContract),
                CHECKBOX_TO_STRING(cbNatureWeb),
                CHECKBOX_TO_STRING(cbNatureGov),
                CHECKBOX_TO_STRING(cbNatureHome),
                CHECKBOX_TO_STRING(cbNatureLGU),
                CHECKBOX_TO_STRING(cbNatureSimul),
                acNatureOther.getText().toString(),
                // Verification (w/o Cert)
                CHECKBOX_TO_STRING(cbVerifAssessor),
                CHECKBOX_TO_STRING(cbVerifSubdiv),
                CHECKBOX_TO_STRING(cbVerifZoning),
                //TODO - Inspection (Land Only)
                CHECKBOX_TO_STRING(cbInsOccular),
                CHECKBOX_TO_STRING(cbInsSite),
                //TODO - Inspection (Land Imp)
                CHECKBOX_TO_STRING(cbInsBuilding),
                CHECKBOX_TO_STRING(cbInsSampling),
                CHECKBOX_TO_STRING(cbInsIdBuilding),
                CHECKBOX_TO_STRING(cbInsIdUtilities),
                //TODO - Inspection (Condo)
                CHECKBOX_TO_STRING(cbUnitinspection),
                CHECKBOX_TO_STRING(cbUnitSamplingInspection),
                // Market Research
                CHECKBOX_TO_STRING(cbMarketNeighbor),
                CHECKBOX_TO_STRING(cbMarketInterview),
                CHECKBOX_TO_STRING(cbMarketMLS),
                CHECKBOX_TO_STRING(cbMarketSocio),
                // Valuation Analysis
                CHECKBOX_TO_STRING(cbValComparison),
                CHECKBOX_TO_STRING(cbValHighest),
                CHECKBOX_TO_STRING(cbValPropMarket),
                CHECKBOX_TO_STRING(cbValComparative),
                //TODO - Inspection (Land Imp)
                CHECKBOX_TO_STRING(cbValEstCost),
                CHECKBOX_TO_STRING(cbValPhysObso),
                //Eng. and other surveys
                CHECKBOX_TO_STRING(cbEngPropMear),
                CHECKBOX_TO_STRING(cbEngGeo),
                CHECKBOX_TO_STRING(cbEngSoil),
                CHECKBOX_TO_STRING(cbEngStruct),
                CHECKBOX_TO_STRING(cbEngQuantity),
                //TODO - Inspection (Condo)
                CHECKBOX_TO_STRING(cbUnitinspection),
                CHECKBOX_TO_STRING(cbUnitSamplingInspection),
                CHECKBOX_TO_STRING(cbEngRiskManage),
                CHECKBOX_TO_STRING(cbEngAssessor),
                CHECKBOX_TO_STRING(cbEngTraffic),
                CHECKBOX_TO_STRING(cbEngSurveyRight),
                //Verification w/ cert
                CHECKBOX_TO_STRING(cbEngDeedSale),
                CHECKBOX_TO_STRING(cbEngTaxDec),
                CHECKBOX_TO_STRING(cbEngTaxMap),
                CHECKBOX_TO_STRING(cbEngZoning),
                CHECKBOX_TO_STRING(cbEngZonal),
                //Assumption
                CHECKBOX_TO_STRING(cbAssFreeClear),
                CHECKBOX_TO_STRING(cbAssOwnership),
                CHECKBOX_TO_STRING(cbAssIndicate),
                CHECKBOX_TO_STRING(cbAssCurrently),
                CHECKBOX_TO_STRING(cbAssThird),
                CHECKBOX_TO_STRING(cbAssSpecial),
                //Qualifying Statement
                CHECKBOX_TO_STRING(cbQualifying),
                CHECKBOX_TO_STRING(cbQualLandArea),
                CHECKBOX_TO_STRING(cbQualImpHighest),
                CHECKBOX_TO_STRING(cbQualCoverage),
                CHECKBOX_TO_STRING(cbQualPremiseAss),
                CHECKBOX_TO_STRING(cbQualPremiseHigh),
                CHECKBOX_TO_STRING(cbQualAdj),
                acQualRemarks.getText().toString()
        ));

        super.onPause();

    }

    @Override
    public void showDetails(Record record) {
        //TODO BASIC DESCRIPTION
        acBasicDesc.setText(record.getValrepLandimpBasicDescription());
        acBasicDesc.setAdapter(GET_CONTENT_ADAPTER(record.getValrepLandimpBasicDescription(), R.array.basicDescriptionArray, this));
        acBasicDesc.setOnClickListener(view -> acBasicDesc.showDropDown());

        //Nature and Source of Information
        cbNatureBuySell.setChecked(getCheckboxBoolean(record.getBovSourceInfoBuyerSeller()));
        cbNatureBuySellBroker.setChecked(getCheckboxBoolean(record.getBovSourceInfoBuyerSellerbroker()));
        cbNatureUnaffiliate.setChecked(getCheckboxBoolean(record.getBovSourceInfoUnaffiliatedBroker()));
        cbNaturePub.setChecked(getCheckboxBoolean(record.getBovSourceInfoPublication()));
        cbPropManage.setChecked(getCheckboxBoolean(record.getBovSourceInfoPropertyManager()));
        cbNatureContract.setChecked(getCheckboxBoolean(record.getBovSourceInfoContractor()));
        cbNatureWeb.setChecked(getCheckboxBoolean(record.getBovSourceInfoPrivateWebsite()));
        cbNatureGov.setChecked(getCheckboxBoolean(record.getBovSourceInfoGovernment()));
        cbNatureHome.setChecked(getCheckboxBoolean(record.getBovSourceInfoHoaOfficer()));
        cbNatureLGU.setChecked(getCheckboxBoolean(record.getBovSourceInfoLguOfficial()));
        cbNatureSimul.setChecked(getCheckboxBoolean(record.getBovSourceInfoSimulation()));

        acNatureOther.setText(record.getBovSourceInfoOther());

       /* IN-SCOPE ACTIVITIES
        Verification (w/o Cert)*/
        cbVerifAssessor.setChecked(getCheckboxBoolean(record.getValrepLandimpIdLocationalVerification()));
        cbVerifSubdiv.setChecked(getCheckboxBoolean(record.getValrepLandimpIdLocationalVerificationSubd()));
        cbVerifZoning.setChecked(getCheckboxBoolean(record.getValrepLandimpIdZoningVerification()));

        /*TODO - Inspection (Land Only)*/
        cbInsOccular.setChecked(getCheckboxBoolean(record.getValrepLandimpIdOcularInspection()));
        cbInsSite.setChecked(getCheckboxBoolean(record.getValrepLandimpIdSiteAssessment()));

        //TODO Inspection Land w/ Improvements
        cbInsBuilding.setChecked(getCheckboxBoolean(record.getValrepLandimpIdBuildingInspection()));
        cbInsSampling.setChecked(getCheckboxBoolean(record.getValrepLandimpIdSamplingVerification()));
        cbInsIdBuilding.setChecked(getCheckboxBoolean(record.getValrepLandimpIdBuildingComponents()));
        cbInsIdUtilities.setChecked(getCheckboxBoolean(record.getValrepLandimpIdUtilitiesServices()));

        //TODO Inspection Condo
        cbUnitinspection.setChecked(getCheckboxBoolean(record.getValrepLandimpIdUnitInspection()));
        cbUnitSamplingInspection.setChecked(getCheckboxBoolean(record.getValrepLandimpIdUnitSamplingVerification()));

        /*Market Research*/
        cbMarketNeighbor.setChecked(getCheckboxBoolean(record.getValrepLandimpIdNeighborhoodResearch()));
        cbMarketInterview.setChecked(getCheckboxBoolean(record.getValrepLandimpIdLocalBrokers()));
        cbMarketMLS.setChecked(getCheckboxBoolean(record.getValrepLandimpIdMls()));
        cbMarketSocio.setChecked(getCheckboxBoolean(record.getValrepLandimpIdSocioPhysicalProfile()));

        /*Valuation Analysis*/
        cbValComparison.setChecked(getCheckboxBoolean(record.getValrepLandimpIdComparison()));
        cbValHighest.setChecked(getCheckboxBoolean(record.getValrepLandimpIdUseAnalysis()));
        cbValPropMarket.setChecked(getCheckboxBoolean(record.getValrepLandimpIdTypicalParticipants()));
        cbValComparative.setChecked(getCheckboxBoolean(record.getValrepLandimpIdMarketAnalysis()));

       //  TODO-Land w/ Improvements NO GETTER SETTER YET
        cbValEstCost.setChecked(getCheckboxBoolean(record.getValrepLandimpIdCostEstimate()));
        cbValPhysObso.setChecked(getCheckboxBoolean(record.getValrepLandimpDepreciatedAnalysis()));

        /*Eng. and other surveys*/
        cbEngPropMear.setChecked(getCheckboxBoolean(record.getValrepLandimpIdMeasurementVerification()));
        cbEngGeo.setChecked(getCheckboxBoolean(record.getValrepLandimpIdGeodeticSurvey()));
        cbEngSoil.setChecked(getCheckboxBoolean(record.getValrepLandimpIdSurveyHazardous()));
        cbEngStruct.setChecked(getCheckboxBoolean(record.getValrepLandimpIdEngineeringTests()));
        cbEngQuantity.setChecked(getCheckboxBoolean(record.getValrepLandimpIdQuantitySurvey()));
        cbEngRiskManage.setChecked(getCheckboxBoolean(record.getValrepLandimpIdNaturalHazards()));
        cbEngAssessor.setChecked(getCheckboxBoolean(record.getValrepLandimpIdValueResearch()));
        cbEngTraffic.setChecked(getCheckboxBoolean(record.getValrepLandimpIdTrafficCount()));
        cbEngSurveyRight.setChecked(getCheckboxBoolean(record.getValrepLandimpIdSpecialLaws()));

        //TODO ENG_OTHER_SURVEY Condo
        cbEngBuildingVal.setChecked(getCheckboxBoolean(record.getValrepLandimpIdBldgValuation()));
        cbEngLandVal.setChecked(getCheckboxBoolean(record.getValrepLandimpIdLandValuation()));

        /*Verification w/ cert*/
        cbEngDeedSale.setChecked(getCheckboxBoolean(record.getValrepLandimpIdDeedsSale()));
        cbEngTaxDec.setChecked(getCheckboxBoolean(record.getValrepLandimpIdTaxDeclaration()));
        cbEngTaxMap.setChecked(getCheckboxBoolean(record.getValrepLandimpIdTaxMap()));
        cbEngZoning.setChecked(getCheckboxBoolean(record.getValrepLandimpIdZoningCertification()));
        cbEngZonal.setChecked(getCheckboxBoolean(record.getValrepLandimpIdZonalValue()));

        /*Assumption*/
        cbAssFreeClear.setChecked(getCheckboxBoolean(record.getValrepLandimpAssumptions1()));
        cbAssOwnership.setChecked(getCheckboxBoolean(record.getValrepLandimpAssumptions2()));
        cbAssIndicate.setChecked(getCheckboxBoolean(record.getValrepLandimpAssumptions3()));
        cbAssCurrently.setChecked(getCheckboxBoolean(record.getValrepLandimpAssumptions4()));
        cbAssThird.setChecked(getCheckboxBoolean(record.getValrepLandimpAssumptions5()));
        cbAssSpecial.setChecked(getCheckboxBoolean(record.getValrepLandimpSpecialAssumptionsCheck()));
        //show hide assumption
        container_specialAssumptions.setVisibility(GET_IS_CHECKED_FROM_CHECKBOX_VISIBILITY(cbAssSpecial));
        ONCHANGE_IS_CHECKED_CHECKBOX_VISIBILITY(cbAssSpecial,container_specialAssumptions);

        /*Qualifying Statement*/
        cbQualifying.setChecked(getCheckboxBoolean(record.getValrepLcIvsQualifyingStatementsCheck()));

        //show hide qualifying
        container_qualifyingStatement.setVisibility(GET_IS_CHECKED_FROM_CHECKBOX_VISIBILITY(cbQualifying));
        ONCHANGE_IS_CHECKED_CHECKBOX_VISIBILITY(cbQualifying,container_qualifyingStatement);

        cbQualLandArea.setChecked(getCheckboxBoolean(record.getLvIvsQualifyingStatements1()));
        cbQualImpHighest.setChecked(getCheckboxBoolean(record.getLvIvsQualifyingStatements2()));
        cbQualCoverage.setChecked(getCheckboxBoolean(record.getLvIvsQualifyingStatements3()));
        cbQualPremiseAss.setChecked(getCheckboxBoolean(record.getLvIvsQualifyingStatements4()));
        cbQualPremiseHigh.setChecked(getCheckboxBoolean(record.getLvIvsQualifyingStatements5()));
        cbQualAdj.setChecked(getCheckboxBoolean(record.getLvIvsQualifyingStatements6()));

        acQualRemarks.setText(record.getLcIvsQualifyingStatementsDesc());
    }



    @Override
    public void showExitDialog(String title, String message) {

    }

    @Override
    public void showErrorDialog(String title, String message) {

    }
}
