package com.gdslinkasia.jobs.di;

import android.app.Application;

import com.gdslinkasia.jobs.contract.JobContract;
import com.gdslinkasia.jobs.contract.JobPresenter;

import dagger.Module;
import dagger.Provides;

@Module
public class JobModule {

    @Provides
    static JobContract.Presenter presenter(Application application) {
        return new JobPresenter(application);
    }

}
