package com.gdslinkasia.jobs.contract;

import android.app.Application;
import android.util.Log;

import androidx.annotation.NonNull;

import com.gdslinkasia.base.database.repository.JobsLocalRepository;
import com.gdslinkasia.base.database.repository.JobsRemoteRepository;
import com.gdslinkasia.base.model.job.JobRecordResult;
import com.gdslinkasia.base.model.job.Record;

import org.json.JSONException;

import java.util.List;

import io.reactivex.Single;

public class JobInteractor implements JobContract.Interactor {

    private JobsLocalRepository localRepository;
    private JobsRemoteRepository remoteRepository;

    JobInteractor(@NonNull Application application) {
//        super(application);
        localRepository = new JobsLocalRepository(application);
        remoteRepository = new JobsRemoteRepository(application);
    }

    @Override
    public Single<JobRecordResult> getAllLocalJobs(String APPRAISER_USERNAME, String JOB_STATUS) {
        return localRepository.getJobsAccordingToStatus(APPRAISER_USERNAME, JOB_STATUS)
                .onErrorResumeNext(throwable -> {
                    Log.d("int--LocalEmpty", "Logic Triggered");
                    return getRxRemoteJobs(APPRAISER_USERNAME, JOB_STATUS);
                })
                ;
//                .onErrorResumeNext(throwable -> {
//                    Log.d("int--LocalEmpty", "Logic Triggered");
//                    return getRxRemoteJobs(APPRAISER_USERNAME, JOB_STATUS);
//                });
    }

    @Override
    public Single<List<Record>> getSingleResultLocalJobs(String APPRAISER_USERNAME, String JOB_STATUS) {
        return localRepository.getJobsByStatus(APPRAISER_USERNAME, JOB_STATUS);
    }

    @Override
    public Single<JobRecordResult> getRxRemoteJobs(String APPRAISER_USERNAME, String JOB_STATUS) throws JSONException {
        return remoteRepository.getRxOnlineJobs(APPRAISER_USERNAME, JOB_STATUS);
    }
}
