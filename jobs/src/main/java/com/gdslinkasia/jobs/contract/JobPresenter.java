package com.gdslinkasia.jobs.contract;

import android.app.Application;
import android.os.Handler;
import android.util.Log;

import androidx.room.EmptyResultSetException;

import com.gdslinkasia.base.model.job.JobRecordResult;
import com.gdslinkasia.base.model.job.Record;

import org.json.JSONException;

import java.net.UnknownHostException;
import java.util.List;

import io.reactivex.Single;
import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.disposables.CompositeDisposable;
import io.reactivex.observers.DisposableSingleObserver;
import io.reactivex.schedulers.Schedulers;

import static com.gdslinkasia.base.utils.GlobalString.JOB_STATUS_SENTENCE_CHANGER;


public class JobPresenter implements JobContract.Presenter {

    private JobInteractor interactor;
    private JobContract.View view;
    private CompositeDisposable compositeDisposable = new CompositeDisposable();
    private String appraiserUsername, jobStatus;

    public JobPresenter(Application application) {
        interactor = new JobInteractor(application);
    }

    @Override
    public void onStart(String JOB_STATUS) {
        view.showEmptyState("Loading " + JOB_STATUS_SENTENCE_CHANGER(JOB_STATUS) + " Jobs. Please wait...");
    }

    @Override
    public void onResume(boolean isCalled, String APPRAISER_USERNAME, String JOB_STATUS) {
        appraiserUsername = APPRAISER_USERNAME;
        jobStatus = JOB_STATUS;

        if (!isCalled) {
            view.showProgress("Fetching " + JOB_STATUS_SENTENCE_CHANGER(JOB_STATUS) + " Jobs", "Please wait...");
            Log.d("int--onResume", "Logic Triggered");
            compositeDisposable.add(getSingleObservable(APPRAISER_USERNAME, JOB_STATUS).subscribeWith(getSingleRemoteObserver()));
//            try {
//                compositeDisposable.add(getRemoteObservable(APPRAISER_USERNAME, JOB_STATUS).subscribeWith(getSingleRemoteObserver()));
//            } catch (JSONException e) {
//                e.printStackTrace();
//                compositeDisposable.add(getSingleObservable(APPRAISER_USERNAME, JOB_STATUS).subscribeWith(getSingleRemoteObserver()));
//
//            }
        } else {
            Log.d("int--JobIsCalled", JOB_STATUS_SENTENCE_CHANGER(JOB_STATUS) + " already called");
            requestCacheJobs(APPRAISER_USERNAME, JOB_STATUS);
        }
    }

    @Override
    public void requestJobs(String APPRAISER_USERNAME, String JOB_STATUS) {
//        view.showProgress("Fetching " + JOB_STATUS_SENTENCE_CHANGER(JOB_STATUS) + " Jobs", "Please wait...");
        view.showRefresh(true);
        view.showEmptyState("Fetching " + JOB_STATUS_SENTENCE_CHANGER(JOB_STATUS) + " Jobs");
        try {
            Log.d("int--requestJobs", "Logic Triggered");
            compositeDisposable.add(getRemoteObservable(APPRAISER_USERNAME, JOB_STATUS)
                    .subscribeWith(getSingleRemoteObserver()));
        } catch (JSONException e) {
            e.printStackTrace();
        }
    }

    //TODO METHOD USED FOR FETCHING LOCAL DATA
    private void requestCacheJobs(String APPRAISER_USERNAME, String JOB_STATUS) {
        view.showRefresh(true);
        view.showEmptyState("Fetching " + JOB_STATUS_SENTENCE_CHANGER(JOB_STATUS) + " Jobs");
        compositeDisposable.add(getCacheObservable(APPRAISER_USERNAME, JOB_STATUS).subscribeWith(getLocalObserver()));
    }

    private Single<List<Record>> getCacheObservable(String APPRAISER_USERNAME, String JOB_STATUS) {
        return interactor.getSingleResultLocalJobs(APPRAISER_USERNAME, JOB_STATUS)
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread());
    }

    private Single<JobRecordResult> getSingleObservable(String APPRAISER_USERNAME, String JOB_STATUS) {
        return interactor.getAllLocalJobs(APPRAISER_USERNAME, JOB_STATUS)
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread());
    }

    private Single<JobRecordResult> getRemoteObservable(String APPRAISER_USERNAME, String JOB_STATUS) throws JSONException {
        return interactor.getRxRemoteJobs(APPRAISER_USERNAME, JOB_STATUS)
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread());
    }

    private DisposableSingleObserver<JobRecordResult> getSingleRemoteObserver() {
        return new DisposableSingleObserver<JobRecordResult>() {
            @Override
            public void onSuccess(JobRecordResult jobRecordResult) {
                List<Record> records = jobRecordResult.getRecords();
                if (!records.isEmpty()) {
//                    Log.d("int--RecordSize", String.valueOf(records.size()));
//                    view.showRecords(records);
                    Log.d("int--getSingleRemote", "calling getCacheObservable");
                    requestCacheJobs(appraiserUsername, jobStatus);
                } else {
                    view.showEmptyState("There are no records found.");
                    view.showSnackMessage("There are no records found.");
                }
                new Handler().postDelayed(() -> view.hideProgress(), 1000);
                view.showRefresh(false);
            }

            @Override
            public void onError(Throwable e) {
                e.printStackTrace();
                if (e instanceof UnknownHostException) {
                    new Handler().postDelayed(() -> requestCacheJobs(appraiserUsername, jobStatus), 1000);
                    view.showSnackMessage("Please connect to the internet.");
                } else {
                    view.showEmptyState(e.getMessage());
                }
                view.hideProgress();
                view.showRefresh(false);
            }
        };
    }

    private DisposableSingleObserver<List<Record>> getLocalObserver() {
        return new DisposableSingleObserver<List<Record>>() {
            @Override
            public void onSuccess(List<Record> records) {
                if (!records.isEmpty()) {
                    Log.d("int--RecordSize", String.valueOf(records.size()));
                    view.showRecords(records);
                } else {
                    view.showEmptyState("There are no records found.");
                    view.showSnackMessage("There are no records found.");
                }
//                new Handler().postDelayed(() -> view.hideProgress(), 1000);
                view.showRefresh(false);
            }

            @Override
            public void onError(Throwable e) {
                e.printStackTrace();
                if (e instanceof EmptyResultSetException) {
                    view.showEmptyState("There are no " + JOB_STATUS_SENTENCE_CHANGER(jobStatus) + " Jobs available.");
                } else {
                    view.showEmptyState(e.getMessage());
                }
//                new Handler().postDelayed(() -> view.hideProgress(), 1000);
                view.showRefresh(false);
            }
        };
    }

    @Override
    public void takeView(JobContract.View view) {
        this.view = view;
    }

    @Override
    public void dropView() {
        this.view = null;
        compositeDisposable.clear();
    }
}
