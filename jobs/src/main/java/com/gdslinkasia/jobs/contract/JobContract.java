package com.gdslinkasia.jobs.contract;

import com.gdslinkasia.base.BasePresenter;
import com.gdslinkasia.base.model.job.JobRecordResult;
import com.gdslinkasia.base.model.job.Record;

import org.json.JSONException;

import java.util.List;

import io.reactivex.Single;

public interface JobContract {

    interface View {
        void showProgress(String title, String message);
        void hideProgress();
        void showRefresh(boolean refresh);
        void showRecords(List<Record> records);
        void showEmptyState(String message);
        void showSnackMessage(String message);
    }

    interface Presenter extends BasePresenter<View> { //<V extends JobContract.View>

        void onStart(String JOB_STATUS);

        void onResume(boolean isCalled, String APPRAISER_USERNAME, String JOB_STATUS);

        void requestJobs(String APPRAISER_USERNAME, String JOB_STATUS);
    }

    interface Interactor {

        Single<JobRecordResult> getAllLocalJobs(String APPRAISER_USERNAME, String JOB_STATUS);

        Single<JobRecordResult> getRxRemoteJobs(String APPRAISER_USERNAME, String JOB_STATUS) throws JSONException;

        //TODO SINGLE FETCHING FOR RETURNING EMPTY RESULT IF RESULT IS NULL OR EMPTY
//        Single<List<Record>> getSingleResultLocalJobs(String APPRAISER_USERNAME, String JOB_STATUS);

        Single<List<Record>> getSingleResultLocalJobs(String APPRAISER_USERNAME, String JOB_STATUS);

    }
}