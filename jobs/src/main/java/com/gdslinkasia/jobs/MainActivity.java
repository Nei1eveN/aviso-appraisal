package com.gdslinkasia.jobs;

import android.annotation.SuppressLint;
import android.os.Bundle;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.appcompat.app.AlertDialog;
import androidx.appcompat.app.AppCompatActivity;
import androidx.core.content.ContextCompat;
import androidx.viewpager.widget.ViewPager;

import com.gdslinkasia.base.AppDistributeListener;
import com.gdslinkasia.base.adapter.ViewPagerAdapter;
import com.gdslinkasia.base.utils.ZoomPageTransformer;
import com.gdslinkasia.commonresources.R2;
import com.google.android.material.tabs.TabLayout;
import com.microsoft.appcenter.AppCenter;
import com.microsoft.appcenter.analytics.Analytics;
import com.microsoft.appcenter.crashes.Crashes;
import com.microsoft.appcenter.distribute.Distribute;

import java.util.Objects;

import butterknife.BindView;
import butterknife.ButterKnife;

import static android.util.Log.VERBOSE;

public class MainActivity extends AppCompatActivity {

    @BindView(R2.id.tabLayout)
    TabLayout tabLayout;
    @BindView(R2.id.viewPager)
    ViewPager viewPager;
    @BindView(R2.id.txt_subTitle)
    TextView txt_subTitle;

    ViewPagerAdapter pagerAdapter = new ViewPagerAdapter(getSupportFragmentManager());

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        ButterKnife.bind(this);

        Distribute.setListener(new AppDistributeListener());

        AppCenter.start(getApplication(), getResources().getString(R.string.app_center_key), Analytics.class, Crashes.class, Distribute.class);

        pagerAdapter.addFragment(new ActiveJobFragment(), "Active Jobs");
        pagerAdapter.addFragment(new ForAcceptanceJobFragment(), "Job Acceptance");
        pagerAdapter.addFragment(new ReworkJobFragment(), "Reworks");
        viewPager.setAdapter(pagerAdapter);
        viewPager.setCurrentItem(0);
        viewPager.setOffscreenPageLimit(3);
        viewPager.setPageTransformer(true, new ZoomPageTransformer());
        tabLayout.setupWithViewPager(viewPager);

        createTabIcons();
    }

    private void createTabIcons() {

        String[] array = {"Active Jobs", "Job Acceptance", "Rework"};

        @SuppressLint("InflateParams") View tabOne = getLayoutInflater().inflate(R.layout.custom_tab, null);
        tabOne.findViewById(R.id.icon).setBackgroundResource(R.drawable.ic_active);
        Objects.requireNonNull(tabLayout.getTabAt(0)).setCustomView(tabOne);

        @SuppressLint("InflateParams") View tabTwo = getLayoutInflater().inflate(R.layout.custom_tab, null);
        tabTwo.findViewById(R.id.icon).setBackgroundResource(R.drawable.ic_acceptance);
        Objects.requireNonNull(tabLayout.getTabAt(1)).setCustomView(tabTwo);

        @SuppressLint("InflateParams") View tabThree = getLayoutInflater().inflate(R.layout.custom_tab, null);
        tabThree.findViewById(R.id.icon).setBackgroundResource(R.drawable.ic_rework);
        Objects.requireNonNull(tabLayout.getTabAt(2)).setCustomView(tabThree);

        int positionTab = tabLayout.getSelectedTabPosition();

        tabLayout.addOnTabSelectedListener(new TabLayout.OnTabSelectedListener() {
            @Override
            public void onTabSelected(TabLayout.Tab tab) {
                int position = tab.getPosition();

                txt_subTitle.setText(array[position]);
                ImageView imgView = Objects.requireNonNull(Objects.requireNonNull(tabLayout.getTabAt(position)).getCustomView()).findViewById(R.id.icon);
                imgView.setColorFilter(ContextCompat.getColor(MainActivity.this, R.color.colorGoldPantone), android.graphics.PorterDuff.Mode.SRC_IN);
            }

            @Override
            public void onTabUnselected(TabLayout.Tab tab) {
                int position = tab.getPosition();
                ImageView imgView = Objects.requireNonNull(Objects.requireNonNull(tabLayout.getTabAt(position)).getCustomView()).findViewById(R.id.icon);
                imgView.setColorFilter(ContextCompat.getColor(MainActivity.this, R.color.colorPrimaryDark), android.graphics.PorterDuff.Mode.SRC_IN);
            }

            @Override
            public void onTabReselected(TabLayout.Tab tab) {

            }
        });

        txt_subTitle.setText(array[positionTab]);
    }

    @Override
    public void onBackPressed() {
        new AlertDialog.Builder(this).setCancelable(false).setTitle("Exit Application").setMessage("Do you want to exit this application?").setPositiveButton("EXIT", (dialogInterface, i) -> finishAffinity()).setNegativeButton("CANCEL", null).create().show();
//        super.onBackPressed();
    }
}
