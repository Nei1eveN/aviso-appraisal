package com.gdslinkasia.jobs;

import android.app.ProgressDialog;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.coordinatorlayout.widget.CoordinatorLayout;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import androidx.swiperefreshlayout.widget.SwipeRefreshLayout;

import com.gdslinkasia.base.SessionManager;
import com.gdslinkasia.base.adapter.ActiveJobAdapter;
import com.gdslinkasia.base.model.job.Record;
import com.gdslinkasia.commonresources.R2;
import com.gdslinkasia.jobs.contract.JobContract;
import com.google.android.material.snackbar.Snackbar;

import java.util.List;
import java.util.Objects;

import javax.inject.Inject;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.Unbinder;
import dagger.android.support.DaggerFragment;

import static com.gdslinkasia.base.utils.GlobalString.FOR_ACCEPTANCE;


public class ForAcceptanceJobFragment extends DaggerFragment implements JobContract.View, SwipeRefreshLayout.OnRefreshListener {

    private Unbinder unbinder;

    @BindView(R2.id.coordinatorLayout)
    CoordinatorLayout coordinatorLayout;
    @BindView(R2.id.swipeRefresh)
    SwipeRefreshLayout swipeRefreshLayout;
    @BindView(R2.id.recyclerView)
    RecyclerView recyclerView;
    @BindView(R2.id.tvList)
    TextView emptyText;
    @BindView(R2.id.ivList)
    ImageView emptyImage;

    private ProgressDialog progressDialog;

    @Inject
    JobContract.Presenter presenter;

    @Inject
    SessionManager sessionManager;

    private String email;

    private boolean isCalled = false;

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        email = sessionManager.getUserEmail();
        Log.d("int--Email", Objects.requireNonNull(email));

        presenter.takeView(this);
    }

    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container,
                             @Nullable Bundle savedInstanceState) {
        return inflater.inflate(R.layout.active_job_fragment, container, false);
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        unbinder = ButterKnife.bind(this, view);

        progressDialog = new ProgressDialog(getActivity());
        swipeRefreshLayout.setOnRefreshListener(this);
    }

    @Override
    public void onResume() {
        super.onResume();
        Log.d("int--forAccJobOnResume", "Logic Triggered");
        presenter.onResume(isCalled, email, FOR_ACCEPTANCE);
        isCalled = true;
    }

    @Override
    public void onStart() {
        super.onStart();
        Log.d("int--forAccJobOnStart", "Logic Triggered");
        presenter.onStart(FOR_ACCEPTANCE);
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        presenter.dropView();
        progressDialog.dismiss();
    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();
        unbinder.unbind();
    }

    @Override
    public void showProgress(String title, String message) {
        progressDialog.setCancelable(false);
        progressDialog.setIndeterminate(false);
        progressDialog.setTitle(title);
        progressDialog.setMessage(message);
        progressDialog.show();
    }

    @Override
    public void hideProgress() {
        progressDialog.hide();
    }

    @Override
    public void showRefresh(boolean refresh) {
        swipeRefreshLayout.setRefreshing(refresh);
    }

    @Override
    public void showRecords(List<Record> records) {
        emptyText.setVisibility(View.GONE);
        emptyImage.setVisibility(View.GONE);

        recyclerView.setVisibility(View.VISIBLE);
        recyclerView.setLayoutManager(new LinearLayoutManager(getActivity()));
        recyclerView.setHasFixedSize(true);

        ActiveJobAdapter adapter = new ActiveJobAdapter(Objects.requireNonNull(getActivity()));
        adapter.submitList(records);

        recyclerView.setAdapter(adapter);
    }

    @Override
    public void showEmptyState(String message) {
        recyclerView.setVisibility(View.GONE);

        emptyText.setVisibility(View.VISIBLE);
        emptyImage.setVisibility(View.VISIBLE);

        emptyText.setText(message);
    }

    @Override
    public void showSnackMessage(String message) {
        Snackbar.make(coordinatorLayout, message, Snackbar.LENGTH_SHORT).show();
    }

    @Override
    public void onRefresh() {
        presenter.requestJobs(email, FOR_ACCEPTANCE);
    }
}