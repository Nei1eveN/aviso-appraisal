package com.gdslinkasia.assumptions.di;

import android.app.Application;

import com.gdslinkasia.assumptions.contract.SpecialAssumptionsContract;
import com.gdslinkasia.assumptions.contract.SpecialAssumptionsPresenter;

import dagger.Module;
import dagger.Provides;

@Module
public class SpecialAssumptionsModule {

    @Provides
    static SpecialAssumptionsContract.Presenter presenter(Application application) {
        return new SpecialAssumptionsPresenter(application);
    }

}
