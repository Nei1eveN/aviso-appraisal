package com.gdslinkasia.assumptions.contract;

import android.app.Application;
import android.util.Log;
import android.view.View;

import com.gdslinkasia.base.model.details.ValrepLandimpAssumptionsRemark;
import com.gdslinkasia.base.model.job.Record;

import java.util.List;

import io.reactivex.Flowable;
import io.reactivex.Single;
import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.disposables.CompositeDisposable;
import io.reactivex.functions.Consumer;
import io.reactivex.observers.DisposableSingleObserver;
import io.reactivex.schedulers.Schedulers;

public class SpecialAssumptionsPresenter implements SpecialAssumptionsContract.Presenter {

    private SpecialAssumptionsContract.View view;
    private SpecialAssumptionsContract.Interactor interactor;
    private CompositeDisposable compositeDisposable = new CompositeDisposable();

    public SpecialAssumptionsPresenter(Application application) {
        this.interactor = new SpecialAssumptionsInteractorImpl(application);
    }

    @Override
    public void onStart() {
//        view.showProgress();
//        compositeDisposable.add(getListFlowable(recordId).subscribe(getFlowableObserver()));
        view.showEmptyState("Loading Assumptions");
    }

    @Override
    public void onResume(String recordId) {
        compositeDisposable.add(getSingleListObservable(recordId).subscribeWith(getSingleListObserver(recordId)));
    }

    @Override
    public void addNewLotDetail(String recordId, int recyclerViewVisibility, int tvNoRecordVisibility) {
        compositeDisposable.add(getRecordByRecordId(recordId).subscribeWith(addLotDetailObservable(recyclerViewVisibility, tvNoRecordVisibility)));
    }

    private Single<List<ValrepLandimpAssumptionsRemark>> getSingleListObservable(String recordId) {
        return interactor.getOwnershipPropertyDescList(recordId)
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                ;
    }

    private DisposableSingleObserver<List<ValrepLandimpAssumptionsRemark>> getSingleListObserver(String recordId) {
        return new DisposableSingleObserver<List<ValrepLandimpAssumptionsRemark>>() {
            @Override
            public void onSuccess(List<ValrepLandimpAssumptionsRemark> valrepLandimpLotDetails) {
                if (!valrepLandimpLotDetails.isEmpty()) {
//                    view.showList(valrepLandimpLotDetails);
                    compositeDisposable.add(getListFlowable(recordId).subscribe(getFlowableObserver()));
                } else {
                    view.showEmptyState("There are no records available.");
                }
//                new Handler().postDelayed(() -> view.hideProgress(), 1000);
            }

            @Override
            public void onError(Throwable e) {
                view.showEmptyState(e.getMessage());
//                new Handler().postDelayed(() -> view.hideProgress(), 1000);
            }
        };
    }

    private Flowable<List<ValrepLandimpAssumptionsRemark>> getListFlowable(String recordId) {
        return interactor.getListFlowable(recordId)
                .onBackpressureBuffer()
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                ;
    }

    private Consumer<List<ValrepLandimpAssumptionsRemark>> getFlowableObserver() {
        return valrepLandimpLotDetails -> {
            view.showList(valrepLandimpLotDetails);
            Log.d("int--flowableSize", String.valueOf(valrepLandimpLotDetails.size()));
        };
    }

    //TODO RECORD OBJECT FETCHING
    private Single<Record> getRecordByRecordId(String recordId) {
        return interactor.getRecordByRecordId(recordId)
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread());
    }

    private DisposableSingleObserver<Record> addLotDetailObservable(int recyclerViewVisibility, int tvNoRecordVisibility) {
        return new DisposableSingleObserver<Record>() {
            @Override
            public void onSuccess(Record record) {
                if (recyclerViewVisibility == View.VISIBLE && tvNoRecordVisibility == View.INVISIBLE) {
                    ValrepLandimpAssumptionsRemark detail = new ValrepLandimpAssumptionsRemark();
                    detail.setRecordId(record.getRecordId());
                    detail.setRecordUId(record.getUniqueId());

                    interactor.insertLotDetail(detail);
                } else {
                    ValrepLandimpAssumptionsRemark detail = new ValrepLandimpAssumptionsRemark();
                    detail.setRecordId(record.getRecordId());
                    detail.setRecordUId(record.getUniqueId());

                    interactor.insertLotDetail(detail);

                    onResume(record.getRecordId());
                }
            }

            @Override
            public void onError(Throwable e) {
                e.printStackTrace();
                view.showErrorDialog("Something Went Wrong", e.getMessage());
            }
        };
    }

    @Override
    public void takeView(SpecialAssumptionsContract.View view) {
        this.view = view;
    }

    @Override
    public void dropView() {
        view = null;
        compositeDisposable.clear();
        interactor.onDestroy();
    }
}
