package com.gdslinkasia.assumptions.contract;

import android.app.Application;

import com.gdslinkasia.base.database.repository.SpecialAssumptionsLocalRepository;
import com.gdslinkasia.base.model.details.ValrepLandimpAssumptionsRemark;
import com.gdslinkasia.base.model.job.Record;

import java.util.List;

import io.reactivex.Flowable;
import io.reactivex.Single;

public class SpecialAssumptionsInteractorImpl implements SpecialAssumptionsContract.Interactor {

    private SpecialAssumptionsLocalRepository localRepository;

    public SpecialAssumptionsInteractorImpl(Application application) {
        localRepository = new SpecialAssumptionsLocalRepository(application);
    }

    @Override
    public Single<List<ValrepLandimpAssumptionsRemark>> getOwnershipPropertyDescList(String recordId) {
        return localRepository.getOwnershipPropertyDescList(recordId);
    }

    @Override
    public Flowable<List<ValrepLandimpAssumptionsRemark>> getListFlowable(String recordId) {
        return localRepository.getListFlowable(recordId);
    }

    @Override
    public void insertLotDetail(ValrepLandimpAssumptionsRemark detail) {
        localRepository.insertLotDetail(detail);
    }

    @Override
    public Single<Record> getRecordByRecordId(String recordId) {
        return localRepository.getRecordByRecordId(recordId);
    }

    @Override
    public void onDestroy() {
        localRepository.onDestroy();
    }
}
