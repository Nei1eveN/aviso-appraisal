package com.gdslinkasia.assumptions.contract;

import android.app.Application;

import com.gdslinkasia.base.database.repository.SpecialAssumptionsLocalRepository;
import com.gdslinkasia.base.model.details.ValrepLandimpAssumptionsRemark;

import io.reactivex.Single;

public class SpecialAssumptionsItemInteractorImpl implements SpecialAssumptionsItemContract.Interactor {

    private SpecialAssumptionsLocalRepository localRepository;

    public SpecialAssumptionsItemInteractorImpl(Application application) {
        this.localRepository = new SpecialAssumptionsLocalRepository(application);
    }

    @Override
    public Single<ValrepLandimpAssumptionsRemark> getSpecialAssumptionsItemDetails(long uniqueId) {
        return localRepository.getPropertyDescDetail(uniqueId);
    }

    @Override
    public void updateLotDetail(ValrepLandimpAssumptionsRemark detail) {
        localRepository.updateLotDetail(detail);
    }

    @Override
    public void onDestroy() {
        localRepository.onDestroy();
    }

    @Override
    public void deleteItem(long uniqueId) {
        localRepository.deleteLotItem(uniqueId);
    }
}
