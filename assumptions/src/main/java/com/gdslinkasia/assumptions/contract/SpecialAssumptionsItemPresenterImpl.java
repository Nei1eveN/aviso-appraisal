package com.gdslinkasia.assumptions.contract;

import android.app.Application;
import android.os.Handler;
import android.util.Log;

import com.gdslinkasia.base.model.details.ValrepLandimpAssumptionsRemark;

import io.reactivex.Single;
import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.disposables.CompositeDisposable;
import io.reactivex.observers.DisposableSingleObserver;
import io.reactivex.schedulers.Schedulers;

public class SpecialAssumptionsItemPresenterImpl implements SpecialAssumptionsItemContract.Presenter {
    private SpecialAssumptionsItemContract.View view;
    private SpecialAssumptionsItemContract.Interactor interactor;
    private CompositeDisposable compositeDisposable = new CompositeDisposable();

    public SpecialAssumptionsItemPresenterImpl(SpecialAssumptionsItemContract.View view, Application application) {
        this.view = view;
        this.interactor = new SpecialAssumptionsItemInteractorImpl(application);
    }

    @Override
    public void onStart(long uniqueId) {
        view.showProgress("Loading Details", "Please wait...");
        compositeDisposable.add(getSingleObservable(uniqueId).subscribeWith(getSingleObserver()));
    }

    @Override
    public void onDestroy() {
        view = null;
        compositeDisposable.clear();
//        interactor.onDestroy();
    }

    @Override
    public void updateItemDetails(long uniqueId, String valrep_landimp_assumptions) {
        compositeDisposable.add(getSingleObservable(uniqueId).subscribeWith(getRecordUpdate(valrep_landimp_assumptions)));
    }

    @Override
    public void deleteItem(long uniqueId) {
        view.showProgress("Removing Item", "Please wait...");
        interactor.deleteItem(uniqueId);
        new Handler().postDelayed(() -> {
            view.hideProgress();
            view.showToastMessage("Item Successfully Removed.");
            view.finishActivity();
        }, 2500);
    }

    private Single<ValrepLandimpAssumptionsRemark> getSingleObservable(long uniqueId) {
        return interactor.getSpecialAssumptionsItemDetails(uniqueId)
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread());
    }

    private DisposableSingleObserver<ValrepLandimpAssumptionsRemark> getSingleObserver() {
        return new DisposableSingleObserver<ValrepLandimpAssumptionsRemark>() {
            @Override
            public void onSuccess(ValrepLandimpAssumptionsRemark detail) {
                view.showDetails(detail);
                new Handler().postDelayed(() -> view.hideProgress(), 2000);
            }

            @Override
            public void onError(Throwable e) {
                view.showExitDialog("Record Error", e.getMessage());
                new Handler().postDelayed(() -> view.hideProgress(), 2000);
            }
        };
    }

    private DisposableSingleObserver<ValrepLandimpAssumptionsRemark> getRecordUpdate(
            String valrep_landimp_assumptions) {
        return new DisposableSingleObserver<ValrepLandimpAssumptionsRemark>() {
            @Override
            public void onSuccess(ValrepLandimpAssumptionsRemark detail) {
                detail.setValrepLandimpAssumptions(valrep_landimp_assumptions);
                interactor.updateLotDetail(detail);

                Log.d("int--lotDetailSave", "Logic triggered");
            }

            @Override
            public void onError(Throwable e) {

            }
        };
    }
}
