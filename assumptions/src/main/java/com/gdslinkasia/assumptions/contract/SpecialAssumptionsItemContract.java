package com.gdslinkasia.assumptions.contract;

import com.gdslinkasia.base.model.details.ValrepLandimpAssumptionsRemark;

import io.reactivex.Single;

public interface SpecialAssumptionsItemContract {
    interface View {
        void showProgress(String title, String message);
        void hideProgress();
        void showDetails(ValrepLandimpAssumptionsRemark detail);
        void showExitDialog(String title, String message);

        void finishActivity();

        void showToastMessage(String message);
    }
    interface Presenter {
        void onStart(long uniqueId);
        void onDestroy();

        void updateItemDetails(long uniqueId, String valrep_landimp_assumptions);

        void deleteItem(long uniqueId);
    }
    interface Interactor {
        Single<ValrepLandimpAssumptionsRemark> getSpecialAssumptionsItemDetails(long uniqueId);

        void updateLotDetail(ValrepLandimpAssumptionsRemark detail);

        void onDestroy();

        void deleteItem(long uniqueId);
    }
}
