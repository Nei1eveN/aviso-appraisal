package com.gdslinkasia.assumptions.contract;
import com.gdslinkasia.base.BasePresenter;
import com.gdslinkasia.base.BaseView;
import com.gdslinkasia.base.model.details.ValrepLandimpAssumptionsRemark;
import com.gdslinkasia.base.model.job.Record;

import java.util.List;

import io.reactivex.Flowable;
import io.reactivex.Single;

public interface SpecialAssumptionsContract {

    interface View extends BaseView<Presenter> {
        void showProgress(String title, String message);

        void hideProgress();

        void showList(List<ValrepLandimpAssumptionsRemark> valrepLandimpLotDetails);

        void showEmptyState(String message);

        void showErrorDialog(String title, String message);
    }

    interface Presenter extends BasePresenter<View> {
        void onStart();

        void onResume(String recordId);

        void addNewLotDetail(String recordId, int recyclerViewVisibility, int tvNoRecordVisibility);
    }

    interface Interactor {
        Single<List<ValrepLandimpAssumptionsRemark>> getOwnershipPropertyDescList(String recordId);

        Flowable<List<ValrepLandimpAssumptionsRemark>> getListFlowable(String recordId);

        void insertLotDetail(ValrepLandimpAssumptionsRemark detail);

        Single<Record> getRecordByRecordId(String recordId);

        void onDestroy();
    }

}
