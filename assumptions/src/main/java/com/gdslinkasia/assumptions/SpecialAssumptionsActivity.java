package com.gdslinkasia.assumptions;

import android.app.ProgressDialog;
import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.MenuItem;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AlertDialog;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.gdslinkasia.assumptions.contract.SpecialAssumptionsContract;
import com.gdslinkasia.base.adapter.SpecialAssumptionsAdapter;
import com.gdslinkasia.base.adapter.click_listener.OwnershipPropDescClickListener;
import com.gdslinkasia.base.model.details.ValrepLandimpAssumptionsRemark;
import com.gdslinkasia.commonresources.R2;
import com.google.android.material.floatingactionbutton.FloatingActionButton;

import java.util.List;
import java.util.Objects;

import javax.inject.Inject;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import dagger.android.support.DaggerAppCompatActivity;

import static com.gdslinkasia.base.database.DatabaseUtils.RECORD_ID;
import static com.gdslinkasia.base.database.DatabaseUtils.UNIQUE_ID;

public class SpecialAssumptionsActivity extends DaggerAppCompatActivity implements SpecialAssumptionsContract.View, OwnershipPropDescClickListener {

    private String recordId;

    @BindView(R2.id.recyclerView) RecyclerView recyclerView;
    @BindView(R2.id.tvList) TextView emptyText;
    @BindView(R2.id.ivList) ImageView emptyImage;
    @BindView(R2.id.tvNoRecord) TextView tvNoRecord;
    @BindView(R2.id.fab) FloatingActionButton floatingActionButton;

    private ProgressDialog progressDialog;

    @Inject
    SpecialAssumptionsContract.Presenter presenter;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.content_list);
        ButterKnife.bind(this);

        Objects.requireNonNull(getSupportActionBar()).setTitle("List");
        getSupportActionBar().setSubtitle("Special Assumptions");
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);

        recordId = getIntent().getStringExtra(RECORD_ID);

        progressDialog = new ProgressDialog(this);
        presenter.takeView(this);
//        presenter = new SpecialAssumptionsPresenter(this, getApplication());
    }

    @Override
    public boolean onOptionsItemSelected(@NonNull MenuItem item) {
        if (item.getItemId() == android.R.id.home) {
            finish();
        }

        return super.onOptionsItemSelected(item);
    }

    @Override
    protected void onStart() {
        super.onStart();
        presenter.onStart();
    }

    @Override
    protected void onResume() {
        super.onResume();
        presenter.onResume(recordId);
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        presenter.dropView();
        progressDialog.dismiss();
    }

    @Override
    public void showProgress(String title, String message) {
//        swipeRefreshLayout.setRefreshing(true);
        progressDialog.setCancelable(false);
        progressDialog.setIndeterminate(false);
        progressDialog.setTitle(title);
        progressDialog.setMessage(message);
        progressDialog.show();
    }

    @Override
    public void hideProgress() {
//        swipeRefreshLayout.setRefreshing(false);
        progressDialog.hide();
    }

    @Override
    public void showList(List<ValrepLandimpAssumptionsRemark> valrepLandimpLotDetails) {
        emptyImage.setVisibility(View.INVISIBLE);
        emptyText.setVisibility(View.INVISIBLE);
        tvNoRecord.setVisibility(View.INVISIBLE);

        recyclerView.setVisibility(View.VISIBLE);
        recyclerView.setLayoutManager(new LinearLayoutManager(this));
        recyclerView.setHasFixedSize(true);

        SpecialAssumptionsAdapter adapter = new SpecialAssumptionsAdapter(this, this);
        adapter.submitList(valrepLandimpLotDetails);

        recyclerView.setAdapter(adapter);
    }

    @Override
    public void showEmptyState(String message) {
        recyclerView.setVisibility(View.INVISIBLE);

        tvNoRecord.setVisibility(View.VISIBLE);
        emptyText.setVisibility(View.INVISIBLE);
        emptyImage.setVisibility(View.VISIBLE);

        tvNoRecord.setText(message);
    }

    @Override
    public void showErrorDialog(String title, String message) {
        new AlertDialog.Builder(this).setCancelable(false).setTitle(title).setMessage(message).setPositiveButton("CLOSE", null).create().show();
    }

    @Override
    public void onPropDescItemClick(String recordId, long uniqueId, int position) {
        Log.d("int--uniqueId", String.valueOf(uniqueId));

        Intent intent = new Intent(this, SpecialAssumptionsItemActivity.class);
        intent.putExtra(UNIQUE_ID, uniqueId);
        intent.putExtra("item_position", position);

        startActivity(intent);
    }

    @OnClick(R2.id.fab)
    void onClickFloatingActionButton() {
        presenter.addNewLotDetail(recordId, recyclerView.getVisibility(), tvNoRecord.getVisibility());
    }


}
