package com.gdslinkasia.attachment;

import android.app.Application;
import android.content.Context;
import android.content.Intent;
import android.location.LocationManager;
import android.net.Uri;
import android.os.Environment;
import android.util.Log;
import android.view.View;

import com.androidnetworking.AndroidNetworking;
import com.androidnetworking.common.Priority;
import com.androidnetworking.error.ANError;
import com.androidnetworking.interfaces.DownloadListener;
import com.androidnetworking.interfaces.JSONObjectRequestListener;
import com.gdslinkasia.base.model.details.*;
import com.gdslinkasia.base.utils.GlobalString;

import org.json.JSONObject;

import java.io.File;
import java.util.List;
import java.util.Objects;

import io.reactivex.Single;
import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.disposables.CompositeDisposable;
import io.reactivex.observers.DisposableSingleObserver;
import io.reactivex.schedulers.Schedulers;

import static com.gdslinkasia.base.utils.GlobalString.*;
import static com.gdslinkasia.base.utils.GlobalString.ATTACHMENTS_DIRECTORY;

public class AttachmentsPresenter implements AttachmentsContract.Presenter {

    private AttachmentsContract.View view;
    private AttachmentsContract.Interactor interactor;
    private CompositeDisposable compositeDisposable = new CompositeDisposable();

    public AttachmentsPresenter(AttachmentsContract.View view, Application application) {
        this.view = view;
        this.interactor = new AttachmentsInteractor(application);
    }

    @Override
    public void onStart(String recordId, String attachmentCreatedFrom) {
//        view.showProgress("Loading Attachments", "Please wait...");
        view.showEmptyState("Loading Attachments\n\nPlease wait");
        if (attachmentCreatedFrom.equals("Local")) {
            view.showAddButton(View.VISIBLE);
        } else {
            view.showAddButton(View.GONE);
        }
    }

    @Override
    public void onResume(String recordId, String attachmentCreatedFrom) {
        compositeDisposable.add(interactor.getAppraisalRequestByRecordId(recordId).subscribeWith(new DisposableSingleObserver<AppraisalRequest>() {
            @Override
            public void onSuccess(AppraisalRequest appraisalRequest) {
                compositeDisposable.add(getSingleListObservable(appraisalRequest.getUniqueId(), attachmentCreatedFrom)
                        .subscribeWith(
                                getSingleListObserver(
//                                        appraisalRequest.getUniqueId(), attachmentCreatedFrom
                                        )
                        ));
            }

            @Override
            public void onError(Throwable e) {
                e.printStackTrace();
                view.showErrorDialog("Query Error", e.getMessage());
            }
        }));
    }

    @Override
    public void onDestroy() {
        view = null;
        compositeDisposable.clear();
        interactor.onDestroy();
    }


    private Single<List<AppAttachment>> getSingleListObservable(long appraisalRequestId, String createdFrom) {
        return interactor.getAttachmentList(appraisalRequestId, createdFrom)
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread());
    }

    private DisposableSingleObserver<List<AppAttachment>> getSingleListObserver(
//            long appraisalRequestId, String attachmentCreatedFrom
    ) {
        return new DisposableSingleObserver<List<AppAttachment>>() {
            @Override
            public void onSuccess(List<AppAttachment> valrepLandimpLotDetails) {
                if (!valrepLandimpLotDetails.isEmpty()) {
                    view.showList(valrepLandimpLotDetails);
//                    compositeDisposable.add(getListFlowable(appraisalRequestId, attachmentCreatedFrom).subscribe(getFlowableObserver()));
                } else {
                    view.showEmptyState("There are no records available.");
                }
            }

            @Override
            public void onError(Throwable e) {
                e.printStackTrace();
                view.showEmptyState(e.getMessage());
                view.hideProgress();
//                new Handler().postDelayed(() -> view.hideProgress(), 1000);
            }
        };
    }

//    private Flowable<List<AppAttachment>> getListFlowable(long appraisalRequestId, String attachmentCreatedFrom) {
//        return interactor.getListFlowable(appraisalRequestId, attachmentCreatedFrom)
//                .subscribeOn(Schedulers.computation())
//                .observeOn(AndroidSchedulers.mainThread());
//    }
//
//    private Consumer<List<AppAttachment>> getFlowableObserver() {
//        return valrepLandimpLotDetails -> {
//            view.showList(valrepLandimpLotDetails);
//            Log.d("int--flowableSize", String.valueOf(valrepLandimpLotDetails.size()));
//        };
//    }


    @Override
    public void addNewAttachment(String record_id, String createdFrom, String pdf_name, String appraisal_type, String attachment_type) {
        compositeDisposable.add(interactor.getAppraisalRequestByRecordId(record_id).subscribeWith(new DisposableSingleObserver<AppraisalRequest>() {
            @Override
            public void onSuccess(AppraisalRequest appraisalRequest) {
                compositeDisposable.add(
                        getSingleListObservable(appraisalRequest.getUniqueId(), createdFrom)
                                .subscribeWith(addAttachmentObservable(
                                        appraisalRequest.getUniqueId(),
                                        //record_id,
                                        createdFrom,
                                        pdf_name, appraisal_type, attachment_type)));

            }

            @Override
            public void onError(Throwable e) {
                e.printStackTrace();
                view.showErrorDialog("Query Error", e.getMessage());
            }
        }));
    }

    @Override
    public void checkFileFromLocalStorage(String attachmentFileName, long attachmentUId, String attachmentCreatedFrom, Context context) {
        if (attachmentFileName.isEmpty()) {
            view.showRemoveItemDialog("Empty File Name",
                    "File Does Not Exist. \n\nThis item will be removed from the list.",
                    "DELETE THIS ITEM",
                    attachmentUId);
        } else {
            final File file = new File(Environment.getExternalStorageDirectory() + "/" + ATTACHMENTS_DIRECTORY + "/" + attachmentFileName);

            File attachmentDirectory = new File(Environment.getExternalStorageDirectory() + "/" + ATTACHMENTS_DIRECTORY + "/");

            File imageDirectory = new File(Environment.getExternalStorageDirectory() + "/" + "gds_appraisal" + "/");

            if (file.exists()) {
                Intent target = new Intent(Intent.ACTION_VIEW);
                target.setDataAndType(Uri.fromFile(file), "application/pdf");
                target.setFlags(Intent.FLAG_ACTIVITY_NO_HISTORY);

                String imageFileName = attachmentFileName;
                String substring = imageFileName.substring(imageFileName.length() - 4);
                if (substring.equals(".pdf") || substring.equals(".PDF")) {
                    imageFileName = imageFileName.replace(
                            substring, ".jpg");//change last 4 chars to jpg
                }

//            String convertedImage = imageFileName.replaceAll(" ", "%20");

                view.openAttachment(target, "Open File", "Please Install Pdf Viewer", imageDirectory +"/"+imageFileName); //imageDirectory +"/"+ convertedImage
            } else {

                String file_name = attachmentFileName;
                String substring = file_name.substring(file_name.length() - 4);
                if (substring.equals(".pdf") || substring.equals(".PDF")) {
                    file_name = file_name.replace(
                            substring, ".jpg");//change last 4 chars to jpg
                }

                //TODO IMAGE DIRECTORY
                String convertedImageName = file_name.replaceAll(" ", "%20");
                File imageFile = new File(imageDirectory, file_name);

                Log.e("int--imageDirectory", String.valueOf(imageFile));

                //TODO ATTACHMENT DIRECTORY
//            String uploaded_attachment = fileName.replaceAll(" ", "%20");
                File uploadedAttachmentFile = new File(attachmentDirectory, attachmentFileName);

                String attachmentURL = GlobalString.attachment_dir + attachmentFileName;
                String imageURL = GlobalString.attachment_dir + file_name;

                if (attachmentCreatedFrom.equals("Local")) {
                    view.showRemoveItemDialog(
                            "ERROR 404",
                            "File Does Not Exist. \n\nThis item will be removed from the list.",
                            "DELETE THIS ITEM",
                            attachmentUId);
                } else if (attachmentCreatedFrom.equals("Server")) {


                    view.showProgress("Converting File", "Please wait...");
                    if (substring.equals(".pdf") || substring.equals(".PDF")) {
                        //ConvertPDFtoJPG(fileName, context);
                        Log.e("futo", file_name.replace(" ", "%20"));
                        Log.e("tyty", GlobalString.pdf_to_jpg_converter_url);

                        Log.e("attachmentDirectory", String.valueOf(uploadedAttachmentFile));

                        Log.e("URL", attachmentURL);

//                    Log.e("filename", uploaded_attachment);
                        Log.e("filename", file_name);


                        //TODO CONVERTING PDF TO URL
                        String finalFile_name = file_name;
                        AndroidNetworking.post(GlobalString.pdf_to_jpg_converter_url)
                                .addBodyParameter(TAG_FILE_NAME, attachmentFileName)
                                .addBodyParameter(TAG_ATTACHMENT, ATT_PHP_AUTH_KEY)
                                .setPriority(Priority.MEDIUM)
                                .build()
                                .getAsJSONObject(new JSONObjectRequestListener() {
                                    @Override
                                    public void onResponse(JSONObject response) {
                                        view.hideProgress();
                                        Log.e("ez", "Convert");

                                        //TODO DOWNLOADING IMAGE FILE
                                        downloadImage(imageURL, imageDirectory, finalFile_name, file,
                                                attachmentURL, attachmentDirectory, attachmentFileName, convertedImageName);
                                    }


                                    @Override
                                    public void onError(ANError error) {
                                        // handle error
                                        Log.e("int--conversionError", error.toString());
                                        error.printStackTrace();
                                        view.hideProgress();
                                        view.showErrorDialog("Something Went Wrong", error.getMessage());

                                    }
                                });
                    } else {
                        //TODO DOWNLOADING ATTACHMENT FILE
                        downloadAttachment(attachmentURL, attachmentDirectory, attachmentFileName, file, file_name, imageDirectory, convertedImageName);
                    }
                }
            }
        }
    }

    @Override
    public void deleteAttachmentItem(long uniqueId, String recordId, String attachmentCreatedFrom) {
        interactor.deleteAttachment(uniqueId);
        this.onResume(recordId, attachmentCreatedFrom);
    }

    @Override
    public void checkLocationStatus(String imageFileName, Context context) {
        final LocationManager manager = (LocationManager) context.getSystemService(Context.LOCATION_SERVICE);

        if (!Objects.requireNonNull(manager).isProviderEnabled(LocationManager.GPS_PROVIDER)) {
            view.requestPermissionDialog("Location Disabled", "Your GPS seems to be disabled, do you want to enable it?");
        } else {
            try {
                Intent openPlottingIntent = new Intent().setAction(Intent.ACTION_SEND);
                openPlottingIntent.putExtra(Intent.EXTRA_TEXT, imageFileName);
                Log.e("Open in GDS Plotting", imageFileName);

                openPlottingIntent.putExtra("logged_in", "true");
                openPlottingIntent.setType("text/plain");
                //intent.setPackage("com.gds.projects.lotplottingapp");
                openPlottingIntent.setPackage(context.getString(R.string.plotter_directory));

                view.showPlottingActivity(openPlottingIntent);
            } catch (Exception e) {
                e.printStackTrace();

                Intent activityIntent;
                try {
                    activityIntent = new Intent(Intent.ACTION_VIEW, Uri.parse("market://details?id=" + context.getString(R.string.plotter_directory)));
                } catch (android.content.ActivityNotFoundException ex) {
                    activityIntent = new Intent(Intent.ACTION_VIEW, Uri.parse("https://play.google.com/store/apps/details?id=" + context.getString(R.string.plotter_directory)));
                }

                view.showPlayStoreApplication(activityIntent);
            }
        }
    }

    private DisposableSingleObserver<List<AppAttachment>> addAttachmentObservable(
            long appraisalRequestId,
            //String record_id,
            String createdFrom,
            String pdf_name,
            String appraisal_type,
            String attachment_type) {
        return new DisposableSingleObserver<List<AppAttachment>>() {
            @Override
            public void onSuccess(List<AppAttachment> appAttachments) {
                AppAttachment detail = new AppAttachment();
                detail.setAppraisalRequestId(appraisalRequestId);
                detail.setAppAttachmentAppraisalType(appraisal_type);
                detail.setAppAttachmentFile(attachment_type);
                detail.setAppAttachmentFilename(pdf_name);
                detail.setCreatedFrom(createdFrom);

                interactor.insertAttachment(detail);
            }

            @Override
            public void onError(Throwable e) {
                e.printStackTrace();
                view.showErrorDialog("Something Went Wrong", e.getMessage());
            }
        };
    }

    //TODO DOWNLOAD CONVERTED IMAGE
    private void downloadImage(String imageURL, File imageDirectory, String convertedImageName, File file,
                               String attachmentURL, File attachmentDirectory, String uploaded_attachment, String convertedImageNameFormatted) {

        view.showProgress("Downloading Image File", "Please wait...");
        AndroidNetworking.download(imageURL, String.valueOf(imageDirectory), convertedImageName)
                .setTag("downloadImage")
                .setPriority(Priority.HIGH)
                .build()
                .setDownloadProgressListener((bytesDownloaded, totalBytes) -> Log.d("int--downloadProgress", bytesDownloaded + " / " + totalBytes))
                .startDownload(new DownloadListener() {
                    @Override
                    public void onDownloadComplete() {
                        view.hideProgress();

                        //TODO DOWNLOADING ATTACHMENT FILE
                        downloadAttachment(attachmentURL, attachmentDirectory, uploaded_attachment, file, convertedImageName, imageDirectory, convertedImageNameFormatted);
//                                                    view.showProgress("Downloading Attachment File", "Please wait...");
                    }

                    @Override
                    public void onError(ANError anError) {
                        Log.e("int--imageError", anError.toString());
                        anError.printStackTrace();
                        view.hideProgress();
                        view.showErrorDialog("Something Went Wrong", anError.getMessage());
                    }
                });
    }

    //TODO DOWNLOAD ATTACHMENT
    private void downloadAttachment(String URL, File directoryPath, String fileName, File file, String convertedImageName, File imageDirectory, String convertedImageFormatted) {
        //TODO DOWNLOADING ATTACHMENT FILE
        view.showProgress("Downloading Attachment File", "Please wait...");
        AndroidNetworking.download(URL, String.valueOf(directoryPath), fileName)
                .setTag("downloadTest")
                .setPriority(Priority.HIGH)
                .build()
                .setDownloadProgressListener((bytesDownloaded, totalBytes) -> Log.d("int--downloadProgress", bytesDownloaded + " / " + totalBytes))
                .startDownload(new DownloadListener() {
                    @Override
                    public void onDownloadComplete() {

                        view.hideProgress();

                        Intent target = new Intent(Intent.ACTION_VIEW);
                        target.setDataAndType(Uri.fromFile(file), "application/pdf");
                        target.setFlags(Intent.FLAG_ACTIVITY_NO_HISTORY);

                        view.openAttachment(target, "Open File", "Please Install Pdf Viewer", imageDirectory+"/"+convertedImageFormatted);
                    }

                    @Override
                    public void onError(ANError error) {
                        Log.e("int--attachmentError", error.getErrorBody());
                        error.printStackTrace();
                        view.hideProgress();

                        view.showErrorDialog("Error " + error.getErrorCode(), error.getErrorDetail());
                    }
                });
    }

}
