package com.gdslinkasia.attachment;

import android.app.Activity;
import android.app.ProgressDialog;
import android.content.ActivityNotFoundException;
import android.content.Intent;
import android.os.Bundle;
import android.os.StrictMode;
import android.util.Log;
import android.view.MenuItem;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.annotation.Nullable;
import androidx.appcompat.app.AlertDialog;
import androidx.appcompat.app.AppCompatActivity;
import androidx.coordinatorlayout.widget.CoordinatorLayout;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.gdslinkasia.base.adapter.AttachmentsAdapter;
import com.gdslinkasia.base.adapter.click_listener.AttachmentClickListener;
import com.gdslinkasia.base.model.details.AppAttachment;
import com.gdslinkasia.base.utils.GlobalString;
import com.gdslinkasia.commonresources.R2;
import com.google.android.material.floatingactionbutton.FloatingActionButton;
import com.google.android.material.snackbar.Snackbar;

import java.util.List;
import java.util.Objects;

import butterknife.BindView;
import butterknife.ButterKnife;

import static com.gdslinkasia.base.database.DatabaseUtils.*;
import static com.gdslinkasia.base.utils.GlobalString.SENTENCE_TO_APPRAISAL_TYPE_CHANGER;
import static com.gdslinkasia.base.utils.GlobalString.attachmentOpenOptions;

public class AttachmentsActivity extends AppCompatActivity
        implements AttachmentsContract.View,
        AttachmentClickListener,
        View.OnClickListener {

    Intent intent;
    String recordId, appraisalType, createdFrom;

    private int COLLAGE_REQUEST_OK = 777;

    @BindView(R2.id.coordinatorLayout) CoordinatorLayout coordinatorLayout;
//    @BindView(R.id.swipeRefresh) SwipeRefreshLayout swipeRefreshLayout;
    @BindView(R2.id.recyclerView) RecyclerView recyclerView;
    @BindView(R2.id.tvList) TextView emptyText;
    @BindView(R2.id.ivList) ImageView emptyImage;
    @BindView(R2.id.tvNoRecord) TextView tvNoRecord;

    @BindView(R2.id.fab) FloatingActionButton fab;

    AttachmentsContract.Presenter presenter;

    ProgressDialog progressDialog;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.content_list);
        ButterKnife.bind(this);

        intent = getIntent();
        recordId = intent.getStringExtra(RECORD_ID);
        appraisalType = intent.getStringExtra(APPRAISAL_TYPE);
        createdFrom = intent.getStringExtra("createdFrom");

        Objects.requireNonNull(getSupportActionBar()).setTitle("List of Attachments");
        getSupportActionBar().setSubtitle(createdFrom);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);

        fab.setOnClickListener(this);

        progressDialog = new ProgressDialog(this);

        presenter = new AttachmentsPresenter(this, getApplication());
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        if (item.getItemId() == android.R.id.home) {
            finish();
        }
        return super.onOptionsItemSelected(item);
    }

    @Override
    protected void onStart() {
        super.onStart();
        presenter.onStart(recordId, createdFrom);
    }

    @Override
    protected void onResume() {
        super.onResume();
        presenter.onResume(recordId, createdFrom);
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        presenter.onDestroy();
        progressDialog.dismiss();
    }

    @Override
    public void showProgress(String title, String message) {
//        swipeRefreshLayout.setRefreshing(true);
        progressDialog.setCancelable(false);
        progressDialog.setIndeterminate(false);
        progressDialog.setTitle(title);
        progressDialog.setMessage(message);
        progressDialog.show();
    }

    @Override
    public void hideProgress() {
//        swipeRefreshLayout.setRefreshing(false);
        progressDialog.hide();
    }

    @Override
    public void showAddButton(int visibility) {
        fab.setVisibility(visibility);
    }

    @Override
    public void showList(List<AppAttachment> appAttachment) {
        emptyImage.setVisibility(View.INVISIBLE);
//        emptyText.setVisibility(View.INVISIBLE);
        tvNoRecord.setVisibility(View.INVISIBLE);
//        fab.setVisibility(View.GONE);

        recyclerView.setVisibility(View.VISIBLE);
        recyclerView.setLayoutManager(new LinearLayoutManager(this));
//        recyclerView.setHasFixedSize(true);

        AttachmentsAdapter adapter = new AttachmentsAdapter(this, this);
        adapter.submitList(appAttachment);
        adapter.onAttachedToRecyclerView(recyclerView);

        recyclerView.setAdapter(adapter);
    }

    @Override
    public void showEmptyState(String message) {
        recyclerView.setVisibility(View.INVISIBLE);

        tvNoRecord.setVisibility(View.VISIBLE);
//        emptyText.setVisibility(View.GONE);
        emptyImage.setVisibility(View.VISIBLE);

        tvNoRecord.setText(message);
    }

    @Override
    public void showErrorDialog(String title, String message) {
        new AlertDialog.Builder(this).setCancelable(false).setTitle(title).setMessage(message).setPositiveButton("CLOSE", null).create().show();
    }

    @Override
    public void showRemoveItemDialog(String title, String message, String optionText, long attachmentUId) {
        new AlertDialog.Builder(this)
                .setCancelable(false)
                .setTitle(title)
                .setMessage(message)
                .setPositiveButton(optionText, (dialog, which) -> presenter.deleteAttachmentItem(attachmentUId, recordId, createdFrom))
                .create()
                .show();
    }

    @Override
    public void openAttachment(Intent target, String optionTitle, String activityNotFoundMessage, String imageFileName) {

        new AlertDialog.Builder(this)
                .setTitle("Actions")
                .setItems(attachmentOpenOptions, (dialog, which) -> {
                    switch (which) {
                        case 0:
                            Intent intent = Intent.createChooser(target, optionTitle);
                            try {
                                StrictMode.VmPolicy.Builder builder = new StrictMode.VmPolicy.Builder();
                                StrictMode.setVmPolicy(builder.build());
                                startActivity(intent);
                            } catch (ActivityNotFoundException e) {
                                Snackbar.make(coordinatorLayout, activityNotFoundMessage, Snackbar.LENGTH_LONG).show();
                            }
                            break;
                        case 1:
                            presenter.checkLocationStatus(imageFileName, this);
                            break;
                    }
                })
                .create().show();

    }

    @Override
    public void showPlottingActivity(Intent openPlottingIntent) {
        startActivityForResult(openPlottingIntent, 4);
    }

    @Override
    public void showPlayStoreApplication(Intent intent) {
        startActivity(intent);
    }

    @Override
    public void requestPermissionDialog(String title, String message) {
        new AlertDialog.Builder(this)
                .setCancelable(false)
                .setTitle(title)
                .setMessage(message)
                .setPositiveButton("ENABLE LOCATION", (dialog, which) -> startActivity(new Intent(android.provider.Settings.ACTION_LOCATION_SOURCE_SETTINGS)))
                .setNegativeButton("CLOSE", null)
                .create().show();
    }

    @Override
    public void onPropDescItemClick(String recordId, long uniqueId,  AppAttachment detail) {
        Log.d("int--uniqueId", String.valueOf(uniqueId));

//        downloadAttachments(detail);
        presenter.checkFileFromLocalStorage(detail.getAppAttachmentFilename(), uniqueId, createdFrom, this);
    }

    @Override
    public void onClick(View v) {
        if (v.getId() == R.id.fab) {
            Intent intent = new Intent(this, Collage_New.class);
            intent.putExtra(GlobalString.ID, recordId);
            intent.putExtra(GlobalString.APP_MAIN_ID, recordId);
            intent.putExtra(GlobalString.TYPE, appraisalType);
            startActivityForResult(intent, COLLAGE_REQUEST_OK);
        }
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, @Nullable Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (requestCode == COLLAGE_REQUEST_OK) {
            switch (resultCode) {
                case Activity.RESULT_OK:
                    String record_id = Objects.requireNonNull(data).getStringExtra("record_id");
                    String pdf_name = data.getStringExtra("pdf_name");
                    String appraisal_type = data.getStringExtra("appraisal_type");
                    String attachment_type = data.getStringExtra("attachment_type");

                    presenter.addNewAttachment(record_id, "Local", pdf_name, SENTENCE_TO_APPRAISAL_TYPE_CHANGER(appraisal_type), attachment_type);
                break;
                case Activity.RESULT_CANCELED:
                    //Write your code if there's no result
                    Log.e("int--ActivityResult", " FALSE ");
                    break;
            }
        }
    }


}
