package com.gdslinkasia.attachment;

import android.app.Application;

import com.gdslinkasia.base.database.repository.AttachmentsLocalRepository;
import com.gdslinkasia.base.model.details.AppAttachment;
import com.gdslinkasia.base.model.details.AppraisalRequest;

import java.util.List;

import io.reactivex.Flowable;
import io.reactivex.Single;

public class AttachmentsInteractor implements AttachmentsContract.Interactor {

    private AttachmentsLocalRepository localRepository;

    public AttachmentsInteractor(Application application) {
        localRepository = new AttachmentsLocalRepository(application);
    }

    @Override
    public Single<AppraisalRequest> getAppraisalRequestByRecordId(String recordId) {
        return localRepository.getAppraisalRequestByRecordId(recordId);
    }

    @Override
    public Single<List<AppAttachment>> getAttachmentList(long appraisalRequestId, String createdFrom) {
        return localRepository.getOwnershipPropertyDescList(appraisalRequestId, createdFrom);
    }


    @Override
    public Flowable<List<AppAttachment>> getListFlowable(long appraisalRequestId, String createdFrom) {
        return localRepository.getListFlowable(appraisalRequestId, createdFrom);
    }

    @Override
    public void insertAttachment(AppAttachment detail) {
        localRepository.insertAttachmentsDetail(detail);
    }

    @Override
    public void deleteAttachment(long attachmentUid) {
        localRepository.deleteAttachmentsItem(attachmentUid);
    }

    @Override
    public void onDestroy() {
        localRepository.onDestroy();
    }
}
