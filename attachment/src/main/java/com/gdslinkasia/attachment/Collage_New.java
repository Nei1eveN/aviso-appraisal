package com.gdslinkasia.attachment;

import android.Manifest;
import android.app.Activity;
import android.app.Dialog;
import android.app.ProgressDialog;
import android.content.ActivityNotFoundException;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.database.Cursor;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Build;
import android.os.Bundle;
import android.os.Environment;
import android.provider.MediaStore;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RadioButton;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AlertDialog;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;
import androidx.core.app.ActivityCompat;
import androidx.core.content.ContextCompat;
import androidx.core.content.FileProvider;

import com.bumptech.glide.Glide;
import com.gdslinkasia.base.utils.GlobalString;
import com.google.android.material.bottomnavigation.BottomNavigationView;
import com.google.android.material.snackbar.Snackbar;
import com.itextpdf.text.BadElementException;
import com.itextpdf.text.Document;
import com.itextpdf.text.DocumentException;
import com.itextpdf.text.Element;
import com.itextpdf.text.Image;
import com.itextpdf.text.PageSize;
import com.itextpdf.text.Paragraph;
import com.itextpdf.text.pdf.PdfPCell;
import com.itextpdf.text.pdf.PdfPTable;
import com.itextpdf.text.pdf.PdfWriter;

import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.net.MalformedURLException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.Objects;


public class Collage_New extends AppCompatActivity {

    //change according to usage
    public static String dir = "gds_appraisal/attachments";
    public static String dir_camera = "gds_appraisal/camera";

    String pdf_name;//record_id+_CI Photos.pdf

    String [] attachment_types = {
            "Property Pictures",
            "Plans",
            "Vicinity",
            "TCT",
            "Tax Declaration",
            "Floor Plans",
            "Vicinity Map"
    };
    String attachment_type = "Property Pictures";

    public String file_suffix = "_"+attachment_type;//pdf_name = record_id+file_suffix
    String record_id,app_main_id,appraisal_type;
    //    DatabaseHandler db;
//    Global gv = new Global();


    int counter = -1;
    int activeViewCounter = -1;
    int ivID = 0;
    boolean takePhotoOnly = false;
    Context context = this;

    int maxPage = 20;
    long maxFileSize = 5;    //size in MB

    private LinearLayout container;
    LayoutInflater layoutInflater;
    View addView;

    private ProgressDialog pDialog;

    ArrayList<Integer> imagesPerPage = new ArrayList<>();

    Toolbar collage_toolbar;


    private BottomNavigationView.OnNavigationItemSelectedListener mOnNavigationItemSelectedListener
            = new BottomNavigationView.OnNavigationItemSelectedListener() {

        @Override
        public boolean onNavigationItemSelected(@NonNull MenuItem item) {
            int i = item.getItemId();
            if (i == R.id.add_photo) {
                takePhotoOnly = true;
                if (!checkCamPermission()) {
                    requestCamPermission();
                } else {
                    dispatchTakePictureIntent();//start camera
                }
                return true;
            } else if (i == R.id.add_page) {
                if (counter < maxPage - 1) {
                    if (!checkFolderPermission()) {
                        requestFolderPermission();
                    } else {
                        create_folder();
                        create_folder_camera();
                    }

                    final Dialog dialogPortrait = new Dialog(Collage_New.this);
                    dialogPortrait.setContentView(R.layout.collage_add_page);
                    dialogPortrait.setTitle("Select Layout");

                    final Button btn_1_img = dialogPortrait.findViewById(R.id.btn_1_img);
                    final Button btn_2_img = dialogPortrait.findViewById(R.id.btn_2_img);
                    final Button btn_2_img_col = dialogPortrait.findViewById(R.id.btn_2_img_col);
                    final Button btn_4_img = dialogPortrait.findViewById(R.id.btn_4_img);

                    btn_1_img.setOnClickListener(view -> {
                        counter++;
                        imagesPerPage.add(1);
                        inflate_1();
                        dialogPortrait.dismiss();
                    });
                    btn_2_img.setOnClickListener(view -> {
                        counter++;
                        imagesPerPage.add(2);
                        inflate_2();
                        dialogPortrait.dismiss();
                    });

                    btn_2_img_col.setOnClickListener(view -> {
                        counter++;
                        imagesPerPage.add(3);
                        inflate_2_col();
                        dialogPortrait.dismiss();
                    });
                    btn_4_img.setOnClickListener(view -> {
                        counter++;
                        imagesPerPage.add(4);
                        inflate_4();
                        dialogPortrait.dismiss();
                    });

                    dialogPortrait.show();

                } else {

                    Toast.makeText(getApplicationContext(), "You have reached the maximum page limit of " + maxPage + " pages.",
                            Toast.LENGTH_LONG).show();

                }


                return true;
            } else if (i == R.id.save_pdf) {
                save_dialog();
                return true;
            }
            return false;
        }

    };

    void save_dialog(){

        if (container.getChildCount() > 0) {

            AlertDialog dialog = new AlertDialog.Builder(context)
                    .setTitle("Select attachment type:")
                    .setSingleChoiceItems(attachment_types, 0, (dialog1, which) -> {

                        attachment_type = attachment_types[which];

                        /*file_suffix = "_"+attachment_type+"_0.pdf";

                        pdf_name = record_id+file_suffix;

                        new CreatePDF().execute();
                        dialog.dismiss();*/
                    }).setPositiveButton("OK", (dialog12, id) -> {


                        file_suffix = "_"+attachment_type+"_"+"0"+".pdf";



                        pdf_name = app_main_id+file_suffix;

                        new CreatePDF().execute();

                    }).setNegativeButton("Cancel", (dialog13, id) -> {
                    }).create();

            dialog.show();

        } else {
//                        Snackbar.make(container,"Please add a page and attach a photo.",Snackbar.LENGTH_LONG).show();
            Toast.makeText(getApplicationContext(), "Please add a page and attach a photo.",
                    Toast.LENGTH_LONG).show();
        }
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.collage_new);



        /*setTitle("Create Attachment");
        getSupportActionBar().setSubtitle("Collage");
        getSupportActionBar().setIcon(R.drawable.collage_ic_add_page);
//      getSupportActionBar().setDisplayShowHomeEnabled(true);//for icon
//      getSupportActionBar().setDisplayShowTitleEnabled(true);
        getSupportActionBar().setHomeButtonEnabled(true);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);*/

        collage_toolbar = (Toolbar) findViewById(R.id.collage_toolbar);
        collage_toolbar.setTitle("Create Attachment");
        setSupportActionBar(collage_toolbar);

        Objects.requireNonNull(getSupportActionBar()).setDisplayHomeAsUpEnabled(true);
        Objects.requireNonNull(getSupportActionBar()).setDisplayShowHomeEnabled(true);

        BottomNavigationView navigation = findViewById(R.id.navigation);
        navigation.setOnNavigationItemSelectedListener(mOnNavigationItemSelectedListener);

        container = findViewById(R.id.container);
        Intent i = getIntent();
        Bundle b = i.getExtras();
        if (b != null) {
            record_id = (String) b.get(GlobalString.ID);
            app_main_id = (String) b.get(GlobalString.APP_MAIN_ID);
            appraisal_type = (String) b.get(GlobalString.TYPE);
        }
        pdf_name = app_main_id+file_suffix;

        if (!checkFolderPermission()) {
            requestFolderPermission();
        } else {
            create_folder();
            create_folder_camera();
        }

    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.collage_menu, menu);
        return true;
    }

    void inflate_1(){
        LayoutInflater layoutInflater = (LayoutInflater) getBaseContext().getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        final View addView = layoutInflater.inflate(R.layout.collage_inflater_1_image, null);
        addView.setEnabled(false);
        addView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                // TODO Auto-generated method stub
            }
        });

        final RadioButton rb_portrait = addView.findViewById(R.id.rb_portrait);
        final RadioButton rb_landscape = addView.findViewById(R.id.rb_landscape);

        final TextView tv_counter = addView.findViewById(R.id.tv_counter);
        tv_counter.setText(""+counter);

        final ImageView iv1 = addView.findViewById(R.id.iv1);
        iv1.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
            }
        });

        final Button btn1_camera = addView.findViewById(R.id.btn1_camera);
        btn1_camera.setOnClickListener(v -> {
            activeViewCounter = Integer.parseInt(tv_counter.getText().toString());
            ivID = iv1.getId();
            takePhotoOnly = false;
            if (!checkCamPermission()) {
                requestCamPermission();
            } else {
                dispatchTakePictureIntent();
            }
        });

        final Button btn1_album = addView.findViewById(R.id.btn1_album);
        btn1_album.setOnClickListener(v -> {
            activeViewCounter = Integer.parseInt(tv_counter.getText().toString());
            ivID = iv1.getId();
            openGallery();
        });

        final EditText et1 = addView.findViewById(R.id.et1);

        final Button btn_remove = addView.findViewById(R.id.btn_remove);
        btn_remove.setOnClickListener(v -> {
            imagesPerPage.remove(Integer.parseInt(tv_counter.getText().toString()));
            counter--;
            container.removeView(addView);
        });

        container.addView(addView);

    }

    void inflate_2(){
        LayoutInflater layoutInflater = (LayoutInflater) getBaseContext().getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        final View addView = layoutInflater.inflate(R.layout.collage_inflater_2_image, null);
        addView.setEnabled(false);
        addView.setOnClickListener(v -> {
            // TODO Auto-generated method stub
        });

        final RadioButton rb_portrait = addView.findViewById(R.id.rb_portrait);
        final RadioButton rb_landscape = addView.findViewById(R.id.rb_landscape);

        final TextView tv_counter = addView.findViewById(R.id.tv_counter);
        tv_counter.setText(""+counter);

        final ImageView iv1 = addView.findViewById(R.id.iv1);
        iv1.setOnClickListener(v -> {
        });

        final Button btn1_camera = addView.findViewById(R.id.btn1_camera);
        btn1_camera.setOnClickListener(v -> {
            activeViewCounter = Integer.parseInt(tv_counter.getText().toString());
            ivID = iv1.getId();
            takePhotoOnly = false;
            if (!checkCamPermission()) {
                requestCamPermission();
            } else {
                dispatchTakePictureIntent();
            }
        });

        final Button btn1_album = addView.findViewById(R.id.btn1_album);
        btn1_album.setOnClickListener(v -> {
            activeViewCounter = Integer.parseInt(tv_counter.getText().toString());
            ivID = iv1.getId();
            openGallery();
        });

        final ImageView iv2 = addView.findViewById(R.id.iv2);
        iv2.setOnClickListener(v -> {
        });

        final Button btn2_camera = addView.findViewById(R.id.btn2_camera);
        btn2_camera.setOnClickListener(v -> {
            activeViewCounter = Integer.parseInt(tv_counter.getText().toString());
            ivID = iv2.getId();
            takePhotoOnly = false;
            if (!checkCamPermission()) {
                requestCamPermission();
            } else {
                dispatchTakePictureIntent();
            }
        });

        final Button btn2_album = addView.findViewById(R.id.btn2_album);
        btn2_album.setOnClickListener(v -> {
            activeViewCounter = Integer.parseInt(tv_counter.getText().toString());
            ivID = iv2.getId();
            openGallery();
        });

        final EditText et1 = addView.findViewById(R.id.et1);
        final EditText et2 = addView.findViewById(R.id.et2);


        final Button btn_remove = addView.findViewById(R.id.btn_remove);
        btn_remove.setOnClickListener(v -> {
            imagesPerPage.remove(Integer.parseInt(tv_counter.getText().toString()));
            counter--;
            container.removeView(addView);
        });

        container.addView(addView);

    }

    void inflate_2_col(){
        LayoutInflater layoutInflater = (LayoutInflater) getBaseContext().getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        final View addView = layoutInflater.inflate(R.layout.collage_inflater_2_image_col, null);
        addView.setEnabled(false);
        addView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                // TODO Auto-generated method stub
            }
        });

        final RadioButton rb_portrait = addView.findViewById(R.id.rb_portrait);
        final RadioButton rb_landscape = addView.findViewById(R.id.rb_landscape);

        final TextView tv_counter = addView.findViewById(R.id.tv_counter);
        tv_counter.setText(""+counter);

        final ImageView iv1 = addView.findViewById(R.id.iv1);
        iv1.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
            }
        });

        final Button btn1_camera = addView.findViewById(R.id.btn1_camera);
        btn1_camera.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                activeViewCounter = Integer.parseInt(tv_counter.getText().toString());
                ivID = iv1.getId();
                takePhotoOnly = false;
                if (!checkCamPermission()) {
                    requestCamPermission();
                } else {
                    dispatchTakePictureIntent();
                }
            }
        });

        final Button btn1_album = addView.findViewById(R.id.btn1_album);
        btn1_album.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                activeViewCounter = Integer.parseInt(tv_counter.getText().toString());
                ivID = iv1.getId();
                openGallery();
            }
        });

        final ImageView iv2 = addView.findViewById(R.id.iv2);
        iv2.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
            }
        });

        final Button btn2_camera = addView.findViewById(R.id.btn2_camera);
        btn2_camera.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                activeViewCounter = Integer.parseInt(tv_counter.getText().toString());
                ivID = iv2.getId();
                takePhotoOnly = false;
                if (!checkCamPermission()) {
                    requestCamPermission();
                } else {
                    dispatchTakePictureIntent();
                }
            }
        });

        final Button btn2_album = addView.findViewById(R.id.btn2_album);
        btn2_album.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                activeViewCounter = Integer.parseInt(tv_counter.getText().toString());
                ivID = iv2.getId();
                openGallery();
            }
        });

        final ImageView iv3 = addView.findViewById(R.id.iv3);
        iv3.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
            }
        });

        final Button btn3_camera = addView.findViewById(R.id.btn3_camera);
        btn3_camera.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                activeViewCounter = Integer.parseInt(tv_counter.getText().toString());
                ivID = iv3.getId();
                takePhotoOnly = false;
                if (!checkCamPermission()) {
                    requestCamPermission();
                } else {
                    dispatchTakePictureIntent();
                }
            }
        });

        final Button btn3_album = addView.findViewById(R.id.btn3_album);
        btn3_album.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                activeViewCounter = Integer.parseInt(tv_counter.getText().toString());
                ivID = iv3.getId();
                openGallery();
            }
        });



        final EditText et1 = addView.findViewById(R.id.et1);
        final EditText et2 = addView.findViewById(R.id.et2);


        final Button btn_remove = addView.findViewById(R.id.btn_remove);
        btn_remove.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                imagesPerPage.remove(Integer.parseInt(tv_counter.getText().toString()));
                counter--;
                container.removeView(addView);
            }
        });

        container.addView(addView);
    }

    void inflate_4(){
        LayoutInflater layoutInflater = (LayoutInflater) getBaseContext().getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        final View addView = layoutInflater.inflate(R.layout.collage_inflater_4_image, null);
        addView.setEnabled(false);
        addView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                // TODO Auto-generated method stub
            }
        });

        final RadioButton rb_portrait = addView.findViewById(R.id.rb_portrait);
        final RadioButton rb_landscape = addView.findViewById(R.id.rb_landscape);

        final TextView tv_counter = addView.findViewById(R.id.tv_counter);
        tv_counter.setText(""+counter);

        final ImageView iv1 = addView.findViewById(R.id.iv1);
        iv1.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
            }
        });

        final Button btn1_camera = addView.findViewById(R.id.btn1_camera);
        btn1_camera.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                activeViewCounter = Integer.parseInt(tv_counter.getText().toString());
                ivID = iv1.getId();
                takePhotoOnly = false;
                if (!checkCamPermission()) {
                    requestCamPermission();
                } else {
                    dispatchTakePictureIntent();
                }
            }
        });

        final Button btn1_album = addView.findViewById(R.id.btn1_album);
        btn1_album.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                activeViewCounter = Integer.parseInt(tv_counter.getText().toString());
                ivID = iv1.getId();
                openGallery();
            }
        });

        final ImageView iv2 = addView.findViewById(R.id.iv2);
        iv2.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
            }
        });

        final Button btn2_camera = addView.findViewById(R.id.btn2_camera);
        btn2_camera.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                activeViewCounter = Integer.parseInt(tv_counter.getText().toString());
                ivID = iv2.getId();
                takePhotoOnly = false;
                if (!checkCamPermission()) {
                    requestCamPermission();
                } else {
                    dispatchTakePictureIntent();
                }
            }
        });

        final Button btn2_album = addView.findViewById(R.id.btn2_album);
        btn2_album.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                activeViewCounter = Integer.parseInt(tv_counter.getText().toString());
                ivID = iv2.getId();
                openGallery();
            }
        });

        final ImageView iv3 = addView.findViewById(R.id.iv3);
        iv3.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
            }
        });

        final Button btn3_camera = addView.findViewById(R.id.btn3_camera);
        btn3_camera.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                activeViewCounter = Integer.parseInt(tv_counter.getText().toString());
                ivID = iv3.getId();
                takePhotoOnly = false;
                if (!checkCamPermission()) {
                    requestCamPermission();
                } else {
                    dispatchTakePictureIntent();
                }
            }
        });

        final Button btn3_album = addView.findViewById(R.id.btn3_album);
        btn3_album.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                activeViewCounter = Integer.parseInt(tv_counter.getText().toString());
                ivID = iv3.getId();
                openGallery();
            }
        });

        final ImageView iv4 = addView.findViewById(R.id.iv4);
        iv4.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
            }
        });

        final Button btn4_camera = addView.findViewById(R.id.btn4_camera);
        btn4_camera.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                activeViewCounter = Integer.parseInt(tv_counter.getText().toString());
                ivID = iv4.getId();
                takePhotoOnly = false;
                if (!checkCamPermission()) {
                    requestCamPermission();
                } else {
                    dispatchTakePictureIntent();
                }
            }
        });

        final Button btn4_album = addView.findViewById(R.id.btn4_album);
        btn4_album.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                activeViewCounter = Integer.parseInt(tv_counter.getText().toString());
                ivID = iv4.getId();
                openGallery();
            }
        });

        final EditText et1 = addView.findViewById(R.id.et1);
        final EditText et2 = addView.findViewById(R.id.et2);
        final EditText et3 = addView.findViewById(R.id.et3);
        final EditText et4 = addView.findViewById(R.id.et4);


        final Button btn_remove = addView.findViewById(R.id.btn_remove);
        btn_remove.setOnClickListener(v -> {
            imagesPerPage.remove(Integer.parseInt(tv_counter.getText().toString()));
            counter--;
            container.removeView(addView);
        });

        container.addView(addView);

    }

    @Override
    public void onBackPressed() {
        // TODO Auto-generated method stub
        new AlertDialog.Builder(this)
                .setTitle("Exit Collage")
                .setMessage("Do you want to exit without generating PDF?")
                .setPositiveButton("Yes", new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int which) {
                        dialog.cancel();
                        Intent returnIntent = new Intent();
                        setResult(Activity.RESULT_CANCELED, returnIntent);
                        finish();
                    }
                })
                .setNegativeButton("Save Changes", new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int which) {
                        save_dialog();
                    }
                })
//                .setIcon(R.drawable.collage_question_mark)
                .show();
    }

    @Override
    public boolean onOptionsItemSelected(@NonNull MenuItem menuItem) {
        int i = menuItem.getItemId();
        if (i == android.R.id.home) {
            new AlertDialog.Builder(this)
//                        .setTitle("Exit Collage")
                    .setMessage("Do you want to exit without generating PDF?")
                    .setPositiveButton("Yes", new DialogInterface.OnClickListener() {
                        public void onClick(DialogInterface dialog, int which) {
                            dialog.cancel();
                            finish();
                        }
                    })
                    .setNegativeButton("Save Changes", new DialogInterface.OnClickListener() {
                        public void onClick(DialogInterface dialog, int which) {
                            save_dialog();
                        }
                    })
//                        .setIcon(R.drawable.collage_question_mark)
                    .show();
        } else if (i == R.id.action_help) {
            AlertDialog.Builder dialogBuilder = new AlertDialog.Builder(this);
            LayoutInflater inflater = this.getLayoutInflater();
            View dialogView = inflater.inflate(R.layout.collage_help, null);
            dialogBuilder.setView(dialogView);

//                AlertDialog alertDialog = dialogBuilder.create();
            AlertDialog alertDialog;
            alertDialog = dialogBuilder.setTitle("Guide")
                    .setPositiveButton("CLOSE", new DialogInterface.OnClickListener() {
                        @Override
                        public void onClick(DialogInterface dialog, int id) {
                        }
                    }).create();

            alertDialog.show();
        }
        return (super.onOptionsItemSelected(menuItem));
    }

    /**
     * CAMERA PERMISSIONS
     */

    private boolean checkCamPermission(){
        int result = ContextCompat.checkSelfPermission(context, Manifest.permission.CAMERA);
        if (result == PackageManager.PERMISSION_GRANTED){
            return true;
        } else {
            return false;
        }
    }

    private static final int PERMISSION_CAM_REQUEST_CODE = 1;//request code permisson of camera should not be the same as select photo
    private void requestCamPermission(){
//        startActivity(new Intent(android.provider.Settings.ACTION_APPLICATION_DETAILS_SETTINGS, Uri.fromParts("package", getPackageName(), null)));//direct to app settings permission
        if (ActivityCompat.shouldShowRequestPermissionRationale(this,Manifest.permission.CAMERA)){
            Snackbar.make(container,"Camera permission allows us to capture photos.", Snackbar.LENGTH_INDEFINITE).show();
            ActivityCompat.requestPermissions(this,new String[]{Manifest.permission.CAMERA},PERMISSION_CAM_REQUEST_CODE);//prompt
        } else {
            ActivityCompat.requestPermissions(this,new String[]{Manifest.permission.CAMERA},PERMISSION_CAM_REQUEST_CODE);
        }
    }

    private boolean checkPermission(){
        int result = ContextCompat.checkSelfPermission(context, Manifest.permission.READ_EXTERNAL_STORAGE);
        if (result == PackageManager.PERMISSION_GRANTED){
            return true;
        } else {
            return false;
        }
    }

    private static final int PERMISSION_REQUEST_CODE = 0;
    private void requestPermission(){
//        startActivity(new Intent(android.provider.Settings.ACTION_APPLICATION_DETAILS_SETTINGS, Uri.fromParts("package", getPackageName(), null)));//direct to app settings permission
        if (ActivityCompat.shouldShowRequestPermissionRationale(this,Manifest.permission.READ_EXTERNAL_STORAGE)){
            Snackbar.make(container,"Read external storage permission allows us to create attachments.", Snackbar.LENGTH_INDEFINITE).show();
            ActivityCompat.requestPermissions(this,new String[]{Manifest.permission.READ_EXTERNAL_STORAGE},PERMISSION_REQUEST_CODE);//prompt
        } else {
            ActivityCompat.requestPermissions(this,new String[]{Manifest.permission.READ_EXTERNAL_STORAGE},PERMISSION_REQUEST_CODE);
        }
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, String permissions[], int[] grantResults) {
        switch (requestCode) {
            case PERMISSION_REQUEST_CODE:
                if (grantResults.length > 0 && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
//                    startActivity(new Intent(this, Property_Pictures.class));
                    openGallery();
                } else {
                    Snackbar.make(container,"You should allow Read external storage permission for creating attachments.", Snackbar.LENGTH_LONG).show();
                }
                break;
            case PERMISSION_CAM_REQUEST_CODE:
                if (grantResults.length > 0 && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
//                    startActivity(new Intent(this, Property_Pictures.class));
                    dispatchTakePictureIntent();//open camera
                } else {
                    Snackbar.make(container,"You should allow Read external storage permission for creating attachments.", Snackbar.LENGTH_LONG).show();
                }
                break;

            //folder
            case PERMISSION_WS_REQUEST_CODE:
                if (grantResults.length > 0 && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                    create_folder();
                    create_folder_camera();
                } else {
                    Snackbar snackbar = Snackbar
                            .make(getWindow().getDecorView().getRootView(), "Allow all permissions to use this app efficiently.", Snackbar.LENGTH_INDEFINITE)
                            .setActionTextColor(getResources().getColor(R.color.colorPrimary))
                            .setAction("Settings", new View.OnClickListener() {
                                @Override
                                public void onClick(View view) {
                                    startActivity(new Intent(android.provider.Settings.ACTION_APPLICATION_DETAILS_SETTINGS, Uri.fromParts("package", getPackageName(), null)));//direct to app settings permission
                                }
                            });
                    snackbar.show();
                }
                break;


        }
    }

    static final int REQUEST_TAKE_PHOTO = 0;//request code of camera and select photo should not be the same

    private void dispatchTakePictureIntent() {
        Intent takePictureIntent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
        // Ensure that there's a camera activity to handle the intent
        if (takePictureIntent.resolveActivity(getPackageManager()) != null) {
            // Create the File where the photo should go
            File photoFile = null;
            try {
                photoFile = createImageFile();
            } catch (IOException ex) {
                // Error occurred while creating the File
                //...
                Toast.makeText(getApplicationContext(), "Error: "+ex,
                        Toast.LENGTH_LONG).show();
            }
            // Continue only if the File was successfully created
            if (photoFile != null) {
               /* takePictureIntent.putExtra(MediaStore.EXTRA_OUTPUT,
                        Uri.fromFile(photoFile));
                startActivityForResult(takePictureIntent, REQUEST_TAKE_PHOTO);*/
               //Nov 26 2018 Ian Lozares
                int versionBuild= 19;
                versionBuild = Build.VERSION.SDK_INT;
                if(versionBuild<=20){
                    takePictureIntent.putExtra(MediaStore.EXTRA_OUTPUT,
                            Uri.fromFile(photoFile));
                    startActivityForResult(takePictureIntent, REQUEST_TAKE_PHOTO);
                }else{
                    String authorities = getApplicationContext().getPackageName()+".fileprovider";
                    Uri imageUri = FileProvider.getUriForFile(this,authorities,photoFile);
                    takePictureIntent.putExtra(MediaStore.EXTRA_OUTPUT,imageUri);
                    //takePictureIntent.putExtra(MediaStore.EXTRA_OUTPUT, Uri.fromFile(photoFile));
                    takePictureIntent.addFlags(Intent.FLAG_GRANT_WRITE_URI_PERMISSION);
                    takePictureIntent.addFlags(Intent.FLAG_GRANT_READ_URI_PERMISSION);
                    startActivityForResult(takePictureIntent, REQUEST_TAKE_PHOTO);
                }
                //end
            } else {
//                Toast.makeText(getApplicationContext(), "Null", Toast.LENGTH_LONG).show();
                Toast.makeText(getApplicationContext(), "Click again the camera button", Toast.LENGTH_LONG).show();
            }

        }
    }

    /**
     * FOLDER PERMISSIONS
     */

    private boolean checkFolderPermission(){
        int result = ContextCompat.checkSelfPermission(context, Manifest.permission.WRITE_EXTERNAL_STORAGE);
        if (result == PackageManager.PERMISSION_GRANTED){
            return true;
        } else {
            return false;
        }
    }

    private static final int PERMISSION_WS_REQUEST_CODE = 2;//request code permisson of camera should not be the same as select photo
    private void requestFolderPermission(){
//        startActivity(new Intent(android.provider.Settings.ACTION_APPLICATION_DETAILS_SETTINGS, Uri.fromParts("package", getPackageName(), null)));//direct to app settings permission
        if (ActivityCompat.shouldShowRequestPermissionRationale(this,Manifest.permission.WRITE_EXTERNAL_STORAGE)){
//            Snackbar.make(container,"Camera permission allows us to capture photos.",Snackbar.LENGTH_INDEFINITE).show();
            ActivityCompat.requestPermissions(this,new String[]{Manifest.permission.WRITE_EXTERNAL_STORAGE}, PERMISSION_WS_REQUEST_CODE);//prompt
        } else {
            ActivityCompat.requestPermissions(this,new String[]{Manifest.permission.WRITE_EXTERNAL_STORAGE}, PERMISSION_WS_REQUEST_CODE);
        }
    }


    public void create_folder() {
        // create ATTACHMENTS_DIRECTORY
        File myDirectory = new File(Environment.getExternalStorageDirectory()
                + "/" + dir + "/");
        if (!myDirectory.exists()) {
            myDirectory.mkdirs();
        }

        File noMedia = new File(Environment.getExternalStorageDirectory()
                + "/" + dir + "/.nomedia");
        if (!noMedia.exists()) {
            noMedia.mkdirs();
            Toast.makeText(getApplicationContext(), "Attachment folder created",
                    Toast.LENGTH_LONG).show();
        }
    }

    public void create_folder_camera() {
        // create ATTACHMENTS_DIRECTORY
        File myDirectory = new File(Environment.getExternalStorageDirectory()
                + "/" + dir_camera + "/");
        if (!myDirectory.exists()) {
            myDirectory.mkdirs();
            Toast.makeText(getApplicationContext(), "Photo folder created",
                    Toast.LENGTH_LONG).show();
        }
    }


    String mCurrentPhotoPath;
    private File createImageFile() throws IOException {
        // Create an image file name
        String timeStamp = new SimpleDateFormat("yyyyMMdd_HHmmss").format(new Date());
        String imageFileName = "GDS_" + timeStamp;
//        File storageDir = new File(Environment.getExternalStorageDirectory() + getString(R.string.directory, Property_Pictures.this));
        File storageDir = new File(Environment.getExternalStorageDirectory() +"/"+ dir_camera);
        File image = File.createTempFile(
                imageFileName,  /* prefix */
                ".jpg",         /* suffix */
                storageDir      /* directory */
        );

        // Save a file: path for use with ACTION_VIEW intents
//        mCurrentPhotoPath = "file:" + image.getAbsolutePath();
        mCurrentPhotoPath = image.toString();
        return image;

    }

    private void galleryAddPic() {
        File f = new File(mCurrentPhotoPath);
        Uri contentUri = Uri.fromFile(f);
        Intent mediaScanIntent = new Intent(Intent.ACTION_MEDIA_SCANNER_SCAN_FILE, contentUri);
        sendBroadcast(mediaScanIntent);
    }

    private void galleryAddPicToImageView() {
        File f = new File(mCurrentPhotoPath);
        Uri contentUri = Uri.fromFile(f);
        Intent mediaScanIntent = new Intent(Intent.ACTION_MEDIA_SCANNER_SCAN_FILE, contentUri);
        sendBroadcast(mediaScanIntent);

        //autoset of image in imgview
        File imgFile = new  File(mCurrentPhotoPath);
        if(imgFile.exists()){

            Bitmap myBitmap = BitmapFactory.decodeFile(imgFile.getAbsolutePath());
//            ImageView iv_image = (ImageView) findViewById(ivID);
            ImageView iv_image = container.getChildAt( activeViewCounter ).findViewById( ivID );
//            iv_image.setScaleType(ImageView.ScaleType.FIT_XY);
//            iv_image.setImageBitmap(myBitmap);

            Glide.with(context).load(imgFile.getAbsolutePath()).into(
//                (ImageView) container.getChildAt(0).findViewById(R.id.iv0)
//                    (ImageView) container.getChildAt( activeViewCounter ).findViewById( ivID )
                    iv_image
            );
        }

        /*selectedImagePath = mCurrentPhotoPath;
//                Glide.with(context).load(selectedImagePath).into(selectedImageVIew);
        setImageToView();*/
    }

    // this is the action code we use in our intent,
    // this way we know we're looking at the response from our own action
    private static final int SELECT_PICTURE = 1;
    private String selectedImagePath;
    private static final int CAMERA_REQUEST = 1888;

    /*
    SELECTING AND CROPPING
     */
    private final int GALLERY_ACTIVITY_CODE = 200;
    private final int RESULT_CROP = 400;
    String picturePath;

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);

        if (requestCode == SELECT_PICTURE && resultCode == RESULT_OK) {
            Uri selectedImageUri = data.getData();
            selectedImagePath = getPath(selectedImageUri);
//                Glide.with(context).load(selectedImagePath).into(selectedImageVIew);
            setImageToView();
        }

        if (requestCode == GALLERY_ACTIVITY_CODE) {
            if(resultCode == Activity.RESULT_OK){
                picturePath = data.getStringExtra("picturePath");
                //perform Crop on the Image Selected from Gallery
                performCrop(picturePath);
            }
        }

        if (requestCode == RESULT_CROP ) {
            if(resultCode == Activity.RESULT_OK){
                Bundle extras = data.getExtras();
                Bitmap selectedBitmap = extras.getParcelable("data");
                // Set The Bitmap Data To ImageView
                /*iv_image.setImageBitmap(selectedBitmap);
                iv_image.setScaleType(ImageView.ScaleType.FIT_XY);*/


                ImageView iv_image = container.getChildAt( activeViewCounter ).findViewById( ivID );
//                iv_image.setScaleType(ImageView.ScaleType.FIT_XY);

                picturePath = data.getData().getPath();
                Uri selectedImage = data.getData();

                Glide.with(context).load(picturePath).into(
//                (ImageView) container.getChildAt(0).findViewById(R.id.iv0)
//                        (ImageView) container.getChildAt( activeViewCounter ).findViewById( ivID )
                        iv_image
                );


            }
        }

        if (requestCode == REQUEST_TAKE_PHOTO && resultCode == RESULT_OK) {
            if (takePhotoOnly) {
                galleryAddPic();
            } else {
                galleryAddPicToImageView();
            }
        }
    }

    /**
     * helper to retrieve the path of an image URI
     */
    public String getPath(Uri uri) {
        // just some safety built in
        if( uri == null ) {
            // TODO perform some logging or show user feedback
            return null;
        }
        // try to retrieve the image from the media store first
        // this will only work for images selected from gallery
        String[] projection = { MediaStore.Images.Media.DATA };
        Cursor cursor = managedQuery(uri, projection, null, null, null);
        if( cursor != null ){
            int column_index = cursor.getColumnIndexOrThrow(MediaStore.Images.Media.DATA);
            cursor.moveToFirst();
            return cursor.getString(column_index);
        }
        // this is our fallback here
        return uri.getPath();
    }

    private void performCrop(String picUri) {
        try {
            //Start Crop Activity

            Intent cropIntent = new Intent("com.android.camera.action.CROP");
            // indicate image type and Uri
            File f = new File(picUri);
            Uri contentUri = Uri.fromFile(f);

            cropIntent.setDataAndType(contentUri, "image/*");
            // set crop properties
            cropIntent.putExtra("crop", "true");
            // indicate aspect of desired crop
            cropIntent.putExtra("aspectX", 1);
            cropIntent.putExtra("aspectY", 1);
            // indicate output X and Y
            cropIntent.putExtra("outputX", 280);
            cropIntent.putExtra("outputY", 280);

            // retrieve data on return
            cropIntent.putExtra("return-data", true);
            // start the activity - we handle returning in onActivityResult
            startActivityForResult(cropIntent, RESULT_CROP);
        }
        // respond to users whose devices do not support the crop action
        catch (ActivityNotFoundException anfe) {
            // display an error message
            String errorMessage = "your device doesn't support the crop action!";
            Toast toast = Toast.makeText(this, errorMessage, Toast.LENGTH_SHORT);
            toast.show();
        }
    }


    void openGallery(){
        if (!checkPermission()) {
            requestPermission();
        } else {
            // in onCreate or any event where your want the user to
            // select a file
            Intent intent = new Intent();
            intent.setType("image/*");
//                    intent.setAction(Intent.ACTION_GET_CONTENT);
            intent.setAction(Intent.ACTION_PICK);//direct to gallery instead of folders with recent
            startActivityForResult(Intent.createChooser(intent, "Select Picture"), SELECT_PICTURE);
        }
    }

    void setImageToView(){
        ImageView iv_image = container.getChildAt( activeViewCounter ).findViewById( ivID );
        iv_image.setScaleType(ImageView.ScaleType.FIT_XY);

        Glide.with(context).load(selectedImagePath).into(
//                (ImageView) container.getChildAt(0).findViewById(R.id.iv0)
//                    (ImageView) container.getChildAt( activeViewCounter ).findViewById( ivID )
                iv_image
        );

    }



    private class CreatePDF extends AsyncTask<String, Void, String> {

        @Override
        protected String doInBackground(String... params) {

            create_pdf();

            return "Executed";
        }

        @Override
        protected void onPostExecute(String result) {
            pDialog.dismiss();

            //check file size if not corrupted or not 0MB
            File file = new File(Environment.getExternalStorageDirectory() + "/" + dir, pdf_name);

            // Get length of file in bytes
            long fileSizeInBytes = file.length();
            // Convert the bytes to Kilobytes (1 KB = 1024 Bytes)
            long fileSizeInKB = fileSizeInBytes / 1024;
            // Convert the KB to MegaBytes (1 MB = 1024 KBytes)
            long fileSizeInMB = fileSizeInKB / 1024;


            int intFileSizeInBytes = (int) fileSizeInBytes;

            Toast.makeText(context, fileSizeInMB+"MB file size created",
                    Toast.LENGTH_LONG).show();


            if (file != null || fileSizeInKB > 0) {

                if (fileSizeInMB >= maxFileSize){

                    AlertDialog dialog = new AlertDialog.Builder(context)
                            .setTitle("File size too large")
                            .setMessage("You have exceeded up to the maximum file size upload which is "+maxFileSize+" MB." +
                                    "\nYour current file size is " +fileSizeInMB+" MB" +
                                    "\nThis attachment will not be sent and may cause incomplete report." +
                                    "\nPlease create a new attachment.")
                            .setPositiveButton("Create New", new DialogInterface.OnClickListener() {
                                @Override
                                public void onClick(DialogInterface dialog, int id) {



                                }
                            }).setNegativeButton("Cancel", new DialogInterface.OnClickListener() {
                                @Override
                                public void onClick(DialogInterface dialog, int id) {
                                }
                            }).create();

                    dialog.show();

                } else {

                    //open pdf reader
                    if (canDisplayPdf(context)){
                        final File attachment_path = new File(Environment.getExternalStorageDirectory() + "/" + dir + "/");
                        final File prop_pictures = new File(attachment_path, pdf_name);
                        Intent viewPDF = new Intent(Intent.ACTION_VIEW);
                        //viewPDF.setDataAndType(Uri.fromFile(prop_pictures), "application/pdf");
                        //Nov 26 2018 Ian Lozares
                        viewPDF.setDataAndType(FileProvider.getUriForFile(context,context.getApplicationContext().getPackageName()+".fileprovider",prop_pictures), "application/pdf");
                        viewPDF.addFlags(Intent.FLAG_GRANT_READ_URI_PERMISSION);
                        //end
                        context.startActivity(viewPDF);
                        //finish();
                    } else {
                        Snackbar snackbar = Snackbar
                                .make(container, "Your phone doesn\'t have a PDF file viewer.", Snackbar.LENGTH_INDEFINITE)
                                .setActionTextColor(getResources().getColor(R.color.colorPrimary))
                                .setAction("DOWNLOAD HERE", new View.OnClickListener() {
                                    @Override
                                    public void onClick(View view) {
                                        final String appPackageName = "com.adobe.reader";
                                        try {
                                            startActivity(new Intent(Intent.ACTION_VIEW, Uri.parse("market://details?id=" + appPackageName)));
                                        } catch (ActivityNotFoundException anfe) {
                                            startActivity(new Intent(Intent.ACTION_VIEW, Uri.parse("http://play.google.com/store/apps/details?id=" + appPackageName)));
                                        }

                                    }
                                });

                        snackbar.show();
                    }

                }


            } else {
                //prompt corrupted file
                Snackbar snackbar = Snackbar
                        .make(container, "Failed to create an attachment.", Snackbar.LENGTH_INDEFINITE)
                        .setActionTextColor(getResources().getColor(R.color.colorPrimary))
                        .setAction("CREATE NEW", new View.OnClickListener() {
                            @Override
                            public void onClick(View view) {
                                finish();
                                startActivity(getIntent());
                            }
                        });
                snackbar.show();
            }

        }

        @Override
        protected void onPreExecute() {

            pDialog = new ProgressDialog(Collage_New.this);
            pDialog.setMessage("Creating Pdf...");
            pDialog.setIndeterminate(false);
            pDialog.setCancelable(true);
            pDialog.setCanceledOnTouchOutside(false);
            pDialog.show();

        }

        @Override
        protected void onProgressUpdate(Void... values) {}
    }

    void create_pdf() {

        //Now create the name of your PDF file that you will generate
        File pdfFile = new File(dir, "test.pdf");
        try {

            Document document = new Document();

            /*PdfWriter.getInstance(document, new FileOutputStream(
                    android.os.Environment.getExternalStorageDirectory()
                            + java.io.File.separator + ATTACHMENTS_DIRECTORY
                            + java.io.File.separator + pdf_name));*/

            PdfWriter.getInstance(document, new FileOutputStream(
                    Environment.getExternalStorageDirectory()
                            + File.separator + dir
                            + File.separator + pdf_name));

            document.open();

            int childcount_container = container.getChildCount();
            for (int i = 0; i < childcount_container; i++) {//loop for inflated view

                RadioButton rb_portrait = container.getChildAt( i ).findViewById( R.id.rb_portrait );
                boolean isPortrait = rb_portrait.isChecked();

                if (isPortrait){
                    document.setPageSize(PageSize.LETTER);
                } else {
                    document.setPageSize(PageSize.LETTER.rotate());
                }

                document.newPage();

                if (imagesPerPage.get(i) == 1){

                    ImageView iv1 = container.getChildAt(i).findViewById( R.id.iv1 );
                    iv1.setDrawingCacheEnabled(true);
                    iv1.buildDrawingCache();
                    Bitmap screen1 = iv1.getDrawingCache();
                    //Nov 26 2018 Ian Lozares
                    //capture a large off-screen view in it's entirety:
                    //prevent View too large to fit into drawing cache
                    /*Bitmap screen1 = Bitmap.createBitmap(iv1.getWidth(), iv1.getHeight(), Bitmap.Config.ARGB_8888);
                    Canvas c = new Canvas(screen1);
                    iv1.layout(0, 0, iv1.getLayoutParams().width, iv1.getLayoutParams().height);
                    iv1.draw(c);*/
                    //end
                    ByteArrayOutputStream baos1 = new ByteArrayOutputStream();
//                screen.compress(Bitmap.CompressFormat.JPEG, 100, baos);
                    screen1.compress(Bitmap.CompressFormat.PNG, 100, baos1);
                    byte[] byteArray1 = baos1.toByteArray();

                    EditText et1 = container.getChildAt( i ).findViewById( R.id.et1 );
                    String p1 = et1.getText().toString();

                    createPage_1_img(document, byteArray1, p1, isPortrait);

                } else if (imagesPerPage.get(i) == 2){

                    ImageView iv1 = container.getChildAt(i).findViewById( R.id.iv1 );
                    iv1.buildDrawingCache();
                    Bitmap screen1 = iv1.getDrawingCache();

                    ByteArrayOutputStream baos1 = new ByteArrayOutputStream();
//                screen.compress(Bitmap.CompressFormat.JPEG, 100, baos);
                    screen1.compress(Bitmap.CompressFormat.PNG, 100, baos1);
                    byte[] byteArray1 = baos1.toByteArray();

                    EditText et1 = container.getChildAt( i ).findViewById( R.id.et1 );
                    String p1 = et1.getText().toString();

                    ImageView iv2 = container.getChildAt(i).findViewById( R.id.iv2 );
                    iv2.buildDrawingCache();
                    Bitmap screen2 = iv2.getDrawingCache();

                    ByteArrayOutputStream baos2 = new ByteArrayOutputStream();
//                screen.compress(Bitmap.CompressFormat.JPEG, 100, baos);
                    screen2.compress(Bitmap.CompressFormat.PNG, 100, baos2);
                    byte[] byteArray2 = baos2.toByteArray();

                    EditText et2 = container.getChildAt( i ).findViewById( R.id.et2 );
                    String p2 = et2.getText().toString();

                    createPage_2_img(document, byteArray1, p1, byteArray2, p2,
                            isPortrait);

                } else if (imagesPerPage.get(i) == 3){

                    ImageView iv1 = container.getChildAt(i).findViewById( R.id.iv1 );
                    iv1.buildDrawingCache();
                    Bitmap screen1 = iv1.getDrawingCache();

                    ByteArrayOutputStream baos1 = new ByteArrayOutputStream();
//                screen.compress(Bitmap.CompressFormat.JPEG, 100, baos);
                    screen1.compress(Bitmap.CompressFormat.PNG, 100, baos1);
                    byte[] byteArray1 = baos1.toByteArray();

                    EditText et1 = container.getChildAt( i ).findViewById( R.id.et1 );
                    String p1 = et1.getText().toString();

                    ImageView iv2 = container.getChildAt(i).findViewById( R.id.iv2 );
                    iv2.buildDrawingCache();
                    Bitmap screen2 = iv2.getDrawingCache();

                    ByteArrayOutputStream baos2 = new ByteArrayOutputStream();
//                screen.compress(Bitmap.CompressFormat.JPEG, 100, baos);
                    screen2.compress(Bitmap.CompressFormat.PNG, 100, baos2);
                    byte[] byteArray2 = baos2.toByteArray();

                    EditText et2 = container.getChildAt( i ).findViewById( R.id.et2 );
                    String p2 = et2.getText().toString();



                    ImageView iv3 = container.getChildAt(i).findViewById( R.id.iv3);
                    iv3.buildDrawingCache();
                    Bitmap screen3 = iv3.getDrawingCache();

                    ByteArrayOutputStream baos3 = new ByteArrayOutputStream();
//                screen.compress(Bitmap.CompressFormat.JPEG, 100, baos);
                    screen3.compress(Bitmap.CompressFormat.PNG, 100, baos3);
                    byte[] byteArray3 = baos3.toByteArray();

                    EditText et3 = container.getChildAt( i ).findViewById( R.id.et3 );
                    String p3 = et3.getText().toString();


                    createPage_2_img_col(document, byteArray1, p1, byteArray2, p2, byteArray3, p3, isPortrait);
                } else if (imagesPerPage.get(i) == 4){


                    ImageView iv1 = container.getChildAt(i).findViewById( R.id.iv1 );
                    iv1.buildDrawingCache();
                    Bitmap screen1 = iv1.getDrawingCache();

                    ByteArrayOutputStream baos1 = new ByteArrayOutputStream();
//                screen.compress(Bitmap.CompressFormat.JPEG, 100, baos);
                    screen1.compress(Bitmap.CompressFormat.PNG, 100, baos1);
                    byte[] byteArray1 = baos1.toByteArray();

                    EditText et1 = container.getChildAt( i ).findViewById( R.id.et1 );
                    String p1 = et1.getText().toString();

                    ImageView iv2 = container.getChildAt(i).findViewById( R.id.iv2 );
                    iv2.buildDrawingCache();
                    Bitmap screen2 = iv2.getDrawingCache();

                    ByteArrayOutputStream baos2 = new ByteArrayOutputStream();
//                screen.compress(Bitmap.CompressFormat.JPEG, 100, baos);
                    screen2.compress(Bitmap.CompressFormat.PNG, 100, baos2);
                    byte[] byteArray2 = baos2.toByteArray();

                    EditText et2 = container.getChildAt( i ).findViewById( R.id.et2 );
                    String p2 = et2.getText().toString();

                    ImageView iv3 = container.getChildAt(i).findViewById( R.id.iv3 );
                    iv3.buildDrawingCache();
                    Bitmap screen3 = iv3.getDrawingCache();

                    ByteArrayOutputStream baos3 = new ByteArrayOutputStream();
//                screen.compress(Bitmap.CompressFormat.JPEG, 100, baos);
                    screen3.compress(Bitmap.CompressFormat.PNG, 100, baos3);
                    byte[] byteArray3 = baos3.toByteArray();

                    EditText et3 = container.getChildAt( i ).findViewById( R.id.et3 );
                    String p3 = et3.getText().toString();

                    ImageView iv4 = container.getChildAt(i).findViewById( R.id.iv4 );
                    iv4.buildDrawingCache();
                    Bitmap screen4 = iv4.getDrawingCache();

                    ByteArrayOutputStream baos4 = new ByteArrayOutputStream();
//                screen.compress(Bitmap.CompressFormat.JPEG, 100, baos);
                    screen4.compress(Bitmap.CompressFormat.PNG, 100, baos4);
                    byte[] byteArray4 = baos4.toByteArray();

                    EditText et4 = container.getChildAt( i ).findViewById( R.id.et4 );
                    String p4 = et4.getText().toString();

                    createPage_4_img(document, byteArray1, p1, byteArray2, p2,
                            byteArray3, p3, byteArray4, p4,
                            isPortrait);

                }
            }

            document.close();
           /*
            //TODO SAve to CollageDB
            final File attachment_path = new File(Environment.getExternalStorageDirectory() + "/" +ATTACHMENTS_DIRECTORY + "/"+"/"+pdf_name);
            Realm.getDefaultInstance();
            Realm realm = Realm.getDefaultInstance();

            realm.executeTransaction(realm1 -> {
                AttachmentCreateeModel attachmentCreateeModel = realm1.createObject(AttachmentCreateeModel.class);
                attachmentCreateeModel.setId(record_id);
                attachmentCreateeModel.setUnique_key(GlobalFunctions.generateRandomHexToken(20));
                attachmentCreateeModel.setFilename(pdf_name);
                attachmentCreateeModel.setAppraisal_type(appraisal_type);
                attachmentCreateeModel.setFiletype(attachment_type);
                Log.e("PDFFILEPATH",attachment_path.toString());
            });
            */

            Intent returnIntent = new Intent();
            returnIntent.putExtra("record_id",record_id);
            returnIntent.putExtra("pdf_name",pdf_name);
            returnIntent.putExtra("appraisal_type",appraisal_type);
            returnIntent.putExtra("attachment_type",attachment_type);
            setResult(Activity.RESULT_OK,returnIntent);
            finish();

        }
        catch (Exception e){
            e.printStackTrace();
        }
    }

    private static void addImage(Document document, byte[] byteArray){
        Image image = null;
        try
        {
            image = Image.getInstance(byteArray);
            image.setAlignment(Image.MIDDLE);
            document.add(image);
        }
        catch (BadElementException e)
        {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }
        catch (MalformedURLException e)
        {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }
        catch (IOException e)
        {
            // TODO Auto-generated catch block
            e.printStackTrace();
        } catch (DocumentException e) {
            e.printStackTrace();
        }
    }

    private static void createPage_1_img(Document document, byte[] byteArray1, String p1,
                                         boolean isPortrait)
            throws IOException, DocumentException {
        Image image1 = null;

        image1 = Image.getInstance(byteArray1);
        image1.setAlignment(Image.MIDDLE);

        if (isPortrait) {
            image1.scaleToFit(PageSize.LETTER.getWidth() / 2, PageSize.LETTER.getHeight() / 2);
        } else {
            image1.scaleToFit(PageSize.LETTER.getWidth() / 2, PageSize.LETTER.getHeight() / 2);
        }

        Paragraph paragraph1 = new Paragraph(p1);
        paragraph1.setAlignment(Element.ALIGN_CENTER);

        PdfPTable table = new PdfPTable(1);
        table.setWidthPercentage(100);

        PdfPCell cell = new PdfPCell();
        cell.setBorder(cell.NO_BORDER);
        cell.setHorizontalAlignment(cell.ALIGN_CENTER);
        cell.setVerticalAlignment(cell.ALIGN_CENTER);

        cell.addElement(image1);
        cell.addElement(paragraph1);

        table.addCell(cell);

           /* PdfPCell cell2 = new PdfPCell();
            cell2.addElement(image);
            cell2.addElement(image);

            table.addCell(cell2);*/

        document.add(table);
    }

    private static void createPage_2_img(Document document, byte[] byteArray1, String p1, byte[] byteArray2, String p2,
                                         boolean isPortrait)
            throws IOException, DocumentException {
        Image image1 = null;

        image1 = Image.getInstance(byteArray1);
        image1.setAlignment(Image.MIDDLE);
//        image1.scaleToFit(PageSize.LETTER.getWidth() / 2, PageSize.LETTER.getHeight() / 2);

        Paragraph paragraph1 = new Paragraph(p1);
        paragraph1.setAlignment(Element.ALIGN_CENTER);

        Image image2 = null;

        image2 = Image.getInstance(byteArray2);
        image2.setAlignment(Image.MIDDLE);
//        image2.scaleToFit(PageSize.LETTER.getWidth() / 2, PageSize.LETTER.getHeight() / 2);

        if (isPortrait) {
            image1.scaleToFit(PageSize.LETTER.getWidth() / 2, PageSize.LETTER.getHeight() / 2);
            image2.scaleToFit(PageSize.LETTER.getWidth() / 2, PageSize.LETTER.getHeight() / 2);
        } else {
            image1.scaleToFit(PageSize.LETTER.getWidth() / 4, PageSize.LETTER.getHeight() / 4);
            image2.scaleToFit(PageSize.LETTER.getWidth() / 4, PageSize.LETTER.getHeight() / 4);
        }

        Paragraph paragraph2 = new Paragraph(p2);
        paragraph2.setAlignment(Element.ALIGN_CENTER);

        PdfPTable table = new PdfPTable(1);
        table.setWidthPercentage(100);

        PdfPCell cell = new PdfPCell();
        cell.setBorder(cell.NO_BORDER);
        cell.setHorizontalAlignment(cell.ALIGN_CENTER);
        cell.setVerticalAlignment(cell.ALIGN_CENTER);

        cell.addElement(image1);
        cell.addElement(paragraph1);

        cell.addElement(new Paragraph("\n"));

        cell.addElement(image2);
        cell.addElement(paragraph2);

        table.addCell(cell);


        /*PdfPCell cell2 = new PdfPCell();
        cell2.setBorderColor(Color.white);

        cell2.addElement(image2);
        cell2.addElement(paragraph2);

        table.addCell(cell2);*/

        document.add(table);
    }

    private static void createPage_2_img_col(Document document, byte[] byteArray1, String p1, byte[] byteArray2, String p2,  byte[] byteArray3, String p3,boolean isPortrait)
            throws IOException, DocumentException {
        Image image1 = null;

        image1 = Image.getInstance(byteArray1);
        image1.setAlignment(Image.MIDDLE);
//        image1.scaleToFit(PageSize.LETTER.getWidth() / 4, PageSize.LETTER.getHeight() / 4);

        Paragraph paragraph1 = new Paragraph(p1);
        paragraph1.setAlignment(Paragraph.ALIGN_JUSTIFIED);
        paragraph1.setIndentationRight(10);
        paragraph1.setIndentationLeft(10);

        Image image2 = null;

        image2 = Image.getInstance(byteArray2);
        image2.setAlignment(Image.MIDDLE);
//        image2.scaleToFit(PageSize.LETTER.getWidth() / 4, PageSize.LETTER.getHeight() / 4);

        Paragraph paragraph2 = new Paragraph(p2);
        paragraph2.setAlignment(Paragraph.ALIGN_JUSTIFIED);
        paragraph2.setIndentationRight(10);
        paragraph2.setIndentationLeft(10);

        Image image3 = null;

        image3 = Image.getInstance(byteArray3);
        image3.setAlignment(Image.MIDDLE);
//        image3.scaleToFit(PageSize.LETTER.getWidth() / 4, PageSize.LETTER.getHeight() / 4);

        Paragraph paragraph3 = new Paragraph(p3);
        paragraph3.setAlignment(Paragraph.ALIGN_MIDDLE);
        paragraph3.setIndentationRight(10);
        paragraph3.setIndentationLeft(10);


//        image4.scaleToFit(PageSize.LETTER.getWidth() / 4, PageSize.LETTER.getHeight() / 4);

        if (isPortrait) {
            /*image1.scaleToFit(PageSize.LETTER.getWidth() / 2, PageSize.LETTER.getHeight() / 2);
            image2.scaleToFit(PageSize.LETTER.getWidth() / 2, PageSize.LETTER.getHeight() / 2);
            image3.scaleToFit(PageSize.LETTER.getWidth() / 2, PageSize.LETTER.getHeight() / 2);
            image4.scaleToFit(PageSize.LETTER.getWidth() / 2, PageSize.LETTER.getHeight() / 2);*/

            image1.scaleAbsolute(200, 200);
            image2.scaleAbsolute(200, 200);
            image3.scaleAbsolute(200, 200);

        } else {
            /*image1.scaleToFit(PageSize.LETTER.getWidth() / 4, PageSize.LETTER.getHeight() / 4);
            image2.scaleToFit(PageSize.LETTER.getWidth() / 4, PageSize.LETTER.getHeight() / 4);
            image3.scaleToFit(PageSize.LETTER.getWidth() / 4, PageSize.LETTER.getHeight() / 4);
            image4.scaleToFit(PageSize.LETTER.getWidth() / 4, PageSize.LETTER.getHeight() / 4);*/

            image1.scaleAbsolute(200, 200);
            image2.scaleAbsolute(200, 200);
            image3.scaleAbsolute(200, 200);

        }


        PdfPTable table = new PdfPTable(2);
        table.setWidthPercentage(100);

        PdfPCell cell = new PdfPCell();
        cell.setBorder(cell.NO_BORDER);
        cell.setHorizontalAlignment(cell.ALIGN_CENTER);
        cell.setVerticalAlignment(cell.ALIGN_CENTER);

        cell.addElement(image1);
        cell.addElement(paragraph1);

        cell.addElement(new Paragraph("\n"));

        cell.addElement(image3);
        cell.addElement(paragraph3);



        table.addCell(cell);


        PdfPCell cell2 = new PdfPCell();
        cell2.setBorder(cell2.NO_BORDER);

        cell2.addElement(image2);
        cell2.addElement(paragraph2);

        cell2.addElement(new Paragraph("\n"));



        table.addCell(cell2);

        document.add(table);
    }


    private static void createPage_4_img(Document document, byte[] byteArray1, String p1, byte[] byteArray2, String p2,
                                         byte[] byteArray3, String p3, byte[] byteArray4, String p4,
                                         boolean isPortrait)
            throws IOException, DocumentException {
        Image image1 = null;

        image1 = Image.getInstance(byteArray1);
        image1.setAlignment(Image.MIDDLE);
//        image1.scaleToFit(PageSize.LETTER.getWidth() / 4, PageSize.LETTER.getHeight() / 4);

        Paragraph paragraph1 = new Paragraph(p1);
        paragraph1.setAlignment(Paragraph.ALIGN_JUSTIFIED);
        paragraph1.setIndentationRight(10);
        paragraph1.setIndentationLeft(10);

        Image image2 = null;

        image2 = Image.getInstance(byteArray2);
        image2.setAlignment(Image.MIDDLE);
//        image2.scaleToFit(PageSize.LETTER.getWidth() / 4, PageSize.LETTER.getHeight() / 4);

        Paragraph paragraph2 = new Paragraph(p2);
        paragraph2.setAlignment(Paragraph.ALIGN_JUSTIFIED);
        paragraph2.setIndentationRight(10);
        paragraph2.setIndentationLeft(10);

        Image image3 = null;

        image3 = Image.getInstance(byteArray3);
        image3.setAlignment(Image.MIDDLE);
//        image3.scaleToFit(PageSize.LETTER.getWidth() / 4, PageSize.LETTER.getHeight() / 4);

        Paragraph paragraph3 = new Paragraph(p3);
        paragraph3.setAlignment(Paragraph.ALIGN_JUSTIFIED);
        paragraph3.setIndentationRight(10);
        paragraph3.setIndentationLeft(10);

        Image image4 = null;

        image4 = Image.getInstance(byteArray4);
        image4.setAlignment(Image.MIDDLE);
//        image4.scaleToFit(PageSize.LETTER.getWidth() / 4, PageSize.LETTER.getHeight() / 4);

        if (isPortrait) {
            /*image1.scaleToFit(PageSize.LETTER.getWidth() / 2, PageSize.LETTER.getHeight() / 2);
            image2.scaleToFit(PageSize.LETTER.getWidth() / 2, PageSize.LETTER.getHeight() / 2);
            image3.scaleToFit(PageSize.LETTER.getWidth() / 2, PageSize.LETTER.getHeight() / 2);
            image4.scaleToFit(PageSize.LETTER.getWidth() / 2, PageSize.LETTER.getHeight() / 2);*/

            image1.scaleAbsolute(200, 200);
            image2.scaleAbsolute(200, 200);
            image3.scaleAbsolute(200, 200);
            image4.scaleAbsolute(200, 200);
        } else {
            /*image1.scaleToFit(PageSize.LETTER.getWidth() / 4, PageSize.LETTER.getHeight() / 4);
            image2.scaleToFit(PageSize.LETTER.getWidth() / 4, PageSize.LETTER.getHeight() / 4);
            image3.scaleToFit(PageSize.LETTER.getWidth() / 4, PageSize.LETTER.getHeight() / 4);
            image4.scaleToFit(PageSize.LETTER.getWidth() / 4, PageSize.LETTER.getHeight() / 4);*/

            image1.scaleAbsolute(200, 200);
            image2.scaleAbsolute(200, 200);
            image3.scaleAbsolute(200, 200);
            image4.scaleAbsolute(200, 200);
        }

        Paragraph paragraph4 = new Paragraph(p4);
        paragraph4.setAlignment(Paragraph.ALIGN_JUSTIFIED);
        paragraph4.setIndentationRight(10);
        paragraph4.setIndentationLeft(10);

        PdfPTable table = new PdfPTable(2);
        table.setWidthPercentage(100);

        PdfPCell cell = new PdfPCell();
        cell.setBorder(cell.NO_BORDER);
        cell.setHorizontalAlignment(cell.ALIGN_CENTER);
        cell.setVerticalAlignment(cell.ALIGN_CENTER);

        cell.addElement(image1);
        cell.addElement(paragraph1);

        cell.addElement(new Paragraph("\n"));

        cell.addElement(image3);
        cell.addElement(paragraph3);



        table.addCell(cell);


        PdfPCell cell2 = new PdfPCell();
        cell2.setBorder(cell2.NO_BORDER);

        cell2.addElement(image2);
        cell2.addElement(paragraph2);

        cell2.addElement(new Paragraph("\n"));

        cell2.addElement(image4);
        cell2.addElement(paragraph4);

        table.addCell(cell2);

        document.add(table);
    }



    /**
     * Check if the supplied context can render PDF files via some installed application that reacts to a intent
     * with the pdf mime type and viewing action.
     *
     * @param context
     * @return
     */
    public static final String MIME_TYPE_PDF = "application/pdf";
    public static boolean canDisplayPdf(Context context) {
        PackageManager packageManager = context.getPackageManager();
        Intent testIntent = new Intent(Intent.ACTION_VIEW);
        testIntent.setType(MIME_TYPE_PDF);
        if (packageManager.queryIntentActivities(testIntent, PackageManager.MATCH_DEFAULT_ONLY).size() > 0) {
            return true;
        } else {
            return false;
        }
    }

}
