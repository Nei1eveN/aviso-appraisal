package com.gdslinkasia.attachment;

import android.content.Context;
import android.content.Intent;

import com.gdslinkasia.base.model.details.AppAttachment;
import com.gdslinkasia.base.model.details.AppraisalRequest;

import java.util.List;

import io.reactivex.Flowable;
import io.reactivex.Single;

public interface AttachmentsContract {

    interface View {
        void showProgress(String title, String message);

        void hideProgress();

        void showAddButton(int visibility);

        void showList(List<AppAttachment> valrepLandimpLotDetails);

        void showEmptyState(String message);

        void showErrorDialog(String title, String message);

        void showRemoveItemDialog(String title, String message, String optionText, long attachmentUId);

        void openAttachment(Intent target, String optionTitle, String activityNotFoundMessage, String imageFileName);

        void showPlottingActivity(Intent openPlottingIntent);

        void showPlayStoreApplication(Intent intent);

        void requestPermissionDialog(String title, String message);
    }

    interface Presenter {
        void onStart(String recordId, String attachmentCreatedFrom);

        void onResume(String recordId, String attachmentCreatedFrom);

        void onDestroy();

        void addNewAttachment(String record_id, String createdFrom, String pdf_name, String appraisal_type, String attachment_type);

        void checkFileFromLocalStorage(String fileName, long attachmentUId, String attachmentCreatedFrom, Context context);

        void deleteAttachmentItem(long uniqueId, String recordId, String attachmentCreatedFrom);

        void checkLocationStatus(String imageFileName, Context context);
    }

    interface Interactor {
        Single<AppraisalRequest> getAppraisalRequestByRecordId(String recordId);

        Single<List<AppAttachment>> getAttachmentList(long appraisalRequestId, String createdFrom);

        Flowable<List<AppAttachment>> getListFlowable(long appraisalRequestId, String createdFrom);

        void insertAttachment(AppAttachment detail);

        void deleteAttachment(long attachmentUid);

        void onDestroy();
    }

}
