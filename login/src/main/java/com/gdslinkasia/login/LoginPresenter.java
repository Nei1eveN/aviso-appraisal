package com.gdslinkasia.login;

import android.content.Context;
import android.os.Build;

import androidx.annotation.RequiresApi;

public class LoginPresenter implements LoginContract.Presenter,
        LoginContract.Interactor.LoginListener, LoginContract.Interactor.FindUserListener {

    private LoginContract.View view;

    private LoginContract.Interactor interactor;

    LoginPresenter(Context context) {
        this.interactor = new LoginInteractor(context);
    }

    @Override
    public void onStart() {
        interactor.getUser(this);
    }

    @RequiresApi(api = Build.VERSION_CODES.KITKAT)
    @Override
    public void submitLoginCredentials(String email, String password) {
        if (view != null) {
            view.showProgress("Please wait...");
            if (email.isEmpty()) {
                view.showErrorDialog("Empty Email", "Please enter your email");
                view.hideProgress();
            } else if (password.isEmpty()) {
                view.showErrorDialog("Login Error", "Please enter your password");
                view.hideProgress();
            } else {
                interactor.getLoginCredentials(email, password, this);
            }
        }
    }

    @Override
    public void takeView(LoginContract.View view) {
        this.view = view;
    }

    @Override
    public void dropView() {
        view = null;
    }

    @Override
    public void onLoginSuccess(String message, String email) {
        if (view != null) {
            view.showToastMessage(message);
            view.hideProgress();
            view.goToHomePage(email);
        }
    }

    @Override
    public void onLoginError(String title, String message) {
        if (view != null) {
            view.showToastMessage(message);
            view.showErrorDialog(title, message);
            view.hideProgress();
        }
    }

    @Override
    public void onUserFound(String message, String email) {
//        if (view != null) {
//
//        }
        //            view.showToastMessage(message);
        view.showToastMessage(email);
        view.goToHomePage(email);
    }

    @Override
    public void onUserEmpty(String message) {
        if (view != null) {
            view.showToastMessage(message);
        }
    }
}
