package com.gdslinkasia.login;

import android.app.ProgressDialog;
import android.content.Intent;
import android.os.Bundle;
import android.widget.Button;
import android.widget.Toast;

import androidx.appcompat.app.AlertDialog;
import androidx.coordinatorlayout.widget.CoordinatorLayout;

import com.gdslinkasia.base.utils.ActivityIntent;
import com.google.android.material.textfield.TextInputEditText;

import java.util.Objects;

import javax.inject.Inject;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import dagger.android.support.DaggerAppCompatActivity;

public class LoginActivity extends DaggerAppCompatActivity implements LoginContract.View {

    @BindView(R2.id.coordinatorLayout) CoordinatorLayout coordinatorLayout;
    @BindView(R2.id.editUsername) TextInputEditText etUsername;
    @BindView(R2.id.editPassword) TextInputEditText etPassword;

    @BindView(R2.id.btLogin) Button btnLogin;

    @Inject
    LoginContract.Presenter presenter;

    ProgressDialog progressDialog;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login);
        ButterKnife.bind(this);

        progressDialog = new ProgressDialog(this);

        presenter.takeView(this);
    }

    @Override
    protected void onResume() {
        super.onResume();
        presenter.onStart();
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        presenter.dropView();
        progressDialog.dismiss();
    }

    @Override
    public void showToastMessage(String message) {
        Toast.makeText(this, message, Toast.LENGTH_SHORT).show();
    }

    @Override
    public void showProgress(String message) {
        progressDialog.setMessage(message);
        progressDialog.setCancelable(false);
        progressDialog.show();
    }

    @Override
    public void hideProgress() {
        progressDialog.hide();
    }

    @Override
    public void showErrorDialog(String title, String message) {
        new AlertDialog.Builder(this)
                .setCancelable(false)
                .setTitle(title)
                .setMessage(message)
                .setPositiveButton("CLOSE", null)
                .create()
                .show();
    }

    @Override
    public void goToHomePage(String email) {
        try {
            Intent intent = new Intent(this, Class.forName(ActivityIntent.MAIN_ACTIVITY));
            intent.putExtra("email", email);
            startActivity(intent);
        } catch (ClassNotFoundException e) {
            e.printStackTrace();
        }
    }

    @OnClick(R2.id.btLogin)
    void onClickLogin() {
        presenter.submitLoginCredentials(
                Objects.requireNonNull(etUsername.getText()).toString(),
                Objects.requireNonNull(etPassword.getText()).toString());
    }
}
