package com.gdslinkasia.login;

import android.content.Context;
import android.os.Build;
import android.util.Base64;
import android.util.Log;

import androidx.annotation.RequiresApi;

import com.androidnetworking.AndroidNetworking;
import com.androidnetworking.common.Priority;
import com.androidnetworking.error.ANError;
import com.androidnetworking.interfaces.JSONObjectRequestListener;
import com.gdslinkasia.base.SessionManager;
import com.gdslinkasia.base.utils.GlobalString;

import org.json.JSONObject;

import java.nio.charset.StandardCharsets;

public class LoginInteractor implements LoginContract.Interactor {

    private Context context;

    LoginInteractor(Context context) {
        this.context = context;
    }

    @Override
    public void getUser(FindUserListener listener) {
        SessionManager sessionManager = new SessionManager(context);
        if (sessionManager.isLoggedIn()) {
            listener.onUserFound("Logged in as "+sessionManager.getUserEmail(), sessionManager.getUserEmail());
        }
    }

    @RequiresApi(api = Build.VERSION_CODES.KITKAT)
    @Override
    public void getLoginCredentials(final String email, String password, final LoginListener listener) {

        byte[] bytes1 = password.getBytes(StandardCharsets.UTF_8);
        String firstPass = Base64.encodeToString(bytes1, Base64.DEFAULT);
        Log.d("int--1stPass", firstPass);

        byte[] bytes2 = firstPass.getBytes(StandardCharsets.UTF_8);
        String secondPass = Base64.encodeToString(bytes2, Base64.DEFAULT);
        Log.d("int--2ndPass", secondPass);

        byte[] bytes3 = secondPass.getBytes(StandardCharsets.UTF_8);
        String thirdPass = Base64.encodeToString(bytes3, Base64.DEFAULT);
        Log.d("int--3rdPass", thirdPass);

        byte[] bytes4 = thirdPass.getBytes(StandardCharsets.UTF_8);
        String fourthPass = Base64.encodeToString(bytes4, Base64.DEFAULT);
        Log.d("int--4thPass", fourthPass);

        AndroidNetworking.post(GlobalString.sign_in_url)
                .addBodyParameter(GlobalString.email, email)
                .addBodyParameter(GlobalString.password, fourthPass)
                .setPriority(Priority.MEDIUM)
                .build()
                .getAsJSONObject(new JSONObjectRequestListener() {
                    @Override
                    public void onResponse(JSONObject response) {
                        // do anything with response
                        SessionManager sessionManager = new SessionManager(context);
                        sessionManager.setLogin(true, email);
                        listener.onLoginSuccess("Login Success", sessionManager.getUserEmail());

                    }
                    @Override
                    public void onError(ANError error) {
                        // handle error
                        error.printStackTrace();
                        listener.onLoginError("Login Error", error.getErrorDetail()+"\n\n"+error.getErrorBody());
                    }});}}


