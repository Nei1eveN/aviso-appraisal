package com.gdslinkasia.login;

import android.content.Context;

import dagger.Module;
import dagger.Provides;

@Module
public class LoginModule {

    @Provides
    static LoginContract.Presenter loginPresenter(Context context) {
        return new LoginPresenter(context);
    }

}
