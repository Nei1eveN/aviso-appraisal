package com.gdslinkasia.login;

import com.gdslinkasia.base.BasePresenter;
import com.gdslinkasia.base.BaseView;

public interface LoginContract {

    interface View extends BaseView<Presenter> {
        void showToastMessage(String message);

        void showProgress(String message);

        void hideProgress();

        void showErrorDialog(String title, String message);

        void goToHomePage(String email);
    }

    interface Presenter extends BasePresenter<View> {
        void onStart();

        void submitLoginCredentials(String email, String password);
    }

    interface Interactor {
        interface FindUserListener {
            void onUserFound(String message, String email);

            void onUserEmpty(String message);
        }

        void getUser(FindUserListener listener);

        interface LoginListener {
            void onLoginSuccess(String message, String email);

            void onLoginError(String title, String message);
        }

        void getLoginCredentials(String email, String password, LoginListener listener);
    }
}
