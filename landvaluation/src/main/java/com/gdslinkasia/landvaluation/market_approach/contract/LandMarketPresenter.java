package com.gdslinkasia.landvaluation.market_approach.contract;

import android.app.Application;
import android.os.Handler;

import com.gdslinkasia.base.model.details.ValrepLandimpLotValuationDetail;

import java.util.List;

import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.disposables.CompositeDisposable;
import io.reactivex.observers.DisposableSingleObserver;
import io.reactivex.schedulers.Schedulers;

public class LandMarketPresenter implements LandMarketContract.Presenter {

    private LandMarketContract.View view;
    private LandMarketContract.Interactor interactor;
    private CompositeDisposable compositeDisposable = new CompositeDisposable();

    public LandMarketPresenter(Application application) {
        this.interactor = new LandMarketInteractor(application);
    }

    @Override
    public void onStart() {
        view.showEmptyState("Loading Records. Please wait...");
    }

    @Override
    public void onResume(String recordId) {
        view.showProgress();
        compositeDisposable.add(interactor.getLotValuationDetailsSingle(recordId)
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribeWith(getLotValuationDetailSingle(recordId)));
    }

    private DisposableSingleObserver<List<ValrepLandimpLotValuationDetail>> getLotValuationDetailSingle(String recordId) {
        return new DisposableSingleObserver<List<ValrepLandimpLotValuationDetail>>() {
            @Override
            public void onSuccess(List<ValrepLandimpLotValuationDetail> lotValuationDetails) {
                if (!lotValuationDetails.isEmpty()) {
                    compositeDisposable.add(interactor.getLotValuationDetailsFlowable(recordId)
                    .subscribeOn(Schedulers.io())
                    .observeOn(AndroidSchedulers.mainThread())
                    .subscribe(lotValuationDetails1 -> view.showList(lotValuationDetails1), throwable -> {
                        throwable.printStackTrace();
                        view.showEmptyState(throwable.getMessage());
                    }));
                } else {
                    view.showEmptyState("No Records Available");
                }
                new Handler().postDelayed(() -> view.hideProgress(), 1000);
            }

            @Override
            public void onError(Throwable e) {
                e.printStackTrace();
                view.showEmptyState(e.getMessage());
            }
        };
    }

    @Override
    public void takeView(LandMarketContract.View view) {
        this.view = view;
    }

    @Override
    public void dropView() {
        view = null;
    }
}
