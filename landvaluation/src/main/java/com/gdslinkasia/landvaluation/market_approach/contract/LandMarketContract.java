package com.gdslinkasia.landvaluation.market_approach.contract;

import com.gdslinkasia.base.BasePresenter;
import com.gdslinkasia.base.model.details.ValrepLandimpLotValuationDetail;

import java.util.List;

import io.reactivex.Flowable;
import io.reactivex.Single;

public interface LandMarketContract {

    interface View {
        void showProgress();
        void hideProgress();
        void showList(List<ValrepLandimpLotValuationDetail> lotValuationDetails);
        void showEmptyState(String message);
    }

    interface Presenter extends BasePresenter<View> {
        void onStart();
        void onResume(String recordId);
    }

    interface Interactor {
        Single<List<ValrepLandimpLotValuationDetail>> getLotValuationDetailsSingle(String recordId);

        Flowable<List<ValrepLandimpLotValuationDetail>> getLotValuationDetailsFlowable(String recordId);
    }

}
