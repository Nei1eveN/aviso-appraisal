package com.gdslinkasia.landvaluation.market_approach.contract;

import android.app.Application;

import com.gdslinkasia.base.database.repository.LotValuationDetailsLocalRepository;
import com.gdslinkasia.base.model.details.ValrepLandimpLotValuationDetail;

import java.util.List;

import io.reactivex.Flowable;
import io.reactivex.Single;

public class LandMarketInteractor implements LandMarketContract.Interactor {

    private LotValuationDetailsLocalRepository localRepository;

    public LandMarketInteractor(Application application) {
        this.localRepository = new LotValuationDetailsLocalRepository(application);
    }

    @Override
    public Single<List<ValrepLandimpLotValuationDetail>> getLotValuationDetailsSingle(String recordId) {
        return localRepository.getLotValuationDetailListSingle(recordId);
    }

    @Override
    public Flowable<List<ValrepLandimpLotValuationDetail>> getLotValuationDetailsFlowable(String recordId) {
        return localRepository.getLotValuationDetailListFlowable(recordId);
    }
}
