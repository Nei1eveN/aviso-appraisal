package com.gdslinkasia.landvaluation.market_approach;

import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.MenuItem;
import android.view.View;
import android.widget.ProgressBar;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.gdslinkasia.base.adapter.LotValuationDetailAdapter;
import com.gdslinkasia.base.adapter.click_listener.LotValuationDetailClickListener;
import com.gdslinkasia.base.model.details.ValrepLandimpLotValuationDetail;
import com.gdslinkasia.commonresources.R2;
import com.gdslinkasia.landvaluation.R;
import com.gdslinkasia.landvaluation.market_approach.contract.LandMarketContract;

import java.util.List;
import java.util.Objects;

import javax.inject.Inject;

import butterknife.BindView;
import butterknife.ButterKnife;
import dagger.android.support.DaggerAppCompatActivity;

import static com.gdslinkasia.base.database.DatabaseUtils.RECORD_ID;

public class LandMarketApproachListActivity extends DaggerAppCompatActivity
        implements LandMarketContract.View, LotValuationDetailClickListener {

    private String recordId;

    @BindView(R2.id.recyclerView) RecyclerView recyclerView;
    @BindView(R2.id.tvList) TextView emptyText;
    @BindView(R2.id.progressBar) ProgressBar progressBar;

    @Inject
    LandMarketContract.Presenter presenter;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_lot_valuation);
        ButterKnife.bind(this);

        Objects.requireNonNull(getSupportActionBar()).setTitle("Valuation");
        getSupportActionBar().setSubtitle("Land (Market Approach)");
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);

        Intent intent = getIntent();
        recordId = intent.getStringExtra(RECORD_ID);

        presenter.takeView(this);
    }

    @Override
    public boolean onOptionsItemSelected(@NonNull MenuItem item) {
        if (item.getItemId() == android.R.id.home) {
            finish();
        }
        return super.onOptionsItemSelected(item);
    }

    @Override
    protected void onStart() {
        super.onStart();
        presenter.onStart();
    }

    @Override
    protected void onResume() {
        super.onResume();
        presenter.onResume(recordId);
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        presenter.dropView();
    }

    @Override
    public void showProgress() {
        progressBar.setVisibility(View.VISIBLE);
    }

    @Override
    public void hideProgress() {
        progressBar.setVisibility(View.INVISIBLE);
    }

    @Override
    public void showList(List<ValrepLandimpLotValuationDetail> lotValuationDetails) {
        emptyText.setVisibility(View.GONE);

        recyclerView.setVisibility(View.VISIBLE);

        LotValuationDetailAdapter adapter = new LotValuationDetailAdapter(this, this);
        adapter.submitList(lotValuationDetails);

        recyclerView.setLayoutManager(new LinearLayoutManager(this));
        recyclerView.setAdapter(adapter);
    }

    @Override
    public void showEmptyState(String message) {
        emptyText.setVisibility(View.VISIBLE);

        recyclerView.setVisibility(View.GONE);
    }

    @Override
    public void onDetailItemClick(String recordId, long uniqueId) {
        Log.d(LandMarketApproachListActivity.class.getSimpleName(), recordId+" "+uniqueId);
    }

    @Override
    public void onTotalValuationForLand(double totalLandAreaSqm, double lessRestrictionSqm, double netUsableArea, double marketValueForLand) {

    }
}
