package com.gdslinkasia.landvaluation.market_approach.di;

import android.app.Application;

import com.gdslinkasia.landvaluation.market_approach.contract.LandMarketContract;
import com.gdslinkasia.landvaluation.market_approach.contract.LandMarketPresenter;

import dagger.Module;
import dagger.Provides;

@Module
public class LandMarketApproachModule {

    @Provides
    static LandMarketContract.Presenter presenter(Application application) {
        return new LandMarketPresenter(application);
    }

}
