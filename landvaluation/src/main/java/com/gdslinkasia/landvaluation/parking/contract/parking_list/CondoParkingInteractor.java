package com.gdslinkasia.landvaluation.parking.contract.parking_list;

import android.app.Application;

import com.gdslinkasia.base.database.repository.CondoParkingValuationLocalRepository;
import com.gdslinkasia.base.model.details.ValrepLandimpParkingValuationDetail;

import java.util.List;

import io.reactivex.Flowable;
import io.reactivex.Single;

public class CondoParkingInteractor implements CondoParkingContract.Interactor {

    private CondoParkingValuationLocalRepository localRepository;

    public CondoParkingInteractor(Application application) {
        //same table for condo Unit Desc
        localRepository = new CondoParkingValuationLocalRepository(application);
    }

    @Override
    public Single<List<ValrepLandimpParkingValuationDetail>> getDetailsList(String recordId) {
        //same table for condo Unit Desc
        return localRepository.getDetailsList(recordId);
    }

    @Override
    public Flowable<List<ValrepLandimpParkingValuationDetail>> getListFlowable(String recordId) {
        return localRepository.getListFlowable(recordId);
    }
}
