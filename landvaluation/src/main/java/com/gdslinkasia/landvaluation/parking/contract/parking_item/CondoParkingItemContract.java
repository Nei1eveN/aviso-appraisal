package com.gdslinkasia.landvaluation.parking.contract.parking_item;

import com.gdslinkasia.base.model.details.ValrepLandimpParkingValuationDetail;

import io.reactivex.Single;

public interface CondoParkingItemContract {
    interface View {
        void showProgress(String title, String message);
        void hideProgress();
        void showDetails(ValrepLandimpParkingValuationDetail detail);
        void showExitDialog(String title, String message);
    }
    interface Presenter {
        void onStart(long uniqueId);
        void onDestroy();

        void updateItemDetails(long uniqueId,
                               String valrep_landimp_value_parking_no,
                               String valrep_landimp_value_parkinglot_area,
                               String valrep_landimp_value_parkinglot_value
        );



    }
    interface Interactor {
        Single<ValrepLandimpParkingValuationDetail> getDetails(long uniqueId);

        void updateLotDetail(ValrepLandimpParkingValuationDetail detail);
    }
}
