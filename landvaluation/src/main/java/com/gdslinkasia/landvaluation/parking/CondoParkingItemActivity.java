package com.gdslinkasia.landvaluation.parking;

import android.app.ProgressDialog;
import android.content.Intent;
import android.os.Bundle;
import android.view.MenuItem;
import android.widget.AutoCompleteTextView;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AlertDialog;
import androidx.appcompat.app.AppCompatActivity;

import com.gdslinkasia.base.model.details.ValrepLandimpParkingValuationDetail;
import com.gdslinkasia.commonresources.R2;
import com.gdslinkasia.landvaluation.R;
import com.gdslinkasia.landvaluation.parking.contract.parking_item.CondoParkingItemContract;
import com.gdslinkasia.landvaluation.parking.contract.parking_item.CondoParkingItemPresenter;

import java.util.Objects;

import butterknife.BindView;
import butterknife.ButterKnife;

import static com.gdslinkasia.base.database.DatabaseUtils.*;

public class CondoParkingItemActivity extends AppCompatActivity implements CondoParkingItemContract.View {

    public final String TAG = this.getClass().getSimpleName();

    private long uniqueId;

    @BindView(R2.id.acParkingTitle) AutoCompleteTextView acParkingTitle;
    @BindView(R2.id.acFloorArea) AutoCompleteTextView acFloorArea;
    @BindView(R2.id.acParkingTotalVal) AutoCompleteTextView acParkingTotalVal;

    private ProgressDialog progressDialog;

    private CondoParkingItemContract.Presenter presenter;
    private String appraisalType;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.content_condo_parking);
        ButterKnife.bind(this);

        Intent intent = getIntent();
        uniqueId = intent.getLongExtra(UNIQUE_ID, 0);
        appraisalType = intent.getStringExtra(APPRAISAL_TYPE);

        Objects.requireNonNull(getSupportActionBar()).setTitle("Property Details");
        getSupportActionBar().setSubtitle("Parking (Market Approach)"+uniqueId);
        getSupportActionBar().setIcon(R.drawable.ic_close_white_24dp);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);

        progressDialog = new ProgressDialog(this);
        presenter = new CondoParkingItemPresenter(this, getApplication());
    }

    @Override
    public boolean onOptionsItemSelected(@NonNull MenuItem item) {
        if (item.getItemId() == android.R.id.home) {
            finish();
        }
        return super.onOptionsItemSelected(item);
    }

    @Override
    protected void onResume() {
        super.onResume();
        presenter.onStart(uniqueId);
    }

    @Override
    protected void onPause() {
        super.onPause();
        presenter.updateItemDetails(
                uniqueId,
                Objects.requireNonNull(acParkingTitle.getText()).toString(),
                Objects.requireNonNull(acFloorArea.getText()).toString(),
                Objects.requireNonNull(acParkingTotalVal.getText()).toString()
        );


    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        presenter.onDestroy();
        progressDialog.dismiss();
    }

    @Override
    public void showProgress(String title, String message) {
        progressDialog.setCancelable(false);
        progressDialog.setIndeterminate(false);
        progressDialog.setTitle(title);
        progressDialog.setMessage(message);
        progressDialog.show();
    }

    @Override
    public void hideProgress() {
        progressDialog.hide();
    }

    @Override
    public void showDetails(ValrepLandimpParkingValuationDetail detail) {

        acParkingTitle.setText(detail.getValrepLandimpValueParkingNo());
        acFloorArea.setText(detail.getValrepLandimpValueParkinglotArea());
        acParkingTotalVal.setText(detail.getValrepLandimpValueParkinglotValue());


    }

    @Override
    public void showExitDialog(String title, String message) {
        new AlertDialog.Builder(this)
                .setCancelable(false)
                .setTitle(title)
                .setMessage(message)
                .setPositiveButton("CLOSE", (dialog, which) -> finish())
                .create()
                .show();
    }
}
