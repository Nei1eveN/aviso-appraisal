package com.gdslinkasia.landvaluation.parking.contract.parking_item;

import android.app.Application;

import com.gdslinkasia.base.database.repository.CondoParkingValuationLocalRepository;
import com.gdslinkasia.base.model.details.ValrepLandimpParkingValuationDetail;

import io.reactivex.Single;

public class CondoParkingItemInteractor implements CondoParkingItemContract.Interactor {

    private CondoParkingValuationLocalRepository localRepository;

    public CondoParkingItemInteractor(Application application) {
        this.localRepository = new CondoParkingValuationLocalRepository(application);
    }

    @Override
    public Single<ValrepLandimpParkingValuationDetail> getDetails(long uniqueId) {
        return localRepository.getDetails(uniqueId);
    }

    @Override
    public void updateLotDetail(ValrepLandimpParkingValuationDetail detail) {
        localRepository.updateLotDetail(detail);
    }
}
