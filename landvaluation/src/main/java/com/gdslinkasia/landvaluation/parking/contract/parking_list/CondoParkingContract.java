package com.gdslinkasia.landvaluation.parking.contract.parking_list;

import com.gdslinkasia.base.model.details.ValrepLandimpParkingValuationDetail;

import java.util.List;

import io.reactivex.Flowable;
import io.reactivex.Single;

public interface CondoParkingContract {

    interface View {
        void showProgress(String title, String message);

        void hideProgress();

        void showList(List<ValrepLandimpParkingValuationDetail> valrepLandimpLotDetails);

        void showEmptyState(String message);

        void showErrorDialog(String title, String message);
    }

    interface Presenter {
        void onStart(String recordId);

        void onDestroy();
    }

    interface Interactor {
        Single<List<ValrepLandimpParkingValuationDetail>> getDetailsList(String recordId);

        Flowable<List<ValrepLandimpParkingValuationDetail>> getListFlowable(String recordId);
    }

}
