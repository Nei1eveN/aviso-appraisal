package com.gdslinkasia.landvaluation;

import com.gdslinkasia.base.model.job.Record;

import io.reactivex.Single;

public interface UpdateLandValContract {
    interface View {
        void showProgress(String title, String message);
        void hideProgress();
        void showSnackMessage(String message);
        void showDetails(Record record);
        void showExitDialog(String title, String message);
        void showErrorDialog(String title, String message);
    }
    interface Presenter {
        void onStart(String recordId);
        void onDestroy();

        void updateLandVal(
                String recordId,
                String lc_valuation_key_input,
                String valrep_landimp_market_prop_market,
                /*Supply and Demand CONDO*/
                String valrep_supplyanddemand_1,
                String valrep_supplyanddemand_2,
                String valrep_supplyanddemand_3,
                String valrep_supplyanddemand_4,
                String valrep_supplyanddemand_5,
                String valrep_supplyanddemand_6,
                String valrep_supplyanddemand_7,
                String valrep_supplyanddemand_8,
                String valrep_supplyanddemand_9,
                /*Supply and Demand*/
                String valrep_landimp_supplyanddemand_1,
                String valrep_landimp_supplyanddemand_2,
                String valrep_landimp_supplyanddemand_3,
                String valrep_landimp_supplyanddemand_4,
                String valrep_landimp_supplyanddemand_5,
                String valrep_landimp_supplyanddemand_6,
                String valrep_landimp_supplyanddemand_7,
                String valrep_landimp_supplyanddemand_8,
                String valrep_landimp_supplyanddemand_9,
                String valrep_landimp_supplyanddemand_10,
                /*Market Phase*/
                String valrep_marketphase_1,
                String valrep_marketphase_2,
                String valrep_marketphase_3,
                String valrep_marketphase_4,
                String valrep_marketphase_5,
                /*Typical Buyers*/
                String valrep_typicalbuyer_1,
                /*Total Value and Nets*/
                String valrep_landimp_weighted_unit_value,
                String valrep_landimp_comp_rounded_to_nearest,
                String valrep_landimp_comp_rounded_to,
                String valrep_landimp_comp_with_comp_maps,
                /*Totality*/
                String valrep_landimp_value_total_area,
                String valrep_landimp_value_total_deduction,
                String valrep_landimp_value_total_net_area,
                String valrep_landimp_value_total_landimp_value
        );
    }
    interface Interactor {
        Single<Record> getRecordById(String recordId);
        void updateLandVal(Record record);
    }
}
