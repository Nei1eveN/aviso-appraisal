package com.gdslinkasia.landvaluation.physical_adjustment;

import android.app.Application;
import android.content.Context;
import android.os.Handler;
import android.util.Log;
import android.widget.AutoCompleteTextView;

import com.gdslinkasia.base.model.job.Record;
import com.gdslinkasia.base.utils.GlobalFunctions;
import com.gdslinkasia.landvaluation.R;

import java.util.Objects;

import io.reactivex.Single;
import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.disposables.CompositeDisposable;
import io.reactivex.observers.DisposableSingleObserver;
import io.reactivex.schedulers.Schedulers;

import static com.gdslinkasia.base.utils.GlobalFunctions.GET_CONTENT_ADAPTER;

public class UpdatePhysicalAdjustmentPresenterImpl implements UpdatePhysicalAdjustmentContract.Presenter {

    private UpdatePhysicalAdjustmentContract.View view;
    private UpdatePhysicalAdjustmentContract.Interactor interactor;

    private CompositeDisposable compositeDisposable = new CompositeDisposable();

    public UpdatePhysicalAdjustmentPresenterImpl(UpdatePhysicalAdjustmentContract.View view, Application application) {
        this.view = view;
        this.interactor = new UpdatePhysicalAdjustmentInteractorImpl(application);
    }


    @Override
    public void onResume(boolean isCalled, String recordId) {
        if (!isCalled) {
            view.showProgress("Loading Record", "Please wait...");
            compositeDisposable.add(getLocalSingleObserver(recordId).subscribeWith(getLocalObserver()));
        } else {
            Log.d("int--isCalled", "Method already called");
        }
    }

    @Override
    public void setupDropdownState(Context context, AutoCompleteTextView[] viewDropdown, AutoCompleteTextView[] viewValue) {
        //value per dropdown
        String[] contentPhysValueArray = Objects.requireNonNull(context).getResources()
                .getStringArray(R.array.physAdjustmentValueArray);

        for(int i=0; i <viewDropdown.length; i++){
            if(viewDropdown[i] != null){
                //setDropdown Display
                int finalI = i;
                //get Parent of AutoCompleteView ISA ISA TAWAG
                viewDropdown[i].setAdapter(GET_CONTENT_ADAPTER(viewDropdown[i].getText().toString(), R.array.physAdjustmentArray, Objects.requireNonNull(context)));
                viewDropdown[i].setOnClickListener(v -> { viewDropdown[finalI].showDropDown(); });
                viewDropdown[i].setKeyListener(null);
                viewValue[i].setEnabled(false);
                viewDropdown[i].setOnItemClickListener(new GlobalFunctions.AutoCompleteClickSetValueListener(viewDropdown[i], viewValue[i], contentPhysValueArray));
                viewValue[i].addTextChangedListener(new GlobalFunctions.FactorsAutoValue(viewDropdown[i], viewValue[i]));
            }
        }

/*
        viewDropdown[0].setAdapter(GET_CONTENT_ADAPTER(viewDropdown[0].getText().toString(), R.array.physAdjustmentArray, Objects.requireNonNull(context)));
        viewDropdown[0].setOnClickListener(v -> { viewDropdown[0].showDropDown(); });
        viewDropdown[0].setKeyListener(null);
        viewDropdown[0].setOnItemClickListener(new GlobalFunctions.AutoCompleteClickSetValueListener(viewDropdown[0], viewValue[0], contentPhysValueArray));
        viewValue[0].addTextChangedListener(new GlobalFunctions.FactorsAutoValue(viewDropdown[0], viewValue[0]));

    //    View 2
        viewDropdown[1].setAdapter(GET_CONTENT_ADAPTER(viewDropdown[1].getText().toString(), R.array.physAdjustmentArray, Objects.requireNonNull(context)));
        viewDropdown[1].setOnClickListener(v -> {viewDropdown[1].showDropDown(); });
        viewDropdown[1].setKeyListener(null);
        viewDropdown[1].setOnItemClickListener(new GlobalFunctions.AutoCompleteClickSetValueListener(viewDropdown[1], viewValue[1], contentPhysValueArray));
        viewValue[1].addTextChangedListener(new GlobalFunctions.FactorsAutoValue(viewDropdown[1], viewValue[1]));

    //    View 3
        viewDropdown[2].setAdapter(GET_CONTENT_ADAPTER(viewDropdown[2].getText().toString(), R.array.physAdjustmentArray, Objects.requireNonNull(context)));
        viewDropdown[2].setOnClickListener(v -> {viewDropdown[2].showDropDown(); });
        viewDropdown[2].setKeyListener(null);
        viewDropdown[2].setOnItemClickListener(new GlobalFunctions.AutoCompleteClickSetValueListener(viewDropdown[2], viewValue[2], contentPhysValueArray));
        viewValue[2].addTextChangedListener(new GlobalFunctions.FactorsAutoValue(viewDropdown[2], viewValue[2]));

     //    View 4
        viewDropdown[3].setAdapter(GET_CONTENT_ADAPTER(viewDropdown[3].getText().toString(), R.array.physAdjustmentArray, Objects.requireNonNull(context)));
        viewDropdown[3].setOnClickListener(v -> {viewDropdown[3].showDropDown(); });
        viewDropdown[3].setKeyListener(null);
        viewDropdown[3].setOnItemClickListener(new GlobalFunctions.AutoCompleteClickSetValueListener(viewDropdown[3], viewValue[3], contentPhysValueArray));
        viewValue[3].addTextChangedListener(new GlobalFunctions.FactorsAutoValue(viewDropdown[3], viewValue[3]));

     //    View 5
        viewDropdown[4].setAdapter(GET_CONTENT_ADAPTER(viewDropdown[4].getText().toString(), R.array.physAdjustmentArray, Objects.requireNonNull(context)));
        viewDropdown[4].setOnClickListener(v -> {viewDropdown[4].showDropDown(); });
        viewDropdown[4].setKeyListener(null);
        viewDropdown[4].setOnItemClickListener(new GlobalFunctions.AutoCompleteClickSetValueListener(viewDropdown[4], viewValue[4], contentPhysValueArray));
        viewValue[4].addTextChangedListener(new GlobalFunctions.FactorsAutoValue(viewDropdown[4], viewValue[4]));

    //    View 6
        viewDropdown[5].setAdapter(GET_CONTENT_ADAPTER(viewDropdown[5].getText().toString(), R.array.physAdjustmentArray, Objects.requireNonNull(context)));
        viewDropdown[5].setOnClickListener(v -> {viewDropdown[5].showDropDown(); });
        viewDropdown[5].setKeyListener(null);
        viewDropdown[5].setOnItemClickListener(new GlobalFunctions.AutoCompleteClickSetValueListener(viewDropdown[5], viewValue[5], contentPhysValueArray));
        viewValue[5].addTextChangedListener(new GlobalFunctions.FactorsAutoValue(viewDropdown[5], viewValue[5]));

    //    View 7
        viewDropdown[6].setAdapter(GET_CONTENT_ADAPTER(viewDropdown[6].getText().toString(), R.array.physAdjustmentArray, Objects.requireNonNull(context)));
        viewDropdown[6].setOnClickListener(v -> {viewDropdown[6].showDropDown(); });
        viewDropdown[6].setKeyListener(null);
        viewDropdown[6].setOnItemClickListener(new GlobalFunctions.AutoCompleteClickSetValueListener(viewDropdown[6], viewValue[6], contentPhysValueArray));
        viewValue[6].addTextChangedListener(new GlobalFunctions.FactorsAutoValue(viewDropdown[6], viewValue[6]));

    //    View 8
        viewDropdown[7].setAdapter(GET_CONTENT_ADAPTER(viewDropdown[7].getText().toString(), R.array.physAdjustmentArray, Objects.requireNonNull(context)));
        viewDropdown[7].setOnClickListener(v -> {viewDropdown[7].showDropDown(); });
        viewDropdown[7].setKeyListener(null);
        viewDropdown[7].setOnItemClickListener(new GlobalFunctions.AutoCompleteClickSetValueListener(viewDropdown[7], viewValue[7], contentPhysValueArray));
        viewValue[7].addTextChangedListener(new GlobalFunctions.FactorsAutoValue(viewDropdown[7], viewValue[7]));

    //    View 9
        viewDropdown[8].setAdapter(GET_CONTENT_ADAPTER(viewDropdown[8].getText().toString(), R.array.physAdjustmentArray, Objects.requireNonNull(context)));
        viewDropdown[8].setOnClickListener(v -> {viewDropdown[8].showDropDown(); });
        viewDropdown[8].setKeyListener(null);
        viewDropdown[8].setOnItemClickListener(new GlobalFunctions.AutoCompleteClickSetValueListener(viewDropdown[8], viewValue[8], contentPhysValueArray));
        viewValue[8].addTextChangedListener(new GlobalFunctions.FactorsAutoValue(viewDropdown[8], viewValue[8]));

    //    View 10
        viewDropdown[9].setAdapter(GET_CONTENT_ADAPTER(viewDropdown[9].getText().toString(), R.array.physAdjustmentArray, Objects.requireNonNull(context)));
        viewDropdown[9].setOnClickListener(v -> {viewDropdown[9].showDropDown(); });
        viewDropdown[9].setKeyListener(null);
        viewDropdown[9].setOnItemClickListener(new GlobalFunctions.AutoCompleteClickSetValueListener(viewDropdown[9], viewValue[9], contentPhysValueArray));
        viewValue[9].addTextChangedListener(new GlobalFunctions.FactorsAutoValue(viewDropdown[9], viewValue[9]));

    //    View 11
        viewDropdown[10].setAdapter(GET_CONTENT_ADAPTER(viewDropdown[10].getText().toString(), R.array.physAdjustmentArray, Objects.requireNonNull(context)));
        viewDropdown[10].setOnClickListener(v -> {viewDropdown[10].showDropDown(); });
        viewDropdown[10].setKeyListener(null);
        viewDropdown[10].setOnItemClickListener(new GlobalFunctions.AutoCompleteClickSetValueListener(viewDropdown[10], viewValue[10], contentPhysValueArray));
        viewValue[10].addTextChangedListener(new GlobalFunctions.FactorsAutoValue(viewDropdown[10], viewValue[10]));

    //    View 12
        viewDropdown[11].setAdapter(GET_CONTENT_ADAPTER(viewDropdown[11].getText().toString(), R.array.physAdjustmentArray, Objects.requireNonNull(context)));
        viewDropdown[11].setOnClickListener(v -> {viewDropdown[11].showDropDown(); });
        viewDropdown[11].setKeyListener(null);
        viewDropdown[11].setOnItemClickListener(new GlobalFunctions.AutoCompleteClickSetValueListener(viewDropdown[11], viewValue[11], contentPhysValueArray));
        viewValue[11].addTextChangedListener(new GlobalFunctions.FactorsAutoValue(viewDropdown[11], viewValue[11]));*/

    }


    @Override
    public void onDestroy() {
        view = null;
        compositeDisposable.clear();
    }

    @Override
    public void updatePhysicalAdjustment1(String recordId,
                                          String valrep_landimp_comp1_rec_location_type,
                                          String valrep_landimp_comp1_rec_size_type,
                                          String valrep_landimp_comp1_rec_shape_type,
                                          String valrep_landimp_comp1_rec_terrain_type,
                                          String valrep_landimp_comp1_rec_elevation_type,
                                          String valrep_landimp_comp1_rec_neighborhood_type,
                                          String valrep_landimp_comp1_rec_accessibility_type,
                                          String valrep_landimp_comp1_rec_lot_type_type,
                                          String valrep_landimp_comp1_rec_road_type,
                                          String valrep_landimp_comp1_rec_others_type,
                                          String valrep_landimp_comp1_rec_others2_type,
                                          String valrep_landimp_comp1_rec_others3_type,
                                          String valrep_landimp_comp1_rec_location_subprop,
                                          String valrep_landimp_comp1_rec_size_subprop,
                                          String valrep_landimp_comp1_rec_shape_subprop,
                                          String valrep_landimp_comp1_rec_terrain_subprop,
                                          String valrep_landimp_comp1_rec_elevation_subprop,
                                          String valrep_landimp_comp1_rec_neighborhood_subprop,
                                          String valrep_landimp_comp1_rec_accessibility_subprop,
                                          String valrep_landimp_comp1_rec_lot_type_subprop,
                                          String valrep_landimp_comp1_rec_road_subprop,
                                          String valrep_landimp_comp1_rec_others_subprop,
                                          String valrep_landimp_comp1_rec_others2_subprop,
                                          String valrep_landimp_comp1_rec_others3_subprop,

                                          String valrep_landimp_comp1_rec_location_description,
                                          String valrep_landimp_comp1_rec_size_description,
                                          String valrep_landimp_comp1_rec_shape_description,
                                          String valrep_landimp_comp1_rec_terrain_description,
                                          String valrep_landimp_comp1_rec_elevation_description,
                                          String valrep_landimp_comp1_rec_neighborhood_description,
                                          String valrep_landimp_comp1_rec_accessibility_description,
                                          String valrep_landimp_comp1_rec_lot_type_description,
                                          String valrep_landimp_comp1_rec_road_description,
                                          String valrep_landimp_comp1_rec_others_description,
                                          String valrep_landimp_comp1_rec_others2_description,
                                          String valrep_landimp_comp1_rec_others3_description,
                                          String valrep_landimp_comp1_rec_location_desc,
                                          String valrep_landimp_comp1_rec_size_desc,
                                          String valrep_landimp_comp1_rec_shape_desc,
                                          String valrep_landimp_comp1_rec_terrain_desc,
                                          String valrep_landimp_comp1_rec_elevation_desc,
                                          String valrep_landimp_comp1_rec_neighborhood_desc,
                                          String valrep_landimp_comp1_rec_accessibility_desc,
                                          String valrep_landimp_comp1_rec_lot_type_desc,
                                          String valrep_landimp_comp1_rec_road_desc,
                                          String valrep_landimp_comp1_rec_others_desc,
                                          String valrep_landimp_comp1_rec_others2_desc,
                                          String valrep_landimp_comp1_rec_others3_desc,
                                          String valrep_landimp_comp1_rec_location,
                                          String valrep_landimp_comp1_rec_size,
                                          String valrep_landimp_comp1_rec_shape,
                                          String valrep_landimp_comp1_rec_terrain,
                                          String valrep_landimp_comp1_rec_elevation,
                                          String valrep_landimp_comp1_rec_neighborhood,
                                          String valrep_landimp_comp1_rec_accessibility,
                                          String valrep_landimp_comp1_rec_lot_type,
                                          String valrep_landimp_comp1_rec_road,
                                          String valrep_landimp_comp1_rec_others,
                                          String valrep_landimp_comp1_rec_others2,
                                          String valrep_landimp_comp1_rec_others3,
                                          String valrep_landimp_comp1_totalgross_adj,
                                          String valrep_landimp_comp1_total_adj,
                                          String valrep_landimp_comp1_adjusted_value,
                                          String valrep_landimp_comp1_weight,
                                          String valrep_landimp_comp1_weight_equivalent) {

        compositeDisposable.add(getLocalSingleObserver(recordId).subscribeWith(
                getRecordUpdatePhysicalAdjustment1(
                         valrep_landimp_comp1_rec_location_type,
                         valrep_landimp_comp1_rec_size_type,
                         valrep_landimp_comp1_rec_shape_type,
                         valrep_landimp_comp1_rec_terrain_type,
                         valrep_landimp_comp1_rec_elevation_type,
                         valrep_landimp_comp1_rec_neighborhood_type,
                         valrep_landimp_comp1_rec_accessibility_type,
                         valrep_landimp_comp1_rec_lot_type_type,
                         valrep_landimp_comp1_rec_road_type,
                         valrep_landimp_comp1_rec_others_type,
                         valrep_landimp_comp1_rec_others2_type,
                         valrep_landimp_comp1_rec_others3_type,
                         valrep_landimp_comp1_rec_location_subprop,
                         valrep_landimp_comp1_rec_size_subprop,
                         valrep_landimp_comp1_rec_shape_subprop,
                         valrep_landimp_comp1_rec_terrain_subprop,
                         valrep_landimp_comp1_rec_elevation_subprop,
                         valrep_landimp_comp1_rec_neighborhood_subprop,
                         valrep_landimp_comp1_rec_accessibility_subprop,
                         valrep_landimp_comp1_rec_lot_type_subprop,
                         valrep_landimp_comp1_rec_road_subprop,
                         valrep_landimp_comp1_rec_others_subprop,
                         valrep_landimp_comp1_rec_others2_subprop,
                         valrep_landimp_comp1_rec_others3_subprop,

                         valrep_landimp_comp1_rec_location_description,
                         valrep_landimp_comp1_rec_size_description,
                         valrep_landimp_comp1_rec_shape_description,
                         valrep_landimp_comp1_rec_terrain_description,
                         valrep_landimp_comp1_rec_elevation_description,
                         valrep_landimp_comp1_rec_neighborhood_description,
                         valrep_landimp_comp1_rec_accessibility_description,
                         valrep_landimp_comp1_rec_lot_type_description,
                         valrep_landimp_comp1_rec_road_description,
                         valrep_landimp_comp1_rec_others_description,
                         valrep_landimp_comp1_rec_others2_description,
                         valrep_landimp_comp1_rec_others3_description,
                         valrep_landimp_comp1_rec_location_desc,
                         valrep_landimp_comp1_rec_size_desc,
                         valrep_landimp_comp1_rec_shape_desc,
                         valrep_landimp_comp1_rec_terrain_desc,
                         valrep_landimp_comp1_rec_elevation_desc,
                         valrep_landimp_comp1_rec_neighborhood_desc,
                         valrep_landimp_comp1_rec_accessibility_desc,
                         valrep_landimp_comp1_rec_lot_type_desc,
                         valrep_landimp_comp1_rec_road_desc,
                         valrep_landimp_comp1_rec_others_desc,
                         valrep_landimp_comp1_rec_others2_desc,
                         valrep_landimp_comp1_rec_others3_desc,
                         valrep_landimp_comp1_rec_location,
                         valrep_landimp_comp1_rec_size,
                         valrep_landimp_comp1_rec_shape,
                         valrep_landimp_comp1_rec_terrain,
                         valrep_landimp_comp1_rec_elevation,
                         valrep_landimp_comp1_rec_neighborhood,
                         valrep_landimp_comp1_rec_accessibility,
                         valrep_landimp_comp1_rec_lot_type,
                         valrep_landimp_comp1_rec_road,
                         valrep_landimp_comp1_rec_others,
                         valrep_landimp_comp1_rec_others2,
                         valrep_landimp_comp1_rec_others3,
                         valrep_landimp_comp1_totalgross_adj,
                         valrep_landimp_comp1_total_adj,
                         valrep_landimp_comp1_adjusted_value,
                         valrep_landimp_comp1_weight,
                         valrep_landimp_comp1_weight_equivalent
                )
        ));
    }

    @Override
    public void updatePhysicalAdjustment2(String recordId,
                                        /*  String valrep_landimp_Comp1_rec_location_type,
                                          String valrep_landimp_Comp1_rec_size_type,
                                          String valrep_landimp_Comp1_rec_shape_type,
                                          String valrep_landimp_Comp1_rec_terrain_type,
                                          String valrep_landimp_Comp1_rec_elevation_type,
                                          String valrep_landimp_Comp1_rec_neighborhood_type,
                                          String valrep_landimp_Comp1_rec_accessibility_type,
                                          String valrep_landimp_Comp1_rec_lot_type_type,
                                          String valrep_landimp_Comp1_rec_road_type,
                                          String valrep_landimp_Comp1_rec_others_type,
                                          String valrep_landimp_Comp1_rec_others2_type,
                                          String valrep_landimp_Comp1_rec_others3_type,
                                          String valrep_landimp_Comp1_rec_location_subprop,
                                          String valrep_landimp_Comp1_rec_size_subprop,
                                          String valrep_landimp_Comp1_rec_shape_subprop,
                                          String valrep_landimp_Comp1_rec_terrain_subprop,
                                          String valrep_landimp_Comp1_rec_elevation_subprop,
                                          String valrep_landimp_Comp1_rec_neighborhood_subprop,
                                          String valrep_landimp_Comp1_rec_accessibility_subprop,
                                          String valrep_landimp_Comp1_rec_lot_type_subprop,
                                          String valrep_landimp_Comp1_rec_road_subprop,
                                          String valrep_landimp_Comp1_rec_others_subprop,
                                          String valrep_landimp_Comp1_rec_others2_subprop,
                                          String valrep_landimp_Comp1_rec_others3_subprop,*/

                                          String valrep_landimp_comp2_rec_location_description,
                                          String valrep_landimp_comp2_rec_size_description,
                                          String valrep_landimp_comp2_rec_shape_description,
                                          String valrep_landimp_comp2_rec_terrain_description,
                                          String valrep_landimp_comp2_rec_elevation_description,
                                          String valrep_landimp_comp2_rec_neighborhood_description,
                                          String valrep_landimp_comp2_rec_accessibility_description,
                                          String valrep_landimp_comp2_rec_lot_type_description,
                                          String valrep_landimp_comp2_rec_road_description,
                                          String valrep_landimp_comp2_rec_others_description,
                                          String valrep_landimp_comp2_rec_others2_description,
                                          String valrep_landimp_comp2_rec_others3_description,
                                          String valrep_landimp_comp2_rec_location_desc,
                                          String valrep_landimp_comp2_rec_size_desc,
                                          String valrep_landimp_comp2_rec_shape_desc,
                                          String valrep_landimp_comp2_rec_terrain_desc,
                                          String valrep_landimp_comp2_rec_elevation_desc,
                                          String valrep_landimp_comp2_rec_neighborhood_desc,
                                          String valrep_landimp_comp2_rec_accessibility_desc,
                                          String valrep_landimp_comp2_rec_lot_type_desc,
                                          String valrep_landimp_comp2_rec_road_desc,
                                          String valrep_landimp_comp2_rec_others_desc,
                                          String valrep_landimp_comp2_rec_others2_desc,
                                          String valrep_landimp_comp2_rec_others3_desc,
                                          String valrep_landimp_comp2_rec_location,
                                          String valrep_landimp_comp2_rec_size,
                                          String valrep_landimp_comp2_rec_shape,
                                          String valrep_landimp_comp2_rec_terrain,
                                          String valrep_landimp_comp2_rec_elevation,
                                          String valrep_landimp_comp2_rec_neighborhood,
                                          String valrep_landimp_comp2_rec_accessibility,
                                          String valrep_landimp_comp2_rec_lot_type,
                                          String valrep_landimp_comp2_rec_road,
                                          String valrep_landimp_comp2_rec_others,
                                          String valrep_landimp_comp2_rec_others2,
                                          String valrep_landimp_comp2_rec_others3,
                                          String valrep_landimp_comp2_totalgross_adj,
                                          String valrep_landimp_comp2_total_adj,
                                          String valrep_landimp_comp2_adjusted_value,
                                          String valrep_landimp_comp2_weight,
                                          String valrep_landimp_comp2_weight_equivalent) {
        compositeDisposable.add(getLocalSingleObserver(recordId).subscribeWith(
                getRecordUpdatePhysicalAdjustment2(
                      /*   valrep_landimp_Comp1_rec_location_type,
                         valrep_landimp_Comp1_rec_size_type,
                         valrep_landimp_Comp1_rec_shape_type,
                         valrep_landimp_Comp1_rec_terrain_type,
                         valrep_landimp_Comp1_rec_elevation_type,
                         valrep_landimp_Comp1_rec_neighborhood_type,
                         valrep_landimp_Comp1_rec_accessibility_type,
                         valrep_landimp_Comp1_rec_lot_type_type,
                         valrep_landimp_Comp1_rec_road_type,
                         valrep_landimp_Comp1_rec_others_type,
                         valrep_landimp_Comp1_rec_others2_type,
                         valrep_landimp_Comp1_rec_others3_type,
                         valrep_landimp_Comp1_rec_location_subprop,
                         valrep_landimp_Comp1_rec_size_subprop,
                         valrep_landimp_Comp1_rec_shape_subprop,
                         valrep_landimp_Comp1_rec_terrain_subprop,
                         valrep_landimp_Comp1_rec_elevation_subprop,
                         valrep_landimp_Comp1_rec_neighborhood_subprop,
                         valrep_landimp_Comp1_rec_accessibility_subprop,
                         valrep_landimp_Comp1_rec_lot_type_subprop,
                         valrep_landimp_Comp1_rec_road_subprop,
                         valrep_landimp_Comp1_rec_others_subprop,
                         valrep_landimp_Comp1_rec_others2_subprop,
                         valrep_landimp_Comp1_rec_others3_subprop,*/

                         valrep_landimp_comp2_rec_location_description,
                         valrep_landimp_comp2_rec_size_description,
                         valrep_landimp_comp2_rec_shape_description,
                         valrep_landimp_comp2_rec_terrain_description,
                         valrep_landimp_comp2_rec_elevation_description,
                         valrep_landimp_comp2_rec_neighborhood_description,
                         valrep_landimp_comp2_rec_accessibility_description,
                         valrep_landimp_comp2_rec_lot_type_description,
                         valrep_landimp_comp2_rec_road_description,
                         valrep_landimp_comp2_rec_others_description,
                         valrep_landimp_comp2_rec_others2_description,
                         valrep_landimp_comp2_rec_others3_description,
                         valrep_landimp_comp2_rec_location_desc,
                         valrep_landimp_comp2_rec_size_desc,
                         valrep_landimp_comp2_rec_shape_desc,
                         valrep_landimp_comp2_rec_terrain_desc,
                         valrep_landimp_comp2_rec_elevation_desc,
                         valrep_landimp_comp2_rec_neighborhood_desc,
                         valrep_landimp_comp2_rec_accessibility_desc,
                         valrep_landimp_comp2_rec_lot_type_desc,
                         valrep_landimp_comp2_rec_road_desc,
                         valrep_landimp_comp2_rec_others_desc,
                         valrep_landimp_comp2_rec_others2_desc,
                         valrep_landimp_comp2_rec_others3_desc,
                         valrep_landimp_comp2_rec_location,
                         valrep_landimp_comp2_rec_size,
                         valrep_landimp_comp2_rec_shape,
                         valrep_landimp_comp2_rec_terrain,
                         valrep_landimp_comp2_rec_elevation,
                         valrep_landimp_comp2_rec_neighborhood,
                         valrep_landimp_comp2_rec_accessibility,
                         valrep_landimp_comp2_rec_lot_type,
                         valrep_landimp_comp2_rec_road,
                         valrep_landimp_comp2_rec_others,
                         valrep_landimp_comp2_rec_others2,
                         valrep_landimp_comp2_rec_others3,
                         valrep_landimp_comp2_totalgross_adj,
                         valrep_landimp_comp2_total_adj,
                         valrep_landimp_comp2_adjusted_value,
                         valrep_landimp_comp2_weight,
                         valrep_landimp_comp2_weight_equivalent
                )
        ));
    }

    @Override
    public void updatePhysicalAdjustment3(String recordId,
                                        /*  String valrep_landimp_Comp1_rec_location_type,
                                          String valrep_landimp_Comp1_rec_size_type,
                                          String valrep_landimp_Comp1_rec_shape_type,
                                          String valrep_landimp_Comp1_rec_terrain_type,
                                          String valrep_landimp_Comp1_rec_elevation_type,
                                          String valrep_landimp_Comp1_rec_neighborhood_type,
                                          String valrep_landimp_Comp1_rec_accessibility_type,
                                          String valrep_landimp_Comp1_rec_lot_type_type,
                                          String valrep_landimp_Comp1_rec_road_type,
                                          String valrep_landimp_Comp1_rec_others_type,
                                          String valrep_landimp_Comp1_rec_others2_type,
                                          String valrep_landimp_Comp1_rec_others3_type,
                                          String valrep_landimp_Comp1_rec_location_subprop,
                                          String valrep_landimp_Comp1_rec_size_subprop,
                                          String valrep_landimp_Comp1_rec_shape_subprop,
                                          String valrep_landimp_Comp1_rec_terrain_subprop,
                                          String valrep_landimp_Comp1_rec_elevation_subprop,
                                          String valrep_landimp_Comp1_rec_neighborhood_subprop,
                                          String valrep_landimp_Comp1_rec_accessibility_subprop,
                                          String valrep_landimp_Comp1_rec_lot_type_subprop,
                                          String valrep_landimp_Comp1_rec_road_subprop,
                                          String valrep_landimp_Comp1_rec_others_subprop,
                                          String valrep_landimp_Comp1_rec_others2_subprop,
                                          String valrep_landimp_Comp1_rec_others3_subprop,*/

                                          String valrep_landimp_comp3_rec_location_description,
                                          String valrep_landimp_comp3_rec_size_description,
                                          String valrep_landimp_comp3_rec_shape_description,
                                          String valrep_landimp_comp3_rec_terrain_description,
                                          String valrep_landimp_comp3_rec_elevation_description,
                                          String valrep_landimp_comp3_rec_neighborhood_description,
                                          String valrep_landimp_comp3_rec_accessibility_description,
                                          String valrep_landimp_comp3_rec_lot_type_description,
                                          String valrep_landimp_comp3_rec_road_description,
                                          String valrep_landimp_comp3_rec_others_description,
                                          String valrep_landimp_comp3_rec_others2_description,
                                          String valrep_landimp_comp3_rec_others3_description,
                                          String valrep_landimp_comp3_rec_location_desc,
                                          String valrep_landimp_comp3_rec_size_desc,
                                          String valrep_landimp_comp3_rec_shape_desc,
                                          String valrep_landimp_comp3_rec_terrain_desc,
                                          String valrep_landimp_comp3_rec_elevation_desc,
                                          String valrep_landimp_comp3_rec_neighborhood_desc,
                                          String valrep_landimp_comp3_rec_accessibility_desc,
                                          String valrep_landimp_comp3_rec_lot_type_desc,
                                          String valrep_landimp_comp3_rec_road_desc,
                                          String valrep_landimp_comp3_rec_others_desc,
                                          String valrep_landimp_comp3_rec_others2_desc,
                                          String valrep_landimp_comp3_rec_others3_desc,
                                          String valrep_landimp_comp3_rec_location,
                                          String valrep_landimp_comp3_rec_size,
                                          String valrep_landimp_comp3_rec_shape,
                                          String valrep_landimp_comp3_rec_terrain,
                                          String valrep_landimp_comp3_rec_elevation,
                                          String valrep_landimp_comp3_rec_neighborhood,
                                          String valrep_landimp_comp3_rec_accessibility,
                                          String valrep_landimp_comp3_rec_lot_type,
                                          String valrep_landimp_comp3_rec_road,
                                          String valrep_landimp_comp3_rec_others,
                                          String valrep_landimp_comp3_rec_others2,
                                          String valrep_landimp_comp3_rec_others3,
                                          String valrep_landimp_comp3_totalgross_adj,
                                          String valrep_landimp_comp3_total_adj,
                                          String valrep_landimp_comp3_adjusted_value,
                                          String valrep_landimp_comp3_weight,
                                          String valrep_landimp_comp3_weight_equivalent) {
        compositeDisposable.add(getLocalSingleObserver(recordId).subscribeWith(
                getRecordUpdatePhysicalAdjustment3(
                    /*    valrep_landimp_Comp1_rec_location_type,
                        valrep_landimp_Comp1_rec_size_type,
                        valrep_landimp_Comp1_rec_shape_type,
                        valrep_landimp_Comp1_rec_terrain_type,
                        valrep_landimp_Comp1_rec_elevation_type,
                        valrep_landimp_Comp1_rec_neighborhood_type,
                        valrep_landimp_Comp1_rec_accessibility_type,
                        valrep_landimp_Comp1_rec_lot_type_type,
                        valrep_landimp_Comp1_rec_road_type,
                        valrep_landimp_Comp1_rec_others_type,
                        valrep_landimp_Comp1_rec_others2_type,
                        valrep_landimp_Comp1_rec_others3_type,
                        valrep_landimp_Comp1_rec_location_subprop,
                        valrep_landimp_Comp1_rec_size_subprop,
                        valrep_landimp_Comp1_rec_shape_subprop,
                        valrep_landimp_Comp1_rec_terrain_subprop,
                        valrep_landimp_Comp1_rec_elevation_subprop,
                        valrep_landimp_Comp1_rec_neighborhood_subprop,
                        valrep_landimp_Comp1_rec_accessibility_subprop,
                        valrep_landimp_Comp1_rec_lot_type_subprop,
                        valrep_landimp_Comp1_rec_road_subprop,
                        valrep_landimp_Comp1_rec_others_subprop,
                        valrep_landimp_Comp1_rec_others2_subprop,
                        valrep_landimp_Comp1_rec_others3_subprop,*/

                        valrep_landimp_comp3_rec_location_description,
                        valrep_landimp_comp3_rec_size_description,
                        valrep_landimp_comp3_rec_shape_description,
                        valrep_landimp_comp3_rec_terrain_description,
                        valrep_landimp_comp3_rec_elevation_description,
                        valrep_landimp_comp3_rec_neighborhood_description,
                        valrep_landimp_comp3_rec_accessibility_description,
                        valrep_landimp_comp3_rec_lot_type_description,
                        valrep_landimp_comp3_rec_road_description,
                        valrep_landimp_comp3_rec_others_description,
                        valrep_landimp_comp3_rec_others2_description,
                        valrep_landimp_comp3_rec_others3_description,
                        valrep_landimp_comp3_rec_location_desc,
                        valrep_landimp_comp3_rec_size_desc,
                        valrep_landimp_comp3_rec_shape_desc,
                        valrep_landimp_comp3_rec_terrain_desc,
                        valrep_landimp_comp3_rec_elevation_desc,
                        valrep_landimp_comp3_rec_neighborhood_desc,
                        valrep_landimp_comp3_rec_accessibility_desc,
                        valrep_landimp_comp3_rec_lot_type_desc,
                        valrep_landimp_comp3_rec_road_desc,
                        valrep_landimp_comp3_rec_others_desc,
                        valrep_landimp_comp3_rec_others2_desc,
                        valrep_landimp_comp3_rec_others3_desc,
                        valrep_landimp_comp3_rec_location,
                        valrep_landimp_comp3_rec_size,
                        valrep_landimp_comp3_rec_shape,
                        valrep_landimp_comp3_rec_terrain,
                        valrep_landimp_comp3_rec_elevation,
                        valrep_landimp_comp3_rec_neighborhood,
                        valrep_landimp_comp3_rec_accessibility,
                        valrep_landimp_comp3_rec_lot_type,
                        valrep_landimp_comp3_rec_road,
                        valrep_landimp_comp3_rec_others,
                        valrep_landimp_comp3_rec_others2,
                        valrep_landimp_comp3_rec_others3,
                        valrep_landimp_comp3_totalgross_adj,
                        valrep_landimp_comp3_total_adj,
                        valrep_landimp_comp3_adjusted_value,
                        valrep_landimp_comp3_weight,
                        valrep_landimp_comp3_weight_equivalent
                )
        ));
    }

    @Override
    public void updatePhysicalAdjustment4(String recordId,
                                    /*       String valrep_landimp_Comp1_rec_location_type,
                                         String valrep_landimp_Comp1_rec_size_type,
                                          String valrep_landimp_Comp1_rec_shape_type,
                                          String valrep_landimp_Comp1_rec_terrain_type,
                                          String valrep_landimp_Comp1_rec_elevation_type,
                                          String valrep_landimp_Comp1_rec_neighborhood_type,
                                          String valrep_landimp_Comp1_rec_accessibility_type,
                                          String valrep_landimp_Comp1_rec_lot_type_type,
                                          String valrep_landimp_Comp1_rec_road_type,
                                          String valrep_landimp_Comp1_rec_others_type,
                                          String valrep_landimp_Comp1_rec_others2_type,
                                          String valrep_landimp_Comp1_rec_others3_type,
                                          String valrep_landimp_Comp1_rec_location_subprop,
                                          String valrep_landimp_Comp1_rec_size_subprop,
                                          String valrep_landimp_Comp1_rec_shape_subprop,
                                          String valrep_landimp_Comp1_rec_terrain_subprop,
                                          String valrep_landimp_Comp1_rec_elevation_subprop,
                                          String valrep_landimp_Comp1_rec_neighborhood_subprop,
                                          String valrep_landimp_Comp1_rec_accessibility_subprop,
                                          String valrep_landimp_Comp1_rec_lot_type_subprop,
                                          String valrep_landimp_Comp1_rec_road_subprop,
                                          String valrep_landimp_Comp1_rec_others_subprop,
                                          String valrep_landimp_Comp1_rec_others2_subprop,
                                          String valrep_landimp_Comp1_rec_others3_subprop,*/

                                          String valrep_landimp_comp4_rec_location_description,
                                          String valrep_landimp_comp4_rec_size_description,
                                          String valrep_landimp_comp4_rec_shape_description,
                                          String valrep_landimp_comp4_rec_terrain_description,
                                          String valrep_landimp_comp4_rec_elevation_description,
                                          String valrep_landimp_comp4_rec_neighborhood_description,
                                          String valrep_landimp_comp4_rec_accessibility_description,
                                          String valrep_landimp_comp4_rec_lot_type_description,
                                          String valrep_landimp_comp4_rec_road_description,
                                          String valrep_landimp_comp4_rec_others_description,
                                          String valrep_landimp_comp4_rec_others2_description,
                                          String valrep_landimp_comp4_rec_others3_description,
                                          String valrep_landimp_comp4_rec_location_desc,
                                          String valrep_landimp_comp4_rec_size_desc,
                                          String valrep_landimp_comp4_rec_shape_desc,
                                          String valrep_landimp_comp4_rec_terrain_desc,
                                          String valrep_landimp_comp4_rec_elevation_desc,
                                          String valrep_landimp_comp4_rec_neighborhood_desc,
                                          String valrep_landimp_comp4_rec_accessibility_desc,
                                          String valrep_landimp_comp4_rec_lot_type_desc,
                                          String valrep_landimp_comp4_rec_road_desc,
                                          String valrep_landimp_comp4_rec_others_desc,
                                          String valrep_landimp_comp4_rec_others2_desc,
                                          String valrep_landimp_comp4_rec_others3_desc,
                                          String valrep_landimp_comp4_rec_location,
                                          String valrep_landimp_comp4_rec_size,
                                          String valrep_landimp_comp4_rec_shape,
                                          String valrep_landimp_comp4_rec_terrain,
                                          String valrep_landimp_comp4_rec_elevation,
                                          String valrep_landimp_comp4_rec_neighborhood,
                                          String valrep_landimp_comp4_rec_accessibility,
                                          String valrep_landimp_comp4_rec_lot_type,
                                          String valrep_landimp_comp4_rec_road,
                                          String valrep_landimp_comp4_rec_others,
                                          String valrep_landimp_comp4_rec_others2,
                                          String valrep_landimp_comp4_rec_others3,
                                          String valrep_landimp_comp4_totalgross_adj,
                                          String valrep_landimp_comp4_total_adj,
                                          String valrep_landimp_comp4_adjusted_value,
                                          String valrep_landimp_comp4_weight,
                                          String valrep_landimp_comp4_weight_equivalent) {
        compositeDisposable.add(getLocalSingleObserver(recordId).subscribeWith(
                getRecordUpdatePhysicalAdjustment4(
                     /*   valrep_landimp_Comp1_rec_location_type,
                        valrep_landimp_Comp1_rec_size_type,
                        valrep_landimp_Comp1_rec_shape_type,
                        valrep_landimp_Comp1_rec_terrain_type,
                        valrep_landimp_Comp1_rec_elevation_type,
                        valrep_landimp_Comp1_rec_neighborhood_type,
                        valrep_landimp_Comp1_rec_accessibility_type,
                        valrep_landimp_Comp1_rec_lot_type_type,
                        valrep_landimp_Comp1_rec_road_type,
                        valrep_landimp_Comp1_rec_others_type,
                        valrep_landimp_Comp1_rec_others2_type,
                        valrep_landimp_Comp1_rec_others3_type,
                        valrep_landimp_Comp1_rec_location_subprop,
                        valrep_landimp_Comp1_rec_size_subprop,
                        valrep_landimp_Comp1_rec_shape_subprop,
                        valrep_landimp_Comp1_rec_terrain_subprop,
                        valrep_landimp_Comp1_rec_elevation_subprop,
                        valrep_landimp_Comp1_rec_neighborhood_subprop,
                        valrep_landimp_Comp1_rec_accessibility_subprop,
                        valrep_landimp_Comp1_rec_lot_type_subprop,
                        valrep_landimp_Comp1_rec_road_subprop,
                        valrep_landimp_Comp1_rec_others_subprop,
                        valrep_landimp_Comp1_rec_others2_subprop,
                        valrep_landimp_Comp1_rec_others3_subprop,*/

                        valrep_landimp_comp4_rec_location_description,
                        valrep_landimp_comp4_rec_size_description,
                        valrep_landimp_comp4_rec_shape_description,
                        valrep_landimp_comp4_rec_terrain_description,
                        valrep_landimp_comp4_rec_elevation_description,
                        valrep_landimp_comp4_rec_neighborhood_description,
                        valrep_landimp_comp4_rec_accessibility_description,
                        valrep_landimp_comp4_rec_lot_type_description,
                        valrep_landimp_comp4_rec_road_description,
                        valrep_landimp_comp4_rec_others_description,
                        valrep_landimp_comp4_rec_others2_description,
                        valrep_landimp_comp4_rec_others3_description,
                        valrep_landimp_comp4_rec_location_desc,
                        valrep_landimp_comp4_rec_size_desc,
                        valrep_landimp_comp4_rec_shape_desc,
                        valrep_landimp_comp4_rec_terrain_desc,
                        valrep_landimp_comp4_rec_elevation_desc,
                        valrep_landimp_comp4_rec_neighborhood_desc,
                        valrep_landimp_comp4_rec_accessibility_desc,
                        valrep_landimp_comp4_rec_lot_type_desc,
                        valrep_landimp_comp4_rec_road_desc,
                        valrep_landimp_comp4_rec_others_desc,
                        valrep_landimp_comp4_rec_others2_desc,
                        valrep_landimp_comp4_rec_others3_desc,
                        valrep_landimp_comp4_rec_location,
                        valrep_landimp_comp4_rec_size,
                        valrep_landimp_comp4_rec_shape,
                        valrep_landimp_comp4_rec_terrain,
                        valrep_landimp_comp4_rec_elevation,
                        valrep_landimp_comp4_rec_neighborhood,
                        valrep_landimp_comp4_rec_accessibility,
                        valrep_landimp_comp4_rec_lot_type,
                        valrep_landimp_comp4_rec_road,
                        valrep_landimp_comp4_rec_others,
                        valrep_landimp_comp4_rec_others2,
                        valrep_landimp_comp4_rec_others3,
                        valrep_landimp_comp4_totalgross_adj,
                        valrep_landimp_comp4_total_adj,
                        valrep_landimp_comp4_adjusted_value,
                        valrep_landimp_comp4_weight,
                        valrep_landimp_comp4_weight_equivalent
                )
        ));
    }

    @Override
    public void updatePhysicalAdjustment5(String recordId,
                                         /* String valrep_landimp_Comp1_rec_location_type,
                                          String valrep_landimp_Comp1_rec_size_type,
                                          String valrep_landimp_Comp1_rec_shape_type,
                                          String valrep_landimp_Comp1_rec_terrain_type,
                                          String valrep_landimp_Comp1_rec_elevation_type,
                                          String valrep_landimp_Comp1_rec_neighborhood_type,
                                          String valrep_landimp_Comp1_rec_accessibility_type,
                                          String valrep_landimp_Comp1_rec_lot_type_type,
                                          String valrep_landimp_Comp1_rec_road_type,
                                          String valrep_landimp_Comp1_rec_others_type,
                                          String valrep_landimp_Comp1_rec_others2_type,
                                          String valrep_landimp_Comp1_rec_others3_type,
                                          String valrep_landimp_Comp1_rec_location_subprop,
                                          String valrep_landimp_Comp1_rec_size_subprop,
                                          String valrep_landimp_Comp1_rec_shape_subprop,
                                          String valrep_landimp_Comp1_rec_terrain_subprop,
                                          String valrep_landimp_Comp1_rec_elevation_subprop,
                                          String valrep_landimp_Comp1_rec_neighborhood_subprop,
                                          String valrep_landimp_Comp1_rec_accessibility_subprop,
                                          String valrep_landimp_Comp1_rec_lot_type_subprop,
                                          String valrep_landimp_Comp1_rec_road_subprop,
                                          String valrep_landimp_Comp1_rec_others_subprop,
                                          String valrep_landimp_Comp1_rec_others2_subprop,
                                          String valrep_landimp_Comp1_rec_others3_subprop,*/

                                          String valrep_landimp_comp5_rec_location_description,
                                          String valrep_landimp_comp5_rec_size_description,
                                          String valrep_landimp_comp5_rec_shape_description,
                                          String valrep_landimp_comp5_rec_terrain_description,
                                          String valrep_landimp_comp5_rec_elevation_description,
                                          String valrep_landimp_comp5_rec_neighborhood_description,
                                          String valrep_landimp_comp5_rec_accessibility_description,
                                          String valrep_landimp_comp5_rec_lot_type_description,
                                          String valrep_landimp_comp5_rec_road_description,
                                          String valrep_landimp_comp5_rec_others_description,
                                          String valrep_landimp_comp5_rec_others2_description,
                                          String valrep_landimp_comp5_rec_others3_description,
                                          String valrep_landimp_comp5_rec_location_desc,
                                          String valrep_landimp_comp5_rec_size_desc,
                                          String valrep_landimp_comp5_rec_shape_desc,
                                          String valrep_landimp_comp5_rec_terrain_desc,
                                          String valrep_landimp_comp5_rec_elevation_desc,
                                          String valrep_landimp_comp5_rec_neighborhood_desc,
                                          String valrep_landimp_comp5_rec_accessibility_desc,
                                          String valrep_landimp_comp5_rec_lot_type_desc,
                                          String valrep_landimp_comp5_rec_road_desc,
                                          String valrep_landimp_comp5_rec_others_desc,
                                          String valrep_landimp_comp5_rec_others2_desc,
                                          String valrep_landimp_comp5_rec_others3_desc,
                                          String valrep_landimp_comp5_rec_location,
                                          String valrep_landimp_comp5_rec_size,
                                          String valrep_landimp_comp5_rec_shape,
                                          String valrep_landimp_comp5_rec_terrain,
                                          String valrep_landimp_comp5_rec_elevation,
                                          String valrep_landimp_comp5_rec_neighborhood,
                                          String valrep_landimp_comp5_rec_accessibility,
                                          String valrep_landimp_comp5_rec_lot_type,
                                          String valrep_landimp_comp5_rec_road,
                                          String valrep_landimp_comp5_rec_others,
                                          String valrep_landimp_comp5_rec_others2,
                                          String valrep_landimp_comp5_rec_others3,
                                          String valrep_landimp_comp5_totalgross_adj,
                                          String valrep_landimp_comp5_total_adj,
                                          String valrep_landimp_comp5_adjusted_value,
                                          String valrep_landimp_comp5_weight,
                                          String valrep_landimp_comp5_weight_equivalent) {
        compositeDisposable.add(getLocalSingleObserver(recordId).subscribeWith(
                getRecordUpdatePhysicalAdjustment5(
                      /*  valrep_landimp_Comp1_rec_location_type,
                        valrep_landimp_Comp1_rec_size_type,
                        valrep_landimp_Comp1_rec_shape_type,
                        valrep_landimp_Comp1_rec_terrain_type,
                        valrep_landimp_Comp1_rec_elevation_type,
                        valrep_landimp_Comp1_rec_neighborhood_type,
                        valrep_landimp_Comp1_rec_accessibility_type,
                        valrep_landimp_Comp1_rec_lot_type_type,
                        valrep_landimp_Comp1_rec_road_type,
                        valrep_landimp_Comp1_rec_others_type,
                        valrep_landimp_Comp1_rec_others2_type,
                        valrep_landimp_Comp1_rec_others3_type,
                        valrep_landimp_Comp1_rec_location_subprop,
                        valrep_landimp_Comp1_rec_size_subprop,
                        valrep_landimp_Comp1_rec_shape_subprop,
                        valrep_landimp_Comp1_rec_terrain_subprop,
                        valrep_landimp_Comp1_rec_elevation_subprop,
                        valrep_landimp_Comp1_rec_neighborhood_subprop,
                        valrep_landimp_Comp1_rec_accessibility_subprop,
                        valrep_landimp_Comp1_rec_lot_type_subprop,
                        valrep_landimp_Comp1_rec_road_subprop,
                        valrep_landimp_Comp1_rec_others_subprop,
                        valrep_landimp_Comp1_rec_others2_subprop,
                        valrep_landimp_Comp1_rec_others3_subprop,*/

                        valrep_landimp_comp5_rec_location_description,
                        valrep_landimp_comp5_rec_size_description,
                        valrep_landimp_comp5_rec_shape_description,
                        valrep_landimp_comp5_rec_terrain_description,
                        valrep_landimp_comp5_rec_elevation_description,
                        valrep_landimp_comp5_rec_neighborhood_description,
                        valrep_landimp_comp5_rec_accessibility_description,
                        valrep_landimp_comp5_rec_lot_type_description,
                        valrep_landimp_comp5_rec_road_description,
                        valrep_landimp_comp5_rec_others_description,
                        valrep_landimp_comp5_rec_others2_description,
                        valrep_landimp_comp5_rec_others3_description,
                        valrep_landimp_comp5_rec_location_desc,
                        valrep_landimp_comp5_rec_size_desc,
                        valrep_landimp_comp5_rec_shape_desc,
                        valrep_landimp_comp5_rec_terrain_desc,
                        valrep_landimp_comp5_rec_elevation_desc,
                        valrep_landimp_comp5_rec_neighborhood_desc,
                        valrep_landimp_comp5_rec_accessibility_desc,
                        valrep_landimp_comp5_rec_lot_type_desc,
                        valrep_landimp_comp5_rec_road_desc,
                        valrep_landimp_comp5_rec_others_desc,
                        valrep_landimp_comp5_rec_others2_desc,
                        valrep_landimp_comp5_rec_others3_desc,
                        valrep_landimp_comp5_rec_location,
                        valrep_landimp_comp5_rec_size,
                        valrep_landimp_comp5_rec_shape,
                        valrep_landimp_comp5_rec_terrain,
                        valrep_landimp_comp5_rec_elevation,
                        valrep_landimp_comp5_rec_neighborhood,
                        valrep_landimp_comp5_rec_accessibility,
                        valrep_landimp_comp5_rec_lot_type,
                        valrep_landimp_comp5_rec_road,
                        valrep_landimp_comp5_rec_others,
                        valrep_landimp_comp5_rec_others2,
                        valrep_landimp_comp5_rec_others3,
                        valrep_landimp_comp5_totalgross_adj,
                        valrep_landimp_comp5_total_adj,
                        valrep_landimp_comp5_adjusted_value,
                        valrep_landimp_comp5_weight,
                        valrep_landimp_comp5_weight_equivalent
                )
        ));
    }


    //TODO Single disposable for fetching 1 Record from database based on recordId
    private Single<Record> getLocalSingleObserver(String recordId) {
        return interactor.getRecordById(recordId)
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread());
    }

    //TODO Results from disposable will be fetched here
    private DisposableSingleObserver<Record> getLocalObserver() {
        return new DisposableSingleObserver<Record>() {
            @Override
            public void onSuccess(Record record) {
                Log.d("int--LocalRepo", "Logic Triggered: UpdateAppraisalDetailsPresenter");
                view.showDetails(record);
                view.showSnackMessage("Loading Successful");
                view.hideProgress();
                new Handler().postDelayed(() -> view.hideProgress(), 1000);
            }

            @Override
            public void onError(Throwable e) {
                view.hideProgress();
                view.showExitDialog("Record Error", e.getMessage());
            }
        };
    }

    //TODO Update Record Physical Adjustment 1
    private DisposableSingleObserver<Record> getRecordUpdatePhysicalAdjustment1(
            String valrep_landimp_comp1_rec_location_type,
            String valrep_landimp_comp1_rec_size_type,
            String valrep_landimp_comp1_rec_shape_type,
            String valrep_landimp_comp1_rec_terrain_type,
            String valrep_landimp_comp1_rec_elevation_type,
            String valrep_landimp_comp1_rec_neighborhood_type,
            String valrep_landimp_comp1_rec_accessibility_type,
            String valrep_landimp_comp1_rec_lot_type_type,
            String valrep_landimp_comp1_rec_road_type,
            String valrep_landimp_comp1_rec_others_type,
            String valrep_landimp_comp1_rec_others2_type,
            String valrep_landimp_comp1_rec_others3_type,
            String valrep_landimp_comp1_rec_location_subprop,
            String valrep_landimp_comp1_rec_size_subprop,
            String valrep_landimp_comp1_rec_shape_subprop,
            String valrep_landimp_comp1_rec_terrain_subprop,
            String valrep_landimp_comp1_rec_elevation_subprop,
            String valrep_landimp_comp1_rec_neighborhood_subprop,
            String valrep_landimp_comp1_rec_accessibility_subprop,
            String valrep_landimp_comp1_rec_lot_type_subprop,
            String valrep_landimp_comp1_rec_road_subprop,
            String valrep_landimp_comp1_rec_others_subprop,
            String valrep_landimp_comp1_rec_others2_subprop,
            String valrep_landimp_comp1_rec_others3_subprop,
            String valrep_landimp_comp1_rec_location_description,
            String valrep_landimp_comp1_rec_size_description,
            String valrep_landimp_comp1_rec_shape_description,
            String valrep_landimp_comp1_rec_terrain_description,
            String valrep_landimp_comp1_rec_elevation_description,
            String valrep_landimp_comp1_rec_neighborhood_description,
            String valrep_landimp_comp1_rec_accessibility_description,
            String valrep_landimp_comp1_rec_lot_type_description,
            String valrep_landimp_comp1_rec_road_description,
            String valrep_landimp_comp1_rec_others_description,
            String valrep_landimp_comp1_rec_others2_description,
            String valrep_landimp_comp1_rec_others3_description,
            String valrep_landimp_comp1_rec_location_desc,
            String valrep_landimp_comp1_rec_size_desc,
            String valrep_landimp_comp1_rec_shape_desc,
            String valrep_landimp_comp1_rec_terrain_desc,
            String valrep_landimp_comp1_rec_elevation_desc,
            String valrep_landimp_comp1_rec_neighborhood_desc,
            String valrep_landimp_comp1_rec_accessibility_desc,
            String valrep_landimp_comp1_rec_lot_type_desc,
            String valrep_landimp_comp1_rec_road_desc,
            String valrep_landimp_comp1_rec_others_desc,
            String valrep_landimp_comp1_rec_others2_desc,
            String valrep_landimp_comp1_rec_others3_desc,
            String valrep_landimp_comp1_rec_location,
            String valrep_landimp_comp1_rec_size,
            String valrep_landimp_comp1_rec_shape,
            String valrep_landimp_comp1_rec_terrain,
            String valrep_landimp_comp1_rec_elevation,
            String valrep_landimp_comp1_rec_neighborhood,
            String valrep_landimp_comp1_rec_accessibility,
            String valrep_landimp_comp1_rec_lot_type,
            String valrep_landimp_comp1_rec_road,
            String valrep_landimp_comp1_rec_others,
            String valrep_landimp_comp1_rec_others2,
            String valrep_landimp_comp1_rec_others3,
            String valrep_landimp_comp1_totalgross_adj,
            String valrep_landimp_comp1_total_adj,
            String valrep_landimp_comp1_adjusted_value,
            String valrep_landimp_comp1_weight,
            String valrep_landimp_comp1_weight_equivalent){
        return new DisposableSingleObserver<Record>() {
            @Override
            public void onSuccess(Record record) {
                //Fix Value
                record.setValrepLandimpComp1RecLocationType(valrep_landimp_comp1_rec_location_type);
                record.setValrepLandimpComp1RecSizeType(valrep_landimp_comp1_rec_size_type);
                record.setValrepLandimpComp1RecShapeType(valrep_landimp_comp1_rec_shape_type);
                record.setValrepLandimpComp1RecTerrainType(valrep_landimp_comp1_rec_terrain_type);
                record.setValrepLandimpComp1RecElevationType(valrep_landimp_comp1_rec_elevation_type);
                record.setValrepLandimpComp1RecNeighborhoodType(valrep_landimp_comp1_rec_neighborhood_type);
                record.setValrepLandimpComp1RecAccessibilityType(valrep_landimp_comp1_rec_accessibility_type);
                record.setValrepLandimpComp1RecLotTypeType(valrep_landimp_comp1_rec_lot_type_type);
                record.setValrepLandimpComp1RecRoadType(valrep_landimp_comp1_rec_road_type);
                record.setValrepLandimpComp1RecOthersType(valrep_landimp_comp1_rec_others_type);
                record.setValrepLandimpComp1RecOthers2Type(valrep_landimp_comp1_rec_others2_type);
                record.setValrepLandimpComp1RecOthers3Type(valrep_landimp_comp1_rec_others3_type);
                record.setValrepLandimpComp1RecLocationSubprop(valrep_landimp_comp1_rec_location_subprop);
                record.setValrepLandimpComp1RecSizeSubprop(valrep_landimp_comp1_rec_size_subprop);
                record.setValrepLandimpComp1RecShapeSubprop(valrep_landimp_comp1_rec_shape_subprop);
                record.setValrepLandimpComp1RecTerrainSubprop(valrep_landimp_comp1_rec_terrain_subprop);
                record.setValrepLandimpComp1RecElevationSubprop(valrep_landimp_comp1_rec_elevation_subprop);
                record.setValrepLandimpComp1RecNeighborhoodSubprop(valrep_landimp_comp1_rec_neighborhood_subprop);
                record.setValrepLandimpComp1RecAccessibilitySubprop(valrep_landimp_comp1_rec_accessibility_subprop);
                record.setValrepLandimpComp1RecLotTypeSubprop(valrep_landimp_comp1_rec_lot_type_subprop);
                record.setValrepLandimpComp1RecRoadSubprop(valrep_landimp_comp1_rec_road_subprop);
                record.setValrepLandimpComp1RecOthersSubprop(valrep_landimp_comp1_rec_others_subprop);
                record.setValrepLandimpComp1RecOthers2Subprop(valrep_landimp_comp1_rec_others2_subprop);
                record.setValrepLandimpComp1RecOthers3Subprop(valrep_landimp_comp1_rec_others3_subprop);

                record.setValrepLandimpComp1RecLocationDescription(valrep_landimp_comp1_rec_location_description);
                record.setValrepLandimpComp1RecSizeDescription(valrep_landimp_comp1_rec_size_description);
                record.setValrepLandimpComp1RecShapeDescription(valrep_landimp_comp1_rec_shape_description);
                record.setValrepLandimpComp1RecTerrainDescription(valrep_landimp_comp1_rec_terrain_description);
                record.setValrepLandimpComp1RecElevationDescription(valrep_landimp_comp1_rec_elevation_description);
                record.setValrepLandimpComp1RecNeighborhoodDescription(valrep_landimp_comp1_rec_neighborhood_description);
                record.setValrepLandimpComp1RecAccessibilityDescription(valrep_landimp_comp1_rec_accessibility_description);
                record.setValrepLandimpComp1RecLotTypeDescription(valrep_landimp_comp1_rec_lot_type_description);
                record.setValrepLandimpComp1RecRoadDescription(valrep_landimp_comp1_rec_road_description);
                record.setValrepLandimpComp1RecOthersDescription(valrep_landimp_comp1_rec_others_description);
                record.setValrepLandimpComp1RecOthers2Description(valrep_landimp_comp1_rec_others2_description);
                record.setValrepLandimpComp1RecOthers3Description(valrep_landimp_comp1_rec_others3_description);

               record.setValrepLandimpComp1RecLocationDesc(valrep_landimp_comp1_rec_location_desc);
                record.setValrepLandimpComp1RecSizeDesc(valrep_landimp_comp1_rec_size_desc);
                record.setValrepLandimpComp1RecShapeDesc(valrep_landimp_comp1_rec_shape_desc);
                record.setValrepLandimpComp1RecTerrainDesc(valrep_landimp_comp1_rec_terrain_desc);
                record.setValrepLandimpComp1RecElevationDesc(valrep_landimp_comp1_rec_elevation_desc);
                record.setValrepLandimpComp1RecNeighborhoodDesc(valrep_landimp_comp1_rec_neighborhood_desc);
                record.setValrepLandimpComp1RecAccessibilityDesc(valrep_landimp_comp1_rec_accessibility_desc);
                record.setValrepLandimpComp1RecLotTypeDesc(valrep_landimp_comp1_rec_lot_type_desc);
                record.setValrepLandimpComp1RecRoadDesc(valrep_landimp_comp1_rec_road_desc);
                record.setValrepLandimpComp1RecOthersDesc(valrep_landimp_comp1_rec_others_desc);
                record.setValrepLandimpComp1RecOthers2Desc(valrep_landimp_comp1_rec_others2_desc);
                record.setValrepLandimpComp1RecOthers3Desc(valrep_landimp_comp1_rec_others3_desc);

                record.setValrepLandimpComp1RecLocation(valrep_landimp_comp1_rec_location);
                record.setValrepLandimpComp1RecSize(valrep_landimp_comp1_rec_size);
                record.setValrepLandimpComp1RecShape(valrep_landimp_comp1_rec_shape);
                record.setValrepLandimpComp1RecTerrain(valrep_landimp_comp1_rec_terrain);
                record.setValrepLandimpComp1RecElevation(valrep_landimp_comp1_rec_elevation);
                record.setValrepLandimpComp1RecNeighborhood(valrep_landimp_comp1_rec_neighborhood);
                record.setValrepLandimpComp1RecAccessibility(valrep_landimp_comp1_rec_accessibility);
                record.setValrepLandimpComp1RecLotType(valrep_landimp_comp1_rec_lot_type);
                record.setValrepLandimpComp1RecRoad(valrep_landimp_comp1_rec_road);
                record.setValrepLandimpComp1RecOthers(valrep_landimp_comp1_rec_others);
                record.setValrepLandimpComp1RecOthers2(valrep_landimp_comp1_rec_others2);
                record.setValrepLandimpComp1RecOthers3(valrep_landimp_comp1_rec_others3);

                /*SUBTOTAL DISPLAY*/
                record.setValrepLandimpComp1TotalgrossAdj(valrep_landimp_comp1_totalgross_adj);
                record.setValrepLandimpComp1TotalAdj(valrep_landimp_comp1_total_adj);
                record.setValrepLandimpComp1AdjustedValue(valrep_landimp_comp1_adjusted_value);
                record.setValrepLandimpComp1Weight(valrep_landimp_comp1_weight);
                record.setValrepLandimpComp1WeightEquivalent(valrep_landimp_comp1_weight_equivalent);

                interactor.updateLandVal(record);

                view.showSnackMessage("Saving Successful");

    //            new Handler().postDelayed(() -> view.hideProgress(), 2000);
            }

            @Override
            public void onError(Throwable e) {
                view.hideProgress();
                view.showExitDialog("Record Error", e.getMessage());
            }
        };
    }


    //TODO Update Record Physical Adjustment 2
    private DisposableSingleObserver<Record> getRecordUpdatePhysicalAdjustment2(
        /*    String valrep_landimp_comp1_rec_location_type,
            String valrep_landimp_comp1_rec_size_type,
            String valrep_landimp_comp1_rec_shape_type,
            String valrep_landimp_comp1_rec_terrain_type,
            String valrep_landimp_comp1_rec_elevation_type,
            String valrep_landimp_comp1_rec_neighborhood_type,
            String valrep_landimp_comp1_rec_accessibility_type,
            String valrep_landimp_comp1_rec_lot_type_type,
            String valrep_landimp_comp1_rec_road_type,
            String valrep_landimp_comp1_rec_others_type,
            String valrep_landimp_comp1_rec_others2_type,
            String valrep_landimp_comp1_rec_others3_type,
            String valrep_landimp_comp1_rec_location_subprop,
            String valrep_landimp_comp1_rec_size_subprop,
            String valrep_landimp_comp1_rec_shape_subprop,
            String valrep_landimp_comp1_rec_terrain_subprop,
            String valrep_landimp_comp1_rec_elevation_subprop,
            String valrep_landimp_comp1_rec_neighborhood_subprop,
            String valrep_landimp_comp1_rec_accessibility_subprop,
            String valrep_landimp_comp1_rec_lot_type_subprop,
            String valrep_landimp_comp1_rec_road_subprop,
            String valrep_landimp_comp1_rec_others_subprop,
            String valrep_landimp_comp1_rec_others2_subprop,
            String valrep_landimp_comp1_rec_others3_subprop,*/

            String valrep_landimp_comp2_rec_location_description,
            String valrep_landimp_comp2_rec_size_description,
            String valrep_landimp_comp2_rec_shape_description,
            String valrep_landimp_comp2_rec_terrain_description,
            String valrep_landimp_comp2_rec_elevation_description,
            String valrep_landimp_comp2_rec_neighborhood_description,
            String valrep_landimp_comp2_rec_accessibility_description,
            String valrep_landimp_comp2_rec_lot_type_description,
            String valrep_landimp_comp2_rec_road_description,
            String valrep_landimp_comp2_rec_others_description,
            String valrep_landimp_comp2_rec_others2_description,
            String valrep_landimp_comp2_rec_others3_description,
            String valrep_landimp_comp2_rec_location_desc,
            String valrep_landimp_comp2_rec_size_desc,
            String valrep_landimp_comp2_rec_shape_desc,
            String valrep_landimp_comp2_rec_terrain_desc,
            String valrep_landimp_comp2_rec_elevation_desc,
            String valrep_landimp_comp2_rec_neighborhood_desc,
            String valrep_landimp_comp2_rec_accessibility_desc,
            String valrep_landimp_comp2_rec_lot_type_desc,
            String valrep_landimp_comp2_rec_road_desc,
            String valrep_landimp_comp2_rec_others_desc,
            String valrep_landimp_comp2_rec_others2_desc,
            String valrep_landimp_comp2_rec_others3_desc,
            String valrep_landimp_comp2_rec_location,
            String valrep_landimp_comp2_rec_size,
            String valrep_landimp_comp2_rec_shape,
            String valrep_landimp_comp2_rec_terrain,
            String valrep_landimp_comp2_rec_elevation,
            String valrep_landimp_comp2_rec_neighborhood,
            String valrep_landimp_comp2_rec_accessibility,
            String valrep_landimp_comp2_rec_lot_type,
            String valrep_landimp_comp2_rec_road,
            String valrep_landimp_comp2_rec_others,
            String valrep_landimp_comp2_rec_others2,
            String valrep_landimp_comp2_rec_others3,
            String valrep_landimp_comp2_totalgross_adj,
            String valrep_landimp_comp2_total_adj,
            String valrep_landimp_comp2_adjusted_value,
            String valrep_landimp_comp2_weight,
            String valrep_landimp_comp2_weight_equivalent){
        return new DisposableSingleObserver<Record>() {
            @Override
            public void onSuccess(Record record) {
              /*  record.setValrepLandimpComp1RecLocationType(valrep_landimp_comp1_rec_location_type);
                record.setValrepLandimpComp1RecSizeType(valrep_landimp_comp1_rec_size_type);
                record.setValrepLandimpComp1RecShapeType(valrep_landimp_comp1_rec_shape_type);
                record.setValrepLandimpComp1RecTerrainType(valrep_landimp_comp1_rec_terrain_type);
                record.setValrepLandimpComp1RecElevationType(valrep_landimp_comp1_rec_elevation_type);
                record.setValrepLandimpComp1RecNeighborhoodType(valrep_landimp_comp1_rec_neighborhood_type);
                record.setValrepLandimpComp1RecAccessibilityType(valrep_landimp_comp1_rec_accessibility_type);
                record.setValrepLandimpComp1RecLotTypeType(valrep_landimp_comp1_rec_lot_type_type);
                record.setValrepLandimpComp1RecRoadType(valrep_landimp_comp1_rec_road_type);
                record.setValrepLandimpComp1RecOthersType(valrep_landimp_comp1_rec_others_type);
                record.setValrepLandimpComp1RecOthers2Type(valrep_landimp_comp1_rec_others2_type);
                record.setValrepLandimpComp1RecOthers3Type(valrep_landimp_comp1_rec_others3_type);
                record.setValrepLandimpComp1RecLocationSubprop(valrep_landimp_comp1_rec_location_subprop);
                record.setValrepLandimpComp1RecSizeSubprop(valrep_landimp_comp1_rec_size_subprop);
                record.setValrepLandimpComp1RecShapeSubprop(valrep_landimp_comp1_rec_shape_subprop);
                record.setValrepLandimpComp1RecTerrainSubprop(valrep_landimp_comp1_rec_terrain_subprop);
                record.setValrepLandimpComp1RecElevationSubprop(valrep_landimp_comp1_rec_elevation_subprop);
                record.setValrepLandimpComp1RecNeighborhoodSubprop(valrep_landimp_comp1_rec_neighborhood_subprop);
                record.setValrepLandimpComp1RecAccessibilitySubprop(valrep_landimp_comp1_rec_accessibility_subprop);
                record.setValrepLandimpComp1RecLotTypeSubprop(valrep_landimp_comp1_rec_lot_type_subprop);
                record.setValrepLandimpComp1RecRoadSubprop(valrep_landimp_comp1_rec_road_subprop);
                record.setValrepLandimpComp1RecOthersSubprop(valrep_landimp_comp1_rec_others_subprop);
                record.setValrepLandimpComp1RecOthers2Subprop(valrep_landimp_comp1_rec_others2_subprop);
                record.setValrepLandimpComp1RecOthers3Subprop(valrep_landimp_comp1_rec_others3_subprop);
*/
                record.setValrepLandimpComp2RecLocationDescription(valrep_landimp_comp2_rec_location_description);
                record.setValrepLandimpComp2RecSizeDescription(valrep_landimp_comp2_rec_size_description);
                record.setValrepLandimpComp2RecShapeDescription(valrep_landimp_comp2_rec_shape_description);
                record.setValrepLandimpComp2RecTerrainDescription(valrep_landimp_comp2_rec_terrain_description);
                record.setValrepLandimpComp2RecElevationDescription(valrep_landimp_comp2_rec_elevation_description);
                record.setValrepLandimpComp2RecNeighborhoodDescription(valrep_landimp_comp2_rec_neighborhood_description);
                record.setValrepLandimpComp2RecAccessibilityDescription(valrep_landimp_comp2_rec_accessibility_description);
                record.setValrepLandimpComp2RecLotTypeDescription(valrep_landimp_comp2_rec_lot_type_description);
                record.setValrepLandimpComp2RecRoadDescription(valrep_landimp_comp2_rec_road_description);
                record.setValrepLandimpComp2RecOthersDescription(valrep_landimp_comp2_rec_others_description);
                record.setValrepLandimpComp2RecOthers2Description(valrep_landimp_comp2_rec_others2_description);
                record.setValrepLandimpComp2RecOthers3Description(valrep_landimp_comp2_rec_others3_description);

                record.setValrepLandimpComp2RecLocationDesc(valrep_landimp_comp2_rec_location_desc);
                record.setValrepLandimpComp2RecSizeDesc(valrep_landimp_comp2_rec_size_desc);
                record.setValrepLandimpComp2RecShapeDesc(valrep_landimp_comp2_rec_shape_desc);
                record.setValrepLandimpComp2RecTerrainDesc(valrep_landimp_comp2_rec_terrain_desc);
                record.setValrepLandimpComp2RecElevationDesc(valrep_landimp_comp2_rec_elevation_desc);
                record.setValrepLandimpComp2RecNeighborhoodDesc(valrep_landimp_comp2_rec_neighborhood_desc);
                record.setValrepLandimpComp2RecAccessibilityDesc(valrep_landimp_comp2_rec_accessibility_desc);
                record.setValrepLandimpComp2RecLotTypeDesc(valrep_landimp_comp2_rec_lot_type_desc);
                record.setValrepLandimpComp2RecRoadDesc(valrep_landimp_comp2_rec_road_desc);
                record.setValrepLandimpComp2RecOthersDesc(valrep_landimp_comp2_rec_others_desc);
                record.setValrepLandimpComp2RecOthers2Desc(valrep_landimp_comp2_rec_others2_desc);
                record.setValrepLandimpComp2RecOthers3Desc(valrep_landimp_comp2_rec_others3_desc);

                record.setValrepLandimpComp2RecLocation(valrep_landimp_comp2_rec_location);
                record.setValrepLandimpComp2RecSize(valrep_landimp_comp2_rec_size);
                record.setValrepLandimpComp2RecShape(valrep_landimp_comp2_rec_shape);
                record.setValrepLandimpComp2RecTerrain(valrep_landimp_comp2_rec_terrain);
                record.setValrepLandimpComp2RecElevation(valrep_landimp_comp2_rec_elevation);
                record.setValrepLandimpComp2RecNeighborhood(valrep_landimp_comp2_rec_neighborhood);
                record.setValrepLandimpComp2RecAccessibility(valrep_landimp_comp2_rec_accessibility);
                record.setValrepLandimpComp2RecLotType(valrep_landimp_comp2_rec_lot_type);
                record.setValrepLandimpComp2RecRoad(valrep_landimp_comp2_rec_road);
                record.setValrepLandimpComp2RecOthers(valrep_landimp_comp2_rec_others);
                record.setValrepLandimpComp2RecOthers2(valrep_landimp_comp2_rec_others2);
                record.setValrepLandimpComp2RecOthers3(valrep_landimp_comp2_rec_others3);

                /*SUBTOTAL DISPLAY*/
                record.setValrepLandimpComp2TotalgrossAdj(valrep_landimp_comp2_totalgross_adj);
                record.setValrepLandimpComp2TotalAdj(valrep_landimp_comp2_total_adj);
                record.setValrepLandimpComp2AdjustedValue(valrep_landimp_comp2_adjusted_value);
                record.setValrepLandimpComp2Weight(valrep_landimp_comp2_weight);
                record.setValrepLandimpComp2WeightEquivalent(valrep_landimp_comp2_weight_equivalent);

                interactor.updateLandVal(record);

                view.showSnackMessage("Saving Successful");

     //           new Handler().postDelayed(() -> view.hideProgress(), 2000);
            }

            @Override
            public void onError(Throwable e) {
                view.hideProgress();
                view.showExitDialog("Record Error", e.getMessage());
            }
        };
    }

    //TODO Update Record Physical Adjustment 3
    private DisposableSingleObserver<Record> getRecordUpdatePhysicalAdjustment3(
        /*    String valrep_landimp_comp1_rec_location_type,
            String valrep_landimp_comp1_rec_size_type,
            String valrep_landimp_comp1_rec_shape_type,
            String valrep_landimp_comp1_rec_terrain_type,
            String valrep_landimp_comp1_rec_elevation_type,
            String valrep_landimp_comp1_rec_neighborhood_type,
            String valrep_landimp_comp1_rec_accessibility_type,
            String valrep_landimp_comp1_rec_lot_type_type,
            String valrep_landimp_comp1_rec_road_type,
            String valrep_landimp_comp1_rec_others_type,
            String valrep_landimp_comp1_rec_others2_type,
            String valrep_landimp_comp1_rec_others3_type,
            String valrep_landimp_comp1_rec_location_subprop,
            String valrep_landimp_comp1_rec_size_subprop,
            String valrep_landimp_comp1_rec_shape_subprop,
            String valrep_landimp_comp1_rec_terrain_subprop,
            String valrep_landimp_comp1_rec_elevation_subprop,
            String valrep_landimp_comp1_rec_neighborhood_subprop,
            String valrep_landimp_comp1_rec_accessibility_subprop,
            String valrep_landimp_comp1_rec_lot_type_subprop,
            String valrep_landimp_comp1_rec_road_subprop,
            String valrep_landimp_comp1_rec_others_subprop,
            String valrep_landimp_comp1_rec_others2_subprop,
            String valrep_landimp_comp1_rec_others3_subprop,*/

            String valrep_landimp_comp3_rec_location_description,
            String valrep_landimp_comp3_rec_size_description,
            String valrep_landimp_comp3_rec_shape_description,
            String valrep_landimp_comp3_rec_terrain_description,
            String valrep_landimp_comp3_rec_elevation_description,
            String valrep_landimp_comp3_rec_neighborhood_description,
            String valrep_landimp_comp3_rec_accessibility_description,
            String valrep_landimp_comp3_rec_lot_type_description,
            String valrep_landimp_comp3_rec_road_description,
            String valrep_landimp_comp3_rec_others_description,
            String valrep_landimp_comp3_rec_others2_description,
            String valrep_landimp_comp3_rec_others3_description,
            String valrep_landimp_comp3_rec_location_desc,
            String valrep_landimp_comp3_rec_size_desc,
            String valrep_landimp_comp3_rec_shape_desc,
            String valrep_landimp_comp3_rec_terrain_desc,
            String valrep_landimp_comp3_rec_elevation_desc,
            String valrep_landimp_comp3_rec_neighborhood_desc,
            String valrep_landimp_comp3_rec_accessibility_desc,
            String valrep_landimp_comp3_rec_lot_type_desc,
            String valrep_landimp_comp3_rec_road_desc,
            String valrep_landimp_comp3_rec_others_desc,
            String valrep_landimp_comp3_rec_others2_desc,
            String valrep_landimp_comp3_rec_others3_desc,
            String valrep_landimp_comp3_rec_location,
            String valrep_landimp_comp3_rec_size,
            String valrep_landimp_comp3_rec_shape,
            String valrep_landimp_comp3_rec_terrain,
            String valrep_landimp_comp3_rec_elevation,
            String valrep_landimp_comp3_rec_neighborhood,
            String valrep_landimp_comp3_rec_accessibility,
            String valrep_landimp_comp3_rec_lot_type,
            String valrep_landimp_comp3_rec_road,
            String valrep_landimp_comp3_rec_others,
            String valrep_landimp_comp3_rec_others2,
            String valrep_landimp_comp3_rec_others3,
            String valrep_landimp_comp3_totalgross_adj,
            String valrep_landimp_comp3_total_adj,
            String valrep_landimp_comp3_adjusted_value,
            String valrep_landimp_comp3_weight,
            String valrep_landimp_comp3_weight_equivalent){
        return new DisposableSingleObserver<Record>() {
            @Override
            public void onSuccess(Record record) {
              /*  record.setValrepLandimpComp1RecLocationType(valrep_landimp_comp1_rec_location_type);
                record.setValrepLandimpComp1RecSizeType(valrep_landimp_comp1_rec_size_type);
                record.setValrepLandimpComp1RecShapeType(valrep_landimp_comp1_rec_shape_type);
                record.setValrepLandimpComp1RecTerrainType(valrep_landimp_comp1_rec_terrain_type);
                record.setValrepLandimpComp1RecElevationType(valrep_landimp_comp1_rec_elevation_type);
                record.setValrepLandimpComp1RecNeighborhoodType(valrep_landimp_comp1_rec_neighborhood_type);
                record.setValrepLandimpComp1RecAccessibilityType(valrep_landimp_comp1_rec_accessibility_type);
                record.setValrepLandimpComp1RecLotTypeType(valrep_landimp_comp1_rec_lot_type_type);
                record.setValrepLandimpComp1RecRoadType(valrep_landimp_comp1_rec_road_type);
                record.setValrepLandimpComp1RecOthersType(valrep_landimp_comp1_rec_others_type);
                record.setValrepLandimpComp1RecOthers2Type(valrep_landimp_comp1_rec_others2_type);
                record.setValrepLandimpComp1RecOthers3Type(valrep_landimp_comp1_rec_others3_type);
                record.setValrepLandimpComp1RecLocationSubprop(valrep_landimp_comp1_rec_location_subprop);
                record.setValrepLandimpComp1RecSizeSubprop(valrep_landimp_comp1_rec_size_subprop);
                record.setValrepLandimpComp1RecShapeSubprop(valrep_landimp_comp1_rec_shape_subprop);
                record.setValrepLandimpComp1RecTerrainSubprop(valrep_landimp_comp1_rec_terrain_subprop);
                record.setValrepLandimpComp1RecElevationSubprop(valrep_landimp_comp1_rec_elevation_subprop);
                record.setValrepLandimpComp1RecNeighborhoodSubprop(valrep_landimp_comp1_rec_neighborhood_subprop);
                record.setValrepLandimpComp1RecAccessibilitySubprop(valrep_landimp_comp1_rec_accessibility_subprop);
                record.setValrepLandimpComp1RecLotTypeSubprop(valrep_landimp_comp1_rec_lot_type_subprop);
                record.setValrepLandimpComp1RecRoadSubprop(valrep_landimp_comp1_rec_road_subprop);
                record.setValrepLandimpComp1RecOthersSubprop(valrep_landimp_comp1_rec_others_subprop);
                record.setValrepLandimpComp1RecOthers2Subprop(valrep_landimp_comp1_rec_others2_subprop);
                record.setValrepLandimpComp1RecOthers3Subprop(valrep_landimp_comp1_rec_others3_subprop);*/

                record.setValrepLandimpComp3RecLocationDescription(valrep_landimp_comp3_rec_location_description);
                record.setValrepLandimpComp3RecSizeDescription(valrep_landimp_comp3_rec_size_description);
                record.setValrepLandimpComp3RecShapeDescription(valrep_landimp_comp3_rec_shape_description);
                record.setValrepLandimpComp3RecTerrainDescription(valrep_landimp_comp3_rec_terrain_description);
                record.setValrepLandimpComp3RecElevationDescription(valrep_landimp_comp3_rec_elevation_description);
                record.setValrepLandimpComp3RecNeighborhoodDescription(valrep_landimp_comp3_rec_neighborhood_description);
                record.setValrepLandimpComp3RecAccessibilityDescription(valrep_landimp_comp3_rec_accessibility_description);
                record.setValrepLandimpComp3RecLotTypeDescription(valrep_landimp_comp3_rec_lot_type_description);
                record.setValrepLandimpComp3RecRoadDescription(valrep_landimp_comp3_rec_road_description);
                record.setValrepLandimpComp3RecOthersDescription(valrep_landimp_comp3_rec_others_description);
                record.setValrepLandimpComp3RecOthers2Description(valrep_landimp_comp3_rec_others2_description);
                record.setValrepLandimpComp3RecOthers3Description(valrep_landimp_comp3_rec_others3_description);

                record.setValrepLandimpComp3RecLocationDesc(valrep_landimp_comp3_rec_location_desc);
                record.setValrepLandimpComp3RecSizeDesc(valrep_landimp_comp3_rec_size_desc);
                record.setValrepLandimpComp3RecShapeDesc(valrep_landimp_comp3_rec_shape_desc);
                record.setValrepLandimpComp3RecTerrainDesc(valrep_landimp_comp3_rec_terrain_desc);
                record.setValrepLandimpComp3RecElevationDesc(valrep_landimp_comp3_rec_elevation_desc);
                record.setValrepLandimpComp3RecNeighborhoodDesc(valrep_landimp_comp3_rec_neighborhood_desc);
                record.setValrepLandimpComp3RecAccessibilityDesc(valrep_landimp_comp3_rec_accessibility_desc);
                record.setValrepLandimpComp3RecLotTypeDesc(valrep_landimp_comp3_rec_lot_type_desc);
                record.setValrepLandimpComp3RecRoadDesc(valrep_landimp_comp3_rec_road_desc);
                record.setValrepLandimpComp3RecOthersDesc(valrep_landimp_comp3_rec_others_desc);
                record.setValrepLandimpComp3RecOthers2Desc(valrep_landimp_comp3_rec_others2_desc);
                record.setValrepLandimpComp3RecOthers3Desc(valrep_landimp_comp3_rec_others3_desc);

                record.setValrepLandimpComp3RecLocation(valrep_landimp_comp3_rec_location);
                record.setValrepLandimpComp3RecSize(valrep_landimp_comp3_rec_size);
                record.setValrepLandimpComp3RecShape(valrep_landimp_comp3_rec_shape);
                record.setValrepLandimpComp3RecTerrain(valrep_landimp_comp3_rec_terrain);
                record.setValrepLandimpComp3RecElevation(valrep_landimp_comp3_rec_elevation);
                record.setValrepLandimpComp3RecNeighborhood(valrep_landimp_comp3_rec_neighborhood);
                record.setValrepLandimpComp3RecAccessibility(valrep_landimp_comp3_rec_accessibility);
                record.setValrepLandimpComp3RecLotType(valrep_landimp_comp3_rec_lot_type);
                record.setValrepLandimpComp3RecRoad(valrep_landimp_comp3_rec_road);
                record.setValrepLandimpComp3RecOthers(valrep_landimp_comp3_rec_others);
                record.setValrepLandimpComp3RecOthers2(valrep_landimp_comp3_rec_others2);
                record.setValrepLandimpComp3RecOthers3(valrep_landimp_comp3_rec_others3);

                /*SUBTOTAL DISPLAY*/
                record.setValrepLandimpComp3TotalgrossAdj(valrep_landimp_comp3_totalgross_adj);
                record.setValrepLandimpComp3TotalAdj(valrep_landimp_comp3_total_adj);
                record.setValrepLandimpComp3AdjustedValue(valrep_landimp_comp3_adjusted_value);
                record.setValrepLandimpComp3Weight(valrep_landimp_comp3_weight);
                record.setValrepLandimpComp3WeightEquivalent(valrep_landimp_comp3_weight_equivalent);

                interactor.updateLandVal(record);

                view.showSnackMessage("Saving Successful");

    //            new Handler().postDelayed(() -> view.hideProgress(), 2000);
            }

            @Override
            public void onError(Throwable e) {
                view.hideProgress();
                view.showExitDialog("Record Error", e.getMessage());
            }
        };
    }

    //TODO Update Record Physical Adjustment 4
    private DisposableSingleObserver<Record> getRecordUpdatePhysicalAdjustment4(
          /*  String valrep_landimp_comp1_rec_location_type,
            String valrep_landimp_comp1_rec_size_type,
            String valrep_landimp_comp1_rec_shape_type,
            String valrep_landimp_comp1_rec_terrain_type,
            String valrep_landimp_comp1_rec_elevation_type,
            String valrep_landimp_comp1_rec_neighborhood_type,
            String valrep_landimp_comp1_rec_accessibility_type,
            String valrep_landimp_comp1_rec_lot_type_type,
            String valrep_landimp_comp1_rec_road_type,
            String valrep_landimp_comp1_rec_others_type,
            String valrep_landimp_comp1_rec_others2_type,
            String valrep_landimp_comp1_rec_others3_type,
            String valrep_landimp_comp1_rec_location_subprop,
            String valrep_landimp_comp1_rec_size_subprop,
            String valrep_landimp_comp1_rec_shape_subprop,
            String valrep_landimp_comp1_rec_terrain_subprop,
            String valrep_landimp_comp1_rec_elevation_subprop,
            String valrep_landimp_comp1_rec_neighborhood_subprop,
            String valrep_landimp_comp1_rec_accessibility_subprop,
            String valrep_landimp_comp1_rec_lot_type_subprop,
            String valrep_landimp_comp1_rec_road_subprop,
            String valrep_landimp_comp1_rec_others_subprop,
            String valrep_landimp_comp1_rec_others2_subprop,
            String valrep_landimp_comp1_rec_others3_subprop,*/

            String valrep_landimp_comp4_rec_location_description,
            String valrep_landimp_comp4_rec_size_description,
            String valrep_landimp_comp4_rec_shape_description,
            String valrep_landimp_comp4_rec_terrain_description,
            String valrep_landimp_comp4_rec_elevation_description,
            String valrep_landimp_comp4_rec_neighborhood_description,
            String valrep_landimp_comp4_rec_accessibility_description,
            String valrep_landimp_comp4_rec_lot_type_description,
            String valrep_landimp_comp4_rec_road_description,
            String valrep_landimp_comp4_rec_others_description,
            String valrep_landimp_comp4_rec_others2_description,
            String valrep_landimp_comp4_rec_others3_description,
            String valrep_landimp_comp4_rec_location_desc,
            String valrep_landimp_comp4_rec_size_desc,
            String valrep_landimp_comp4_rec_shape_desc,
            String valrep_landimp_comp4_rec_terrain_desc,
            String valrep_landimp_comp4_rec_elevation_desc,
            String valrep_landimp_comp4_rec_neighborhood_desc,
            String valrep_landimp_comp4_rec_accessibility_desc,
            String valrep_landimp_comp4_rec_lot_type_desc,
            String valrep_landimp_comp4_rec_road_desc,
            String valrep_landimp_comp4_rec_others_desc,
            String valrep_landimp_comp4_rec_others2_desc,
            String valrep_landimp_comp4_rec_others3_desc,
            String valrep_landimp_comp4_rec_location,
            String valrep_landimp_comp4_rec_size,
            String valrep_landimp_comp4_rec_shape,
            String valrep_landimp_comp4_rec_terrain,
            String valrep_landimp_comp4_rec_elevation,
            String valrep_landimp_comp4_rec_neighborhood,
            String valrep_landimp_comp4_rec_accessibility,
            String valrep_landimp_comp4_rec_lot_type,
            String valrep_landimp_comp4_rec_road,
            String valrep_landimp_comp4_rec_others,
            String valrep_landimp_comp4_rec_others2,
            String valrep_landimp_comp4_rec_others3,
            String valrep_landimp_comp4_totalgross_adj,
            String valrep_landimp_comp4_total_adj,
            String valrep_landimp_comp4_adjusted_value,
            String valrep_landimp_comp4_weight,
            String valrep_landimp_comp4_weight_equivalent){
        return new DisposableSingleObserver<Record>() {
            @Override
            public void onSuccess(Record record) {
             /*   record.setValrepLandimpComp1RecLocationType(valrep_landimp_comp1_rec_location_type);
                record.setValrepLandimpComp1RecSizeType(valrep_landimp_comp1_rec_size_type);
                record.setValrepLandimpComp1RecShapeType(valrep_landimp_comp1_rec_shape_type);
                record.setValrepLandimpComp1RecTerrainType(valrep_landimp_comp1_rec_terrain_type);
                record.setValrepLandimpComp1RecElevationType(valrep_landimp_comp1_rec_elevation_type);
                record.setValrepLandimpComp1RecNeighborhoodType(valrep_landimp_comp1_rec_neighborhood_type);
                record.setValrepLandimpComp1RecAccessibilityType(valrep_landimp_comp1_rec_accessibility_type);
                record.setValrepLandimpComp1RecLotTypeType(valrep_landimp_comp1_rec_lot_type_type);
                record.setValrepLandimpComp1RecRoadType(valrep_landimp_comp1_rec_road_type);
                record.setValrepLandimpComp1RecOthersType(valrep_landimp_comp1_rec_others_type);
                record.setValrepLandimpComp1RecOthers2Type(valrep_landimp_comp1_rec_others2_type);
                record.setValrepLandimpComp1RecOthers3Type(valrep_landimp_comp1_rec_others3_type);
                record.setValrepLandimpComp1RecLocationSubprop(valrep_landimp_comp1_rec_location_subprop);
                record.setValrepLandimpComp1RecSizeSubprop(valrep_landimp_comp1_rec_size_subprop);
                record.setValrepLandimpComp1RecShapeSubprop(valrep_landimp_comp1_rec_shape_subprop);
                record.setValrepLandimpComp1RecTerrainSubprop(valrep_landimp_comp1_rec_terrain_subprop);
                record.setValrepLandimpComp1RecElevationSubprop(valrep_landimp_comp1_rec_elevation_subprop);
                record.setValrepLandimpComp1RecNeighborhoodSubprop(valrep_landimp_comp1_rec_neighborhood_subprop);
                record.setValrepLandimpComp1RecAccessibilitySubprop(valrep_landimp_comp1_rec_accessibility_subprop);
                record.setValrepLandimpComp1RecLotTypeSubprop(valrep_landimp_comp1_rec_lot_type_subprop);
                record.setValrepLandimpComp1RecRoadSubprop(valrep_landimp_comp1_rec_road_subprop);
                record.setValrepLandimpComp1RecOthersSubprop(valrep_landimp_comp1_rec_others_subprop);
                record.setValrepLandimpComp1RecOthers2Subprop(valrep_landimp_comp1_rec_others2_subprop);
                record.setValrepLandimpComp1RecOthers3Subprop(valrep_landimp_comp1_rec_others3_subprop);*/

                record.setValrepLandimpComp4RecLocationDescription(valrep_landimp_comp4_rec_location_description);
                record.setValrepLandimpComp4RecSizeDescription(valrep_landimp_comp4_rec_size_description);
                record.setValrepLandimpComp4RecShapeDescription(valrep_landimp_comp4_rec_shape_description);
                record.setValrepLandimpComp4RecTerrainDescription(valrep_landimp_comp4_rec_terrain_description);
                record.setValrepLandimpComp4RecElevationDescription(valrep_landimp_comp4_rec_elevation_description);
                record.setValrepLandimpComp4RecNeighborhoodDescription(valrep_landimp_comp4_rec_neighborhood_description);
                record.setValrepLandimpComp4RecAccessibilityDescription(valrep_landimp_comp4_rec_accessibility_description);
                record.setValrepLandimpComp4RecLotTypeDescription(valrep_landimp_comp4_rec_lot_type_description);
                record.setValrepLandimpComp4RecRoadDescription(valrep_landimp_comp4_rec_road_description);
                record.setValrepLandimpComp4RecOthersDescription(valrep_landimp_comp4_rec_others_description);
                record.setValrepLandimpComp4RecOthers2Description(valrep_landimp_comp4_rec_others2_description);
                record.setValrepLandimpComp4RecOthers3Description(valrep_landimp_comp4_rec_others3_description);

                record.setValrepLandimpComp4RecLocationDesc(valrep_landimp_comp4_rec_location_desc);
                record.setValrepLandimpComp4RecSizeDesc(valrep_landimp_comp4_rec_size_desc);
                record.setValrepLandimpComp4RecShapeDesc(valrep_landimp_comp4_rec_shape_desc);
                record.setValrepLandimpComp4RecTerrainDesc(valrep_landimp_comp4_rec_terrain_desc);
                record.setValrepLandimpComp4RecElevationDesc(valrep_landimp_comp4_rec_elevation_desc);
                record.setValrepLandimpComp4RecNeighborhoodDesc(valrep_landimp_comp4_rec_neighborhood_desc);
                record.setValrepLandimpComp4RecAccessibilityDesc(valrep_landimp_comp4_rec_accessibility_desc);
                record.setValrepLandimpComp4RecLotTypeDesc(valrep_landimp_comp4_rec_lot_type_desc);
                record.setValrepLandimpComp4RecRoadDesc(valrep_landimp_comp4_rec_road_desc);
                record.setValrepLandimpComp4RecOthersDesc(valrep_landimp_comp4_rec_others_desc);
                record.setValrepLandimpComp4RecOthers2Desc(valrep_landimp_comp4_rec_others2_desc);
                record.setValrepLandimpComp4RecOthers3Desc(valrep_landimp_comp4_rec_others3_desc);

                record.setValrepLandimpComp4RecLocation(valrep_landimp_comp4_rec_location);
                record.setValrepLandimpComp4RecSize(valrep_landimp_comp4_rec_size);
                record.setValrepLandimpComp4RecShape(valrep_landimp_comp4_rec_shape);
                record.setValrepLandimpComp4RecTerrain(valrep_landimp_comp4_rec_terrain);
                record.setValrepLandimpComp4RecElevation(valrep_landimp_comp4_rec_elevation);
                record.setValrepLandimpComp4RecNeighborhood(valrep_landimp_comp4_rec_neighborhood);
                record.setValrepLandimpComp4RecAccessibility(valrep_landimp_comp4_rec_accessibility);
                record.setValrepLandimpComp4RecLotType(valrep_landimp_comp4_rec_lot_type);
                record.setValrepLandimpComp4RecRoad(valrep_landimp_comp4_rec_road);
                record.setValrepLandimpComp4RecOthers(valrep_landimp_comp4_rec_others);
                record.setValrepLandimpComp4RecOthers2(valrep_landimp_comp4_rec_others2);
                record.setValrepLandimpComp4RecOthers3(valrep_landimp_comp4_rec_others3);

                /*SUBTOTAL DISPLAY*/
                record.setValrepLandimpComp4TotalgrossAdj(valrep_landimp_comp4_totalgross_adj);
                record.setValrepLandimpComp4TotalAdj(valrep_landimp_comp4_total_adj);
                record.setValrepLandimpComp4AdjustedValue(valrep_landimp_comp4_adjusted_value);
                record.setValrepLandimpComp4Weight(valrep_landimp_comp4_weight);
                record.setValrepLandimpComp4WeightEquivalent(valrep_landimp_comp4_weight_equivalent);

                interactor.updateLandVal(record);

                view.showSnackMessage("Saving Successful");

   //             new Handler().postDelayed(() -> view.hideProgress(), 2000);
            }

            @Override
            public void onError(Throwable e) {
                view.hideProgress();
                view.showExitDialog("Record Error", e.getMessage());
            }
        };
    }

    //TODO Update Record Physical Adjustment 5
    private DisposableSingleObserver<Record> getRecordUpdatePhysicalAdjustment5(
  /*          String valrep_landimp_comp1_rec_location_type,
            String valrep_landimp_comp1_rec_size_type,
            String valrep_landimp_comp1_rec_shape_type,
            String valrep_landimp_comp1_rec_terrain_type,
            String valrep_landimp_comp1_rec_elevation_type,
            String valrep_landimp_comp1_rec_neighborhood_type,
            String valrep_landimp_comp1_rec_accessibility_type,
            String valrep_landimp_comp1_rec_lot_type_type,
            String valrep_landimp_comp1_rec_road_type,
            String valrep_landimp_comp1_rec_others_type,
            String valrep_landimp_comp1_rec_others2_type,
            String valrep_landimp_comp1_rec_others3_type,
            String valrep_landimp_comp1_rec_location_subprop,
            String valrep_landimp_comp1_rec_size_subprop,
            String valrep_landimp_comp1_rec_shape_subprop,
            String valrep_landimp_comp1_rec_terrain_subprop,
            String valrep_landimp_comp1_rec_elevation_subprop,
            String valrep_landimp_comp1_rec_neighborhood_subprop,
            String valrep_landimp_comp1_rec_accessibility_subprop,
            String valrep_landimp_comp1_rec_lot_type_subprop,
            String valrep_landimp_comp1_rec_road_subprop,
            String valrep_landimp_comp1_rec_others_subprop,
            String valrep_landimp_comp1_rec_others2_subprop,
            String valrep_landimp_comp1_rec_others3_subprop,
*/
            String valrep_landimp_comp5_rec_location_description,
            String valrep_landimp_comp5_rec_size_description,
            String valrep_landimp_comp5_rec_shape_description,
            String valrep_landimp_comp5_rec_terrain_description,
            String valrep_landimp_comp5_rec_elevation_description,
            String valrep_landimp_comp5_rec_neighborhood_description,
            String valrep_landimp_comp5_rec_accessibility_description,
            String valrep_landimp_comp5_rec_lot_type_description,
            String valrep_landimp_comp5_rec_road_description,
            String valrep_landimp_comp5_rec_others_description,
            String valrep_landimp_comp5_rec_others2_description,
            String valrep_landimp_comp5_rec_others3_description,
            String valrep_landimp_comp5_rec_location_desc,
            String valrep_landimp_comp5_rec_size_desc,
            String valrep_landimp_comp5_rec_shape_desc,
            String valrep_landimp_comp5_rec_terrain_desc,
            String valrep_landimp_comp5_rec_elevation_desc,
            String valrep_landimp_comp5_rec_neighborhood_desc,
            String valrep_landimp_comp5_rec_accessibility_desc,
            String valrep_landimp_comp5_rec_lot_type_desc,
            String valrep_landimp_comp5_rec_road_desc,
            String valrep_landimp_comp5_rec_others_desc,
            String valrep_landimp_comp5_rec_others2_desc,
            String valrep_landimp_comp5_rec_others3_desc,
            String valrep_landimp_comp5_rec_location,
            String valrep_landimp_comp5_rec_size,
            String valrep_landimp_comp5_rec_shape,
            String valrep_landimp_comp5_rec_terrain,
            String valrep_landimp_comp5_rec_elevation,
            String valrep_landimp_comp5_rec_neighborhood,
            String valrep_landimp_comp5_rec_accessibility,
            String valrep_landimp_comp5_rec_lot_type,
            String valrep_landimp_comp5_rec_road,
            String valrep_landimp_comp5_rec_others,
            String valrep_landimp_comp5_rec_others2,
            String valrep_landimp_comp5_rec_others3,
            String valrep_landimp_comp5_totalgross_adj,
            String valrep_landimp_comp5_total_adj,
            String valrep_landimp_comp5_adjusted_value,
            String valrep_landimp_comp5_weight,
            String valrep_landimp_comp5_weight_equivalent){
        return new DisposableSingleObserver<Record>() {
            @Override
            public void onSuccess(Record record) {
            /*    record.setValrepLandimpComp1RecLocationType(valrep_landimp_comp1_rec_location_type);
                record.setValrepLandimpComp1RecSizeType(valrep_landimp_comp1_rec_size_type);
                record.setValrepLandimpComp1RecShapeType(valrep_landimp_comp1_rec_shape_type);
                record.setValrepLandimpComp1RecTerrainType(valrep_landimp_comp1_rec_terrain_type);
                record.setValrepLandimpComp1RecElevationType(valrep_landimp_comp1_rec_elevation_type);
                record.setValrepLandimpComp1RecNeighborhoodType(valrep_landimp_comp1_rec_neighborhood_type);
                record.setValrepLandimpComp1RecAccessibilityType(valrep_landimp_comp1_rec_accessibility_type);
                record.setValrepLandimpComp1RecLotTypeType(valrep_landimp_comp1_rec_lot_type_type);
                record.setValrepLandimpComp1RecRoadType(valrep_landimp_comp1_rec_road_type);
                record.setValrepLandimpComp1RecOthersType(valrep_landimp_comp1_rec_others_type);
                record.setValrepLandimpComp1RecOthers2Type(valrep_landimp_comp1_rec_others2_type);
                record.setValrepLandimpComp1RecOthers3Type(valrep_landimp_comp1_rec_others3_type);
                record.setValrepLandimpComp1RecLocationSubprop(valrep_landimp_comp1_rec_location_subprop);
                record.setValrepLandimpComp1RecSizeSubprop(valrep_landimp_comp1_rec_size_subprop);
                record.setValrepLandimpComp1RecShapeSubprop(valrep_landimp_comp1_rec_shape_subprop);
                record.setValrepLandimpComp1RecTerrainSubprop(valrep_landimp_comp1_rec_terrain_subprop);
                record.setValrepLandimpComp1RecElevationSubprop(valrep_landimp_comp1_rec_elevation_subprop);
                record.setValrepLandimpComp1RecNeighborhoodSubprop(valrep_landimp_comp1_rec_neighborhood_subprop);
                record.setValrepLandimpComp1RecAccessibilitySubprop(valrep_landimp_comp1_rec_accessibility_subprop);
                record.setValrepLandimpComp1RecLotTypeSubprop(valrep_landimp_comp1_rec_lot_type_subprop);
                record.setValrepLandimpComp1RecRoadSubprop(valrep_landimp_comp1_rec_road_subprop);
                record.setValrepLandimpComp1RecOthersSubprop(valrep_landimp_comp1_rec_others_subprop);
                record.setValrepLandimpComp1RecOthers2Subprop(valrep_landimp_comp1_rec_others2_subprop);
                record.setValrepLandimpComp1RecOthers3Subprop(valrep_landimp_comp1_rec_others3_subprop);*/

                record.setValrepLandimpComp5RecLocationDescription(valrep_landimp_comp5_rec_location_description);
                record.setValrepLandimpComp5RecSizeDescription(valrep_landimp_comp5_rec_size_description);
                record.setValrepLandimpComp5RecShapeDescription(valrep_landimp_comp5_rec_shape_description);
                record.setValrepLandimpComp5RecTerrainDescription(valrep_landimp_comp5_rec_terrain_description);
                record.setValrepLandimpComp5RecElevationDescription(valrep_landimp_comp5_rec_elevation_description);
                record.setValrepLandimpComp5RecNeighborhoodDescription(valrep_landimp_comp5_rec_neighborhood_description);
                record.setValrepLandimpComp5RecAccessibilityDescription(valrep_landimp_comp5_rec_accessibility_description);
                record.setValrepLandimpComp5RecLotTypeDescription(valrep_landimp_comp5_rec_lot_type_description);
                record.setValrepLandimpComp5RecRoadDescription(valrep_landimp_comp5_rec_road_description);
                record.setValrepLandimpComp5RecOthersDescription(valrep_landimp_comp5_rec_others_description);
                record.setValrepLandimpComp5RecOthers2Description(valrep_landimp_comp5_rec_others2_description);
                record.setValrepLandimpComp5RecOthers3Description(valrep_landimp_comp5_rec_others3_description);

                record.setValrepLandimpComp5RecLocationDesc(valrep_landimp_comp5_rec_location_desc);
                record.setValrepLandimpComp5RecSizeDesc(valrep_landimp_comp5_rec_size_desc);
                record.setValrepLandimpComp5RecShapeDesc(valrep_landimp_comp5_rec_shape_desc);
                record.setValrepLandimpComp5RecTerrainDesc(valrep_landimp_comp5_rec_terrain_desc);
                record.setValrepLandimpComp5RecElevationDesc(valrep_landimp_comp5_rec_elevation_desc);
                record.setValrepLandimpComp5RecNeighborhoodDesc(valrep_landimp_comp5_rec_neighborhood_desc);
                record.setValrepLandimpComp5RecAccessibilityDesc(valrep_landimp_comp5_rec_accessibility_desc);
                record.setValrepLandimpComp5RecLotTypeDesc(valrep_landimp_comp5_rec_lot_type_desc);
                record.setValrepLandimpComp5RecRoadDesc(valrep_landimp_comp5_rec_road_desc);
                record.setValrepLandimpComp5RecOthersDesc(valrep_landimp_comp5_rec_others_desc);
                record.setValrepLandimpComp5RecOthers2Desc(valrep_landimp_comp5_rec_others2_desc);
                record.setValrepLandimpComp5RecOthers3Desc(valrep_landimp_comp5_rec_others3_desc);

                record.setValrepLandimpComp5RecLocation(valrep_landimp_comp5_rec_location);
                record.setValrepLandimpComp5RecSize(valrep_landimp_comp5_rec_size);
                record.setValrepLandimpComp5RecShape(valrep_landimp_comp5_rec_shape);
                record.setValrepLandimpComp5RecTerrain(valrep_landimp_comp5_rec_terrain);
                record.setValrepLandimpComp5RecElevation(valrep_landimp_comp5_rec_elevation);
                record.setValrepLandimpComp5RecNeighborhood(valrep_landimp_comp5_rec_neighborhood);
                record.setValrepLandimpComp5RecAccessibility(valrep_landimp_comp5_rec_accessibility);
                record.setValrepLandimpComp5RecLotType(valrep_landimp_comp5_rec_lot_type);
                record.setValrepLandimpComp5RecRoad(valrep_landimp_comp5_rec_road);
                record.setValrepLandimpComp5RecOthers(valrep_landimp_comp5_rec_others);
                record.setValrepLandimpComp5RecOthers2(valrep_landimp_comp5_rec_others2);
                record.setValrepLandimpComp5RecOthers3(valrep_landimp_comp5_rec_others3);

                /*SUBTOTAL DISPLAY*/
                record.setValrepLandimpComp5TotalgrossAdj(valrep_landimp_comp5_totalgross_adj);
                record.setValrepLandimpComp5TotalAdj(valrep_landimp_comp5_total_adj);
                record.setValrepLandimpComp5AdjustedValue(valrep_landimp_comp5_adjusted_value);
                record.setValrepLandimpComp5Weight(valrep_landimp_comp5_weight);
                record.setValrepLandimpComp5WeightEquivalent(valrep_landimp_comp5_weight_equivalent);

                interactor.updateLandVal(record);

                view.showSnackMessage("Saving Successful");

//                new Handler().postDelayed(() -> view.hideProgress(), 2000);
            }

            @Override
            public void onError(Throwable e) {
                view.hideProgress();
                view.showExitDialog("Record Error", e.getMessage());
            }
        };
    }

}