package com.gdslinkasia.landvaluation.physical_adjustment;

import android.app.Application;

import com.gdslinkasia.base.database.repository.JobDetailsLocalRepository;
import com.gdslinkasia.base.model.job.Record;

import io.reactivex.Single;

class UpdatePhysicalAdjustmentInteractorImpl implements UpdatePhysicalAdjustmentContract.Interactor {

    private JobDetailsLocalRepository localRepository;

    public UpdatePhysicalAdjustmentInteractorImpl(Application application) {
        localRepository = new JobDetailsLocalRepository(application);
    }

    @Override
    public Single<Record> getRecordById(String recordId) {
        return localRepository.getRecordById(recordId);
    }

    @Override
    public void updateLandVal(Record record) {
        localRepository.updateRecord(record);  //update all record: should be rename to updateRecordAll
    }
}
