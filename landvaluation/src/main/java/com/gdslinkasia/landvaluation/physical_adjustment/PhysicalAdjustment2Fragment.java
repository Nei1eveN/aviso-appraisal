package com.gdslinkasia.landvaluation.physical_adjustment;

import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AutoCompleteTextView;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;

import com.gdslinkasia.base.model.job.Record;
import com.gdslinkasia.commonresources.R2;
import com.gdslinkasia.landvaluation.R;
import com.google.android.material.textfield.TextInputLayout;

import java.util.Objects;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.Unbinder;

import static com.gdslinkasia.base.database.DatabaseUtils.RECORD_ID;
import static com.gdslinkasia.base.utils.GlobalFunctions.GET_CONTENT_ADAPTER;
import static com.gdslinkasia.base.utils.computation.PhysicalAdjustmentComputation.*;


public class PhysicalAdjustment2Fragment extends Fragment implements UpdatePhysicalAdjustmentContract.View{

    private Unbinder unbinder;

    @BindView(R2.id.acElementType1) AutoCompleteTextView acElementType1;
    @BindView(R2.id.acElementType2) AutoCompleteTextView acElementType2;
    @BindView(R2.id.acElementType3) AutoCompleteTextView acElementType3;
    @BindView(R2.id.acElementType4) AutoCompleteTextView acElementType4;
    @BindView(R2.id.acElementType5) AutoCompleteTextView acElementType5;
    @BindView(R2.id.acElementType6) AutoCompleteTextView acElementType6;
    @BindView(R2.id.acElementType7) AutoCompleteTextView acElementType7;
    @BindView(R2.id.acElementType8) AutoCompleteTextView acElementType8;
    @BindView(R2.id.acElementType9) AutoCompleteTextView acElementType9;
    @BindView(R2.id.acElementType10) AutoCompleteTextView acElementType10;
    @BindView(R2.id.acElementType11) AutoCompleteTextView acElementType11;
    @BindView(R2.id.acElementType12) AutoCompleteTextView acElementType12;

    @BindView(R2.id.acSubjectProp1) AutoCompleteTextView acSubjectProp1;
    @BindView(R2.id.acSubjectProp2) AutoCompleteTextView acSubjectProp2;
    @BindView(R2.id.acSubjectProp3) AutoCompleteTextView acSubjectProp3;
    @BindView(R2.id.acSubjectProp4) AutoCompleteTextView acSubjectProp4;
    @BindView(R2.id.acSubjectProp5) AutoCompleteTextView acSubjectProp5;
    @BindView(R2.id.acSubjectProp6) AutoCompleteTextView acSubjectProp6;
    @BindView(R2.id.acSubjectProp7) AutoCompleteTextView acSubjectProp7;
    @BindView(R2.id.acSubjectProp8) AutoCompleteTextView acSubjectProp8;
    @BindView(R2.id.acSubjectProp9) AutoCompleteTextView acSubjectProp9;
    @BindView(R2.id.acSubjectProp10) AutoCompleteTextView acSubjectProp10;
    @BindView(R2.id.acSubjectProp11) AutoCompleteTextView acSubjectProp11;
    @BindView(R2.id.acSubjectProp12) AutoCompleteTextView acSubjectProp12;

    @BindView(R2.id.acComparable1) AutoCompleteTextView acComparable1;
    @BindView(R2.id.acComparable2) AutoCompleteTextView acComparable2;
    @BindView(R2.id.acComparable3) AutoCompleteTextView acComparable3;
    @BindView(R2.id.acComparable4) AutoCompleteTextView acComparable4;
    @BindView(R2.id.acComparable5) AutoCompleteTextView acComparable5;
    @BindView(R2.id.acComparable6) AutoCompleteTextView acComparable6;
    @BindView(R2.id.acComparable7) AutoCompleteTextView acComparable7;
    @BindView(R2.id.acComparable8) AutoCompleteTextView acComparable8;
    @BindView(R2.id.acComparable9) AutoCompleteTextView acComparable9;
    @BindView(R2.id.acComparable10) AutoCompleteTextView acComparable10;
    @BindView(R2.id.acComparable11) AutoCompleteTextView acComparable11;
    @BindView(R2.id.acComparable12) AutoCompleteTextView acComparable12;

    @BindView(R2.id.acAdjustment1) AutoCompleteTextView acAdjustment1;
    @BindView(R2.id.acAdjustment2) AutoCompleteTextView acAdjustment2;
    @BindView(R2.id.acAdjustment3) AutoCompleteTextView acAdjustment3;
    @BindView(R2.id.acAdjustment4) AutoCompleteTextView acAdjustment4;
    @BindView(R2.id.acAdjustment5) AutoCompleteTextView acAdjustment5;
    @BindView(R2.id.acAdjustment6) AutoCompleteTextView acAdjustment6;
    @BindView(R2.id.acAdjustment7) AutoCompleteTextView acAdjustment7;
    @BindView(R2.id.acAdjustment8) AutoCompleteTextView acAdjustment8;
    @BindView(R2.id.acAdjustment9) AutoCompleteTextView acAdjustment9;
    @BindView(R2.id.acAdjustment10) AutoCompleteTextView acAdjustment10;
    @BindView(R2.id.acAdjustment11) AutoCompleteTextView acAdjustment11;
    @BindView(R2.id.acAdjustment12) AutoCompleteTextView acAdjustment12;

    @BindView(R2.id.acValue1) AutoCompleteTextView acValue1;
    @BindView(R2.id.acValue2) AutoCompleteTextView acValue2;
    @BindView(R2.id.acValue3) AutoCompleteTextView acValue3;
    @BindView(R2.id.acValue4) AutoCompleteTextView acValue4;
    @BindView(R2.id.acValue5) AutoCompleteTextView acValue5;
    @BindView(R2.id.acValue6) AutoCompleteTextView acValue6;
    @BindView(R2.id.acValue7) AutoCompleteTextView acValue7;
    @BindView(R2.id.acValue8) AutoCompleteTextView acValue8;
    @BindView(R2.id.acValue9) AutoCompleteTextView acValue9;
    @BindView(R2.id.acValue10) AutoCompleteTextView acValue10;
    @BindView(R2.id.acValue11) AutoCompleteTextView acValue11;
    @BindView(R2.id.acValue12) AutoCompleteTextView acValue12;

    /*SUBTOTAL DISPLAY*/
    @BindView(R2.id.acTotalGross) AutoCompleteTextView acTotalGross;
    @BindView(R2.id.acTotalNetAdj) AutoCompleteTextView acTotalNetAdj;
    @BindView(R2.id.acAdjPrice) AutoCompleteTextView acAdjPrice;
    @BindView(R2.id.acWeight) AutoCompleteTextView acWeight;
    @BindView(R2.id.acWeightedVal) AutoCompleteTextView acWeightedVal;

    private UpdatePhysicalAdjustmentContract.Presenter presenter;

    private String recordId;
    private boolean isCalled = false;

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        Intent intent = Objects.requireNonNull(getActivity()).getIntent();
        recordId = intent.getStringExtra(RECORD_ID);

        presenter = new UpdatePhysicalAdjustmentPresenterImpl( this, Objects.requireNonNull(getActivity()).getApplication());

    }

    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container,
                             @Nullable Bundle savedInstanceState) {
        return inflater.inflate(R.layout.content_physical_adjustment, container, false);
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        unbinder = ButterKnife.bind(this, view);


        //TODO setup Dropdown state view
        AutoCompleteTextView[] viewDropdown = {
                acAdjustment1,
                acAdjustment2,
                acAdjustment3,
                acAdjustment4,
                acAdjustment5,
                acAdjustment6,
                acAdjustment7,
                acAdjustment8,
                acAdjustment9,
                acAdjustment10,
                acAdjustment11,
                acAdjustment12};

        AutoCompleteTextView[] viewValue = {
                acValue1,
                acValue2,
                acValue3,
                acValue4,
                acValue5,
                acValue6,
                acValue7,
                acValue8,
                acValue9,
                acValue10,
                acValue11,
                acValue12};
        presenter.setupDropdownState(getContext(), viewDropdown, viewValue);
    }

    @Override
    public void onResume() {
        super.onResume();
        presenter.onResume(isCalled, recordId);
        isCalled = true;
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        presenter.onDestroy();
    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();
        unbinder.unbind();
    }


    @Override
    public void showProgress(String title, String message) {

    }

    @Override
    public void hideProgress() {

    }

    @Override
    public void showSnackMessage(String message) {


    }

    @Override
    public void onPause() {
        super.onPause();
        new Handler().post(() -> presenter.updatePhysicalAdjustment2(
                recordId,
             /*   acElementType1.getText().toString(),
                acElementType2.getText().toString(),
                acElementType3.getText().toString(),
                acElementType4.getText().toString(),
                acElementType5.getText().toString(),
                acElementType6.getText().toString(),
                acElementType7.getText().toString(),
                acElementType8.getText().toString(),
                acElementType9.getText().toString(),
                acElementType10.getText().toString(),
                acElementType11.getText().toString(),
                acElementType12.getText().toString(),

                acSubjectProp1.getText().toString(),
                acSubjectProp2.getText().toString(),
                acSubjectProp3.getText().toString(),
                acSubjectProp4.getText().toString(),
                acSubjectProp5.getText().toString(),
                acSubjectProp6.getText().toString(),
                acSubjectProp7.getText().toString(),
                acSubjectProp8.getText().toString(),
                acSubjectProp9.getText().toString(),
                acSubjectProp10.getText().toString(),
                acSubjectProp11.getText().toString(),
                acSubjectProp12.getText().toString(),*/

                acComparable1.getText().toString(),
                acComparable2.getText().toString(),
                acComparable3.getText().toString(),
                acComparable4.getText().toString(),
                acComparable5.getText().toString(),
                acComparable6.getText().toString(),
                acComparable7.getText().toString(),
                acComparable8.getText().toString(),
                acComparable9.getText().toString(),
                acComparable10.getText().toString(),
                acComparable11.getText().toString(),
                acComparable12.getText().toString(),

                acAdjustment1.getText().toString(),
                acAdjustment2.getText().toString(),
                acAdjustment3.getText().toString(),
                acAdjustment4.getText().toString(),
                acAdjustment5.getText().toString(),
                acAdjustment6.getText().toString(),
                acAdjustment7.getText().toString(),
                acAdjustment8.getText().toString(),
                acAdjustment9.getText().toString(),
                acAdjustment10.getText().toString(),
                acAdjustment11.getText().toString(),
                acAdjustment12.getText().toString(),

                acValue1.getText().toString(),
                acValue2.getText().toString(),
                acValue3.getText().toString(),
                acValue4.getText().toString(),
                acValue5.getText().toString(),
                acValue6.getText().toString(),
                acValue7.getText().toString(),
                acValue8.getText().toString(),
                acValue9.getText().toString(),
                acValue10.getText().toString(),
                acValue11.getText().toString(),
                acValue12.getText().toString(),

                /*SUBTOTAL DISPLAY*/
                acTotalGross.getText().toString(),
                acTotalNetAdj.getText().toString(),
                acAdjPrice.getText().toString(),
                acWeight.getText().toString(),
                acWeightedVal.getText().toString()
        ));

    }

    @Override
    public void showDetails(Record record) {

        //Fix Value - Constant Hide Value TextInputLayout
        ((TextInputLayout)acElementType1.getParent().getParent()).setVisibility(View.GONE);
        ((TextInputLayout)acElementType2.getParent().getParent()).setVisibility(View.GONE);
        ((TextInputLayout)acElementType3.getParent().getParent()).setVisibility(View.GONE);
        ((TextInputLayout)acElementType4.getParent().getParent()).setVisibility(View.GONE);
        ((TextInputLayout)acElementType5.getParent().getParent()).setVisibility(View.GONE);
        ((TextInputLayout)acElementType6.getParent().getParent()).setVisibility(View.GONE);
        ((TextInputLayout)acElementType7.getParent().getParent()).setVisibility(View.GONE);
        ((TextInputLayout)acElementType8.getParent().getParent()).setVisibility(View.GONE);
        ((TextInputLayout)acElementType9.getParent().getParent()).setVisibility(View.GONE);
        ((TextInputLayout)acElementType10.getParent().getParent()).setVisibility(View.GONE);
        ((TextInputLayout)acElementType11.getParent().getParent()).setVisibility(View.GONE);
        ((TextInputLayout)acElementType12.getParent().getParent()).setVisibility(View.GONE);
        ((TextInputLayout)acSubjectProp1.getParent().getParent()).setVisibility(View.GONE);
        ((TextInputLayout)acSubjectProp2.getParent().getParent()).setVisibility(View.GONE);
        ((TextInputLayout)acSubjectProp3.getParent().getParent()).setVisibility(View.GONE);
        ((TextInputLayout)acSubjectProp4.getParent().getParent()).setVisibility(View.GONE);
        ((TextInputLayout)acSubjectProp5.getParent().getParent()).setVisibility(View.GONE);
        ((TextInputLayout)acSubjectProp6.getParent().getParent()).setVisibility(View.GONE);
        ((TextInputLayout)acSubjectProp7.getParent().getParent()).setVisibility(View.GONE);
        ((TextInputLayout)acSubjectProp8.getParent().getParent()).setVisibility(View.GONE);
        ((TextInputLayout)acSubjectProp9.getParent().getParent()).setVisibility(View.GONE);
        ((TextInputLayout)acSubjectProp10.getParent().getParent()).setVisibility(View.GONE);
        ((TextInputLayout)acSubjectProp11.getParent().getParent()).setVisibility(View.GONE);
        ((TextInputLayout)acSubjectProp12.getParent().getParent()).setVisibility(View.GONE);

/*        acElementType1.setText(record.getValrepLandimpComp1RecLocationType());
        acElementType2.setText(record.getValrepLandimpComp1RecSizeType());
        acElementType3.setText(record.getValrepLandimpComp1RecShapeType());
        acElementType4.setText(record.getValrepLandimpComp1RecTerrainType());
        acElementType5.setText(record.getValrepLandimpComp1RecElevationType());
        acElementType6.setText(record.getValrepLandimpComp1RecNeighborhoodType());
        acElementType7.setText(record.getValrepLandimpComp1RecAccessibilityType());
        acElementType8.setText(record.getValrepLandimpComp1RecLotTypeType());
        acElementType9.setText(record.getValrepLandimpComp1RecRoadType());
        acElementType10.setText(record.getValrepLandimpComp1RecOthersType());
        acElementType11.setText(record.getValrepLandimpComp1RecOthers2Type());
        acElementType12.setText(record.getValrepLandimpComp1RecOthers3Type());
        acSubjectProp1.setText(record.getValrepLandimpComp1RecLocationSubprop());
        acSubjectProp2.setText(record.getValrepLandimpComp1RecSizeSubprop());
        acSubjectProp3.setText(record.getValrepLandimpComp1RecShapeSubprop());
        acSubjectProp4.setText(record.getValrepLandimpComp1RecTerrainSubprop());
        acSubjectProp5.setText(record.getValrepLandimpComp1RecElevationSubprop());
        acSubjectProp6.setText(record.getValrepLandimpComp1RecNeighborhoodSubprop());
        acSubjectProp7.setText(record.getValrepLandimpComp1RecAccessibilitySubprop());
        acSubjectProp8.setText(record.getValrepLandimpComp1RecLotTypeSubprop());
        acSubjectProp9.setText(record.getValrepLandimpComp1RecRoadSubprop());
        acSubjectProp10.setText(record.getValrepLandimpComp1RecOthersSubprop());
        acSubjectProp11.setText(record.getValrepLandimpComp1RecOthers2Subprop());
        acSubjectProp12.setText(record.getValrepLandimpComp1RecOthers3Subprop());*/

        acComparable1.setText(record.getValrepLandimpComp2RecLocationDescription());
        acComparable2.setText(record.getValrepLandimpComp2RecSizeDescription());
        acComparable3.setText(record.getValrepLandimpComp2RecShapeDescription());
        acComparable4.setText(record.getValrepLandimpComp2RecTerrainDescription());
        acComparable5.setText(record.getValrepLandimpComp2RecElevationDescription());
        acComparable6.setText(record.getValrepLandimpComp2RecNeighborhoodDescription());
        acComparable7.setText(record.getValrepLandimpComp2RecAccessibilityDescription());
        acComparable8.setText(record.getValrepLandimpComp2RecLotTypeDescription());
        acComparable9.setText(record.getValrepLandimpComp2RecRoadDescription());
        acComparable10.setText(record.getValrepLandimpComp2RecOthersDescription());
        acComparable11.setText(record.getValrepLandimpComp2RecOthers2Description());
        acComparable12.setText(record.getValrepLandimpComp2RecOthers3Description());

        acAdjustment1.setText(record.getValrepLandimpComp2RecLocationDesc());
        acAdjustment2.setText(record.getValrepLandimpComp2RecSizeDesc());
        acAdjustment3.setText(record.getValrepLandimpComp2RecShapeDesc());
        acAdjustment4.setText(record.getValrepLandimpComp2RecTerrainDesc());
        acAdjustment5.setText(record.getValrepLandimpComp2RecElevationDesc());
        acAdjustment6.setText(record.getValrepLandimpComp2RecNeighborhoodDesc());
        acAdjustment7.setText(record.getValrepLandimpComp2RecAccessibilityDesc());
        acAdjustment8.setText(record.getValrepLandimpComp2RecLotTypeDesc());
        acAdjustment9.setText(record.getValrepLandimpComp2RecRoadDesc());
        acAdjustment10.setText(record.getValrepLandimpComp2RecOthersDesc());
        acAdjustment11.setText(record.getValrepLandimpComp2RecOthers2Desc());
        acAdjustment12.setText(record.getValrepLandimpComp2RecOthers3Desc());

        acValue1.setText(record.getValrepLandimpComp2RecLocation());
        acValue2.setText(record.getValrepLandimpComp2RecSize());
        acValue3.setText(record.getValrepLandimpComp2RecShape());
        acValue4.setText(record.getValrepLandimpComp2RecTerrain());
        acValue5.setText(record.getValrepLandimpComp2RecElevation());
        acValue6.setText(record.getValrepLandimpComp2RecNeighborhood());
        acValue7.setText(record.getValrepLandimpComp2RecAccessibility());
        acValue8.setText(record.getValrepLandimpComp2RecLotType());
        acValue9.setText(record.getValrepLandimpComp2RecRoad());
        acValue10.setText(record.getValrepLandimpComp2RecOthers());
        acValue11.setText(record.getValrepLandimpComp2RecOthers2());
        acValue12.setText(record.getValrepLandimpComp2RecOthers3());


        /*SUBTOTAL DISPLAY*/
        acTotalGross.setText(record.getValrepLandimpComp2TotalgrossAdj());
        acTotalNetAdj.setText(record.getValrepLandimpComp2TotalAdj());
        acAdjPrice.setText(record.getValrepLandimpComp2AdjustedValue());
        acWeight.setText(record.getValrepLandimpComp2Weight());
        acWeightedVal.setText(record.getValrepLandimpComp2WeightEquivalent());


        //setDropdown Display
        acAdjustment1.setAdapter(GET_CONTENT_ADAPTER(record.getValrepLandimpComp2RecLocationDesc(),
                R.array.physAdjustmentArray, Objects.requireNonNull(getContext())));
        acAdjustment2.setAdapter(GET_CONTENT_ADAPTER(record.getValrepLandimpComp2RecSizeDesc(),
                R.array.physAdjustmentArray, Objects.requireNonNull(getContext())));
        acAdjustment3.setAdapter(GET_CONTENT_ADAPTER(record.getValrepLandimpComp2RecShapeDesc(),
                R.array.physAdjustmentArray, Objects.requireNonNull(getContext())));
        acAdjustment4.setAdapter(GET_CONTENT_ADAPTER(record.getValrepLandimpComp2RecTerrainDesc(),
                R.array.physAdjustmentArray, Objects.requireNonNull(getContext())));
        acAdjustment5.setAdapter(GET_CONTENT_ADAPTER(record.getValrepLandimpComp2RecElevationDesc(),
                R.array.physAdjustmentArray, Objects.requireNonNull(getContext())));
        acAdjustment6.setAdapter(GET_CONTENT_ADAPTER(record.getValrepLandimpComp2RecNeighborhoodDesc(),
                R.array.physAdjustmentArray, Objects.requireNonNull(getContext())));
        acAdjustment7.setAdapter(GET_CONTENT_ADAPTER(record.getValrepLandimpComp2RecAccessibilityDesc(),
                R.array.physAdjustmentArray, Objects.requireNonNull(getContext())));
        acAdjustment8.setAdapter(GET_CONTENT_ADAPTER(record.getValrepLandimpComp2RecLotTypeDesc(),
                R.array.physAdjustmentArray, Objects.requireNonNull(getContext())));
        acAdjustment9.setAdapter(GET_CONTENT_ADAPTER(record.getValrepLandimpComp2RecRoadDesc(),
                R.array.physAdjustmentArray, Objects.requireNonNull(getContext())));
        acAdjustment10.setAdapter(GET_CONTENT_ADAPTER(record.getValrepLandimpComp2RecOthersDesc(),
                R.array.physAdjustmentArray, Objects.requireNonNull(getContext())));
        acAdjustment11.setAdapter(GET_CONTENT_ADAPTER(record.getValrepLandimpComp2RecOthers2Desc(),
                R.array.physAdjustmentArray, Objects.requireNonNull(getContext())));
        acAdjustment12.setAdapter(GET_CONTENT_ADAPTER(record.getValrepLandimpComp2RecOthers3Desc(),
                R.array.physAdjustmentArray, Objects.requireNonNull(getContext())));

        acAdjustment1.setOnClickListener(v -> acAdjustment1.showDropDown());
        acAdjustment2.setOnClickListener(v -> acAdjustment2.showDropDown());
        acAdjustment3.setOnClickListener(v -> acAdjustment3.showDropDown());
        acAdjustment4.setOnClickListener(v -> acAdjustment4.showDropDown());
        acAdjustment5.setOnClickListener(v -> acAdjustment5.showDropDown());
        acAdjustment6.setOnClickListener(v -> acAdjustment6.showDropDown());
        acAdjustment7.setOnClickListener(v -> acAdjustment7.showDropDown());
        acAdjustment8.setOnClickListener(v -> acAdjustment8.showDropDown());
        acAdjustment9.setOnClickListener(v -> acAdjustment9.showDropDown());
        acAdjustment10.setOnClickListener(v -> acAdjustment10.showDropDown());
        acAdjustment11.setOnClickListener(v -> acAdjustment11.showDropDown());
        acAdjustment12.setOnClickListener(v -> acAdjustment12.showDropDown());

        acValue1.addTextChangedListener(AC_VALUE1(record.getValrepLandimpComp2AdjustablePriceSqm(), acValue1, acValue2, acValue3, acValue4, acValue5, acValue6, acValue7, acValue8, acValue9, acValue10, acValue11, acValue12, acTotalGross, acTotalNetAdj, acAdjPrice, acWeight, acWeightedVal));
        acValue2.addTextChangedListener(AC_VALUE2(record.getValrepLandimpComp2AdjustablePriceSqm(), acValue1, acValue2, acValue3, acValue4, acValue5, acValue6, acValue7, acValue8, acValue9, acValue10, acValue11, acValue12, acTotalGross, acTotalNetAdj, acAdjPrice, acWeight, acWeightedVal));
        acValue3.addTextChangedListener(AC_VALUE3(record.getValrepLandimpComp2AdjustablePriceSqm(), acValue1, acValue2, acValue3, acValue4, acValue5, acValue6, acValue7, acValue8, acValue9, acValue10, acValue11, acValue12, acTotalGross, acTotalNetAdj, acAdjPrice, acWeight, acWeightedVal));
        acValue4.addTextChangedListener(AC_VALUE4(record.getValrepLandimpComp2AdjustablePriceSqm(), acValue1, acValue2, acValue3, acValue4, acValue5, acValue6, acValue7, acValue8, acValue9, acValue10, acValue11, acValue12, acTotalGross, acTotalNetAdj, acAdjPrice, acWeight, acWeightedVal));
        acValue5.addTextChangedListener(AC_VALUE5(record.getValrepLandimpComp2AdjustablePriceSqm(), acValue1, acValue2, acValue3, acValue4, acValue5, acValue6, acValue7, acValue8, acValue9, acValue10, acValue11, acValue12, acTotalGross, acTotalNetAdj, acAdjPrice, acWeight, acWeightedVal));
        acValue6.addTextChangedListener(AC_VALUE6(record.getValrepLandimpComp2AdjustablePriceSqm(), acValue1, acValue2, acValue3, acValue4, acValue5, acValue6, acValue7, acValue8, acValue9, acValue10, acValue11, acValue12, acTotalGross, acTotalNetAdj, acAdjPrice, acWeight, acWeightedVal));
        acValue7.addTextChangedListener(AC_VALUE7(record.getValrepLandimpComp2AdjustablePriceSqm(), acValue1, acValue2, acValue3, acValue4, acValue5, acValue6, acValue7, acValue8, acValue9, acValue10, acValue11, acValue12, acTotalGross, acTotalNetAdj, acAdjPrice, acWeight, acWeightedVal));
        acValue8.addTextChangedListener(AC_VALUE8(record.getValrepLandimpComp2AdjustablePriceSqm(), acValue1, acValue2, acValue3, acValue4, acValue5, acValue6, acValue7, acValue8, acValue9, acValue10, acValue11, acValue12, acTotalGross, acTotalNetAdj, acAdjPrice, acWeight, acWeightedVal));
        acValue9.addTextChangedListener(AC_VALUE9(record.getValrepLandimpComp2AdjustablePriceSqm(), acValue1, acValue2, acValue3, acValue4, acValue5, acValue6, acValue7, acValue8, acValue9, acValue10, acValue11, acValue12, acTotalGross, acTotalNetAdj, acAdjPrice, acWeight, acWeightedVal));
        acValue10.addTextChangedListener(AC_VALUE10(record.getValrepLandimpComp2AdjustablePriceSqm(), acValue1, acValue2, acValue3, acValue4, acValue5, acValue6, acValue7, acValue8, acValue9, acValue10, acValue11, acValue12, acTotalGross, acTotalNetAdj, acAdjPrice, acWeight, acWeightedVal));
        acValue11.addTextChangedListener(AC_VALUE11(record.getValrepLandimpComp2AdjustablePriceSqm(), acValue1, acValue2, acValue3, acValue4, acValue5, acValue6, acValue7, acValue8, acValue9, acValue10, acValue11, acValue12, acTotalGross, acTotalNetAdj, acAdjPrice, acWeight, acWeightedVal));
        acValue12.addTextChangedListener(AC_VALUE12(record.getValrepLandimpComp2AdjustablePriceSqm(), acValue1, acValue2, acValue3, acValue4, acValue5, acValue6, acValue7, acValue8, acValue9, acValue10, acValue11, acValue12, acTotalGross, acTotalNetAdj, acAdjPrice, acWeight, acWeightedVal));
        acWeight.addTextChangedListener(AC_WEIGHT(record.getValrepLandimpComp2AdjustablePriceSqm(), acValue1, acValue2, acValue3, acValue4, acValue5, acValue6, acValue7, acValue8, acValue9, acValue10, acValue11, acValue12, acTotalGross, acTotalNetAdj, acAdjPrice, acWeight, acWeightedVal));
    }

    @Override
    public void showExitDialog(String title, String message) {

    }

    @Override
    public void showErrorDialog(String title, String message) {

    }
}
