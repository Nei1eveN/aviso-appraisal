package com.gdslinkasia.landvaluation.physical_adjustment;

import android.os.Bundle;

import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;
import androidx.viewpager.widget.ViewPager;

import com.gdslinkasia.base.adapter.ViewPagerAdapter;
import com.gdslinkasia.commonresources.R2;
import com.gdslinkasia.landvaluation.R;
import com.google.android.material.tabs.TabLayout;

import java.util.Objects;

import butterknife.BindView;
import butterknife.ButterKnife;

public class PhysicalAdjustmentActivity extends AppCompatActivity {

    @BindView(R2.id.tabLayout)
    TabLayout tabLayout;
    @BindView(R2.id.viewPager)
    ViewPager viewPager;

    ViewPagerAdapter pagerAdapter = new ViewPagerAdapter(getSupportFragmentManager());

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_market_data_analysis);
        ButterKnife.bind(this);

        Objects.requireNonNull(getSupportActionBar()).setTitle("Physical Adjustments");
//        getSupportActionBar().setIcon(R.drawable.ic_close_white_24dp);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);

        pagerAdapter.addFragment(new PhysicalAdjustment1Fragment(), "Physical Adjustment 1");
        pagerAdapter.addFragment(new PhysicalAdjustment2Fragment(), "Physical Adjustment 2");
        pagerAdapter.addFragment(new PhysicalAdjustment3Fragment(), "Physical Adjustment 3");
        pagerAdapter.addFragment(new PhysicalAdjustment4Fragment(), "Physical Adjustment 4");
        pagerAdapter.addFragment(new PhysicalAdjustment5Fragment(), "Physical Adjustment 5");
        viewPager.setAdapter(pagerAdapter);
        viewPager.setCurrentItem(0);
        viewPager.setOffscreenPageLimit(5);
        tabLayout.setupWithViewPager(viewPager);
}


    @Override
    public void onBackPressed() {
        super.onBackPressed();
        finish();
    }
}
