package com.gdslinkasia.landvaluation;

import android.app.ProgressDialog;
import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;
import android.view.View;
import android.widget.AutoCompleteTextView;
import android.widget.CheckBox;
import android.widget.LinearLayout;

import androidx.appcompat.app.AppCompatActivity;
import androidx.fragment.app.FragmentTransaction;

import com.gdslinkasia.base.model.job.Record;
import com.gdslinkasia.commonresources.R2;
import com.gdslinkasia.landvaluation.market_approach.LandMarketApproachListActivity;
import com.gdslinkasia.landvaluation.market_data_analysis.MarketDataAnalysisActivity;
import com.gdslinkasia.landvaluation.parking.CondoParkingFragment;
import com.gdslinkasia.landvaluation.physical_adjustment.PhysicalAdjustmentActivity;
import com.gdslinkasia.landvaluation.price_adjustment.PriceAdjustmentActivity;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

import static com.gdslinkasia.base.database.DatabaseUtils.*;
import static com.gdslinkasia.base.utils.GlobalFunctions.SET_CONTENT_DROPDOWN;
import static com.gdslinkasia.base.utils.GlobalFunctions.getCheckboxBoolean;
import static com.gdslinkasia.base.utils.GlobalString.CHECKBOX_TO_STRING;

public class LandValuationActivity extends AppCompatActivity implements UpdateLandValContract.View {

    @BindView(R2.id.acKeyInput) AutoCompleteTextView acKeyInput;
    @BindView(R2.id.acPropertyMarket) AutoCompleteTextView acPropertyMarket;
    
    @BindView(R2.id.llValuationLandMarket) LinearLayout llValuationLandMarket;
    @BindView(R2.id.llValuationParking) LinearLayout llValuationParking;

    @BindView(R2.id.llSupplyDemandLand) LinearLayout llSupplyDemandLand;
    @BindView(R2.id.llSupplyDemandCondoLand) LinearLayout llSupplyDemandCondoLand;

    @BindView(R2.id.acSupplyDemandCondo1) AutoCompleteTextView acSupplyDemandCondo1;
    @BindView(R2.id.acSupplyDemandCondo2) AutoCompleteTextView acSupplyDemandCondo2;
    @BindView(R2.id.acSupplyDemandCondo3) AutoCompleteTextView acSupplyDemandCondo3;
    @BindView(R2.id.acSupplyDemandCondo4) AutoCompleteTextView acSupplyDemandCondo4;
    @BindView(R2.id.acSupplyDemandCondo5) AutoCompleteTextView acSupplyDemandCondo5;
    @BindView(R2.id.acSupplyDemandCondo6) AutoCompleteTextView acSupplyDemandCondo6;
    @BindView(R2.id.acSupplyDemandCondo7) AutoCompleteTextView acSupplyDemandCondo7;
    @BindView(R2.id.acSupplyDemandCondo8) AutoCompleteTextView acSupplyDemandCondo8;
    @BindView(R2.id.acSupplyDemandCondo9) AutoCompleteTextView acSupplyDemandCondo9;


    @BindView(R2.id.acSupplyDemand1) AutoCompleteTextView acSupplyDemand1;
    @BindView(R2.id.acSupplyDemand2) AutoCompleteTextView acSupplyDemand2;
    @BindView(R2.id.acSupplyDemand3) AutoCompleteTextView acSupplyDemand3;
    @BindView(R2.id.acSupplyDemand4) AutoCompleteTextView acSupplyDemand4;
    @BindView(R2.id.acSupplyDemand5) AutoCompleteTextView acSupplyDemand5;
    @BindView(R2.id.acSupplyDemand6) AutoCompleteTextView acSupplyDemand6;
    @BindView(R2.id.acSupplyDemand7) AutoCompleteTextView acSupplyDemand7;
    @BindView(R2.id.acSupplyDemand8) AutoCompleteTextView acSupplyDemand8;
    @BindView(R2.id.acSupplyDemand9) AutoCompleteTextView acSupplyDemand9;
    @BindView(R2.id.acSupplyDemand10) AutoCompleteTextView acSupplyDemand10;

    @BindView(R2.id.acMarketPhase1) AutoCompleteTextView acMarketPhase1;
    @BindView(R2.id.acMarketPhase2) AutoCompleteTextView acMarketPhase2;
    @BindView(R2.id.acMarketPhase3) AutoCompleteTextView acMarketPhase3;
    @BindView(R2.id.acMarketPhase4) AutoCompleteTextView acMarketPhase4;
    @BindView(R2.id.acMarketPhase5) AutoCompleteTextView acMarketPhase5;

    @BindView(R2.id.acTypicalBuyer1) AutoCompleteTextView acTypicalBuyer1;

    //Total Value of Computations
    @BindView(R2.id.acIndicatedValueSqm) AutoCompleteTextView acIndicatedValueSqm;
    @BindView(R2.id.acRounded) AutoCompleteTextView acRounded;
    @BindView(R2.id.acIndicatedValueRounded) AutoCompleteTextView acIndicatedValueRounded;
    @BindView(R2.id.cbComparableMap) CheckBox cbComparableMap;
    @BindView(R2.id.acTotalRounded) AutoCompleteTextView acTotalRounded;

    @BindView(R2.id.acTotalLandArea) AutoCompleteTextView acTotalLandArea;
    @BindView(R2.id.acLessRestriction) AutoCompleteTextView acLessRestriction;
    @BindView(R2.id.acNetUsable) AutoCompleteTextView acNetUsable;
    @BindView(R2.id.acTotalMarketValue) AutoCompleteTextView acTotalMarketValue;


    private String recordId;
    private UpdateLandValContract.Presenter presenter;

    private Intent intent;
    private ProgressDialog progressDialog;
    private String appraisalType;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.form_land_valuation);
        ButterKnife.bind(this);

        intent = getIntent();
        recordId = intent.getStringExtra(RECORD_ID);
        appraisalType = intent.getStringExtra(APPRAISAL_TYPE);
        
        presenter = new UpdateLandValPresenterImpl(this, getApplication());


        progressDialog = new ProgressDialog(this);


        presenter.onStart(recordId);

    }

    @OnClick(R2.id.priceAdjustments)
    void onClickPriceAdjustments() {
        intent = new Intent(this, PriceAdjustmentActivity.class);
        intent.putExtra(RECORD_ID, recordId);

        startActivity(intent);
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        presenter.onDestroy();
        progressDialog.dismiss();
    }

   @OnClick(R2.id.marketDataAnalysisLayout)
    void onClickMarketDataAnalysis() {
        intent = new Intent(this, MarketDataAnalysisActivity.class);
        intent.putExtra(RECORD_ID, recordId);

        startActivity(intent);
    }

    @OnClick(R2.id.physicalAdjustments)
    void onClickLandValuation() {
        intent = new Intent(this, PhysicalAdjustmentActivity.class);
        intent.putExtra(RECORD_ID, recordId);

        startActivity(intent);
    }

    @OnClick(R2.id.llValuationLandMarket)
    void onClickLandMarketValuation() {

        intent = new Intent(this, LandMarketApproachListActivity.class);
        intent.putExtra(RECORD_ID, recordId);
        startActivity(intent);

//        Bundle bundle = new Bundle();
//        bundle.putString(RECORD_ID, recordId);
//        bundle.putString(APPRAISAL_TYPE, appraisalType);
//
//        OwnershipPropertyDescFragment dialogFragment = new OwnershipPropertyDescFragment();
//        dialogFragment.setArguments(bundle);
//        FragmentTransaction fragmentTransaction = getSupportFragmentManager().beginTransaction();
//        dialogFragment.show(fragmentTransaction, OwnershipPropertyDescFragment.TAG);

    }


    @OnClick(R2.id.llValuationParking)
    void onClickParkingValuation() {
//        startActivity(intent);
        Bundle bundle = new Bundle();
        bundle.putString(RECORD_ID, recordId);
        bundle.putString(APPRAISAL_TYPE, appraisalType);

        CondoParkingFragment dialogFragment = new CondoParkingFragment();
        dialogFragment.setArguments(bundle);
        FragmentTransaction fragmentTransaction = getSupportFragmentManager().beginTransaction();
        dialogFragment.show(fragmentTransaction, CondoParkingFragment.TAG);
    }



    @Override
    public void showProgress(String title, String message) {
        progressDialog.setCancelable(false);
        progressDialog.setIndeterminate(false);
        progressDialog.setTitle(title);
        progressDialog.setMessage(message);
        progressDialog.show();
    }

    @Override
    public void hideProgress() {
        progressDialog.hide();
    }

    @Override
    public void showSnackMessage(String message) {

    }

    @Override
    protected void onPause() {
        super.onPause();

        new Handler().post(() -> presenter.updateLandVal(
                recordId,
                acKeyInput.getText().toString(),
                acPropertyMarket.getText().toString(),

                //for condo
                acSupplyDemandCondo1.getText().toString(),
                acSupplyDemandCondo2.getText().toString(),
                acSupplyDemandCondo3.getText().toString(),
                acSupplyDemandCondo4.getText().toString(),
                acSupplyDemandCondo5.getText().toString(),
                acSupplyDemandCondo6.getText().toString(),
                acSupplyDemandCondo7.getText().toString(),
                acSupplyDemandCondo8.getText().toString(),
                acSupplyDemandCondo9.getText().toString(),

                acSupplyDemand1.getText().toString(),
                acSupplyDemand2.getText().toString(),
                acSupplyDemand3.getText().toString(),
                acSupplyDemand4.getText().toString(),
                acSupplyDemand5.getText().toString(),
                acSupplyDemand6.getText().toString(),
                acSupplyDemand7.getText().toString(),
                acSupplyDemand8.getText().toString(),
                acSupplyDemand9.getText().toString(),
                acSupplyDemand10.getText().toString(),

                acMarketPhase1.getText().toString(),
                acMarketPhase2.getText().toString(),
                acMarketPhase3.getText().toString(),
                acMarketPhase4.getText().toString(),
                acMarketPhase5.getText().toString(),

                acTypicalBuyer1.getText().toString(),

                //Total Value of Computations
                acIndicatedValueSqm.getText().toString(),
                acRounded.getText().toString(),
                acIndicatedValueRounded.getText().toString(),
                CHECKBOX_TO_STRING(cbComparableMap),
                acTotalRounded.getText().toString(),

                acTotalLandArea.getText().toString(),
                acLessRestriction.getText().toString(),
                acNetUsable.getText().toString()
        ));


    }

    @Override
    public void showDetails(Record record) {

        if(appraisalType.equals(VAR_CONDO)){
            llSupplyDemandLand.setVisibility(View.GONE);
            llSupplyDemandCondoLand.setVisibility(View.VISIBLE);

            llValuationLandMarket.setVisibility(View.GONE);
            llValuationParking.setVisibility(View.VISIBLE);

            ((View)acTotalLandArea.getParent().getParent()).setVisibility(View.GONE);
            ((View)acLessRestriction.getParent().getParent()).setVisibility(View.GONE);
            ((View)acNetUsable.getParent().getParent()).setVisibility(View.GONE);
        }else{
            llSupplyDemandCondoLand.setVisibility(View.GONE);
            llValuationParking.setVisibility(View.GONE);
        }

        acKeyInput.setText(record.getLcValuationKeyInput());
        acPropertyMarket.setText(record.getLcValuationKeyInput());

        //for condo only
        acSupplyDemand1.setText(record.getValrepSupplyanddemand1());
        acSupplyDemand2.setText(record.getValrepSupplyanddemand2());
        acSupplyDemand3.setText(record.getValrepSupplyanddemand3());
        acSupplyDemand4.setText(record.getValrepSupplyanddemand4());
        acSupplyDemand5.setText(record.getValrepSupplyanddemand5());
        acSupplyDemand6.setText(record.getValrepSupplyanddemand6());
        acSupplyDemand7.setText(record.getValrepSupplyanddemand7());
        acSupplyDemand8.setText(record.getValrepSupplyanddemand8());
        acSupplyDemand9.setText(record.getValrepSupplyanddemand9());


        //for land imp and land only
        acSupplyDemand1.setText(record.getValrepLandimpSupplyanddemand1());
        acSupplyDemand2.setText(record.getValrepLandimpSupplyanddemand2());
        acSupplyDemand3.setText(record.getValrepLandimpSupplyanddemand3());
        acSupplyDemand4.setText(record.getValrepLandimpSupplyanddemand4());
        acSupplyDemand5.setText(record.getValrepLandimpSupplyanddemand5());
        acSupplyDemand6.setText(record.getValrepLandimpSupplyanddemand6());
        acSupplyDemand7.setText(record.getValrepLandimpSupplyanddemand7());
        acSupplyDemand8.setText(record.getValrepLandimpSupplyanddemand8());
        acSupplyDemand9.setText(record.getValrepLandimpSupplyanddemand9());
        acSupplyDemand10.setText(record.getValrepLandimpSupplyanddemand10());

        acMarketPhase1.setText(record.getValrepMarketphase1());
        acMarketPhase2.setText(record.getValrepMarketphase2());
        acMarketPhase3.setText(record.getValrepMarketphase3());
        acMarketPhase4.setText(record.getValrepMarketphase4());
        acMarketPhase5.setText(record.getValrepMarketphase5());

        acTypicalBuyer1.setText(record.getValrepTypicalbuyer1());


        //Total Value of Computations
        acIndicatedValueSqm.setText(record.getValrepLandimpWeightedUnitValue());
        acRounded.setText(record.getValrepLandimpCompRoundedToNearest());
        acIndicatedValueRounded.setText(record.getValrepLandimpCompRoundedTo());
        cbComparableMap.setChecked(getCheckboxBoolean(record.getValrepLandimpCompWithCompMaps()));
        acTotalRounded.setText(record.getValrepLandimpWeightedUnitValue());

        acTotalLandArea.setText(record.getValrepLandimpValueTotalArea());
        acLessRestriction.setText(record.getValrepLandimpValueTotalDeduction());
        acNetUsable.setText(record.getValrepLandimpValueTotalNetArea());

        acTotalMarketValue.setText(record.getValrepLandimpValueTotalLandimpValue());

        //setup dropdown and hint
        SET_CONTENT_DROPDOWN(this, acKeyInput, R.array.keyInputArray, false);
        SET_CONTENT_DROPDOWN(this, acPropertyMarket, R.array.propertyMarketTypeArray, false);

        SET_CONTENT_DROPDOWN(this, acSupplyDemandCondo1, R.array.supplyDemandCondo1Array, true);
        SET_CONTENT_DROPDOWN(this, acSupplyDemandCondo2, R.array.supplyDemandCondo2Array, true);
        SET_CONTENT_DROPDOWN(this, acSupplyDemandCondo3, R.array.supplyDemandCondo3Array, true);
        SET_CONTENT_DROPDOWN(this, acSupplyDemandCondo4, R.array.supplyDemandCondo4Array, true);
        SET_CONTENT_DROPDOWN(this, acSupplyDemandCondo5, R.array.supplyDemandCondo5Array, true);
        SET_CONTENT_DROPDOWN(this, acSupplyDemandCondo6, R.array.supplyDemandCondo6Array, true);
        SET_CONTENT_DROPDOWN(this, acSupplyDemandCondo7, R.array.supplyDemandCondo7Array, true);
        SET_CONTENT_DROPDOWN(this, acSupplyDemandCondo8, R.array.supplyDemandCondo8Array, true);
        SET_CONTENT_DROPDOWN(this, acSupplyDemandCondo9, R.array.supplyDemandCondo9Array, true);

        SET_CONTENT_DROPDOWN(this, acSupplyDemand1, R.array.supplyDemand1Array, true);
        SET_CONTENT_DROPDOWN(this, acSupplyDemand2, R.array.supplyDemand2Array, true);
        SET_CONTENT_DROPDOWN(this, acSupplyDemand3, R.array.supplyDemand3Array, true);
        SET_CONTENT_DROPDOWN(this, acSupplyDemand4, R.array.supplyDemand4Array, true);
        SET_CONTENT_DROPDOWN(this, acSupplyDemand5, R.array.supplyDemand5Array, true);
        SET_CONTENT_DROPDOWN(this, acSupplyDemand6, R.array.supplyDemand6Array, true);
        SET_CONTENT_DROPDOWN(this, acSupplyDemand7, R.array.supplyDemand7Array, true);
        SET_CONTENT_DROPDOWN(this, acSupplyDemand8, R.array.supplyDemand8Array, true);
        SET_CONTENT_DROPDOWN(this, acSupplyDemand9, R.array.supplyDemand9Array, true);
        SET_CONTENT_DROPDOWN(this, acSupplyDemand10, R.array.supplyDemand10Array, true);

        SET_CONTENT_DROPDOWN(this, acMarketPhase1, R.array.marketPhase1Array, true);
        SET_CONTENT_DROPDOWN(this, acMarketPhase2, R.array.marketPhase2Array, true);
        SET_CONTENT_DROPDOWN(this, acMarketPhase3, R.array.marketPhase3Array, true);
        SET_CONTENT_DROPDOWN(this, acMarketPhase4, R.array.marketPhase4Array, true);
        SET_CONTENT_DROPDOWN(this, acMarketPhase5, R.array.marketPhase5Array, true);

        SET_CONTENT_DROPDOWN(this, acTypicalBuyer1, R.array.typicalBuyer1Array, true);

        SET_CONTENT_DROPDOWN(this, acRounded, R.array.roundedToNearestArray, true);
        SET_CONTENT_DROPDOWN(this, acRounded, R.array.roundedToNearestArray, true);
    }

    @Override
    public void showExitDialog(String title, String message) {

    }

    @Override
    public void showErrorDialog(String title, String message) {

    }
}
