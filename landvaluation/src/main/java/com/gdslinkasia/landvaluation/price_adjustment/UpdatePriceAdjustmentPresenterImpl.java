package com.gdslinkasia.landvaluation.price_adjustment;

import android.app.Application;
import android.content.Context;
import android.os.Handler;
import android.util.Log;
import android.widget.AutoCompleteTextView;

import com.gdslinkasia.base.model.job.Record;
import com.gdslinkasia.base.utils.GlobalFunctions;
import com.gdslinkasia.landvaluation.R;

import java.util.Objects;

import io.reactivex.Single;
import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.disposables.CompositeDisposable;
import io.reactivex.observers.DisposableSingleObserver;
import io.reactivex.schedulers.Schedulers;

import static com.gdslinkasia.base.utils.GlobalFunctions.GET_CONTENT_ADAPTER;


public class UpdatePriceAdjustmentPresenterImpl implements UpdatePriceAdjustmentContract.Presenter {

    private UpdatePriceAdjustmentContract.View view;
    private UpdatePriceAdjustmentContract.Interactor interactor;

    private CompositeDisposable compositeDisposable = new CompositeDisposable();

    public UpdatePriceAdjustmentPresenterImpl(UpdatePriceAdjustmentContract.View view, Application application) {
        this.view = view;
        this.interactor = new UpdatePriceAdjustmentInteractorImpl(application);
    }


    @Override
    public void onResume(boolean isCalled, String recordId) {
        if (!isCalled) {
            view.showProgress("Loading Record", "Please wait...");
            compositeDisposable.add(getLocalSingleObserver(recordId).subscribeWith(getLocalObserver()));
        } else {
            Log.d("int--isCalled", "Method already called");
        }
    }

    @Override
    public void setupDropdownState(Context context, AutoCompleteTextView[] viewDropdown, AutoCompleteTextView[] viewValue) {
        //value per dropdown
        String[] contentPhysValueArray = Objects.requireNonNull(context).getResources()
                .getStringArray(R.array.priceAdjustmentValueArray);

        for(int i=0; i <viewDropdown.length; i++){
            if(viewDropdown[i] != null){
                //setDropdown Display
                int finalI = i;
                //get Parent of AutoCompleteView ISA ISA TAWAG
                viewDropdown[i].setAdapter(GET_CONTENT_ADAPTER(viewDropdown[i].getText().toString(), R.array.priceAdjustmentArray, Objects.requireNonNull(context)));
                viewDropdown[i].setOnClickListener(v -> { viewDropdown[finalI].showDropDown(); });
                viewDropdown[i].setKeyListener(null);
                viewValue[i].setEnabled(false);
                viewDropdown[i].setOnItemClickListener(new GlobalFunctions.AutoCompleteClickSetValueListener(viewDropdown[i], viewValue[i], contentPhysValueArray));
                viewValue[i].addTextChangedListener(new GlobalFunctions.FactorsAutoValue(viewDropdown[i], viewValue[i]));
            }
        }

    }


    @Override
    public void onDestroy() {
        view = null;
        compositeDisposable.clear();
    }

    @Override
    public void updatePriceAdjustment1(String recordId,
                                       //FIX NAME
                                       String valrep_landimp_comp1_rec_property_rights_conveyed_type,
                                       String valrep_landimp_comp1_rec_time1_type,
                                       String valrep_landimp_comp1_rec_bargaining_allow_type,
                                       String valrep_landimp_comp1_rec_property_rights_conveyed_subprop,
                                       String valrep_landimp_comp1_rec_time1_subprop,
                                       String valrep_landimp_comp1_rec_bargaining_allow_subprop,

                                       String valrep_landimp_comp1_rec_property_rights_conveyed_description,
                                       String valrep_landimp_comp1_rec_time_description,
                                       String valrep_landimp_comp1_rec_bargaining_allow_description,

                                       String valrep_landimp_comp1_rec_property_rights_conveyed_desc,
                                       String valrep_landimp_comp1_rec_time_desc,
                                       String valrep_landimp_comp1_rec_bargaining_allow_desc,

                                       String valrep_landimp_comp1_rec_property_rights_conveyed,
                                       String valrep_landimp_comp1_rec_time,
                                       String valrep_landimp_comp1_rec_bargaining_allow,

                                       String valrep_landimp_comp1_adjustable_price_sqm) {

        compositeDisposable.add(getLocalSingleObserver(recordId).subscribeWith(
                getRecordUpdatePriceAdjustment1(
                        //FIX NAME
                         valrep_landimp_comp1_rec_property_rights_conveyed_type,
                         valrep_landimp_comp1_rec_time1_type,
                         valrep_landimp_comp1_rec_bargaining_allow_type,
                         valrep_landimp_comp1_rec_property_rights_conveyed_subprop,
                         valrep_landimp_comp1_rec_time1_subprop,
                         valrep_landimp_comp1_rec_bargaining_allow_subprop,

                         valrep_landimp_comp1_rec_property_rights_conveyed_description,
                         valrep_landimp_comp1_rec_time_description,
                         valrep_landimp_comp1_rec_bargaining_allow_description,

                         valrep_landimp_comp1_rec_property_rights_conveyed_desc,
                         valrep_landimp_comp1_rec_time_desc,
                         valrep_landimp_comp1_rec_bargaining_allow_desc,

                         valrep_landimp_comp1_rec_property_rights_conveyed,
                         valrep_landimp_comp1_rec_time,
                         valrep_landimp_comp1_rec_bargaining_allow,

                         valrep_landimp_comp1_adjustable_price_sqm
                )
        ));
    }

    @Override
    public void updatePriceAdjustment2(String recordId,
                                       String valrep_landimp_comp2_rec_property_rights_conveyed_description,
                                       String valrep_landimp_comp2_rec_time_description,
                                       String valrep_landimp_comp2_rec_bargaining_allow_description,

                                       String valrep_landimp_comp2_rec_property_rights_conveyed_desc,
                                       String valrep_landimp_comp2_rec_time_desc,
                                       String valrep_landimp_comp2_rec_bargaining_allow_desc,

                                       String valrep_landimp_comp2_rec_property_rights_conveyed,
                                       String valrep_landimp_comp2_rec_time,
                                       String valrep_landimp_comp2_rec_bargaining_allow,

                                       String valrep_landimp_comp2_adjustable_price_sqm) {

        compositeDisposable.add(getLocalSingleObserver(recordId).subscribeWith(
                getRecordUpdatePriceAdjustment2(
                        valrep_landimp_comp2_rec_property_rights_conveyed_description,
                        valrep_landimp_comp2_rec_time_description,
                        valrep_landimp_comp2_rec_bargaining_allow_description,

                        valrep_landimp_comp2_rec_property_rights_conveyed_desc,
                        valrep_landimp_comp2_rec_time_desc,
                        valrep_landimp_comp2_rec_bargaining_allow_desc,

                        valrep_landimp_comp2_rec_property_rights_conveyed,
                        valrep_landimp_comp2_rec_time,
                        valrep_landimp_comp2_rec_bargaining_allow,

                        valrep_landimp_comp2_adjustable_price_sqm
                )
        ));
    }

    @Override
    public void updatePriceAdjustment3(String recordId,
                                       String valrep_landimp_comp3_rec_property_rights_conveyed_description,
                                       String valrep_landimp_comp3_rec_time_description,
                                       String valrep_landimp_comp3_rec_bargaining_allow_description,

                                       String valrep_landimp_comp3_rec_property_rights_conveyed_desc,
                                       String valrep_landimp_comp3_rec_time_desc,
                                       String valrep_landimp_comp3_rec_bargaining_allow_desc,

                                       String valrep_landimp_comp3_rec_property_rights_conveyed,
                                       String valrep_landimp_comp3_rec_time,
                                       String valrep_landimp_comp3_rec_bargaining_allow,

                                       String valrep_landimp_comp3_adjustable_price_sqm) {

        compositeDisposable.add(getLocalSingleObserver(recordId).subscribeWith(
                getRecordUpdatePriceAdjustment3(
                        valrep_landimp_comp3_rec_property_rights_conveyed_description,
                        valrep_landimp_comp3_rec_time_description,
                        valrep_landimp_comp3_rec_bargaining_allow_description,

                        valrep_landimp_comp3_rec_property_rights_conveyed_desc,
                        valrep_landimp_comp3_rec_time_desc,
                        valrep_landimp_comp3_rec_bargaining_allow_desc,

                        valrep_landimp_comp3_rec_property_rights_conveyed,
                        valrep_landimp_comp3_rec_time,
                        valrep_landimp_comp3_rec_bargaining_allow,

                        valrep_landimp_comp3_adjustable_price_sqm
                )
        ));
    }

    @Override
    public void updatePriceAdjustment4(String recordId,
                                       String valrep_landimp_comp4_rec_property_rights_conveyed_description,
                                       String valrep_landimp_comp4_rec_time_description,
                                       String valrep_landimp_comp4_rec_bargaining_allow_description,

                                       String valrep_landimp_comp4_rec_property_rights_conveyed_desc,
                                       String valrep_landimp_comp4_rec_time_desc,
                                       String valrep_landimp_comp4_rec_bargaining_allow_desc,

                                       String valrep_landimp_comp4_rec_property_rights_conveyed,
                                       String valrep_landimp_comp4_rec_time,
                                       String valrep_landimp_comp4_rec_bargaining_allow,

                                       String valrep_landimp_comp4_adjustable_price_sqm) {

        compositeDisposable.add(getLocalSingleObserver(recordId).subscribeWith(
                getRecordUpdatePriceAdjustment4(
                        valrep_landimp_comp4_rec_property_rights_conveyed_description,
                        valrep_landimp_comp4_rec_time_description,
                        valrep_landimp_comp4_rec_bargaining_allow_description,

                        valrep_landimp_comp4_rec_property_rights_conveyed_desc,
                        valrep_landimp_comp4_rec_time_desc,
                        valrep_landimp_comp4_rec_bargaining_allow_desc,

                        valrep_landimp_comp4_rec_property_rights_conveyed,
                        valrep_landimp_comp4_rec_time,
                        valrep_landimp_comp4_rec_bargaining_allow,

                        valrep_landimp_comp4_adjustable_price_sqm
                )
        ));
    }

    @Override
    public void updatePriceAdjustment5(String recordId,
                                       String valrep_landimp_comp5_rec_property_rights_conveyed_description,
                                       String valrep_landimp_comp5_rec_time_description,
                                       String valrep_landimp_comp5_rec_bargaining_allow_description,

                                       String valrep_landimp_comp5_rec_property_rights_conveyed_desc,
                                       String valrep_landimp_comp5_rec_time_desc,
                                       String valrep_landimp_comp5_rec_bargaining_allow_desc,

                                       String valrep_landimp_comp5_rec_property_rights_conveyed,
                                       String valrep_landimp_comp5_rec_time,
                                       String valrep_landimp_comp5_rec_bargaining_allow,

                                       String valrep_landimp_comp5_adjustable_price_sqm) {

        compositeDisposable.add(getLocalSingleObserver(recordId).subscribeWith(
                getRecordUpdatePriceAdjustment5(
                        valrep_landimp_comp5_rec_property_rights_conveyed_description,
                        valrep_landimp_comp5_rec_time_description,
                        valrep_landimp_comp5_rec_bargaining_allow_description,

                        valrep_landimp_comp5_rec_property_rights_conveyed_desc,
                        valrep_landimp_comp5_rec_time_desc,
                        valrep_landimp_comp5_rec_bargaining_allow_desc,

                        valrep_landimp_comp5_rec_property_rights_conveyed,
                        valrep_landimp_comp5_rec_time,
                        valrep_landimp_comp5_rec_bargaining_allow,

                        valrep_landimp_comp5_adjustable_price_sqm
                )
        ));
    }

    //TODO Single disposable for fetching 1 Record from database based on recordId
    private Single<Record> getLocalSingleObserver(String recordId) {
        return interactor.getRecordById(recordId)
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread());
    }

    //TODO Results from disposable will be fetched here
    private DisposableSingleObserver<Record> getLocalObserver() {
        return new DisposableSingleObserver<Record>() {
            @Override
            public void onSuccess(Record record) {
                Log.d("int--LocalRepo", "Logic Triggered: UpdateAppraisalDetailsPresenter");
                view.showDetails(record);
                view.showSnackMessage("Loading Successful");
                view.hideProgress();
                new Handler().postDelayed(() -> view.hideProgress(), 1000);
            }

            @Override
            public void onError(Throwable e) {
                view.hideProgress();
                view.showExitDialog("Record Error", e.getMessage());
            }
        };
    }

    //TODO Update Record Physical Adjustment 1
    private DisposableSingleObserver<Record> getRecordUpdatePriceAdjustment1(
            //FIX NAME
            String valrep_landimp_comp1_rec_property_rights_conveyed_type,
            String valrep_landimp_comp1_rec_time1_type,
            String valrep_landimp_comp1_rec_bargaining_allow_type,
            String valrep_landimp_comp1_rec_property_rights_conveyed_subprop,
            String valrep_landimp_comp1_rec_time1_subprop,
            String valrep_landimp_comp1_rec_bargaining_allow_subprop,

            String valrep_landimp_comp1_rec_property_rights_conveyed_description,
            String valrep_landimp_comp1_rec_time_description,
            String valrep_landimp_comp1_rec_bargaining_allow_description,

            String valrep_landimp_comp1_rec_property_rights_conveyed_desc,
            String valrep_landimp_comp1_rec_time_desc,
            String valrep_landimp_comp1_rec_bargaining_allow_desc,

            String valrep_landimp_comp1_rec_property_rights_conveyed,
            String valrep_landimp_comp1_rec_time,
            String valrep_landimp_comp1_rec_bargaining_allow,

            String valrep_landimp_comp1_adjustable_price_sqm){
        return new DisposableSingleObserver<Record>() {
            @Override
            public void onSuccess(Record record) {
                record.setValrepLandimpComp1RecPropertyRightsConveyedType(valrep_landimp_comp1_rec_property_rights_conveyed_type);
                record.setValrepLandimpComp1RecTime1Type(valrep_landimp_comp1_rec_time1_type);
                record.setValrepLandimpComp1RecBargainingAllowType(valrep_landimp_comp1_rec_bargaining_allow_type);

                record.setValrepLandimpComp1RecPropertyRightsConveyedSubprop(valrep_landimp_comp1_rec_property_rights_conveyed_subprop);
                record.setValrepLandimpComp1RecTime1Subprop(valrep_landimp_comp1_rec_time1_subprop);
                record.setValrepLandimpComp1RecBargainingAllowSubprop(valrep_landimp_comp1_rec_bargaining_allow_subprop);

                record.setValrepLandimpComp1RecPropertyRightsConveyedDescription(valrep_landimp_comp1_rec_property_rights_conveyed_description);
                record.setValrepLandimpComp1RecTimeDescription(valrep_landimp_comp1_rec_time_description);
                record.setValrepLandimpComp1RecBargainingAllowDescription(valrep_landimp_comp1_rec_bargaining_allow_description);

                record.setValrepLandimpComp1RecPropertyRightsConveyedDesc(valrep_landimp_comp1_rec_property_rights_conveyed_desc);
                record.setValrepLandimpComp1RecTimeDesc(valrep_landimp_comp1_rec_time_desc);
                record.setValrepLandimpComp1RecBargainingAllowDesc(valrep_landimp_comp1_rec_bargaining_allow_desc);

                record.setValrepLandimpComp1RecPropertyRightsConveyed(valrep_landimp_comp1_rec_property_rights_conveyed);
                record.setValrepLandimpComp1RecTime(valrep_landimp_comp1_rec_time);
                record.setValrepLandimpComp1RecBargainingAllow(valrep_landimp_comp1_rec_bargaining_allow);

                /*SUBTOTAL DISPLAY*/
                record.setValrepLandimpComp1AdjustablePriceSqm(valrep_landimp_comp1_adjustable_price_sqm);

                interactor.updateLandVal(record);

                view.showSnackMessage("Saving Successful");

    //            new Handler().postDelayed(() -> view.hideProgress(), 2000);
            }

            @Override
            public void onError(Throwable e) {
                view.hideProgress();
                view.showExitDialog("Record Error", e.getMessage());
            }
        };
    }


    //TODO Update Record Physical Adjustment 2
    private DisposableSingleObserver<Record> getRecordUpdatePriceAdjustment2(
            String valrep_landimp_comp2_rec_property_rights_conveyed_description,
            String valrep_landimp_comp2_rec_time_description,
            String valrep_landimp_comp2_rec_bargaining_allow_description,

            String valrep_landimp_comp2_rec_property_rights_conveyed_desc,
            String valrep_landimp_comp2_rec_time_desc,
            String valrep_landimp_comp2_rec_bargaining_allow_desc,

            String valrep_landimp_comp2_rec_property_rights_conveyed,
            String valrep_landimp_comp2_rec_time,
            String valrep_landimp_comp2_rec_bargaining_allow,

            String valrep_landimp_comp2_adjustable_price_sqm){
        return new DisposableSingleObserver<Record>() {
            @Override
            public void onSuccess(Record record) {
                record.setValrepLandimpComp2RecPropertyRightsConveyedDescription(valrep_landimp_comp2_rec_property_rights_conveyed_description);
                record.setValrepLandimpComp2RecTimeDescription(valrep_landimp_comp2_rec_time_description);
                record.setValrepLandimpComp2RecBargainingAllowDescription(valrep_landimp_comp2_rec_bargaining_allow_description);

                record.setValrepLandimpComp2RecPropertyRightsConveyedDesc(valrep_landimp_comp2_rec_property_rights_conveyed_desc);
                record.setValrepLandimpComp2RecTimeDesc(valrep_landimp_comp2_rec_time_desc);
                record.setValrepLandimpComp2RecBargainingAllowDesc(valrep_landimp_comp2_rec_bargaining_allow_desc);

                record.setValrepLandimpComp2RecPropertyRightsConveyed(valrep_landimp_comp2_rec_property_rights_conveyed);
                record.setValrepLandimpComp2RecTime(valrep_landimp_comp2_rec_time);
                record.setValrepLandimpComp2RecBargainingAllow(valrep_landimp_comp2_rec_bargaining_allow);

                /*SUBTOTAL DISPLAY*/
                record.setValrepLandimpComp2AdjustablePriceSqm(valrep_landimp_comp2_adjustable_price_sqm);

                interactor.updateLandVal(record);

                view.showSnackMessage("Saving Successful");

                //            new Handler().postDelayed(() -> view.hideProgress(), 2000);
            }

            @Override
            public void onError(Throwable e) {
                view.hideProgress();
                view.showExitDialog("Record Error", e.getMessage());
            }
        };
    }

    //TODO Update Record Physical Adjustment 3
    private DisposableSingleObserver<Record> getRecordUpdatePriceAdjustment3(
            String valrep_landimp_comp3_rec_property_rights_conveyed_description,
            String valrep_landimp_comp3_rec_time_description,
            String valrep_landimp_comp3_rec_bargaining_allow_description,

            String valrep_landimp_comp3_rec_property_rights_conveyed_desc,
            String valrep_landimp_comp3_rec_time_desc,
            String valrep_landimp_comp3_rec_bargaining_allow_desc,

            String valrep_landimp_comp3_rec_property_rights_conveyed,
            String valrep_landimp_comp3_rec_time,
            String valrep_landimp_comp3_rec_bargaining_allow,

            String valrep_landimp_comp3_adjustable_price_sqm){
        return new DisposableSingleObserver<Record>() {
            @Override
            public void onSuccess(Record record) {
                record.setValrepLandimpComp3RecPropertyRightsConveyedDescription(valrep_landimp_comp3_rec_property_rights_conveyed_description);
                record.setValrepLandimpComp3RecTimeDescription(valrep_landimp_comp3_rec_time_description);
                record.setValrepLandimpComp3RecBargainingAllowDescription(valrep_landimp_comp3_rec_bargaining_allow_description);

                record.setValrepLandimpComp3RecPropertyRightsConveyedDesc(valrep_landimp_comp3_rec_property_rights_conveyed_desc);
                record.setValrepLandimpComp3RecTimeDesc(valrep_landimp_comp3_rec_time_desc);
                record.setValrepLandimpComp3RecBargainingAllowDesc(valrep_landimp_comp3_rec_bargaining_allow_desc);

                record.setValrepLandimpComp3RecPropertyRightsConveyed(valrep_landimp_comp3_rec_property_rights_conveyed);
                record.setValrepLandimpComp3RecTime(valrep_landimp_comp3_rec_time);
                record.setValrepLandimpComp3RecBargainingAllow(valrep_landimp_comp3_rec_bargaining_allow);

                /*SUBTOTAL DISPLAY*/
                record.setValrepLandimpComp3AdjustablePriceSqm(valrep_landimp_comp3_adjustable_price_sqm);

                interactor.updateLandVal(record);

                view.showSnackMessage("Saving Successful");

                //            new Handler().postDelayed(() -> view.hideProgress(), 2000);
            }

            @Override
            public void onError(Throwable e) {
                view.hideProgress();
                view.showExitDialog("Record Error", e.getMessage());
            }
        };
    }

    //TODO Update Record Physical Adjustment 4
    private DisposableSingleObserver<Record> getRecordUpdatePriceAdjustment4(
            String valrep_landimp_comp4_rec_property_rights_conveyed_description,
            String valrep_landimp_comp4_rec_time_description,
            String valrep_landimp_comp4_rec_bargaining_allow_description,

            String valrep_landimp_comp4_rec_property_rights_conveyed_desc,
            String valrep_landimp_comp4_rec_time_desc,
            String valrep_landimp_comp4_rec_bargaining_allow_desc,

            String valrep_landimp_comp4_rec_property_rights_conveyed,
            String valrep_landimp_comp4_rec_time,
            String valrep_landimp_comp4_rec_bargaining_allow,

            String valrep_landimp_comp4_adjustable_price_sqm){
        return new DisposableSingleObserver<Record>() {
            @Override
            public void onSuccess(Record record) {
                record.setValrepLandimpComp4RecPropertyRightsConveyedDescription(valrep_landimp_comp4_rec_property_rights_conveyed_description);
                record.setValrepLandimpComp4RecTimeDescription(valrep_landimp_comp4_rec_time_description);
                record.setValrepLandimpComp4RecBargainingAllowDescription(valrep_landimp_comp4_rec_bargaining_allow_description);

                record.setValrepLandimpComp4RecPropertyRightsConveyedDesc(valrep_landimp_comp4_rec_property_rights_conveyed_desc);
                record.setValrepLandimpComp4RecTimeDesc(valrep_landimp_comp4_rec_time_desc);
                record.setValrepLandimpComp4RecBargainingAllowDesc(valrep_landimp_comp4_rec_bargaining_allow_desc);

                record.setValrepLandimpComp4RecPropertyRightsConveyed(valrep_landimp_comp4_rec_property_rights_conveyed);
                record.setValrepLandimpComp4RecTime(valrep_landimp_comp4_rec_time);
                record.setValrepLandimpComp4RecBargainingAllow(valrep_landimp_comp4_rec_bargaining_allow);

                /*SUBTOTAL DISPLAY*/
                record.setValrepLandimpComp4AdjustablePriceSqm(valrep_landimp_comp4_adjustable_price_sqm);

                interactor.updateLandVal(record);

                view.showSnackMessage("Saving Successful");

                //            new Handler().postDelayed(() -> view.hideProgress(), 2000);
            }

            @Override
            public void onError(Throwable e) {
                view.hideProgress();
                view.showExitDialog("Record Error", e.getMessage());
            }
        };
    }

    //TODO Update Record Physical Adjustment 5
    private DisposableSingleObserver<Record> getRecordUpdatePriceAdjustment5(
            String valrep_landimp_comp5_rec_property_rights_conveyed_description,
            String valrep_landimp_comp5_rec_time_description,
            String valrep_landimp_comp5_rec_bargaining_allow_description,

            String valrep_landimp_comp5_rec_property_rights_conveyed_desc,
            String valrep_landimp_comp5_rec_time_desc,
            String valrep_landimp_comp5_rec_bargaining_allow_desc,

            String valrep_landimp_comp5_rec_property_rights_conveyed,
            String valrep_landimp_comp5_rec_time,
            String valrep_landimp_comp5_rec_bargaining_allow,

            String valrep_landimp_comp5_adjustable_price_sqm){
        return new DisposableSingleObserver<Record>() {
            @Override
            public void onSuccess(Record record) {
                record.setValrepLandimpComp5RecPropertyRightsConveyedDescription(valrep_landimp_comp5_rec_property_rights_conveyed_description);
                record.setValrepLandimpComp5RecTimeDescription(valrep_landimp_comp5_rec_time_description);
                record.setValrepLandimpComp5RecBargainingAllowDescription(valrep_landimp_comp5_rec_bargaining_allow_description);

                record.setValrepLandimpComp5RecPropertyRightsConveyedDesc(valrep_landimp_comp5_rec_property_rights_conveyed_desc);
                record.setValrepLandimpComp5RecTimeDesc(valrep_landimp_comp5_rec_time_desc);
                record.setValrepLandimpComp5RecBargainingAllowDesc(valrep_landimp_comp5_rec_bargaining_allow_desc);

                record.setValrepLandimpComp5RecPropertyRightsConveyed(valrep_landimp_comp5_rec_property_rights_conveyed);
                record.setValrepLandimpComp5RecTime(valrep_landimp_comp5_rec_time);
                record.setValrepLandimpComp5RecBargainingAllow(valrep_landimp_comp5_rec_bargaining_allow);

                /*SUBTOTAL DISPLAY*/
                record.setValrepLandimpComp5AdjustablePriceSqm(valrep_landimp_comp5_adjustable_price_sqm);

                interactor.updateLandVal(record);

                view.showSnackMessage("Saving Successful");

                //            new Handler().postDelayed(() -> view.hideProgress(), 2000);
            }

            @Override
            public void onError(Throwable e) {
                view.hideProgress();
                view.showExitDialog("Record Error", e.getMessage());
            }
        };
    }


}