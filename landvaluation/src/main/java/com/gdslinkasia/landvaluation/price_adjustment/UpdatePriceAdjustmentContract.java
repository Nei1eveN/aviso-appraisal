package com.gdslinkasia.landvaluation.price_adjustment;

import android.content.Context;
import android.widget.AutoCompleteTextView;

import com.gdslinkasia.base.model.job.Record;

import io.reactivex.Single;

public interface UpdatePriceAdjustmentContract {
    interface View {
        void showProgress(String title, String message);
        void hideProgress();
        void showSnackMessage(String message);
        void showDetails(Record record);
        void showExitDialog(String title, String message);
        void showErrorDialog(String title, String message);
    }
    interface Presenter {
        void onResume(boolean isCalled, String recordId);

        void setupDropdownState(Context context, AutoCompleteTextView[] viewDropdown, AutoCompleteTextView[] viewValue);

        void onDestroy();



        //Price Adjustment 1
        void updatePriceAdjustment1(
                String recordId,
                //FIX NAME
                String valrep_landimp_comp1_rec_property_rights_conveyed_type,
                String valrep_landimp_comp1_rec_time1_type,
                String valrep_landimp_comp1_rec_bargaining_allow_type,
                String valrep_landimp_comp1_rec_property_rights_conveyed_subprop,
                String valrep_landimp_comp1_rec_time1_subprop,
                String valrep_landimp_comp1_rec_bargaining_allow_subprop,

                String valrep_landimp_comp1_rec_property_rights_conveyed_description,
                String valrep_landimp_comp1_rec_time_description,
                String valrep_landimp_comp1_rec_bargaining_allow_description,

                String valrep_landimp_comp1_rec_property_rights_conveyed_desc,
                String valrep_landimp_comp1_rec_time_desc,
                String valrep_landimp_comp1_rec_bargaining_allow_desc,

                String valrep_landimp_comp1_rec_property_rights_conveyed,
                String valrep_landimp_comp1_rec_time,
                String valrep_landimp_comp1_rec_bargaining_allow,

                String valrep_landimp_comp1_adjustable_price_sqm
        );

        //Price Adjustment 2
        void updatePriceAdjustment2(
                String recordId,

                String valrep_landimp_comp1_rec_property_rights_conveyed_description,
                String valrep_landimp_comp1_rec_time_description,
                String valrep_landimp_comp1_rec_bargaining_allow_description,

                String valrep_landimp_comp1_rec_property_rights_conveyed_desc,
                String valrep_landimp_comp1_rec_time_desc,
                String valrep_landimp_comp1_rec_bargaining_allow_desc,

                String valrep_landimp_comp1_rec_property_rights_conveyed,
                String valrep_landimp_comp2_rec_time,
                String valrep_landimp_comp1_rec_bargaining_allow,

                String valrep_landimp_comp1_adjustable_price_sqm
        );

        //Price Adjustment 3
        void updatePriceAdjustment3(
                String recordId,

                String valrep_landimp_comp1_rec_property_rights_conveyed_description,
                String valrep_landimp_comp1_rec_time_description,
                String valrep_landimp_comp1_rec_bargaining_allow_description,

                String valrep_landimp_comp1_rec_property_rights_conveyed_desc,
                String valrep_landimp_comp1_rec_time_desc,
                String valrep_landimp_comp1_rec_bargaining_allow_desc,

                String valrep_landimp_comp1_rec_property_rights_conveyed,
                String valrep_landimp_comp1_rec_time,
                String valrep_landimp_comp1_rec_bargaining_allow,

                String valrep_landimp_comp1_adjustable_price_sqm
        );

        //Price Adjustment 4
        void updatePriceAdjustment4(
                String recordId,

                String valrep_landimp_comp1_rec_property_rights_conveyed_description,
                String valrep_landimp_comp1_rec_time_description,
                String valrep_landimp_comp1_rec_bargaining_allow_description,

                String valrep_landimp_comp1_rec_property_rights_conveyed_desc,
                String valrep_landimp_comp1_rec_time_desc,
                String valrep_landimp_comp1_rec_bargaining_allow_desc,

                String valrep_landimp_comp1_rec_property_rights_conveyed,
                String valrep_landimp_comp1_rec_time,
                String valrep_landimp_comp1_rec_bargaining_allow,

                String valrep_landimp_comp1_adjustable_price_sqm
        );

        //Price Adjustment 5
        void updatePriceAdjustment5(
                String recordId,

                String valrep_landimp_comp1_rec_property_rights_conveyed_description,
                String valrep_landimp_comp1_rec_time_description,
                String valrep_landimp_comp1_rec_bargaining_allow_description,

                String valrep_landimp_comp1_rec_property_rights_conveyed_desc,
                String valrep_landimp_comp1_rec_time_desc,
                String valrep_landimp_comp1_rec_bargaining_allow_desc,

                String valrep_landimp_comp1_rec_property_rights_conveyed,
                String valrep_landimp_comp1_rec_time,
                String valrep_landimp_comp1_rec_bargaining_allow,

                String valrep_landimp_comp1_adjustable_price_sqm
        );

    }
    interface Interactor {
        Single<Record> getRecordById(String recordId);
        void updateLandVal(Record record);
    }
}
