package com.gdslinkasia.landvaluation.price_adjustment;

import android.os.Bundle;

import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;
import androidx.viewpager.widget.ViewPager;

import com.gdslinkasia.base.adapter.ViewPagerAdapter;
import com.gdslinkasia.base.utils.ZoomPageTransformer;
import com.gdslinkasia.commonresources.R2;
import com.gdslinkasia.landvaluation.R;
import com.google.android.material.tabs.TabLayout;

import butterknife.BindView;
import butterknife.ButterKnife;

public class PriceAdjustmentActivity extends AppCompatActivity {

    @BindView(R2.id.tabLayout)
    TabLayout tabLayout;
    @BindView(R2.id.viewPager)
    ViewPager viewPager;

    ViewPagerAdapter pagerAdapter = new ViewPagerAdapter(getSupportFragmentManager());

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_market_data_analysis);
        ButterKnife.bind(this);

        pagerAdapter.addFragment(new PriceAdjustment1Fragment(), "Price Adjustment 1");
        pagerAdapter.addFragment(new PriceAdjustment2Fragment(), "Price Adjustment 2");
        pagerAdapter.addFragment(new PriceAdjustment3Fragment(), "Price Adjustment 3");
        pagerAdapter.addFragment(new PriceAdjustment4Fragment(), "Price Adjustment 4");
        pagerAdapter.addFragment(new PriceAdjustment5Fragment(), "Price Adjustment 5");
        viewPager.setAdapter(pagerAdapter);
        viewPager.setOffscreenPageLimit(5);
        viewPager.setPageTransformer(true, new ZoomPageTransformer());
        tabLayout.setupWithViewPager(viewPager);
//        createTabIcons();
}


    @Override
    public void onBackPressed() {
        super.onBackPressed();
        finish();
    }
}
