package com.gdslinkasia.landvaluation.price_adjustment;

import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AutoCompleteTextView;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;

import com.gdslinkasia.base.model.job.Record;
import com.gdslinkasia.commonresources.R2;
import com.gdslinkasia.landvaluation.R;

import java.util.Objects;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.Unbinder;

import static com.gdslinkasia.base.database.DatabaseUtils.RECORD_ID;
import static com.gdslinkasia.base.utils.computation.PriceAdjustmentComputation.*;

public class PriceAdjustment3Fragment extends Fragment implements UpdatePriceAdjustmentContract.View{

    private Unbinder unbinder;

    @BindView(R2.id.acElementType1) AutoCompleteTextView acElementType1;
    @BindView(R2.id.acElementType2) AutoCompleteTextView acElementType2;
    @BindView(R2.id.acElementType3) AutoCompleteTextView acElementType3;

    @BindView(R2.id.acSubjectProp1) AutoCompleteTextView acSubjectProp1;
    @BindView(R2.id.acSubjectProp2) AutoCompleteTextView acSubjectProp2;
    @BindView(R2.id.acSubjectProp3) AutoCompleteTextView acSubjectProp3;

    @BindView(R2.id.acComparable1) AutoCompleteTextView acComparable1;
    @BindView(R2.id.acComparable2) AutoCompleteTextView acComparable2;
    @BindView(R2.id.acComparable3) AutoCompleteTextView acComparable3;

    @BindView(R2.id.acAdjustment1) AutoCompleteTextView acAdjustment1;
    @BindView(R2.id.acAdjustment2) AutoCompleteTextView acAdjustment2;
    @BindView(R2.id.acAdjustment3) AutoCompleteTextView acAdjustment3;

    @BindView(R2.id.acValue1) AutoCompleteTextView acValue1;
    @BindView(R2.id.acValue2) AutoCompleteTextView acValue2;
    @BindView(R2.id.acValue3) AutoCompleteTextView acValue3;

    /*SUBTOTAL DISPLAY*/
    @BindView(R2.id.acNetPriceAdjustment) AutoCompleteTextView acNetPriceAdjustment;

    private UpdatePriceAdjustmentContract.Presenter presenter;

    private String recordId;

    private boolean isCalled = false;

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        Intent intent = Objects.requireNonNull(getActivity()).getIntent();
        recordId = intent.getStringExtra(RECORD_ID);

        presenter = new UpdatePriceAdjustmentPresenterImpl( this, Objects.requireNonNull(getActivity()).getApplication());


    }

    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container,
                             @Nullable Bundle savedInstanceState) {
        return inflater.inflate(R.layout.content_price_adjustment, container, false);
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        unbinder = ButterKnife.bind(this, view);


    }

    @Override
    public void onResume() {
        super.onResume();
        presenter.onResume(isCalled, recordId);
        isCalled = true;
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        presenter.onDestroy();
    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();
        unbinder.unbind();
    }


    @Override
    public void showProgress(String title, String message) {

    }

    @Override
    public void hideProgress() {

    }

    @Override
    public void showSnackMessage(String message) {


    }


    @Override
    public void onPause() {
        super.onPause();
        new Handler().post(() -> presenter.updatePriceAdjustment2(
                recordId,
                acComparable1.getText().toString(),
                acComparable2.getText().toString(),
                acComparable3.getText().toString(),

                acAdjustment1.getText().toString(),
                acAdjustment2.getText().toString(),
                acAdjustment3.getText().toString(),

                acValue1.getText().toString(),
                acValue2.getText().toString(),
                acValue3.getText().toString(),
                /*SUBTOTAL DISPLAY*/
                acNetPriceAdjustment.getText().toString()
        ));
    }

    @Override
    public void showDetails(Record record) {

        ((View) acElementType1.getParent().getParent()).setVisibility(View.GONE);
        ((View) acElementType2.getParent().getParent()).setVisibility(View.GONE);
        ((View) acElementType3.getParent().getParent()).setVisibility(View.GONE);
        ((View) acSubjectProp1.getParent().getParent()).setVisibility(View.GONE);
        ((View) acSubjectProp1.getParent().getParent()).setVisibility(View.GONE);
        ((View) acSubjectProp2.getParent().getParent()).setVisibility(View.GONE);

        acComparable1.setText(record.getValrepLandimpComp3RecPropertyRightsConveyedDescription());
        acComparable2.setText(record.getValrepLandimpComp3RecTimeDescription());
        acComparable3.setText(record.getValrepLandimpComp3RecBargainingAllowDescription());

        acAdjustment1.setText(record.getValrepLandimpComp3RecPropertyRightsConveyedDesc());
        acAdjustment2.setText(record.getValrepLandimpComp3RecTimeDesc());
        acAdjustment3.setText(record.getValrepLandimpComp3RecBargainingAllowDesc());

        acValue1.setText(record.getValrepLandimpComp3RecPropertyRightsConveyed());
        acValue2.setText(record.getValrepLandimpComp3RecTime());
        acValue3.setText(record.getValrepLandimpComp3RecBargainingAllow());

        /*SUBTOTAL DISPLAY*/
        acNetPriceAdjustment.setText(record.getValrepLandimpComp3AdjustablePriceSqm());

        //TODO setup Dropdown state view
        AutoCompleteTextView[] viewDropdown = {
                acAdjustment1,
                acAdjustment2,
                acAdjustment3
        };

        AutoCompleteTextView[] viewValue = {
                acValue1,
                acValue2,
                acValue3,
        };
        presenter.setupDropdownState(getContext(), viewDropdown, viewValue);

        acValue1.addTextChangedListener(PROPERTY_CONVEYED_VALUE_1(record.getValrepLandimpComp3PriceSqm(), acValue1, acValue2, acValue3, acNetPriceAdjustment));
        acValue2.addTextChangedListener(QUOTED_PERIOD_VALUE_2(record.getValrepLandimpComp3PriceSqm(), acValue1, acValue2, acValue3, acNetPriceAdjustment));
        acValue3.addTextChangedListener(ARBITRAGE_VALUE_3(record.getValrepLandimpComp3PriceSqm(), acValue1, acValue2, acValue3, acNetPriceAdjustment));
    }


    @Override
    public void showExitDialog(String title, String message) {

    }

    @Override
    public void showErrorDialog(String title, String message) {

    }


}
