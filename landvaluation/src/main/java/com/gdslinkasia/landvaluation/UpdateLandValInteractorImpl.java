package com.gdslinkasia.landvaluation;

import android.app.Application;

import com.gdslinkasia.base.database.repository.JobDetailsLocalRepository;
import com.gdslinkasia.base.model.job.Record;

import io.reactivex.Single;

class UpdateLandValInteractorImpl implements UpdateLandValContract.Interactor {

    private JobDetailsLocalRepository localRepository;

    public UpdateLandValInteractorImpl(Application application) {
        localRepository = new JobDetailsLocalRepository(application);
    }

    @Override
    public Single<Record> getRecordById(String recordId) {
        return localRepository.getRecordById(recordId);
    }

    @Override
    public void updateLandVal(Record record) {
        localRepository.updateRecord(record);  //update all record: should be rename to updateRecordAll
    }
}
