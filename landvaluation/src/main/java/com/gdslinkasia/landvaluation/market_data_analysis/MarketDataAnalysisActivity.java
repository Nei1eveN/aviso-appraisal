package com.gdslinkasia.landvaluation.market_data_analysis;

import android.os.Bundle;
import android.view.MenuItem;

import androidx.appcompat.app.AppCompatActivity;
import androidx.viewpager.widget.ViewPager;

import com.gdslinkasia.base.adapter.ViewPagerAdapter;
import com.gdslinkasia.commonresources.R2;
import com.gdslinkasia.landvaluation.R;
import com.google.android.material.tabs.TabLayout;

import java.util.Objects;

import butterknife.BindView;
import butterknife.ButterKnife;

public class MarketDataAnalysisActivity extends AppCompatActivity {

    @BindView(R2.id.viewPager) ViewPager viewPager;
    @BindView(R2.id.tabLayout) TabLayout tabLayout;

    ViewPagerAdapter pagerAdapter = new ViewPagerAdapter(getSupportFragmentManager());

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_market_data_analysis);
        ButterKnife.bind(this);

        Objects.requireNonNull(getSupportActionBar()).setTitle("Market Data Analysis");
        getSupportActionBar().setIcon(R.drawable.ic_close_white_24dp);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);

        pagerAdapter.addFragment(new Comp1MarketDataAnalysisFragment(), "Market Data Analysis 1");
        pagerAdapter.addFragment(new Comp2MarketDataAnalysisFragment(), "Market Data Analysis 2");
        pagerAdapter.addFragment(new Comp3MarketDataAnalysisFragment(), "Market Data Analysis 3");
        pagerAdapter.addFragment(new Comp4MarketDataAnalysisFragment(), "Market Data Analysis 4");
        pagerAdapter.addFragment(new Comp5MarketDataAnalysisFragment(), "Market Data Analysis 5");
        viewPager.setAdapter(pagerAdapter);
        viewPager.setCurrentItem(0);
        viewPager.setOffscreenPageLimit(5);
        tabLayout.setupWithViewPager(viewPager);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        if (item.getItemId() == android.R.id.home) {
            finish();
        }
        return super.onOptionsItemSelected(item);
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
    }
}
