package com.gdslinkasia.landvaluation.market_data_analysis;

import android.app.ProgressDialog;
import android.content.Intent;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AutoCompleteTextView;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.appcompat.app.AlertDialog;
import androidx.constraintlayout.widget.ConstraintLayout;
import androidx.fragment.app.Fragment;

import com.gdslinkasia.base.model.job.Record;
import com.gdslinkasia.commonresources.R2;
import com.gdslinkasia.landvaluation.R;
import com.gdslinkasia.landvaluation.market_data_analysis.contract.MarketDataAnalysisContract;
import com.gdslinkasia.landvaluation.market_data_analysis.contract.MarketDataAnalysisPresenter;
import com.google.android.material.snackbar.Snackbar;
import com.google.android.material.textfield.TextInputEditText;
import com.google.android.material.textfield.TextInputLayout;

import java.util.Objects;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.Unbinder;

import static com.gdslinkasia.base.database.DatabaseUtils.RECORD_ID;
import static com.gdslinkasia.base.utils.GlobalFunctions.*;
import static com.gdslinkasia.base.utils.computation.MarketDataComputation.*;

public class Comp2MarketDataAnalysisFragment extends Fragment implements MarketDataAnalysisContract.View {

    private Unbinder unbinder;
    @BindView(R2.id.constraintLayout) ConstraintLayout constraintLayout;

    @BindView(R2.id.txCompType) TextInputLayout txCompType;
    @BindView(R2.id.acCompType) AutoCompleteTextView acCompType;

    @BindView(R2.id.txClassification) TextInputLayout txClassification;
    @BindView(R2.id.acClassification) AutoCompleteTextView acClassification;

    @BindView(R2.id.txSource) TextInputLayout txSource;
    @BindView(R2.id.etSource)
    TextInputEditText etSource;

    @BindView(R2.id.txContact) TextInputLayout txContact;
    @BindView(R2.id.etContact) TextInputEditText etContact;

    @BindView(R2.id.txLocation) TextInputLayout txLocation;
    @BindView(R2.id.etLocation) TextInputEditText etLocation;

    @BindView(R2.id.txCharacteristic) TextInputLayout txCharacteristic;
    @BindView(R2.id.etCharacteristic) TextInputEditText etCharacteristic;

    @BindView(R2.id.txZoning) TextInputLayout txZoning;
    @BindView(R2.id.acZoning) AutoCompleteTextView acZoning;

    @BindView(R2.id.txPricingCirc) TextInputLayout txPricingCirc;
    @BindView(R2.id.acPricingCirc) AutoCompleteTextView acPricingCirc;

    @BindView(R2.id.txDate) TextInputLayout txDate;
    @BindView(R2.id.acDate) AutoCompleteTextView acDate;

    @BindView(R2.id.txLandAreaSqm) TextInputLayout txLandAreaSqm;
    @BindView(R2.id.etLandAreaSqm) TextInputEditText etLandAreaSqm;

    @BindView(R2.id.txBuildingAreaSqm) TextInputLayout txBuildingAreaSqm;
    @BindView(R2.id.etBuildingAreaSqm) TextInputEditText etBuildingAreaSqm;

    @BindView(R2.id.txAskingPriceSqm) TextInputLayout txAskingPriceSqm;
    @BindView(R2.id.etAskingPriceSqm) TextInputEditText etAskingPriceSqm;

    @BindView(R2.id.txTotalAskingPriceSqm) TextInputLayout txTotalAskingPriceSqm;
    @BindView(R2.id.etTotalAskingPriceSqm) TextInputEditText etTotalAskingPriceSqm;

    @BindView(R2.id.txBuildingReplaceCostSqm) TextInputLayout txBuildingReplaceCostSqm;
    @BindView(R2.id.etBuildingReplaceCostSqm) TextInputEditText acBuildingReplaceCostSqm;

    @BindView(R2.id.txBuildingReplaceCost) TextInputLayout txBuildingReplaceCost;
    @BindView(R2.id.etBuildingReplaceCost) TextInputEditText acBuildingReplaceCost;

    @BindView(R2.id.txBuildingDepreciationPercent) TextInputLayout txBuildingDepreciationPercent;
    @BindView(R2.id.etBuildingDepreciationPercent) TextInputEditText acBuildingDepreciationPercent;

    @BindView(R2.id.txBuildingDepreciation) TextInputLayout txBuildingDepreciation;
    @BindView(R2.id.etBuildingDepreciation) TextInputEditText acBuildingDepreciation;

    @BindView(R2.id.txEstBuildingVal) TextInputLayout txEstBuildingVal;
    @BindView(R2.id.etEstBuildingVal) TextInputEditText acEstBuildingVal;

    @BindView(R2.id.txNetBuildingVal) TextInputLayout txNetBuildingVal;
    @BindView(R2.id.etNetBuildingVal) TextInputEditText etNetBuildingVal;

    @BindView(R2.id.txNetValSqm) TextInputLayout txNetValSqm;
    @BindView(R2.id.etNetValSqm) TextInputEditText etNetValSqm;

    private String recordId;

    private MarketDataAnalysisContract.Presenter presenter;

    private ProgressDialog progressDialog;

    private boolean isCalled = false;

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        Intent intent = Objects.requireNonNull(getActivity()).getIntent();
        recordId = intent.getStringExtra(RECORD_ID);

//        recordId = Objects.requireNonNull(getArguments()).getString(RECORD_ID);

        progressDialog = new ProgressDialog(getActivity());

        presenter = new MarketDataAnalysisPresenter(this, Objects.requireNonNull(getActivity()).getApplication());
    }

    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container,
                             @Nullable Bundle savedInstanceState) {
        return inflater.inflate(R.layout.content_market_data_analysis, container, false);
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        unbinder = ButterKnife.bind(this, view);
    }

    @Override
    public void onResume() {
        super.onResume();
        presenter.onResume(isCalled, recordId);
        isCalled = true;
    }

    @Override
    public void onPause() {
        super.onPause();
        presenter.updateComp1MarketData(
                this,
                recordId,
                acCompType.getText().toString(),
                acClassification.getText().toString(),
                Objects.requireNonNull(etSource.getText()).toString(),
                Objects.requireNonNull(etContact.getText()).toString(),
                Objects.requireNonNull(etLocation.getText()).toString(),
                Objects.requireNonNull(etCharacteristic.getText()).toString(),
                acZoning.getText().toString(),
                acPricingCirc.getText().toString(),
                acDate.getText().toString(),
                Objects.requireNonNull(etLandAreaSqm.getText()).toString(),
                Objects.requireNonNull(etBuildingAreaSqm.getText()).toString(),
                Objects.requireNonNull(etAskingPriceSqm.getText()).toString(),
                Objects.requireNonNull(etTotalAskingPriceSqm.getText()).toString(),
                Objects.requireNonNull(acBuildingReplaceCostSqm.getText()).toString(),
                Objects.requireNonNull(acBuildingReplaceCost.getText()).toString(),
                Objects.requireNonNull(acBuildingDepreciationPercent.getText()).toString(),
                Objects.requireNonNull(acBuildingDepreciation.getText()).toString(),
                Objects.requireNonNull(acEstBuildingVal.getText()).toString(),
                Objects.requireNonNull(etNetBuildingVal.getText()).toString(),
                Objects.requireNonNull(etNetValSqm.getText()).toString());
    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();
        unbinder.unbind();
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        presenter.onDestroy();
        progressDialog.dismiss();
    }

    @Override
    public void showProgress(String title, String message) {
        progressDialog.setCancelable(false);
        progressDialog.setIndeterminate(false);
        progressDialog.setTitle(title);
        progressDialog.setMessage(message);
        progressDialog.show();
    }

    @Override
    public void hideProgress() {
        progressDialog.hide();
    }

    @Override
    public void showSnackMessage(String message) {
        Snackbar.make(constraintLayout, message, Snackbar.LENGTH_SHORT).show();
    }

    @Override
    public void showDetails(Record record) {
        acCompType.setText(record.getValrepLandImpComp2CompType());
        acCompType.setKeyListener(null);
        acCompType.setAdapter(comparativeType(Objects.requireNonNull(getActivity()), acCompType.getText().toString()));
        acCompType.setOnClickListener(v -> acCompType.showDropDown());

        acClassification.setText(record.getValrepLandimpComp2PropertyType());
        acClassification.setKeyListener(null);
        acClassification.setAdapter(ownershipClassificationType(getActivity(), acClassification.getText().toString()));
        acClassification.setOnClickListener(v -> acClassification.showDropDown());

        etSource.setText(record.getValrepLandimpComp2Source());
        etContact.setText(record.getValrepLandimpComp2ContactNo());
        etLocation.setText(record.getValrepLandimpComp2Location());
        etCharacteristic.setText(record.getValrepLandimpComp2Characteristic());

        acZoning.setText(record.getValrepLandimpComp2Zoning());
        acZoning.setKeyListener(null);
        acZoning.setAdapter(zoningType(Objects.requireNonNull(getActivity()), acZoning.getText().toString()));
        acZoning.setOnClickListener(v -> acZoning.showDropDown());

        acPricingCirc.setText(record.getValrepLandimpComp2PricingCircumstance());
        acPricingCirc.setKeyListener(null);
        acPricingCirc.setAdapter(pricingCircumstanceType(Objects.requireNonNull(getActivity()), acPricingCirc.getText().toString()));
        acPricingCirc.setOnClickListener(v -> acPricingCirc.showDropDown());

        acDate.setText(record.getValrepLandimpComp2EffectiveDate());
        acDate.setKeyListener(null);

        etLandAreaSqm.setText(record.getValrepLandimpComp2Area());
        etBuildingAreaSqm.setText(record.getValrepLandimpComp2BldgArea());
        etAskingPriceSqm.setText(record.getValrepLandimpComp2BasePriceSqm());
        etTotalAskingPriceSqm.setText(record.getValrepLandimpComp2BasePrice());
        acBuildingReplaceCostSqm.setText(record.getValrepLandimpComp2BldgReplacementUnitValue());
        acBuildingReplaceCost.setText(record.getValrepLandimpComp2BldgReplacementValue());
        acBuildingDepreciationPercent.setText(record.getValrepLandimpComp2BldgDepreciation());
        acBuildingDepreciation.setText(record.getValrepLandimpComp2BldgDepreciationVal());
        acEstBuildingVal.setText(record.getValrepLandimpComp2NetBldgValue());
        etNetBuildingVal.setText(record.getValrepLandimpComp2NetLandValuation());
        etNetValSqm.setText(record.getValrepLandimpComp2PriceSqm());

        etLandAreaSqm.addTextChangedListener(AC_LAND_AREA_SQM(etLandAreaSqm, etBuildingAreaSqm, etAskingPriceSqm, etTotalAskingPriceSqm, acBuildingReplaceCostSqm, acBuildingReplaceCost, acBuildingDepreciationPercent, acBuildingDepreciation, acEstBuildingVal, etNetBuildingVal, etNetValSqm));
        etBuildingAreaSqm.addTextChangedListener(AC_BUILDING_AREA_SQM(etLandAreaSqm, etBuildingAreaSqm, etAskingPriceSqm, etTotalAskingPriceSqm, acBuildingReplaceCostSqm, acBuildingReplaceCost, acBuildingDepreciationPercent, acBuildingDepreciation, acEstBuildingVal, etNetBuildingVal, etNetValSqm));
        etAskingPriceSqm.addTextChangedListener(AC_ASKING_PRICE_SQM(etLandAreaSqm, etBuildingAreaSqm, etAskingPriceSqm, etTotalAskingPriceSqm, acBuildingReplaceCostSqm, acBuildingReplaceCost, acBuildingDepreciationPercent, acBuildingDepreciation, acEstBuildingVal, etNetBuildingVal, etNetValSqm));
        acBuildingReplaceCostSqm.addTextChangedListener(AC_BUILDING_REPLACE_COST_SQM(etLandAreaSqm, etBuildingAreaSqm, etAskingPriceSqm, etTotalAskingPriceSqm, acBuildingReplaceCostSqm, acBuildingReplaceCost, acBuildingDepreciationPercent, acBuildingDepreciation, acEstBuildingVal, etNetBuildingVal, etNetValSqm));
        acBuildingDepreciationPercent.addTextChangedListener(AC_BUILDING_DEPRECIATION_PERCENT(etLandAreaSqm, etBuildingAreaSqm, etAskingPriceSqm, etTotalAskingPriceSqm, acBuildingReplaceCostSqm, acBuildingReplaceCost, acBuildingDepreciationPercent, acBuildingDepreciation, acEstBuildingVal, etNetBuildingVal, etNetValSqm));
    }

    @Override
    public void showExitDialog(String title, String message) {
        new AlertDialog.Builder(Objects.requireNonNull(getActivity()))
                .setCancelable(false)
                .setTitle(title)
                .setMessage(message)
                .setPositiveButton("EXIT", (dialogInterface, i) -> getActivity().finish())
                .create()
                .show();
    }
}
