package com.gdslinkasia.landvaluation.market_data_analysis.contract;

import androidx.fragment.app.Fragment;

import com.gdslinkasia.base.model.job.Record;

import io.reactivex.Single;

public interface MarketDataAnalysisContract {
    interface View {
        void showProgress(String title, String message);

        void hideProgress();

        void showSnackMessage(String message);

        void showDetails(Record record);

        void showExitDialog(String title, String message);
    }

    interface Presenter {
//        void onResume();

        void onResume(boolean isCalled, String recordId);

        void updateComp1MarketData(Fragment fragment,
                                   String recordId,
                                   String comp1Type, String comp1PropertyType,
                                   String comp1Source,
                                   String comp1ContactNo,
                                   String comp1Location,
                                   String comp1Characteristic,
                                   String comp1Zoning,
                                   String comp1PricingCircumstance,
                                   String comp1EffectiveDate,
                                   String comp1Area,
                                   String comp1BuildingArea,
                                   String comp1BasePriceSqm,
                                   String comp1BasePrice,
                                   String comp1BldgReplacementUnitValue,
                                   String comp1BldgReplacementValue,
                                   String comp1BldgDepreciation,
                                   String comp1BldgDepreciationVal,
                                   String comp1NetBldgValue,
                                   String comp1NetLandValuation,
                                   String comp1PriceSqm);

//        interface Comp1MarketDataPresenter {
//            void updateComp1MarketData(String recordId,
//                                       String comp1Type, String comp1PropertyType,
//                                       String comp1Source,
//                                       String comp1ContactNo,
//                                       String comp1Location,
//                                       String comp1Characteristic,
//                                       String comp1Zoning,
//                                       String comp1PricingCircumstance,
//                                       String comp1EffectiveDate,
//                                       String comp1Area,
//                                       String comp1BuildingArea,
//                                       String comp1BasePriceSqm,
//                                       String comp1BasePrice,
//                                       String comp1BldgReplacementUnitValue,
//                                       String comp1BldgReplacementValue,
//                                       String comp1BldgDepreciation,
//                                       String comp1BldgDepreciationVal,
//                                       String comp1NetBldgValue,
//                                       String comp1NetLandValuation,
//                                       String comp1PriceSqm);
//        }

        void onDestroy();
    }

    interface Interactor {
        Single<Record> getRecordById(String recordId);

        void updateRecord(Record record);
    }
}
