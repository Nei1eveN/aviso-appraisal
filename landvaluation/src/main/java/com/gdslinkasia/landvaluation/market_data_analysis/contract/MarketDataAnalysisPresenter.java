package com.gdslinkasia.landvaluation.market_data_analysis.contract;

import android.app.Application;
import android.os.Handler;
import android.util.Log;

import androidx.fragment.app.Fragment;

import com.gdslinkasia.base.model.job.Record;
import com.gdslinkasia.landvaluation.market_data_analysis.Comp1MarketDataAnalysisFragment;
import com.gdslinkasia.landvaluation.market_data_analysis.Comp2MarketDataAnalysisFragment;
import com.gdslinkasia.landvaluation.market_data_analysis.Comp3MarketDataAnalysisFragment;
import com.gdslinkasia.landvaluation.market_data_analysis.Comp4MarketDataAnalysisFragment;
import com.gdslinkasia.landvaluation.market_data_analysis.Comp5MarketDataAnalysisFragment;

import io.reactivex.Single;
import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.disposables.CompositeDisposable;
import io.reactivex.observers.DisposableSingleObserver;
import io.reactivex.schedulers.Schedulers;

public class MarketDataAnalysisPresenter implements MarketDataAnalysisContract.Presenter {

    private MarketDataAnalysisContract.View view;
    private MarketDataAnalysisContract.Interactor interactor;
    private CompositeDisposable compositeDisposable = new CompositeDisposable();

    public MarketDataAnalysisPresenter(MarketDataAnalysisContract.View view, Application application) {
        this.view = view;
        this.interactor = new MarketDataAnalysisInteractor(application);
    }

    @Override
    public void onResume(boolean isCalled, String recordId) {
        if (!isCalled) {
            Log.d("int--onStart", "Logic Triggered");
            view.showProgress("Loading Details", "Please wait...");
            compositeDisposable.add(getLocalSingleObserver(recordId).subscribeWith(getLocalObserver()));
        } else {
            Log.d("int--isCalled", "Method already triggered");
        }
    }

    @Override
    public void onDestroy() {
        view = null;
        compositeDisposable.clear();
    }

    @Override
    public void updateComp1MarketData(Fragment fragment,
                                      String recordId,
                                      String comp1Type,
                                      String comp1PropertyType,
                                      String comp1Source,
                                      String comp1ContactNo,
                                      String comp1Location,
                                      String comp1Characteristic,
                                      String comp1Zoning,
                                      String comp1PricingCircumstance,
                                      String comp1EffectiveDate,
                                      String comp1Area,
                                      String comp1BuildingArea,
                                      String comp1BasePriceSqm,
                                      String comp1BasePrice,
                                      String comp1BldgReplacementUnitValue,
                                      String comp1BldgReplacementValue,
                                      String comp1BldgDepreciation,
                                      String comp1BldgDepreciationVal,
                                      String comp1NetBldgValue,
                                      String comp1NetLandValuation,
                                      String comp1PriceSqm) {
//        view.showProgress("Saving Details", "Please wait...");
        compositeDisposable.add(getLocalSingleObserver(recordId).subscribeWith(updateRecord(fragment, comp1Type, comp1PropertyType, comp1Source, comp1ContactNo, comp1Location, comp1Characteristic, comp1Zoning, comp1PricingCircumstance, comp1EffectiveDate, comp1Area, comp1BuildingArea, comp1BasePriceSqm, comp1BasePrice, comp1BldgReplacementUnitValue, comp1BldgReplacementValue, comp1BldgDepreciation, comp1BldgDepreciationVal, comp1NetBldgValue, comp1NetLandValuation, comp1PriceSqm)));
    }

    private Single<Record> getLocalSingleObserver(String recordId) {
        return interactor.getRecordById(recordId)
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread());
    }

    private DisposableSingleObserver<Record> getLocalObserver() {
        return new DisposableSingleObserver<Record>() {
            @Override
            public void onSuccess(Record record) {
                Log.d("int--LocalRepo", "Logic Triggered: " + MarketDataAnalysisPresenter.class.getCanonicalName());
                view.showDetails(record);
                view.showSnackMessage("Loading Successful");
                new Handler().postDelayed(() -> view.hideProgress(), 1000);
            }

            @Override
            public void onError(Throwable e) {
                e.printStackTrace();
                view.hideProgress();
                view.showExitDialog("Record Error", e.getMessage());
            }
        };
    }

    private DisposableSingleObserver<Record> updateRecord(Fragment fragment,
                                                          String comp1Type,
                                                          String comp1PropertyType,
                                                          String comp1Source,
                                                          String comp1ContactNo,
                                                          String comp1Location,
                                                          String comp1Characteristic,
                                                          String comp1Zoning,
                                                          String comp1PricingCircumstance,
                                                          String comp1EffectiveDate,
                                                          String comp1Area,
                                                          String comp1BuildingArea,
                                                          String comp1BasePriceSqm,
                                                          String comp1BasePrice,
                                                          String comp1BldgReplacementUnitValue,
                                                          String comp1BldgReplacementValue,
                                                          String comp1BldgDepreciation,
                                                          String comp1BldgDepreciationVal,
                                                          String comp1NetBldgValue,
                                                          String comp1NetLandValuation,
                                                          String comp1PriceSqm) {
        return new DisposableSingleObserver<Record>() {
            @Override
            public void onSuccess(Record record) {
                if (fragment instanceof Comp1MarketDataAnalysisFragment) {
                    Log.e("int--instanceof", Comp1MarketDataAnalysisFragment.class.getSimpleName());
                    record.setValrepLandImpComp1CompType(comp1Type);
                    record.setValrepLandimpComp1PropertyType(comp1PropertyType);
                    record.setValrepLandimpComp1Source(comp1Source);
                    record.setValrepLandimpComp1ContactNo(comp1ContactNo);
                    record.setValrepLandimpComp1Location(comp1Location);
                    record.setValrepLandimpComp1Characteristic(comp1Characteristic);
                    record.setValrepLandimpComp1Zoning(comp1Zoning);
                    record.setValrepLandimpComp1PricingCircumstance(comp1PricingCircumstance);
                    record.setValrepLandimpComp1EffectiveDate(comp1EffectiveDate);
                    record.setValrepLandimpComp1Area(comp1Area);
                    record.setValrepLandimpComp1BldgArea(comp1BuildingArea);
                    record.setValrepLandimpComp1BasePriceSqm(comp1BasePriceSqm);
                    record.setValrepLandimpComp1BasePrice(comp1BasePrice);
                    record.setValrepLandimpComp1BldgReplacementUnitValue(comp1BldgReplacementUnitValue);
                    record.setValrepLandimpComp1BldgReplacementValue(comp1BldgReplacementValue);
                    record.setValrepLandimpComp1BldgDepreciation(comp1BldgDepreciation);
                    record.setValrepLandimpComp1BldgDepreciationVal(comp1BldgDepreciationVal);
                    record.setValrepLandimpComp1NetBldgValue(comp1NetBldgValue);
                    record.setValrepLandimpComp1NetLandValuation(comp1NetLandValuation);
                    record.setValrepLandimpComp1PriceSqm(comp1PriceSqm);
                } else if (fragment instanceof Comp2MarketDataAnalysisFragment) {
                    Log.e("int--instanceof", Comp2MarketDataAnalysisFragment.class.getSimpleName());
                    record.setValrepLandImpComp2CompType(comp1Type);
                    record.setValrepLandimpComp2PropertyType(comp1PropertyType);
                    record.setValrepLandimpComp2Source(comp1Source);
                    record.setValrepLandimpComp2ContactNo(comp1ContactNo);
                    record.setValrepLandimpComp2Location(comp1Location);
                    record.setValrepLandimpComp2Characteristic(comp1Characteristic);
                    record.setValrepLandimpComp2Zoning(comp1Zoning);
                    record.setValrepLandimpComp2PricingCircumstance(comp1PricingCircumstance);
                    record.setValrepLandimpComp2EffectiveDate(comp1EffectiveDate);
                    record.setValrepLandimpComp2Area(comp1Area);
                    record.setValrepLandimpComp2BldgArea(comp1BuildingArea);
                    record.setValrepLandimpComp2BasePriceSqm(comp1BasePriceSqm);
                    record.setValrepLandimpComp2BasePrice(comp1BasePrice);
                    record.setValrepLandimpComp2BldgReplacementUnitValue(comp1BldgReplacementUnitValue);
                    record.setValrepLandimpComp2BldgReplacementValue(comp1BldgReplacementValue);
                    record.setValrepLandimpComp2BldgDepreciation(comp1BldgDepreciation);
                    record.setValrepLandimpComp2BldgDepreciationVal(comp1BldgDepreciationVal);
                    record.setValrepLandimpComp2NetBldgValue(comp1NetBldgValue);
                    record.setValrepLandimpComp2NetLandValuation(comp1NetLandValuation);
                    record.setValrepLandimpComp2PriceSqm(comp1PriceSqm);
                } else if (fragment instanceof Comp3MarketDataAnalysisFragment) {
                    Log.e("int--instanceof", Comp3MarketDataAnalysisFragment.class.getSimpleName());
                    record.setValrepLandImpComp3CompType(comp1Type);
                    record.setValrepLandimpComp3PropertyType(comp1PropertyType);
                    record.setValrepLandimpComp3Source(comp1Source);
                    record.setValrepLandimpComp3ContactNo(comp1ContactNo);
                    record.setValrepLandimpComp3Location(comp1Location);
                    record.setValrepLandimpComp3Characteristic(comp1Characteristic);
                    record.setValrepLandimpComp3Zoning(comp1Zoning);
                    record.setValrepLandimpComp3PricingCircumstance(comp1PricingCircumstance);
                    record.setValrepLandimpComp3EffectiveDate(comp1EffectiveDate);
                    record.setValrepLandimpComp3Area(comp1Area);
                    record.setValrepLandimpComp3BldgArea(comp1BuildingArea);
                    record.setValrepLandimpComp3BasePriceSqm(comp1BasePriceSqm);
                    record.setValrepLandimpComp3BasePrice(comp1BasePrice);
                    record.setValrepLandimpComp3BldgReplacementUnitValue(comp1BldgReplacementUnitValue);
                    record.setValrepLandimpComp3BldgReplacementValue(comp1BldgReplacementValue);
                    record.setValrepLandimpComp3BldgDepreciation(comp1BldgDepreciation);
                    record.setValrepLandimpComp3BldgDepreciationVal(comp1BldgDepreciationVal);
                    record.setValrepLandimpComp3NetBldgValue(comp1NetBldgValue);
                    record.setValrepLandimpComp3NetLandValuation(comp1NetLandValuation);
                    record.setValrepLandimpComp3PriceSqm(comp1PriceSqm);
                } else if (fragment instanceof Comp4MarketDataAnalysisFragment) {
                    Log.e("int--instanceof", Comp4MarketDataAnalysisFragment.class.getSimpleName());
                    record.setValrepLandImpComp4CompType(comp1Type);
                    record.setValrepLandimpComp4PropertyType(comp1PropertyType);
                    record.setValrepLandimpComp4Source(comp1Source);
                    record.setValrepLandimpComp4ContactNo(comp1ContactNo);
                    record.setValrepLandimpComp4Location(comp1Location);
                    record.setValrepLandimpComp4Characteristic(comp1Characteristic);
                    record.setValrepLandimpComp4Zoning(comp1Zoning);
                    record.setValrepLandimpComp4PricingCircumstance(comp1PricingCircumstance);
                    record.setValrepLandimpComp4EffectiveDate(comp1EffectiveDate);
                    record.setValrepLandimpComp4Area(comp1Area);
                    record.setValrepLandimpComp4BldgArea(comp1BuildingArea);
                    record.setValrepLandimpComp4BasePriceSqm(comp1BasePriceSqm);
                    record.setValrepLandimpComp4BasePrice(comp1BasePrice);
                    record.setValrepLandimpComp4BldgReplacementUnitValue(comp1BldgReplacementUnitValue);
                    record.setValrepLandimpComp4BldgReplacementValue(comp1BldgReplacementValue);
                    record.setValrepLandimpComp4BldgDepreciation(comp1BldgDepreciation);
                    record.setValrepLandimpComp4BldgDepreciationVal(comp1BldgDepreciationVal);
                    record.setValrepLandimpComp4NetBldgValue(comp1NetBldgValue);
                    record.setValrepLandimpComp4NetLandValuation(comp1NetLandValuation);
                    record.setValrepLandimpComp4PriceSqm(comp1PriceSqm);
                } else if (fragment instanceof Comp5MarketDataAnalysisFragment) {
                    Log.e("int--instanceof", Comp5MarketDataAnalysisFragment.class.getSimpleName());
                    record.setValrepLandImpComp5CompType(comp1Type);
                    record.setValrepLandimpComp5PropertyType(comp1PropertyType);
                    record.setValrepLandimpComp5Source(comp1Source);
                    record.setValrepLandimpComp5ContactNo(comp1ContactNo);
                    record.setValrepLandimpComp5Location(comp1Location);
                    record.setValrepLandimpComp5Characteristic(comp1Characteristic);
                    record.setValrepLandimpComp5Zoning(comp1Zoning);
                    record.setValrepLandimpComp5PricingCircumstance(comp1PricingCircumstance);
                    record.setValrepLandimpComp5EffectiveDate(comp1EffectiveDate);
                    record.setValrepLandimpComp5Area(comp1Area);
                    record.setValrepLandimpComp5BldgArea(comp1BuildingArea);
                    record.setValrepLandimpComp5BasePriceSqm(comp1BasePriceSqm);
                    record.setValrepLandimpComp5BasePrice(comp1BasePrice);
                    record.setValrepLandimpComp5BldgReplacementUnitValue(comp1BldgReplacementUnitValue);
                    record.setValrepLandimpComp5BldgReplacementValue(comp1BldgReplacementValue);
                    record.setValrepLandimpComp5BldgDepreciation(comp1BldgDepreciation);
                    record.setValrepLandimpComp5BldgDepreciationVal(comp1BldgDepreciationVal);
                    record.setValrepLandimpComp5NetBldgValue(comp1NetBldgValue);
                    record.setValrepLandimpComp5NetLandValuation(comp1NetLandValuation);
                    record.setValrepLandimpComp5PriceSqm(comp1PriceSqm);
                }

                interactor.updateRecord(record);

//                view.showSnackMessage("Record Updated");
//                new Handler().post(() -> view.hideProgress());
                Log.e("int--marketDataSaved", "Logic Triggered "+ MarketDataAnalysisPresenter.class.getSimpleName());
            }

            @Override
            public void onError(Throwable e) {
                e.printStackTrace();
                view.hideProgress();
                view.showExitDialog("Saving Error", e.getMessage());
            }
        };
    }
}
