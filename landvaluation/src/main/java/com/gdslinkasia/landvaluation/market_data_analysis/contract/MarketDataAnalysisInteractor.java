package com.gdslinkasia.landvaluation.market_data_analysis.contract;

import android.app.Application;

import com.gdslinkasia.base.database.repository.JobDetailsLocalRepository;
import com.gdslinkasia.base.model.job.Record;

import io.reactivex.Single;

public class MarketDataAnalysisInteractor implements MarketDataAnalysisContract.Interactor {

    private JobDetailsLocalRepository localRepository;

    public MarketDataAnalysisInteractor(Application application) {
        this.localRepository = new JobDetailsLocalRepository(application);
    }

    @Override
    public Single<Record> getRecordById(String recordId) {
        return localRepository.getRecordById(recordId);
    }

    @Override
    public void updateRecord(Record record) {
        localRepository.updateRecord(record);
    }
}
