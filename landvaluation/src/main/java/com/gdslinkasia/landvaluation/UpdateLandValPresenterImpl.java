package com.gdslinkasia.landvaluation;

import android.app.Application;
import android.os.Handler;
import android.util.Log;

import com.gdslinkasia.base.model.job.Record;

import io.reactivex.Single;
import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.disposables.CompositeDisposable;
import io.reactivex.observers.DisposableSingleObserver;
import io.reactivex.schedulers.Schedulers;

public class UpdateLandValPresenterImpl implements UpdateLandValContract.Presenter {

    private UpdateLandValContract.View view;
    private UpdateLandValContract.Interactor interactor;

    private CompositeDisposable compositeDisposable = new CompositeDisposable();

    public UpdateLandValPresenterImpl(UpdateLandValContract.View view, Application application) {
        this.view = view;
        this.interactor = new UpdateLandValInteractorImpl(application);
    }


    @Override
    public void onStart(String recordId) {
        view.showProgress("Loading Record", "Please wait...");
        compositeDisposable.add(getLocalSingleObserver(recordId).subscribeWith(getLocalObserver()));
    }

    @Override
    public void onDestroy() {
        view = null;
        compositeDisposable.clear();
    }

    @Override
    public void updateLandVal(String recordId,
                              String lc_valuation_key_input,
                              String valrep_landimp_market_prop_market,
            /*Supply and Demand CONDO*/
                              String valrep_supplyanddemand_1,
                              String valrep_supplyanddemand_2,
                              String valrep_supplyanddemand_3,
                              String valrep_supplyanddemand_4,
                              String valrep_supplyanddemand_5,
                              String valrep_supplyanddemand_6,
                              String valrep_supplyanddemand_7,
                              String valrep_supplyanddemand_8,
                              String valrep_supplyanddemand_9,
            /*Supply and Demand*/
                              String valrep_landimp_supplyanddemand_1,
                              String valrep_landimp_supplyanddemand_2,
                              String valrep_landimp_supplyanddemand_3,
                              String valrep_landimp_supplyanddemand_4,
                              String valrep_landimp_supplyanddemand_5,
                              String valrep_landimp_supplyanddemand_6,
                              String valrep_landimp_supplyanddemand_7,
                              String valrep_landimp_supplyanddemand_8,
                              String valrep_landimp_supplyanddemand_9,
                              String valrep_landimp_supplyanddemand_10,
            /*Market Phase*/
                              String valrep_marketphase_1,
                              String valrep_marketphase_2,
                              String valrep_marketphase_3,
                              String valrep_marketphase_4,
                              String valrep_marketphase_5,
            /*Typical Buyers*/
                              String valrep_typicalbuyer_1,
            /*Total Value and Nets*/
                              String valrep_landimp_weighted_unit_value,
                              String valrep_landimp_comp_rounded_to_nearest,
                              String valrep_landimp_comp_rounded_to,
                              String valrep_landimp_comp_with_comp_maps,
            /*Totality*/
                              String valrep_landimp_value_total_area,
                              String valrep_landimp_value_total_deduction,
                              String valrep_landimp_value_total_net_area,
                              String valrep_landimp_value_total_landimp_value) {

                compositeDisposable.add(getLocalSingleObserver(recordId).subscribeWith(
                        getRecordUpdate(
                                 lc_valuation_key_input,
                                 valrep_landimp_market_prop_market,
                                /*Supply and Demand CONDO*/
                                 valrep_supplyanddemand_1,
                                 valrep_supplyanddemand_2,
                                 valrep_supplyanddemand_3,
                                 valrep_supplyanddemand_4,
                                 valrep_supplyanddemand_5,
                                 valrep_supplyanddemand_6,
                                 valrep_supplyanddemand_7,
                                 valrep_supplyanddemand_8,
                                 valrep_supplyanddemand_9,
                                /*Supply and Demand*/
                                 valrep_landimp_supplyanddemand_1,
                                 valrep_landimp_supplyanddemand_2,
                                 valrep_landimp_supplyanddemand_3,
                                 valrep_landimp_supplyanddemand_4,
                                 valrep_landimp_supplyanddemand_5,
                                 valrep_landimp_supplyanddemand_6,
                                 valrep_landimp_supplyanddemand_7,
                                 valrep_landimp_supplyanddemand_8,
                                 valrep_landimp_supplyanddemand_9,
                                 valrep_landimp_supplyanddemand_10,
                                /*Market Phase*/
                                 valrep_marketphase_1,
                                 valrep_marketphase_2,
                                 valrep_marketphase_3,
                                 valrep_marketphase_4,
                                 valrep_marketphase_5,
                                /*Typical Buyers*/
                                 valrep_typicalbuyer_1,
                                /*Total Value and Nets*/
                                 valrep_landimp_weighted_unit_value,
                                 valrep_landimp_comp_rounded_to_nearest,
                                 valrep_landimp_comp_rounded_to,
                                 valrep_landimp_comp_with_comp_maps,
                                /*Totality*/
                                 valrep_landimp_value_total_area,
                                 valrep_landimp_value_total_deduction,
                                 valrep_landimp_value_total_net_area,
                                 valrep_landimp_value_total_landimp_value
                        )
                ));

    }


    //TODO Single disposable for fetching 1 Record from database based on recordId
    private Single<Record> getLocalSingleObserver(String recordId) {
        return interactor.getRecordById(recordId)
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread());
    }

    //TODO Results from disposable will be fetched here
    private DisposableSingleObserver<Record> getLocalObserver() {
        return new DisposableSingleObserver<Record>() {
            @Override
            public void onSuccess(Record record) {
                Log.d("int--LocalRepo", "Logic Triggered: UpdateAppraisalDetailsPresenter");
                view.showDetails(record);
                view.showSnackMessage("Loading Successful");
                new Handler().postDelayed(() -> view.hideProgress(), 1000);
            }

            @Override
            public void onError(Throwable e) {
                view.hideProgress();
                view.showExitDialog("Record Error", e.getMessage());
            }
        };
    }

    //TODO Update Record with parameters passed from method updateLandVal
    private DisposableSingleObserver<Record> getRecordUpdate(
            String lc_valuation_key_input,
            String valrep_landimp_market_prop_market,
            /*Supply and Demand CONDO*/
            String valrep_supplyanddemand_1,
            String valrep_supplyanddemand_2,
            String valrep_supplyanddemand_3,
            String valrep_supplyanddemand_4,
            String valrep_supplyanddemand_5,
            String valrep_supplyanddemand_6,
            String valrep_supplyanddemand_7,
            String valrep_supplyanddemand_8,
            String valrep_supplyanddemand_9,
            /*Supply and Demand*/
            String valrep_landimp_supplyanddemand_1,
            String valrep_landimp_supplyanddemand_2,
            String valrep_landimp_supplyanddemand_3,
            String valrep_landimp_supplyanddemand_4,
            String valrep_landimp_supplyanddemand_5,
            String valrep_landimp_supplyanddemand_6,
            String valrep_landimp_supplyanddemand_7,
            String valrep_landimp_supplyanddemand_8,
            String valrep_landimp_supplyanddemand_9,
            String valrep_landimp_supplyanddemand_10,
            /*Market Phase*/
            String valrep_marketphase_1,
            String valrep_marketphase_2,
            String valrep_marketphase_3,
            String valrep_marketphase_4,
            String valrep_marketphase_5,
            /*Typical Buyers*/
            String valrep_typicalbuyer_1,
            /*Total Value and Nets*/
            String valrep_landimp_weighted_unit_value,
            String valrep_landimp_comp_rounded_to_nearest,
            String valrep_landimp_comp_rounded_to,
            String valrep_landimp_comp_with_comp_maps,
            /*Totality*/
            String valrep_landimp_value_total_area,
            String valrep_landimp_value_total_deduction,
            String valrep_landimp_value_total_net_area,
            String valrep_landimp_value_total_landimp_value
    ) {
        return new DisposableSingleObserver<Record>() {
            @Override
            public void onSuccess(Record record) {
                Log.d("int--RecordFlowable", "Logic Triggered: UpdateLandValriptions");
                record.setLcValuationKeyInput(lc_valuation_key_input);
                record.setValrepLandimpMarketPropMarket(valrep_landimp_market_prop_market);

                record.setValrepSupplyanddemand1(valrep_supplyanddemand_1);
                record.setValrepSupplyanddemand2(valrep_supplyanddemand_2);
                record.setValrepSupplyanddemand3(valrep_supplyanddemand_3);
                record.setValrepSupplyanddemand4(valrep_supplyanddemand_4);
                record.setValrepSupplyanddemand5(valrep_supplyanddemand_5);
                record.setValrepSupplyanddemand6(valrep_supplyanddemand_6);
                record.setValrepSupplyanddemand7(valrep_supplyanddemand_7);
                record.setValrepSupplyanddemand8(valrep_supplyanddemand_8);
                record.setValrepSupplyanddemand9(valrep_supplyanddemand_9);


                record.setValrepLandimpSupplyanddemand1(valrep_landimp_supplyanddemand_1);
                record.setValrepLandimpSupplyanddemand2(valrep_landimp_supplyanddemand_2);
                record.setValrepLandimpSupplyanddemand3(valrep_landimp_supplyanddemand_3);
                record.setValrepLandimpSupplyanddemand4(valrep_landimp_supplyanddemand_4);
                record.setValrepLandimpSupplyanddemand5(valrep_landimp_supplyanddemand_5);
                record.setValrepLandimpSupplyanddemand6(valrep_landimp_supplyanddemand_6);
                record.setValrepLandimpSupplyanddemand7(valrep_landimp_supplyanddemand_7);
                record.setValrepLandimpSupplyanddemand8(valrep_landimp_supplyanddemand_8);
                record.setValrepLandimpSupplyanddemand9(valrep_landimp_supplyanddemand_9);
                record.setValrepLandimpSupplyanddemand10(valrep_landimp_supplyanddemand_10);

                record.setValrepMarketphase1(valrep_marketphase_1);
                record.setValrepMarketphase2(valrep_marketphase_2);
                record.setValrepMarketphase3(valrep_marketphase_3);
                record.setValrepMarketphase4(valrep_marketphase_4);
                record.setValrepMarketphase5(valrep_marketphase_5);

                record.setValrepTypicalbuyer1(valrep_typicalbuyer_1);

                //Total Value of Computations
                record.setValrepLandimpWeightedUnitValue(valrep_landimp_weighted_unit_value);
                record.setValrepLandimpCompRoundedToNearest(valrep_landimp_comp_rounded_to_nearest);
                record.setValrepLandimpCompRoundedTo(valrep_landimp_comp_rounded_to);
                record.setValrepLandimpCompWithCompMaps(valrep_landimp_comp_with_comp_maps);
                record.setValrepLandimpWeightedUnitValue(valrep_landimp_value_total_area);

                record.setValrepLandimpValueTotalArea(valrep_landimp_value_total_deduction);
                record.setValrepLandimpValueTotalDeduction(valrep_landimp_value_total_net_area);
                record.setValrepLandimpValueTotalNetArea(valrep_landimp_value_total_landimp_value);



                interactor.updateLandVal(record);

                view.showSnackMessage("Saving Successful");

//                new Handler().postDelayed(() -> view.hideProgress(), 2000);
            }

            @Override
            public void onError(Throwable e) {
                view.hideProgress();
                view.showExitDialog("Record Error", e.getMessage());
            }
        };
    }
}