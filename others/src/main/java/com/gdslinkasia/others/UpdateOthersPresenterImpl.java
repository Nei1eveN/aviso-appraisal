package com.gdslinkasia.others;

import android.app.Application;
import android.os.Handler;
import android.util.Log;

import com.gdslinkasia.base.model.job.Record;

import io.reactivex.Single;
import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.disposables.CompositeDisposable;
import io.reactivex.observers.DisposableSingleObserver;
import io.reactivex.schedulers.Schedulers;

public class UpdateOthersPresenterImpl implements UpdateOthersContract.Presenter {

    private UpdateOthersContract.View view;
    private UpdateOthersContract.Interactor interactor;

    private CompositeDisposable compositeDisposable = new CompositeDisposable();

    public UpdateOthersPresenterImpl(UpdateOthersContract.View view, Application application) {
        this.view = view;
        this.interactor = new UpdateOthersInteractorImpl(application);
    }


    @Override
    public void onStart(String recordId) {
        view.showProgress("Loading Record", "Please wait...");
        compositeDisposable.add(getLocalSingleObserver(recordId).subscribeWith(getLocalObserver()));
    }

    @Override
    public void onDestroy() {
        view = null;
        compositeDisposable.clear();
    }

    @Override
    public void updateOthers(
            String recordId,
            String lc_ivs_complied,
            String lc_ivs_reason1,
            String lc_ivs_reason2,
            String lc_ivs_reason3,
            String lc_ivs_reason4,
            String lc_ivs_reason5,
            String lc_ivs_reason_others,
            String lc_ivs_complied_summary,
            String lc_valuation_approach,
            String lc_valuation_approach_reason) {
        compositeDisposable.add(getLocalSingleObserver(recordId).subscribeWith(
                getRecordUpdate(
                         lc_ivs_complied,
                         lc_ivs_reason1,
                         lc_ivs_reason2,
                         lc_ivs_reason3,
                         lc_ivs_reason4,
                         lc_ivs_reason5,
                         lc_ivs_reason_others,
                         lc_ivs_complied_summary,
                         lc_valuation_approach,
                         lc_valuation_approach_reason
                )
        ));
    }


    //TODO Single disposable for fetching 1 Record from database based on recordId
    private Single<Record> getLocalSingleObserver(String recordId) {
        return interactor.getRecordById(recordId)
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread());
    }

    //TODO Results from disposable will be fetched here
    private DisposableSingleObserver<Record> getLocalObserver() {
        return new DisposableSingleObserver<Record>() {
            @Override
            public void onSuccess(Record record) {
                Log.d("int--LocalRepo", "Logic Triggered: UpdateAppraisalDetailsPresenter");
                view.showDetails(record);
                view.showSnackMessage("Loading Successful");
                view.hideProgress();
                new Handler().postDelayed(() -> view.hideProgress(), 1000);
            }

            @Override
            public void onError(Throwable e) {
                view.hideProgress();
                view.showExitDialog("Record Error", e.getMessage());
            }
        };
    }

    //TODO Update Record with parameters passed from method updateScopeOfWork
    private DisposableSingleObserver<Record> getRecordUpdate(//Nature and Source of Information
                                                             String lc_ivs_complied,
                                                             String lc_ivs_reason1,
                                                             String lc_ivs_reason2,
                                                             String lc_ivs_reason3,
                                                             String lc_ivs_reason4,
                                                             String lc_ivs_reason5,
                                                             String lc_ivs_reason_others,
                                                             String lc_ivs_complied_summary,
                                                             String lc_valuation_approach,
                                                             String lc_valuation_approach_reason) {
        return new DisposableSingleObserver<Record>() {
            @Override
            public void onSuccess(Record record) {
                Log.d("int--RecordFlowable", "Logic Triggered: UpdateAppraisalDetailsPresenter");

                //Nature and Source of Information
                record.setLcIvsComplied(lc_ivs_complied);
                record.setLcIvsReason1(lc_ivs_reason1);
                record.setLcIvsReason2(lc_ivs_reason2);
                record.setLcIvsReason3(lc_ivs_reason3);
                record.setLcIvsReason4(lc_ivs_reason4);
                record.setLcIvsReason5(lc_ivs_reason5);
                record.setLcIvsReasonOthers(lc_ivs_reason_others);
                record.setLcIvsCompliedSummary(lc_ivs_complied_summary);
                record.setLcValuationApproach(lc_valuation_approach);
                record.setLcValuationApproachReason(lc_valuation_approach_reason);

                interactor.updateOthers(record);

                view.showSnackMessage("Saving Successful");

                new Handler().postDelayed(() -> view.hideProgress(), 2000);
            }

            @Override
            public void onError(Throwable e) {
                view.hideProgress();
                view.showExitDialog("Record Error", e.getMessage());
            }
        };
    }
}