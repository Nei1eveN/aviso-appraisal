package com.gdslinkasia.others;

import android.os.Bundle;
import android.os.Handler;
import android.widget.AutoCompleteTextView;
import android.widget.CheckBox;

import androidx.appcompat.app.AppCompatActivity;

import com.gdslinkasia.base.model.job.Record;
import com.gdslinkasia.commonresources.R2;
import com.google.android.material.textfield.TextInputLayout;

import butterknife.BindView;
import butterknife.ButterKnife;

import static com.gdslinkasia.base.database.DatabaseUtils.*;
import static com.gdslinkasia.base.utils.GlobalFunctions.getCheckboxBoolean;
import static com.gdslinkasia.base.utils.GlobalString.CHECKBOX_TO_STRING;

public class OthersActivity extends AppCompatActivity implements UpdateOthersContract.View {

    @BindView(R2.id.txIvsCompliance) TextInputLayout txIvsCompliance;
    @BindView(R2.id.acIvsCompliance) AutoCompleteTextView acIvsCompliance;

    @BindView(R2.id.cbIvs1) CheckBox cbIvs1;
    @BindView(R2.id.cbIvs2) CheckBox cbIvs2;
    @BindView(R2.id.cbIvs3) CheckBox cbIvs3;
    @BindView(R2.id.cbIvs4) CheckBox cbIvs4;
    @BindView(R2.id.cbIvs5) CheckBox cbIvs5;

    @BindView(R2.id.txOtherReason) TextInputLayout txOtherReason;
    @BindView(R2.id.acOtherReason) AutoCompleteTextView acOtherReason;
    @BindView(R2.id.txSummary) TextInputLayout txSummary;
    @BindView(R2.id.acSummary) AutoCompleteTextView acSummary;
    @BindView(R2.id.txValApproach) TextInputLayout txValApproach;
    @BindView(R2.id.acValApproach) AutoCompleteTextView acValApproach;
    @BindView(R2.id.txReasonApproach) TextInputLayout txReasonApproach;
    @BindView(R2.id.acReasonApproach) AutoCompleteTextView acReasonApproach;

    public static String TAG = "PropertyDescriptionActivity";

    private String recordId;
    private UpdateOthersContract.Presenter presenter;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.form_others);

        ButterKnife.bind(this);

        Bundle putExtraBundle = getIntent().getExtras();
        if(putExtraBundle == null) {
            recordId= null;
            return;
        } else {
            recordId= putExtraBundle.getString(RECORD_ID);
        }

        presenter = new UpdateOthersPresenterImpl(this, getApplication());

        presenter.onStart(recordId);

    }

    @Override
    protected void onPause() {

        new Handler().post(() -> presenter.updateOthers(
                recordId,
                //Nature and Source of Information
                acIvsCompliance.getText().toString(),
                CHECKBOX_TO_STRING(cbIvs1),
                CHECKBOX_TO_STRING(cbIvs2),
                CHECKBOX_TO_STRING(cbIvs3),
                CHECKBOX_TO_STRING(cbIvs4),
                CHECKBOX_TO_STRING(cbIvs5),
                acOtherReason.getText().toString(),
                acSummary.getText().toString(),
                acValApproach.getText().toString(),
                acReasonApproach.getText().toString()
        ));

        super.onPause();

    }


    @Override
    public void showProgress(String title, String message) {

    }

    @Override
    public void hideProgress() {

    }

    @Override
    public void showSnackMessage(String message) {

    }

    @Override
    public void showDetails(Record record) {


        acIvsCompliance.setText(record.getLcIvsComplied());

        cbIvs1.setChecked(getCheckboxBoolean(record.getLcIvsReason1()));
        cbIvs2.setChecked(getCheckboxBoolean(record.getLcIvsReason2()));
        cbIvs3.setChecked(getCheckboxBoolean(record.getLcIvsReason3()));
        cbIvs4.setChecked(getCheckboxBoolean(record.getLcIvsReason4()));
        cbIvs5.setChecked(getCheckboxBoolean(record.getLcIvsReason5()));

        acOtherReason.setText(record.getLcIvsReasonOthers());
        acSummary.setText(record.getLcIvsCompliedSummary());
        acValApproach.setText(record.getLcValuationApproach());
        acReasonApproach.setText(record.getLcValuationApproachReason());

    }

    @Override
    public void showExitDialog(String title, String message) {

    }

    @Override
    public void showErrorDialog(String title, String message) {

    }
}
