package com.gdslinkasia.others;

import android.app.Application;

import com.gdslinkasia.base.database.repository.JobDetailsLocalRepository;
import com.gdslinkasia.base.model.job.Record;

import io.reactivex.Single;

class UpdateOthersInteractorImpl implements UpdateOthersContract.Interactor {

    private JobDetailsLocalRepository localRepository;

    public UpdateOthersInteractorImpl(Application application) {
        localRepository = new JobDetailsLocalRepository(application);
    }

    @Override
    public Single<Record> getRecordById(String recordId) {
        return localRepository.getRecordById(recordId);
    }

    @Override
    public void updateOthers(Record record) {
        localRepository.updateRecord(record);  //update all record: should be rename to updateRecordAll
    }
}
