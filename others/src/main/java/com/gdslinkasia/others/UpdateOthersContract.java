package com.gdslinkasia.others;

import com.gdslinkasia.base.model.job.Record;

import io.reactivex.Single;

public interface UpdateOthersContract {
    interface View {
        void showProgress(String title, String message);
        void hideProgress();
        void showSnackMessage(String message);
        void showDetails(Record record);
        void showExitDialog(String title, String message);
        void showErrorDialog(String title, String message);
    }
    interface Presenter {
        void onStart(String recordId);
        void onDestroy();

        void updateOthers(String recordId,
                          String lc_ivs_complied,
                          String lc_ivs_reason1,
                          String lc_ivs_reason2,
                          String lc_ivs_reason3,
                          String lc_ivs_reason4,
                          String lc_ivs_reason5,
                          String lc_ivs_reason_others,
                          String lc_ivs_complied_summary,
                          String lc_valuation_approach,
                          String lc_valuation_approach_reason
        );
    }
    interface Interactor {
        Single<Record> getRecordById(String recordId);
        void updateOthers(Record record);
    }
}
