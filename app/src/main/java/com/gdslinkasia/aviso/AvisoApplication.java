package com.gdslinkasia.aviso;

import com.androidnetworking.AndroidNetworking;
import com.crashlytics.android.Crashlytics;
import com.gdslinkasia.aviso.di.DaggerAppComponent;
import com.microsoft.appcenter.AppCenter;
import com.microsoft.appcenter.analytics.Analytics;
import com.microsoft.appcenter.crashes.Crashes;
import com.microsoft.appcenter.distribute.Distribute;

import dagger.android.AndroidInjector;
import dagger.android.support.DaggerApplication;
import io.fabric.sdk.android.Fabric;
import io.reactivex.plugins.RxJavaPlugins;

import static android.util.Log.VERBOSE;

public class AvisoApplication extends DaggerApplication {

    @Override
    public void onCreate() {
        super.onCreate();
        Fabric.with(this, new Crashlytics());
        AndroidNetworking.initialize(this);
        RxJavaPlugins.setErrorHandler(throwable -> {});

        Analytics.setEnabled(true);
        Crashes.setEnabled(true);
        Distribute.setEnabled(true);
        Distribute.setEnabledForDebuggableBuild(true);

        AppCenter.setLogLevel(VERBOSE);
    }

    @Override
    protected AndroidInjector<? extends DaggerApplication> applicationInjector() {
        return DaggerAppComponent.builder().application(this).context(this).build();
    }
}
