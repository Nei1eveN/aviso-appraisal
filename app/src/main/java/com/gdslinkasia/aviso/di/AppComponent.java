package com.gdslinkasia.aviso.di;

import android.app.Application;
import android.content.Context;

import com.gdslinkasia.aviso.AvisoApplication;
import com.gdslinkasia.base.di.AppModule;

import javax.inject.Singleton;

import dagger.BindsInstance;
import dagger.Component;
import dagger.android.AndroidInjector;
import dagger.android.support.AndroidSupportInjectionModule;

@Singleton
@Component(
        modules = {
                AndroidSupportInjectionModule.class,
                AppModule.class,
                ActivityBuildersModule.class,
                FragmentBuildersModule.class
        })
public interface AppComponent extends AndroidInjector<AvisoApplication> {

        @Component.Builder
        interface Builder {
                @BindsInstance
                Builder application(Application application);

                @BindsInstance
                Builder context(Context context);

                AppComponent build();
        }

}
