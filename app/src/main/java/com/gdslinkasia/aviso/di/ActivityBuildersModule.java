package com.gdslinkasia.aviso.di;

import com.gdslinkasia.appraisaldetails.ForAcceptanceDetailsActivity;
import com.gdslinkasia.appraisaldetails.di.ForAcceptanceDetailModule;
import com.gdslinkasia.assumptions.SpecialAssumptionsActivity;
import com.gdslinkasia.assumptions.di.SpecialAssumptionsModule;
import com.gdslinkasia.jobs.MainActivity;
import com.gdslinkasia.landvaluation.market_approach.LandMarketApproachListActivity;
import com.gdslinkasia.landvaluation.market_approach.di.LandMarketApproachModule;
import com.gdslinkasia.login.LoginActivity;
import com.gdslinkasia.login.LoginModule;
import com.gdslinkasia.qualifyingstatements.QualifyingStatementsActivity;
import com.gdslinkasia.qualifyingstatements.di.QualifyingStatementsModule;

import dagger.Module;
import dagger.android.ContributesAndroidInjector;

@Module
public abstract class ActivityBuildersModule {

    //TODO LOGIN
    @ContributesAndroidInjector (modules = {LoginModule.class})
    abstract LoginActivity loginActivity();

    //TODO MAIN ACTIVITY
    @ContributesAndroidInjector (modules = {JobModule.class})
    abstract MainActivity mainActivity();

    //TODO LAND VALUATION (MARKET APPROACH)
    @ContributesAndroidInjector(modules = {LandMarketApproachModule.class})
    abstract LandMarketApproachListActivity lotValuationDetailActivity();

    //TODO JOB ACCEPTANCE / FOR ACCEPTANCE DETAILS
    @ContributesAndroidInjector(modules = {ForAcceptanceDetailModule.class})
    abstract ForAcceptanceDetailsActivity forAcceptanceDetailsActivity();

    //TODO SPECIAL ASSUMPTIONS
    @ContributesAndroidInjector(modules = {SpecialAssumptionsModule.class})
    abstract SpecialAssumptionsActivity specialAssumptionsActivity();

    //TODO QUALIFYING STATEMENT LIST
    @ContributesAndroidInjector(modules = {QualifyingStatementsModule.class})
    abstract QualifyingStatementsActivity qualifyingStatementsActivity();
}
