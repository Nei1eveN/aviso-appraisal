package com.gdslinkasia.aviso.di;

import android.app.Application;

import com.gdslinkasia.jobs.contract.JobContract;
import com.gdslinkasia.jobs.contract.JobPresenter;

import dagger.Module;
import dagger.Provides;

@Module
public class JobModule {

    @Provides
    static JobContract.Presenter jobPresenter(Application application) {
        return new JobPresenter(application);
    }

}
