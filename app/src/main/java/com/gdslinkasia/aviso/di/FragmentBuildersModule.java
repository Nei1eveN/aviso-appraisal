package com.gdslinkasia.aviso.di;

import com.gdslinkasia.jobs.ActiveJobFragment;
import com.gdslinkasia.jobs.ForAcceptanceJobFragment;
import com.gdslinkasia.jobs.ReworkJobFragment;

import dagger.Module;
import dagger.android.ContributesAndroidInjector;

@Module
public abstract class FragmentBuildersModule {

    @ContributesAndroidInjector(modules = {JobModule.class})
    abstract ActiveJobFragment activeJobFragment();

    @ContributesAndroidInjector(modules = {JobModule.class})
    abstract ForAcceptanceJobFragment forAcceptanceJobFragment();

    @ContributesAndroidInjector(modules = {JobModule.class})
    abstract ReworkJobFragment reworkJobFragment();

}
