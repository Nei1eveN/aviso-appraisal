package com.gdslinkasia.qualification.contract;

import android.app.Application;
import android.os.Handler;
import android.util.Log;

import com.gdslinkasia.base.model.job.Record;

import io.reactivex.Single;
import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.disposables.CompositeDisposable;
import io.reactivex.observers.DisposableSingleObserver;
import io.reactivex.schedulers.Schedulers;

public class UpdateQualificationsPresenter implements UpdateQualificationsContract.Presenter {

    private UpdateQualificationsContract.View view;
    private UpdateQualificationsContract.Interactor interactor;

    private CompositeDisposable compositeDisposable = new CompositeDisposable();

    public UpdateQualificationsPresenter(UpdateQualificationsContract.View view, Application application) {
        this.view = view;
        this.interactor = new UpdateQualificationsInteractor(application);
    }


    @Override
    public void onStart(String recordId) {
        view.showProgress("Loading Record", "Please wait...");
        compositeDisposable.add(getLocalSingleObserver(recordId).subscribeWith(getLocalObserver()));
    }

    @Override
    public void onDestroy() {
        view = null;
        compositeDisposable.clear();
    }

    //TODO Single disposable for fetching 1 Record from database based on recordId
    private Single<Record> getLocalSingleObserver(String recordId) {
        return interactor.getRecordById(recordId)
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread());
    }

    //TODO Results from disposable will be fetched here
    private DisposableSingleObserver<Record> getLocalObserver() {
        return new DisposableSingleObserver<Record>() {
            @Override
            public void onSuccess(Record record) {
                Log.d("int--LocalRepo", "Logic Triggered: UpdateAppraisalDetailsPresenter");
                view.showDetails(record);
                view.showSnackMessage("Loading Successful");
                view.hideProgress();
                new Handler().postDelayed(() -> view.hideProgress(), 1000);
            }

            @Override
            public void onError(Throwable e) {
                view.hideProgress();
                view.showExitDialog("Record Error", e.getMessage());
            }
        };
    }

}