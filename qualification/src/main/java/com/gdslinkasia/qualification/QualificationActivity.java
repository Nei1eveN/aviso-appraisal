package com.gdslinkasia.qualification;

import android.os.Bundle;
import android.widget.TextView;

import androidx.appcompat.app.AppCompatActivity;

import com.gdslinkasia.base.model.job.Record;
import com.gdslinkasia.commonresources.R2;
import com.gdslinkasia.qualification.contract.UpdateQualificationsContract;
import com.gdslinkasia.qualification.contract.UpdateQualificationsPresenter;

import butterknife.BindView;
import butterknife.ButterKnife;

import static com.gdslinkasia.base.database.DatabaseUtils.RECORD_ID;

public class QualificationActivity extends AppCompatActivity implements UpdateQualificationsContract.View {

    @BindView(R2.id.txtTitleName) TextView txtTitleName;
    @BindView(R2.id.txtName) TextView txtName;
    @BindView(R2.id.txPrcLicenseNo) TextView txPrcLicenseNo;
    @BindView(R2.id.txtDateIssuance) TextView txtDateIssuance;
    @BindView(R2.id.txtDateExpiration) TextView txtDateExpiration;
    @BindView(R2.id.txtPtrDetailsNo) TextView txtPtrDetailsNo;
    @BindView(R2.id.txtIssuedFrom) TextView txtIssuedFrom;
    @BindView(R2.id.txtFreeLanceCompany) TextView txtFreeLanceCompany;
    @BindView(R2.id.txtCompanyName) TextView txtCompanyName;
    @BindView(R2.id.txtIpreaNo) TextView txtIpreaNo;


    public static String TAG = "PropertyDescriptionActivity";

    private String recordId;
    private UpdateQualificationsContract.Presenter presenter;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.form_qualifications);

        ButterKnife.bind(this);

        Bundle putExtraBundle = getIntent().getExtras();
        if(putExtraBundle == null) {
            recordId= null;
            return;
        } else {
            recordId= putExtraBundle.getString(RECORD_ID);
        }

        presenter = new UpdateQualificationsPresenter(this, getApplication());

        presenter.onStart(recordId);

    }


    @Override
    public void showProgress(String title, String message) {

    }

    @Override
    public void hideProgress() {

    }

    @Override
    public void showSnackMessage(String message) {

    }

    @Override
    public void showDetails(Record record) {
        txtTitleName.setText(record.getAppraisalRequest().get(0).getAppraiserTitle());
        txtName.setText(record.getAppraisalRequest().get(0).getAppAppraiserName());
        txPrcLicenseNo.setText(record.getAppraisalRequest().get(0).getAppraiserPrcNo());
        txtDateIssuance.setText(record.getAppraisalRequest().get(0).getAppraiserPrcIssuanceDate());
        txtDateExpiration.setText(record.getAppraisalRequest().get(0).getAppraiserPrcExpiryDate());
        txtPtrDetailsNo.setText(record.getAppraisalRequest().get(0).getAppraiserPtrNo());
        txtIssuedFrom.setText(record.getAppraisalRequest().get(0).getAppraiserPtrIssuedFrom());
        txtFreeLanceCompany.setText(record.getAppraisalRequest().get(0).getAppraiserEmpPractice());
        txtCompanyName.setText(record.getAppraisalRequest().get(0).getAppraiserCompany());
        txtIpreaNo.setText(record.getAppraisalRequest().get(0).getIpreaMemNo());

    }

    @Override
    public void showExitDialog(String title, String message) {

    }

    @Override
    public void showErrorDialog(String title, String message) {

    }
}
