package com.gdslinkasia.base.database.converters;

import androidx.room.TypeConverter;

import com.gdslinkasia.base.model.details.JobSystem;
import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;

import java.lang.reflect.Type;

public class JobSystemTypeConverter {
    private Gson gson = new Gson();
    private Type type = new TypeToken<JobSystem>(){}.getType();

    @TypeConverter
    public JobSystem stringToSystem(String jsonToStringValue) {
        return gson.fromJson(jsonToStringValue, type);
    }

    @TypeConverter
    public String systemToString(JobSystem value) {
        return gson.toJson(value, type);
    }
}
