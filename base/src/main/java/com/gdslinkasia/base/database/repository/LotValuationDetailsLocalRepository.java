package com.gdslinkasia.base.database.repository;

import android.app.Application;

import com.gdslinkasia.base.database.ApplicationDatabase;
import com.gdslinkasia.base.database.dao.LotValuationDetailDao;
import com.gdslinkasia.base.model.details.ValrepLandimpLotValuationDetail;

import java.util.List;

import io.reactivex.Flowable;
import io.reactivex.Single;

public class LotValuationDetailsLocalRepository {

    private LotValuationDetailDao lotValuationDetailDao;

    public LotValuationDetailsLocalRepository(Application application) {
        this.lotValuationDetailDao = ApplicationDatabase.getInstance(application).lotValuationDetailDao();
    }


    public Single<List<ValrepLandimpLotValuationDetail>> getLotValuationDetailListSingle(String recordId) {
        return lotValuationDetailDao.getLotValuationDetailsListSingle(recordId);
    }

    public Flowable<List<ValrepLandimpLotValuationDetail>> getLotValuationDetailListFlowable(String recordId) {
        return lotValuationDetailDao.getLotValuationDetailsListFlowable(recordId);
    }
}
