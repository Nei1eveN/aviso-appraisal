package com.gdslinkasia.base.database.converters;

import androidx.room.TypeConverter;

import com.gdslinkasia.base.model.details.ValrepLandimpOtherLandImpValue;
import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;

import java.lang.reflect.Type;
import java.util.Collections;
import java.util.List;

public class ValrepLandimpOtherLandImpValueTypeConverter {

    private Gson gson = new Gson();
    private Type type = new TypeToken<List<ValrepLandimpOtherLandImpValue>>() {}.getType();

    @TypeConverter
    public List<ValrepLandimpOtherLandImpValue> stringToData(String jsonToStringValue) {
        if (jsonToStringValue == null) {
            return Collections.emptyList();
        }

        return gson.fromJson(jsonToStringValue, type);
    }

    @TypeConverter
    public String dataToString(List<ValrepLandimpOtherLandImpValue> value) {
        return gson.toJson(value, type);
    }

}
