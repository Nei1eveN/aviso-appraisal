package com.gdslinkasia.base.database.dao;

import androidx.room.Dao;
import androidx.room.Insert;
import androidx.room.Query;

import com.gdslinkasia.base.model.details.ValrepLandimpLotValuationDetail;

import java.util.List;

import io.reactivex.Single;

import static androidx.room.OnConflictStrategy.REPLACE;
import static com.gdslinkasia.base.database.DatabaseUtils.RECORD_ID;

@Dao
public abstract class LandimpLotValuationDetailDao {

    @Query("SELECT * FROM " + "ValrepLandimpLotValuationDetail" + " WHERE " + RECORD_ID + " =:recordId")
    public abstract Single<List<ValrepLandimpLotValuationDetail>> getLandimpLotValuationDetails(String recordId);

    @Insert(onConflict = REPLACE)
    public abstract void insertLotValuationDetails(List<ValrepLandimpLotValuationDetail> lotValuationDetails);

    @Query("DELETE FROM ValrepLandimpLotValuationDetail")
    public abstract void deleteLandimpLotValuationDetails();
}
