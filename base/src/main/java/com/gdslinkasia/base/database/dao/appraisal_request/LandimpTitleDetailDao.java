package com.gdslinkasia.base.database.dao.appraisal_request;

import androidx.room.Dao;
import androidx.room.Insert;
import androidx.room.Query;

import com.gdslinkasia.base.model.details.LandimpTitleDetail;

import java.util.List;

import io.reactivex.Single;

import static androidx.room.OnConflictStrategy.REPLACE;
import static com.gdslinkasia.base.database.DatabaseUtils.APPRAISAL_REQUEST_ID;

@Dao
public abstract class LandimpTitleDetailDao {

    @Insert(onConflict = REPLACE)
    public abstract void insertLandimpTitleDetail(LandimpTitleDetail detail);

    @Query("SELECT * FROM "+"LandimpTitleDetail"+" WHERE "+APPRAISAL_REQUEST_ID+" =:recordUId")
    public abstract Single<List<LandimpTitleDetail>> getLandimpTitleDetailsByAppraisalRequestId(long recordUId);

}
