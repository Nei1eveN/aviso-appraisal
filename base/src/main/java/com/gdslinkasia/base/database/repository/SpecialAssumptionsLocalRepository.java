package com.gdslinkasia.base.database.repository;

import android.app.Application;

import com.gdslinkasia.base.database.ApplicationDatabase;
import com.gdslinkasia.base.database.dao.RecordDao;
import com.gdslinkasia.base.database.dao.SpecialAssumptionsDao;
import com.gdslinkasia.base.model.details.ValrepLandimpAssumptionsRemark;
import com.gdslinkasia.base.model.job.Record;

import java.util.List;

import io.reactivex.Flowable;
import io.reactivex.Single;

public class SpecialAssumptionsLocalRepository {

    private SpecialAssumptionsDao assumptionsDao;
    private RecordDao recordDao;
    private ApplicationDatabase applicationDatabase;

    public SpecialAssumptionsLocalRepository(Application application) {
        applicationDatabase = ApplicationDatabase.getInstance(application);
        assumptionsDao = applicationDatabase.specialAssumptionsDao();
        recordDao = applicationDatabase.recordDao();
    }

    public Single<List<ValrepLandimpAssumptionsRemark>> getOwnershipPropertyDescList(String recordId) {
        return assumptionsDao.getAssumptionsRemarksByRecordId(recordId);
    }

    public Flowable<List<ValrepLandimpAssumptionsRemark>> getListFlowable(String recordId) {
        return assumptionsDao.getListFlowable(recordId);
    }

    public Single<ValrepLandimpAssumptionsRemark> getPropertyDescDetail(long uniqueId) {
        return assumptionsDao.getPropertyDescDetail(uniqueId);
    }

    public void updateLotDetail(ValrepLandimpAssumptionsRemark detail) {
        assumptionsDao.updateAssumptionRemark(detail);
    }

    public void insertLotDetail(ValrepLandimpAssumptionsRemark detail) {
        assumptionsDao.insertAssumptionRemark(detail);
    }

    public Single<Record> getRecordByRecordId(String recordId) {
        return recordDao.getRecordById(recordId);
    }

    //TODO DELETE ITEM BY UNIQUE ID
    public void deleteLotItem(long uniqueId) {
        assumptionsDao.deleteLotItem(uniqueId);
    }

    //TODO DESTROY DATABASE INSTANCE
    public void onDestroy() {
        if (applicationDatabase != null)
            ApplicationDatabase.destroyInstance();
    }


}
