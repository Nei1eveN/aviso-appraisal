package com.gdslinkasia.base.database.dao.appraisal_request;

import androidx.room.Dao;
import androidx.room.Insert;
import androidx.room.Query;

import com.gdslinkasia.base.model.details.AppCollateralAddress;
import com.gdslinkasia.base.model.details.AppContactPerson;

import java.util.List;

import io.reactivex.Single;

import static androidx.room.OnConflictStrategy.REPLACE;
import static com.gdslinkasia.base.database.DatabaseUtils.APPRAISAL_REQUEST_ID;

@Dao
public abstract class AppContactPersonDao {

    @Insert(onConflict = REPLACE)
    public abstract void insertContactPerson(AppContactPerson contactPerson);

    @Query("SELECT * FROM "+"AppContactPerson"+" WHERE "+APPRAISAL_REQUEST_ID+" =:appraisalRequestId")
    public abstract Single<List<AppContactPerson>> getAppContactsByAppraisalRequestUId(long appraisalRequestId);

    @Insert(onConflict = REPLACE)
    public abstract long[] insertCollateralAddress(List<AppCollateralAddress> address);
}
