package com.gdslinkasia.base.database.dao;

import androidx.room.Dao;
import androidx.room.Insert;
import androidx.room.Query;
import androidx.room.Update;

import com.gdslinkasia.base.model.details.ValrepLandimpParkingValuationDetail;

import java.util.List;

import io.reactivex.Flowable;
import io.reactivex.Single;

import static androidx.room.OnConflictStrategy.REPLACE;
import static com.gdslinkasia.base.database.DatabaseUtils.RECORD_ID;
import static com.gdslinkasia.base.database.DatabaseUtils.UNIQUE_ID;


@Dao
public interface CondoParkingValuationDao {

    @Query("SELECT * FROM "+"ValrepLandimpParkingValuationDetail"+" WHERE "+RECORD_ID+" =:recordId")
    Single<List<ValrepLandimpParkingValuationDetail>> getDetailsList(String recordId);

    @Query("SELECT * FROM "+"ValrepLandimpParkingValuationDetail"+" WHERE "+RECORD_ID+" =:recordId")
    Flowable<List<ValrepLandimpParkingValuationDetail>> getListFlowable(String recordId);

    @Query("SELECT * FROM "+"ValrepLandimpParkingValuationDetail"+" WHERE "+UNIQUE_ID+" =:uniqueId")
    Single<ValrepLandimpParkingValuationDetail> getDetails(long uniqueId);

    @Insert(onConflict = REPLACE)
    void insertParkingValuationDetails(List<ValrepLandimpParkingValuationDetail> parkingValuationDetails);

    @Update
    void updateLotDetail(ValrepLandimpParkingValuationDetail detail);

    @Query("DELETE FROM ValrepLandimpParkingValuationDetail")
    void deleteValrepLandimpParkingValuationDetails();
}
