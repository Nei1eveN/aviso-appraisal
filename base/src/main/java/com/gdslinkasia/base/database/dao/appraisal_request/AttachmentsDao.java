package com.gdslinkasia.base.database.dao.appraisal_request;

import androidx.room.Dao;
import androidx.room.Delete;
import androidx.room.Insert;
import androidx.room.Query;
import androidx.room.Update;

import com.gdslinkasia.base.model.details.AppAttachment;

import java.util.List;

import io.reactivex.Flowable;
import io.reactivex.Single;

import static androidx.room.OnConflictStrategy.REPLACE;
import static com.gdslinkasia.base.database.DatabaseUtils.APPRAISAL_REQUEST_ID;
import static com.gdslinkasia.base.database.DatabaseUtils.ATTACHMENT_CREATED_FROM;
import static com.gdslinkasia.base.database.DatabaseUtils.UNIQUE_ID;

@Dao
public abstract class AttachmentsDao {

    //TODO QUERY
    @Query("SELECT * FROM "+"AppAttachment"+" WHERE "+APPRAISAL_REQUEST_ID+" =:appraisalRequestId"+" AND "+ATTACHMENT_CREATED_FROM+" =:createdFrom")
    public abstract Single<List<AppAttachment>> getAttachmentsDetailsList(long appraisalRequestId, String createdFrom);

    @Query("SELECT * FROM "+"AppAttachment"+" WHERE "+APPRAISAL_REQUEST_ID+" =:appraisalRequestId"+" AND "+ATTACHMENT_CREATED_FROM+" =:createdFrom")
    public abstract Flowable<List<AppAttachment>> getListFlowable(long appraisalRequestId, String createdFrom);

    @Query("SELECT * FROM "+"AppAttachment"+" WHERE "+UNIQUE_ID+" =:uniqueId")
    public abstract Single<AppAttachment> getAttachmentDetail(long uniqueId);

    //TODO INSERT
    @Insert(onConflict = REPLACE)
    public abstract void insertAppAttachment(AppAttachment appAttachment);

    //TODO UPDATE
    @Update
    public abstract void updateAttachmentDetail(AppAttachment detail);

    //TODO DELETE
    //TODO DELETE ITEM BY QUERYING UNIQUE ID
    @Query("DELETE FROM "+"AppAttachment"+" WHERE "+UNIQUE_ID+" =:uniqueId")
    public abstract void deleteAttachmentItem(long uniqueId);

    @Delete
    public abstract void deleteAllAttachments(List<AppAttachment> appAttachments);

    @Query("SELECT * FROM "+"AppAttachment"+" WHERE "+APPRAISAL_REQUEST_ID+" =:appraisalRequestId")
    public abstract Single<List<AppAttachment>> getAppAttachmentsByAppraisalRequestId(long appraisalRequestId);
}
