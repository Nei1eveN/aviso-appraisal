package com.gdslinkasia.base.database.repository;

import android.app.Application;

import com.gdslinkasia.base.database.ApplicationDatabase;
import com.gdslinkasia.base.database.dao.LandImpOtherValuationDao;
import com.gdslinkasia.base.model.details.ValrepLandimpOtherLandImpValue;

import java.util.List;

import io.reactivex.Flowable;
import io.reactivex.Single;

public class LandImpOtherValuationLocalRepository {
    private LandImpOtherValuationDao landImpDetailDao;

    public LandImpOtherValuationLocalRepository(Application application) {
        landImpDetailDao = ApplicationDatabase.getInstance(application).landImpOtherValuationDao();
    }

    public Single<List<ValrepLandimpOtherLandImpValue>> getDetailsList(String recordId) {
        return landImpDetailDao.getOtherLandImpValuesByRecordId(recordId);
    }

    public Flowable<List<ValrepLandimpOtherLandImpValue>> getListFlowable(String recordId) {
        return landImpDetailDao.getListFlowable(recordId);
    }

    public Single<ValrepLandimpOtherLandImpValue> getDetails(long uniqueId) {
        return landImpDetailDao.getDetails(uniqueId);
    }

    public void updateLotDetail(ValrepLandimpOtherLandImpValue detail) {
        landImpDetailDao.updateLotDetail(detail);
    }
}
