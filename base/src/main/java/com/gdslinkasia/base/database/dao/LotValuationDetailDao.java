package com.gdslinkasia.base.database.dao;

import androidx.room.Dao;
import androidx.room.Insert;
import androidx.room.Query;
import androidx.room.Update;

import com.gdslinkasia.base.model.details.ValrepLandimpLotValuationDetail;

import java.util.List;

import io.reactivex.Flowable;
import io.reactivex.Single;

import static androidx.room.OnConflictStrategy.REPLACE;
import static com.gdslinkasia.base.database.DatabaseUtils.*;

@Dao
public interface LotValuationDetailDao {

    @Query("SELECT * FROM "+"ValrepLandimpLotValuationDetail"+" WHERE "+RECORD_ID+" =:recordId")
    Single<List<ValrepLandimpLotValuationDetail>> getLotValuationDetailsListSingle(String recordId);

    @Query("SELECT * FROM "+"ValrepLandimpLotValuationDetail"+" WHERE "+RECORD_ID+" =:recordId")
    Flowable<List<ValrepLandimpLotValuationDetail>> getLotValuationDetailsListFlowable(String recordId);

    @Query("SELECT * FROM "+"ValrepLandimpLotValuationDetail"+" WHERE "+UNIQUE_ID+" =:uniqueId")
    Single<ValrepLandimpLotValuationDetail> getLotValuationDetails(long uniqueId);

    @Update
    void updateDetails(ValrepLandimpLotValuationDetail detail);

    @Insert(onConflict = REPLACE)
    void insertLotValuationDetails(ValrepLandimpLotValuationDetail detail);

    /*@Query("SELECT * FROM ValrepLandimpLotValuationDetail"+" WHERE "+LANDIMP_LOT_VALUATION_ID+" =:lotValuationUniqueId")
    Single<List<ValrepLandimpLotValuationDetail>> getValrepLandimpLotValuationsDetailsByUid(long lotValuationUniqueId);*/
}
