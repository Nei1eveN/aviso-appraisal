package com.gdslinkasia.base.database.repository;

import android.app.Application;

import com.gdslinkasia.base.database.ApplicationDatabase;
import com.gdslinkasia.base.database.dao.CondoParkingValuationDao;
import com.gdslinkasia.base.model.details.ValrepLandimpParkingValuationDetail;

import java.util.List;

import io.reactivex.Flowable;
import io.reactivex.Single;

public class CondoParkingValuationLocalRepository {
    private CondoParkingValuationDao landImpDetailDao;

    public CondoParkingValuationLocalRepository(Application application) {
        landImpDetailDao = ApplicationDatabase.getInstance(application).condoParkingValuationDao();
    }

    public Single<List<ValrepLandimpParkingValuationDetail>> getDetailsList(String recordId) {
        return landImpDetailDao.getDetailsList(recordId);
    }

    public Flowable<List<ValrepLandimpParkingValuationDetail>> getListFlowable(String recordId) {
        return landImpDetailDao.getListFlowable(recordId);
    }

    public Single<ValrepLandimpParkingValuationDetail> getDetails(long uniqueId) {
        return landImpDetailDao.getDetails(uniqueId);
    }

    public void updateLotDetail(ValrepLandimpParkingValuationDetail detail) {
        landImpDetailDao.updateLotDetail(detail);
    }
}
