package com.gdslinkasia.base.database.dao;

import androidx.room.Dao;
import androidx.room.Insert;
import androidx.room.Query;
import androidx.room.Update;

import com.gdslinkasia.base.model.details.ValrepLandimpImpDetail;

import java.util.List;

import io.reactivex.Flowable;
import io.reactivex.Single;

import static androidx.room.OnConflictStrategy.REPLACE;
import static com.gdslinkasia.base.database.DatabaseUtils.RECORD_ID;
import static com.gdslinkasia.base.database.DatabaseUtils.UNIQUE_ID;

@Dao
public interface LandImpDetailDao {

    @Query("SELECT * FROM "+"ValrepLandimpImpDetail"+" WHERE "+RECORD_ID+" =:recordId")
    Single<List<ValrepLandimpImpDetail>> getValrepLandimpDetailsByRecordId(String recordId);

    @Query("SELECT * FROM "+"ValrepLandimpImpDetail"+" WHERE "+RECORD_ID+" =:recordId")
    Single<List<ValrepLandimpImpDetail>> getLandImpImpDetailsList(String recordId);

    @Query("SELECT * FROM "+"ValrepLandimpImpDetail"+" WHERE "+RECORD_ID+" =:recordId")
    Flowable<List<ValrepLandimpImpDetail>> getListFlowable(String recordId);

    @Query("SELECT * FROM "+"ValrepLandimpImpDetail"+" WHERE "+UNIQUE_ID+" =:uniqueId")
    Single<ValrepLandimpImpDetail> getPropertyDescDetail(long uniqueId);

    @Update
    void updateLotDetail(ValrepLandimpImpDetail detail);

    @Insert(onConflict = REPLACE)
    long insertValrepLandimpImpDetail(ValrepLandimpImpDetail detail);

    @Insert(onConflict = REPLACE)
    void insertValrepLandimpImpDetails(List<ValrepLandimpImpDetail> valrepLandimpImpDetails);

    @Query("DELETE FROM ValrepLandimpImpDetail")
    void deleteValrepLandimpImpDetails();
}
