package com.gdslinkasia.base.database.repository;

import android.app.Application;

import com.gdslinkasia.base.database.ApplicationDatabase;
import com.gdslinkasia.base.database.dao.QualifyingStatementDao;
import com.gdslinkasia.base.database.dao.RecordDao;
import com.gdslinkasia.base.model.details.ValrepLcIvsQualifyingStatement;
import com.gdslinkasia.base.model.job.Record;

import java.util.List;

import io.reactivex.Flowable;
import io.reactivex.Single;

public class QualifyingStatementLocalRepository {

    private QualifyingStatementDao qualifyingStatementDao;
    private RecordDao recordDao;
    private ApplicationDatabase applicationDatabase;

    public QualifyingStatementLocalRepository(Application application) {
        applicationDatabase = ApplicationDatabase.getInstance(application);
        qualifyingStatementDao = applicationDatabase.qualifyingStatementDao();
        recordDao = applicationDatabase.recordDao();
    }

    public Single<List<ValrepLcIvsQualifyingStatement>> getOwnershipPropertyDescList(String recordId) {
        return qualifyingStatementDao.getQualifyingStatementsByRecordId(recordId);
    }

    public Flowable<List<ValrepLcIvsQualifyingStatement>> getListFlowable(String recordId) {
        return qualifyingStatementDao.getListFlowable(recordId);
    }

    public Single<ValrepLcIvsQualifyingStatement> getPropertyDescDetail(long uniqueId) {
        return qualifyingStatementDao.getPropertyDescDetail(uniqueId);
    }

    public void updateLotDetail(ValrepLcIvsQualifyingStatement detail) {
        qualifyingStatementDao.updateLotDetail(detail);
    }

    public void insertLotDetail(ValrepLcIvsQualifyingStatement detail) {
        qualifyingStatementDao.insertLotDetail(detail);
    }

    public Single<Record> getRecordByRecordId(String recordId) {
        return recordDao.getRecordById(recordId);
    }

    //TODO DELETE ITEM BY UNIQUE ID
    public void deleteLotItem(long uniqueId) {
        qualifyingStatementDao.deleteLotItem(uniqueId);
    }

    //TODO DESTROY DATABASE INSTANCE
    public void onDestroy() {
        if (applicationDatabase != null)
            ApplicationDatabase.destroyInstance();
    }


}
