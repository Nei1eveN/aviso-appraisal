package com.gdslinkasia.base.database.repository;

import android.app.Application;

import com.gdslinkasia.base.database.ApplicationDatabase;
import com.gdslinkasia.base.database.dao.LandImpDetailDao;
import com.gdslinkasia.base.database.dao.LandImpValuationDao;
import com.gdslinkasia.base.database.dao.RecordDao;
import com.gdslinkasia.base.model.details.ValrepLandimpImpDetail;
import com.gdslinkasia.base.model.details.ValrepLandimpImpValuation;
import com.gdslinkasia.base.model.job.Record;

import java.util.List;

import io.reactivex.Flowable;
import io.reactivex.Single;

public class LandImpImpDetailLocalRepository {
    private LandImpDetailDao landImpDetailDao;
    private LandImpValuationDao valuationDao;
    private RecordDao recordDao;

    public LandImpImpDetailLocalRepository(Application application) {
        landImpDetailDao = ApplicationDatabase.getInstance(application).landImpDetailDao();
        valuationDao = ApplicationDatabase.getInstance(application).landImpValuationDao();
        recordDao = ApplicationDatabase.getInstance(application).recordDao();
    }

    public Single<List<ValrepLandimpImpDetail>> getLandImpImpDetailsList(String recordId) {
        return landImpDetailDao.getLandImpImpDetailsList(recordId);
    }

    public Flowable<List<ValrepLandimpImpDetail>> getListFlowable(String recordId) {
        return landImpDetailDao.getListFlowable(recordId);
    }

    public Single<ValrepLandimpImpDetail> getPropertyDescDetail(long uniqueId) {
        return landImpDetailDao.getPropertyDescDetail(uniqueId);
    }

    public void updateLotDetail(ValrepLandimpImpDetail detail) {
        landImpDetailDao.updateLotDetail(detail);
    }

    public long insertLandimpImpDetail(ValrepLandimpImpDetail detail) {
        return landImpDetailDao.insertValrepLandimpImpDetail(detail);
    }

    public Single<Record> getRecordById(String recordId) {
        return recordDao.getRecordById(recordId);
    }

    //TODO LandimpImpValuation METHODS
    public void insertLandimpImpValuation(ValrepLandimpImpValuation valuation) {
        valuationDao.insertValrepLandimpImpValuation(valuation);
    }

    public void updateLandimpImpValuation(ValrepLandimpImpValuation valuation) {
        valuationDao.updateLandimpImpValuation(valuation);
    }

    public Single<ValrepLandimpImpValuation> getValuationDetails(long uniqueId) {
        return valuationDao.getDetailsByDescId(uniqueId);
    }
}
