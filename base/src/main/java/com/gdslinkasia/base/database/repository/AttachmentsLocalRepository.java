package com.gdslinkasia.base.database.repository;

import android.app.Application;

import com.gdslinkasia.base.database.ApplicationDatabase;
import com.gdslinkasia.base.database.dao.appraisal_request.AppraisalRequestDao;
import com.gdslinkasia.base.database.dao.appraisal_request.AttachmentsDao;
import com.gdslinkasia.base.model.details.AppAttachment;
import com.gdslinkasia.base.model.details.AppraisalRequest;

import java.util.List;

import io.reactivex.Flowable;
import io.reactivex.Single;

public class AttachmentsLocalRepository {

    private AttachmentsDao attachmentsDao;
    private AppraisalRequestDao appraisalRequestDao;
    private ApplicationDatabase applicationDatabase;

    public AttachmentsLocalRepository(Application application) {
        applicationDatabase = ApplicationDatabase.getInstance(application);
        attachmentsDao = applicationDatabase.attachmentsDao();
        appraisalRequestDao = applicationDatabase.appraisalRequestDao();
    }

    public Single<List<AppAttachment>> getOwnershipPropertyDescList(long appraisalRequestId, String createdFrom) {
        return attachmentsDao.getAttachmentsDetailsList(appraisalRequestId, createdFrom);
    }
    public Single<AppraisalRequest> getAppraisalRequestByRecordId(String recordId) {
        return appraisalRequestDao.getAppraisalRequestByRecordId(recordId);
    }

    public Flowable<List<AppAttachment>> getListFlowable(long appraisalRequestId, String createdFrom) {
        return attachmentsDao.getListFlowable(appraisalRequestId, createdFrom);
    }

    public void updateAttachmentsDetail(AppAttachment detail) {
        attachmentsDao.updateAttachmentDetail(detail);
    }

    public void insertAttachmentsDetail(AppAttachment detail) {
        attachmentsDao.insertAppAttachment(detail);
    }

    //TODO DELETE ITEM BY UNIQUE ID
    public void deleteAttachmentsItem(long uniqueId) {
        attachmentsDao.deleteAttachmentItem(uniqueId);
    }

    //TODO DESTROY DATABASE INSTANCE
    public void onDestroy() {
        if (applicationDatabase != null)
            ApplicationDatabase.destroyInstance();
    }


}
