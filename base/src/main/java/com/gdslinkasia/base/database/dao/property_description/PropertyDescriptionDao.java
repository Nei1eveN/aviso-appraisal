package com.gdslinkasia.base.database.dao.property_description;

import androidx.room.Dao;
import androidx.room.Insert;
import androidx.room.Query;
import androidx.room.Update;

import com.gdslinkasia.base.model.details.ValrepLandimpLotDetail;

import java.util.List;

import io.reactivex.Flowable;
import io.reactivex.Single;

import static androidx.room.OnConflictStrategy.REPLACE;
import static com.gdslinkasia.base.database.DatabaseUtils.RECORD_ID;
import static com.gdslinkasia.base.database.DatabaseUtils.UNIQUE_ID;

@Dao
public interface PropertyDescriptionDao {

    @Query("SELECT * FROM "+"ValrepLandimpLotDetail"+" WHERE "+RECORD_ID+" =:recordId")
    Single<List<ValrepLandimpLotDetail>> getOwnershipPropertyDescList(String recordId);

    @Query("SELECT * FROM "+"ValrepLandimpLotDetail"+" WHERE "+RECORD_ID+" =:recordId")
    Flowable<List<ValrepLandimpLotDetail>> getListFlowable(String recordId);

    @Query("SELECT * FROM "+"ValrepLandimpLotDetail"+" WHERE "+UNIQUE_ID+" =:uniqueId")
    Single<ValrepLandimpLotDetail> getPropertyDescDetail(long uniqueId);

    @Update
    void updateLotDetail(ValrepLandimpLotDetail detail);

    @Insert(onConflict = REPLACE)
    void insertLotDetail(ValrepLandimpLotDetail detail);

    //TODO DELETE ITEM BY QUERYING UNIQUE ID
    @Query("DELETE FROM "+"ValrepLandimpLotDetail"+" WHERE "+UNIQUE_ID+" =:uniqueId")
    void deleteLotItem(long uniqueId);

    @Insert(onConflict = REPLACE)
    long insertOwnershipPropertyDesc(ValrepLandimpLotDetail lotDetail);
}
