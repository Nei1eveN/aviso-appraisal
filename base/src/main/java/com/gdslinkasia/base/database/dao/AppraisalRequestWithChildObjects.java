package com.gdslinkasia.base.database.dao;

import androidx.room.Embedded;
import androidx.room.Relation;

import com.gdslinkasia.base.model.details.*;

import java.util.List;

import static com.gdslinkasia.base.database.DatabaseUtils.*;


@SuppressWarnings("unused")
public class AppraisalRequestWithChildObjects {

    @Embedded
    private AppraisalRequest appraisalRequest;

    @Relation(parentColumn = UNIQUE_ID, entityColumn = APPRAISAL_REQUEST_ID, entity = AppAttachment.class)
    private List<AppAttachment> attachmentList;

    @Relation(parentColumn = UNIQUE_ID, entityColumn = APPRAISAL_REQUEST_ID, entity = AppContactPerson.class)
    private List<AppContactPerson> appContactPersonList;

    @Relation(parentColumn = UNIQUE_ID, entityColumn = APPRAISAL_REQUEST_ID, entity = AppCollateralAddress.class)
    private List<AppCollateralAddress> appCollateralAddresses;

    @Relation(parentColumn = UNIQUE_ID, entityColumn = APPRAISAL_REQUEST_ID, entity = CondoTitleDetail.class)
    private List<CondoTitleDetail> condoTitleDetails;

    @Relation(parentColumn = UNIQUE_ID, entityColumn = APPRAISAL_REQUEST_ID, entity = LandimpImpDetail.class)
    private List<LandimpImpDetail> landimpImpDetails;

    @Relation(parentColumn = UNIQUE_ID, entityColumn = APPRAISAL_REQUEST_ID, entity = LandimpTitleDetail.class)
    private List<LandimpTitleDetail> landimpTitleDetails;

    public AppraisalRequest getAppraisalRequest() {
        return appraisalRequest;
    }

    public void setAppraisalRequest(AppraisalRequest appraisalRequest) {
        this.appraisalRequest = appraisalRequest;
    }

    public List<AppAttachment> getAttachmentList() {
        return attachmentList;
    }

    public void setAttachmentList(List<AppAttachment> attachmentList) {
        this.attachmentList = attachmentList;
    }

    public List<AppContactPerson> getAppContactPersonList() {
        return appContactPersonList;
    }

    public void setAppContactPersonList(List<AppContactPerson> appContactPersonList) {
        this.appContactPersonList = appContactPersonList;
    }

    public List<AppCollateralAddress> getAppCollateralAddresses() {
        return appCollateralAddresses;
    }

    public void setAppCollateralAddresses(List<AppCollateralAddress> appCollateralAddresses) {
        this.appCollateralAddresses = appCollateralAddresses;
    }

    public List<CondoTitleDetail> getCondoTitleDetails() {
        return condoTitleDetails;
    }

    public void setCondoTitleDetails(List<CondoTitleDetail> condoTitleDetails) {
        this.condoTitleDetails = condoTitleDetails;
    }

    public List<LandimpImpDetail> getLandimpImpDetails() {
        return landimpImpDetails;
    }

    public void setLandimpImpDetails(List<LandimpImpDetail> landimpImpDetails) {
        this.landimpImpDetails = landimpImpDetails;
    }

    public List<LandimpTitleDetail> getLandimpTitleDetails() {
        return landimpTitleDetails;
    }

    public void setLandimpTitleDetails(List<LandimpTitleDetail> landimpTitleDetails) {
        this.landimpTitleDetails = landimpTitleDetails;
    }
}
