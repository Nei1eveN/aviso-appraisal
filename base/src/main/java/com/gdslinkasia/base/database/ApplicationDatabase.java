package com.gdslinkasia.base.database;

import android.content.Context;

import androidx.room.Database;
import androidx.room.Room;
import androidx.room.RoomDatabase;
import androidx.room.TypeConverters;

import com.gdslinkasia.base.database.converters.AppAttachmentTypeConverter;
import com.gdslinkasia.base.database.converters.AppCollateralAddressTypeConverter;
import com.gdslinkasia.base.database.converters.AppContactPersonTypeConverter;
import com.gdslinkasia.base.database.converters.AppraisalRequestTypeConverter;
import com.gdslinkasia.base.database.converters.CondoTitleDetailTypeConverter;
import com.gdslinkasia.base.database.converters.JobSystemTypeConverter;
import com.gdslinkasia.base.database.converters.LandimpImpDetailTypeConverter;
import com.gdslinkasia.base.database.converters.LandimpTitleDetailTypeConverter;
import com.gdslinkasia.base.database.converters.RecordTypeConverter;
import com.gdslinkasia.base.database.converters.ValrepLandimpAssumptionsRemarkTypeConverter;
import com.gdslinkasia.base.database.converters.ValrepLandimpImpDetailTypeConverter;
import com.gdslinkasia.base.database.converters.ValrepLandimpImpValuationTypeConverter;
import com.gdslinkasia.base.database.converters.ValrepLandimpLotDetailTypeConverter;
import com.gdslinkasia.base.database.converters.ValrepLandimpLotValuationDetailTypeConverter;
import com.gdslinkasia.base.database.converters.ValrepLandimpLotValuationTypeConverter;
import com.gdslinkasia.base.database.converters.ValrepLandimpOtherLandImpDetailTypeConverter;
import com.gdslinkasia.base.database.converters.ValrepLandimpOtherLandImpValueTypeConverter;
import com.gdslinkasia.base.database.converters.ValrepLandimpParkingValuationDetailTypeConverter;
import com.gdslinkasia.base.database.converters.ValrepLcIvsQualifyingStatementTypeConverter;
import com.gdslinkasia.base.database.dao.CondoParkingValuationDao;
import com.gdslinkasia.base.database.dao.JobsDao;
import com.gdslinkasia.base.database.dao.LandImpDetailDao;
import com.gdslinkasia.base.database.dao.LandImpOtherDetailDao;
import com.gdslinkasia.base.database.dao.LandImpOtherValuationDao;
import com.gdslinkasia.base.database.dao.LandImpValuationDao;
import com.gdslinkasia.base.database.dao.LandimpLotDetailDao;
import com.gdslinkasia.base.database.dao.ValrepLandimpLotValuationDao;
import com.gdslinkasia.base.database.dao.LandimpLotValuationDetailDao;
import com.gdslinkasia.base.database.dao.LotValuationDetailDao;
import com.gdslinkasia.base.database.dao.QualifyingStatementDao;
import com.gdslinkasia.base.database.dao.RecordDao;
import com.gdslinkasia.base.database.dao.RecordIDPreferenceDao;
import com.gdslinkasia.base.database.dao.SpecialAssumptionsDao;
import com.gdslinkasia.base.database.dao.appraisal_request.AppCollateralAddressDao;
import com.gdslinkasia.base.database.dao.appraisal_request.AppContactPersonDao;
import com.gdslinkasia.base.database.dao.appraisal_request.AppraisalRequestDao;
import com.gdslinkasia.base.database.dao.appraisal_request.AttachmentsDao;
import com.gdslinkasia.base.database.dao.appraisal_request.CondoTitleDetailDao;
import com.gdslinkasia.base.database.dao.appraisal_request.LandimpDetailDao;
import com.gdslinkasia.base.database.dao.appraisal_request.LandimpTitleDetailDao;
import com.gdslinkasia.base.database.dao.property_description.PropertyDescriptionDao;
import com.gdslinkasia.base.model.RecordIDPreference;
import com.gdslinkasia.base.model.details.AppAttachment;
import com.gdslinkasia.base.model.details.AppCollateralAddress;
import com.gdslinkasia.base.model.details.AppContactPerson;
import com.gdslinkasia.base.model.details.AppraisalRequest;
import com.gdslinkasia.base.model.details.CondoTitleDetail;
import com.gdslinkasia.base.model.details.JobSystem;
import com.gdslinkasia.base.model.details.LandimpImpDetail;
import com.gdslinkasia.base.model.details.LandimpTitleDetail;
import com.gdslinkasia.base.model.details.ValrepLandimpAssumptionsRemark;
import com.gdslinkasia.base.model.details.ValrepLandimpImpDetail;
import com.gdslinkasia.base.model.details.ValrepLandimpImpValuation;
import com.gdslinkasia.base.model.details.ValrepLandimpLotDetail;
import com.gdslinkasia.base.model.details.ValrepLandimpLotValuation;
import com.gdslinkasia.base.model.details.ValrepLandimpLotValuationDetail;
import com.gdslinkasia.base.model.details.ValrepLandimpOtherLandImpDetail;
import com.gdslinkasia.base.model.details.ValrepLandimpOtherLandImpValue;
import com.gdslinkasia.base.model.details.ValrepLandimpParkingValuationDetail;
import com.gdslinkasia.base.model.details.ValrepLcIvsQualifyingStatement;
import com.gdslinkasia.base.model.job.Record;

import static com.gdslinkasia.base.database.DatabaseUtils.DATABASE_NAME;


//TODO Key Points:
//TODO 1. Entity Table Names must be the named the same as their classes (ex.: PostMapper.class (TABLE NAME = "PostMapper")
@Database(
        entities = {
                AppAttachment.class,
                AppCollateralAddress.class,
                AppContactPerson.class,
                AppraisalRequest.class,
                CondoTitleDetail.class,
                JobSystem.class,
                LandimpImpDetail.class,
                LandimpTitleDetail.class,
                Record.class,
                ValrepLandimpAssumptionsRemark.class,
                ValrepLandimpImpDetail.class,
                ValrepLandimpImpValuation.class,
                ValrepLandimpLotDetail.class,
                ValrepLandimpLotValuation.class,
                ValrepLandimpLotValuationDetail.class,
                ValrepLandimpOtherLandImpDetail.class,
                ValrepLandimpOtherLandImpValue.class,
                ValrepLandimpParkingValuationDetail.class,
                ValrepLcIvsQualifyingStatement.class,
                RecordIDPreference.class
        },
        version = 1,
        exportSchema = false)
@TypeConverters(
        {
                AppAttachmentTypeConverter.class,
                AppCollateralAddressTypeConverter.class,
                AppContactPersonTypeConverter.class,
                AppraisalRequestTypeConverter.class,
                CondoTitleDetailTypeConverter.class,
                JobSystemTypeConverter.class,
                LandimpImpDetailTypeConverter.class,
                LandimpTitleDetailTypeConverter.class,
                RecordTypeConverter.class,
                ValrepLandimpAssumptionsRemarkTypeConverter.class,
                ValrepLandimpImpDetailTypeConverter.class,
                ValrepLandimpImpValuationTypeConverter.class,
                ValrepLandimpLotDetailTypeConverter.class,
                ValrepLandimpLotValuationDetailTypeConverter.class,
                ValrepLandimpLotValuationTypeConverter.class,
                ValrepLandimpOtherLandImpDetailTypeConverter.class,
                ValrepLandimpOtherLandImpValueTypeConverter.class,
                ValrepLandimpParkingValuationDetailTypeConverter.class,
                ValrepLcIvsQualifyingStatementTypeConverter.class
        })
public abstract class ApplicationDatabase extends RoomDatabase {

    private static ApplicationDatabase INSTANCE;

    public abstract AppraisalRequestDao appraisalRequestDao();
    public abstract AppCollateralAddressDao collateralAddressDao();
    public abstract AppContactPersonDao contactPersonDao();

    public abstract CondoTitleDetailDao condoTitleDetailDao();
    public abstract LandimpDetailDao landimpDetailDao();
    public abstract LandimpTitleDetailDao landimpTitleDetailDao();

    public abstract JobsDao jobsDao();
    public abstract RecordIDPreferenceDao idPreferenceDao();
    public abstract RecordDao recordDao();

    public abstract ValrepLandimpLotValuationDao landimpLotValuationDao();
    public abstract LandimpLotValuationDetailDao landimpLotValuationDetailDao();

    public abstract LandimpLotDetailDao landimpLotDetailDao();

    public abstract PropertyDescriptionDao descriptionDao();
    //Property Desc -> land improvement
    public abstract LandImpDetailDao landImpDetailDao();
    //Property Desc -> land with other improvement
    public abstract LandImpOtherDetailDao landImpOtherDetailDao();
    //Improvement Valuation -> land improvement
    public abstract LandImpValuationDao landImpValuationDao();
    //Improvement Valuation -> land other improvement
    public abstract LandImpOtherValuationDao landImpOtherValuationDao();
    //Unit Valuation - > Parking
    public abstract CondoParkingValuationDao condoParkingValuationDao();
    //Scope of Work - > Special Assumptions
    public abstract SpecialAssumptionsDao specialAssumptionsDao();
    //Scope of Work - > Qualifying Statement
    public abstract QualifyingStatementDao qualifyingStatementDao();
    //Attachments
    public abstract AttachmentsDao attachmentsDao();
    //Land Valuation -> Land (Market Approach)
    public abstract LotValuationDetailDao lotValuationDetailDao();


    public synchronized static ApplicationDatabase getInstance(Context context) {
        if (INSTANCE == null) {
            INSTANCE = Room.databaseBuilder(context, ApplicationDatabase.class, DATABASE_NAME)
                    .allowMainThreadQueries()
                    .build();
        }
        return INSTANCE;
    }

    public static void destroyInstance() {
        INSTANCE = null;
    }
}
