package com.gdslinkasia.base.database.repository;

import android.app.Application;

import com.gdslinkasia.base.database.ApplicationDatabase;
import com.gdslinkasia.base.database.dao.LotValuationDetailDao;
import com.gdslinkasia.base.database.dao.RecordDao;
import com.gdslinkasia.base.database.dao.ValrepLandimpLotValuationDao;
import com.gdslinkasia.base.database.dao.property_description.PropertyDescriptionDao;
import com.gdslinkasia.base.model.details.ValrepLandimpLotDetail;
import com.gdslinkasia.base.model.details.ValrepLandimpLotValuation;
import com.gdslinkasia.base.model.details.ValrepLandimpLotValuationDetail;
import com.gdslinkasia.base.model.job.Record;

import java.util.List;

import io.reactivex.Flowable;
import io.reactivex.Single;

public class PropertyDescriptionLocalRepository {

    private PropertyDescriptionDao descriptionDao;
    private RecordDao recordDao;
    private ValrepLandimpLotValuationDao valrepLandimpLotValuationDao;
    private LotValuationDetailDao lotValuationDetailDao;
    private ApplicationDatabase applicationDatabase;

    public PropertyDescriptionLocalRepository(Application application) {
        applicationDatabase = ApplicationDatabase.getInstance(application);
        descriptionDao = applicationDatabase.descriptionDao();
        recordDao = applicationDatabase.recordDao();
        valrepLandimpLotValuationDao = applicationDatabase.landimpLotValuationDao();
        lotValuationDetailDao = applicationDatabase.lotValuationDetailDao();
    }

    public Single<List<ValrepLandimpLotDetail>> getOwnershipPropertyDescList(String recordId) {
        return descriptionDao.getOwnershipPropertyDescList(recordId);
    }

    public Flowable<List<ValrepLandimpLotDetail>> getListFlowable(String recordId) {
        return descriptionDao.getListFlowable(recordId);
    }

    public Single<ValrepLandimpLotDetail> getPropertyDescDetail(long uniqueId) {
        return descriptionDao.getPropertyDescDetail(uniqueId);
    }

    public void updateLotDetail(ValrepLandimpLotDetail detail) {
        descriptionDao.updateLotDetail(detail);
    }

    public void insertLotDetail(ValrepLandimpLotDetail detail) {
        descriptionDao.insertLotDetail(detail);
    }

    public Single<Record> getRecordByRecordId(String recordId) {
        return recordDao.getRecordById(recordId);
    }

    //TODO DELETE ITEM BY UNIQUE ID
    public void deleteLotItem(long uniqueId) {
        descriptionDao.deleteLotItem(uniqueId);
    }

    //TODO DESTROY DATABASE INSTANCE
    public void onDestroy() {
        if (applicationDatabase != null)
            ApplicationDatabase.destroyInstance();
    }


    public long insertOwnershipPropertyDesc(ValrepLandimpLotDetail detail) {
        return descriptionDao.insertOwnershipPropertyDesc(detail);
    }

    public Single<ValrepLandimpLotValuation> getValrepLandimpLotValuationByUid(long recordUniqueId) {
        return valrepLandimpLotValuationDao.getLotValuationsByUid(recordUniqueId);
    }

   /* public Single<List<ValrepLandimpLotValuationDetail>> getValrepLandimpLotValuationDetailsByUid(long lotValuationUniqueId) {
        return lotValuationDetailDao.getValrepLandimpLotValuationsDetailsByUid(lotValuationUniqueId);
    }*/

    public void insertLotValuationDetail(ValrepLandimpLotValuationDetail lotValuationDetail) {
        lotValuationDetailDao.insertLotValuationDetails(lotValuationDetail);
    }
}
