package com.gdslinkasia.base.database.converters;

import androidx.room.TypeConverter;

import com.gdslinkasia.base.model.job.Record;
import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;

import java.lang.reflect.Type;
import java.util.Collections;
import java.util.List;

public class RecordTypeConverter {
    private Gson gson = new Gson();
    private Type type = new TypeToken<List<Record>>(){}.getType();

    @TypeConverter
    public List<Record> stringToRecord(String jsonToStringValue) {
        if (jsonToStringValue == null) {
            return Collections.emptyList();
        }
        return gson.fromJson(jsonToStringValue, type);
    }

    @TypeConverter
    public String recordToString(List<Record> value) {
        return gson.toJson(value, type);
    }
}
