package com.gdslinkasia.base.database.dao;

import androidx.room.Dao;
import androidx.room.Insert;
import androidx.room.Query;

import com.gdslinkasia.base.model.details.ValrepLandimpLotDetail;

import java.util.List;

import io.reactivex.Single;

import static androidx.room.OnConflictStrategy.REPLACE;
import static com.gdslinkasia.base.database.DatabaseUtils.RECORD_ID;

@Dao
public abstract class LandimpLotDetailDao {

    @Query("SELECT * FROM "+"ValrepLandimpLotDetail"+" WHERE "+RECORD_ID+" =:recordId")
    public abstract Single<List<ValrepLandimpLotDetail>> getLandimpLotDetailsByRecordId(String recordId);

    @Insert(onConflict = REPLACE)
    public abstract void insertLandimpLotDetails(List<ValrepLandimpLotDetail> landimpLotDetails);

    @Query("DELETE FROM ValrepLandimpLotDetail")
    public abstract void deleteLandimpLotDetails();
}
