package com.gdslinkasia.base.database.dao;

import androidx.room.Dao;
import androidx.room.Insert;
import androidx.room.Query;
import androidx.room.Update;

import com.gdslinkasia.base.model.details.ValrepLcIvsQualifyingStatement;

import java.util.List;

import io.reactivex.Flowable;
import io.reactivex.Single;

import static androidx.room.OnConflictStrategy.REPLACE;
import static com.gdslinkasia.base.database.DatabaseUtils.RECORD_ID;
import static com.gdslinkasia.base.database.DatabaseUtils.UNIQUE_ID;

@Dao
public interface QualifyingStatementDao {

    @Query("SELECT * FROM " + "ValrepLcIvsQualifyingStatement" + " WHERE " + RECORD_ID + " =:recordId")
    Single<List<ValrepLcIvsQualifyingStatement>> getQualifyingStatementsByRecordId(String recordId);

    @Query("SELECT * FROM " + "ValrepLcIvsQualifyingStatement" + " WHERE " + RECORD_ID + " =:recordId")
    Flowable<List<ValrepLcIvsQualifyingStatement>> getListFlowable(String recordId);

    @Query("SELECT * FROM " + "ValrepLcIvsQualifyingStatement" + " WHERE " + UNIQUE_ID + " =:uniqueId")
    Single<ValrepLcIvsQualifyingStatement> getPropertyDescDetail(long uniqueId);

    @Insert(onConflict = REPLACE)
    void insertQualifyingStatements(List<ValrepLcIvsQualifyingStatement> qualifyingStatements);

    @Insert(onConflict = REPLACE)
    void insertLotDetail(ValrepLcIvsQualifyingStatement detail);

    @Update
    void updateLotDetail(ValrepLcIvsQualifyingStatement detail);

    //TODO DELETE ITEM BY QUERYING UNIQUE ID
    @Query("DELETE FROM " + "ValrepLcIvsQualifyingStatement" + " WHERE " + UNIQUE_ID + " =:uniqueId")
    void deleteLotItem(long uniqueId);

    @Query("DELETE FROM ValrepLcIvsQualifyingStatement")
    void deleteQualifyingStatements();
}
