package com.gdslinkasia.base.database.dao;

import androidx.room.Dao;
import androidx.room.Query;
import androidx.room.Transaction;
import androidx.room.Update;

import com.gdslinkasia.base.model.job.Record;

import io.reactivex.Flowable;
import io.reactivex.Single;

import static androidx.room.OnConflictStrategy.REPLACE;
import static com.gdslinkasia.base.database.DatabaseUtils.RECORD_ID;
import static com.gdslinkasia.base.database.DatabaseUtils.RECORD_TABLE;
import static com.gdslinkasia.base.database.DatabaseUtils.UNIQUE_ID;

@Dao
public abstract class RecordDao {

    @Transaction
    @Query("SELECT * FROM "+"Record"+" WHERE "+UNIQUE_ID+" =:recordUid")
    public abstract Single<RecordWithAppraisalRequest> getRecordWithAppraisalRequest(long recordUid);

    @Query("SELECT * FROM " + RECORD_TABLE + " WHERE " + RECORD_ID + " = :recordId")
    public abstract Single<Record> getRecordById(String recordId);

    @Query("SELECT * FROM " + RECORD_TABLE + " WHERE " + RECORD_ID + " = :recordId")
    public abstract Flowable<Record> getFlowableRecordById(String recordId);

    @Query("SELECT * FROM "+"Record"+" WHERE "+UNIQUE_ID+" =:recordUId")
    public abstract Single<Record> getRecordByUId(long recordUId);

    @Update(onConflict = REPLACE)
    public abstract void updateRecord(Record record);

}
