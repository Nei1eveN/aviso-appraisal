package com.gdslinkasia.base.database.dao;

import androidx.room.Embedded;
import androidx.room.Relation;

import com.gdslinkasia.base.model.details.AppraisalRequest;
import com.gdslinkasia.base.model.job.Record;

import java.util.List;

import static com.gdslinkasia.base.database.DatabaseUtils.*;

public class RecordWithAppraisalRequest {

    @Embedded
    private Record record;

    @Relation(parentColumn = UNIQUE_ID, entityColumn = RECORD_UID, entity = AppraisalRequest.class)
    private List<AppraisalRequest> appraisalRequests;

    public Record getRecord() {
        return record;
    }

    public void setRecord(Record record) {
        this.record = record;
    }

    public List<AppraisalRequest> getAppraisalRequests() {
        return appraisalRequests;
    }

    public void setAppraisalRequests(List<AppraisalRequest> appraisalRequests) {
        this.appraisalRequests = appraisalRequests;
    }
}
