package com.gdslinkasia.base.database.repository;

import android.annotation.SuppressLint;
import android.app.Application;
import android.util.Log;

import com.gdslinkasia.base.model.RecordIDPreference;
import com.gdslinkasia.base.model.details.AppAttachment;
import com.gdslinkasia.base.model.details.AppCollateralAddress;
import com.gdslinkasia.base.model.details.AppContactPerson;
import com.gdslinkasia.base.model.details.AppraisalRequest;
import com.gdslinkasia.base.model.details.CondoTitleDetail;
import com.gdslinkasia.base.model.details.LandimpImpDetail;
import com.gdslinkasia.base.model.details.LandimpTitleDetail;
import com.gdslinkasia.base.model.details.ValrepLandimpAssumptionsRemark;
import com.gdslinkasia.base.model.details.ValrepLandimpImpDetail;
import com.gdslinkasia.base.model.details.ValrepLandimpImpValuation;
import com.gdslinkasia.base.model.details.ValrepLandimpLotDetail;
import com.gdslinkasia.base.model.details.ValrepLandimpLotValuation;
import com.gdslinkasia.base.model.details.ValrepLandimpLotValuationDetail;
import com.gdslinkasia.base.model.details.ValrepLandimpOtherLandImpDetail;
import com.gdslinkasia.base.model.details.ValrepLandimpOtherLandImpValue;
import com.gdslinkasia.base.model.details.ValrepLandimpParkingValuationDetail;
import com.gdslinkasia.base.model.details.ValrepLcIvsQualifyingStatement;
import com.gdslinkasia.base.model.job.JobSubmitResult;
import com.gdslinkasia.base.model.job.Record;
import com.gdslinkasia.base.networking.api.RetrofitClient;
import com.gdslinkasia.base.networking.api.RetrofitService;
import com.gdslinkasia.base.utils.GlobalString;
import com.google.gson.Gson;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import io.reactivex.Completable;
import io.reactivex.Flowable;
import io.reactivex.Single;
import io.reactivex.internal.operators.single.SingleEquals;
import io.reactivex.schedulers.Schedulers;

import static com.gdslinkasia.base.utils.GlobalFunctions.JOB_RECORD_DETAILS_JSON_BUILDER;
import static com.gdslinkasia.base.utils.GlobalString.FOR_ACCEPTANCE;
import static com.gdslinkasia.base.utils.GlobalString.auth_token_value;
import static com.gdslinkasia.base.utils.GlobalString.server_url;

public class JobDetailsRemoteRepository {

    private JobDetailsLocalRepository localRepository;

    public JobDetailsRemoteRepository(Application application) {
        localRepository = new JobDetailsLocalRepository(application);
    }

    @SuppressLint("LongLogTag")
    public Single<Record> getRecordById(String recordId) throws JSONException {
        return SingleEquals.zip(localRepository.getRecordById(recordId),
                RetrofitClient.getApiClient(server_url)
                        .create(RetrofitService.class)
                        .getJobRecordById(auth_token_value, String.valueOf(JOB_RECORD_DETAILS_JSON_BUILDER(recordId))), (record, record2) -> {

                    Completable.fromAction(() -> {
                        //TODO DELETE APPRAISAL REQUEST
                        localRepository.deleteAppraisalRequests();

                        localRepository.deleteAssumptionRemarks();
                        localRepository.deleteValrepLandimpImpDetails();
                        localRepository.deleteValrepLandimpLotDetails();
                        localRepository.deleteLandimpLotValuations();
                        localRepository.deleteValrepLandimpLotValuationDetails();
                        localRepository.deleteValrepLandimpOtherLandImpDetails();
                        localRepository.deleteValrepLandimpParkingValuationDetails();
                        localRepository.deleteQualifyingStatements();
                    }).subscribe();

                    //TODO APPRAISAL REQUEST
                    List<AppraisalRequest> appraisalRequests = new ArrayList<>();
                    for (int i = 0; i < record2.getAppraisalRequest().size(); i++) {
                        AppraisalRequest appraisalRequest = record2.getAppraisalRequest().get(i);
                        Log.d("int--RecordUId", String.valueOf(record.getUniqueId()));
                        appraisalRequest.setRecordUId(record.getUniqueId());
                        appraisalRequest.setRecordId(recordId);

                        long appraisalRequestId = localRepository.insertAppraisalRequest(appraisalRequest);
                        Log.d("int--appraisalReqId", String.valueOf(appraisalRequestId));

                        //TODO APP ATTACHMENT
                        for (AppAttachment appAttachment : appraisalRequest.getAppAttachment()) {
                            List<AppAttachment> appAttachments = new ArrayList<>();
                            appAttachment.setAppraisalRequestId(appraisalRequestId);
                            appAttachment.setCreatedFrom("Server");
                            localRepository.insertAppAttachment(appAttachment);
                            appAttachments.add(appAttachment);


                            appraisalRequest.setAppAttachment(appAttachments);
                        }

                        //TODO APP CONTACT PERSON
                        List<AppContactPerson> contactPersonList = new ArrayList<>();
                        for (AppContactPerson contactPerson : appraisalRequest.getAppContactPerson()) {
                            contactPerson.setAppraisalRequestId(appraisalRequestId);
                            localRepository.insertContactPerson(contactPerson);
                            contactPersonList.add(contactPerson);
                            appraisalRequest.setAppContactPerson(contactPersonList);
                        }

                        //TODO APP COLLATERAL ADDRESS
                        List<AppCollateralAddress> collateralAddresses = new ArrayList<>();
                        if (appraisalRequest.getAppCollateralAddress() == null && appraisalRequest.getAppRequestAppraisal().equals(FOR_ACCEPTANCE)) {
                            AppCollateralAddress address = new AppCollateralAddress();
                            address.setAppraisalRequestId(appraisalRequestId);

                            collateralAddresses.add(address);

                            appraisalRequest.setAppCollateralAddress(collateralAddresses);
                        } else {
                            for (AppCollateralAddress address : appraisalRequest.getAppCollateralAddress()) {
                                address.setAppraisalRequestId(appraisalRequestId);
                                collateralAddresses.add(address);

                                appraisalRequest.setAppCollateralAddress(collateralAddresses);
                            }
                        }

                        //TODO CONDO TITLE DETAIL
                        for (CondoTitleDetail detail : appraisalRequest.getCondoTitleDetails()) {
                            List<CondoTitleDetail> condoTitleDetails = new ArrayList<>();
                            detail.setAppraisalRequestId(appraisalRequestId);
                            localRepository.insertCondoTitleDetail(detail);
                            condoTitleDetails.add(detail);

                            appraisalRequest.setCondoTitleDetails(condoTitleDetails);
                        }

                        //TODO LANDIMP IMP DETAIL
                        for (LandimpImpDetail detail : appraisalRequest.getLandimpImpDetails()) {
                            List<LandimpImpDetail> landimpImpDetails = new ArrayList<>();
                            detail.setAppraisalRequestId(appraisalRequestId);
                            localRepository.insertLandimpImpDetail(detail);
                            landimpImpDetails.add(detail);

                            appraisalRequest.setLandimpImpDetails(landimpImpDetails);
                        }

                        //TODO LANDIMP TITLE DETAIL
                        for (LandimpTitleDetail detail : appraisalRequest.getLandimpTitleDetails()) {
                            List<LandimpTitleDetail> landimpTitleDetails = new ArrayList<>();
                            detail.setAppraisalRequestId(appraisalRequestId);
                            localRepository.insertLandimpTitleDetail(detail);
                            landimpTitleDetails.add(detail);

                            appraisalRequest.setLandimpTitleDetails(landimpTitleDetails);
                        }

//                        if (i == (appraisalRequest1.size() - 1)) {
                        appraisalRequests.add(appraisalRequest);
//                        }

                        long[] collateralAddressIds = localRepository.insertCollateralAddress(collateralAddresses);
                        Log.d("int--CollAddressIds", Arrays.toString(collateralAddressIds));

                        //                            for (AppraisalRequest appraisalRequest1 : appraisalRequests) {
                        //                                List<AppraisalRequest> appraisalRequests1 = new ArrayList<>();
                        //                                appraisalRequest1.setUniqueId(appraisalRequestId);
                        //
                        //                                appraisalRequests1.add(appraisalRequest1);
                        //
                        //                                record.setAppraisalRequest(appraisalRequests1);
                        //                            }

                        record.setAppraisalRequest(appraisalRequests);
                        localRepository.updateAppraisalRequests(appraisalRequests);
                    }

                    List<ValrepLandimpAssumptionsRemark> assumptionsRemarks = new ArrayList<>();
                    List<ValrepLandimpImpDetail> valrepLandimpImpDetails = new ArrayList<>();
                    List<ValrepLandimpImpValuation> landimpImpValuations = new ArrayList<>();
                    List<ValrepLandimpLotDetail> landimpLotDetails = new ArrayList<>();
                    List<ValrepLandimpLotValuation> lotValuations = new ArrayList<>();
                    List<ValrepLandimpOtherLandImpDetail> otherLandImpDetails = new ArrayList<>();
                    List<ValrepLandimpOtherLandImpValue> otherLandImpValues = new ArrayList<>();
                    List<ValrepLandimpParkingValuationDetail> parkingValuationDetails = new ArrayList<>();
                    List<ValrepLcIvsQualifyingStatement> qualifyingStatements = new ArrayList<>();

                    //TODO [SPECIAL ASSUMPTIONS REMARKS](ValrepLandimpAssumptionsRemark)
                    Log.d("int--assumptionSize", String.valueOf(record2.getValrepLandimpAssumptionsRemarks().size()));
                    for (ValrepLandimpAssumptionsRemark remark : record2.getValrepLandimpAssumptionsRemarks()) {
                        remark.setRecordId(record.getRecordId());
                        remark.setRecordUId(record.getUniqueId());

                        assumptionsRemarks.add(remark);

                        localRepository.insertAssumptionRemark(remark);
                    }
//                    localRepository.insertAssumptionsRemarks(assumptionsRemarks);
                    record.setValrepLandimpAssumptionsRemarks(assumptionsRemarks);

                    //TODO [ValrepLandimpImpDetail] SET RECORD_ID + UNIQUE_ID
                    for (ValrepLandimpImpDetail detail : record2.getValrepLandimpImpDetails()) {
                        detail.setRecordId(record.getRecordId());
                        detail.setRecordUId(record.getUniqueId());

                        long descImpId = localRepository.insertValrepLandimpImpDetail(detail);

                        valrepLandimpImpDetails.add(detail);

                        record.setValrepLandimpImpDetails(valrepLandimpImpDetails);

                        //TODO [ValrepLandimpImpValuation] SET RECORD_ID + UNIQUE_ID
                        for (ValrepLandimpImpValuation valuation : record2.getValrepLandimpImpValuation()) {
                            valuation.setRecordId(record.getRecordId());
                            valuation.setRecordUId(record.getUniqueId());
                            valuation.setDescId(descImpId);

                            landimpImpValuations.add(valuation);
                            localRepository.insertLandimpImpValuations(landimpImpValuations);
                            record.setValrepLandimpImpValuation(landimpImpValuations);
                        }
                    }

                    //TODO PROPERTY DESCRIPTION ([ValrepLandimpLotDetail] SET RECORD_ID + UNIQUE_ID)
                    long ownershipPropertyDescriptionId = 0;
                    if (record2.getValrepLandimpLotDetails() == null || record2.getValrepLandimpLotDetails().isEmpty()) {
                        Log.d("int--nullLotDetails", "Logic Triggered");
                        ValrepLandimpLotDetail detail = new ValrepLandimpLotDetail();
                        detail.setRecordId(record.getRecordId());
                        detail.setRecordUId(record.getUniqueId());

                        ownershipPropertyDescriptionId = localRepository.insertValrepLandimpLotDetail(detail);

                        landimpLotDetails.add(detail);
                    } else {
                        Log.d("int--notNullLotDetails", "Logic Triggered");
                        for (ValrepLandimpLotDetail detail : record2.getValrepLandimpLotDetails()) {
                            detail.setRecordId(record.getRecordId());
                            detail.setRecordUId(record.getUniqueId());

                            ownershipPropertyDescriptionId = localRepository.insertValrepLandimpLotDetail(detail);

                            landimpLotDetails.add(detail);

                        }
                    }
                    record.setValrepLandimpLotDetails(landimpLotDetails);

                    //TODO [ValrepLandimpLotValuation] SET RECORD_ID + UNIQUE_ID
                    if (record2.getValrepLandimpLotValuation() == null || record2.getValrepLandimpLotValuation().isEmpty()) {
                        Log.d("int--nullLotValuation", "Logic Triggered");
                        ValrepLandimpLotValuation valuation = new ValrepLandimpLotValuation();
                        valuation.setRecordId(record.getRecordId());
                        valuation.setRecordUId(record.getUniqueId());

                        long lotValuationId = localRepository.insertLandimpLotValuation(valuation);

                        //TODO Land MARKET / Condo PARKING APPROACH [ValrepLandimpLotValuationDetail] SET RECORD_ID + UNIQUE_ID + LANDIMP_LOT_VALUATION_ID from ValrepLandimpLotValuation
                        List<ValrepLandimpLotValuationDetail> lotValuationDetails = new ArrayList<>();
                        ValrepLandimpLotValuationDetail lotValuationDetail = new ValrepLandimpLotValuationDetail();

                        lotValuationDetail.setRecordId(record.getRecordId());
                        lotValuationDetail.setLotValuationId(lotValuationId);
                        lotValuationDetail.setOwnershipPropertyDescId(ownershipPropertyDescriptionId);

                        lotValuationDetails.add(lotValuationDetail);

                        //TODO SAVE Land MARKET / Condo PARKING APPROACH
                        localRepository.insertLotValuationDetails(lotValuationDetails);
                        valuation.setValrepLandimpLotValuationDetails(lotValuationDetails);

                        lotValuations.add(valuation);
                        record.setValrepLandimpLotValuation(lotValuations);
                    } else {
                        Log.d("int--notNullLotValuation", "Logic Triggered");
                        for (ValrepLandimpLotValuation valuation : record2.getValrepLandimpLotValuation()) {
                            valuation.setRecordId(record.getRecordId());
                            valuation.setRecordUId(record.getUniqueId());

                            long lotValuationId = localRepository.insertLandimpLotValuation(valuation);

                            //TODO Land MARKET / Condo PARKING APPROACH [ValrepLandimpLotValuationDetail] SET RECORD_ID + UNIQUE_ID + LANDIMP_LOT_VALUATION_ID from ValrepLandimpLotValuation
                            List<ValrepLandimpLotValuationDetail> lotValuationDetails = new ArrayList<>();
                            if (valuation.getValrepLandimpLotValuationDetails() == null) {
                                Log.d("int--nullLotValuationDetail", "Logic Triggered");
                                ValrepLandimpLotValuationDetail lotValuationDetail = new ValrepLandimpLotValuationDetail();

                                lotValuationDetail.setRecordId(record.getRecordId());
                                lotValuationDetail.setLotValuationId(lotValuationId);
                                lotValuationDetail.setOwnershipPropertyDescId(ownershipPropertyDescriptionId);

                                lotValuationDetails.add(lotValuationDetail);
                            } else {
                                Log.d("int--notNullLotValuationDetail", "Logic Triggered");
                                for (ValrepLandimpLotValuationDetail lotValuationDetail : valuation.getValrepLandimpLotValuationDetails()) {
                                    lotValuationDetail.setRecordId(record.getRecordId());
                                    lotValuationDetail.setLotValuationId(lotValuationId);
                                    lotValuationDetail.setOwnershipPropertyDescId(ownershipPropertyDescriptionId);

                                    lotValuationDetails.add(lotValuationDetail);
                                }
                            }

                            //TODO SAVE Land MARKET / Condo PARKING APPROACH
                            localRepository.insertLotValuationDetails(lotValuationDetails);
                            valuation.setValrepLandimpLotValuationDetails(lotValuationDetails);

                            lotValuations.add(valuation);
                            record.setValrepLandimpLotValuation(lotValuations);
                        }
                    }



                    //TODO [ValrepLandimpOtherLandImpDetail] SET RECORD_ID + UNIQUE_ID
                    for (ValrepLandimpOtherLandImpDetail detail : record2.getValrepLandimpOtherLandImpDetails()) {
                        detail.setRecordId(record.getRecordId());
                        detail.setRecordUId(record.getUniqueId());


                        long otherLandimpDetailId = localRepository.insertOtherLandImpDetail(detail);

                        otherLandImpDetails.add(detail);
//                        localRepository.insertOtherLandImpDetails(otherLandImpDetails);
                        record.setValrepLandimpOtherLandImpDetails(otherLandImpDetails);

                        //TODO [ValrepLandimpOtherLandImpValue] SET RECORD_ID + UNIQUE_ID + LANDIMP_OTHER_DESC_IMP_ID from ValrepLandimpOtherLandImpDetail
                        for (ValrepLandimpOtherLandImpValue otherLandImpValue : record2.getValrepLandimpOtherLandImpValue()) {
                            otherLandImpValue.setRecordId(record.getRecordId());
                            otherLandImpValue.setRecordUId(record.getUniqueId());
                            otherLandImpValue.setOtherDescImpId(otherLandimpDetailId);

                            otherLandImpValues.add(otherLandImpValue);
                        }
                    }
                    localRepository.insertOtherLandImpValues(otherLandImpValues);
                    record.setValrepLandimpOtherLandImpValue(otherLandImpValues);

                    //TODO [ValrepLandimpParkingValuationDetail] SET RECORD_ID
                    for (ValrepLandimpParkingValuationDetail parkingValuationDetail : record2.getValrepLandimpParkingValuationDetails()) {
                        parkingValuationDetail.setRecordId(record.getRecordId());
                        parkingValuationDetail.setRecordUId(record.getUniqueId());

                        parkingValuationDetails.add(parkingValuationDetail);
                    }
                    localRepository.insertParkingValuationDetails(parkingValuationDetails);
                    record.setValrepLandimpParkingValuationDetails(parkingValuationDetails);

                    //TODO [ValrepLcIvsQualifyingStatement] SET RECORD_ID
                    for (ValrepLcIvsQualifyingStatement qualifyingStatement : record2.getValrepLcIvsQualifyingStatements()) {
                        qualifyingStatement.setRecordId(record.getRecordId());
                        qualifyingStatement.setRecordUId(record.getUniqueId());

                        qualifyingStatements.add(qualifyingStatement);
                    }
                    localRepository.insertQualifyingStatements(qualifyingStatements);
                    record.setValrepLcIvsQualifyingStatements(qualifyingStatements);

                    localRepository.updateRecord(record);

                    //TODO SAVE TO LOCAL PREFERENCE
                    RecordIDPreference recordIDPreference = new RecordIDPreference();
                    recordIDPreference.setRecordId(record.getRecordId());

                    localRepository.insertRecordPreference(recordIDPreference);

                    Log.d("int--RemoteJobDetails", "Job Details: end point reached");

                    return record;
                })
//                .doOnSuccess(getConsumer())
                .doOnError(Throwable::printStackTrace)
                .subscribeOn(Schedulers.io());
    }


    public Flowable<JobSubmitResult> saveDataToServer(Record record) throws JSONException {
        JSONObject recordObject = new JSONObject();
        Gson gson = new Gson();
        JSONObject jsonObject = new JSONObject(gson.toJson(record));
        recordObject.put(GlobalString.record, jsonObject);
        Log.d("int--recordObject", String.valueOf(recordObject));
        return RetrofitClient.getApiClient(server_url)
                .create(RetrofitService.class)
                .saveToServer(auth_token_value, String.valueOf(JOB_RECORD_DETAILS_JSON_BUILDER(record.getRecordId())), String.valueOf(recordObject))
//                .subscribeOn(Schedulers.computation())
                ;
    }

}
