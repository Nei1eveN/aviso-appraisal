package com.gdslinkasia.base.database.dao;

import androidx.room.Dao;
import androidx.room.Insert;
import androidx.room.Query;

import com.gdslinkasia.base.model.details.ValrepLandimpLotValuation;

import java.util.List;

import io.reactivex.Single;

import static androidx.room.OnConflictStrategy.REPLACE;
import static com.gdslinkasia.base.database.DatabaseUtils.RECORD_ID;
import static com.gdslinkasia.base.database.DatabaseUtils.RECORD_UID;

@Dao
public abstract class ValrepLandimpLotValuationDao {

    @Query("SELECT * FROM "+"ValrepLandimpLotValuation"+" WHERE "+RECORD_ID+" =:recordId")
    public abstract Single<List<ValrepLandimpLotValuation>> getLotValuationsByRecordId(String recordId);

    @Insert(onConflict = REPLACE)
    public abstract void insertLotValuations(List<ValrepLandimpLotValuation> lotValuations);

    @Insert(onConflict = REPLACE)
    public abstract long insertLandimpLotValuation(ValrepLandimpLotValuation valuation);

    @Query("DELETE FROM ValrepLandimpLotValuation")
    public abstract void deleteLandimpLotValuations();

    @Query("SELECT * FROM "+"ValrepLandimpLotValuation"+" WHERE "+RECORD_UID+" =:recordUniqueId")
    public abstract Single<ValrepLandimpLotValuation> getLotValuationsByUid(long recordUniqueId);
}
