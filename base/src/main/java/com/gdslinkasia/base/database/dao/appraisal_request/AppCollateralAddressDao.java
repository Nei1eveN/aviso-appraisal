package com.gdslinkasia.base.database.dao.appraisal_request;

import androidx.room.Dao;
import androidx.room.Insert;
import androidx.room.Query;
import androidx.room.Update;

import com.gdslinkasia.base.model.details.AppCollateralAddress;

import java.util.List;

import io.reactivex.Single;

import static androidx.room.OnConflictStrategy.REPLACE;
import static com.gdslinkasia.base.database.DatabaseUtils.APPRAISAL_REQUEST_ID;
import static com.gdslinkasia.base.database.DatabaseUtils.APP_COLLATERAL_ADDRESS_TABLE;
import static com.gdslinkasia.base.database.DatabaseUtils.UNIQUE_ID;

@Dao
public abstract class AppCollateralAddressDao {

    @Query("SELECT * FROM "+APP_COLLATERAL_ADDRESS_TABLE+" WHERE "+APPRAISAL_REQUEST_ID+" =:appraisalRequestId")
    public abstract Single<List<AppCollateralAddress>> getCollateralAddressesByAppraisalRequestUId(long appraisalRequestId);

    @Query("SELECT * FROM " + APP_COLLATERAL_ADDRESS_TABLE + " WHERE " + UNIQUE_ID + " =:uniqueId")
    public abstract Single<AppCollateralAddress> getCollateralAddress(long uniqueId);

    @Update(onConflict = REPLACE)
    public abstract void updateCollateralAddress(AppCollateralAddress address);

    @Insert(onConflict = REPLACE)
    public abstract long[] insertCollateralAddress(List<AppCollateralAddress> address);

}
