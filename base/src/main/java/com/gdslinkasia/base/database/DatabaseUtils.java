package com.gdslinkasia.base.database;

public class DatabaseUtils {

    final static String DATABASE_NAME = "aviso-appraisal";
    final static String JOB_DETAILS_TABLE_NAME = "JobDetails";

    public final static String RECORD_PREFERENCE_TABLE = "RecordIDPreference";

    public final static String RECORD_TABLE = "Record";
    public final static String ATTACHMENT_TABLE = "AppAttachment";
    public final static String APP_COLLATERAL_ADDRESS_TABLE = "AppCollateralAddress";
    public final static String APPR_REQ_TABLE = "AppraisalRequest";
    public final static String SYSTEM_TABLE = "JobSystem";

    //TODO Var of Appraisal Types
    public static String VAR_LAND_IMP = "Land with Improvement";
    public static String VAR_LAND_ONLY = "Land Only";
    public static String VAR_CONDO = "Condominium";

    public final static String APPRAISER_USERNAME = "appraiser_username";

    public final static String APPRAISAL_TYPE = "app_request_appraisal";

    public final static String APPRAISAL_REQUEST_ID = "appraisal_req_id";
    public final static String COLL_ADDRESS_ID = "coll_address_id";

    public final static String RECORD_UID = "record_uid";
    public final static String LANDIMP_LOT_VALUATION_ID = "lot_valuation_id";

    public final static String OWNERSHIP_PROPERTY_DESC_ID = "ownership_property_desc_id";

    public final static String LANDIMP_DESC_IMP_ID = "desc_imp_id";
    public final static String LANDIMP_OTHER_DESC_IMP_ID = "other_desc_imp_id";

    public final static String ATTACHMENT_CREATED_FROM = "created_from";

    //TODO JobDetails Columns / Table Fields
    public final static String SYSTEM_RECORD_ID = "system_record_id";
    public final static String RECORD_ID = "record_id";
    public final static String REQUESTED_DAY = "app_daterequested_day";
    public final static String REQUESTED_MONTH = "app_daterequested_month";
    public final static String REQUESTED_YEAR = "app_daterequested_year";
    public final static String ACCT_COMPANY = "app_account_company";
    public final static String ACCT_NAME_TITLE = "app_account_name_title";
    public final static String ACCT_FIRST_NAME = "app_account_first_name";
    public final static String ACCT_LAST_NAME = "app_account_last_name";
    public final static String ACCT_MIDDLE_NAME = "app_account_middle_name";
    public final static String ACCT_AUTHORIZING_OFFICER = "app_account_authroizing_officer";
    public final static String ACCT_DESIGNATION = "app_account_designation";
    public final static String ACCT_BUSINESS_ADDR = "app_account_business_address";
    public final static String ACCT_BUSINESS_CONTACT = "app_account_business_contact";
    public final static String ACCT_BUSINESS_EMAIL = "app_account_business_email";
    public final static String APP_DESIRED_LOAN_AMT = "app_desired_loan_amount";

    public final static String APPLICATION_STATUS = "application_status";

    public final static String CONTRACT_DAY = "app_date_contract_day";
    public final static String CONTRACT_MONTH = "app_date_contract_month";
    public final static String CONTRACT_YEAR = "app_date_contract_year";

    public final static String INT_USERS_NAME = "app_intended_users_name";
    public final static String INT_USERS_OTHERS = "app_intended_users_others";
    public final static String VALUER_NAME = "app_valuer_name";
    public final static String VALUER_OFFICER_TYPE = "app_valuer_officer_type";
    public final static String VALUER_CONTACT_NO = "app_valuer_contact_number";
    public final static String VALUER_EMAIL_ADDR = "app_valuer_email_address";

    public final static String REWORK_REASON = "app_declined_reason";

    public final static String CONTROL_NO = "app_control_no";
    public final static String NATURE_APPRAISAL = "app_nature_appraisal";
    public final static String PRIORITY = "app_priority";
    public final static String PURPOSE_APPRAISAL = "app_purpose_appraisal";
    public final static String PURPOSE_APPRAISAL_OTHER = "app_purpose_appraisal_other";

    //TODO AppContactPerson Columns
    public final static String CONTACT_PERSON_INDEX = "app_contact_person_index";
    public final static String CONTACT_PERSON_NAME = "app_contact_person_name";
    public final static String CONTACT_PERSON_TYPE = "app_contact_person_type";
    public final static String CONTACT_TYPE = "app_contact_type";
    public final static String MOBILE_NO = "app_mobile_no";
    public final static String MOBILE_PREFIX = "app_mobile_prefix";
    public final static String TELEPHONE_AREA = "app_telephone_area";
    public final static String TELEPHONE_NO = "app_telephone_no";

    //TODO Collateral Address Columns
    public final static String ADDR_TYPE = "app_address_type";
    public final static String BLDG = "app_bldg";
    public final static String BLOCK_NO = "app_block_no";
    public final static String BRGY = "app_brgy";
    public final static String CITY = "app_city";
    public final static String COUNTRY = "app_country";
    public final static String DISTRICT = "app_district";
    public final static String LATITUDE = "app_latitude";
    public final static String LONGITUDE = "app_longitude";
    public final static String LOCATION_LAT = "app_location_latitude";
    public final static String LOCATION_LONG = "app_location_longitude";
    public final static String LOT_NO = "app_lot_no";
    public final static String PROVINCE = "app_province";
    public final static String REGION = "app_region";
    public final static String STREET_NAME = "app_street_name";
    public final static String STREET_NO = "app_street_no";
    public final static String TCT_NO = "app_tct_no";
    public final static String UNIT_NO = "app_unit_no";
    public final static String VILLAGE = "app_village";
    public final static String ZIP = "app_zip";

    public final static String UNIQUE_ID = "unique_id";

//    public final static String USER_ID = "userId";
//    public final static String TITLE = "title";
//    public final static String BODY = "body";


    //TODO FOR CONTINUATION
}
