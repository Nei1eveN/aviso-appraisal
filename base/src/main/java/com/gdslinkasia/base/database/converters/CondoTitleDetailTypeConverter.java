package com.gdslinkasia.base.database.converters;

import androidx.room.TypeConverter;

import com.gdslinkasia.base.model.details.CondoTitleDetail;
import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;

import java.lang.reflect.Type;
import java.util.Collections;
import java.util.List;

public class CondoTitleDetailTypeConverter {

    private Gson gson = new Gson();
    private Type type = new TypeToken<List<CondoTitleDetail>>() {}.getType();

    @TypeConverter
    public List<CondoTitleDetail> stringToData(String jsonToStringValue) {
        if (jsonToStringValue == null) {
            return Collections.emptyList();
        }

        return gson.fromJson(jsonToStringValue, type);
    }

    @TypeConverter
    public String dataToString(List<CondoTitleDetail> value) {
        return gson.toJson(value, type);
    }

}
