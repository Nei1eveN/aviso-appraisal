package com.gdslinkasia.base.database.dao;

import androidx.room.Dao;
import androidx.room.Insert;
import androidx.room.Query;
import androidx.room.Transaction;
import androidx.room.Update;

import com.gdslinkasia.base.model.details.AppraisalRequest;
import com.gdslinkasia.base.model.details.JobSystem;
import com.gdslinkasia.base.model.job.JobRecordResult;
import com.gdslinkasia.base.model.job.Record;

import java.util.List;

import io.reactivex.Single;

import static androidx.room.OnConflictStrategy.REPLACE;
import static com.gdslinkasia.base.database.DatabaseUtils.APPLICATION_STATUS;
import static com.gdslinkasia.base.database.DatabaseUtils.APPRAISAL_TYPE;
import static com.gdslinkasia.base.database.DatabaseUtils.APPRAISER_USERNAME;
import static com.gdslinkasia.base.database.DatabaseUtils.RECORD_ID;
import static com.gdslinkasia.base.database.DatabaseUtils.RECORD_TABLE;
import static com.gdslinkasia.base.database.DatabaseUtils.UNIQUE_ID;

@Dao
public interface JobsDao {

    @Transaction
    @Query("SELECT application_status, appAccountFirstName, appAccountMiddleName, appAccountLastName, appDaterequestedMonth, appDaterequestedDay, appDaterequestedYear, "+APPRAISAL_TYPE+", "+RECORD_ID+" FROM "+RECORD_TABLE+" WHERE "+APPRAISER_USERNAME + " = :appraiserUsername AND "+APPLICATION_STATUS+" = :jobStatus")
    Single<JobRecordResult> getJobsAccordingToStatus(String appraiserUsername, String jobStatus);

    @Transaction
    @Query("SELECT application_status, appAccountFirstName, appAccountMiddleName, appAccountLastName, appDaterequestedMonth, appDaterequestedDay, appDaterequestedYear, "+APPRAISAL_TYPE+", "+RECORD_ID+", "+UNIQUE_ID+" FROM "+RECORD_TABLE+" WHERE "+APPRAISER_USERNAME + " = :appraiserUsername AND "+APPLICATION_STATUS+" = :jobStatus")
    Single<List<Record>> getJobsByStatus(String appraiserUsername, String jobStatus);

    //TODO SAVE RECORDS
    @Insert (onConflict = REPLACE)
    void saveAllRecords(List<Record> record);

    @Insert(onConflict = REPLACE)
    void saveJobSystem(JobSystem system);

    @Update(onConflict = REPLACE)
    void updateAllRecords(List<Record> records);

    @Insert(onConflict = REPLACE)
    void insertRecord(Record record);

    @Insert(onConflict = REPLACE)
    void insertAppraisalRequests(List<AppraisalRequest> appraisalRequests);
}