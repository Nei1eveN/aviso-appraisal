package com.gdslinkasia.base.database.dao;

import androidx.room.Dao;
import androidx.room.Insert;
import androidx.room.Query;
import androidx.room.Update;

import com.gdslinkasia.base.model.details.ValrepLandimpOtherLandImpValue;

import java.util.List;

import io.reactivex.Flowable;
import io.reactivex.Single;

import static androidx.room.OnConflictStrategy.REPLACE;
import static com.gdslinkasia.base.database.DatabaseUtils.LANDIMP_OTHER_DESC_IMP_ID;
import static com.gdslinkasia.base.database.DatabaseUtils.RECORD_ID;
import static com.gdslinkasia.base.database.DatabaseUtils.UNIQUE_ID;

@Dao
public interface LandImpOtherValuationDao {

    @Query("SELECT * FROM " + "ValrepLandimpOtherLandImpValue" + " WHERE " + RECORD_ID + " =:recordId")
    Single<List<ValrepLandimpOtherLandImpValue>> getOtherLandImpValuesByRecordId(String recordId);

    @Query("SELECT * FROM " + "ValrepLandimpOtherLandImpValue" + " WHERE " + RECORD_ID + " =:recordId")
    Flowable<List<ValrepLandimpOtherLandImpValue>> getListFlowable(String recordId);

    @Query("SELECT * FROM " + "ValrepLandimpOtherLandImpValue" + " WHERE " + UNIQUE_ID + " =:uniqueId")
    Single<ValrepLandimpOtherLandImpValue> getDetails(long uniqueId);

    @Query("SELECT * FROM " + "ValrepLandimpOtherLandImpValue" + " WHERE " + LANDIMP_OTHER_DESC_IMP_ID + " =:otherDescImpId")
    Single<ValrepLandimpOtherLandImpValue> getOtherValueDetails(long otherDescImpId);

    @Update
    void updateLotDetail(ValrepLandimpOtherLandImpValue value);

    @Insert(onConflict = REPLACE)
    void insertOtherLandImpValue(ValrepLandimpOtherLandImpValue value);

    @Insert(onConflict = REPLACE)
    void insertOtherLandImpValues(List<ValrepLandimpOtherLandImpValue> otherLandImpValues);
}
