package com.gdslinkasia.base.database.dao;

import androidx.room.Dao;
import androidx.room.Insert;
import androidx.room.Query;
import androidx.room.Update;

import com.gdslinkasia.base.model.details.ValrepLandimpOtherLandImpDetail;

import java.util.List;

import io.reactivex.Flowable;
import io.reactivex.Single;

import static androidx.room.OnConflictStrategy.REPLACE;
import static com.gdslinkasia.base.database.DatabaseUtils.RECORD_ID;
import static com.gdslinkasia.base.database.DatabaseUtils.UNIQUE_ID;

@Dao
public interface LandImpOtherDetailDao {

    @Query("SELECT * FROM "+"ValrepLandimpOtherLandImpDetail"+" WHERE "+RECORD_ID+" =:recordId")
    Single<List<ValrepLandimpOtherLandImpDetail>> getOtherLandImpDetailsByRecordId(String recordId);

    @Query("SELECT * FROM "+"ValrepLandimpOtherLandImpDetail"+" WHERE "+RECORD_ID+" =:recordId")
    Flowable<List<ValrepLandimpOtherLandImpDetail>> getListFlowable(String recordId);

    @Query("SELECT * FROM "+"ValrepLandimpOtherLandImpDetail"+" WHERE "+UNIQUE_ID+" =:uniqueId")
    Single<ValrepLandimpOtherLandImpDetail> getDetails(long uniqueId);

    @Update
    void updateLotDetail(ValrepLandimpOtherLandImpDetail detail);

    @Insert(onConflict = REPLACE)
    long insertOtherLandImpDetail(ValrepLandimpOtherLandImpDetail detail);

    @Insert(onConflict = REPLACE)
    void insertOtherLandImpDetails(List<ValrepLandimpOtherLandImpDetail> otherLandImpDetails);

    @Query("DELETE FROM ValrepLandimpOtherLandImpDetail")
    void deleteValrepLandimpOtherLandImpDetails();
}
