package com.gdslinkasia.base.database.repository;

import android.app.Application;

import com.gdslinkasia.base.database.ApplicationDatabase;
import com.gdslinkasia.base.database.dao.JobsDao;
import com.gdslinkasia.base.model.details.JobSystem;
import com.gdslinkasia.base.model.job.JobRecordResult;
import com.gdslinkasia.base.model.job.Record;

import java.util.List;

import io.reactivex.Single;

public class JobsLocalRepository {
    private JobsDao jobsDao;

    public JobsLocalRepository(Application application) {
        jobsDao = ApplicationDatabase.getInstance(application).jobsDao();
    }

    //TODO SAVE JOB SYSTEM [1ST]
    public void saveSystem(JobSystem system) {
        jobsDao.saveJobSystem(system);
    }

    //TODO SAVE RECORD OBJECTS [2ND]
    public void saveAllJobs(List<Record> records) {
        jobsDao.saveAllRecords(records);
    }

    public Single<JobRecordResult> getJobsAccordingToStatus(String APPRAISER_USERNAME, String JOB_STATUS) {
        return jobsDao.getJobsAccordingToStatus(APPRAISER_USERNAME, JOB_STATUS);
    }

    public Single<List<Record>> getJobsByStatus(String APPRAISER_USERNAME, String JOB_STATUS) {
        return jobsDao.getJobsByStatus(APPRAISER_USERNAME, JOB_STATUS);
    }

    public void insertRecord(Record record) {
        jobsDao.insertRecord(record);
    }

//    public List<Record> getJobsByStatus(String APPRAISER_USERNAME, String JOB_STATUS) {
//        return jobsDao.getJobsByStatus(APPRAISER_USERNAME, JOB_STATUS);
//    }
}
