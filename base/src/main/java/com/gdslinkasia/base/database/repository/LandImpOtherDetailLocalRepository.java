package com.gdslinkasia.base.database.repository;

import android.app.Application;


import com.gdslinkasia.base.database.ApplicationDatabase;
import com.gdslinkasia.base.database.dao.LandImpOtherDetailDao;
import com.gdslinkasia.base.database.dao.LandImpOtherValuationDao;
import com.gdslinkasia.base.model.details.ValrepLandimpOtherLandImpDetail;
import com.gdslinkasia.base.model.details.ValrepLandimpOtherLandImpValue;

import java.util.List;

import io.reactivex.Flowable;
import io.reactivex.Single;

public class LandImpOtherDetailLocalRepository {
    private LandImpOtherDetailDao landImpDetailDao;
    private LandImpOtherValuationDao otherValuationDao;

    public LandImpOtherDetailLocalRepository(Application application) {
        landImpDetailDao = ApplicationDatabase.getInstance(application).landImpOtherDetailDao();
        otherValuationDao = ApplicationDatabase.getInstance(application).landImpOtherValuationDao();
    }

    public Single<List<ValrepLandimpOtherLandImpDetail>> getLandImpImpDetailsList(String recordId) {
        return landImpDetailDao.getOtherLandImpDetailsByRecordId(recordId);
    }

    public Flowable<List<ValrepLandimpOtherLandImpDetail>> getListFlowable(String recordId) {
        return landImpDetailDao.getListFlowable(recordId);
    }

    public Single<ValrepLandimpOtherLandImpDetail> getDetails(long uniqueId) {
        return landImpDetailDao.getDetails(uniqueId);
    }

    public void updateLotDetail(ValrepLandimpOtherLandImpDetail detail) {
        landImpDetailDao.updateLotDetail(detail);
    }

    public long insertOtherLandimpImpDetail(ValrepLandimpOtherLandImpDetail detail) {
        return landImpDetailDao.insertOtherLandImpDetail(detail);
    }

    public void insertOtherLandimpValue(ValrepLandimpOtherLandImpValue value) {
        otherValuationDao.insertOtherLandImpValue(value);
    }

    public Single<ValrepLandimpOtherLandImpValue> getOtherLandImpValue(long otherDescImpId) {
        return otherValuationDao.getOtherValueDetails(otherDescImpId);
    }

    public void updateOtherValue(ValrepLandimpOtherLandImpValue value) {
        otherValuationDao.updateLotDetail(value);
    }
}
