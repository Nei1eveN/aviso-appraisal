package com.gdslinkasia.base.database.repository;

import android.app.Application;

import com.gdslinkasia.base.database.ApplicationDatabase;
import com.gdslinkasia.base.database.dao.LandImpValuationDao;
import com.gdslinkasia.base.model.details.ValrepLandimpImpValuation;

import java.util.List;

import io.reactivex.Flowable;
import io.reactivex.Single;

public class LandImpValuationLocalRepository {
    private LandImpValuationDao landImpDetailDao;

    public LandImpValuationLocalRepository(Application application) {
        landImpDetailDao = ApplicationDatabase.getInstance(application).landImpValuationDao();
    }

    public Single<List<ValrepLandimpImpValuation>> getDetailsList(String recordId) {
        return landImpDetailDao.getLandimpValuationsByRecordId(recordId);
    }

    public Flowable<List<ValrepLandimpImpValuation>> getListFlowable(String recordId) {
        return landImpDetailDao.getListFlowable(recordId);
    }

    public Single<ValrepLandimpImpValuation> getDetails(long uniqueId) {
        return landImpDetailDao.getDetails(uniqueId);
    }

    public void updateLotDetail(ValrepLandimpImpValuation detail) {
        landImpDetailDao.updateLandimpImpValuation(detail);
    }
}
