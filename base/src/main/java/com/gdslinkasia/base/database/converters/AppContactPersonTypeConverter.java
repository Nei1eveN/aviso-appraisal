package com.gdslinkasia.base.database.converters;

import androidx.room.TypeConverter;

import com.gdslinkasia.base.model.details.AppContactPerson;
import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;

import java.lang.reflect.Type;
import java.util.List;

public class AppContactPersonTypeConverter {

    private Gson gson = new Gson();
    private Type type = new TypeToken<List<AppContactPerson>>(){}.getType();

    @TypeConverter
    public List<AppContactPerson> stringToData(String jsonToStringValue) {
        return gson.fromJson(jsonToStringValue, type);
    }

    @TypeConverter
    public String systemToString(List<AppContactPerson> value) {
        return gson.toJson(value, type);
    }

}
