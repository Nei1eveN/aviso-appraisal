package com.gdslinkasia.base.database.dao;

import androidx.room.Dao;
import androidx.room.Insert;
import androidx.room.Query;

import com.gdslinkasia.base.model.RecordIDPreference;

import io.reactivex.Single;

import static androidx.room.OnConflictStrategy.REPLACE;
import static com.gdslinkasia.base.database.DatabaseUtils.RECORD_ID;
import static com.gdslinkasia.base.database.DatabaseUtils.RECORD_PREFERENCE_TABLE;

@Dao
public abstract class RecordIDPreferenceDao {

    @Query("SELECT * FROM " + RECORD_PREFERENCE_TABLE + " WHERE " + RECORD_ID + " = :recordId")
    public abstract Single<RecordIDPreference> getRecordByPreference(String recordId);

    @Insert(onConflict = REPLACE)
    public abstract void insertRecordPreference(RecordIDPreference recordIDPreference);


}
