package com.gdslinkasia.base.database.repository;

import android.app.Application;
import android.util.Log;

import com.gdslinkasia.base.database.ApplicationDatabase;
import com.gdslinkasia.base.database.dao.*;
import com.gdslinkasia.base.database.dao.appraisal_request.*;
import com.gdslinkasia.base.database.dao.property_description.PropertyDescriptionDao;
import com.gdslinkasia.base.model.RecordIDPreference;
import com.gdslinkasia.base.model.details.*;
import com.gdslinkasia.base.model.job.Record;

import java.util.List;

import io.reactivex.Flowable;
import io.reactivex.Single;
import io.reactivex.schedulers.Schedulers;

public class JobDetailsLocalRepository {

    private RecordDao recordDao;
    private RecordIDPreferenceDao idPreferenceDao;
    private AppraisalRequestDao appraisalRequestDao;
    private AttachmentsDao attachmentsDao;
    private AppCollateralAddressDao collateralAddressDao;
    private AppContactPersonDao contactPersonDao;
    private CondoTitleDetailDao condoTitleDetailDao;
    private LandimpDetailDao landimpDetailDao;
    private LandimpTitleDetailDao landimpTitleDetailDao;
    private SpecialAssumptionsDao assumptionRemarksDao;
    private ValrepLandimpLotValuationDao valrepLandimpLotValuationDao;
    private QualifyingStatementDao qualifyingStatementDao;
    private LandImpDetailDao valrepLandimpImpDetailDao;
    private LandImpValuationDao landImpValuationDao;

    private LandImpOtherDetailDao valrepLandimpOtherLandImpDetailDao;

    private LandimpLotValuationDetailDao valrepLandimpLotValuationDetailDao;

    //TODO LAND ONLY / LAND WITH IMPROVEMENT PROPERTY DESCRIPTION / CONDO UNIT DESCRIPTION
    private LandimpLotDetailDao valrepLandimpLotDetailDao;
    private PropertyDescriptionDao propertyDescriptionDao;

    private LandImpOtherValuationDao landImpOtherValuationDao;

    private CondoParkingValuationDao valrepLandimpParkingValuationDetailDao;

    public JobDetailsLocalRepository(Application application) {
        this.recordDao = ApplicationDatabase.getInstance(application).recordDao();
        this.idPreferenceDao = ApplicationDatabase.getInstance(application).idPreferenceDao();
        this.appraisalRequestDao = ApplicationDatabase.getInstance(application).appraisalRequestDao();
        this.attachmentsDao = ApplicationDatabase.getInstance(application).attachmentsDao();
        this.collateralAddressDao = ApplicationDatabase.getInstance(application).collateralAddressDao();
        this.contactPersonDao = ApplicationDatabase.getInstance(application).contactPersonDao();
        this.condoTitleDetailDao = ApplicationDatabase.getInstance(application).condoTitleDetailDao();
        this.landimpDetailDao = ApplicationDatabase.getInstance(application).landimpDetailDao();
        this.landimpTitleDetailDao = ApplicationDatabase.getInstance(application).landimpTitleDetailDao();
        this.assumptionRemarksDao = ApplicationDatabase.getInstance(application).specialAssumptionsDao();
        this.valrepLandimpLotValuationDao = ApplicationDatabase.getInstance(application).landimpLotValuationDao();
        this.qualifyingStatementDao = ApplicationDatabase.getInstance(application).qualifyingStatementDao();
        this.valrepLandimpImpDetailDao = ApplicationDatabase.getInstance(application).landImpDetailDao();
        this.landImpValuationDao = ApplicationDatabase.getInstance(application).landImpValuationDao();

        this.valrepLandimpOtherLandImpDetailDao = ApplicationDatabase.getInstance(application).landImpOtherDetailDao();

        this.valrepLandimpLotValuationDetailDao = ApplicationDatabase.getInstance(application).landimpLotValuationDetailDao();

        this.valrepLandimpLotDetailDao = ApplicationDatabase.getInstance(application).landimpLotDetailDao();
        this.propertyDescriptionDao = ApplicationDatabase.getInstance(application).descriptionDao();

        this.landImpOtherValuationDao = ApplicationDatabase.getInstance(application).landImpOtherValuationDao();

        this.valrepLandimpParkingValuationDetailDao = ApplicationDatabase.getInstance(application).condoParkingValuationDao();
    }

    //TODO GET RECORD_ID PREFERENCE
    public Single<RecordIDPreference> getRecordIdByPreference(String recordId) {
        return idPreferenceDao.getRecordByPreference(recordId);
    }

    public void insertRecordPreference(RecordIDPreference recordIDPreference) {
        idPreferenceDao.insertRecordPreference(recordIDPreference);
    }

    public Single<Record> getRecordById(String recordId) {
        return recordDao.getRecordById(recordId);
    }

    public Flowable<Record> getFlowableRecord(String recordId) {
        return recordDao.getFlowableRecordById(recordId);
    }

    public void updateRecord(Record record) {
        recordDao.updateRecord(record);
    }

    //TODO COLLATERAL ADDRESS
    public Single<AppCollateralAddress> getCollateralAddress(long uniqueId) {
        return collateralAddressDao.getCollateralAddress(uniqueId);
    }

    public void updateCollateralAddress(AppCollateralAddress address) {
        collateralAddressDao.updateCollateralAddress(address);
    }

    public long insertAppraisalRequest(AppraisalRequest appraisalRequest) {
        return appraisalRequestDao.insertAppraisalRequest(appraisalRequest);
    }

    public void insertAppAttachment(AppAttachment appAttachment) {
        attachmentsDao.insertAppAttachment(appAttachment);
    }

    public void insertContactPerson(AppContactPerson contactPerson) {
        contactPersonDao.insertContactPerson(contactPerson);
    }

    public long[] insertCollateralAddress(List<AppCollateralAddress> address) {
        return contactPersonDao.insertCollateralAddress(address);
    }

    public void insertCondoTitleDetail(CondoTitleDetail detail) {
        condoTitleDetailDao.insertCondoTitleDetail(detail);
    }

    public void insertLandimpImpDetail(LandimpImpDetail detail) {
        landimpDetailDao.insertLandimpImpDetail(detail);
    }

    public void insertLandimpTitleDetail(LandimpTitleDetail detail) {
        landimpTitleDetailDao.insertLandimpTitleDetail(detail);
    }

    public void updateAppraisalRequests(List<AppraisalRequest> appraisalRequests) {
        appraisalRequestDao.updateAppraisalRequests(appraisalRequests);
    }

    public Single<List<AppraisalRequest>> getAppraisalRequestByRecordUId(long recordUId) {
        return Single.zip(appraisalRequestDao.getAppraisalRequestsListByRecordUId(recordUId),
                recordDao.getRecordByUId(recordUId), (appraisalRequests, record) -> {
                    record.setAppraisalRequest(appraisalRequests);
                    recordDao.updateRecord(record);
                    for (AppraisalRequest appraisalRequest : appraisalRequests) {
                        Single.zip(
                                attachmentsDao.getAppAttachmentsByAppraisalRequestId(appraisalRequest.getUniqueId()),
                                collateralAddressDao.getCollateralAddressesByAppraisalRequestUId(appraisalRequest.getUniqueId()),
                                contactPersonDao.getAppContactsByAppraisalRequestUId(appraisalRequest.getUniqueId()),
                                condoTitleDetailDao.getCondoTitleDetailsByAppraisalRequestId(appraisalRequest.getUniqueId()),
                                landimpDetailDao.getLandimpImpDetailsByAppraisalRequestId(appraisalRequest.getUniqueId()),
                                landimpTitleDetailDao.getLandimpTitleDetailsByAppraisalRequestId(appraisalRequest.getUniqueId()),
                                appraisalRequestDao.getAppraisalRequestByRecordUId(appraisalRequest.getRecordUId()),
                                (appAttachments, collateralAddresses, contactPersonList, condoTitleDetails, landimpImpDetails, landimpTitleDetails, appraisalRequest1) -> {
                                    Log.d("int--AttachmentSize", String.valueOf(appAttachments.size()));
                                    appraisalRequest1.setAppAttachment(appAttachments);
                                    Log.d("int--AddressSize", String.valueOf(collateralAddresses.size()));
                                    appraisalRequest1.setAppCollateralAddress(collateralAddresses);
                                    Log.d("int--ContactSize", String.valueOf(contactPersonList.size()));
                                    appraisalRequest1.setAppContactPerson(contactPersonList);
                                    Log.d("int--condoSize", String.valueOf(condoTitleDetails.size()));
                                    appraisalRequest1.setCondoTitleDetails(condoTitleDetails);
                                    Log.d("int--landimpImpDetSize", String.valueOf(landimpImpDetails.size()));
                                    appraisalRequest1.setLandimpImpDetails(landimpImpDetails);
                                    Log.d("int--landimpTitleSize", String.valueOf(landimpTitleDetails.size()));
                                    appraisalRequest1.setLandimpTitleDetails(landimpTitleDetails);
                                    appraisalRequests.add(appraisalRequest1);
                                    return appraisalRequest1;
                                })
                                .subscribeOn(Schedulers.io())
                                .subscribe();

                    }
                    record.setAppraisalRequest(appraisalRequests);
                    recordDao.updateRecord(record);
                    appraisalRequestDao.updateAppraisalRequests(appraisalRequests);
                    return appraisalRequests;
                });
    }

    public void insertAssumptionsRemarks(List<ValrepLandimpAssumptionsRemark> assumptionsRemarks) {
        assumptionRemarksDao.insertAssumptionsRemarks(assumptionsRemarks);
    }

    public void insertValrepLandimpImpDetails(List<ValrepLandimpImpDetail> valrepLandimpImpDetails) {
        valrepLandimpImpDetailDao.insertValrepLandimpImpDetails(valrepLandimpImpDetails);
    }

    public void insertLandimpImpValuations(List<ValrepLandimpImpValuation> landimpImpValuations) {
        landImpValuationDao.insertLandimpImpValuations(landimpImpValuations);
    }

    public void insertLandimpLotDetails(List<ValrepLandimpLotDetail> landimpLotDetails) {
        valrepLandimpLotDetailDao.insertLandimpLotDetails(landimpLotDetails);
    }

    public void insertLotValuationDetails(List<ValrepLandimpLotValuationDetail> lotValuationDetails) {
        valrepLandimpLotValuationDetailDao.insertLotValuationDetails(lotValuationDetails);
    }

    public void insertLotValuations(List<ValrepLandimpLotValuation> lotValuations) {
        valrepLandimpLotValuationDao.insertLotValuations(lotValuations);
    }

    public void insertOtherLandImpDetails(List<ValrepLandimpOtherLandImpDetail> otherLandImpDetails) {
        valrepLandimpOtherLandImpDetailDao.insertOtherLandImpDetails(otherLandImpDetails);
    }

    public void insertOtherLandImpValues(List<ValrepLandimpOtherLandImpValue> otherLandImpValues) {
        landImpOtherValuationDao.insertOtherLandImpValues(otherLandImpValues);
    }

    public void insertParkingValuationDetails(List<ValrepLandimpParkingValuationDetail> parkingValuationDetails) {
        valrepLandimpParkingValuationDetailDao.insertParkingValuationDetails(parkingValuationDetails);
    }

    public void insertQualifyingStatements(List<ValrepLcIvsQualifyingStatement> qualifyingStatements) {
        qualifyingStatementDao.insertQualifyingStatements(qualifyingStatements);
    }

    public Single<List<ValrepLandimpAssumptionsRemark>> getAssumptionsRemarksByRecordId(String recordId) {
        return assumptionRemarksDao.getAssumptionsRemarksByRecordId(recordId);
    }

    public Single<List<ValrepLandimpImpDetail>> getValrepLandimpImpDetails(String recordId) {
        return valrepLandimpImpDetailDao.getValrepLandimpDetailsByRecordId(recordId);
    }

    public Single<List<ValrepLandimpImpValuation>> getLandimpImpValuations(String recordId) {
        return landImpValuationDao.getLandimpValuationsByRecordId(recordId);
    }

    public Single<List<ValrepLandimpLotDetail>> getValrepLandimpLotDetails(String recordId) {
        return valrepLandimpLotDetailDao.getLandimpLotDetailsByRecordId(recordId);
    }

    public Single<List<ValrepLandimpLotValuation>> getLotValuations(String recordId) {
        return valrepLandimpLotValuationDao.getLotValuationsByRecordId(recordId);
    }

    public Single<List<ValrepLandimpOtherLandImpDetail>> getValrepLandimpOtherLandImpDetails(String recordId) {
        return valrepLandimpOtherLandImpDetailDao.getOtherLandImpDetailsByRecordId(recordId);
    }

    public Single<List<ValrepLandimpOtherLandImpValue>> getValrepLandimpOtherLandImpValue(String recordId) {
        return landImpOtherValuationDao.getOtherLandImpValuesByRecordId(recordId);
    }

    public Single<List<ValrepLandimpParkingValuationDetail>> getValrepLandimpParkingValuationDetails(String recordId) {
        return valrepLandimpParkingValuationDetailDao.getDetailsList(recordId);
    }

    public Single<List<ValrepLcIvsQualifyingStatement>> getValrepLcIvsQualifyingStatements(String recordId) {
        return qualifyingStatementDao.getQualifyingStatementsByRecordId(recordId);
    }

    public Single<List<AppraisalRequest>> getAppraisalRequestListByRecordUId(long uniqueId) {
        return appraisalRequestDao.getAppraisalRequestsListByRecordUId(uniqueId);
    }

    public long insertLandimpLotValuation(ValrepLandimpLotValuation valuation) {
        return valrepLandimpLotValuationDao.insertLandimpLotValuation(valuation);
    }

    public long insertValrepLandimpImpDetail(ValrepLandimpImpDetail detail) {
        return valrepLandimpImpDetailDao.insertValrepLandimpImpDetail(detail);
    }

    public long insertOtherLandImpDetail(ValrepLandimpOtherLandImpDetail detail) {
        return valrepLandimpOtherLandImpDetailDao.insertOtherLandImpDetail(detail);
    }

    public Single<List<ValrepLandimpLotValuationDetail>> getLotValuationDetails(String recordId) {
        return valrepLandimpLotValuationDetailDao.getLandimpLotValuationDetails(recordId);
    }

    public Single<List<AppraisalRequest>> getAppraisalRequestByRecordId(String recordId) {
        return appraisalRequestDao.getAppraisalRequestsListByRecordId(recordId);
    }

    public Single<List<AppAttachment>> getAttachmentsByAppReqId(long appraisalRequestId, String createdFrom) {
        return attachmentsDao.getAttachmentsDetailsList(appraisalRequestId, createdFrom);
    }

    public void deleteAllAttachments(List<AppAttachment> appAttachments) {
        attachmentsDao.deleteAllAttachments(appAttachments);
    }

    public void onDestroy() {
        ApplicationDatabase.destroyInstance();
    }

    public Single<RecordWithAppraisalRequest> getRecordWithAppraisalRequest(long recordUid) {
        return recordDao.getRecordWithAppraisalRequest(recordUid);
    }

    public Flowable<AppraisalRequestWithChildObjects> getAppraisalRequestChildObjects(long appraisalRequestId) {
        return appraisalRequestDao.getAppraisalRequestChildObject(appraisalRequestId);
    }

    public void insertAssumptionRemark(ValrepLandimpAssumptionsRemark remark) {
        assumptionRemarksDao.insertAssumptionRemark(remark);
    }

    //TODO DELETE FUNCTIONS
    public void deleteAssumptionRemarks() {
        assumptionRemarksDao.deleteAssumptionRemarks();
    }

    public void deleteQualifyingStatements() {
        qualifyingStatementDao.deleteQualifyingStatements();
    }

    public void deleteValrepLandimpLotDetails() {
        valrepLandimpLotDetailDao.deleteLandimpLotDetails();
    }

    public void deleteValrepLandimpLotValuationDetails() {
        valrepLandimpLotValuationDetailDao.deleteLandimpLotValuationDetails();
    }

    public void deleteLandimpLotValuations() {
        valrepLandimpLotValuationDao.deleteLandimpLotValuations();
    }

    public void deleteValrepLandimpOtherLandImpDetails() {
        valrepLandimpOtherLandImpDetailDao.deleteValrepLandimpOtherLandImpDetails();
    }

    public void deleteValrepLandimpImpDetails() {
        valrepLandimpImpDetailDao.deleteValrepLandimpImpDetails();
    }

    public void deleteValrepLandimpParkingValuationDetails() {
        valrepLandimpParkingValuationDetailDao.deleteValrepLandimpParkingValuationDetails();
    }

    public void deleteAppraisalRequests() {
        appraisalRequestDao.deleteAppraisalRequests();
    }

    public long insertValrepLandimpLotDetail(ValrepLandimpLotDetail detail) {
        return propertyDescriptionDao.insertOwnershipPropertyDesc(detail);
    }
}
