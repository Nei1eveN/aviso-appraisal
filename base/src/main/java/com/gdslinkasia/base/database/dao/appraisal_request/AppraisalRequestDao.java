package com.gdslinkasia.base.database.dao.appraisal_request;

import androidx.room.Dao;
import androidx.room.Insert;
import androidx.room.Query;
import androidx.room.Transaction;
import androidx.room.Update;

import com.gdslinkasia.base.database.dao.AppraisalRequestWithChildObjects;
import com.gdslinkasia.base.model.details.AppraisalRequest;

import java.util.List;

import io.reactivex.Flowable;
import io.reactivex.Single;

import static androidx.room.OnConflictStrategy.REPLACE;
import static com.gdslinkasia.base.database.DatabaseUtils.RECORD_ID;
import static com.gdslinkasia.base.database.DatabaseUtils.RECORD_UID;
import static com.gdslinkasia.base.database.DatabaseUtils.UNIQUE_ID;

@Dao
public abstract class AppraisalRequestDao {

    @Transaction
    @Query("SELECT * FROM "+"AppraisalRequest"+" WHERE "+UNIQUE_ID+" =:appraisalRequestId")
    public abstract Flowable<AppraisalRequestWithChildObjects> getAppraisalRequestChildObject(long appraisalRequestId);

    @Query("SELECT * FROM " +"AppraisalRequest"+" WHERE "+RECORD_ID+" =:recordId")
    public abstract Single<AppraisalRequest> getAppraisalRequestByRecordId(String recordId);

    @Query("SELECT * FROM "+"AppraisalRequest"+" WHERE "+RECORD_ID+" =:recordId")
    public abstract Single<List<AppraisalRequest>> getAppraisalRequestsListByRecordId(String recordId);

    @Query("SELECT * FROM "+"AppraisalRequest"+" WHERE "+RECORD_UID+" =:recordUId")
    public abstract Single<List<AppraisalRequest>> getAppraisalRequestsListByRecordUId(long recordUId);

    @Query("SELECT * FROM "+"AppraisalRequest"+" WHERE "+RECORD_UID+" =:recordUId")
    public abstract Single<AppraisalRequest> getAppraisalRequestByRecordUId(long recordUId);

    @Update
    public abstract void updateAppraisalRequest(AppraisalRequest appraisalRequest);

    @Update
    public abstract void updateAppraisalRequests(List<AppraisalRequest> appraisalRequests);

    @Insert(onConflict = REPLACE)
    public abstract long insertAppraisalRequest(AppraisalRequest appraisalRequest);

    @Query("DELETE FROM AppraisalRequest")
    public abstract void deleteAppraisalRequests();
}
