package com.gdslinkasia.base.database.converters;

import androidx.room.TypeConverter;

import com.gdslinkasia.base.model.details.AppraisalRequest;
import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;

import java.lang.reflect.Type;
import java.util.Collections;
import java.util.List;

public class AppraisalRequestTypeConverter {

    private Gson gson = new Gson();
    private Type type = new TypeToken<List<AppraisalRequest>>() {}.getType();

    @TypeConverter
    public List<AppraisalRequest> stringToAppraisalRequest(String jsonToStringValue) {
        if (jsonToStringValue == null) {
            return Collections.emptyList();
        }

        return gson.fromJson(jsonToStringValue, type);
    }

    @TypeConverter
    public String appraisalRequestToString(List<AppraisalRequest> value) {
        return gson.toJson(value, type);
    }
}
