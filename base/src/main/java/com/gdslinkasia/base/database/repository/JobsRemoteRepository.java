package com.gdslinkasia.base.database.repository;

import android.app.Application;
import android.util.Log;

import com.gdslinkasia.base.networking.api.RetrofitClient;
import com.gdslinkasia.base.networking.api.RetrofitService;
import com.gdslinkasia.base.model.details.AppraisalRequest;
import com.gdslinkasia.base.model.details.JobSystem;
import com.gdslinkasia.base.model.job.JobRecordResult;
import com.gdslinkasia.base.model.job.Record;

import org.json.JSONException;

import java.util.ArrayList;
import java.util.List;

import io.reactivex.Single;
import io.reactivex.functions.Consumer;
import io.reactivex.schedulers.Schedulers;

import static com.gdslinkasia.base.utils.GlobalFunctions.JOB_ACCEPTANCE_JSON_BUILDER;
import static com.gdslinkasia.base.utils.GlobalFunctions.JOB_REWORK_JSON_BUILDER;
import static com.gdslinkasia.base.utils.GlobalString.FOR_REWORK;
import static com.gdslinkasia.base.utils.GlobalString.auth_token_value;
import static com.gdslinkasia.base.utils.GlobalString.server_url;


public class JobsRemoteRepository {

    private JobsLocalRepository localRepository;

    public JobsRemoteRepository(Application application) {
        localRepository = new JobsLocalRepository(application);
    }

    public Single<JobRecordResult> getRxOnlineJobs(String APPRAISER_USERNAME, String JOB_STATUS) throws JSONException {

        if (JOB_STATUS.equals(FOR_REWORK)) {
            Log.d("int--ReworkStatus", "Logic Triggered for " + JOB_STATUS);
            return RetrofitClient.getApiClient(server_url)
                    .create(RetrofitService.class)
                    .getJobs(auth_token_value, String.valueOf(JOB_REWORK_JSON_BUILDER(APPRAISER_USERNAME, JOB_STATUS)), "0")
                    .doOnSuccess(getConsumer())
                    .subscribeOn(Schedulers.io());
        } else {
            Log.d("int--AcceptedOrActive", "Logic Triggered for " + JOB_STATUS);
            return RetrofitClient.getApiClient(server_url)
                    .create(RetrofitService.class)
                    .getJobs(auth_token_value, String.valueOf(JOB_ACCEPTANCE_JSON_BUILDER(APPRAISER_USERNAME, JOB_STATUS)), "0")
                    .doOnSuccess(getConsumer())
                    .subscribeOn(Schedulers.io());
        }
    }

    private Consumer<JobRecordResult> getConsumer() {
        return jobRecordResult -> {

            List<Record> records = new ArrayList<>();
//            List<AppraisalRequest> appraisalRequests = new ArrayList<>();
//            List<ValrepLandimpAssumptionsRemark> assumptionsRemarks = new ArrayList<>();
//            List<ValrepLandimpImpDetail> valrepLandimpImpDetails = new ArrayList<>();
//            List<ValrepLandimpImpValuation> landimpImpValuations = new ArrayList<>();
//            List<ValrepLandimpLotDetail> landimpLotDetails = new ArrayList<>();
//            List<ValrepLandimpLotValuation> lotValuations = new ArrayList<>();
//            List<ValrepLandimpOtherLandImpDetail> otherLandImpDetails = new ArrayList<>();
//            List<ValrepLandimpOtherLandImpValue> otherLandImpValues = new ArrayList<>();
//            List<ValrepLandimpParkingValuationDetail> parkingValuationDetails = new ArrayList<>();
//            List<ValrepLcIvsQualifyingStatement> qualifyingStatements = new ArrayList<>();

//            List<AppAttachment> appAttachments = new ArrayList<>();
//            List<AppCollateralAddress> collateralAddresses = new ArrayList<>();
//            List<AppContactPerson> contactPersonList = new ArrayList<>();
//            List<CondoTitleDetail> condoTitleDetails = new ArrayList<>();
//            List<LandimpImpDetail> landimpImpDetails = new ArrayList<>();
//            List<LandimpTitleDetail> landimpTitleDetails = new ArrayList<>();
//            List<ValrepLandimpLotValuationDetail> lotValuationDetails = new ArrayList<>();

            //TODO [RECORD] SET RECORD_ID
            for (Record record : jobRecordResult.getRecords()) {

                JobSystem system = record.getJobSystem();
                record.setRecordId(system.getRecordId());

                //TODO [RECORD OBJECT] SET APPRAISER USERNAME
                //TODO [APPRAISAL REQUEST] SET RECORD_ID
                for (AppraisalRequest appraisalRequest : record.getAppraisalRequest()) {
                    appraisalRequest.setRecordId(system.getRecordId());

                    record.setAppraiserUsername(appraisalRequest.getAppraiserUsername());
                    record.setAppraisalType(appraisalRequest.getAppRequestAppraisal());


//                    if (!appraisalRequest.getAppAttachment().isEmpty()) {
//                        for (AppAttachment attachment : appraisalRequest.getAppAttachment()) {
//                            //TODO [APP ATTACHMENT] SET RECORD_ID FOR APP ATTACHMENT
////                            attachment.setRecordId(system.getRecordId());
//                            appAttachments.add(attachment);
//
//                            appraisalRequest.setAppAttachment(appAttachments);
//                        }
//                    }

//                    for (AppCollateralAddress address : appraisalRequest.getAppCollateralAddress()) {
//                        //TODO [APP COLLATERAL ADDRESS] SET RECORD_ID
////                        address.setRecordId(system.getRecordId());
//                        collateralAddresses.add(address);
//
//                        appraisalRequest.setAppCollateralAddress(collateralAddresses);
//                    }

//                    for (AppContactPerson contactPerson : appraisalRequest.getAppContactPerson()) {
//                        //TODO [APP CONTACT PERSON] SET RECORD_ID
////                        contactPerson.setRecordId(system.getRecordId());
//                        contactPersonList.add(contactPerson);
//
//                        appraisalRequest.setAppContactPerson(contactPersonList);
//                    }

                    //TODO [CONDO TITLE DETAIL] SET RECORD_ID
//                    for (CondoTitleDetail detail : appraisalRequest.getCondoTitleDetails()) {
////                        detail.setRecordId(system.getRecordId());
//                        condoTitleDetails.add(detail);
//
//                        appraisalRequest.setCondoTitleDetails(condoTitleDetails);
//                    }

                    //TODO [LANDIMP IMP DETAIL] SET RECORD_ID
//                    for (LandimpImpDetail detail : appraisalRequest.getLandimpImpDetails()) {
////                        detail.setRecordId(system.getRecordId());
//                        landimpImpDetails.add(detail);
//
//                        appraisalRequest.setLandimpImpDetails(landimpImpDetails);
//                    }

                    //TODO [LANDIMP TITLE DETAIL] SET RECORD_ID
//                    for (LandimpTitleDetail detail : appraisalRequest.getLandimpTitleDetails()) {
////                        detail.setRecordId(system.getRecordId());
//                        landimpTitleDetails.add(detail);
//
//                        appraisalRequest.setLandimpTitleDetails(landimpTitleDetails);
//                    }

//                    appraisalRequests.add(appraisalRequest);
                }

//                //TODO [VALREP LANDIMP ASSUMPTIONS REMARKS] SET RECORD_ID
//                for (ValrepLandimpAssumptionsRemark remark : record.getValrepLandimpAssumptionsRemarks()) {
//                    remark.setRecordId(system.getRecordId());
//
//                    assumptionsRemarks.add(remark);
//                }
//
//                //TODO [ValrepLandimpImpDetail] SET RECORD_ID
//                for (ValrepLandimpImpDetail detail : record.getValrepLandimpImpDetails()) {
//                    detail.setRecordId(system.getRecordId());
//
//                    valrepLandimpImpDetails.add(detail);
//                }
//
//                //TODO [ValrepLandimpImpValuation] SET RECORD_ID
//                for (ValrepLandimpImpValuation valuation : record.getValrepLandimpImpValuation()) {
//                    valuation.setRecordId(system.getRecordId());
//
//                    landimpImpValuations.add(valuation);
//                }
//
//                //TODO [ValrepLandimpLotDetail] SET RECORD_ID
//                for (ValrepLandimpLotDetail detail : record.getValrepLandimpLotDetails()) {
//                    detail.setRecordId(system.getRecordId());
//
//                    landimpLotDetails.add(detail);
//                }
//
//                //TODO [ValrepLandimpLotValuation] SET RECORD_ID
//                for (ValrepLandimpLotValuation valuation : record.getValrepLandimpLotValuation()) {
//                    valuation.setRecordId(system.getRecordId());
//
//
//                    for (ValrepLandimpLotValuationDetail detail : valuation.getValrepLandimpLotValuationDetails()) {
//                        detail.setRecordId(system.getRecordId());
//
//                        lotValuationDetails.add(detail);
//                    }
//
//                    valuation.setValrepLandimpLotValuationDetails(lotValuationDetails);
//
//                    lotValuations.add(valuation);
//                }
//
//                //TODO [ValrepLandimpOtherLandImpDetail] SET RECORD_ID
//                for (ValrepLandimpOtherLandImpDetail detail : record.getValrepLandimpOtherLandImpDetails()) {
//                    detail.setRecordId(system.getRecordId());
//
//                    otherLandImpDetails.add(detail);
//                }
//
//                //TODO [ValrepLandimpOtherLandImpValue] SET RECORD_ID
//                for (ValrepLandimpOtherLandImpValue otherLandImpValue : record.getValrepLandimpOtherLandImpValue()) {
//                    otherLandImpValue.setRecordId(system.getRecordId());
//
//                    otherLandImpValues.add(otherLandImpValue);
//                }
//
//                //TODO [ValrepLandimpParkingValuationDetail] SET RECORD_ID
//                for (ValrepLandimpParkingValuationDetail parkingValuationDetail : record.getValrepLandimpParkingValuationDetails()) {
//                    parkingValuationDetail.setRecordId(system.getRecordId());
//
//                    parkingValuationDetails.add(parkingValuationDetail);
//                }
//
//                //TODO [ValrepLcIvsQualifyingStatement] SET RECORD_ID
//                for (ValrepLcIvsQualifyingStatement qualifyingStatement : record.getValrepLcIvsQualifyingStatements()) {
//                    qualifyingStatement.setRecordId(system.getRecordId());
//
//                    qualifyingStatements.add(qualifyingStatement);
//                }

                localRepository.saveSystem(system);

//                record.setAppraisalRequest(appraisalRequests);
//                record.setValrepLandimpAssumptionsRemarks(assumptionsRemarks);
//                record.setValrepLandimpImpDetails(valrepLandimpImpDetails);
//                record.setValrepLandimpImpValuation(landimpImpValuations);
//                record.setValrepLandimpLotDetails(landimpLotDetails);
//                record.setValrepLandimpLotValuation(lotValuations);
//                record.setValrepLandimpOtherLandImpDetails(otherLandImpDetails);
//                record.setValrepLandimpOtherLandImpValue(otherLandImpValues);
//                record.setValrepLandimpParkingValuationDetails(parkingValuationDetails);
//                record.setValrepLcIvsQualifyingStatements(qualifyingStatements);
//                record.setJobSystem(system);


                records.add(record);


                Log.d("int--RecCount", String.valueOf(records.size()));

                localRepository.insertRecord(record);

                Log.d("int--applicationStatus", record.getApplicationStatus());
            }

//            jobRecordResult.setRecords(records);

            Log.d("int--savingRecords", "Logic Triggered");
            Log.d("int--getRxRemoteJobs", "end point reached");
        };
    }

}
