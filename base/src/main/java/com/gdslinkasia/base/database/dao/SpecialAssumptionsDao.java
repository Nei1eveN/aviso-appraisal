package com.gdslinkasia.base.database.dao;

import androidx.room.Dao;
import androidx.room.Delete;
import androidx.room.Insert;
import androidx.room.Query;
import androidx.room.Update;

import com.gdslinkasia.base.model.details.ValrepLandimpAssumptionsRemark;

import java.util.List;

import io.reactivex.Flowable;
import io.reactivex.Single;

import static androidx.room.OnConflictStrategy.REPLACE;
import static com.gdslinkasia.base.database.DatabaseUtils.RECORD_ID;
import static com.gdslinkasia.base.database.DatabaseUtils.UNIQUE_ID;

@Dao
public interface SpecialAssumptionsDao {

    @Query("SELECT * FROM "+"ValrepLandimpAssumptionsRemark"+" WHERE "+RECORD_ID+" =:recordId")
    Single<List<ValrepLandimpAssumptionsRemark>> getAssumptionsRemarksByRecordId(String recordId);

    @Query("SELECT * FROM "+"ValrepLandimpAssumptionsRemark"+" WHERE "+RECORD_ID+" =:recordId")
    Flowable<List<ValrepLandimpAssumptionsRemark>> getListFlowable(String recordId);

    @Query("SELECT * FROM "+"ValrepLandimpAssumptionsRemark"+" WHERE "+UNIQUE_ID+" =:uniqueId")
    Single<ValrepLandimpAssumptionsRemark> getPropertyDescDetail(long uniqueId);

    //TODO INSERT
    @Insert(onConflict = REPLACE)
    void insertAssumptionRemark(ValrepLandimpAssumptionsRemark detail);

    @Insert(onConflict = REPLACE)
    void insertAssumptionsRemarks(List<ValrepLandimpAssumptionsRemark> assumptionsRemarks);

    //TODO UPDATE
    @Update
    void updateAssumptionRemark(ValrepLandimpAssumptionsRemark detail);

    //TODO DELETE
    //TODO DELETE ITEM BY QUERYING UNIQUE ID
    @Query("DELETE FROM "+"ValrepLandimpAssumptionsRemark"+" WHERE "+UNIQUE_ID+" =:uniqueId")
    void deleteLotItem(long uniqueId);

    @Query("DELETE FROM ValrepLandimpAssumptionsRemark")
    void deleteAssumptionRemarks();
}
