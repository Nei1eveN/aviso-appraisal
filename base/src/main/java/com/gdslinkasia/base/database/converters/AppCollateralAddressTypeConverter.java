package com.gdslinkasia.base.database.converters;

import androidx.room.TypeConverter;

import com.gdslinkasia.base.model.details.AppCollateralAddress;
import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;

import java.lang.reflect.Type;
import java.util.List;

public class AppCollateralAddressTypeConverter {

    private Gson gson = new Gson();
    private Type type = new TypeToken<List<AppCollateralAddress>>(){}.getType();

    @TypeConverter
    public List<AppCollateralAddress> stringToData(String jsonToStringValue) {
        return gson.fromJson(jsonToStringValue, type);
    }

    @TypeConverter
    public String systemToString(List<AppCollateralAddress> value) {
        return gson.toJson(value, type);
    }

}
