package com.gdslinkasia.base.database.converters;

import androidx.room.TypeConverter;

import com.gdslinkasia.base.model.details.AppAttachment;
import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;

import java.lang.reflect.Type;
import java.util.Collections;
import java.util.List;

public class AppAttachmentTypeConverter {
    private Gson gson = new Gson();
    private Type type = new TypeToken<List<AppAttachment>>() {}.getType();

    @TypeConverter
    public List<AppAttachment> stringToAppAttachment(String jsonToStringValue) {
        if (jsonToStringValue == null) {
            return Collections.emptyList();
        }
        return gson.fromJson(jsonToStringValue, type);
    }

    @TypeConverter
    public String appAttachmentToString(List<AppAttachment> value) {
        return gson.toJson(value, type);
    }
}
