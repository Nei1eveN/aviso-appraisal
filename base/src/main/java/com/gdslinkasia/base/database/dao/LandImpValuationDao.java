package com.gdslinkasia.base.database.dao;

import androidx.room.Dao;
import androidx.room.Insert;
import androidx.room.Query;
import androidx.room.Update;

import com.gdslinkasia.base.model.details.ValrepLandimpImpValuation;

import java.util.List;

import io.reactivex.Flowable;
import io.reactivex.Single;

import static androidx.room.OnConflictStrategy.REPLACE;
import static com.gdslinkasia.base.database.DatabaseUtils.LANDIMP_DESC_IMP_ID;
import static com.gdslinkasia.base.database.DatabaseUtils.RECORD_ID;
import static com.gdslinkasia.base.database.DatabaseUtils.UNIQUE_ID;

@Dao
public interface LandImpValuationDao {

    @Query("SELECT * FROM "+"ValrepLandimpImpValuation"+" WHERE "+RECORD_ID+" =:recordId")
    Single<List<ValrepLandimpImpValuation>> getLandimpValuationsByRecordId(String recordId);

    @Query("SELECT * FROM "+"ValrepLandimpImpValuation"+" WHERE "+RECORD_ID+" =:recordId")
    Flowable<List<ValrepLandimpImpValuation>> getListFlowable(String recordId);

    @Query("SELECT * FROM "+"ValrepLandimpImpValuation"+" WHERE "+UNIQUE_ID+" =:uniqueId")
    Single<ValrepLandimpImpValuation> getDetails(long uniqueId);

    @Query("SELECT * FROM "+"ValrepLandimpImpValuation"+" WHERE "+LANDIMP_DESC_IMP_ID+" =:descId")
    Single<ValrepLandimpImpValuation> getDetailsByDescId(long descId);

    @Update
    void updateLandimpImpValuation(ValrepLandimpImpValuation detail);

    @Insert(onConflict = REPLACE)
    void insertValrepLandimpImpValuation(ValrepLandimpImpValuation valuation);

    @Insert(onConflict = REPLACE)
    void insertLandimpImpValuations(List<ValrepLandimpImpValuation> landimpImpValuations);
}
