package com.gdslinkasia.base.database.dao.appraisal_request;

import androidx.room.Dao;
import androidx.room.Insert;
import androidx.room.Query;

import com.gdslinkasia.base.model.details.CondoTitleDetail;

import java.util.List;

import io.reactivex.Single;

import static androidx.room.OnConflictStrategy.REPLACE;
import static com.gdslinkasia.base.database.DatabaseUtils.APPRAISAL_REQUEST_ID;

@Dao
public abstract class CondoTitleDetailDao {

    @Insert(onConflict = REPLACE)
    public abstract void insertCondoTitleDetail(CondoTitleDetail detail);

    @Query("SELECT * FROM "+"CondoTitleDetail"+" WHERE "+APPRAISAL_REQUEST_ID+" =:appraisalRequestId")
    public abstract Single<List<CondoTitleDetail>> getCondoTitleDetailsByAppraisalRequestId(long appraisalRequestId);


}
