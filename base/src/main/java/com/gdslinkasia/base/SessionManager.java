package com.gdslinkasia.base;

import android.content.Context;
import android.content.SharedPreferences;
import android.util.Log;

import com.gdslinkasia.base.utils.GlobalString;

import static com.gdslinkasia.base.database.DatabaseUtils.RECORD_ID;
import static com.gdslinkasia.base.utils.GlobalString.KEY_IS_LOGGED_IN;

public class SessionManager {
    // LogCat tag
    private static String TAG = SessionManager.class.getSimpleName();

    // Shared Preferences
    private SharedPreferences pref;

    private SharedPreferences.Editor editor;

    public SessionManager(Context _context) {
        pref = _context.getSharedPreferences(GlobalString.PREF_NAME, GlobalString.PRIVATE_MODE);
        editor = pref.edit();
    }

    public void setLogin(boolean isLoggedIn, String email) {
        editor.putBoolean(KEY_IS_LOGGED_IN, isLoggedIn);
        editor.putString(GlobalString.email, email);
        // commit changes
        editor.commit();

        Log.d(TAG, "User login session modified.");
    }

    public String getUserEmail() {
        return pref.getString(GlobalString.email, "");
    }

    public boolean isLoggedIn(){
        return pref.getBoolean(KEY_IS_LOGGED_IN, false);
    }

    public void setLoadCount(int loadCount, String recordId, boolean hasLoadedOnce) {
        editor.putInt("load_count", loadCount);
        editor.putString(RECORD_ID, recordId);
        editor.putBoolean("has_loaded_once", hasLoadedOnce);

        editor.commit();

        Log.d(TAG, "Loading count for "+recordId+" has been modified.");
    }

    public int getLoadCount() {
        return pref.getInt("load_count", 1);
    }

    public boolean hasNotLoadedOnce() {
        return pref.getBoolean("has_loaded_once", false);
    }

    public String getLoadedRecordId() {
        return pref.getString(RECORD_ID, "");
    }

    //Logout user
    public void LogOut() {
        editor.clear();
        editor.commit();
    }
}
