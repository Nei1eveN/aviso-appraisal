package com.gdslinkasia.base;

public interface BasePresenter<T> {

    void takeView(T view);

    void dropView();

}
