package com.gdslinkasia.base.utils.computation;

import android.text.Editable;
import android.text.TextWatcher;
import android.widget.AutoCompleteTextView;

import java.math.BigDecimal;

import static com.gdslinkasia.base.utils.GlobalFunctions.nullcheckInt;
import static com.gdslinkasia.base.utils.GlobalString.cleanFormat;

public class PriceAdjustmentComputation {

    public static TextWatcher PROPERTY_CONVEYED_VALUE_1(
            String netValSqm,
            AutoCompleteTextView acValue1,
            AutoCompleteTextView acValue2,
            AutoCompleteTextView acValue3,
            AutoCompleteTextView acNetPriceAdjustment) {
        return new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {

            }

            @Override
            public void afterTextChanged(Editable s) {
                acValue1.removeTextChangedListener(this);
                acValue1.setSelection(acValue1.getText().length());
                acValue1.addTextChangedListener(this);

                double cleanNetValSqm = Double.valueOf(cleanFormat(nullcheckInt(netValSqm)));
                double cleanPropRightValue = Double.valueOf(cleanFormat(nullcheckInt(acValue1.getText().toString())));
                double cleanFinalPropRight = cleanNetValSqm + (cleanNetValSqm * (cleanPropRightValue / 100));

                double cleanQuotedPeriodValue = Double.valueOf(cleanFormat(nullcheckInt(acValue2.getText().toString())));

                double cleanFinalQuotedPeriod = cleanFinalPropRight + (cleanFinalPropRight * (cleanQuotedPeriodValue / 100));

                double cleanArbitrageValue = Double.valueOf(cleanFormat(nullcheckInt(acValue3.getText().toString())));

                double cleanFinalArbitrageValue = cleanFinalQuotedPeriod + (cleanFinalQuotedPeriod * (cleanArbitrageValue / 100));

                double cleanNetAdjPrice = cleanFinalArbitrageValue + (cleanNetValSqm * (cleanPropRightValue / 100));

                acNetPriceAdjustment.setText(String.valueOf(Math.round(Double.valueOf(new BigDecimal(cleanNetAdjPrice).stripTrailingZeros().toPlainString()))));
            }
        };
    }

    public static TextWatcher QUOTED_PERIOD_VALUE_2(
            String netValSqm,
            AutoCompleteTextView acValue1,
            AutoCompleteTextView acValue2,
            AutoCompleteTextView acValue3,
            AutoCompleteTextView acNetPriceAdjustment) {
        return new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {

            }

            @Override
            public void afterTextChanged(Editable s) {
                acValue2.removeTextChangedListener(this);
                acValue2.setSelection(acValue2.getText().length());
                acValue2.addTextChangedListener(this);

                double cleanNetValSqm = Double.valueOf(cleanFormat(nullcheckInt(netValSqm)));
                double cleanPropRightValue = Double.valueOf(cleanFormat(nullcheckInt(acValue1.getText().toString())));
                double cleanFinalPropRight = cleanNetValSqm + (cleanNetValSqm * (cleanPropRightValue / 100));

                double cleanQuotedPeriodValue = Double.valueOf(cleanFormat(nullcheckInt(acValue2.getText().toString())));

                double cleanFinalQuotedPeriod = cleanFinalPropRight + (cleanFinalPropRight * (cleanQuotedPeriodValue / 100));

                double cleanArbitrageValue = Double.valueOf(cleanFormat(nullcheckInt(acValue3.getText().toString())));

                double cleanFinalArbitrageValue = cleanFinalQuotedPeriod + (cleanFinalQuotedPeriod * (cleanArbitrageValue / 100));

                double cleanNetAdjPrice = cleanFinalArbitrageValue + (cleanNetValSqm * (cleanPropRightValue / 100));

                acNetPriceAdjustment.setText(String.valueOf(Math.round(Double.valueOf(new BigDecimal(cleanNetAdjPrice).stripTrailingZeros().toPlainString()))));
            }
        };
    }

    public static TextWatcher ARBITRAGE_VALUE_3(
            String netValSqm,
            AutoCompleteTextView acValue1,
            AutoCompleteTextView acValue2,
            AutoCompleteTextView acValue3,
            AutoCompleteTextView acNetPriceAdjustment) {
        return new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {

            }

            @Override
            public void afterTextChanged(Editable s) {
                acValue3.removeTextChangedListener(this);
                acValue3.setSelection(acValue3.getText().length());
                acValue3.addTextChangedListener(this);

                double cleanNetValSqm = Double.valueOf(cleanFormat(nullcheckInt(netValSqm)));
                double cleanPropRightValue = Double.valueOf(cleanFormat(nullcheckInt(acValue1.getText().toString())));
                double cleanFinalPropRight = cleanNetValSqm + (cleanNetValSqm * (cleanPropRightValue / 100));

                double cleanQuotedPeriodValue = Double.valueOf(cleanFormat(nullcheckInt(acValue2.getText().toString())));

                double cleanFinalQuotedPeriod = cleanFinalPropRight + (cleanFinalPropRight * (cleanQuotedPeriodValue / 100));

                double cleanArbitrageValue = Double.valueOf(cleanFormat(nullcheckInt(acValue3.getText().toString())));

                double cleanFinalArbitrageValue = cleanFinalQuotedPeriod + (cleanFinalQuotedPeriod * (cleanArbitrageValue / 100));

                double cleanNetAdjPrice = cleanFinalArbitrageValue + (cleanNetValSqm * (cleanPropRightValue / 100));

                acNetPriceAdjustment.setText(String.valueOf(Math.round(Double.valueOf(new BigDecimal(cleanNetAdjPrice).stripTrailingZeros().toPlainString()))));
            }
        };
    }

}
