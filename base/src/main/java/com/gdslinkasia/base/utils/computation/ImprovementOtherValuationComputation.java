package com.gdslinkasia.base.utils.computation;

import android.text.Editable;
import android.text.TextWatcher;
import android.widget.AutoCompleteTextView;

import static com.gdslinkasia.base.utils.GlobalFunctions.nullcheckInt;
import static com.gdslinkasia.base.utils.GlobalString.cleanFormat;
import static com.gdslinkasia.base.utils.GlobalString.getFormattedAmount;
import static com.gdslinkasia.base.utils.GlobalString.getFormattedAmountDouble;

public class ImprovementOtherValuationComputation {

    public static TextWatcher AC_UNIT_COST(AutoCompleteTextView acArea,
                                           AutoCompleteTextView acUnitCost,
                                           AutoCompleteTextView acEcoLife,
                                           AutoCompleteTextView acEffectiveAge,
                                           AutoCompleteTextView acRCN,
                                           AutoCompleteTextView acPercentage,
                                           AutoCompleteTextView acTotalDepreciation,
                                           AutoCompleteTextView acDepreciatedRCN) {
        return new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {

            }

            @Override
            public void afterTextChanged(Editable s) {
                acUnitCost.removeTextChangedListener(this);
                acUnitCost.setSelection(acUnitCost.getText().length());
                acUnitCost.addTextChangedListener(this);

                double cleanArea = Double.valueOf(cleanFormat(nullcheckInt(acArea.getText().toString())));
                double cleanUnitCost = Double.valueOf(cleanFormat(nullcheckInt(acUnitCost.getText().toString())));

                double cleanEconomicLife = Double.valueOf(cleanFormat(nullcheckInt(acEcoLife.getText().toString())));
                double cleanEffectiveAge = Double.valueOf(cleanFormat(nullcheckInt(acEffectiveAge.getText().toString())));

                double cleanRCN = cleanArea * cleanUnitCost;
                acRCN.setText(getFormattedAmountDouble(String.valueOf(cleanRCN)));

                double cleanPER = (cleanEffectiveAge / cleanEconomicLife) * 100;
                acPercentage.setText(getFormattedAmount(String.valueOf(cleanPER)));

                double cleanTotalDepreciation = cleanRCN * (cleanPER / 100);
                acTotalDepreciation.setText(getFormattedAmountDouble(String.valueOf(cleanTotalDepreciation)));

                double cleanDepreciatedRCN = cleanRCN - cleanTotalDepreciation;
                acDepreciatedRCN.setText(getFormattedAmountDouble(String.valueOf(cleanDepreciatedRCN)));
            }
        };
    }

}
