package com.gdslinkasia.base.utils.computation;

import android.annotation.SuppressLint;
import android.text.Editable;
import android.text.TextWatcher;

import com.google.android.material.textfield.TextInputEditText;

import java.math.BigDecimal;
import java.util.Objects;

import static com.gdslinkasia.base.utils.GlobalFunctions.nullcheckInt;
import static com.gdslinkasia.base.utils.GlobalString.cleanFormat;

public class MarketDataComputation {

    public static TextWatcher AC_LAND_AREA_SQM(
            TextInputEditText etLandAreaSqm,
            TextInputEditText etBuildingAreaSqm,
            TextInputEditText etAskingPriceSqm,
            TextInputEditText etTotalAskingPriceSqm,
            TextInputEditText etBuildingReplaceCostSqm,
            TextInputEditText etBuildingReplaceCost,
            TextInputEditText etBuildingDepreciationPercent,
            TextInputEditText etBuildingDepreciation,
            TextInputEditText etEstBuildingVal,
            TextInputEditText etNetBuildingVal,
            TextInputEditText etNetValSqm) {
        return new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {

            }

            @Override
            public void afterTextChanged(Editable s) {
                etLandAreaSqm.removeTextChangedListener(this);
                etLandAreaSqm.setSelection(Objects.requireNonNull(etLandAreaSqm.getText()).length());
                etLandAreaSqm.addTextChangedListener(this);

                double cleanLandAreaSqm = Double.valueOf(cleanFormat(nullcheckInt(Objects.requireNonNull(etLandAreaSqm.getText()).toString())));

                double cleanAskingPriceSqm = Double.valueOf(cleanFormat(nullcheckInt(Objects.requireNonNull(etAskingPriceSqm.getText()).toString())));

                double cleanTotalAskingPrice = cleanLandAreaSqm * cleanAskingPriceSqm;

                etTotalAskingPriceSqm.setText(String.valueOf(new BigDecimal(cleanTotalAskingPrice).stripTrailingZeros().toPlainString()));

                double cleanBuildingAreaSqm = Double.valueOf(cleanFormat(nullcheckInt(Objects.requireNonNull(etBuildingAreaSqm.getText()).toString())));

                double cleanBuildingReplaceCostSqm = Double.valueOf(cleanFormat(nullcheckInt(Objects.requireNonNull(etBuildingReplaceCostSqm.getText()).toString())));

                double cleanBuildingReplacementCost = cleanBuildingAreaSqm * cleanBuildingReplaceCostSqm;

                etBuildingReplaceCost.setText(String.valueOf(new BigDecimal(cleanBuildingReplacementCost).stripTrailingZeros().toPlainString()));

                double cleanBuildingDepreciationPercent = Double.valueOf(cleanFormat(nullcheckInt(Objects.requireNonNull(etBuildingDepreciationPercent.getText()).toString())));

                double cleanBuildingDepreciation = (Double.valueOf(cleanFormat(nullcheckInt(Objects.requireNonNull(etBuildingReplaceCost.getText()).toString()))) * (cleanBuildingDepreciationPercent)) / 100; // /100

                etBuildingDepreciation.setText(String.valueOf(new BigDecimal(cleanBuildingDepreciation).stripTrailingZeros().toPlainString()));

                double cleanEstimatedBuildingValue = Double.valueOf(cleanFormat(nullcheckInt(etBuildingReplaceCost.getText().toString()))) - Double.valueOf(cleanFormat(nullcheckInt(Objects.requireNonNull(etBuildingDepreciation.getText()).toString())));

                etEstBuildingVal.setText(String.valueOf(new BigDecimal(cleanEstimatedBuildingValue).stripTrailingZeros().toPlainString()));

                double cleanNetLandValue = cleanTotalAskingPrice - cleanEstimatedBuildingValue;

                etNetBuildingVal.setText(String.valueOf(new BigDecimal(cleanNetLandValue).stripTrailingZeros().toPlainString()));

                double cleanNetValueLandSqm = cleanNetLandValue / cleanLandAreaSqm;

                etNetValSqm.setText(String.valueOf(Math.round(cleanNetValueLandSqm)));
            }
        };
    }

    public static TextWatcher AC_BUILDING_AREA_SQM(
            TextInputEditText etLandAreaSqm,
            TextInputEditText etBuildingAreaSqm,
            TextInputEditText etAskingPriceSqm,
            TextInputEditText etTotalAskingPriceSqm,
            TextInputEditText etBuildingReplaceCostSqm,
            TextInputEditText etBuildingReplaceCost,
            TextInputEditText etBuildingDepreciationPercent,
            TextInputEditText etBuildingDepreciation,
            TextInputEditText etEstBuildingVal,
            TextInputEditText etNetBuildingVal,
            TextInputEditText etNetValSqm) {
        return new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {


            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {

            }

            @Override
            public void afterTextChanged(Editable s) {
                etBuildingAreaSqm.removeTextChangedListener(this);
                etBuildingAreaSqm.setSelection(Objects.requireNonNull(etBuildingAreaSqm.getText()).length());
                etBuildingAreaSqm.addTextChangedListener(this);

                double cleanLandAreaSqm = Double.valueOf(cleanFormat(nullcheckInt(Objects.requireNonNull(etLandAreaSqm.getText()).toString())));

                double cleanAskingPriceSqm = Double.valueOf(cleanFormat(nullcheckInt(Objects.requireNonNull(etAskingPriceSqm.getText()).toString())));

                double cleanTotalAskingPrice = cleanLandAreaSqm * cleanAskingPriceSqm;

                etTotalAskingPriceSqm.setText(String.valueOf(new BigDecimal(cleanTotalAskingPrice).stripTrailingZeros().toPlainString()));

                double cleanBuildingAreaSqm = Double.valueOf(cleanFormat(nullcheckInt(Objects.requireNonNull(etBuildingAreaSqm.getText()).toString())));

                double cleanBuildingReplaceCostSqm = Double.valueOf(cleanFormat(nullcheckInt(Objects.requireNonNull(etBuildingReplaceCostSqm.getText()).toString())));

                double cleanBuildingReplacementCost = cleanBuildingAreaSqm * cleanBuildingReplaceCostSqm;

                etBuildingReplaceCost.setText(String.valueOf(new BigDecimal(cleanBuildingReplacementCost).stripTrailingZeros().toPlainString()));

                double cleanBuildingDepreciationPercent = Double.valueOf(cleanFormat(nullcheckInt(Objects.requireNonNull(etBuildingDepreciationPercent.getText()).toString())));

                double cleanBuildingDepreciation = (Double.valueOf(cleanFormat(nullcheckInt(Objects.requireNonNull(etBuildingReplaceCost.getText()).toString()))) * (cleanBuildingDepreciationPercent)) / 100; // /100

                etBuildingDepreciation.setText(String.valueOf(new BigDecimal(cleanBuildingDepreciation).stripTrailingZeros().toPlainString()));

                double cleanEstimatedBuildingValue = Double.valueOf(cleanFormat(nullcheckInt(etBuildingReplaceCost.getText().toString()))) - Double.valueOf(cleanFormat(nullcheckInt(Objects.requireNonNull(etBuildingDepreciation.getText()).toString())));

                etEstBuildingVal.setText(String.valueOf(new BigDecimal(cleanEstimatedBuildingValue).stripTrailingZeros().toPlainString()));

                double cleanNetLandValue = cleanTotalAskingPrice - cleanEstimatedBuildingValue;

                etNetBuildingVal.setText(String.valueOf(new BigDecimal(cleanNetLandValue).stripTrailingZeros().toPlainString()));

                double cleanNetValueLandSqm = cleanNetLandValue / cleanLandAreaSqm;

                etNetValSqm.setText(String.valueOf(Math.round(cleanNetValueLandSqm)));
            }
        };
    }

    public static TextWatcher AC_ASKING_PRICE_SQM(
            TextInputEditText etLandAreaSqm,
            TextInputEditText etBuildingAreaSqm,
            TextInputEditText etAskingPriceSqm,
            TextInputEditText etTotalAskingPriceSqm,
            TextInputEditText etBuildingReplaceCostSqm,
            TextInputEditText etBuildingReplaceCost,
            TextInputEditText etBuildingDepreciationPercent,
            TextInputEditText etBuildingDepreciation,
            TextInputEditText etEstBuildingVal,
            TextInputEditText etNetBuildingVal,
            TextInputEditText etNetValSqm) {
        return new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {


            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {

            }

            @Override
            public void afterTextChanged(Editable s) {
                etAskingPriceSqm.removeTextChangedListener(this);
                etAskingPriceSqm.setSelection(Objects.requireNonNull(etAskingPriceSqm.getText()).length());
                etAskingPriceSqm.addTextChangedListener(this);

                double cleanLandAreaSqm = Double.valueOf(cleanFormat(nullcheckInt(Objects.requireNonNull(etLandAreaSqm.getText()).toString())));

                double cleanAskingPriceSqm = Double.valueOf(cleanFormat(nullcheckInt(Objects.requireNonNull(etAskingPriceSqm.getText()).toString())));

                double cleanTotalAskingPrice = cleanLandAreaSqm * cleanAskingPriceSqm;

                etTotalAskingPriceSqm.setText(String.valueOf(new BigDecimal(cleanTotalAskingPrice).stripTrailingZeros().toPlainString()));

                double cleanBuildingAreaSqm = Double.valueOf(cleanFormat(nullcheckInt(Objects.requireNonNull(etBuildingAreaSqm.getText()).toString())));

                double cleanBuildingReplaceCostSqm = Double.valueOf(cleanFormat(nullcheckInt(Objects.requireNonNull(etBuildingReplaceCostSqm.getText()).toString())));

                double cleanBuildingReplacementCost = cleanBuildingAreaSqm * cleanBuildingReplaceCostSqm;

                etBuildingReplaceCost.setText(String.valueOf(new BigDecimal(cleanBuildingReplacementCost).stripTrailingZeros().toPlainString()));

                double cleanBuildingDepreciationPercent = Double.valueOf(cleanFormat(nullcheckInt(Objects.requireNonNull(etBuildingDepreciationPercent.getText()).toString())));

                double cleanBuildingDepreciation = (Double.valueOf(cleanFormat(nullcheckInt(Objects.requireNonNull(etBuildingReplaceCost.getText()).toString()))) * (cleanBuildingDepreciationPercent)) / 100; // /100

                etBuildingDepreciation.setText(String.valueOf(new BigDecimal(cleanBuildingDepreciation).stripTrailingZeros().toPlainString()));

                double cleanEstimatedBuildingValue = Double.valueOf(cleanFormat(nullcheckInt(etBuildingReplaceCost.getText().toString()))) - Double.valueOf(cleanFormat(nullcheckInt(Objects.requireNonNull(etBuildingDepreciation.getText()).toString())));

                etEstBuildingVal.setText(String.valueOf(new BigDecimal(cleanEstimatedBuildingValue).stripTrailingZeros().toPlainString()));

                double cleanNetLandValue = cleanTotalAskingPrice - cleanEstimatedBuildingValue;

                etNetBuildingVal.setText(String.valueOf(new BigDecimal(cleanNetLandValue).stripTrailingZeros().toPlainString()));

                double cleanNetValueLandSqm = cleanNetLandValue / cleanLandAreaSqm;

                etNetValSqm.setText(String.valueOf(Math.round(cleanNetValueLandSqm)));
            }
        };
    }

    public static TextWatcher AC_BUILDING_REPLACE_COST_SQM(
            TextInputEditText etLandAreaSqm,
            TextInputEditText etBuildingAreaSqm,
            TextInputEditText etAskingPriceSqm,
            TextInputEditText etTotalAskingPriceSqm,
            TextInputEditText etBuildingReplaceCostSqm,
            TextInputEditText etBuildingReplaceCost,
            TextInputEditText etBuildingDepreciationPercent,
            TextInputEditText etBuildingDepreciation,
            TextInputEditText etEstBuildingVal,
            TextInputEditText etNetBuildingVal,
            TextInputEditText etNetValSqm) {
        return new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {


            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {

            }

            @Override
            public void afterTextChanged(Editable s) {
                etBuildingReplaceCostSqm.removeTextChangedListener(this);
                etBuildingReplaceCostSqm.setSelection(Objects.requireNonNull(etBuildingReplaceCostSqm.getText()).length());
                etBuildingReplaceCostSqm.addTextChangedListener(this);

                double cleanLandAreaSqm = Double.valueOf(cleanFormat(nullcheckInt(Objects.requireNonNull(etLandAreaSqm.getText()).toString())));

                double cleanAskingPriceSqm = Double.valueOf(cleanFormat(nullcheckInt(Objects.requireNonNull(etAskingPriceSqm.getText()).toString())));

                double cleanTotalAskingPrice = cleanLandAreaSqm * cleanAskingPriceSqm;

                etTotalAskingPriceSqm.setText(String.valueOf(new BigDecimal(cleanTotalAskingPrice).stripTrailingZeros().toPlainString()));

                double cleanBuildingAreaSqm = Double.valueOf(cleanFormat(nullcheckInt(Objects.requireNonNull(etBuildingAreaSqm.getText()).toString())));

                double cleanBuildingReplaceCostSqm = Double.valueOf(cleanFormat(nullcheckInt(etBuildingReplaceCostSqm.getText().toString())));

                double cleanBuildingReplacementCost = cleanBuildingAreaSqm * cleanBuildingReplaceCostSqm;

                etBuildingReplaceCost.setText(String.valueOf(new BigDecimal(cleanBuildingReplacementCost).stripTrailingZeros().toPlainString()));

                double cleanBuildingDepreciationPercent = Double.valueOf(cleanFormat(nullcheckInt(Objects.requireNonNull(etBuildingDepreciationPercent.getText()).toString())));

                double cleanBuildingDepreciation = (Double.valueOf(cleanFormat(nullcheckInt(Objects.requireNonNull(etBuildingReplaceCost.getText()).toString()))) * (cleanBuildingDepreciationPercent)) / 100; // /100

                etBuildingDepreciation.setText(String.valueOf(new BigDecimal(cleanBuildingDepreciation).stripTrailingZeros().toPlainString()));

                double cleanEstimatedBuildingValue = Double.valueOf(cleanFormat(nullcheckInt(etBuildingReplaceCost.getText().toString()))) - Double.valueOf(cleanFormat(nullcheckInt(Objects.requireNonNull(etBuildingDepreciation.getText()).toString())));

                etEstBuildingVal.setText(String.valueOf(new BigDecimal(cleanEstimatedBuildingValue).stripTrailingZeros().toPlainString()));

                double cleanNetLandValue = cleanTotalAskingPrice - cleanEstimatedBuildingValue;

                etNetBuildingVal.setText(String.valueOf(new BigDecimal(cleanNetLandValue).stripTrailingZeros().toPlainString()));

                double cleanNetValueLandSqm = cleanNetLandValue / cleanLandAreaSqm;

                etNetValSqm.setText(String.valueOf(Math.round(cleanNetValueLandSqm)));
            }
        };
    }

    public static TextWatcher AC_BUILDING_DEPRECIATION_PERCENT(
            TextInputEditText etLandAreaSqm,
            TextInputEditText etBuildingAreaSqm,
            TextInputEditText etAskingPriceSqm,
            TextInputEditText etTotalAskingPriceSqm,
            TextInputEditText etBuildingReplaceCostSqm,
            TextInputEditText etBuildingReplaceCost,
            TextInputEditText etBuildingDepreciationPercent,
            TextInputEditText etBuildingDepreciation,
            TextInputEditText etEstBuildingVal,
            TextInputEditText etNetBuildingVal,
            TextInputEditText etNetValSqm) {
        return new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {


            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {

            }

            @SuppressLint("SetTextI18n")
            @Override
            public void afterTextChanged(Editable s) {
                etBuildingDepreciationPercent.removeTextChangedListener(this);
                etBuildingDepreciationPercent.setSelection(Objects.requireNonNull(etBuildingDepreciationPercent.getText()).length());
                etBuildingDepreciationPercent.addTextChangedListener(this);

                double cleanLandAreaSqm = Double.valueOf(cleanFormat(nullcheckInt(Objects.requireNonNull(etLandAreaSqm.getText()).toString())));

                double cleanAskingPriceSqm = Double.valueOf(cleanFormat(nullcheckInt(Objects.requireNonNull(etAskingPriceSqm.getText()).toString())));

                double cleanTotalAskingPrice = cleanLandAreaSqm * cleanAskingPriceSqm;

                etTotalAskingPriceSqm.setText(new BigDecimal(cleanTotalAskingPrice).stripTrailingZeros().toPlainString());

                double cleanBuildingAreaSqm = Double.valueOf(cleanFormat(nullcheckInt(Objects.requireNonNull(etBuildingAreaSqm.getText()).toString())));

                double cleanBuildingReplaceCostSqm = Double.valueOf(cleanFormat(nullcheckInt(Objects.requireNonNull(etBuildingReplaceCostSqm.getText()).toString())));

                double cleanBuildingReplacementCost = cleanBuildingAreaSqm * cleanBuildingReplaceCostSqm;

                etBuildingReplaceCost.setText(new BigDecimal(cleanBuildingReplacementCost).stripTrailingZeros().toPlainString());

                double cleanBuildingDepreciationPercent = Double.valueOf(cleanFormat(nullcheckInt(etBuildingDepreciationPercent.getText().toString())));

                double cleanBuildingDepreciation = (Double.valueOf(cleanFormat(nullcheckInt(Objects.requireNonNull(etBuildingReplaceCost.getText()).toString()))) * (cleanBuildingDepreciationPercent)) / 100; // /100

                etBuildingDepreciation.setText(String.valueOf(new BigDecimal(cleanBuildingDepreciation).stripTrailingZeros().toPlainString()));

                double cleanEstimatedBuildingValue = Double.valueOf(cleanFormat(nullcheckInt(etBuildingReplaceCost.getText().toString()))) - Double.valueOf(cleanFormat(nullcheckInt(Objects.requireNonNull(etBuildingDepreciation.getText()).toString())));

                etEstBuildingVal.setText(String.valueOf(new BigDecimal(cleanEstimatedBuildingValue).stripTrailingZeros().toPlainString()));

                double cleanNetLandValue = Double.valueOf(cleanFormat(nullcheckInt(Objects.requireNonNull(etTotalAskingPriceSqm.getText()).toString()))) - cleanEstimatedBuildingValue;

                etNetBuildingVal.setText(String.valueOf(new BigDecimal(cleanNetLandValue).stripTrailingZeros().toPlainString())); //new BigDecimal().stripTrailingZeros().toPlainString()

                double cleanNetValueLandSqm = cleanNetLandValue / cleanLandAreaSqm;

                etNetValSqm.setText(String.valueOf(Math.round(cleanNetValueLandSqm)));
            }
        };
    }

}
