package com.gdslinkasia.base.utils.computation;

import android.text.Editable;
import android.text.TextWatcher;
import android.util.Log;
import android.widget.AutoCompleteTextView;

import static com.gdslinkasia.base.utils.GlobalFunctions.nullcheckInt;
import static com.gdslinkasia.base.utils.GlobalString.cleanFormat;
import static com.gdslinkasia.base.utils.GlobalString.getFormattedAmount;

public class ImprovementCostAppComputation {

    public static TextWatcher RCN_SQM(String effectiveAge,
                                      String economicLife,
                                      AutoCompleteTextView acFloorArea,
                                      AutoCompleteTextView acRcnSqm,
                                      AutoCompleteTextView acRcn,
                                      AutoCompleteTextView acPercent,
                                      AutoCompleteTextView acPhysical,
                                      AutoCompleteTextView acFunctional,
                                      AutoCompleteTextView acEconomic,
                                      AutoCompleteTextView acDepreciate) {
        return new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {

            }

            @Override
            public void afterTextChanged(Editable s) {
                acRcnSqm.removeTextChangedListener(this);
                acRcnSqm.setSelection(acRcnSqm.getText().length());
                acRcnSqm.addTextChangedListener(this);

                double cleanFloorArea = Double.valueOf(cleanFormat(nullcheckInt(acFloorArea.getText().toString())));

                double cleanRCNSQM = Double.valueOf(cleanFormat(nullcheckInt(acRcnSqm.getText().toString())));

                double cleanRCN = cleanFloorArea * cleanRCNSQM;

                acRcn.setText(getFormattedAmount(String.valueOf(cleanRCN)));

                double cleanEffectiveAge = Double.valueOf(cleanFormat(nullcheckInt(effectiveAge)));
                double cleanEconomicLife = Double.valueOf(cleanFormat(nullcheckInt(economicLife)));

//                double cleanIncurablePerc = (cleanEffectiveAge / cleanEconomicLife) * 100;
                double cleanIncurablePerc = Double.valueOf(cleanFormat(nullcheckInt(acPercent.getText().toString())));
                Log.d("int--cleanIncPer", String.valueOf(cleanIncurablePerc));
                double cleanIncurable = cleanRCN * ((cleanEffectiveAge / cleanEconomicLife) * 100 / 100);

                acPhysical.setText(getFormattedAmount(String.valueOf(cleanIncurable)));

                double cleanFunctionalObsolence = Double.valueOf(cleanFormat(nullcheckInt(acFunctional.getText().toString())));
                double cleanEconomicObsolence = Double.valueOf(cleanFormat(nullcheckInt(acEconomic.getText().toString())));

                double cleanDepreciatedRCN = cleanRCN - cleanIncurable - cleanFunctionalObsolence - cleanEconomicObsolence;

                acDepreciate.setText(getFormattedAmount(String.valueOf(cleanDepreciatedRCN)));
            }
        };
    }

    public static TextWatcher FUNCTIONAL_OBSOLENCE(String effectiveAge,
                                      String economicLife,
                                      AutoCompleteTextView acFloorArea,
                                      AutoCompleteTextView acRcnSqm,
                                      AutoCompleteTextView acRcn,
                                      AutoCompleteTextView acPercent,
                                      AutoCompleteTextView acPhysical,
                                      AutoCompleteTextView acFunctional,
                                      AutoCompleteTextView acEconomic,
                                      AutoCompleteTextView acDepreciate) {
        return new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {

            }

            @Override
            public void afterTextChanged(Editable s) {
                acFunctional.removeTextChangedListener(this);
                acFunctional.setSelection(acFunctional.getText().length());
                acFunctional.addTextChangedListener(this);

                double cleanFloorArea = Double.valueOf(cleanFormat(nullcheckInt(acFloorArea.getText().toString())));

                double cleanRCNSQM = Double.valueOf(cleanFormat(nullcheckInt(acRcnSqm.getText().toString())));

                double cleanRCN = cleanFloorArea * cleanRCNSQM;

                acRcn.setText(getFormattedAmount(String.valueOf(cleanRCN)));

                double cleanEffectiveAge = Double.valueOf(cleanFormat(nullcheckInt(effectiveAge)));
                double cleanEconomicLife = Double.valueOf(cleanFormat(nullcheckInt(economicLife)));

//                double cleanIncurablePerc = (cleanEffectiveAge / cleanEconomicLife) * 100;
                double cleanIncurablePerc = Double.valueOf(cleanFormat(nullcheckInt(acPercent.getText().toString())));
                Log.d("int--cleanIncPer", String.valueOf(cleanIncurablePerc));
                double cleanIncurable = cleanRCN * ((cleanEffectiveAge / cleanEconomicLife) * 100 / 100);

                acPhysical.setText(getFormattedAmount(String.valueOf(cleanIncurable)));

                double cleanFunctionalObsolence = Double.valueOf(cleanFormat(nullcheckInt(acFunctional.getText().toString())));
                double cleanEconomicObsolence = Double.valueOf(cleanFormat(nullcheckInt(acEconomic.getText().toString())));

                double cleanDepreciatedRCN = cleanRCN - cleanIncurable - cleanFunctionalObsolence - cleanEconomicObsolence;

                acDepreciate.setText(getFormattedAmount(String.valueOf(cleanDepreciatedRCN)));
            }
        };
    }

    public static TextWatcher ECONOMIC_OBSOLENCE(String effectiveAge,
                                                   String economicLife,
                                                   AutoCompleteTextView acFloorArea,
                                                   AutoCompleteTextView acRcnSqm,
                                                   AutoCompleteTextView acRcn,
                                                   AutoCompleteTextView acPercent,
                                                   AutoCompleteTextView acPhysical,
                                                   AutoCompleteTextView acFunctional,
                                                   AutoCompleteTextView acEconomic,
                                                   AutoCompleteTextView acDepreciate) {
        return new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {

            }

            @Override
            public void afterTextChanged(Editable s) {
                acEconomic.removeTextChangedListener(this);
                acEconomic.setSelection(acEconomic.getText().length());
                acEconomic.addTextChangedListener(this);

                double cleanFloorArea = Double.valueOf(cleanFormat(nullcheckInt(acFloorArea.getText().toString())));

                double cleanRCNSQM = Double.valueOf(cleanFormat(nullcheckInt(acRcnSqm.getText().toString())));

                double cleanRCN = cleanFloorArea * cleanRCNSQM;

                acRcn.setText(getFormattedAmount(String.valueOf(cleanRCN)));

                double cleanEffectiveAge = Double.valueOf(cleanFormat(nullcheckInt(effectiveAge)));
                double cleanEconomicLife = Double.valueOf(cleanFormat(nullcheckInt(economicLife)));

//                double cleanIncurablePerc = (cleanEffectiveAge / cleanEconomicLife) * 100;
                double cleanIncurablePerc = Double.valueOf(cleanFormat(nullcheckInt(acPercent.getText().toString())));
                Log.d("int--cleanIncPer", String.valueOf(cleanIncurablePerc));
                double cleanIncurable = cleanRCN * ((cleanEffectiveAge / cleanEconomicLife) * 100 / 100);

                acPhysical.setText(getFormattedAmount(String.valueOf(cleanIncurable)));

                double cleanFunctionalObsolence = Double.valueOf(cleanFormat(nullcheckInt(acFunctional.getText().toString())));
                double cleanEconomicObsolence = Double.valueOf(cleanFormat(nullcheckInt(acEconomic.getText().toString())));

                double cleanDepreciatedRCN = cleanRCN - cleanIncurable - cleanFunctionalObsolence - cleanEconomicObsolence;

//                double cleanDepreciatedRCNRoundedBy1000 = Math.round(cleanDepreciatedRCN/1000) * 1000;

                acDepreciate.setText(getFormattedAmount(String.valueOf(cleanDepreciatedRCN)));
            }
        };
    }

}
