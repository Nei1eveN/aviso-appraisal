package com.gdslinkasia.base.utils.computation;

import android.text.Editable;
import android.text.TextWatcher;
import android.widget.AutoCompleteTextView;

import static com.gdslinkasia.base.utils.GlobalFunctions.nullcheckInt;
import static com.gdslinkasia.base.utils.GlobalString.cleanFormat;
import static com.gdslinkasia.base.utils.GlobalString.getFormattedAmountDouble;

public class OtherDescImprovementComputation {

    public static TextWatcher AC_ECONOMIC_LIFE(
            AutoCompleteTextView acEcoLife,
            AutoCompleteTextView acEffectiveAge,
            AutoCompleteTextView acRemLife) {
        return new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {

            }

            @Override
            public void afterTextChanged(Editable s) {
                acEcoLife.removeTextChangedListener(this);
                acEcoLife.setSelection(acEcoLife.getText().length());
                acEcoLife.addTextChangedListener(this);

                double cleanEcoLife = Double.valueOf(cleanFormat(nullcheckInt(acEcoLife.getText().toString())));
                double cleanEffectiveAge = Double.valueOf(cleanFormat(nullcheckInt(acEffectiveAge.getText().toString())));

                double cleanRemainingEcoLife = cleanEcoLife - cleanEffectiveAge;

                acRemLife.setText(getFormattedAmountDouble(String.valueOf(cleanRemainingEcoLife)));
            }
        };
    }

    public static TextWatcher AC_EFFECTIVE_AGE(
            AutoCompleteTextView acEcoLife,
            AutoCompleteTextView acEffectiveAge,
            AutoCompleteTextView acRemLife) {
        return new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {

            }

            @Override
            public void afterTextChanged(Editable s) {
                acEffectiveAge.removeTextChangedListener(this);
                acEffectiveAge.setSelection(acEffectiveAge.getText().length());
                acEffectiveAge.addTextChangedListener(this);

                double cleanEcoLife = Double.valueOf(cleanFormat(nullcheckInt(acEcoLife.getText().toString())));
                double cleanEffectiveAge = Double.valueOf(cleanFormat(nullcheckInt(acEffectiveAge.getText().toString())));

                double cleanRemainingEcoLife = cleanEcoLife - cleanEffectiveAge;

                acRemLife.setText(getFormattedAmountDouble(String.valueOf(cleanRemainingEcoLife)));
            }
        };
    }

}
