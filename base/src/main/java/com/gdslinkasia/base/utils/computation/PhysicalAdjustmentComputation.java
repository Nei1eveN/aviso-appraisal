package com.gdslinkasia.base.utils.computation;

import android.text.Editable;
import android.text.TextWatcher;
import android.widget.AutoCompleteTextView;

import static com.gdslinkasia.base.utils.GlobalFunctions.nullcheckInt;
import static com.gdslinkasia.base.utils.GlobalString.cleanFormat;

public class PhysicalAdjustmentComputation {

    public static TextWatcher AC_VALUE1(String netAdjustmentPrice,
                                        AutoCompleteTextView acValue1,
                                        AutoCompleteTextView acValue2,
                                        AutoCompleteTextView acValue3,
                                        AutoCompleteTextView acValue4,
                                        AutoCompleteTextView acValue5,
                                        AutoCompleteTextView acValue6,
                                        AutoCompleteTextView acValue7,
                                        AutoCompleteTextView acValue8,
                                        AutoCompleteTextView acValue9,
                                        AutoCompleteTextView acValue10,
                                        AutoCompleteTextView acValue11,
                                        AutoCompleteTextView acValue12,
                                        AutoCompleteTextView acTotalGross,
                                        AutoCompleteTextView acTotalNetAdj,
                                        AutoCompleteTextView acAdjPriceSqm,
                                        AutoCompleteTextView acWeight,
                                        AutoCompleteTextView acWeightedVal) {
        return new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {

            }

            @Override
            public void afterTextChanged(Editable s) {
                acValue1.removeTextChangedListener(this);
                acValue1.setSelection(acValue1.getText().length());
                acValue1.addTextChangedListener(this);

                double cleanValue1 = Double.valueOf(cleanFormat(nullcheckInt(acValue1.getText().toString())));
                double cleanValue2 = Double.valueOf(cleanFormat(nullcheckInt(acValue2.getText().toString())));
                double cleanValue3 = Double.valueOf(cleanFormat(nullcheckInt(acValue3.getText().toString())));
                double cleanValue4 = Double.valueOf(cleanFormat(nullcheckInt(acValue4.getText().toString())));
                double cleanValue5 = Double.valueOf(cleanFormat(nullcheckInt(acValue5.getText().toString())));
                double cleanValue6 = Double.valueOf(cleanFormat(nullcheckInt(acValue6.getText().toString())));
                double cleanValue7 = Double.valueOf(cleanFormat(nullcheckInt(acValue7.getText().toString())));
                double cleanValue8 = Double.valueOf(cleanFormat(nullcheckInt(acValue8.getText().toString())));
                double cleanValue9 = Double.valueOf(cleanFormat(nullcheckInt(acValue9.getText().toString())));
                double cleanValue10 = Double.valueOf(cleanFormat(nullcheckInt(acValue10.getText().toString())));
                double cleanValue11 = Double.valueOf(cleanFormat(nullcheckInt(acValue11.getText().toString())));
                double cleanValue12 = Double.valueOf(cleanFormat(nullcheckInt(acValue12.getText().toString())));

                double cleanTotalGross =
                        (Math.abs(cleanValue1) + Math.abs(cleanValue2) + Math.abs(cleanValue3) + Math.abs(cleanValue4) + Math.abs(cleanValue5) + Math.abs(cleanValue6)
                                + Math.abs(cleanValue7) + Math.abs(cleanValue8) + Math.abs(cleanValue9) + Math.abs(cleanValue10) + Math.abs(cleanValue11) + Math.abs(cleanValue12)
                        );

                double cleanTotalNetAdj =
                        (cleanValue1 + cleanValue2 + cleanValue3 + cleanValue4 + cleanValue5 + cleanValue6
                                + cleanValue7 + cleanValue8 + cleanValue9 + cleanValue10 + cleanValue11 + cleanValue12
                        );

                acTotalGross.setText(String.valueOf(Math.round(cleanTotalGross)));

                acTotalNetAdj.setText(String.valueOf(Math.round((cleanTotalNetAdj))));

                double cleanNetAdjPrice = Double.valueOf(cleanFormat(nullcheckInt(netAdjustmentPrice)));

                double cleanAdjPriceSqm = (cleanNetAdjPrice - (cleanNetAdjPrice * (Double.valueOf(acTotalNetAdj.getText().toString()) / 100))); // / 100

                acAdjPriceSqm.setText(String.valueOf(Math.round(cleanAdjPriceSqm)));

                double cleanWeight = Double.valueOf(cleanFormat(nullcheckInt(acWeight.getText().toString())));

                double cleanWeightedValue = (cleanAdjPriceSqm * (cleanWeight)) / 100;

                acWeightedVal.setText(String.valueOf(Math.round(cleanWeightedValue)));
            }
        };
    }

    public static TextWatcher AC_VALUE2(String netAdjustmentPrice,
                                        AutoCompleteTextView acValue1,
                                        AutoCompleteTextView acValue2,
                                        AutoCompleteTextView acValue3,
                                        AutoCompleteTextView acValue4,
                                        AutoCompleteTextView acValue5,
                                        AutoCompleteTextView acValue6,
                                        AutoCompleteTextView acValue7,
                                        AutoCompleteTextView acValue8,
                                        AutoCompleteTextView acValue9,
                                        AutoCompleteTextView acValue10,
                                        AutoCompleteTextView acValue11,
                                        AutoCompleteTextView acValue12,
                                        AutoCompleteTextView acTotalGross,
                                        AutoCompleteTextView acTotalNetAdj,
                                        AutoCompleteTextView acAdjPriceSqm,
                                        AutoCompleteTextView acWeight,
                                        AutoCompleteTextView acWeightedVal) {
        return new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {

            }

            @Override
            public void afterTextChanged(Editable s) {
                acValue2.removeTextChangedListener(this);
                acValue2.setSelection(acValue2.getText().length());
                acValue2.addTextChangedListener(this);

                double cleanValue1 = Double.valueOf(cleanFormat(nullcheckInt(acValue1.getText().toString())));
                double cleanValue2 = Double.valueOf(cleanFormat(nullcheckInt(acValue2.getText().toString())));
                double cleanValue3 = Double.valueOf(cleanFormat(nullcheckInt(acValue3.getText().toString())));
                double cleanValue4 = Double.valueOf(cleanFormat(nullcheckInt(acValue4.getText().toString())));
                double cleanValue5 = Double.valueOf(cleanFormat(nullcheckInt(acValue5.getText().toString())));
                double cleanValue6 = Double.valueOf(cleanFormat(nullcheckInt(acValue6.getText().toString())));
                double cleanValue7 = Double.valueOf(cleanFormat(nullcheckInt(acValue7.getText().toString())));
                double cleanValue8 = Double.valueOf(cleanFormat(nullcheckInt(acValue8.getText().toString())));
                double cleanValue9 = Double.valueOf(cleanFormat(nullcheckInt(acValue9.getText().toString())));
                double cleanValue10 = Double.valueOf(cleanFormat(nullcheckInt(acValue10.getText().toString())));
                double cleanValue11 = Double.valueOf(cleanFormat(nullcheckInt(acValue11.getText().toString())));
                double cleanValue12 = Double.valueOf(cleanFormat(nullcheckInt(acValue12.getText().toString())));

                double cleanTotalGross =
                        (Math.abs(cleanValue1) + Math.abs(cleanValue2) + Math.abs(cleanValue3) + Math.abs(cleanValue4) + Math.abs(cleanValue5) + Math.abs(cleanValue6)
                                + Math.abs(cleanValue7) + Math.abs(cleanValue8) + Math.abs(cleanValue9) + Math.abs(cleanValue10) + Math.abs(cleanValue11) + Math.abs(cleanValue12)
                        );

                double cleanTotalNetAdj =
                        (cleanValue1 + cleanValue2 + cleanValue3 + cleanValue4 + cleanValue5 + cleanValue6
                                + cleanValue7 + cleanValue8 + cleanValue9 + cleanValue10 + cleanValue11 + cleanValue12
                        );

                acTotalGross.setText(String.valueOf(Math.round(cleanTotalGross)));

                acTotalNetAdj.setText(String.valueOf(Math.round((cleanTotalNetAdj))));

                double cleanNetAdjPrice = Double.valueOf(cleanFormat(nullcheckInt(netAdjustmentPrice)));

                double cleanAdjPriceSqm = (cleanNetAdjPrice - (cleanNetAdjPrice * (Double.valueOf(acTotalNetAdj.getText().toString()) / 100))); // / 100

                acAdjPriceSqm.setText(String.valueOf(Math.round(cleanAdjPriceSqm)));

                double cleanWeight = Double.valueOf(cleanFormat(nullcheckInt(acWeight.getText().toString())));

                double cleanWeightedValue = (cleanAdjPriceSqm * (cleanWeight)) / 100;

                acWeightedVal.setText(String.valueOf(Math.round(cleanWeightedValue)));
            }
        };
    }

    public static TextWatcher AC_VALUE3(String netAdjustmentPrice,
                                        AutoCompleteTextView acValue1,
                                        AutoCompleteTextView acValue2,
                                        AutoCompleteTextView acValue3,
                                        AutoCompleteTextView acValue4,
                                        AutoCompleteTextView acValue5,
                                        AutoCompleteTextView acValue6,
                                        AutoCompleteTextView acValue7,
                                        AutoCompleteTextView acValue8,
                                        AutoCompleteTextView acValue9,
                                        AutoCompleteTextView acValue10,
                                        AutoCompleteTextView acValue11,
                                        AutoCompleteTextView acValue12,
                                        AutoCompleteTextView acTotalGross,
                                        AutoCompleteTextView acTotalNetAdj,
                                        AutoCompleteTextView acAdjPriceSqm,
                                        AutoCompleteTextView acWeight,
                                        AutoCompleteTextView acWeightedVal) {
        return new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {

            }

            @Override
            public void afterTextChanged(Editable s) {
                acValue3.removeTextChangedListener(this);
                acValue3.setSelection(acValue3.getText().length());
                acValue3.addTextChangedListener(this);

                double cleanValue1 = Double.valueOf(cleanFormat(nullcheckInt(acValue1.getText().toString())));
                double cleanValue2 = Double.valueOf(cleanFormat(nullcheckInt(acValue2.getText().toString())));
                double cleanValue3 = Double.valueOf(cleanFormat(nullcheckInt(acValue3.getText().toString())));
                double cleanValue4 = Double.valueOf(cleanFormat(nullcheckInt(acValue4.getText().toString())));
                double cleanValue5 = Double.valueOf(cleanFormat(nullcheckInt(acValue5.getText().toString())));
                double cleanValue6 = Double.valueOf(cleanFormat(nullcheckInt(acValue6.getText().toString())));
                double cleanValue7 = Double.valueOf(cleanFormat(nullcheckInt(acValue7.getText().toString())));
                double cleanValue8 = Double.valueOf(cleanFormat(nullcheckInt(acValue8.getText().toString())));
                double cleanValue9 = Double.valueOf(cleanFormat(nullcheckInt(acValue9.getText().toString())));
                double cleanValue10 = Double.valueOf(cleanFormat(nullcheckInt(acValue10.getText().toString())));
                double cleanValue11 = Double.valueOf(cleanFormat(nullcheckInt(acValue11.getText().toString())));
                double cleanValue12 = Double.valueOf(cleanFormat(nullcheckInt(acValue12.getText().toString())));

                double cleanTotalGross =
                        (Math.abs(cleanValue1) + Math.abs(cleanValue2) + Math.abs(cleanValue3) + Math.abs(cleanValue4) + Math.abs(cleanValue5) + Math.abs(cleanValue6)
                                + Math.abs(cleanValue7) + Math.abs(cleanValue8) + Math.abs(cleanValue9) + Math.abs(cleanValue10) + Math.abs(cleanValue11) + Math.abs(cleanValue12)
                        );

                double cleanTotalNetAdj =
                        (cleanValue1 + cleanValue2 + cleanValue3 + cleanValue4 + cleanValue5 + cleanValue6
                                + cleanValue7 + cleanValue8 + cleanValue9 + cleanValue10 + cleanValue11 + cleanValue12
                        );

                acTotalGross.setText(String.valueOf(Math.round(cleanTotalGross)));

                acTotalNetAdj.setText(String.valueOf(Math.round((cleanTotalNetAdj))));

                double cleanNetAdjPrice = Double.valueOf(cleanFormat(nullcheckInt(netAdjustmentPrice)));

                double cleanAdjPriceSqm = (cleanNetAdjPrice - (cleanNetAdjPrice * (Double.valueOf(acTotalNetAdj.getText().toString()) / 100))); // / 100

                acAdjPriceSqm.setText(String.valueOf(Math.round(cleanAdjPriceSqm)));

                double cleanWeight = Double.valueOf(cleanFormat(nullcheckInt(acWeight.getText().toString())));

                double cleanWeightedValue = (cleanAdjPriceSqm * (cleanWeight)) / 100;

                acWeightedVal.setText(String.valueOf(Math.round(cleanWeightedValue)));
            }
        };
    }

    public static TextWatcher AC_VALUE4(String netAdjustmentPrice,
                                        AutoCompleteTextView acValue1,
                                        AutoCompleteTextView acValue2,
                                        AutoCompleteTextView acValue3,
                                        AutoCompleteTextView acValue4,
                                        AutoCompleteTextView acValue5,
                                        AutoCompleteTextView acValue6,
                                        AutoCompleteTextView acValue7,
                                        AutoCompleteTextView acValue8,
                                        AutoCompleteTextView acValue9,
                                        AutoCompleteTextView acValue10,
                                        AutoCompleteTextView acValue11,
                                        AutoCompleteTextView acValue12,
                                        AutoCompleteTextView acTotalGross,
                                        AutoCompleteTextView acTotalNetAdj,
                                        AutoCompleteTextView acAdjPriceSqm,
                                        AutoCompleteTextView acWeight,
                                        AutoCompleteTextView acWeightedVal) {
        return new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {

            }

            @Override
            public void afterTextChanged(Editable s) {
                acValue4.removeTextChangedListener(this);
                acValue4.setSelection(acValue4.getText().length());
                acValue4.addTextChangedListener(this);

                double cleanValue1 = Double.valueOf(cleanFormat(nullcheckInt(acValue1.getText().toString())));
                double cleanValue2 = Double.valueOf(cleanFormat(nullcheckInt(acValue2.getText().toString())));
                double cleanValue3 = Double.valueOf(cleanFormat(nullcheckInt(acValue3.getText().toString())));
                double cleanValue4 = Double.valueOf(cleanFormat(nullcheckInt(acValue4.getText().toString())));
                double cleanValue5 = Double.valueOf(cleanFormat(nullcheckInt(acValue5.getText().toString())));
                double cleanValue6 = Double.valueOf(cleanFormat(nullcheckInt(acValue6.getText().toString())));
                double cleanValue7 = Double.valueOf(cleanFormat(nullcheckInt(acValue7.getText().toString())));
                double cleanValue8 = Double.valueOf(cleanFormat(nullcheckInt(acValue8.getText().toString())));
                double cleanValue9 = Double.valueOf(cleanFormat(nullcheckInt(acValue9.getText().toString())));
                double cleanValue10 = Double.valueOf(cleanFormat(nullcheckInt(acValue10.getText().toString())));
                double cleanValue11 = Double.valueOf(cleanFormat(nullcheckInt(acValue11.getText().toString())));
                double cleanValue12 = Double.valueOf(cleanFormat(nullcheckInt(acValue12.getText().toString())));

                double cleanTotalGross =
                        (Math.abs(cleanValue1) + Math.abs(cleanValue2) + Math.abs(cleanValue3) + Math.abs(cleanValue4) + Math.abs(cleanValue5) + Math.abs(cleanValue6)
                                + Math.abs(cleanValue7) + Math.abs(cleanValue8) + Math.abs(cleanValue9) + Math.abs(cleanValue10) + Math.abs(cleanValue11) + Math.abs(cleanValue12)
                        );

                double cleanTotalNetAdj =
                        (cleanValue1 + cleanValue2 + cleanValue3 + cleanValue4 + cleanValue5 + cleanValue6
                                + cleanValue7 + cleanValue8 + cleanValue9 + cleanValue10 + cleanValue11 + cleanValue12
                        );

                acTotalGross.setText(String.valueOf(Math.round(cleanTotalGross)));

                acTotalNetAdj.setText(String.valueOf(Math.round((cleanTotalNetAdj))));

                double cleanNetAdjPrice = Double.valueOf(cleanFormat(nullcheckInt(netAdjustmentPrice)));

                double cleanAdjPriceSqm = (cleanNetAdjPrice - (cleanNetAdjPrice * (Double.valueOf(acTotalNetAdj.getText().toString()) / 100))); // / 100

                acAdjPriceSqm.setText(String.valueOf(Math.round(cleanAdjPriceSqm)));

                double cleanWeight = Double.valueOf(cleanFormat(nullcheckInt(acWeight.getText().toString())));

                double cleanWeightedValue = (cleanAdjPriceSqm * (cleanWeight)) / 100;

                acWeightedVal.setText(String.valueOf(Math.round(cleanWeightedValue)));
            }
        };
    }

    public static TextWatcher AC_VALUE5(String netAdjustmentPrice,
                                        AutoCompleteTextView acValue1,
                                        AutoCompleteTextView acValue2,
                                        AutoCompleteTextView acValue3,
                                        AutoCompleteTextView acValue4,
                                        AutoCompleteTextView acValue5,
                                        AutoCompleteTextView acValue6,
                                        AutoCompleteTextView acValue7,
                                        AutoCompleteTextView acValue8,
                                        AutoCompleteTextView acValue9,
                                        AutoCompleteTextView acValue10,
                                        AutoCompleteTextView acValue11,
                                        AutoCompleteTextView acValue12,
                                        AutoCompleteTextView acTotalGross,
                                        AutoCompleteTextView acTotalNetAdj,
                                        AutoCompleteTextView acAdjPriceSqm,
                                        AutoCompleteTextView acWeight,
                                        AutoCompleteTextView acWeightedVal) {
        return new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {

            }

            @Override
            public void afterTextChanged(Editable s) {
                acValue5.removeTextChangedListener(this);
                acValue5.setSelection(acValue5.getText().length());
                acValue5.addTextChangedListener(this);

                double cleanValue1 = Double.valueOf(cleanFormat(nullcheckInt(acValue1.getText().toString())));
                double cleanValue2 = Double.valueOf(cleanFormat(nullcheckInt(acValue2.getText().toString())));
                double cleanValue3 = Double.valueOf(cleanFormat(nullcheckInt(acValue3.getText().toString())));
                double cleanValue4 = Double.valueOf(cleanFormat(nullcheckInt(acValue4.getText().toString())));
                double cleanValue5 = Double.valueOf(cleanFormat(nullcheckInt(acValue5.getText().toString())));
                double cleanValue6 = Double.valueOf(cleanFormat(nullcheckInt(acValue6.getText().toString())));
                double cleanValue7 = Double.valueOf(cleanFormat(nullcheckInt(acValue7.getText().toString())));
                double cleanValue8 = Double.valueOf(cleanFormat(nullcheckInt(acValue8.getText().toString())));
                double cleanValue9 = Double.valueOf(cleanFormat(nullcheckInt(acValue9.getText().toString())));
                double cleanValue10 = Double.valueOf(cleanFormat(nullcheckInt(acValue10.getText().toString())));
                double cleanValue11 = Double.valueOf(cleanFormat(nullcheckInt(acValue11.getText().toString())));
                double cleanValue12 = Double.valueOf(cleanFormat(nullcheckInt(acValue12.getText().toString())));

                double cleanTotalGross =
                        (Math.abs(cleanValue1) + Math.abs(cleanValue2) + Math.abs(cleanValue3) + Math.abs(cleanValue4) + Math.abs(cleanValue5) + Math.abs(cleanValue6)
                                + Math.abs(cleanValue7) + Math.abs(cleanValue8) + Math.abs(cleanValue9) + Math.abs(cleanValue10) + Math.abs(cleanValue11) + Math.abs(cleanValue12)
                        );

                double cleanTotalNetAdj =
                        (cleanValue1 + cleanValue2 + cleanValue3 + cleanValue4 + cleanValue5 + cleanValue6
                                + cleanValue7 + cleanValue8 + cleanValue9 + cleanValue10 + cleanValue11 + cleanValue12
                        );

                acTotalGross.setText(String.valueOf(Math.round(cleanTotalGross)));

                acTotalNetAdj.setText(String.valueOf(Math.round((cleanTotalNetAdj))));

                double cleanNetAdjPrice = Double.valueOf(cleanFormat(nullcheckInt(netAdjustmentPrice)));

                double cleanAdjPriceSqm = (cleanNetAdjPrice - (cleanNetAdjPrice * (Double.valueOf(acTotalNetAdj.getText().toString()) / 100))); // / 100

                acAdjPriceSqm.setText(String.valueOf(Math.round(cleanAdjPriceSqm)));

                double cleanWeight = Double.valueOf(cleanFormat(nullcheckInt(acWeight.getText().toString())));

                double cleanWeightedValue = (cleanAdjPriceSqm * (cleanWeight)) / 100;

                acWeightedVal.setText(String.valueOf(Math.round(cleanWeightedValue)));
            }
        };
    }

    public static TextWatcher AC_VALUE6(String netAdjustmentPrice,
                                        AutoCompleteTextView acValue1,
                                        AutoCompleteTextView acValue2,
                                        AutoCompleteTextView acValue3,
                                        AutoCompleteTextView acValue4,
                                        AutoCompleteTextView acValue5,
                                        AutoCompleteTextView acValue6,
                                        AutoCompleteTextView acValue7,
                                        AutoCompleteTextView acValue8,
                                        AutoCompleteTextView acValue9,
                                        AutoCompleteTextView acValue10,
                                        AutoCompleteTextView acValue11,
                                        AutoCompleteTextView acValue12,
                                        AutoCompleteTextView acTotalGross,
                                        AutoCompleteTextView acTotalNetAdj,
                                        AutoCompleteTextView acAdjPriceSqm,
                                        AutoCompleteTextView acWeight,
                                        AutoCompleteTextView acWeightedVal) {
        return new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {

            }

            @Override
            public void afterTextChanged(Editable s) {
                acValue6.removeTextChangedListener(this);
                acValue6.setSelection(acValue6.getText().length());
                acValue6.addTextChangedListener(this);

                double cleanValue1 = Double.valueOf(cleanFormat(nullcheckInt(acValue1.getText().toString())));
                double cleanValue2 = Double.valueOf(cleanFormat(nullcheckInt(acValue2.getText().toString())));
                double cleanValue3 = Double.valueOf(cleanFormat(nullcheckInt(acValue3.getText().toString())));
                double cleanValue4 = Double.valueOf(cleanFormat(nullcheckInt(acValue4.getText().toString())));
                double cleanValue5 = Double.valueOf(cleanFormat(nullcheckInt(acValue5.getText().toString())));
                double cleanValue6 = Double.valueOf(cleanFormat(nullcheckInt(acValue6.getText().toString())));
                double cleanValue7 = Double.valueOf(cleanFormat(nullcheckInt(acValue7.getText().toString())));
                double cleanValue8 = Double.valueOf(cleanFormat(nullcheckInt(acValue8.getText().toString())));
                double cleanValue9 = Double.valueOf(cleanFormat(nullcheckInt(acValue9.getText().toString())));
                double cleanValue10 = Double.valueOf(cleanFormat(nullcheckInt(acValue10.getText().toString())));
                double cleanValue11 = Double.valueOf(cleanFormat(nullcheckInt(acValue11.getText().toString())));
                double cleanValue12 = Double.valueOf(cleanFormat(nullcheckInt(acValue12.getText().toString())));

                double cleanTotalGross =
                        (Math.abs(cleanValue1) + Math.abs(cleanValue2) + Math.abs(cleanValue3) + Math.abs(cleanValue4) + Math.abs(cleanValue5) + Math.abs(cleanValue6)
                                + Math.abs(cleanValue7) + Math.abs(cleanValue8) + Math.abs(cleanValue9) + Math.abs(cleanValue10) + Math.abs(cleanValue11) + Math.abs(cleanValue12)
                        );

                double cleanTotalNetAdj =
                        (cleanValue1 + cleanValue2 + cleanValue3 + cleanValue4 + cleanValue5 + cleanValue6
                                + cleanValue7 + cleanValue8 + cleanValue9 + cleanValue10 + cleanValue11 + cleanValue12
                        );

                acTotalGross.setText(String.valueOf(Math.round(cleanTotalGross)));

                acTotalNetAdj.setText(String.valueOf(Math.round((cleanTotalNetAdj))));

                double cleanNetAdjPrice = Double.valueOf(cleanFormat(nullcheckInt(netAdjustmentPrice)));

                double cleanAdjPriceSqm = (cleanNetAdjPrice - (cleanNetAdjPrice * (Double.valueOf(acTotalNetAdj.getText().toString()) / 100))); // / 100

                acAdjPriceSqm.setText(String.valueOf(Math.round(cleanAdjPriceSqm)));

                double cleanWeight = Double.valueOf(cleanFormat(nullcheckInt(acWeight.getText().toString())));

                double cleanWeightedValue = (cleanAdjPriceSqm * (cleanWeight)) / 100;

                acWeightedVal.setText(String.valueOf(Math.round(cleanWeightedValue)));
            }
        };
    }

    public static TextWatcher AC_VALUE7(String netAdjustmentPrice,
                                         AutoCompleteTextView acValue1,
                                         AutoCompleteTextView acValue2,
                                         AutoCompleteTextView acValue3,
                                         AutoCompleteTextView acValue4,
                                         AutoCompleteTextView acValue5,
                                         AutoCompleteTextView acValue6,
                                         AutoCompleteTextView acValue7,
                                         AutoCompleteTextView acValue8,
                                         AutoCompleteTextView acValue9,
                                         AutoCompleteTextView acValue10,
                                         AutoCompleteTextView acValue11,
                                         AutoCompleteTextView acValue12,
                                         AutoCompleteTextView acTotalGross,
                                         AutoCompleteTextView acTotalNetAdj,
                                         AutoCompleteTextView acAdjPriceSqm,
                                         AutoCompleteTextView acWeight,
                                         AutoCompleteTextView acWeightedVal) {
        return new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {

            }

            @Override
            public void afterTextChanged(Editable s) {
                acValue7.removeTextChangedListener(this);
                acValue7.setSelection(acValue7.getText().length());
                acValue7.addTextChangedListener(this);

                double cleanValue1 = Double.valueOf(cleanFormat(nullcheckInt(acValue1.getText().toString())));
                double cleanValue2 = Double.valueOf(cleanFormat(nullcheckInt(acValue2.getText().toString())));
                double cleanValue3 = Double.valueOf(cleanFormat(nullcheckInt(acValue3.getText().toString())));
                double cleanValue4 = Double.valueOf(cleanFormat(nullcheckInt(acValue4.getText().toString())));
                double cleanValue5 = Double.valueOf(cleanFormat(nullcheckInt(acValue5.getText().toString())));
                double cleanValue6 = Double.valueOf(cleanFormat(nullcheckInt(acValue6.getText().toString())));
                double cleanValue7 = Double.valueOf(cleanFormat(nullcheckInt(acValue7.getText().toString())));
                double cleanValue8 = Double.valueOf(cleanFormat(nullcheckInt(acValue8.getText().toString())));
                double cleanValue9 = Double.valueOf(cleanFormat(nullcheckInt(acValue9.getText().toString())));
                double cleanValue10 = Double.valueOf(cleanFormat(nullcheckInt(acValue10.getText().toString())));
                double cleanValue11 = Double.valueOf(cleanFormat(nullcheckInt(acValue11.getText().toString())));
                double cleanValue12 = Double.valueOf(cleanFormat(nullcheckInt(acValue12.getText().toString())));

                double cleanTotalGross =
                        (Math.abs(cleanValue1) + Math.abs(cleanValue2) + Math.abs(cleanValue3) + Math.abs(cleanValue4) + Math.abs(cleanValue5) + Math.abs(cleanValue6)
                                + Math.abs(cleanValue7) + Math.abs(cleanValue8) + Math.abs(cleanValue9) + Math.abs(cleanValue10) + Math.abs(cleanValue11) + Math.abs(cleanValue12)
                        );

                double cleanTotalNetAdj =
                        (cleanValue1 + cleanValue2 + cleanValue3 + cleanValue4 + cleanValue5 + cleanValue6
                                + cleanValue7 + cleanValue8 + cleanValue9 + cleanValue10 + cleanValue11 + cleanValue12
                        );

                acTotalGross.setText(String.valueOf(Math.round(cleanTotalGross)));

                acTotalNetAdj.setText(String.valueOf(Math.round((cleanTotalNetAdj))));

                double cleanNetAdjPrice = Double.valueOf(cleanFormat(nullcheckInt(netAdjustmentPrice)));

                double cleanAdjPriceSqm = (cleanNetAdjPrice - (cleanNetAdjPrice * (Double.valueOf(acTotalNetAdj.getText().toString()) / 100))); // / 100

                acAdjPriceSqm.setText(String.valueOf(Math.round(cleanAdjPriceSqm)));

                double cleanWeight = Double.valueOf(cleanFormat(nullcheckInt(acWeight.getText().toString())));

                double cleanWeightedValue = (cleanAdjPriceSqm * (cleanWeight)) / 100;

                acWeightedVal.setText(String.valueOf(Math.round(cleanWeightedValue)));
            }
        };
    }

    public static TextWatcher AC_VALUE8(String netAdjustmentPrice,
                                        AutoCompleteTextView acValue1,
                                        AutoCompleteTextView acValue2,
                                        AutoCompleteTextView acValue3,
                                        AutoCompleteTextView acValue4,
                                        AutoCompleteTextView acValue5,
                                        AutoCompleteTextView acValue6,
                                        AutoCompleteTextView acValue7,
                                        AutoCompleteTextView acValue8,
                                        AutoCompleteTextView acValue9,
                                        AutoCompleteTextView acValue10,
                                        AutoCompleteTextView acValue11,
                                        AutoCompleteTextView acValue12,
                                        AutoCompleteTextView acTotalGross,
                                        AutoCompleteTextView acTotalNetAdj,
                                        AutoCompleteTextView acAdjPriceSqm,
                                        AutoCompleteTextView acWeight,
                                        AutoCompleteTextView acWeightedVal) {
        return new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {

            }

            @Override
            public void afterTextChanged(Editable s) {
                acValue8.removeTextChangedListener(this);
                acValue8.setSelection(acValue8.getText().length());
                acValue8.addTextChangedListener(this);

                double cleanValue1 = Double.valueOf(cleanFormat(nullcheckInt(acValue1.getText().toString())));
                double cleanValue2 = Double.valueOf(cleanFormat(nullcheckInt(acValue2.getText().toString())));
                double cleanValue3 = Double.valueOf(cleanFormat(nullcheckInt(acValue3.getText().toString())));
                double cleanValue4 = Double.valueOf(cleanFormat(nullcheckInt(acValue4.getText().toString())));
                double cleanValue5 = Double.valueOf(cleanFormat(nullcheckInt(acValue5.getText().toString())));
                double cleanValue6 = Double.valueOf(cleanFormat(nullcheckInt(acValue6.getText().toString())));
                double cleanValue7 = Double.valueOf(cleanFormat(nullcheckInt(acValue7.getText().toString())));
                double cleanValue8 = Double.valueOf(cleanFormat(nullcheckInt(acValue8.getText().toString())));
                double cleanValue9 = Double.valueOf(cleanFormat(nullcheckInt(acValue9.getText().toString())));
                double cleanValue10 = Double.valueOf(cleanFormat(nullcheckInt(acValue10.getText().toString())));
                double cleanValue11 = Double.valueOf(cleanFormat(nullcheckInt(acValue11.getText().toString())));
                double cleanValue12 = Double.valueOf(cleanFormat(nullcheckInt(acValue12.getText().toString())));

                double cleanTotalGross =
                        (Math.abs(cleanValue1) + Math.abs(cleanValue2) + Math.abs(cleanValue3) + Math.abs(cleanValue4) + Math.abs(cleanValue5) + Math.abs(cleanValue6)
                                + Math.abs(cleanValue7) + Math.abs(cleanValue8) + Math.abs(cleanValue9) + Math.abs(cleanValue10) + Math.abs(cleanValue11) + Math.abs(cleanValue12)
                        );

                double cleanTotalNetAdj =
                        (cleanValue1 + cleanValue2 + cleanValue3 + cleanValue4 + cleanValue5 + cleanValue6
                                + cleanValue7 + cleanValue8 + cleanValue9 + cleanValue10 + cleanValue11 + cleanValue12
                        );

                acTotalGross.setText(String.valueOf(Math.round(cleanTotalGross)));

                acTotalNetAdj.setText(String.valueOf(Math.round((cleanTotalNetAdj))));

                double cleanNetAdjPrice = Double.valueOf(cleanFormat(nullcheckInt(netAdjustmentPrice)));

                double cleanAdjPriceSqm = (cleanNetAdjPrice - (cleanNetAdjPrice * (Double.valueOf(acTotalNetAdj.getText().toString()) / 100))); // / 100

                acAdjPriceSqm.setText(String.valueOf(Math.round(cleanAdjPriceSqm)));

                double cleanWeight = Double.valueOf(cleanFormat(nullcheckInt(acWeight.getText().toString())));

                double cleanWeightedValue = (cleanAdjPriceSqm * (cleanWeight)) / 100;

                acWeightedVal.setText(String.valueOf(Math.round(cleanWeightedValue)));
            }
        };
    }


    public static TextWatcher AC_VALUE9(String netAdjustmentPrice,
                                        AutoCompleteTextView acValue1,
                                        AutoCompleteTextView acValue2,
                                        AutoCompleteTextView acValue3,
                                        AutoCompleteTextView acValue4,
                                        AutoCompleteTextView acValue5,
                                        AutoCompleteTextView acValue6,
                                        AutoCompleteTextView acValue7,
                                        AutoCompleteTextView acValue8,
                                        AutoCompleteTextView acValue9,
                                        AutoCompleteTextView acValue10,
                                        AutoCompleteTextView acValue11,
                                        AutoCompleteTextView acValue12,
                                        AutoCompleteTextView acTotalGross,
                                        AutoCompleteTextView acTotalNetAdj,
                                        AutoCompleteTextView acAdjPriceSqm,
                                        AutoCompleteTextView acWeight,
                                        AutoCompleteTextView acWeightedVal) {
        return new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {

            }

            @Override
            public void afterTextChanged(Editable s) {
                acValue9.removeTextChangedListener(this);
                acValue9.setSelection(acValue9.getText().length());
                acValue9.addTextChangedListener(this);

                double cleanValue1 = Double.valueOf(cleanFormat(nullcheckInt(acValue1.getText().toString())));
                double cleanValue2 = Double.valueOf(cleanFormat(nullcheckInt(acValue2.getText().toString())));
                double cleanValue3 = Double.valueOf(cleanFormat(nullcheckInt(acValue3.getText().toString())));
                double cleanValue4 = Double.valueOf(cleanFormat(nullcheckInt(acValue4.getText().toString())));
                double cleanValue5 = Double.valueOf(cleanFormat(nullcheckInt(acValue5.getText().toString())));
                double cleanValue6 = Double.valueOf(cleanFormat(nullcheckInt(acValue6.getText().toString())));
                double cleanValue7 = Double.valueOf(cleanFormat(nullcheckInt(acValue7.getText().toString())));
                double cleanValue8 = Double.valueOf(cleanFormat(nullcheckInt(acValue8.getText().toString())));
                double cleanValue9 = Double.valueOf(cleanFormat(nullcheckInt(acValue9.getText().toString())));
                double cleanValue10 = Double.valueOf(cleanFormat(nullcheckInt(acValue10.getText().toString())));
                double cleanValue11 = Double.valueOf(cleanFormat(nullcheckInt(acValue11.getText().toString())));
                double cleanValue12 = Double.valueOf(cleanFormat(nullcheckInt(acValue12.getText().toString())));

                double cleanTotalGross =
                        (Math.abs(cleanValue1) + Math.abs(cleanValue2) + Math.abs(cleanValue3) + Math.abs(cleanValue4) + Math.abs(cleanValue5) + Math.abs(cleanValue6)
                                + Math.abs(cleanValue7) + Math.abs(cleanValue8) + Math.abs(cleanValue9) + Math.abs(cleanValue10) + Math.abs(cleanValue11) + Math.abs(cleanValue12)
                        );

                double cleanTotalNetAdj =
                        (cleanValue1 + cleanValue2 + cleanValue3 + cleanValue4 + cleanValue5 + cleanValue6
                                + cleanValue7 + cleanValue8 + cleanValue9 + cleanValue10 + cleanValue11 + cleanValue12
                        );

                acTotalGross.setText(String.valueOf(Math.round(cleanTotalGross)));

                acTotalNetAdj.setText(String.valueOf(Math.round((cleanTotalNetAdj))));

                double cleanNetAdjPrice = Double.valueOf(cleanFormat(nullcheckInt(netAdjustmentPrice)));

                double cleanAdjPriceSqm = (cleanNetAdjPrice - (cleanNetAdjPrice * (Double.valueOf(acTotalNetAdj.getText().toString()) / 100))); // / 100

                acAdjPriceSqm.setText(String.valueOf(Math.round(cleanAdjPriceSqm)));

                double cleanWeight = Double.valueOf(cleanFormat(nullcheckInt(acWeight.getText().toString())));

                double cleanWeightedValue = (cleanAdjPriceSqm * (cleanWeight)) / 100;

                acWeightedVal.setText(String.valueOf(Math.round(cleanWeightedValue)));
            }
        };
    }

    public static TextWatcher AC_VALUE10(String netAdjustmentPrice,
                                        AutoCompleteTextView acValue1,
                                        AutoCompleteTextView acValue2,
                                        AutoCompleteTextView acValue3,
                                        AutoCompleteTextView acValue4,
                                        AutoCompleteTextView acValue5,
                                        AutoCompleteTextView acValue6,
                                        AutoCompleteTextView acValue7,
                                        AutoCompleteTextView acValue8,
                                        AutoCompleteTextView acValue9,
                                        AutoCompleteTextView acValue10,
                                        AutoCompleteTextView acValue11,
                                        AutoCompleteTextView acValue12,
                                        AutoCompleteTextView acTotalGross,
                                        AutoCompleteTextView acTotalNetAdj,
                                        AutoCompleteTextView acAdjPriceSqm,
                                        AutoCompleteTextView acWeight,
                                        AutoCompleteTextView acWeightedVal) {
        return new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {

            }

            @Override
            public void afterTextChanged(Editable s) {
                acValue10.removeTextChangedListener(this);
                acValue10.setSelection(acValue10.getText().length());
                acValue10.addTextChangedListener(this);

                double cleanValue1 = Double.valueOf(cleanFormat(nullcheckInt(acValue1.getText().toString())));
                double cleanValue2 = Double.valueOf(cleanFormat(nullcheckInt(acValue2.getText().toString())));
                double cleanValue3 = Double.valueOf(cleanFormat(nullcheckInt(acValue3.getText().toString())));
                double cleanValue4 = Double.valueOf(cleanFormat(nullcheckInt(acValue4.getText().toString())));
                double cleanValue5 = Double.valueOf(cleanFormat(nullcheckInt(acValue5.getText().toString())));
                double cleanValue6 = Double.valueOf(cleanFormat(nullcheckInt(acValue6.getText().toString())));
                double cleanValue7 = Double.valueOf(cleanFormat(nullcheckInt(acValue7.getText().toString())));
                double cleanValue8 = Double.valueOf(cleanFormat(nullcheckInt(acValue8.getText().toString())));
                double cleanValue9 = Double.valueOf(cleanFormat(nullcheckInt(acValue9.getText().toString())));
                double cleanValue10 = Double.valueOf(cleanFormat(nullcheckInt(acValue10.getText().toString())));
                double cleanValue11 = Double.valueOf(cleanFormat(nullcheckInt(acValue11.getText().toString())));
                double cleanValue12 = Double.valueOf(cleanFormat(nullcheckInt(acValue12.getText().toString())));

                double cleanTotalGross =
                        (Math.abs(cleanValue1) + Math.abs(cleanValue2) + Math.abs(cleanValue3) + Math.abs(cleanValue4) + Math.abs(cleanValue5) + Math.abs(cleanValue6)
                                + Math.abs(cleanValue7) + Math.abs(cleanValue8) + Math.abs(cleanValue9) + Math.abs(cleanValue10) + Math.abs(cleanValue11) + Math.abs(cleanValue12)
                        );

                double cleanTotalNetAdj =
                        (cleanValue1 + cleanValue2 + cleanValue3 + cleanValue4 + cleanValue5 + cleanValue6
                                + cleanValue7 + cleanValue8 + cleanValue9 + cleanValue10 + cleanValue11 + cleanValue12
                        );

                acTotalGross.setText(String.valueOf(Math.round(cleanTotalGross)));

                acTotalNetAdj.setText(String.valueOf(Math.round((cleanTotalNetAdj))));

                double cleanNetAdjPrice = Double.valueOf(cleanFormat(nullcheckInt(netAdjustmentPrice)));

                double cleanAdjPriceSqm = (cleanNetAdjPrice - (cleanNetAdjPrice * (Double.valueOf(acTotalNetAdj.getText().toString()) / 100))); // / 100

                acAdjPriceSqm.setText(String.valueOf(Math.round(cleanAdjPriceSqm)));

                double cleanWeight = Double.valueOf(cleanFormat(nullcheckInt(acWeight.getText().toString())));

                double cleanWeightedValue = (cleanAdjPriceSqm * (cleanWeight)) / 100;

                acWeightedVal.setText(String.valueOf(Math.round(cleanWeightedValue)));
            }
        };
    }

    public static TextWatcher AC_VALUE11(String netAdjustmentPrice,
                                        AutoCompleteTextView acValue1,
                                        AutoCompleteTextView acValue2,
                                        AutoCompleteTextView acValue3,
                                        AutoCompleteTextView acValue4,
                                        AutoCompleteTextView acValue5,
                                        AutoCompleteTextView acValue6,
                                        AutoCompleteTextView acValue7,
                                        AutoCompleteTextView acValue8,
                                        AutoCompleteTextView acValue9,
                                        AutoCompleteTextView acValue10,
                                        AutoCompleteTextView acValue11,
                                        AutoCompleteTextView acValue12,
                                        AutoCompleteTextView acTotalGross,
                                        AutoCompleteTextView acTotalNetAdj,
                                        AutoCompleteTextView acAdjPriceSqm,
                                        AutoCompleteTextView acWeight,
                                        AutoCompleteTextView acWeightedVal) {
        return new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {

            }

            @Override
            public void afterTextChanged(Editable s) {
                acValue11.removeTextChangedListener(this);
                acValue11.setSelection(acValue11.getText().length());
                acValue11.addTextChangedListener(this);

                double cleanValue1 = Double.valueOf(cleanFormat(nullcheckInt(acValue1.getText().toString())));
                double cleanValue2 = Double.valueOf(cleanFormat(nullcheckInt(acValue2.getText().toString())));
                double cleanValue3 = Double.valueOf(cleanFormat(nullcheckInt(acValue3.getText().toString())));
                double cleanValue4 = Double.valueOf(cleanFormat(nullcheckInt(acValue4.getText().toString())));
                double cleanValue5 = Double.valueOf(cleanFormat(nullcheckInt(acValue5.getText().toString())));
                double cleanValue6 = Double.valueOf(cleanFormat(nullcheckInt(acValue6.getText().toString())));
                double cleanValue7 = Double.valueOf(cleanFormat(nullcheckInt(acValue7.getText().toString())));
                double cleanValue8 = Double.valueOf(cleanFormat(nullcheckInt(acValue8.getText().toString())));
                double cleanValue9 = Double.valueOf(cleanFormat(nullcheckInt(acValue9.getText().toString())));
                double cleanValue10 = Double.valueOf(cleanFormat(nullcheckInt(acValue10.getText().toString())));
                double cleanValue11 = Double.valueOf(cleanFormat(nullcheckInt(acValue11.getText().toString())));
                double cleanValue12 = Double.valueOf(cleanFormat(nullcheckInt(acValue12.getText().toString())));

                double cleanTotalGross =
                        (Math.abs(cleanValue1) + Math.abs(cleanValue2) + Math.abs(cleanValue3) + Math.abs(cleanValue4) + Math.abs(cleanValue5) + Math.abs(cleanValue6)
                                + Math.abs(cleanValue7) + Math.abs(cleanValue8) + Math.abs(cleanValue9) + Math.abs(cleanValue10) + Math.abs(cleanValue11) + Math.abs(cleanValue12)
                        );

                double cleanTotalNetAdj =
                        (cleanValue1 + cleanValue2 + cleanValue3 + cleanValue4 + cleanValue5 + cleanValue6
                                + cleanValue7 + cleanValue8 + cleanValue9 + cleanValue10 + cleanValue11 + cleanValue12
                        );

                acTotalGross.setText(String.valueOf(Math.round(cleanTotalGross)));

                acTotalNetAdj.setText(String.valueOf(Math.round((cleanTotalNetAdj))));

                double cleanNetAdjPrice = Double.valueOf(cleanFormat(nullcheckInt(netAdjustmentPrice)));

                double cleanAdjPriceSqm = (cleanNetAdjPrice - (cleanNetAdjPrice * (Double.valueOf(acTotalNetAdj.getText().toString()) / 100))); // / 100

                acAdjPriceSqm.setText(String.valueOf(Math.round(cleanAdjPriceSqm)));

                double cleanWeight = Double.valueOf(cleanFormat(nullcheckInt(acWeight.getText().toString())));

                double cleanWeightedValue = (cleanAdjPriceSqm * (cleanWeight)) / 100;

                acWeightedVal.setText(String.valueOf(Math.round(cleanWeightedValue)));
            }
        };
    }

    public static TextWatcher AC_VALUE12(String netAdjustmentPrice,
                                        AutoCompleteTextView acValue1,
                                        AutoCompleteTextView acValue2,
                                        AutoCompleteTextView acValue3,
                                        AutoCompleteTextView acValue4,
                                        AutoCompleteTextView acValue5,
                                        AutoCompleteTextView acValue6,
                                        AutoCompleteTextView acValue7,
                                        AutoCompleteTextView acValue8,
                                        AutoCompleteTextView acValue9,
                                        AutoCompleteTextView acValue10,
                                        AutoCompleteTextView acValue11,
                                        AutoCompleteTextView acValue12,
                                        AutoCompleteTextView acTotalGross,
                                        AutoCompleteTextView acTotalNetAdj,
                                        AutoCompleteTextView acAdjPriceSqm,
                                        AutoCompleteTextView acWeight,
                                        AutoCompleteTextView acWeightedVal) {
        return new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {

            }

            @Override
            public void afterTextChanged(Editable s) {
                acValue12.removeTextChangedListener(this);
                acValue12.setSelection(acValue12.getText().length());
                acValue12.addTextChangedListener(this);

                double cleanValue1 = Double.valueOf(cleanFormat(nullcheckInt(acValue1.getText().toString())));
                double cleanValue2 = Double.valueOf(cleanFormat(nullcheckInt(acValue2.getText().toString())));
                double cleanValue3 = Double.valueOf(cleanFormat(nullcheckInt(acValue3.getText().toString())));
                double cleanValue4 = Double.valueOf(cleanFormat(nullcheckInt(acValue4.getText().toString())));
                double cleanValue5 = Double.valueOf(cleanFormat(nullcheckInt(acValue5.getText().toString())));
                double cleanValue6 = Double.valueOf(cleanFormat(nullcheckInt(acValue6.getText().toString())));
                double cleanValue7 = Double.valueOf(cleanFormat(nullcheckInt(acValue7.getText().toString())));
                double cleanValue8 = Double.valueOf(cleanFormat(nullcheckInt(acValue8.getText().toString())));
                double cleanValue9 = Double.valueOf(cleanFormat(nullcheckInt(acValue9.getText().toString())));
                double cleanValue10 = Double.valueOf(cleanFormat(nullcheckInt(acValue10.getText().toString())));
                double cleanValue11 = Double.valueOf(cleanFormat(nullcheckInt(acValue11.getText().toString())));
                double cleanValue12 = Double.valueOf(cleanFormat(nullcheckInt(acValue12.getText().toString())));

                double cleanTotalGross =
                        (Math.abs(cleanValue1) + Math.abs(cleanValue2) + Math.abs(cleanValue3) + Math.abs(cleanValue4) + Math.abs(cleanValue5) + Math.abs(cleanValue6)
                                + Math.abs(cleanValue7) + Math.abs(cleanValue8) + Math.abs(cleanValue9) + Math.abs(cleanValue10) + Math.abs(cleanValue11) + Math.abs(cleanValue12)
                        );

                double cleanTotalNetAdj =
                        (cleanValue1 + cleanValue2 + cleanValue3 + cleanValue4 + cleanValue5 + cleanValue6
                                + cleanValue7 + cleanValue8 + cleanValue9 + cleanValue10 + cleanValue11 + cleanValue12
                        );

                acTotalGross.setText(String.valueOf(Math.round(cleanTotalGross)));

                acTotalNetAdj.setText(String.valueOf(Math.round((cleanTotalNetAdj))));

                double cleanNetAdjPrice = Double.valueOf(cleanFormat(nullcheckInt(netAdjustmentPrice)));

                double cleanAdjPriceSqm = (cleanNetAdjPrice - (cleanNetAdjPrice * (Double.valueOf(acTotalNetAdj.getText().toString()) / 100))); // / 100

                acAdjPriceSqm.setText(String.valueOf(Math.round(cleanAdjPriceSqm)));

                double cleanWeight = Double.valueOf(cleanFormat(nullcheckInt(acWeight.getText().toString())));

                double cleanWeightedValue = (cleanAdjPriceSqm * (cleanWeight)) / 100;

                acWeightedVal.setText(String.valueOf(Math.round(cleanWeightedValue)));
            }
        };
    }

    public static TextWatcher AC_WEIGHT(String netAdjustmentPrice,
                                         AutoCompleteTextView acValue1,
                                         AutoCompleteTextView acValue2,
                                         AutoCompleteTextView acValue3,
                                         AutoCompleteTextView acValue4,
                                         AutoCompleteTextView acValue5,
                                         AutoCompleteTextView acValue6,
                                         AutoCompleteTextView acValue7,
                                         AutoCompleteTextView acValue8,
                                         AutoCompleteTextView acValue9,
                                         AutoCompleteTextView acValue10,
                                         AutoCompleteTextView acValue11,
                                         AutoCompleteTextView acValue12,
                                         AutoCompleteTextView acTotalGross,
                                         AutoCompleteTextView acTotalNetAdj,
                                         AutoCompleteTextView acAdjPriceSqm,
                                         AutoCompleteTextView acWeight,
                                         AutoCompleteTextView acWeightedVal) {
        return new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {

            }

            @Override
            public void afterTextChanged(Editable s) {
                acWeight.removeTextChangedListener(this);
                acWeight.setSelection(acWeight.getText().length());
                acWeight.addTextChangedListener(this);

                double cleanValue1 = Double.valueOf(cleanFormat(nullcheckInt(acValue1.getText().toString())));
                double cleanValue2 = Double.valueOf(cleanFormat(nullcheckInt(acValue2.getText().toString())));
                double cleanValue3 = Double.valueOf(cleanFormat(nullcheckInt(acValue3.getText().toString())));
                double cleanValue4 = Double.valueOf(cleanFormat(nullcheckInt(acValue4.getText().toString())));
                double cleanValue5 = Double.valueOf(cleanFormat(nullcheckInt(acValue5.getText().toString())));
                double cleanValue6 = Double.valueOf(cleanFormat(nullcheckInt(acValue6.getText().toString())));
                double cleanValue7 = Double.valueOf(cleanFormat(nullcheckInt(acValue7.getText().toString())));
                double cleanValue8 = Double.valueOf(cleanFormat(nullcheckInt(acValue8.getText().toString())));
                double cleanValue9 = Double.valueOf(cleanFormat(nullcheckInt(acValue9.getText().toString())));
                double cleanValue10 = Double.valueOf(cleanFormat(nullcheckInt(acValue10.getText().toString())));
                double cleanValue11 = Double.valueOf(cleanFormat(nullcheckInt(acValue11.getText().toString())));
                double cleanValue12 = Double.valueOf(cleanFormat(nullcheckInt(acValue12.getText().toString())));

                double cleanTotalGross =
                        (Math.abs(cleanValue1) + Math.abs(cleanValue2) + Math.abs(cleanValue3) + Math.abs(cleanValue4) + Math.abs(cleanValue5) + Math.abs(cleanValue6)
                                + Math.abs(cleanValue7) + Math.abs(cleanValue8) + Math.abs(cleanValue9) + Math.abs(cleanValue10) + Math.abs(cleanValue11) + Math.abs(cleanValue12)
                        );

                double cleanTotalNetAdj =
                        (cleanValue1 + cleanValue2 + cleanValue3 + cleanValue4 + cleanValue5 + cleanValue6
                                + cleanValue7 + cleanValue8 + cleanValue9 + cleanValue10 + cleanValue11 + cleanValue12
                        );

                acTotalGross.setText(String.valueOf(Math.round(cleanTotalGross)));

                acTotalNetAdj.setText(String.valueOf(Math.round((cleanTotalNetAdj))));

                double cleanNetAdjPrice = Double.valueOf(cleanFormat(nullcheckInt(netAdjustmentPrice)));

                double cleanAdjPriceSqm = (cleanNetAdjPrice - (cleanNetAdjPrice * (Double.valueOf(acTotalNetAdj.getText().toString()) / 100))); // / 100

                acAdjPriceSqm.setText(String.valueOf(Math.round(cleanAdjPriceSqm)));

                double cleanWeight = Double.valueOf(cleanFormat(nullcheckInt(acWeight.getText().toString())));

                double cleanWeightedValue = (cleanAdjPriceSqm * (cleanWeight)) / 100;

                acWeightedVal.setText(String.valueOf(Math.round(cleanWeightedValue)));
            }
        };
    }

}
