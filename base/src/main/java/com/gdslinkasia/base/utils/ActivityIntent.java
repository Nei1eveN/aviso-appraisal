package com.gdslinkasia.base.utils;

public class ActivityIntent {

    public static String LOGIN_ACTIVITY = "com.gdslinkasia.login.LoginActivity";

    public static String MAIN_ACTIVITY = "com.gdslinkasia.jobs.MainActivity";

    public static String FOR_ACCEPTANCE_ACTIVITY = "com.gdslinkasia.appraisaldetails.ForAcceptanceDetailsActivity";

    public static String FOR_REWORK_ACTIVITY = "com.gdslinkasia.appraisaldetails.ForReworkDetailsActivity";

    public static String ACCEPTED_JOB_ACTIVITY = "com.gdslinkasia.appraisaldetails.ActiveJobDetails";

    public static String SCOPE_OF_WORK_ACTIVITY = "com.gdslinkasia.scope_of_work.ScopeWorkActivity";

    public static String PROPERTY_DESCRIPTION_ACTIVITY = "com.gdslinkasia.propertydescription.PropertyDescriptionActivity";

    public static String LAND_VALUATION_ACTIVITY = "com.gdslinkasia.landvaluation.LandValuationActivity";

    public static String IMPROVEMENT_VALUATION_ACTIVITY = "com.gdslinkasia.improvementvaluation.ImprovementValuationActivity";

    public static String QUALIFICATION_ACTIVITY = "com.gdslinkasia.qualification.QualificationActivity";

    public static String VALUATION_SUMMARY_ACTIVITY = "com.gdslinkasia.valuationsummary.ValuationSummaryActivity";

    public static String OTHERS_ACTIVITY = "com.gdslinkasia.others.OthersActivity";

    public static String ATTACHMENTS_ACTIVITY = "com.gdslinkasia.attachment.AttachmentsActivity";

    //TODO SPECIAL ASSUMPTIONS
    public static String SPECIAL_ASSUMPTIONS_ACTIVITY = "com.gdslinkasia.assumptions.SpecialAssumptionsActivity";
    public static String SPECIAL_ASSUMPTIONS_ITEM_ACTIVITY = "com.gdslinkasia.assumptions.SpecialAssumptionsItemActivity";

    //TODO QUALIFYING STATEMENTS
    public static String QUALIFYING_STATEMENTS_ACTIVITY = "com.gdslinkasia.qualifyingstatements.QualifyingStatementsActivity";
    public static String QUALIFYING_STATEMENT_ITEM_ACTIVITY = "com.gdslinkasia.qualifyingstatements.QualifyingStatementsItemActivity";

}
