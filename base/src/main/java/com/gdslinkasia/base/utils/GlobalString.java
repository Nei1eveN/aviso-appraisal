package com.gdslinkasia.base.utils;

import android.widget.CheckBox;

import org.json.JSONObject;

import java.text.DecimalFormat;
import java.text.NumberFormat;
import java.util.Collections;
import java.util.List;
import java.util.Locale;

import static com.gdslinkasia.base.database.DatabaseUtils.VAR_CONDO;
import static com.gdslinkasia.base.database.DatabaseUtils.VAR_LAND_IMP;
import static com.gdslinkasia.base.database.DatabaseUtils.VAR_LAND_ONLY;

public class GlobalString {


    /**
     * URL
     **/

    public static String ATT_PHP_AUTH_KEY = "QweAsdZxc";

    //TODO FETCHING ATTACHMENT PARAMETERS
    public static String TAG_ATTACHMENT = "attachments_tag";
    public static String TAG_FILE_NAME = "file_name";

    public static String[] attachmentOpenOptions = new String[] {"View", "Open Land Plotting"};

    //TODO INSERT ATTACHMENTS DATA PARAMETERS
    static String ATT_UID = "uid";
    static String ATT_FILE = "file";
    static String ATT_FILE_NAME = "file_name";
    static String ATT_APPRAISAL_TYPE = "appraisal_type";
    static String ATT_CANDIDATE_DONE = "candidate_done";

    static String ATT_ATTACHMENTS = "attachments";

    public static String ATTACHMENTS_DIRECTORY = "gds_appraisal/attachments";

    //TODO CaseCenter URL
    public static String server_type = "Dev2";//
    public static String server_certificate = "dev2.crt";
    public static String create_url = "";
    public static String server_url = "https://casecenter-dev-167.gdslinkasia.com";
    public static String server_attachment_url = "https://casecenter-dev.gdslinkasia.com:8080";
    public static String server_report_url = "http://dv-dev.gdslinkasia.com";

    public static final String records_url = server_url + "/aviso/appraisal/records.json";
    public static final String show_url = server_url + "/aviso/appraisal/records/show.json";
    public static final String update_url = server_url + "/aviso/appraisal/records/update.json";
    public static final String sign_in_url = server_url + "/users/sign_in.json";
    public static final String app_attachment_url = server_attachment_url + "/aviso_php/app_attachment.php";
    public static final String attachment_dir = server_attachment_url + "/aviso_attachments/";
    public static final String pdf_to_jpg_converter_url = server_attachment_url + "/aviso_php/pdf_to_jpg_converter.php";

//    public static String get_files_url = server_attachment_url + ":8080/pnb_php/get_all_uploaded_files.php";
//    public static String pdf_loc_url =  server_attachment_url + "/pdf_attachments/pnb_appraisal/";
//    public static String pdf_to_jpg_converter_url = server_attachment_url + "/pnb_php/pdf_to_jpg_converter.php";
//    public static String auth_token_value2 = "yfFTAW7dapdiZwxaqBus";
//    public static String auth_token_value = "zHysMgyAEcsSp4ypXpVJ";
//    public static String upload_token = "";
//    public static String attachments_tables_url = "";
//    public static String user_role_url = "";
//    public static String user_role_end_url = ".json?auth_token="+auth_token_value;
    public static String pdf_upload_url = server_attachment_url + "/aviso_php/app_upload.php";
    public static String insert_data_attachment_url = server_attachment_url+"/aviso_php/insert_data_attachment.php";
//    public static String main_report_url= server_report_url + "/LBPAppraisalWebApp/ValuationReport.aspx?url1=";
//    public static String report_url = server_report_url + "jasperserver/rest_v2/reports/reports/LBP/";


    public static String REPORT_URL_ERROR = "file:///android_asset/myerrorpage.html";

    //TODO APPRAISAL TYPES
    public static final String LAND_IMP = "land_improvements";
    public static final String LAND_ONLY = "vacant_lot";
    public static final String MOTOR_VEHICLE = "motor_vehicle";
    public static final String CONDO = "townhouse_condo";
    public static final String MACHINERY = "machineries_equipment";
    public static final String VESSEL = "vessel";
    public static final String AIRCRAFT = "aircraft";
    public static final String PPCR = "ppcr";
    public static final String INVENTORIES = "inventories";
    public static final String LAND_IMP_ME = "land_improvements_me";
    public static final String SPLIT_INTEREST = "split_interest";


    //TODO UAT URL
/*    public static String server_type = "UAT";//
    public static String server_certificate = "uat.crt";
    public static String create_url = "";
    public static String cc_url = "https://jtmas.gdslinkasia.com";
    public static String php_url = "https://jtmas.gdslinkasia.com/mobile";
    public static String records_url = cc_url + "/landbank/jtmas/records.json";
    public static String show_url = cc_url + "/landbank/jtmas/records/show.json";
    public static String update_url = cc_url + "/landbank/jtmas/records/update.json";
    public static String sign_in_url = cc_url + "/users/sign_in.json";
    public static String get_files_url = php_url + "/lbp_php/get_all_uploaded_files.php";
    public static String pdf_loc_url = php_url+"/appraisal/pdf_attachments/";
    public static String pdf_to_jpg_converter_url = php_url + "appraisal/lbp_php/pdf_to_jpg_converter.php";
//  public static String auth_token_value2 = "yfFTAW7dapdiZwxaqBus";
//  public static String auth_token_value = "zHysMgyAEcsSp4ypXpVJ";
    public static String auth_token_value = "xSELyjuwsR91wTjDsxBm";
    public static String upload_token = "";
    public static String attachments_tables_url = "";
    public static String user_role_url = "";
    public static String user_role_end_url = ".json?auth_token=" + auth_token_value;
    public static String pdf_upload_url = php_url + "/appraisal/lbp_php/upload_pdf_pnb.php";
    public static String main_report_url = "https://jtmas.gdslinkasia.com/fupload/LBPAppraisalWebApp/ValuationReport.aspx?url1=";
    public static String report_url = "http://localhost:8087/jasperserver/rest_v2/reports/reports/LBP/";*/


    /**
     * Intent
     **/
    //Intent extra from list to details
    public static String ID = "record_id";//
    public static String NAME = "record_name";
    public static String APP_MAIN_ID = "app_main_id";//
    public static String TYPE = "record_type";
    public static String UNIQUE_KEY = "unique_key";


    /**
     * DownloadAttachmentsModel
     **/
    public static String Detail_Title = "Detail_Title";
    public static String Detail_Content = "Detail_Content";


    /**
     * Buttons
     **/
    public static String UPDATE_BTN_TXT = "Save";


    /**
     * Parameters
     **/
    //Mostly used

    public static String appraiser_username = "specialist@lbpappraisal.com";
//  public String appraiser_username = "brilliant.cabria@gdslinkasia.com";

    public static final String auth_token = "auth_token";
    public static final String auth_token_value = "Fz3Mdxp6yTKfhyTnZNNU";
    public static final String query = "query";
    public static final String limit = "limit";
    public final static String system_record_id = "system.record_id";
    public final static String record = "record";
    public final static String data = "data";

    //Login
    public static String email = "user[email]";
    public static String password = "user[password]";


    //JSON QUERY (Parameters)
    static String app_simul_type = "app_simul_type";
    static String application_status = "application_status";
    static String appraisal_request_appraiser_username = "appraisal_request.appraiser_username";
    static String system_hidden = "system.hidden";
    public static String APP_JTMAS_REQUEST_TYPE = "app_jtmas_request_type";
    public static String APPRAISAL = "appraisal";
//    public static String STATUS_WHEN_SUBMITTED = "report_review";



    //JSON QUERY (values)
    public static String sub = "sub";


    //TODO application_status values
    static String REWORK_ACCEPTED = "rework_accepted";
    public final static String ACCEPTED_JOB = "accepted_job";
    public final static String FOR_REWORK = "for_rework";
    public final static String FOR_ACCEPTANCE = "appraiser_job_acceptance";
    public static String REPORT_REVIEW = "report_review";

    //SHARED PREFERENCES
    // Shared pref mode
    public static int PRIVATE_MODE = 0;

    // Shared preferences file name
    public static final String PREF_NAME = "UserLogin";
    public static final String KEY_IS_LOGGED_IN = "isLoggedIn";


    public static String type = "tls";//tls or http
    public static String server_name = "Dev2";//Dev2 or UAT or Dev167 or PROD or SEC
    public static boolean checkRoot = false;//true or false
    public static boolean checkKey = false;//true or false
    public static String defaultTimeout = "60";//mins

    private static String NULL_CHECKER(String value) {
        if (value.contentEquals("null") || value.contentEquals("") || value.contentEquals("No record found")) {
            value = "";
        }
        return value;
    }

    public static String NULL_STRING_SETTER(String value) {
        return NULL_CHECKER(value == null ? JSONObject.NULL.toString() : value);
    }

    public static <T>List<T> NULL_LIST_SETTER(List<T> value) {
        return value == null ? Collections.emptyList() : value;
    }

    public static String JOB_STATUS_SENTENCE_CHANGER(String JOB_STATUS) {
        switch (JOB_STATUS) {
            case ACCEPTED_JOB:
                return "Active";
            case FOR_REWORK:
                return "Rework";
            case FOR_ACCEPTANCE:
                return "For Acceptance";
        }
        return JOB_STATUS;
    }


    public static String APPRAISAL_TYPE_SENTENCE_CHANGER(String APPRAISAL_TYPE) {
        switch (APPRAISAL_TYPE) {
            case "land_improvements":
                APPRAISAL_TYPE = VAR_LAND_IMP;
                break;
            case "vacant_lot":
                APPRAISAL_TYPE = VAR_LAND_ONLY;
                break;
            case "condominium":
                APPRAISAL_TYPE = VAR_CONDO;
                break;
        }
        return APPRAISAL_TYPE;
    }

    public static String SENTENCE_TO_APPRAISAL_TYPE_CHANGER(String APPRAISAL_TYPE_SENTENCE) {
        switch (APPRAISAL_TYPE_SENTENCE) {
            case "Land with Improvement":
                APPRAISAL_TYPE_SENTENCE = "land_improvements";
                break;
            case "Land Only":
                APPRAISAL_TYPE_SENTENCE = "vacant_lot";
                break;
            case "Condominium":
                APPRAISAL_TYPE_SENTENCE = "condominium";
                break;
        }

        return APPRAISAL_TYPE_SENTENCE;
    }

    public static String DATE_NUMBER_TO_MONTH_CONVERSION(String DATE_NUMBER) {
        switch (DATE_NUMBER) {
            case "01":
                DATE_NUMBER = "Jan";
                break;
            case "02":
                DATE_NUMBER = "Feb";
                break;
            case "03":
                DATE_NUMBER = "Mar";
                break;
            case "04":
                DATE_NUMBER = "Apr";
                break;
            case "05":
                DATE_NUMBER = "May";
                break;
            case "06":
                DATE_NUMBER = "Jun";
                break;
            case "07":
                DATE_NUMBER = "Jul";
                break;
            case "08":
                DATE_NUMBER = "Aug";
                break;
            case "09":
                DATE_NUMBER = "Sep";
                break;
            case "10":
                DATE_NUMBER = "Oct";
                break;
            case "11":
                DATE_NUMBER = "Nov";
                break;
            case "12":
                DATE_NUMBER = "Dec";
                break;
        }
        return DATE_NUMBER;
    }

    public static String CHECKBOX_TO_STRING(CheckBox checkBox) {
        if (checkBox.isChecked()) {
            return "true";
        } else {
            return "false";
        }
    }

    public static String cleanFormat(String string) {
        return string.replaceAll(",", "");
    }

    public static String getFormattedAmountDouble(String amount) {
        DecimalFormat df2 = new DecimalFormat("###,###,###.##");

        return df2.format(Double.parseDouble(GlobalFunctions.nullcheckInt(amount.replaceAll("-", ""))));
    }

    public static String getFormattedAmount(String amount) {
        return NumberFormat.getNumberInstance(Locale.US).format(Double.parseDouble(amount));
    }
}
