package com.gdslinkasia.base.utils;

import android.annotation.SuppressLint;
import android.app.DatePickerDialog;
import android.content.Context;
import android.content.Intent;
import android.net.Uri;
import android.os.StrictMode;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.Log;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.AutoCompleteTextView;
import android.widget.CheckBox;
import android.widget.EditText;
import android.widget.MultiAutoCompleteTextView;

import com.gdslinkasia.base.R;
import com.google.android.material.textfield.TextInputEditText;
import com.google.android.material.textfield.TextInputLayout;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.File;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Calendar;
import java.util.List;
import java.util.Locale;

import static com.gdslinkasia.base.utils.GlobalString.ATT_APPRAISAL_TYPE;
import static com.gdslinkasia.base.utils.GlobalString.ATT_ATTACHMENTS;
import static com.gdslinkasia.base.utils.GlobalString.ATT_CANDIDATE_DONE;
import static com.gdslinkasia.base.utils.GlobalString.ATT_FILE;
import static com.gdslinkasia.base.utils.GlobalString.ATT_FILE_NAME;
import static com.gdslinkasia.base.utils.GlobalString.ATT_UID;
import static com.gdslinkasia.base.utils.GlobalString.REWORK_ACCEPTED;
import static com.gdslinkasia.base.utils.GlobalString.SENTENCE_TO_APPRAISAL_TYPE_CHANGER;
import static com.gdslinkasia.base.utils.GlobalString.app_simul_type;
import static com.gdslinkasia.base.utils.GlobalString.application_status;
import static com.gdslinkasia.base.utils.GlobalString.appraisal_request_appraiser_username;
import static com.gdslinkasia.base.utils.GlobalString.sub;
import static com.gdslinkasia.base.utils.GlobalString.system_hidden;
import static com.gdslinkasia.base.utils.GlobalString.system_record_id;

public class GlobalFunctions {

    public static JSONObject JOB_ACCEPTANCE_JSON_BUILDER(String appraiser_username, String job_status) throws JSONException {
        JSONObject jsonObject = new JSONObject();
        jsonObject.put(app_simul_type, sub);
        jsonObject.put(application_status, job_status);
        jsonObject.put(appraisal_request_appraiser_username, appraiser_username);
        jsonObject.put(system_hidden, false);
        return jsonObject;
    }

    public static JSONObject JOB_REWORK_JSON_BUILDER(String appraiser_username, String job_status) throws JSONException {
        JSONObject jsonObject = new JSONObject();
        jsonObject.put(app_simul_type, sub);
        jsonObject.put(application_status, job_status);
        jsonObject.put(appraisal_request_appraiser_username, appraiser_username);
        jsonObject.put(REWORK_ACCEPTED, false);
        jsonObject.put(system_hidden, false);
        return jsonObject;
    }

    public static JSONObject JOB_RECORD_DETAILS_JSON_BUILDER(String recordId) throws JSONException {
        JSONObject jsonObject = new JSONObject();
        jsonObject.put(system_record_id, recordId);
        return jsonObject;
    }

    public static JSONObject INSERT_APPRAISAL_ATTACHMENTS_JSON_BUILDER(String recordId,
                                                                       String attachmentFile,
                                                                       String attachmentFileName,
                                                                       String appraisalType) {

        JSONObject appraisalAttachmentObject = new JSONObject();
        JSONArray appAttachmentArray = new JSONArray();

        JSONObject appraisalAttachmentsDataObject = new JSONObject();

        try {
            appraisalAttachmentObject.put(ATT_UID, recordId);
            appraisalAttachmentObject.put(ATT_FILE, attachmentFile);
            appraisalAttachmentObject.put(ATT_FILE_NAME, attachmentFileName);
            appraisalAttachmentObject.put(ATT_APPRAISAL_TYPE, SENTENCE_TO_APPRAISAL_TYPE_CHANGER(appraisalType)); //+"0"
            appraisalAttachmentObject.put(ATT_CANDIDATE_DONE, "true");

            appAttachmentArray.put(appraisalAttachmentObject);

            appraisalAttachmentsDataObject.put(ATT_ATTACHMENTS, appAttachmentArray);
        } catch (JSONException e) {
            e.printStackTrace();
        }

        return appraisalAttachmentsDataObject;
    }


    public static void openFile(File url, Context context) {

        Uri uri = Uri.fromFile(url);

        Intent intent = new Intent(Intent.ACTION_VIEW);
        intent.addFlags(Intent.FLAG_GRANT_READ_URI_PERMISSION);
        StrictMode.VmPolicy.Builder builder = new StrictMode.VmPolicy.Builder();
        StrictMode.setVmPolicy(builder.build());

        if (url.toString().contains(".doc") || url.toString().contains(".docx")) {
            // Word document
            intent.setDataAndType(uri, "application/msword");
        } else if (url.toString().contains(".pdf")) {
            // PDF file
            intent.setDataAndType(uri, "application/pdf");
        } else if (url.toString().contains(".ppt") || url.toString().contains(".pptx")) {
            // Powerpoint file
            intent.setDataAndType(uri, "application/vnd.ms-powerpoint");
        } else if (url.toString().contains(".xls") || url.toString().contains(".xlsx")) {
            // Excel file
            intent.setDataAndType(uri, "application/vnd.ms-excel");
        } else if (url.toString().contains(".zip") || url.toString().contains(".rar")) {
            // WAV audio file
            intent.setDataAndType(uri, "application/x-wav");
        } else if (url.toString().contains(".rtf")) {
            // RTF file
            intent.setDataAndType(uri, "application/rtf");
        } else if (url.toString().contains(".wav") || url.toString().contains(".mp3")) {
            // WAV audio file
            intent.setDataAndType(uri, "audio/x-wav");
        } else if (url.toString().contains(".gif")) {
            // GIF file
            intent.setDataAndType(uri, "image/gif");
        } else if (url.toString().contains(".jpg") || url.toString().contains(".jpeg") || url.toString().contains(".png")) {
            // JPG file
            intent.setDataAndType(uri, "image/jpeg");
        } else if (url.toString().contains(".txt")) {
            // Text file
            intent.setDataAndType(uri, "text/plain");
        } else if (url.toString().contains(".3gp") || url.toString().contains(".mpg") || url.toString().contains(".mpeg") || url.toString().contains(".mpe") || url.toString().contains(".mp4") || url.toString().contains(".avi")) {
            // Video files
            intent.setDataAndType(uri, "video/*");
        } else {
            //if you want you can also define the intent type for any other file
            //additionally use else clause below, to manage other unknown extensions
            //in this case, Android will show all applications installed on the device
            //so you can choose which application to use
            intent.setDataAndType(uri, "*/*");
        }

        intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
        context.startActivity(intent);

    }


    /**
     * Show setError on textinputlayout parent
     *
     * @AutoCompleteTextView autoCompleteTextView - autocomplete view
     * @TextInputEditText textInputEditText - textInputEditText view
     * @EditText editText - autocomplete view
     * @setErrorText setErrorText - string value to setError
     **/
    public static void SET_ERROR_TEXTINPUTLAYOUT(AutoCompleteTextView autoCompleteTextView, String setErrorText) {
        if (autoCompleteTextView.getParent().getParent() instanceof TextInputLayout) {
            ((TextInputLayout) autoCompleteTextView.getParent().getParent()).setError(setErrorText);
        } else {
            autoCompleteTextView.setError(setErrorText);
        }
    }

    public static void SET_ERROR_TEXTINPUTLAYOUT(TextInputEditText textInputEditText, String setErrorText) {
        if (textInputEditText.getParent().getParent() instanceof TextInputLayout) {
            ((TextInputLayout) textInputEditText.getParent().getParent()).setError(setErrorText);
        } else {
            textInputEditText.setError(setErrorText);
        }
    }

    public static void SET_ERROR_TEXTINPUTLAYOUT(EditText editText, String setErrorText) {
        if (editText.getParent().getParent() instanceof TextInputLayout) {
            ((TextInputLayout) editText.getParent().getParent()).setError(setErrorText);
        } else {
            editText.setError(setErrorText);
        }
    }

    public static void SET_CONTENT_DROPDOWN(Context context, AutoCompleteTextView autoCompleteTextView,
                                            int stringArrayId, boolean isFieldDisabled) {

        autoCompleteTextView.setAdapter(GET_CONTENT_ADAPTER(autoCompleteTextView.getText().toString(),
                stringArrayId, context));
        autoCompleteTextView.setOnClickListener(v -> {
            autoCompleteTextView.showDropDown();
        });
        //is Field Disabled
        if (isFieldDisabled) {
            autoCompleteTextView.setKeyListener(null);
        }
    }

    public static void SET_CONTENT_DROPDOWN(Context context, MultiAutoCompleteTextView autoCompleteTextView,
                                            int stringArrayId, boolean isFieldDisabled) {

        autoCompleteTextView.setAdapter(GET_CONTENT_ADAPTER(autoCompleteTextView.getText().toString(),
                stringArrayId, context));
        autoCompleteTextView.setOnClickListener(v -> {
            autoCompleteTextView.showDropDown();
        });
        //is Field Disabled
        if (isFieldDisabled) {
            autoCompleteTextView.setKeyListener(null);
        }
    }

    public static ArrayAdapter<String> GET_CONTENT_ADAPTER(String value, int stringArrayId, Context context) {
        boolean hasContent = false;
        String[] contentArray = context.getResources().getStringArray(stringArrayId);
        List<String> stringList = new ArrayList<>(Arrays.asList(contentArray));
        ArrayAdapter<String> arrayAdapter;

        if (!value.isEmpty()) { //!hasContent
            Log.d("int--notEmptyContent", "Logic triggered");
            stringList.remove(value);
            stringList.add(0, value);

            arrayAdapter = new ArrayAdapter<>(context, android.R.layout.simple_list_item_1, stringList);

        } else {
            Log.d("int--emptyContent", "Logic triggered");
            arrayAdapter = new ArrayAdapter<>(context, android.R.layout.simple_list_item_1, stringList);
            // arrayAdapter = new ArrayFilterAdapter(context, android.R.layout.simple_list_item_1, stringList);
        }

        arrayAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        return arrayAdapter;

    }

    public static ArrayAdapter<String> GET_STATIC_CONTENT_ADAPTER(int stringArrayId, Context context) {
        boolean hasContent = false;
        String[] contentArray = context.getResources().getStringArray(stringArrayId);
        List<String> stringList = new ArrayList<>(Arrays.asList(contentArray));
        ArrayAdapter<String> arrayAdapter;

      /*  if (!value.isEmpty()) { //!hasContent
            Log.d("int--notEmptyContent", "Logic triggered");
            stringList.remove(value);
            stringList.add(0, value);*/

        arrayAdapter = new ArrayAdapter<>(context, android.R.layout.simple_list_item_1, stringList);

     /*   } else {
            Log.d("int--emptyContent", "Logic triggered");

            arrayAdapter = new ArrayFilterAdapter(context, android.R.layout.simple_list_item_1, stringList);
        }*/

        arrayAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        return arrayAdapter;


    }


    //TODO SIMPLE DATE FORMATS
    public static SimpleDateFormat fullFormat = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ssZ", Locale.ENGLISH);
    //    public static SimpleDateFormat monthWordFormat = new SimpleDateFormat("MMMM", Locale.ENGLISH);
    public static SimpleDateFormat monthNumberFormat = new SimpleDateFormat("MM", Locale.ENGLISH);
    public static SimpleDateFormat dayNumberFormat = new SimpleDateFormat("dd", Locale.ENGLISH);
    public static SimpleDateFormat yearNumberFormat = new SimpleDateFormat("yyyy", Locale.ENGLISH);

    //TODO ACCOUNT TITLE ADAPTER
    public static ArrayAdapter<String> GET_CLIENT_TITLE_ADAPTER(String value, Context context) {
        String[] clientTitleArray = context.getResources().getStringArray(R.array.clientTitleArray);
        List<String> strings = new ArrayList<>(Arrays.asList(clientTitleArray));
        ArrayAdapter<String> arrayAdapter;

        if ("Ms.".equals(value)) {
            strings.remove(value);
            strings.add(0, value);
            arrayAdapter = new ArrayAdapter<>(context, android.R.layout.simple_list_item_1, strings);
//                arrayAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        } else {
            arrayAdapter = new ArrayAdapter<>(context, android.R.layout.simple_list_item_1, strings);
//                    arrayAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        }
        arrayAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);

        return arrayAdapter;
    }

    //TODO CLIENT TYPE ADAPTER
    public static ArrayAdapter<String> GET_CLIENT_TYPE_ADAPTER(String value, Context context) {
        Log.d("int--intClientType", value);
        String[] clientTypeArray = context.getResources().getStringArray(R.array.clientTypeArray);
        List<String> strings = new ArrayList<>(Arrays.asList(clientTypeArray));
        Log.d("int--clientTypesLength", String.valueOf(strings.size()));
        ArrayAdapter<String> arrayAdapter;

        switch (value) {
            case "Private Individual":
            case "Local Government Unit":
            case "Corporation":
            case "Lender":
            case "Investor":
            case "BIR":
            case "SEC":
            case "Other Government Agency":
            case "Insurer":
            case "Auditor":
                strings.remove(value);
                strings.add(0, value);
                arrayAdapter = new ArrayAdapter<>(context, android.R.layout.simple_list_item_1, strings);
                arrayAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
                break;
            default:
                arrayAdapter = new ArrayAdapter<>(context, android.R.layout.simple_list_item_1, strings);
                arrayAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
                break;
        }

        return arrayAdapter;
    }

    //TODO BASIS OF VALUE ADAPTER
    public static ArrayAdapter<String> GET_BASIS_OF_VALUE_ADAPTER(String value, Context context) {
        Log.d("int--basisOfValue", value);
        String[] basisOfValueArray = context.getResources().getStringArray(R.array.basisOfValueArray);
        List<String> strings = new ArrayList<>(Arrays.asList(basisOfValueArray));
        Log.d("int--basisOfValueSize", String.valueOf(strings.size()));
        ArrayAdapter<String> arrayAdapter;

        if (value.isEmpty()) {
//            value = strings.get(0);
//            strings.remove(value);
//            strings.add(value);
            arrayAdapter = new ArrayAdapter<>(context, android.R.layout.simple_list_item_1, strings);
        } else {
            strings.remove(value);
            strings.add(0, value);
            arrayAdapter = new ArrayAdapter<>(context, android.R.layout.simple_list_item_1, strings);
        }
        arrayAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);

        return arrayAdapter;
    }

    //TODO PURPOSE APPRAISAL ADAPTER
    @SuppressLint("LongLogTag")
    public static ArrayAdapter<String> GET_PURPOSE_APPRAISAL_ADAPTER(String value, Context context, TextInputEditText etAppNatureAppraisalOther) {
        Log.d("int--purposeOfValuation", value);
        String[] clientTypeArray = context.getResources().getStringArray(R.array.purposeAppraisalArray);
        List<String> strings = new ArrayList<>(Arrays.asList(clientTypeArray));
        Log.d("int--purposeOfValuationSize", String.valueOf(strings.size()));
        ArrayAdapter<String> arrayAdapter;

        if ("Others".equals(value)) {
            strings.remove(value);
            strings.add(0, value);
            arrayAdapter = new ArrayAdapter<>(context, android.R.layout.simple_list_item_1, strings);
            etAppNatureAppraisalOther.setVisibility(View.VISIBLE);
        } else if (value.isEmpty()) {
            arrayAdapter = new ArrayAdapter<>(context, android.R.layout.simple_list_item_1, strings);
            etAppNatureAppraisalOther.setVisibility(View.GONE);
        } else {
            strings.remove(value);
            strings.add(0, value);
            arrayAdapter = new ArrayAdapter<>(context, android.R.layout.simple_list_item_1, strings);
            etAppNatureAppraisalOther.setVisibility(View.GONE);
        }
        arrayAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);

        return arrayAdapter;
    }

    //TODO SUBJECT PROPERTY TYPE ADAPTER
    public static ArrayAdapter<String> GET_ADDRESS_TYPE_ADAPTER(AutoCompleteTextView acSubjPropType, String value, Context context) {
        Log.d("int--addressType", value);
        String[] clientTypeArray = context.getResources().getStringArray(R.array.addressTypeArray);
        List<String> strings = new ArrayList<>(Arrays.asList(clientTypeArray));
        Log.d("int--addressTypeSize", String.valueOf(strings.size()));
        ArrayAdapter<String> arrayAdapter;

        if (value.isEmpty()) {
            value = strings.get(0);
            strings.remove(value);
            strings.add(0, value);
            acSubjPropType.setText(value);
        } else {
            strings.remove(value);
            strings.add(0, value);
            acSubjPropType.setText(value);
        }
        arrayAdapter = new ArrayAdapter<>(context, android.R.layout.simple_list_item_1, strings);
        arrayAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);

        return arrayAdapter;
    }

    //TODO STATIC AUTOCOMPLETE SetValue OnItemSelected on other EditText
    public static class AutoCompleteClickSetValueListener implements AdapterView.OnItemClickListener {

        AutoCompleteTextView autoCompleteTextViewDropdown;
        AutoCompleteTextView toSetOnAutoCompleteTextView;
        String[] toSetValue;

        public AutoCompleteClickSetValueListener(AutoCompleteTextView autoCompleteTextViewDropdown,
                                                 AutoCompleteTextView toSetOnAutoCompleteTextView,
                                                 String[] toSetValue
        ) {
            this.autoCompleteTextViewDropdown = autoCompleteTextViewDropdown;
            this.toSetOnAutoCompleteTextView = toSetOnAutoCompleteTextView;
            this.toSetValue = toSetValue;
        }

        @Override
        public void onItemClick(AdapterView<?> adapterView, View view, int i, long l) {
            toSetOnAutoCompleteTextView.setEnabled(true);
            toSetOnAutoCompleteTextView.setText(toSetValue[i]);
        }
    }

    //TODO STATIC AUTOCOMPLETE SetValue SIGN OnItemSelected
    public static class FactorsAutoValue implements TextWatcher {

        private AutoCompleteTextView Factors;
        private EditText FactorsValue;


        public FactorsAutoValue(
                AutoCompleteTextView Factors,
                EditText FactorsValue
        ) {

            this.Factors = Factors;
            this.FactorsValue = FactorsValue;

        }

        @Override
        public void afterTextChanged(Editable s) {


            switch (Factors.getText().toString()) {


                case "Superior":
                    double DoubleFactorsValue;
                    if (FactorsValue.getText().toString().startsWith("-") || FactorsValue.getText().toString().startsWith(".")) {

                    } else {
                        DoubleFactorsValue = Double.parseDouble(GlobalFunctions.nullcheckInt(FactorsValue.getText().toString()));
                        if (0 < DoubleFactorsValue) {
                            FactorsValue.setText("-1");
                            Log.e("FACTORS", FactorsValue.getText().toString());
                        }
                    }
                    break;

                case "Inferior":
                    if (FactorsValue.getText().toString().startsWith("-")) {
                        FactorsValue.setText("0");
                    }
                    break;

                case "Similar":

                case "Without Adjustment":
                    String c = FactorsValue.getText().toString();
                    if (!c.contentEquals("0")) {
                        FactorsValue.setText("0");
                    }
                    break;

                case "With Adjustment":
                    if (FactorsValue.getText().toString().startsWith("-")) {
                        FactorsValue.setText("1");
                    }
                    break;

            }
        }


        @Override
        public void beforeTextChanged(CharSequence s, int start, int count,
                                      int after) {

        }

        @Override
        public void onTextChanged(CharSequence s, int start, int before,
                                  int count) {

            //GlobalFunctions.checkInferior(Factors,FactorsValue);
        }
    }

    public static String nullcheckInt(String value) {

        if (value.contentEquals("  ") || value.contentEquals("") || value.contentEquals("null") || value.contentEquals(" ") || value.contentEquals("NaN")
                || value.contentEquals("∞")) {

            value = "0";
        }
        return value;
    }

    public static void onPurposeOfValuationItemClick(AutoCompleteTextView acPurposeOfValuation,
                                                     TextInputLayout txAppNatureAppraisalOther,
                                                     TextInputEditText etAppNatureAppraisalOther) {

        acPurposeOfValuation.setOnItemClickListener((adapterView, view, i, l) -> {
            String item = acPurposeOfValuation.getText().toString(); //.getAdapter().getItem(i).toString()
            Log.d("int--PurposeValuation1", item);
            if (item.contentEquals("Others")) {
                txAppNatureAppraisalOther.setVisibility(View.VISIBLE);
                etAppNatureAppraisalOther.setVisibility(View.VISIBLE);
            } else {
                txAppNatureAppraisalOther.setVisibility(View.GONE);
                etAppNatureAppraisalOther.setVisibility(View.GONE);
            }
        });


    }


    //TODO STATIC AUTOCOMPLETE TEXTVIEW FOR DROPDOWN
    public static class AutoCompleteClickListener implements AdapterView.OnItemClickListener {

        AutoCompleteTextView autoCompleteTextView;
        TextInputLayout textInputLayout;
        TextInputEditText textInputEditText;

        public AutoCompleteClickListener(AutoCompleteTextView autoCompleteTextView, TextInputLayout textInputLayout, TextInputEditText textInputEditText) {
            this.autoCompleteTextView = autoCompleteTextView;
            this.textInputLayout = textInputLayout;
            this.textInputEditText = textInputEditText;
        }

        @Override
        public void onItemClick(AdapterView<?> adapterView, View view, int i, long l) {
            //TODO [APPRAISAL DETAILS] CLIENT TYPE
            if (autoCompleteTextView.getId() == R.id.acClientType) {
                String item = autoCompleteTextView.getAdapter().getItem(i).toString();
                Log.d("int--ClientTypeSelected", item);
            } else if (autoCompleteTextView.getId() == R.id.acPurposeOfValuation) {
                String item = autoCompleteTextView.getAdapter().getItem(i).toString();
                Log.d("int--PurposeOfValuation", item);
                if (item.trim().contains("Others")) {
                    textInputLayout.setVisibility(View.VISIBLE);
                    textInputEditText.setVisibility(View.VISIBLE);
                } else {
                    textInputLayout.setVisibility(View.GONE);
                    textInputEditText.setVisibility(View.GONE);
                }
//                if (textInputLayout.getId() == R.id.txAppNatureAppraisalOther) {
//
//                }
            }
        }
    }

    //TODO BOOLEAN FOR CHECKBOX
    public static boolean getCheckboxBoolean(String value) {
        return value.equals("true");
    }

    //TODO LAYOUT VISIBILITY FROM CHECKBOX COMPANY
    public static int GET_CLIENT_NAME_VISIBILITY_FROM_COMPANY_CHECKBOX(CheckBox cbCompany) {
        if (cbCompany.isChecked()) {
            return View.GONE;
        } else {
            return View.VISIBLE;
        }
    }

    public static int GET_COMPANY_VISIBILITY_FROM_COMPANY_CHECKBOX(CheckBox cbCompany) {
        if (cbCompany.isChecked()) {
            return View.VISIBLE;
        } else {
            return View.GONE;
        }
    }


    //TODO LAYOUT VISIBILITY FOR CLIENT AND COMPANY LAYOUT
    public static void ONCHANGE_VISIBILITY_COMPANY_CHECKBOX(CheckBox cbCompany, View clientNameContainer, View companyContainer) {
        cbCompany.setOnCheckedChangeListener((buttonView, isChecked) -> {
            if (isChecked) {
                clientNameContainer.setVisibility(View.GONE);
                companyContainer.setVisibility(View.VISIBLE);
            } else {
                companyContainer.setVisibility(View.GONE);
                clientNameContainer.setVisibility(View.VISIBLE);
            }
        });
    }

    //TODO LAYOUT VISIBILITY FROM CHECKBOX
    public static int GET_IS_CHECKED_FROM_CHECKBOX_VISIBILITY(CheckBox checkBox) {
        if (checkBox.isChecked()) {
            return View.VISIBLE;
        } else {
            return View.GONE;
        }
    }

    //TODO LAYOUT VISIBILITY FROM CHECKBOX
    public static void ONCHANGE_IS_CHECKED_CHECKBOX_VISIBILITY(CheckBox checkBox, View container) {

        checkBox.setOnCheckedChangeListener((buttonView, isChecked) -> {
            if (isChecked) {
                container.setVisibility(View.VISIBLE);
            } else {
                container.setVisibility(View.GONE);
            }
        });

    }

    //TODO DATEPICKERDIALOG FOR INSPECT AND VALUATION DATE
    public static DatePickerDialog setDialogDate(Context context,
                                                 int year, int month, int dayOfMonth,
                                                 TextInputEditText yearView,
                                                 TextInputEditText monthView,
                                                 TextInputEditText dayOfMonthView) {

        DatePickerDialog.OnDateSetListener listener = (view, year1, month1, dayOfMonth1) -> {
            yearView.setText(String.valueOf(year1));
            monthView.setText(String.valueOf(month1 + 1));
            dayOfMonthView.setText(String.valueOf(dayOfMonth1));
        };
        return new DatePickerDialog(context, listener, year, month, dayOfMonth);
    }

    //TODO INTEGER FOR YEAR, MONTH, DAY OF MONTH
    public static int getInspectedOrValuationYear(int year) {
        if (year == 0) {
            year = Calendar.getInstance().get(Calendar.YEAR);
        }
        return year;
    }

    public static int getInspectedOrValuationMonth(int month) {
        if (month == 0) {
            month = Calendar.getInstance().get(Calendar.MONTH); //-1
        } else if (String.valueOf(month).equals("----")) {
            month = Calendar.getInstance().get(Calendar.MONTH); //-1
        } else {
            month = month - 1;
        }
        return month;
    }

    public static int getInspectedOrValuationDayOfMonth(int dayOfMonth) {
        if (dayOfMonth == 0) {
            dayOfMonth = Calendar.getInstance().get(Calendar.DAY_OF_MONTH);
        }
        return dayOfMonth;
    }

//    public static ArrayAdapter<String> monthAdapter(Context context, String monthText) {
//        String[] monthArray = context.getResources().getStringArray(R.array.monthType);
//        List<String> strings = new ArrayList<>(Arrays.asList(monthArray));
//        ArrayAdapter<String> arrayAdapter;
//
//        Log.d("int--monthText", monthText);
//
//        if (!monthText.isEmpty()) {
//            monthText = monthSelected(monthText);
//            Log.d("int--monthText2", monthText);
//            strings.remove(monthText);
//            strings.add(0, monthText);
//
//            arrayAdapter = new ArrayAdapter<>(context, android.R.layout.simple_list_item_1, strings);
//        } else {
//            arrayAdapter = new ArrayAdapter<>(context, android.R.layout.simple_list_item_1, strings);
//        }
//
//        arrayAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
//
//        return arrayAdapter;
//    }

    public static String monthSelected(String monthValue) {
        switch (monthValue) {
            case "1":
                return "January";
            case "01":
                return "January";
            case "2":
                return "February";
            case "02":
                return "February";
            case "3":
                return "March";
            case "03":
                return "March";
            case "4":
                return "April";
            case "04":
                return "April";
            case "5":
                return "May";
            case "05":
                return "May";
            case "6":
                return "June";
            case "06":
                return "June";
            case "7":
                return "July";
            case "07":
                return "July";
            case "8":
                return "August";
            case "08":
                return "August";
            case "9":
                return "September";
            case "09":
                return "September";
            case "10":
                return "October";
            case "11":
                return "November";
            case "12":
                return "December";
            default:
                return "----";
        }
    }

    public static String monthConversionFromLetters(String value) {
        switch (value) {
            case "January":
                value = "01";
                break;
            case "February":
                value = "02";
                break;
            case "March":
                value = "03";
                break;
            case "April":
                value = "04";
                break;
            case "May":
                value = "05";
                break;
            case "June":
                value = "06";
                break;
            case "July":
                value = "07";
                break;
            case "August":
                value = "08";
                break;
            case "September":
                value = "09";
                break;
            case "October":
                value = "10";
                break;
            case "November":
                value = "11";
                break;
            case "December":
                value = "12";
                break;
            default:
                value = "01";
                break;

        }
        return value;
    }

    public static ArrayAdapter<String> titleTypeAdapter(Context context, String value, AutoCompleteTextView acTitleType) {
        String[] array = context.getResources().getStringArray(R.array.titleType);
        List<String> strings = new ArrayList<>(Arrays.asList(array));
        ArrayAdapter<String> arrayAdapter;

        if (!value.isEmpty()) {
            Log.d("int--monthText2", value);
            strings.remove(value);
            strings.add(0, value);

            acTitleType.setText(value);

            arrayAdapter = new ArrayAdapter<>(context, android.R.layout.simple_list_item_1, strings);
        } else {
            arrayAdapter = new ArrayAdapter<>(context, android.R.layout.simple_list_item_1, strings);
        }

        arrayAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);

        return arrayAdapter;
    }

    public static ArrayAdapter<String> descriptionTypeAdapter(Context context) {
        String[] array = context.getResources().getStringArray(R.array.descriptionType);
        List<String> strings = new ArrayList<>(Arrays.asList(array));
        ArrayAdapter<String> arrayAdapter = new ArrayAdapter<>(context, android.R.layout.simple_list_item_1, strings);
        arrayAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);

        return arrayAdapter;
    }

    public static ArrayAdapter<String> registryDeedType(Context context, String value) {
        String[] array = context.getResources().getStringArray(R.array.registryOfDeedsType);
        List<String> strings = new ArrayList<>(Arrays.asList(array));
        ArrayAdapter<String> arrayAdapter;

        if (!value.isEmpty()) {
            Log.d("int--registryDeedValue", value);
            strings.remove(value);
            strings.add(0, value);

            arrayAdapter = new ArrayAdapter<>(context, android.R.layout.simple_list_item_1, strings);
        } else {
            arrayAdapter = new ArrayAdapter<>(context, android.R.layout.simple_list_item_1, strings);
        }

        arrayAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);

        return arrayAdapter;
    }

    public static ArrayAdapter<String> ownershipClassificationType(Context context, String value) {
        String[] array = context.getResources().getStringArray(R.array.ownershipClassificationType);
        List<String> strings = new ArrayList<>(Arrays.asList(array));
        ArrayAdapter<String> arrayAdapter;

        if (!value.isEmpty()) {
            Log.d("int--ownershipClassType", value);
            strings.remove(value);
            strings.add(0, value);

            arrayAdapter = new ArrayAdapter<>(context, android.R.layout.simple_list_item_1, strings);
        } else {
            arrayAdapter = new ArrayAdapter<>(context, android.R.layout.simple_list_item_1, strings);
        }

        arrayAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);

        return arrayAdapter;
    }

    public static ArrayAdapter<String> zoningType(Context context, String value) {
        String[] array = context.getResources().getStringArray(R.array.zoningType);
        List<String> strings = new ArrayList<>(Arrays.asList(array));
        ArrayAdapter<String> arrayAdapter;

        if (!value.isEmpty()) {
            Log.d("int--zoningType", value);
            strings.remove(value);
            strings.add(0, value);

            arrayAdapter = new ArrayAdapter<>(context, android.R.layout.simple_list_item_1, strings);
        } else {
            arrayAdapter = new ArrayAdapter<>(context, android.R.layout.simple_list_item_1, strings);
        }

        arrayAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);

        return arrayAdapter;
    }

    public static ArrayAdapter<String> pricingCircumstanceType(Context context, String value) {
        String[] array = context.getResources().getStringArray(R.array.pricingCircumstanceType);
        List<String> strings = new ArrayList<>(Arrays.asList(array));
        ArrayAdapter<String> arrayAdapter;

        if (!value.isEmpty()) {
            Log.d("int--pricingCircType", value);
            strings.remove(value);
            strings.add(0, value);

            arrayAdapter = new ArrayAdapter<>(context, android.R.layout.simple_list_item_1, strings);
        } else {
            arrayAdapter = new ArrayAdapter<>(context, android.R.layout.simple_list_item_1, strings);
        }

        arrayAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);

        return arrayAdapter;
    }

    public static ArrayAdapter<String> comparativeType(Context context, String value) {
        String[] array = context.getResources().getStringArray(R.array.comparativeType);
        List<String> strings = new ArrayList<>(Arrays.asList(array));
        ArrayAdapter<String> arrayAdapter;

        if (!value.isEmpty()) {
            Log.d("int--comparativeType", value);
            strings.remove(value);
            strings.add(0, value);

            arrayAdapter = new ArrayAdapter<>(context, android.R.layout.simple_list_item_1, strings);
        } else {
            arrayAdapter = new ArrayAdapter<>(context, android.R.layout.simple_list_item_1, strings);
        }

        arrayAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);

        return arrayAdapter;
    }

    //TODO SET DATE PICKER DIALOG WITH AUTOCOMPLETE TEXTVIEW
    public static DatePickerDialog setDialogDateWithAutoCompleteTextView(Context context,
                                                                         int year, int month, int dayOfMonth,
                                                                         TextInputEditText yearView,
                                                                         AutoCompleteTextView monthView,
                                                                         TextInputEditText dayOfMonthView) {

        DatePickerDialog.OnDateSetListener listener = (view, year1, month1, dayOfMonth1) -> {
            yearView.setText(String.valueOf(year1));
            monthView.setText(monthSelected(String.valueOf(month1 + 1)));
            dayOfMonthView.setText(String.valueOf(dayOfMonth1));
        };
        return new DatePickerDialog(context, listener, year, month, dayOfMonth);
    }

    public static boolean containsWordMonth(AutoCompleteTextView acDateRegMonth, Context context) {
        String[] array = context.getResources().getStringArray(R.array.monthType);
        List<String> strings = new ArrayList<>(Arrays.asList(array));

        String value = acDateRegMonth.getText().toString();
        return strings.contains(value);
    }

    public static int convertMonthWordToNumber(String monthWord, Context context) {
        String[] array = context.getResources().getStringArray(R.array.monthType);
        List<String> strings = new ArrayList<>(Arrays.asList(array));

//        switch (monthWord) {
//            case "January":
//                return Calendar.getInstance().get(Calendar.JANUARY);
//            case "February":
//                return Calendar.getInstance().get(Calendar.FEBRUARY);
//            case "March":
//                return Calendar.getInstance().get(Calendar.MARCH);
//            case "April":
//                return Calendar.getInstance().get(Calendar.APRIL);
//                default:
//                    return Calendar.getInstance().get(Calendar.MONTH);
//
//        }

        return strings.indexOf(monthWord);

    }

}
