package com.gdslinkasia.base;

import android.annotation.SuppressLint;
import android.app.Activity;
import android.util.Log;

import androidx.appcompat.app.AlertDialog;

import com.microsoft.appcenter.distribute.Distribute;
import com.microsoft.appcenter.distribute.DistributeListener;
import com.microsoft.appcenter.distribute.ReleaseDetails;
import com.microsoft.appcenter.distribute.UpdateAction;

public class AppDistributeListener implements DistributeListener {

    @SuppressLint("LongLogTag")
    @Override
    public boolean onReleaseAvailable(Activity activity, ReleaseDetails releaseDetails) {
        Log.d("int--onReleaseAvailable", "Logic triggered");
        // Look at releaseDetails public methods to get version information, release notes text or release notes URL
        String versionName = releaseDetails.getShortVersion();
        int versionCode = releaseDetails.getVersion();
        String releaseNotes = releaseDetails.getReleaseNotes();
//        Uri releaseNotesUrl = releaseDetails.getReleaseNotesUrl();

        Log.d("int--onReleaseVersionName", versionName);

        //TODO Build our own dialog title and message
        AlertDialog.Builder dialogBuilder = new AlertDialog.Builder(activity);
        dialogBuilder.setTitle("Version " + versionCode + "." + versionName + " Available"); // you should use a string resource instead of course, this is just to simplify example
        dialogBuilder.setMessage(releaseNotes);

        //TODO Mimic default SDK buttons
        dialogBuilder.setPositiveButton(com.microsoft.appcenter.distribute.R.string.appcenter_distribute_update_dialog_download,
                (dialog, which) -> {

                    // This method is used to tell the SDK what button was clicked
                    Distribute.notifyUpdateAction(UpdateAction.UPDATE);
                });

        dialogBuilder.setCancelable(false); // if it's cancelable you should map cancel to postpone, but only for optional updates
        dialogBuilder.create().show();

        // TODO Return true if you are using your own dialog, false otherwise
        return true;
//        return false;
    }
}
