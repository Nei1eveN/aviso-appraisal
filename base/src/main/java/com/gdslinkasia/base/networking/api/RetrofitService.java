package com.gdslinkasia.base.networking.api;

import com.gdslinkasia.base.model.job.JobRecordResult;
import com.gdslinkasia.base.model.job.JobSubmitResult;
import com.gdslinkasia.base.model.job.Record;

import io.reactivex.Flowable;
import io.reactivex.Single;
import retrofit2.http.Field;
import retrofit2.http.FormUrlEncoded;
import retrofit2.http.POST;
import retrofit2.http.Query;

public interface RetrofitService {

//    @POST("/aviso/appraisal/records.json")
//    Observable<JobRecordResult> getJobs(@Query("auth_token") String AUTH_TOKEN_VALUE,
//                                              @Query("query") String JSON_TO_STRING,
//                                              @Query("limit") String limit);

    @POST("/aviso/appraisal/records.json")
    Single<JobRecordResult> getJobs(@Query("auth_token") String AUTH_TOKEN_VALUE,
                                    @Query("query") String JSON_TO_STRING,
                                    @Query("limit") String limit);

    @POST("aviso/appraisal/records/show.json")
    Single<Record> getJobRecordById(@Query("auth_token") String AUTH_TOKEN_VALUE,
                                    @Query("query") String JSON_TO_STRING);

    @FormUrlEncoded
//    @Multipart
//    @Headers("Content-Type: multipart/form-data")
    @POST("/aviso/appraisal/records/update.json")
    Flowable<JobSubmitResult> saveToServer(@Query("auth_token") String AUTH_TOKEN_VALUE,
                                           @Query("query") String JSON_TO_STRING,
                                           @Field("data") String MODEL_TO_JSON_STRING);
}