package com.gdslinkasia.base.networking.api;

import com.gdslinkasia.base.networking.ConnectionTimeOut;

import retrofit2.Retrofit;
import retrofit2.adapter.rxjava2.RxJava2CallAdapterFactory;
import retrofit2.converter.gson.GsonConverterFactory;

public class RetrofitClient {

    public static Retrofit getApiClient(String URL) {
        return new Retrofit.Builder()
                .baseUrl(URL)
                .client(ConnectionTimeOut.SET_TIMEOUT())
                .addConverterFactory(GsonConverterFactory.create())
                .addCallAdapterFactory(RxJava2CallAdapterFactory.create())
                .build();
    }
}
