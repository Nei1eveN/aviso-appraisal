package com.gdslinkasia.base.networking;

import java.util.concurrent.TimeUnit;

import okhttp3.OkHttpClient;

public class ConnectionTimeOut {

    public static OkHttpClient SET_TIMEOUT() {
        return new OkHttpClient.Builder()
                .connectTimeout(60, TimeUnit.SECONDS)
                .readTimeout(60, TimeUnit.SECONDS)
                .writeTimeout(60, TimeUnit.SECONDS)
                .build();
    }

}
