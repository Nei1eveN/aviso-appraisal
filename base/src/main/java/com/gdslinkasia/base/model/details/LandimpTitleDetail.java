
package com.gdslinkasia.base.model.details;

import androidx.room.ColumnInfo;
import androidx.room.Entity;
import androidx.room.ForeignKey;
import androidx.room.PrimaryKey;

import com.google.gson.annotations.SerializedName;

import static androidx.room.ForeignKey.CASCADE;
import static com.gdslinkasia.base.database.DatabaseUtils.APPRAISAL_REQUEST_ID;
import static com.gdslinkasia.base.database.DatabaseUtils.UNIQUE_ID;
import static com.gdslinkasia.base.utils.GlobalString.NULL_STRING_SETTER;

@SuppressWarnings("unused")
@Entity(foreignKeys = {
        @ForeignKey(entity = AppraisalRequest.class,
                parentColumns = UNIQUE_ID,
                childColumns = APPRAISAL_REQUEST_ID,
                onDelete = CASCADE)})
public class LandimpTitleDetail {

    //TODO NON-SERIALIZED
//    @Expose(serialize = false, deserialize = false)
    @PrimaryKey(autoGenerate = true)
    @ColumnInfo(name = UNIQUE_ID)
    private transient long uniqueId;

//    @Expose(serialize = false, deserialize = false)
    @ColumnInfo(name = APPRAISAL_REQUEST_ID, index = true)
    private transient long appraisalRequestId;

    @SerializedName("landimp_title_details_area")
    private String landimpTitleDetailsArea;
    @SerializedName("landimp_title_details_block")
    private String landimpTitleDetailsBlock;
    @SerializedName("landimp_title_details_date_registry_day")
    private String landimpTitleDetailsDateRegistryDay;
    @SerializedName("landimp_title_details_date_registry_month")
    private String landimpTitleDetailsDateRegistryMonth;
    @SerializedName("landimp_title_details_date_registry_year")
    private String landimpTitleDetailsDateRegistryYear;
    @SerializedName("landimp_title_details_index")
    private String landimpTitleDetailsIndex;
    @SerializedName("landimp_title_details_lot_no")
    private String landimpTitleDetailsLotNo;
    @SerializedName("landimp_title_details_reg_deeds")
    private String landimpTitleDetailsRegDeeds;
    @SerializedName("landimp_title_details_reg_owner")
    private String landimpTitleDetailsRegOwner;
    @SerializedName("landimp_title_details_survey")
    private String landimpTitleDetailsSurvey;
    @SerializedName("landimp_title_details_tct")
    private String landimpTitleDetailsTct;
    @SerializedName("landimp_title_details_title_type")
    private String landimpTitleDetailsTitleType;

    public long getUniqueId() {
        return uniqueId;
    }

    public void setUniqueId(long uniqueId) {
        this.uniqueId = uniqueId;
    }

    public long getAppraisalRequestId() {
        return appraisalRequestId;
    }

    public void setAppraisalRequestId(long appraisalRequestId) {
        this.appraisalRequestId = appraisalRequestId;
    }

    public String getLandimpTitleDetailsArea() {
        return NULL_STRING_SETTER(landimpTitleDetailsArea);
    }

    public void setLandimpTitleDetailsArea(String landimpTitleDetailsArea) {
        this.landimpTitleDetailsArea = landimpTitleDetailsArea;
    }

    public String getLandimpTitleDetailsBlock() {
        return NULL_STRING_SETTER(landimpTitleDetailsBlock);
    }

    public void setLandimpTitleDetailsBlock(String landimpTitleDetailsBlock) {
        this.landimpTitleDetailsBlock = landimpTitleDetailsBlock;
    }

    public String getLandimpTitleDetailsDateRegistryDay() {
        return NULL_STRING_SETTER(landimpTitleDetailsDateRegistryDay);
    }

    public void setLandimpTitleDetailsDateRegistryDay(String landimpTitleDetailsDateRegistryDay) {
        this.landimpTitleDetailsDateRegistryDay = landimpTitleDetailsDateRegistryDay;
    }

    public String getLandimpTitleDetailsDateRegistryMonth() {
        return NULL_STRING_SETTER(landimpTitleDetailsDateRegistryMonth);
    }

    public void setLandimpTitleDetailsDateRegistryMonth(String landimpTitleDetailsDateRegistryMonth) {
        this.landimpTitleDetailsDateRegistryMonth = landimpTitleDetailsDateRegistryMonth;
    }

    public String getLandimpTitleDetailsDateRegistryYear() {
        return NULL_STRING_SETTER(landimpTitleDetailsDateRegistryYear);
    }

    public void setLandimpTitleDetailsDateRegistryYear(String landimpTitleDetailsDateRegistryYear) {
        this.landimpTitleDetailsDateRegistryYear = landimpTitleDetailsDateRegistryYear;
    }

    public String getLandimpTitleDetailsIndex() {
        return NULL_STRING_SETTER(landimpTitleDetailsIndex);
    }

    public void setLandimpTitleDetailsIndex(String landimpTitleDetailsIndex) {
        this.landimpTitleDetailsIndex = landimpTitleDetailsIndex;
    }

    public String getLandimpTitleDetailsLotNo() {
        return NULL_STRING_SETTER(landimpTitleDetailsLotNo);
    }

    public void setLandimpTitleDetailsLotNo(String landimpTitleDetailsLotNo) {
        this.landimpTitleDetailsLotNo = landimpTitleDetailsLotNo;
    }

    public String getLandimpTitleDetailsRegDeeds() {
        return NULL_STRING_SETTER(landimpTitleDetailsRegDeeds);
    }

    public void setLandimpTitleDetailsRegDeeds(String landimpTitleDetailsRegDeeds) {
        this.landimpTitleDetailsRegDeeds = landimpTitleDetailsRegDeeds;
    }

    public String getLandimpTitleDetailsRegOwner() {
        return NULL_STRING_SETTER(landimpTitleDetailsRegOwner);
    }

    public void setLandimpTitleDetailsRegOwner(String landimpTitleDetailsRegOwner) {
        this.landimpTitleDetailsRegOwner = landimpTitleDetailsRegOwner;
    }

    public String getLandimpTitleDetailsSurvey() {
        return NULL_STRING_SETTER(landimpTitleDetailsSurvey);
    }

    public void setLandimpTitleDetailsSurvey(String landimpTitleDetailsSurvey) {
        this.landimpTitleDetailsSurvey = landimpTitleDetailsSurvey;
    }

    public String getLandimpTitleDetailsTct() {
        return NULL_STRING_SETTER(landimpTitleDetailsTct);
    }

    public void setLandimpTitleDetailsTct(String landimpTitleDetailsTct) {
        this.landimpTitleDetailsTct = landimpTitleDetailsTct;
    }

    public String getLandimpTitleDetailsTitleType() {
        return NULL_STRING_SETTER(landimpTitleDetailsTitleType);
    }

    public void setLandimpTitleDetailsTitleType(String landimpTitleDetailsTitleType) {
        this.landimpTitleDetailsTitleType = landimpTitleDetailsTitleType;
    }

}
