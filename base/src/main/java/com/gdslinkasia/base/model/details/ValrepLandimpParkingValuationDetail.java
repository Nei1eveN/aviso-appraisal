
package com.gdslinkasia.base.model.details;

import androidx.room.ColumnInfo;
import androidx.room.Entity;
import androidx.room.ForeignKey;
import androidx.room.PrimaryKey;

import com.gdslinkasia.base.model.job.Record;
import com.google.gson.annotations.SerializedName;

import static androidx.room.ForeignKey.CASCADE;
import static com.gdslinkasia.base.database.DatabaseUtils.RECORD_ID;
import static com.gdslinkasia.base.database.DatabaseUtils.RECORD_UID;
import static com.gdslinkasia.base.database.DatabaseUtils.UNIQUE_ID;
import static com.gdslinkasia.base.utils.GlobalString.NULL_STRING_SETTER;


@SuppressWarnings("all")
@Entity(foreignKeys = {@ForeignKey(
        entity = Record.class,
        parentColumns = UNIQUE_ID,
        childColumns = RECORD_UID,
        onDelete = CASCADE)})
public class ValrepLandimpParkingValuationDetail {

//    @Expose(serialize = false, deserialize = false)
    @PrimaryKey(autoGenerate = true)
    @ColumnInfo(name = UNIQUE_ID)
    private transient long uniqueId;

    //TODO NON-SERIALIZED
//    @Expose(serialize = false, deserialize = false)
    @ColumnInfo(name = RECORD_ID, index = true)
    private transient String recordId;

//    @Expose(serialize = false, deserialize = false)
    @ColumnInfo(name = RECORD_UID, index = true)
    private transient long recordUId;

    @SerializedName("valrep_landimp_value_parking_no")
    private String valrepLandimpValueParkingNo;
    @SerializedName("valrep_landimp_value_parkinglot_area")
    private String valrepLandimpValueParkinglotArea;
    @SerializedName("valrep_landimp_value_parkinglot_value")
    private String valrepLandimpValueParkinglotValue;

    public long getUniqueId() {
        return uniqueId;
    }

    public void setUniqueId(long uniqueId) {
        this.uniqueId = uniqueId;
    }

    public String getRecordId() {
        return recordId;
    }

    public void setRecordId(String recordId) {
        this.recordId = recordId;
    }

    public long getRecordUId() {
        return recordUId;
    }

    public void setRecordUId(long recordUId) {
        this.recordUId = recordUId;
    }

    public String getValrepLandimpValueParkingNo() {
        return NULL_STRING_SETTER(valrepLandimpValueParkingNo);
    }

    public void setValrepLandimpValueParkingNo(String valrepLandimpValueParkingNo) {
        this.valrepLandimpValueParkingNo = valrepLandimpValueParkingNo;
    }

    public String getValrepLandimpValueParkinglotArea() {
        return NULL_STRING_SETTER(valrepLandimpValueParkinglotArea);
    }

    public void setValrepLandimpValueParkinglotArea(String valrepLandimpValueParkinglotArea) {
        this.valrepLandimpValueParkinglotArea = valrepLandimpValueParkinglotArea;
    }

    public String getValrepLandimpValueParkinglotValue() {
        return NULL_STRING_SETTER(valrepLandimpValueParkinglotValue);
    }

    public void setValrepLandimpValueParkinglotValue(String valrepLandimpValueParkinglotValue) {
        this.valrepLandimpValueParkinglotValue = valrepLandimpValueParkinglotValue;
    }

}
