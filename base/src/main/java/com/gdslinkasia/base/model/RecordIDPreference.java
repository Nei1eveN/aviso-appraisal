package com.gdslinkasia.base.model;

import androidx.annotation.NonNull;
import androidx.room.ColumnInfo;
import androidx.room.Entity;
import androidx.room.PrimaryKey;

import static com.gdslinkasia.base.database.DatabaseUtils.RECORD_ID;
import static com.gdslinkasia.base.database.DatabaseUtils.RECORD_PREFERENCE_TABLE;


@Entity(tableName = RECORD_PREFERENCE_TABLE)
public class RecordIDPreference {

    @PrimaryKey
    @ColumnInfo(name = RECORD_ID)
    private @NonNull String recordId;

    @NonNull
    public String getRecordId() {
        return recordId;
    }

    public void setRecordId(@NonNull String recordId) {
        this.recordId = recordId;
    }
}
