
package com.gdslinkasia.base.model.details;

import androidx.room.ColumnInfo;
import androidx.room.Entity;
import androidx.room.ForeignKey;
import androidx.room.PrimaryKey;
import androidx.room.TypeConverters;

import com.gdslinkasia.base.database.converters.AppAttachmentTypeConverter;
import com.gdslinkasia.base.database.converters.AppCollateralAddressTypeConverter;
import com.gdslinkasia.base.database.converters.AppContactPersonTypeConverter;
import com.gdslinkasia.base.database.converters.CondoTitleDetailTypeConverter;
import com.gdslinkasia.base.database.converters.LandimpImpDetailTypeConverter;
import com.gdslinkasia.base.database.converters.LandimpTitleDetailTypeConverter;
import com.gdslinkasia.base.model.job.Record;
import com.google.gson.annotations.SerializedName;

import java.util.List;

import static androidx.room.ForeignKey.CASCADE;
import static com.gdslinkasia.base.database.DatabaseUtils.RECORD_ID;
import static com.gdslinkasia.base.database.DatabaseUtils.RECORD_UID;
import static com.gdslinkasia.base.database.DatabaseUtils.UNIQUE_ID;
import static com.gdslinkasia.base.utils.GlobalString.NULL_LIST_SETTER;
import static com.gdslinkasia.base.utils.GlobalString.NULL_STRING_SETTER;

@SuppressWarnings("all")
@Entity(foreignKeys = {
        @ForeignKey(entity = Record.class,
                parentColumns = UNIQUE_ID,
                childColumns = RECORD_UID,
                onDelete = CASCADE, onUpdate = CASCADE)}) //, deferred = true
public class AppraisalRequest {

//    @Expose(serialize = false, deserialize = false)
    @PrimaryKey(autoGenerate = true)
    @ColumnInfo(name = UNIQUE_ID)
    private transient long uniqueId;

//    @Expose(serialize = false, deserialize = false)
    @ColumnInfo(name = RECORD_ID, index = true)
    private transient String recordId;

    //TODO NON-SERIALIZED
//    @Expose(serialize = false, deserialize = false)
    @ColumnInfo(name = RECORD_UID, index = true)
    private transient long recordUId;

    @SerializedName("address")
    private String address;
    @SerializedName("app_amount")
    private String appAmount;
    @SerializedName("app_application_reference")
    private String appApplicationReference;
    @SerializedName("app_appraisal_payment")
    private String appAppraisalPayment;
    @SerializedName("app_appraisal_procedure")
    private String appAppraisalProcedure;
    @SerializedName("app_appraiser_name")
    private String appAppraiserName;
    @SerializedName("app_approved_by")
    private String appApprovedBy;

    @TypeConverters(AppAttachmentTypeConverter.class)
    @SerializedName("app_attachment")
    private List<AppAttachment> appAttachment;

    @SerializedName("app_car_body_type")
    private String appCarBodyType;
    @SerializedName("app_car_chassis_no")
    private String appCarChassisNo;
    @SerializedName("app_car_color")
    private String appCarColor;
    @SerializedName("app_car_cr_date_day")
    private String appCarCrDateDay;
    @SerializedName("app_car_cr_date_month")
    private String appCarCrDateMonth;
    @SerializedName("app_car_cr_date_year")
    private String appCarCrDateYear;
    @SerializedName("app_car_cr_no")
    private String appCarCrNo;
    @SerializedName("app_car_displacement")
    private String appCarDisplacement;
    @SerializedName("app_car_inspection_time")
    private String appCarInspectionTime;
    @SerializedName("app_car_loan_dept")
    private String appCarLoanDept;
    @SerializedName("app_car_loan_purpose")
    private String appCarLoanPurpose;
    @SerializedName("app_car_mileage")
    private String appCarMileage;
    @SerializedName("app_car_model")
    private String appCarModel;
    @SerializedName("app_car_motor_no")
    private String appCarMotorNo;
    @SerializedName("app_car_odometer_reading")
    private String appCarOdometerReading;
    @SerializedName("app_car_plate_no")
    private String appCarPlateNo;
    @SerializedName("app_car_series")
    private String appCarSeries;
    @SerializedName("app_car_submission_time")
    private String appCarSubmissionTime;
    @SerializedName("app_car_vin")
    private String appCarVin;
    @SerializedName("app_car_year")
    private String appCarYear;
    @SerializedName("app_ciad_unit")
    private String appCiadUnit;

    @TypeConverters(AppCollateralAddressTypeConverter.class)
    @SerializedName("app_collateral_address")
    private List<AppCollateralAddress> appCollateralAddress;

    @TypeConverters(AppContactPersonTypeConverter.class)
    @SerializedName("app_contact_person")
    private List<AppContactPerson> appContactPerson;

    @SerializedName("app_control_no")
    private String appControlNo;
    @SerializedName("app_cost_center")
    private String appCostCenter;
    @SerializedName("app_division")
    private String appDivision;
    @SerializedName("app_gen_attachment1")
    private String appGenAttachment1;
    @SerializedName("app_gen_attachment2")
    private String appGenAttachment2;
    @SerializedName("app_gen_attachment3")
    private String appGenAttachment3;
    @SerializedName("app_gen_attachment4")
    private String appGenAttachment4;
    @SerializedName("app_gen_attachment5")
    private String appGenAttachment5;
    @SerializedName("app_gen_attachment6")
    private String appGenAttachment6;
    @SerializedName("app_has_tvr")
    private String appHasTvr;
    @SerializedName("app_me_age")
    private String appMeAge;
    @SerializedName("app_me_condition")
    private String appMeCondition;
    @SerializedName("app_me_manufacturer")
    private String appMeManufacturer;
    @SerializedName("app_me_model")
    private String appMeModel;
    @SerializedName("app_me_quantity")
    private String appMeQuantity;
    @SerializedName("app_me_serial_no")
    private String appMeSerialNo;
    @SerializedName("app_me_type")
    private String appMeType;
    @SerializedName("app_nature_appraisal")
    private String appNatureAppraisal;
    @SerializedName("app_or_no")
    private String appOrNo;
    @SerializedName("app_pref_completion_date_day")
    private String appPrefCompletionDateDay;
    @SerializedName("app_pref_completion_date_month")
    private String appPrefCompletionDateMonth;
    @SerializedName("app_pref_completion_date_year")
    private String appPrefCompletionDateYear;
    @SerializedName("app_pref_inspection_date1_day")
    private String appPrefInspectionDate1Day;
    @SerializedName("app_pref_inspection_date1_month")
    private String appPrefInspectionDate1Month;
    @SerializedName("app_pref_inspection_date1_time")
    private String appPrefInspectionDate1Time;
    @SerializedName("app_pref_inspection_date1_year")
    private String appPrefInspectionDate1Year;
    @SerializedName("app_pref_inspection_date2_day")
    private String appPrefInspectionDate2Day;
    @SerializedName("app_pref_inspection_date2_month")
    private String appPrefInspectionDate2Month;
    @SerializedName("app_pref_inspection_date2_time")
    private String appPrefInspectionDate2Time;
    @SerializedName("app_pref_inspection_date2_year")
    private String appPrefInspectionDate2Year;
    @SerializedName("app_priority")
    private String appPriority;
    @SerializedName("app_purpose_appraisal")
    private String appPurposeAppraisal;
    @SerializedName("app_purpose_appraisal_other")
    private String appPurposeAppraisalOther;
    @SerializedName("app_purpose_other_desc")
    private String appPurposeOtherDesc;
    @SerializedName("app_request_appraisal")
    private String appRequestAppraisal;
    @SerializedName("app_request_remarks")
    private String appRequestRemarks;
    @SerializedName("app_request_type_of_loan")
    private String appRequestTypeOfLoan;
    @SerializedName("app_tvr_no_of_tracebacks")
    private String appTvrNoOfTracebacks;
    @SerializedName("app_vehicle_attachment1")
    private String appVehicleAttachment1;
    @SerializedName("app_vehicle_attachment2")
    private String appVehicleAttachment2;
    @SerializedName("app_vehicle_type")
    private String appVehicleType;
    @SerializedName("appraiser_area")
    private String appraiserArea;
    @SerializedName("appraiser_code")
    private String appraiserCode;
    @SerializedName("appraiser_company")
    private String appraiserCompany;
    @SerializedName("appraiser_emp_practice")
    private String appraiserEmpPractice;
    @SerializedName("appraiser_manager_code")
    private String appraiserManagerCode;
    @SerializedName("appraiser_prc_expiry_date")
    private String appraiserPrcExpiryDate;
    @SerializedName("appraiser_prc_issuance_date")
    private String appraiserPrcIssuanceDate;
    @SerializedName("appraiser_prc_no")
    private String appraiserPrcNo;
    @SerializedName("appraiser_ptr_issued_from")
    private String appraiserPtrIssuedFrom;
    @SerializedName("appraiser_ptr_no")
    private String appraiserPtrNo;
    @SerializedName("appraiser_signor")
    private String appraiserSignor;
    @SerializedName("appraiser_status")
    private String appraiserStatus;
    @SerializedName("appraiser_title")
    private String appraiserTitle;
    @SerializedName("appraiser_unit_id")
    private String appraiserUnitId;
    @SerializedName("appraiser_username")
    private String appraiserUsername;
    @SerializedName("case_number")
    private String caseNumber;

    @TypeConverters(CondoTitleDetailTypeConverter.class)
    @SerializedName("condo_title_details")
    private List<CondoTitleDetail> condoTitleDetails;

    @SerializedName("iprea_mem_no")
    private String ipreaMemNo;
    @SerializedName("iprea_mem_status")
    private String ipreaMemStatus;
    @SerializedName("iprea_membership_num")
    private String ipreaMembershipNum;

    @TypeConverters(LandimpImpDetailTypeConverter.class)
    @SerializedName("landimp_imp_details")
    private List<LandimpImpDetail> landimpImpDetails;

    @TypeConverters(LandimpTitleDetailTypeConverter.class)
    @SerializedName("landimp_title_details")
    private List<LandimpTitleDetail> landimpTitleDetails;

    @SerializedName("mobile_no")
    private String mobileNo;
    @SerializedName("real_estate_appraiser_no")
    private String realEstateAppraiserNo;
    @SerializedName("real_estate_expiration_date")
    private String realEstateExpirationDate;
    @SerializedName("real_estate_issuance_date")
    private String realEstateIssuanceDate;
    private String sex;
    @SerializedName("tin_no")
    private String tinNo;

    public long getUniqueId() {
        return uniqueId;
    }

    public void setUniqueId(long uniqueId) {
        this.uniqueId = uniqueId;
    }

    public long getRecordUId() {
        return recordUId;
    }

    public void setRecordUId(long recordUId) {
        this.recordUId = recordUId;
    }

    public String getRecordId() {
        return recordId;
    }

    public void setRecordId(String recordId) {
        this.recordId = recordId;
    }

    public String getAddress() {
        return NULL_STRING_SETTER(address);
    }

    public void setAddress(String address) {
        this.address = address;
    }

    public String getAppAmount() {
        return NULL_STRING_SETTER(appAmount);
    }

    public void setAppAmount(String appAmount) {
        this.appAmount = appAmount;
    }

    public String getAppApplicationReference() {
        return NULL_STRING_SETTER(appApplicationReference);
    }

    public void setAppApplicationReference(String appApplicationReference) {
        this.appApplicationReference = appApplicationReference;
    }

    public String getAppAppraisalPayment() {
        return NULL_STRING_SETTER(appAppraisalPayment);
    }

    public void setAppAppraisalPayment(String appAppraisalPayment) {
        this.appAppraisalPayment = appAppraisalPayment;
    }

    public String getAppAppraisalProcedure() {
        return NULL_STRING_SETTER(appAppraisalProcedure);
    }

    public void setAppAppraisalProcedure(String appAppraisalProcedure) {
        this.appAppraisalProcedure = appAppraisalProcedure;
    }

    public String getAppAppraiserName() {
        return NULL_STRING_SETTER(appAppraiserName);
    }

    public void setAppAppraiserName(String appAppraiserName) {
        this.appAppraiserName = appAppraiserName;
    }

    public String getAppApprovedBy() {
        return NULL_STRING_SETTER(appApprovedBy);
    }

    public void setAppApprovedBy(String appApprovedBy) {
        this.appApprovedBy = appApprovedBy;
    }

    public List<AppAttachment> getAppAttachment() {
        return NULL_LIST_SETTER(appAttachment);
    }

    public void setAppAttachment(List<AppAttachment> appAttachment) {
        this.appAttachment = appAttachment;
    }

    public String getAppCarBodyType() {
        return NULL_STRING_SETTER(appCarBodyType);
    }

    public void setAppCarBodyType(String appCarBodyType) {
        this.appCarBodyType = appCarBodyType;
    }

    public String getAppCarChassisNo() {
        return NULL_STRING_SETTER(appCarChassisNo);
    }

    public void setAppCarChassisNo(String appCarChassisNo) {
        this.appCarChassisNo = appCarChassisNo;
    }

    public String getAppCarColor() {
        return NULL_STRING_SETTER(appCarColor);
    }

    public void setAppCarColor(String appCarColor) {
        this.appCarColor = appCarColor;
    }

    public String getAppCarCrDateDay() {
        return NULL_STRING_SETTER(appCarCrDateDay);
    }

    public void setAppCarCrDateDay(String appCarCrDateDay) {
        this.appCarCrDateDay = appCarCrDateDay;
    }

    public String getAppCarCrDateMonth() {
        return NULL_STRING_SETTER(appCarCrDateMonth);
    }

    public void setAppCarCrDateMonth(String appCarCrDateMonth) {
        this.appCarCrDateMonth = appCarCrDateMonth;
    }

    public String getAppCarCrDateYear() {
        return NULL_STRING_SETTER(appCarCrDateYear);
    }

    public void setAppCarCrDateYear(String appCarCrDateYear) {
        this.appCarCrDateYear = appCarCrDateYear;
    }

    public String getAppCarCrNo() {
        return NULL_STRING_SETTER(appCarCrNo);
    }

    public void setAppCarCrNo(String appCarCrNo) {
        this.appCarCrNo = appCarCrNo;
    }

    public String getAppCarDisplacement() {
        return NULL_STRING_SETTER(appCarDisplacement);
    }

    public void setAppCarDisplacement(String appCarDisplacement) {
        this.appCarDisplacement = appCarDisplacement;
    }

    public String getAppCarInspectionTime() {
        return NULL_STRING_SETTER(appCarInspectionTime);
    }

    public void setAppCarInspectionTime(String appCarInspectionTime) {
        this.appCarInspectionTime = appCarInspectionTime;
    }

    public String getAppCarLoanDept() {
        return NULL_STRING_SETTER(appCarLoanDept);
    }

    public void setAppCarLoanDept(String appCarLoanDept) {
        this.appCarLoanDept = appCarLoanDept;
    }

    public String getAppCarLoanPurpose() {
        return NULL_STRING_SETTER(appCarLoanPurpose);
    }

    public void setAppCarLoanPurpose(String appCarLoanPurpose) {
        this.appCarLoanPurpose = appCarLoanPurpose;
    }

    public String getAppCarMileage() {
        return NULL_STRING_SETTER(appCarMileage);
    }

    public void setAppCarMileage(String appCarMileage) {
        this.appCarMileage = appCarMileage;
    }

    public String getAppCarModel() {
        return NULL_STRING_SETTER(appCarModel);
    }

    public void setAppCarModel(String appCarModel) {
        this.appCarModel = appCarModel;
    }

    public String getAppCarMotorNo() {
        return NULL_STRING_SETTER(appCarMotorNo);
    }

    public void setAppCarMotorNo(String appCarMotorNo) {
        this.appCarMotorNo = appCarMotorNo;
    }

    public String getAppCarOdometerReading() {
        return NULL_STRING_SETTER(appCarOdometerReading);
    }

    public void setAppCarOdometerReading(String appCarOdometerReading) {
        this.appCarOdometerReading = appCarOdometerReading;
    }

    public String getAppCarPlateNo() {
        return NULL_STRING_SETTER(appCarPlateNo);
    }

    public void setAppCarPlateNo(String appCarPlateNo) {
        this.appCarPlateNo = appCarPlateNo;
    }

    public String getAppCarSeries() {
        return NULL_STRING_SETTER(appCarSeries);
    }

    public void setAppCarSeries(String appCarSeries) {
        this.appCarSeries = appCarSeries;
    }

    public String getAppCarSubmissionTime() {
        return NULL_STRING_SETTER(appCarSubmissionTime);
    }

    public void setAppCarSubmissionTime(String appCarSubmissionTime) {
        this.appCarSubmissionTime = appCarSubmissionTime;
    }

    public String getAppCarVin() {
        return NULL_STRING_SETTER(appCarVin);
    }

    public void setAppCarVin(String appCarVin) {
        this.appCarVin = appCarVin;
    }

    public String getAppCarYear() {
        return NULL_STRING_SETTER(appCarYear);
    }

    public void setAppCarYear(String appCarYear) {
        this.appCarYear = appCarYear;
    }

    public String getAppCiadUnit() {
        return NULL_STRING_SETTER(appCiadUnit);
    }

    public void setAppCiadUnit(String appCiadUnit) {
        this.appCiadUnit = appCiadUnit;
    }

    public List<AppCollateralAddress> getAppCollateralAddress() {
        return NULL_LIST_SETTER(appCollateralAddress);
    }

    public void setAppCollateralAddress(List<AppCollateralAddress> appCollateralAddress) {
        this.appCollateralAddress = appCollateralAddress;
    }

    public List<AppContactPerson> getAppContactPerson() {
        return NULL_LIST_SETTER(appContactPerson);
    }

    public void setAppContactPerson(List<AppContactPerson> appContactPerson) {
        this.appContactPerson = appContactPerson;
    }

    public String getAppControlNo() {
        return NULL_STRING_SETTER(appControlNo);
    }

    public void setAppControlNo(String appControlNo) {
        this.appControlNo = appControlNo;
    }

    public String getAppCostCenter() {
        return NULL_STRING_SETTER(appCostCenter);
    }

    public void setAppCostCenter(String appCostCenter) {
        this.appCostCenter = appCostCenter;
    }

    public String getAppDivision() {
        return NULL_STRING_SETTER(appDivision);
    }

    public void setAppDivision(String appDivision) {
        this.appDivision = appDivision;
    }

    public String getAppGenAttachment1() {
        return NULL_STRING_SETTER(appGenAttachment1);
    }

    public void setAppGenAttachment1(String appGenAttachment1) {
        this.appGenAttachment1 = appGenAttachment1;
    }

    public String getAppGenAttachment2() {
        return NULL_STRING_SETTER(appGenAttachment2);
    }

    public void setAppGenAttachment2(String appGenAttachment2) {
        this.appGenAttachment2 = appGenAttachment2;
    }

    public String getAppGenAttachment3() {
        return NULL_STRING_SETTER(appGenAttachment3);
    }

    public void setAppGenAttachment3(String appGenAttachment3) {
        this.appGenAttachment3 = appGenAttachment3;
    }

    public String getAppGenAttachment4() {
        return NULL_STRING_SETTER(appGenAttachment4);
    }

    public void setAppGenAttachment4(String appGenAttachment4) {
        this.appGenAttachment4 = appGenAttachment4;
    }

    public String getAppGenAttachment5() {
        return NULL_STRING_SETTER(appGenAttachment5);
    }

    public void setAppGenAttachment5(String appGenAttachment5) {
        this.appGenAttachment5 = appGenAttachment5;
    }

    public String getAppGenAttachment6() {
        return NULL_STRING_SETTER(appGenAttachment6);
    }

    public void setAppGenAttachment6(String appGenAttachment6) {
        this.appGenAttachment6 = appGenAttachment6;
    }

    public String getAppHasTvr() {
        return NULL_STRING_SETTER(appHasTvr);
    }

    public void setAppHasTvr(String appHasTvr) {
        this.appHasTvr = appHasTvr;
    }

    public String getAppMeAge() {
        return NULL_STRING_SETTER(appMeAge);
    }

    public void setAppMeAge(String appMeAge) {
        this.appMeAge = appMeAge;
    }

    public String getAppMeCondition() {
        return NULL_STRING_SETTER(appMeCondition);
    }

    public void setAppMeCondition(String appMeCondition) {
        this.appMeCondition = appMeCondition;
    }

    public String getAppMeManufacturer() {
        return NULL_STRING_SETTER(appMeManufacturer);
    }

    public void setAppMeManufacturer(String appMeManufacturer) {
        this.appMeManufacturer = appMeManufacturer;
    }

    public String getAppMeModel() {
        return NULL_STRING_SETTER(appMeModel);
    }

    public void setAppMeModel(String appMeModel) {
        this.appMeModel = appMeModel;
    }

    public String getAppMeQuantity() {
        return NULL_STRING_SETTER(appMeQuantity);
    }

    public void setAppMeQuantity(String appMeQuantity) {
        this.appMeQuantity = appMeQuantity;
    }

    public String getAppMeSerialNo() {
        return NULL_STRING_SETTER(appMeSerialNo);
    }

    public void setAppMeSerialNo(String appMeSerialNo) {
        this.appMeSerialNo = appMeSerialNo;
    }

    public String getAppMeType() {
        return NULL_STRING_SETTER(appMeType);
    }

    public void setAppMeType(String appMeType) {
        this.appMeType = appMeType;
    }

    public String getAppNatureAppraisal() {
        return NULL_STRING_SETTER(appNatureAppraisal);
    }

    public void setAppNatureAppraisal(String appNatureAppraisal) {
        this.appNatureAppraisal = appNatureAppraisal;
    }

    public String getAppOrNo() {
        return NULL_STRING_SETTER(appOrNo);
    }

    public void setAppOrNo(String appOrNo) {
        this.appOrNo = appOrNo;
    }

    public String getAppPrefCompletionDateDay() {
        return NULL_STRING_SETTER(appPrefCompletionDateDay);
    }

    public void setAppPrefCompletionDateDay(String appPrefCompletionDateDay) {
        this.appPrefCompletionDateDay = appPrefCompletionDateDay;
    }

    public String getAppPrefCompletionDateMonth() {
        return NULL_STRING_SETTER(appPrefCompletionDateMonth);
    }

    public void setAppPrefCompletionDateMonth(String appPrefCompletionDateMonth) {
        this.appPrefCompletionDateMonth = appPrefCompletionDateMonth;
    }

    public String getAppPrefCompletionDateYear() {
        return NULL_STRING_SETTER(appPrefCompletionDateYear);
    }

    public void setAppPrefCompletionDateYear(String appPrefCompletionDateYear) {
        this.appPrefCompletionDateYear = appPrefCompletionDateYear;
    }

    public String getAppPrefInspectionDate1Day() {
        return NULL_STRING_SETTER(appPrefInspectionDate1Day);
    }

    public void setAppPrefInspectionDate1Day(String appPrefInspectionDate1Day) {
        this.appPrefInspectionDate1Day = appPrefInspectionDate1Day;
    }

    public String getAppPrefInspectionDate1Month() {
        return NULL_STRING_SETTER(appPrefInspectionDate1Month);
    }

    public void setAppPrefInspectionDate1Month(String appPrefInspectionDate1Month) {
        this.appPrefInspectionDate1Month = appPrefInspectionDate1Month;
    }

    public String getAppPrefInspectionDate1Time() {
        return NULL_STRING_SETTER(appPrefInspectionDate1Time);
    }

    public void setAppPrefInspectionDate1Time(String appPrefInspectionDate1Time) {
        this.appPrefInspectionDate1Time = appPrefInspectionDate1Time;
    }

    public String getAppPrefInspectionDate1Year() {
        return NULL_STRING_SETTER(appPrefInspectionDate1Year);
    }

    public void setAppPrefInspectionDate1Year(String appPrefInspectionDate1Year) {
        this.appPrefInspectionDate1Year = appPrefInspectionDate1Year;
    }

    public String getAppPrefInspectionDate2Day() {
        return NULL_STRING_SETTER(appPrefInspectionDate2Day);
    }

    public void setAppPrefInspectionDate2Day(String appPrefInspectionDate2Day) {
        this.appPrefInspectionDate2Day = appPrefInspectionDate2Day;
    }

    public String getAppPrefInspectionDate2Month() {
        return NULL_STRING_SETTER(appPrefInspectionDate2Month);
    }

    public void setAppPrefInspectionDate2Month(String appPrefInspectionDate2Month) {
        this.appPrefInspectionDate2Month = appPrefInspectionDate2Month;
    }

    public String getAppPrefInspectionDate2Time() {
        return NULL_STRING_SETTER(appPrefInspectionDate2Time);
    }

    public void setAppPrefInspectionDate2Time(String appPrefInspectionDate2Time) {
        this.appPrefInspectionDate2Time = appPrefInspectionDate2Time;
    }

    public String getAppPrefInspectionDate2Year() {
        return NULL_STRING_SETTER(appPrefInspectionDate2Year);
    }

    public void setAppPrefInspectionDate2Year(String appPrefInspectionDate2Year) {
        this.appPrefInspectionDate2Year = appPrefInspectionDate2Year;
    }

    public String getAppPriority() {
        return NULL_STRING_SETTER(appPriority);
    }

    public void setAppPriority(String appPriority) {
        this.appPriority = appPriority;
    }

    public String getAppPurposeAppraisal() {
        return NULL_STRING_SETTER(appPurposeAppraisal);
    }

    public void setAppPurposeAppraisal(String appPurposeAppraisal) {
        this.appPurposeAppraisal = appPurposeAppraisal;
    }

    public String getAppPurposeAppraisalOther() {
        return NULL_STRING_SETTER(appPurposeAppraisalOther);
    }

    public void setAppPurposeAppraisalOther(String appPurposeAppraisalOther) {
        this.appPurposeAppraisalOther = appPurposeAppraisalOther;
    }

    public String getAppPurposeOtherDesc() {
        return NULL_STRING_SETTER(appPurposeOtherDesc);
    }

    public void setAppPurposeOtherDesc(String appPurposeOtherDesc) {
        this.appPurposeOtherDesc = appPurposeOtherDesc;
    }

    public String getAppRequestAppraisal() {
        return NULL_STRING_SETTER(appRequestAppraisal);
    }

    public void setAppRequestAppraisal(String appRequestAppraisal) {
        this.appRequestAppraisal = appRequestAppraisal;
    }

    public String getAppRequestRemarks() {
        return NULL_STRING_SETTER(appRequestRemarks);
    }

    public void setAppRequestRemarks(String appRequestRemarks) {
        this.appRequestRemarks = appRequestRemarks;
    }

    public String getAppRequestTypeOfLoan() {
        return NULL_STRING_SETTER(appRequestTypeOfLoan);
    }

    public void setAppRequestTypeOfLoan(String appRequestTypeOfLoan) {
        this.appRequestTypeOfLoan = appRequestTypeOfLoan;
    }

    public String getAppTvrNoOfTracebacks() {
        return NULL_STRING_SETTER(appTvrNoOfTracebacks);
    }

    public void setAppTvrNoOfTracebacks(String appTvrNoOfTracebacks) {
        this.appTvrNoOfTracebacks = appTvrNoOfTracebacks;
    }

    public String getAppVehicleAttachment1() {
        return NULL_STRING_SETTER(appVehicleAttachment1);
    }

    public void setAppVehicleAttachment1(String appVehicleAttachment1) {
        this.appVehicleAttachment1 = appVehicleAttachment1;
    }

    public String getAppVehicleAttachment2() {
        return NULL_STRING_SETTER(appVehicleAttachment2);
    }

    public void setAppVehicleAttachment2(String appVehicleAttachment2) {
        this.appVehicleAttachment2 = appVehicleAttachment2;
    }

    public String getAppVehicleType() {
        return NULL_STRING_SETTER(appVehicleType);
    }

    public void setAppVehicleType(String appVehicleType) {
        this.appVehicleType = appVehicleType;
    }

    public String getAppraiserArea() {
        return NULL_STRING_SETTER(appraiserArea);
    }

    public void setAppraiserArea(String appraiserArea) {
        this.appraiserArea = appraiserArea;
    }

    public String getAppraiserCode() {
        return NULL_STRING_SETTER(appraiserCode);
    }

    public void setAppraiserCode(String appraiserCode) {
        this.appraiserCode = appraiserCode;
    }

    public String getAppraiserCompany() {
        return NULL_STRING_SETTER(appraiserCompany);
    }

    public void setAppraiserCompany(String appraiserCompany) {
        this.appraiserCompany = appraiserCompany;
    }

    public String getAppraiserEmpPractice() {
        return NULL_STRING_SETTER(appraiserEmpPractice);
    }

    public void setAppraiserEmpPractice(String appraiserEmpPractice) {
        this.appraiserEmpPractice = appraiserEmpPractice;
    }

    public String getAppraiserManagerCode() {
        return NULL_STRING_SETTER(appraiserManagerCode);
    }

    public void setAppraiserManagerCode(String appraiserManagerCode) {
        this.appraiserManagerCode = appraiserManagerCode;
    }

    public String getAppraiserPrcExpiryDate() {
        return NULL_STRING_SETTER(appraiserPrcExpiryDate);
    }

    public void setAppraiserPrcExpiryDate(String appraiserPrcExpiryDate) {
        this.appraiserPrcExpiryDate = appraiserPrcExpiryDate;
    }

    public String getAppraiserPrcIssuanceDate() {
        return NULL_STRING_SETTER(appraiserPrcIssuanceDate);
    }

    public void setAppraiserPrcIssuanceDate(String appraiserPrcIssuanceDate) {
        this.appraiserPrcIssuanceDate = appraiserPrcIssuanceDate;
    }

    public String getAppraiserPrcNo() {
        return NULL_STRING_SETTER(appraiserPrcNo);
    }

    public void setAppraiserPrcNo(String appraiserPrcNo) {
        this.appraiserPrcNo = appraiserPrcNo;
    }

    public String getAppraiserPtrIssuedFrom() {
        return NULL_STRING_SETTER(appraiserPtrIssuedFrom);
    }

    public void setAppraiserPtrIssuedFrom(String appraiserPtrIssuedFrom) {
        this.appraiserPtrIssuedFrom = appraiserPtrIssuedFrom;
    }

    public String getAppraiserPtrNo() {
        return NULL_STRING_SETTER(appraiserPtrNo);
    }

    public void setAppraiserPtrNo(String appraiserPtrNo) {
        this.appraiserPtrNo = appraiserPtrNo;
    }

    public String getAppraiserSignor() {
        return NULL_STRING_SETTER(appraiserSignor);
    }

    public void setAppraiserSignor(String appraiserSignor) {
        this.appraiserSignor = appraiserSignor;
    }

    public String getAppraiserStatus() {
        return NULL_STRING_SETTER(appraiserStatus);
    }

    public void setAppraiserStatus(String appraiserStatus) {
        this.appraiserStatus = appraiserStatus;
    }

    public String getAppraiserTitle() {
        return NULL_STRING_SETTER(appraiserTitle);
    }

    public void setAppraiserTitle(String appraiserTitle) {
        this.appraiserTitle = appraiserTitle;
    }

    public String getAppraiserUnitId() {
        return NULL_STRING_SETTER(appraiserUnitId);
    }

    public void setAppraiserUnitId(String appraiserUnitId) {
        this.appraiserUnitId = appraiserUnitId;
    }

    public String getAppraiserUsername() {
        return NULL_STRING_SETTER(appraiserUsername);
    }

    public void setAppraiserUsername(String appraiserUsername) {
        this.appraiserUsername = appraiserUsername;
    }

    public String getCaseNumber() {
        return NULL_STRING_SETTER(caseNumber);
    }

    public void setCaseNumber(String caseNumber) {
        this.caseNumber = caseNumber;
    }

    public List<CondoTitleDetail> getCondoTitleDetails() {
        return NULL_LIST_SETTER(condoTitleDetails);
    }

    public void setCondoTitleDetails(List<CondoTitleDetail> condoTitleDetails) {
        this.condoTitleDetails = condoTitleDetails;
    }

    public String getIpreaMemNo() {
        return NULL_STRING_SETTER(ipreaMemNo);
    }

    public void setIpreaMemNo(String ipreaMemNo) {
        this.ipreaMemNo = ipreaMemNo;
    }

    public String getIpreaMemStatus() {
        return NULL_STRING_SETTER(ipreaMemStatus);
    }

    public void setIpreaMemStatus(String ipreaMemStatus) {
        this.ipreaMemStatus = ipreaMemStatus;
    }

    public String getIpreaMembershipNum() {
        return NULL_STRING_SETTER(ipreaMembershipNum);
    }

    public void setIpreaMembershipNum(String ipreaMembershipNum) {
        this.ipreaMembershipNum = ipreaMembershipNum;
    }

    public List<LandimpImpDetail> getLandimpImpDetails() {
        return NULL_LIST_SETTER(landimpImpDetails);
    }

    public void setLandimpImpDetails(List<LandimpImpDetail> landimpImpDetails) {
        this.landimpImpDetails = landimpImpDetails;
    }

    public List<LandimpTitleDetail> getLandimpTitleDetails() {
        return NULL_LIST_SETTER(landimpTitleDetails);
    }

    public void setLandimpTitleDetails(List<LandimpTitleDetail> landimpTitleDetails) {
        this.landimpTitleDetails = landimpTitleDetails;
    }

    public String getMobileNo() {
        return NULL_STRING_SETTER(mobileNo);
    }

    public void setMobileNo(String mobileNo) {
        this.mobileNo = mobileNo;
    }

    public String getRealEstateAppraiserNo() {
        return NULL_STRING_SETTER(realEstateAppraiserNo);
    }

    public void setRealEstateAppraiserNo(String realEstateAppraiserNo) {
        this.realEstateAppraiserNo = realEstateAppraiserNo;
    }

    public String getRealEstateExpirationDate() {
        return NULL_STRING_SETTER(realEstateExpirationDate);
    }

    public void setRealEstateExpirationDate(String realEstateExpirationDate) {
        this.realEstateExpirationDate = realEstateExpirationDate;
    }

    public String getRealEstateIssuanceDate() {
        return NULL_STRING_SETTER(realEstateIssuanceDate);
    }

    public void setRealEstateIssuanceDate(String realEstateIssuanceDate) {
        this.realEstateIssuanceDate = realEstateIssuanceDate;
    }

    public String getSex() {
        return NULL_STRING_SETTER(sex);
    }

    public void setSex(String sex) {
        this.sex = sex;
    }

    public String getTinNo() {
        return NULL_STRING_SETTER(tinNo);
    }

    public void setTinNo(String tinNo) {
        this.tinNo = tinNo;
    }

}
