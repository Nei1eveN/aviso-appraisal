
package com.gdslinkasia.base.model.details;

import androidx.room.ColumnInfo;
import androidx.room.Entity;
import androidx.room.ForeignKey;
import androidx.room.PrimaryKey;

import com.google.gson.annotations.SerializedName;

import static androidx.room.ForeignKey.CASCADE;
import static com.gdslinkasia.base.database.DatabaseUtils.APPRAISAL_REQUEST_ID;
import static com.gdslinkasia.base.database.DatabaseUtils.UNIQUE_ID;
import static com.gdslinkasia.base.utils.GlobalString.NULL_STRING_SETTER;

@SuppressWarnings("unused")
@Entity(foreignKeys = {
        @ForeignKey(entity = AppraisalRequest.class,
                parentColumns = UNIQUE_ID,
                childColumns = APPRAISAL_REQUEST_ID,
                onDelete = CASCADE)})
public class CondoTitleDetail {

//    @Expose(serialize = false, deserialize = false)
    @PrimaryKey(autoGenerate = true)
    @ColumnInfo(name = UNIQUE_ID)
    private transient long uniqueId;

//    @Expose(serialize = false, deserialize = false)
    @ColumnInfo(name = APPRAISAL_REQUEST_ID, index = true)
    private transient long appraisalRequestId;

    @SerializedName("condo_title_details_area")
    private String condoTitleDetailsArea;
    @SerializedName("condo_title_details_bldg_name")
    private String condoTitleDetailsBldgName;
    @SerializedName("condo_title_details_block_no")
    private String condoTitleDetailsBlockNo;
    @SerializedName("condo_title_details_cct_no")
    private String condoTitleDetailsCctNo;
    @SerializedName("condo_title_details_floor")
    private String condoTitleDetailsFloor;
    @SerializedName("condo_title_details_lot_no")
    private String condoTitleDetailsLotNo;
    @SerializedName("condo_title_details_reg_of_deeds")
    private String condoTitleDetailsRegOfDeeds;
    @SerializedName("condo_title_details_registered_owner")
    private String condoTitleDetailsRegisteredOwner;
    @SerializedName("condo_title_details_registry_date_day")
    private String condoTitleDetailsRegistryDateDay;
    @SerializedName("condo_title_details_registry_date_month")
    private String condoTitleDetailsRegistryDateMonth;
    @SerializedName("condo_title_details_registry_date_year")
    private String condoTitleDetailsRegistryDateYear;
    @SerializedName("condo_title_details_title_type")
    private String condoTitleDetailsTitleType;
    @SerializedName("condo_title_details_unit_no")
    private String condoTitleDetailsUnitNo;

    public long getUniqueId() {
        return uniqueId;
    }

    public void setUniqueId(long uniqueId) {
        this.uniqueId = uniqueId;
    }

    public long getAppraisalRequestId() {
        return appraisalRequestId;
    }

    public void setAppraisalRequestId(long appraisalRequestId) {
        this.appraisalRequestId = appraisalRequestId;
    }

    public String getCondoTitleDetailsArea() {
        return NULL_STRING_SETTER(condoTitleDetailsArea);
    }

    public void setCondoTitleDetailsArea(String condoTitleDetailsArea) {
        this.condoTitleDetailsArea = condoTitleDetailsArea;
    }

    public String getCondoTitleDetailsBldgName() {
        return NULL_STRING_SETTER(condoTitleDetailsBldgName);
    }

    public void setCondoTitleDetailsBldgName(String condoTitleDetailsBldgName) {
        this.condoTitleDetailsBldgName = condoTitleDetailsBldgName;
    }

    public String getCondoTitleDetailsBlockNo() {
        return NULL_STRING_SETTER(condoTitleDetailsBlockNo);
    }

    public void setCondoTitleDetailsBlockNo(String condoTitleDetailsBlockNo) {
        this.condoTitleDetailsBlockNo = condoTitleDetailsBlockNo;
    }

    public String getCondoTitleDetailsCctNo() {
        return NULL_STRING_SETTER(condoTitleDetailsCctNo);
    }

    public void setCondoTitleDetailsCctNo(String condoTitleDetailsCctNo) {
        this.condoTitleDetailsCctNo = condoTitleDetailsCctNo;
    }

    public String getCondoTitleDetailsFloor() {
        return NULL_STRING_SETTER(condoTitleDetailsFloor);
    }

    public void setCondoTitleDetailsFloor(String condoTitleDetailsFloor) {
        this.condoTitleDetailsFloor = condoTitleDetailsFloor;
    }

    public String getCondoTitleDetailsLotNo() {
        return NULL_STRING_SETTER(condoTitleDetailsLotNo);
    }

    public void setCondoTitleDetailsLotNo(String condoTitleDetailsLotNo) {
        this.condoTitleDetailsLotNo = condoTitleDetailsLotNo;
    }

    public String getCondoTitleDetailsRegOfDeeds() {
        return NULL_STRING_SETTER(condoTitleDetailsRegOfDeeds);
    }

    public void setCondoTitleDetailsRegOfDeeds(String condoTitleDetailsRegOfDeeds) {
        this.condoTitleDetailsRegOfDeeds = condoTitleDetailsRegOfDeeds;
    }

    public String getCondoTitleDetailsRegisteredOwner() {
        return NULL_STRING_SETTER(condoTitleDetailsRegisteredOwner);
    }

    public void setCondoTitleDetailsRegisteredOwner(String condoTitleDetailsRegisteredOwner) {
        this.condoTitleDetailsRegisteredOwner = condoTitleDetailsRegisteredOwner;
    }

    public String getCondoTitleDetailsRegistryDateDay() {
        return NULL_STRING_SETTER(condoTitleDetailsRegistryDateDay);
    }

    public void setCondoTitleDetailsRegistryDateDay(String condoTitleDetailsRegistryDateDay) {
        this.condoTitleDetailsRegistryDateDay = condoTitleDetailsRegistryDateDay;
    }

    public String getCondoTitleDetailsRegistryDateMonth() {
        return NULL_STRING_SETTER(condoTitleDetailsRegistryDateMonth);
    }

    public void setCondoTitleDetailsRegistryDateMonth(String condoTitleDetailsRegistryDateMonth) {
        this.condoTitleDetailsRegistryDateMonth = condoTitleDetailsRegistryDateMonth;
    }

    public String getCondoTitleDetailsRegistryDateYear() {
        return NULL_STRING_SETTER(condoTitleDetailsRegistryDateYear);
    }

    public void setCondoTitleDetailsRegistryDateYear(String condoTitleDetailsRegistryDateYear) {
        this.condoTitleDetailsRegistryDateYear = condoTitleDetailsRegistryDateYear;
    }

    public String getCondoTitleDetailsTitleType() {
        return NULL_STRING_SETTER(condoTitleDetailsTitleType);
    }

    public void setCondoTitleDetailsTitleType(String condoTitleDetailsTitleType) {
        this.condoTitleDetailsTitleType = condoTitleDetailsTitleType;
    }

    public String getCondoTitleDetailsUnitNo() {
        return NULL_STRING_SETTER(condoTitleDetailsUnitNo);
    }

    public void setCondoTitleDetailsUnitNo(String condoTitleDetailsUnitNo) {
        this.condoTitleDetailsUnitNo = condoTitleDetailsUnitNo;
    }

}
