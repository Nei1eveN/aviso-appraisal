package com.gdslinkasia.base.model.job;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class JobSubmitResult {

    @SerializedName("record")
    @Expose
    private Record record;

    public Record getRecord() {
        return record;
    }

    public void setRecord(Record record) {
        this.record = record;
    }
}
