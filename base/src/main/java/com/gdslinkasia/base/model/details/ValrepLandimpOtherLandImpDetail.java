
package com.gdslinkasia.base.model.details;

import androidx.room.ColumnInfo;
import androidx.room.Entity;
import androidx.room.ForeignKey;
import androidx.room.PrimaryKey;

import com.gdslinkasia.base.model.job.Record;
import com.google.gson.annotations.SerializedName;

import static androidx.room.ForeignKey.CASCADE;
import static com.gdslinkasia.base.database.DatabaseUtils.RECORD_ID;
import static com.gdslinkasia.base.database.DatabaseUtils.RECORD_UID;
import static com.gdslinkasia.base.database.DatabaseUtils.UNIQUE_ID;
import static com.gdslinkasia.base.utils.GlobalString.NULL_STRING_SETTER;

@SuppressWarnings("unused")
@Entity(foreignKeys = {@ForeignKey(
        entity = Record.class,
        parentColumns = UNIQUE_ID,
        childColumns = RECORD_UID,
        onDelete = CASCADE)})
public class ValrepLandimpOtherLandImpDetail {

//    @Expose(serialize = false, deserialize = false)
    @PrimaryKey(autoGenerate = true)
    @ColumnInfo(name = UNIQUE_ID)
    private transient long uniqueId;

    //TODO NON-SERIALIZED
//    @Expose(serialize = false, deserialize = false)
    @ColumnInfo(name = RECORD_ID, index = true)
    private transient String recordId;

//    @Expose(serialize = false, deserialize = false)
    @ColumnInfo(name = RECORD_UID, index = true)
    private transient long recordUId;

    @SerializedName("valrep_landimp_other_land_imp_details_area")
    private String valrepLandimpOtherLandImpDetailsArea;
    @SerializedName("valrep_landimp_other_land_imp_details_area_unit")
    private String valrepLandimpOtherLandImpDetailsAreaUnit;
    @SerializedName("valrep_landimp_other_land_imp_details_condition")
    private String valrepLandimpOtherLandImpDetailsCondition;
    @SerializedName("valrep_landimp_other_land_imp_details_desc")
    private String valrepLandimpOtherLandImpDetailsDesc;
    @SerializedName("valrep_landimp_other_land_imp_details_economic_life")
    private String valrepLandimpOtherLandImpDetailsEconomicLife;
    @SerializedName("valrep_landimp_other_land_imp_details_effective_age")
    private String valrepLandimpOtherLandImpDetailsEffectiveAge;
    @SerializedName("valrep_landimp_other_land_imp_details_remain_life")
    private String valrepLandimpOtherLandImpDetailsRemainLife;
    @SerializedName("valrep_landimp_other_land_imp_details_remarks")
    private String valrepLandimpOtherLandImpDetailsRemarks;
    @SerializedName("valrep_landimp_other_land_imp_details_type")
    private String valrepLandimpOtherLandImpDetailsType;

    public long getUniqueId() {
        return uniqueId;
    }

    public void setUniqueId(long uniqueId) {
        this.uniqueId = uniqueId;
    }

    public String getRecordId() {
        return recordId;
    }

    public void setRecordId(String recordId) {
        this.recordId = recordId;
    }

    public long getRecordUId() {
        return recordUId;
    }

    public void setRecordUId(long recordUId) {
        this.recordUId = recordUId;
    }

    public String getValrepLandimpOtherLandImpDetailsArea() {
        return NULL_STRING_SETTER(valrepLandimpOtherLandImpDetailsArea);
    }

    public void setValrepLandimpOtherLandImpDetailsArea(String valrepLandimpOtherLandImpDetailsArea) {
        this.valrepLandimpOtherLandImpDetailsArea = valrepLandimpOtherLandImpDetailsArea;
    }

    public String getValrepLandimpOtherLandImpDetailsAreaUnit() {
        return NULL_STRING_SETTER(valrepLandimpOtherLandImpDetailsAreaUnit);
    }

    public void setValrepLandimpOtherLandImpDetailsAreaUnit(String valrepLandimpOtherLandImpDetailsAreaUnit) {
        this.valrepLandimpOtherLandImpDetailsAreaUnit = valrepLandimpOtherLandImpDetailsAreaUnit;
    }

    public String getValrepLandimpOtherLandImpDetailsCondition() {
        return NULL_STRING_SETTER(valrepLandimpOtherLandImpDetailsCondition);
    }

    public void setValrepLandimpOtherLandImpDetailsCondition(String valrepLandimpOtherLandImpDetailsCondition) {
        this.valrepLandimpOtherLandImpDetailsCondition = valrepLandimpOtherLandImpDetailsCondition;
    }

    public String getValrepLandimpOtherLandImpDetailsDesc() {
        return NULL_STRING_SETTER(valrepLandimpOtherLandImpDetailsDesc);
    }

    public void setValrepLandimpOtherLandImpDetailsDesc(String valrepLandimpOtherLandImpDetailsDesc) {
        this.valrepLandimpOtherLandImpDetailsDesc = valrepLandimpOtherLandImpDetailsDesc;
    }

    public String getValrepLandimpOtherLandImpDetailsEconomicLife() {
        return NULL_STRING_SETTER(valrepLandimpOtherLandImpDetailsEconomicLife);
    }

    public void setValrepLandimpOtherLandImpDetailsEconomicLife(String valrepLandimpOtherLandImpDetailsEconomicLife) {
        this.valrepLandimpOtherLandImpDetailsEconomicLife = valrepLandimpOtherLandImpDetailsEconomicLife;
    }

    public String getValrepLandimpOtherLandImpDetailsEffectiveAge() {
        return NULL_STRING_SETTER(valrepLandimpOtherLandImpDetailsEffectiveAge);
    }

    public void setValrepLandimpOtherLandImpDetailsEffectiveAge(String valrepLandimpOtherLandImpDetailsEffectiveAge) {
        this.valrepLandimpOtherLandImpDetailsEffectiveAge = valrepLandimpOtherLandImpDetailsEffectiveAge;
    }

    public String getValrepLandimpOtherLandImpDetailsRemainLife() {
        return NULL_STRING_SETTER(valrepLandimpOtherLandImpDetailsRemainLife);
    }

    public void setValrepLandimpOtherLandImpDetailsRemainLife(String valrepLandimpOtherLandImpDetailsRemainLife) {
        this.valrepLandimpOtherLandImpDetailsRemainLife = valrepLandimpOtherLandImpDetailsRemainLife;
    }

    public String getValrepLandimpOtherLandImpDetailsRemarks() {
        return NULL_STRING_SETTER(valrepLandimpOtherLandImpDetailsRemarks);
    }

    public void setValrepLandimpOtherLandImpDetailsRemarks(String valrepLandimpOtherLandImpDetailsRemarks) {
        this.valrepLandimpOtherLandImpDetailsRemarks = valrepLandimpOtherLandImpDetailsRemarks;
    }

    public String getValrepLandimpOtherLandImpDetailsType() {
        return NULL_STRING_SETTER(valrepLandimpOtherLandImpDetailsType);
    }

    public void setValrepLandimpOtherLandImpDetailsType(String valrepLandimpOtherLandImpDetailsType) {
        this.valrepLandimpOtherLandImpDetailsType = valrepLandimpOtherLandImpDetailsType;
    }

}
