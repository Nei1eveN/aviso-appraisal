package com.gdslinkasia.base.model.details;

import androidx.annotation.NonNull;
import androidx.room.ColumnInfo;
import androidx.room.Entity;
import androidx.room.PrimaryKey;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import static com.gdslinkasia.base.database.DatabaseUtils.RECORD_ID;

@Entity
public class JobSystem {

    @PrimaryKey
    @ColumnInfo(name = RECORD_ID)
    @SerializedName("record_id")
    @Expose
    private @NonNull String recordId;

    @NonNull
    public String getRecordId() {
        return recordId;
    }

    public void setRecordId(@NonNull String recordId) {
        this.recordId = recordId;
    }

}