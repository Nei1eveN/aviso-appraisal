
package com.gdslinkasia.base.model.details;

import androidx.room.ColumnInfo;
import androidx.room.Entity;
import androidx.room.ForeignKey;
import androidx.room.PrimaryKey;
import androidx.room.TypeConverters;

import com.gdslinkasia.base.database.converters.ValrepLandimpLotValuationDetailTypeConverter;
import com.gdslinkasia.base.model.job.Record;
import com.google.gson.annotations.SerializedName;

import java.util.List;

import static androidx.room.ForeignKey.CASCADE;
import static com.gdslinkasia.base.database.DatabaseUtils.RECORD_ID;
import static com.gdslinkasia.base.database.DatabaseUtils.RECORD_UID;
import static com.gdslinkasia.base.database.DatabaseUtils.UNIQUE_ID;
import static com.gdslinkasia.base.utils.GlobalString.NULL_LIST_SETTER;


@SuppressWarnings("unused")
@Entity(foreignKeys = {@ForeignKey(
        entity = Record.class,
        parentColumns = UNIQUE_ID,
        childColumns = RECORD_UID,
        onDelete = CASCADE)})
public class ValrepLandimpLotValuation {

//    @Expose(serialize = false, deserialize = false)
    @PrimaryKey(autoGenerate = true)
    @ColumnInfo(name = UNIQUE_ID)
    private transient long uniqueId;

    //TODO NON-SERIALIZED
//    @Expose(serialize = false, deserialize = false)
    @ColumnInfo(name = RECORD_ID, index = true)
    private transient String recordId;

//    @Expose(serialize = false, deserialize = false)
    @ColumnInfo(name = RECORD_UID, index = true)
    private transient long recordUId;

    @TypeConverters(ValrepLandimpLotValuationDetailTypeConverter.class)
    @SerializedName("valrep_landimp_lot_valuation_details")
    private List<ValrepLandimpLotValuationDetail> valrepLandimpLotValuationDetails;

    public long getUniqueId() {
        return uniqueId;
    }

    public void setUniqueId(long uniqueId) {
        this.uniqueId = uniqueId;
    }

    public String getRecordId() {
        return recordId;
    }

    public void setRecordId(String recordId) {
        this.recordId = recordId;
    }

    public long getRecordUId() {
        return recordUId;
    }

    public void setRecordUId(long recordUId) {
        this.recordUId = recordUId;
    }

    public List<ValrepLandimpLotValuationDetail> getValrepLandimpLotValuationDetails() {
        return NULL_LIST_SETTER(valrepLandimpLotValuationDetails);
    }

    public void setValrepLandimpLotValuationDetails(List<ValrepLandimpLotValuationDetail> valrepLandimpLotValuationDetails) {
        this.valrepLandimpLotValuationDetails = valrepLandimpLotValuationDetails;
    }

}
