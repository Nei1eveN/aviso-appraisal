
package com.gdslinkasia.base.model.details;

import androidx.room.ColumnInfo;
import androidx.room.Entity;
import androidx.room.ForeignKey;
import androidx.room.PrimaryKey;

import com.google.gson.annotations.SerializedName;

import static androidx.room.ForeignKey.CASCADE;
import static com.gdslinkasia.base.database.DatabaseUtils.APPRAISAL_REQUEST_ID;
import static com.gdslinkasia.base.database.DatabaseUtils.UNIQUE_ID;
import static com.gdslinkasia.base.utils.GlobalString.NULL_STRING_SETTER;

@SuppressWarnings("unused")
@Entity(foreignKeys = {
        @ForeignKey(entity = AppraisalRequest.class,
                parentColumns = UNIQUE_ID,
                childColumns = APPRAISAL_REQUEST_ID,
                onDelete = CASCADE)})
public class LandimpImpDetail {

//    @Expose(serialize = false, deserialize = false)
    @PrimaryKey(autoGenerate = true)
    @ColumnInfo(name = UNIQUE_ID)
    private transient long uniqueId;

//    @Expose(serialize = false, deserialize = false)
    @ColumnInfo(name = APPRAISAL_REQUEST_ID, index = true)
    private transient long appraisalRequestId;

    //TODO NON-SERIALIZED

    @SerializedName("landimp_imp_details_desc")
    private String landimpImpDetailsDesc;
    @SerializedName("landimp_imp_details_index")
    private String landimpImpDetailsIndex;

    public long getUniqueId() {
        return uniqueId;
    }

    public void setUniqueId(long uniqueId) {
        this.uniqueId = uniqueId;
    }

    public long getAppraisalRequestId() {
        return appraisalRequestId;
    }

    public void setAppraisalRequestId(long appraisalRequestId) {
        this.appraisalRequestId = appraisalRequestId;
    }

    public String getLandimpImpDetailsDesc() {
        return NULL_STRING_SETTER(landimpImpDetailsDesc);
    }

    public void setLandimpImpDetailsDesc(String landimpImpDetailsDesc) {
        this.landimpImpDetailsDesc = landimpImpDetailsDesc;
    }

    public String getLandimpImpDetailsIndex() {
        return NULL_STRING_SETTER(landimpImpDetailsIndex);
    }

    public void setLandimpImpDetailsIndex(String landimpImpDetailsIndex) {
        this.landimpImpDetailsIndex = landimpImpDetailsIndex;
    }

}
