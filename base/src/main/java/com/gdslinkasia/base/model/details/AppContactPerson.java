package com.gdslinkasia.base.model.details;

import androidx.room.ColumnInfo;
import androidx.room.Entity;
import androidx.room.ForeignKey;
import androidx.room.PrimaryKey;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import static androidx.room.ForeignKey.CASCADE;
import static com.gdslinkasia.base.database.DatabaseUtils.APPRAISAL_REQUEST_ID;
import static com.gdslinkasia.base.database.DatabaseUtils.CONTACT_PERSON_INDEX;
import static com.gdslinkasia.base.database.DatabaseUtils.CONTACT_PERSON_NAME;
import static com.gdslinkasia.base.database.DatabaseUtils.CONTACT_PERSON_TYPE;
import static com.gdslinkasia.base.database.DatabaseUtils.CONTACT_TYPE;
import static com.gdslinkasia.base.database.DatabaseUtils.MOBILE_NO;
import static com.gdslinkasia.base.database.DatabaseUtils.MOBILE_PREFIX;
import static com.gdslinkasia.base.database.DatabaseUtils.TELEPHONE_AREA;
import static com.gdslinkasia.base.database.DatabaseUtils.TELEPHONE_NO;
import static com.gdslinkasia.base.database.DatabaseUtils.UNIQUE_ID;
import static com.gdslinkasia.base.utils.GlobalString.NULL_STRING_SETTER;

@Entity(foreignKeys = {
        @ForeignKey(entity = AppraisalRequest.class,
                parentColumns = UNIQUE_ID,
                childColumns = APPRAISAL_REQUEST_ID,
                onDelete = CASCADE)})
public class AppContactPerson {

//    @Expose(serialize = false, deserialize = false)
    @PrimaryKey(autoGenerate = true)
    @ColumnInfo(name = UNIQUE_ID)
    private transient long uniqueId;

//    @Expose(serialize = false, deserialize = false)
    @ColumnInfo(name = APPRAISAL_REQUEST_ID, index = true)
    private transient long appraisalRequestId;

    @ColumnInfo(name = CONTACT_PERSON_INDEX)
    @SerializedName("app_contact_person_index")
    @Expose
    private String appContactPersonIndex;

    @ColumnInfo(name = CONTACT_PERSON_NAME)
    @SerializedName("app_contact_person_name")
    @Expose
    private String appContactPersonName;

    @ColumnInfo(name = CONTACT_PERSON_TYPE)
    @SerializedName("app_contact_person_type")
    @Expose
    private String appContactPersonType;

    @ColumnInfo(name = CONTACT_TYPE)
    @SerializedName("app_contact_type")
    @Expose
    private String appContactType;

    @ColumnInfo(name = MOBILE_NO)
    @SerializedName("app_mobile_no")
    @Expose
    private String appMobileNo;

    @ColumnInfo(name = MOBILE_PREFIX)
    @SerializedName("app_mobile_prefix")
    @Expose
    private String appMobilePrefix;

    @ColumnInfo(name = TELEPHONE_AREA)
    @SerializedName("app_telephone_area")
    @Expose
    private String appTelephoneArea;

    @ColumnInfo(name = TELEPHONE_NO)
    @SerializedName("app_telephone_no")
    @Expose
    private String appTelephoneNo;

    public long getUniqueId() {
        return uniqueId;
    }

    public void setUniqueId(long uniqueId) {
        this.uniqueId = uniqueId;
    }

    public long getAppraisalRequestId() {
        return appraisalRequestId;
    }

    public void setAppraisalRequestId(long appraisalRequestId) {
        this.appraisalRequestId = appraisalRequestId;
    }

    public String getAppContactPersonIndex() {
        return NULL_STRING_SETTER(appContactPersonIndex);
    }

    public void setAppContactPersonIndex(String appContactPersonIndex) {
        this.appContactPersonIndex = appContactPersonIndex;
    }

    public String getAppContactPersonName() {
        return NULL_STRING_SETTER(appContactPersonName);
    }

    public void setAppContactPersonName(String appContactPersonName) {
        this.appContactPersonName = appContactPersonName;
    }

    public String getAppContactPersonType() {
        return NULL_STRING_SETTER(appContactPersonType);
    }

    public void setAppContactPersonType(String appContactPersonType) {
        this.appContactPersonType = appContactPersonType;
    }

    public String getAppContactType() {
        return NULL_STRING_SETTER(appContactType);
    }

    public void setAppContactType(String appContactType) {
        this.appContactType = appContactType;
    }

    public String getAppMobileNo() {
        return NULL_STRING_SETTER(appMobileNo);
    }

    public void setAppMobileNo(String appMobileNo) {
        this.appMobileNo = appMobileNo;
    }

    public String getAppMobilePrefix() {
        return NULL_STRING_SETTER(appMobilePrefix);
    }

    public void setAppMobilePrefix(String appMobilePrefix) {
        this.appMobilePrefix = appMobilePrefix;
    }

    public String getAppTelephoneArea() {
        return NULL_STRING_SETTER(appTelephoneArea);
    }

    public void setAppTelephoneArea(String appTelephoneArea) {
        this.appTelephoneArea = appTelephoneArea;
    }

    public String getAppTelephoneNo() {
        return NULL_STRING_SETTER(appTelephoneNo);
    }

    public void setAppTelephoneNo(String appTelephoneNo) {
        this.appTelephoneNo = appTelephoneNo;
    }
}
