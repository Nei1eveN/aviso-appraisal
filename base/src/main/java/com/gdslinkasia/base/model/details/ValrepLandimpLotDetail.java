
package com.gdslinkasia.base.model.details;

import androidx.room.ColumnInfo;
import androidx.room.Entity;
import androidx.room.ForeignKey;
import androidx.room.PrimaryKey;

import com.gdslinkasia.base.model.job.Record;
import com.google.gson.annotations.SerializedName;

import static androidx.room.ForeignKey.CASCADE;
import static com.gdslinkasia.base.database.DatabaseUtils.RECORD_ID;
import static com.gdslinkasia.base.database.DatabaseUtils.RECORD_UID;
import static com.gdslinkasia.base.database.DatabaseUtils.UNIQUE_ID;
import static com.gdslinkasia.base.utils.GlobalString.NULL_STRING_SETTER;

@SuppressWarnings("all")
@Entity(foreignKeys = {@ForeignKey(
        entity = Record.class,
        parentColumns = UNIQUE_ID,
        childColumns = RECORD_UID,
        onDelete = CASCADE)})
public class ValrepLandimpLotDetail {

//    @Expose(serialize = false, deserialize = false)
    @PrimaryKey(autoGenerate = true)
    @ColumnInfo(name = UNIQUE_ID)
    private transient long uniqueId;

    //TODO NON-SERIALIZED
//    @Expose(serialize = false, deserialize = false)
    @ColumnInfo(name = RECORD_ID, index = true)
    private transient String recordId;

//    @Expose(serialize = false, deserialize = false)
    @ColumnInfo(name = RECORD_UID, index = true)
    private transient long recordUId;

    @SerializedName("valrep_landimp_desc_ceiling_untit")
    private String valrepLandimpDescCeilingUntit;
    @SerializedName("valrep_landimp_desc_doors_unit")
    private String valrepLandimpDescDoorsUnit;
    @SerializedName("valrep_landimp_desc_imp_flooring_unit")
    private String valrepLandimpDescImpFlooringUnit;
    @SerializedName("valrep_landimp_desc_observed_condition_unit")
    private String valrepLandimpDescObservedConditionUnit;
    @SerializedName("valrep_landimp_desc_others_unit")
    private String valrepLandimpDescOthersUnit;
    @SerializedName("valrep_landimp_desc_partitions_unit")
    private String valrepLandimpDescPartitionsUnit;
    @SerializedName("valrep_landimp_propdesc_area")
    private String valrepLandimpPropdescArea;
    @SerializedName("valrep_landimp_propdesc_block")
    private String valrepLandimpPropdescBlock;
    @SerializedName("valrep_landimp_propdesc_deductions")
    private String valrepLandimpPropdescDeductions;
    @SerializedName("valrep_landimp_propdesc_deductions_desc")
    private String valrepLandimpPropdescDeductionsDesc;
    @SerializedName("valrep_landimp_propdesc_index")
    private String valrepLandimpPropdescIndex;
    @SerializedName("valrep_landimp_propdesc_lot")
    private String valrepLandimpPropdescLot;
    @SerializedName("valrep_landimp_propdesc_place")
    private String valrepLandimpPropdescPlace;
    @SerializedName("valrep_landimp_propdesc_registered_owner")
    private String valrepLandimpPropdescRegisteredOwner;
    @SerializedName("valrep_landimp_propdesc_registry_date_day")
    private String valrepLandimpPropdescRegistryDateDay;
    @SerializedName("valrep_landimp_propdesc_registry_date_month")
    private String valrepLandimpPropdescRegistryDateMonth;
    @SerializedName("valrep_landimp_propdesc_registry_date_year")
    private String valrepLandimpPropdescRegistryDateYear;
    @SerializedName("valrep_landimp_propdesc_registry_of_deeds")
    private String valrepLandimpPropdescRegistryOfDeeds;
    @SerializedName("valrep_landimp_propdesc_survey_nos")
    private String valrepLandimpPropdescSurveyNos;
    @SerializedName("valrep_landimp_propdesc_tct_no")
    private String valrepLandimpPropdescTctNo;
    @SerializedName("valrep_landimp_propdesc_title_type")
    private String valrepLandimpPropdescTitleType;
    @SerializedName("valrep_landimp_tax_dec_classification")
    private String valrepLandimpTaxDecClassification;
    @SerializedName("valrep_landimp_tax_dec_declarant")
    private String valrepLandimpTaxDecDeclarant;
    @SerializedName("valrep_landimp_tax_dec_imp")
    private String valrepLandimpTaxDecImp;
    @SerializedName("valrep_landimp_tax_dec_lot")
    private String valrepLandimpTaxDecLot;
    @SerializedName("valrep_landimp_tax_dec_lot_no")
    private String valrepLandimpTaxDecLotNo;

    public long getUniqueId() {
        return uniqueId;
    }

    public void setUniqueId(long uniqueId) {
        this.uniqueId = uniqueId;
    }

    public String getRecordId() {
        return recordId;
    }

    public void setRecordId(String recordId) {
        this.recordId = recordId;
    }

    public long getRecordUId() {
        return recordUId;
    }

    public void setRecordUId(long recordUId) {
        this.recordUId = recordUId;
    }

    public String getValrepLandimpDescCeilingUntit() {
        return NULL_STRING_SETTER(valrepLandimpDescCeilingUntit);
    }

    public void setValrepLandimpDescCeilingUntit(String valrepLandimpDescCeilingUntit) {
        this.valrepLandimpDescCeilingUntit = valrepLandimpDescCeilingUntit;
    }

    public String getValrepLandimpDescDoorsUnit() {
        return NULL_STRING_SETTER(valrepLandimpDescDoorsUnit);
    }

    public void setValrepLandimpDescDoorsUnit(String valrepLandimpDescDoorsUnit) {
        this.valrepLandimpDescDoorsUnit = valrepLandimpDescDoorsUnit;
    }

    public String getValrepLandimpDescImpFlooringUnit() {
        return NULL_STRING_SETTER(valrepLandimpDescImpFlooringUnit);
    }

    public void setValrepLandimpDescImpFlooringUnit(String valrepLandimpDescImpFlooringUnit) {
        this.valrepLandimpDescImpFlooringUnit = valrepLandimpDescImpFlooringUnit;
    }

    public String getValrepLandimpDescObservedConditionUnit() {
        return NULL_STRING_SETTER(valrepLandimpDescObservedConditionUnit);
    }

    public void setValrepLandimpDescObservedConditionUnit(String valrepLandimpDescObservedConditionUnit) {
        this.valrepLandimpDescObservedConditionUnit = valrepLandimpDescObservedConditionUnit;
    }

    public String getValrepLandimpDescOthersUnit() {
        return NULL_STRING_SETTER(valrepLandimpDescOthersUnit);
    }

    public void setValrepLandimpDescOthersUnit(String valrepLandimpDescOthersUnit) {
        this.valrepLandimpDescOthersUnit = valrepLandimpDescOthersUnit;
    }

    public String getValrepLandimpDescPartitionsUnit() {
        return NULL_STRING_SETTER(valrepLandimpDescPartitionsUnit);
    }

    public void setValrepLandimpDescPartitionsUnit(String valrepLandimpDescPartitionsUnit) {
        this.valrepLandimpDescPartitionsUnit = valrepLandimpDescPartitionsUnit;
    }

    public String getValrepLandimpPropdescArea() {
        return NULL_STRING_SETTER(valrepLandimpPropdescArea);
    }

    public void setValrepLandimpPropdescArea(String valrepLandimpPropdescArea) {
        this.valrepLandimpPropdescArea = valrepLandimpPropdescArea;
    }

    public String getValrepLandimpPropdescBlock() {
        return NULL_STRING_SETTER(valrepLandimpPropdescBlock);
    }

    public void setValrepLandimpPropdescBlock(String valrepLandimpPropdescBlock) {
        this.valrepLandimpPropdescBlock = valrepLandimpPropdescBlock;
    }

    public String getValrepLandimpPropdescDeductions() {
        return NULL_STRING_SETTER(valrepLandimpPropdescDeductions);
    }

    public void setValrepLandimpPropdescDeductions(String valrepLandimpPropdescDeductions) {
        this.valrepLandimpPropdescDeductions = valrepLandimpPropdescDeductions;
    }

    public String getValrepLandimpPropdescDeductionsDesc() {
        return NULL_STRING_SETTER(valrepLandimpPropdescDeductionsDesc);
    }

    public void setValrepLandimpPropdescDeductionsDesc(String valrepLandimpPropdescDeductionsDesc) {
        this.valrepLandimpPropdescDeductionsDesc = valrepLandimpPropdescDeductionsDesc;
    }

    public String getValrepLandimpPropdescIndex() {
        return NULL_STRING_SETTER(valrepLandimpPropdescIndex);
    }

    public void setValrepLandimpPropdescIndex(String valrepLandimpPropdescIndex) {
        this.valrepLandimpPropdescIndex = valrepLandimpPropdescIndex;
    }

    public String getValrepLandimpPropdescLot() {
        return NULL_STRING_SETTER(valrepLandimpPropdescLot);
    }

    public void setValrepLandimpPropdescLot(String valrepLandimpPropdescLot) {
        this.valrepLandimpPropdescLot = valrepLandimpPropdescLot;
    }

    public String getValrepLandimpPropdescPlace() {
        return NULL_STRING_SETTER(valrepLandimpPropdescPlace);
    }

    public void setValrepLandimpPropdescPlace(String valrepLandimpPropdescPlace) {
        this.valrepLandimpPropdescPlace = valrepLandimpPropdescPlace;
    }

    public String getValrepLandimpPropdescRegisteredOwner() {
        return NULL_STRING_SETTER(valrepLandimpPropdescRegisteredOwner);
    }

    public void setValrepLandimpPropdescRegisteredOwner(String valrepLandimpPropdescRegisteredOwner) {
        this.valrepLandimpPropdescRegisteredOwner = valrepLandimpPropdescRegisteredOwner;
    }

    public String getValrepLandimpPropdescRegistryDateDay() {
        return NULL_STRING_SETTER(valrepLandimpPropdescRegistryDateDay);
    }

    public void setValrepLandimpPropdescRegistryDateDay(String valrepLandimpPropdescRegistryDateDay) {
        this.valrepLandimpPropdescRegistryDateDay = valrepLandimpPropdescRegistryDateDay;
    }

    public String getValrepLandimpPropdescRegistryDateMonth() {
        return NULL_STRING_SETTER(valrepLandimpPropdescRegistryDateMonth);
    }

    public void setValrepLandimpPropdescRegistryDateMonth(String valrepLandimpPropdescRegistryDateMonth) {
        this.valrepLandimpPropdescRegistryDateMonth = valrepLandimpPropdescRegistryDateMonth;
    }

    public String getValrepLandimpPropdescRegistryDateYear() {
        return NULL_STRING_SETTER(valrepLandimpPropdescRegistryDateYear);
    }

    public void setValrepLandimpPropdescRegistryDateYear(String valrepLandimpPropdescRegistryDateYear) {
        this.valrepLandimpPropdescRegistryDateYear = valrepLandimpPropdescRegistryDateYear;
    }

    public String getValrepLandimpPropdescRegistryOfDeeds() {
        return NULL_STRING_SETTER(valrepLandimpPropdescRegistryOfDeeds);
    }

    public void setValrepLandimpPropdescRegistryOfDeeds(String valrepLandimpPropdescRegistryOfDeeds) {
        this.valrepLandimpPropdescRegistryOfDeeds = valrepLandimpPropdescRegistryOfDeeds;
    }

    public String getValrepLandimpPropdescSurveyNos() {
        return NULL_STRING_SETTER(valrepLandimpPropdescSurveyNos);
    }

    public void setValrepLandimpPropdescSurveyNos(String valrepLandimpPropdescSurveyNos) {
        this.valrepLandimpPropdescSurveyNos = valrepLandimpPropdescSurveyNos;
    }

    public String getValrepLandimpPropdescTctNo() {
        return NULL_STRING_SETTER(valrepLandimpPropdescTctNo);
    }

    public void setValrepLandimpPropdescTctNo(String valrepLandimpPropdescTctNo) {
        this.valrepLandimpPropdescTctNo = valrepLandimpPropdescTctNo;
    }

    public String getValrepLandimpPropdescTitleType() {
        return NULL_STRING_SETTER(valrepLandimpPropdescTitleType);
    }

    public void setValrepLandimpPropdescTitleType(String valrepLandimpPropdescTitleType) {
        this.valrepLandimpPropdescTitleType = valrepLandimpPropdescTitleType;
    }

    public String getValrepLandimpTaxDecClassification() {
        return NULL_STRING_SETTER(valrepLandimpTaxDecClassification);
    }

    public void setValrepLandimpTaxDecClassification(String valrepLandimpTaxDecClassification) {
        this.valrepLandimpTaxDecClassification = valrepLandimpTaxDecClassification;
    }

    public String getValrepLandimpTaxDecDeclarant() {
        return NULL_STRING_SETTER(valrepLandimpTaxDecDeclarant);
    }

    public void setValrepLandimpTaxDecDeclarant(String valrepLandimpTaxDecDeclarant) {
        this.valrepLandimpTaxDecDeclarant = valrepLandimpTaxDecDeclarant;
    }

    public String getValrepLandimpTaxDecImp() {
        return NULL_STRING_SETTER(valrepLandimpTaxDecImp);
    }

    public void setValrepLandimpTaxDecImp(String valrepLandimpTaxDecImp) {
        this.valrepLandimpTaxDecImp = valrepLandimpTaxDecImp;
    }

    public String getValrepLandimpTaxDecLot() {
        return NULL_STRING_SETTER(valrepLandimpTaxDecLot);
    }

    public void setValrepLandimpTaxDecLot(String valrepLandimpTaxDecLot) {
        this.valrepLandimpTaxDecLot = valrepLandimpTaxDecLot;
    }

    public String getValrepLandimpTaxDecLotNo() {
        return NULL_STRING_SETTER(valrepLandimpTaxDecLotNo);
    }

    public void setValrepLandimpTaxDecLotNo(String valrepLandimpTaxDecLotNo) {
        this.valrepLandimpTaxDecLotNo = valrepLandimpTaxDecLotNo;
    }

}
