
package com.gdslinkasia.base.model.details;

import androidx.room.ColumnInfo;
import androidx.room.Entity;
import androidx.room.ForeignKey;
import androidx.room.PrimaryKey;

import com.google.gson.annotations.SerializedName;

import static androidx.room.ForeignKey.CASCADE;
import static com.gdslinkasia.base.database.DatabaseUtils.LANDIMP_LOT_VALUATION_ID;
import static com.gdslinkasia.base.database.DatabaseUtils.OWNERSHIP_PROPERTY_DESC_ID;
import static com.gdslinkasia.base.database.DatabaseUtils.RECORD_ID;
import static com.gdslinkasia.base.database.DatabaseUtils.UNIQUE_ID;
import static com.gdslinkasia.base.utils.GlobalString.NULL_STRING_SETTER;

@SuppressWarnings("unused")
@Entity(
        foreignKeys = {
                @ForeignKey(
                        entity = ValrepLandimpLotValuation.class,
                        parentColumns = UNIQUE_ID,
                        childColumns = LANDIMP_LOT_VALUATION_ID,
                        onDelete = CASCADE),
                @ForeignKey(
                        entity = ValrepLandimpLotDetail.class,
                        parentColumns = UNIQUE_ID,
                        childColumns = OWNERSHIP_PROPERTY_DESC_ID,
                        onDelete = CASCADE)
        })
public class ValrepLandimpLotValuationDetail {

    //    @Expose(serialize = false, deserialize = false)
    @PrimaryKey(autoGenerate = true)
    @ColumnInfo(name = UNIQUE_ID)
    private transient long uniqueId;

    //    @Expose(serialize = false, deserialize = false)
    @ColumnInfo(name = LANDIMP_LOT_VALUATION_ID, index = true)
    private transient long lotValuationId;

    @ColumnInfo(name = OWNERSHIP_PROPERTY_DESC_ID, index = true)
    private transient long ownershipPropertyDescId;

    //TODO NON-SERIALIZED
//    @Expose(serialize = false, deserialize = false)
    @ColumnInfo(name = RECORD_ID, index = true)
    private transient String recordId;

    @SerializedName("list_separator")
    private String listSeparator;
    @SerializedName("valrep_landimp_land_value_appraised_mv")
    private String valrepLandimpLandValueAppraisedMv;
    @SerializedName("valrep_landimp_land_value_insurance_value")
    private String valrepLandimpLandValueInsuranceValue;
    @SerializedName("valrep_landimp_land_value_loan_value")
    private String valrepLandimpLandValueLoanValue;
    @SerializedName("valrep_landimp_land_value_per_of_mv")
    private String valrepLandimpLandValuePerOfMv;
    @SerializedName("valrep_landimp_value_area")
    private String valrepLandimpValueArea;
    @SerializedName("valrep_landimp_value_block_no")
    private String valrepLandimpValueBlockNo;
    @SerializedName("valrep_landimp_value_deduction")
    private String valrepLandimpValueDeduction;
    @SerializedName("valrep_landimp_value_land_value")
    private String valrepLandimpValueLandValue;
    @SerializedName("valrep_landimp_value_lot_no")
    private String valrepLandimpValueLotNo;
    @SerializedName("valrep_landimp_value_net_area")
    private String valrepLandimpValueNetArea;
    @SerializedName("valrep_landimp_value_tct_no")
    private String valrepLandimpValueTctNo;
    @SerializedName("valrep_landimp_value_unit_value")
    private String valrepLandimpValueUnitValue;

    public long getUniqueId() {
        return uniqueId;
    }

    public void setUniqueId(long uniqueId) {
        this.uniqueId = uniqueId;
    }

    public long getLotValuationId() {
        return lotValuationId;
    }

    public void setLotValuationId(long lotValuationId) {
        this.lotValuationId = lotValuationId;
    }

    public long getOwnershipPropertyDescId() {
        return ownershipPropertyDescId;
    }

    public void setOwnershipPropertyDescId(long ownershipPropertyDescId) {
        this.ownershipPropertyDescId = ownershipPropertyDescId;
    }

    public String getRecordId() {
        return recordId;
    }

    public void setRecordId(String recordId) {
        this.recordId = recordId;
    }

    public String getListSeparator() {
        return NULL_STRING_SETTER(listSeparator);
    }

    public void setListSeparator(String listSeparator) {
        this.listSeparator = listSeparator;
    }

    public String getValrepLandimpLandValueAppraisedMv() {
        return NULL_STRING_SETTER(valrepLandimpLandValueAppraisedMv);
    }

    public void setValrepLandimpLandValueAppraisedMv(String valrepLandimpLandValueAppraisedMv) {
        this.valrepLandimpLandValueAppraisedMv = valrepLandimpLandValueAppraisedMv;
    }

    public String getValrepLandimpLandValueInsuranceValue() {
        return NULL_STRING_SETTER(valrepLandimpLandValueInsuranceValue);
    }

    public void setValrepLandimpLandValueInsuranceValue(String valrepLandimpLandValueInsuranceValue) {
        this.valrepLandimpLandValueInsuranceValue = valrepLandimpLandValueInsuranceValue;
    }

    public String getValrepLandimpLandValueLoanValue() {
        return NULL_STRING_SETTER(valrepLandimpLandValueLoanValue);
    }

    public void setValrepLandimpLandValueLoanValue(String valrepLandimpLandValueLoanValue) {
        this.valrepLandimpLandValueLoanValue = valrepLandimpLandValueLoanValue;
    }

    public String getValrepLandimpLandValuePerOfMv() {
        return NULL_STRING_SETTER(valrepLandimpLandValuePerOfMv);
    }

    public void setValrepLandimpLandValuePerOfMv(String valrepLandimpLandValuePerOfMv) {
        this.valrepLandimpLandValuePerOfMv = valrepLandimpLandValuePerOfMv;
    }

    public String getValrepLandimpValueArea() {
        return NULL_STRING_SETTER(valrepLandimpValueArea);
    }

    public void setValrepLandimpValueArea(String valrepLandimpValueArea) {
        this.valrepLandimpValueArea = valrepLandimpValueArea;
    }

    public String getValrepLandimpValueBlockNo() {
        return NULL_STRING_SETTER(valrepLandimpValueBlockNo);
    }

    public void setValrepLandimpValueBlockNo(String valrepLandimpValueBlockNo) {
        this.valrepLandimpValueBlockNo = valrepLandimpValueBlockNo;
    }

    public String getValrepLandimpValueDeduction() {
        return NULL_STRING_SETTER(valrepLandimpValueDeduction);
    }

    public void setValrepLandimpValueDeduction(String valrepLandimpValueDeduction) {
        this.valrepLandimpValueDeduction = valrepLandimpValueDeduction;
    }

    public String getValrepLandimpValueLandValue() {
        return NULL_STRING_SETTER(valrepLandimpValueLandValue);
    }

    public void setValrepLandimpValueLandValue(String valrepLandimpValueLandValue) {
        this.valrepLandimpValueLandValue = valrepLandimpValueLandValue;
    }

    public String getValrepLandimpValueLotNo() {
        return NULL_STRING_SETTER(valrepLandimpValueLotNo);
    }

    public void setValrepLandimpValueLotNo(String valrepLandimpValueLotNo) {
        this.valrepLandimpValueLotNo = valrepLandimpValueLotNo;
    }

    public String getValrepLandimpValueNetArea() {
        return NULL_STRING_SETTER(valrepLandimpValueNetArea);
    }

    public void setValrepLandimpValueNetArea(String valrepLandimpValueNetArea) {
        this.valrepLandimpValueNetArea = valrepLandimpValueNetArea;
    }

    public String getValrepLandimpValueTctNo() {
        return NULL_STRING_SETTER(valrepLandimpValueTctNo);
    }

    public void setValrepLandimpValueTctNo(String valrepLandimpValueTctNo) {
        this.valrepLandimpValueTctNo = valrepLandimpValueTctNo;
    }

    public String getValrepLandimpValueUnitValue() {
        return NULL_STRING_SETTER(valrepLandimpValueUnitValue);
    }

    public void setValrepLandimpValueUnitValue(String valrepLandimpValueUnitValue) {
        this.valrepLandimpValueUnitValue = valrepLandimpValueUnitValue;
    }

}
