
package com.gdslinkasia.base.model.details;

import androidx.room.ColumnInfo;
import androidx.room.Entity;
import androidx.room.ForeignKey;
import androidx.room.PrimaryKey;

import com.gdslinkasia.base.model.job.Record;
import com.google.gson.annotations.SerializedName;

import static androidx.room.ForeignKey.CASCADE;
import static com.gdslinkasia.base.database.DatabaseUtils.LANDIMP_OTHER_DESC_IMP_ID;
import static com.gdslinkasia.base.database.DatabaseUtils.RECORD_ID;
import static com.gdslinkasia.base.database.DatabaseUtils.RECORD_UID;
import static com.gdslinkasia.base.database.DatabaseUtils.UNIQUE_ID;
import static com.gdslinkasia.base.utils.GlobalString.NULL_STRING_SETTER;


@SuppressWarnings("unused")
@Entity(foreignKeys = {
        @ForeignKey(
                entity = Record.class,
                parentColumns = UNIQUE_ID,
                childColumns = RECORD_UID,
                onDelete = CASCADE),
        @ForeignKey(
                entity = ValrepLandimpOtherLandImpDetail.class,
                parentColumns = UNIQUE_ID,
                childColumns = LANDIMP_OTHER_DESC_IMP_ID,
                onDelete = CASCADE)})
public class ValrepLandimpOtherLandImpValue {

    //    @Expose(serialize = false, deserialize = false)
    @PrimaryKey(autoGenerate = true)
    @ColumnInfo(name = UNIQUE_ID)
    private transient long uniqueId;

    //TODO NON-SERIALIZED
//    @Expose(serialize = false, deserialize = false)
    @ColumnInfo(name = RECORD_ID, index = true)
    private transient String recordId;

    //    @Expose(serialize = false, deserialize = false)
    @ColumnInfo(name = RECORD_UID, index = true)
    private transient long recordUId;

    @ColumnInfo(name = LANDIMP_OTHER_DESC_IMP_ID, index = true)
    private transient long otherDescImpId;

    @SerializedName("valrep_landimp_other_land_imp_value_area")
    private String valrepLandimpOtherLandImpValueArea;
    @SerializedName("valrep_landimp_other_land_imp_value_area_unit")
    private String valrepLandimpOtherLandImpValueAreaUnit;
    @SerializedName("valrep_landimp_other_land_imp_value_depreciated_value")
    private String valrepLandimpOtherLandImpValueDepreciatedValue;
    @SerializedName("valrep_landimp_other_land_imp_value_depreciation")
    private String valrepLandimpOtherLandImpValueDepreciation;
    @SerializedName("valrep_landimp_other_land_imp_value_desc")
    private String valrepLandimpOtherLandImpValueDesc;
    @SerializedName("valrep_landimp_other_land_imp_value_economic_life")
    private String valrepLandimpOtherLandImpValueEconomicLife;
    @SerializedName("valrep_landimp_other_land_imp_value_effective_age")
    private String valrepLandimpOtherLandImpValueEffectiveAge;
    @SerializedName("valrep_landimp_other_land_imp_value_percentage")
    private String valrepLandimpOtherLandImpValuePercentage;
    @SerializedName("valrep_landimp_other_land_imp_value_rcn")
    private String valrepLandimpOtherLandImpValueRcn;
    @SerializedName("valrep_landimp_other_land_imp_value_remain_life")
    private String valrepLandimpOtherLandImpValueRemainLife;
    @SerializedName("valrep_landimp_other_land_imp_value_type")
    private String valrepLandimpOtherLandImpValueType;
    @SerializedName("valrep_landimp_other_land_imp_value_unit_value")
    private String valrepLandimpOtherLandImpValueUnitValue;

    public long getUniqueId() {
        return uniqueId;
    }

    public void setUniqueId(long uniqueId) {
        this.uniqueId = uniqueId;
    }

    public String getRecordId() {
        return recordId;
    }

    public void setRecordId(String recordId) {
        this.recordId = recordId;
    }

    public long getRecordUId() {
        return recordUId;
    }

    public void setRecordUId(long recordUId) {
        this.recordUId = recordUId;
    }

    public long getOtherDescImpId() {
        return otherDescImpId;
    }

    public void setOtherDescImpId(long otherDescImpId) {
        this.otherDescImpId = otherDescImpId;
    }

    public String getValrepLandimpOtherLandImpValueArea() {
        return NULL_STRING_SETTER(valrepLandimpOtherLandImpValueArea);
    }

    public void setValrepLandimpOtherLandImpValueArea(String valrepLandimpOtherLandImpValueArea) {
        this.valrepLandimpOtherLandImpValueArea = valrepLandimpOtherLandImpValueArea;
    }

    public String getValrepLandimpOtherLandImpValueAreaUnit() {
        return NULL_STRING_SETTER(valrepLandimpOtherLandImpValueAreaUnit);
    }

    public void setValrepLandimpOtherLandImpValueAreaUnit(String valrepLandimpOtherLandImpValueAreaUnit) {
        this.valrepLandimpOtherLandImpValueAreaUnit = valrepLandimpOtherLandImpValueAreaUnit;
    }

    public String getValrepLandimpOtherLandImpValueDepreciatedValue() {
        return NULL_STRING_SETTER(valrepLandimpOtherLandImpValueDepreciatedValue);
    }

    public void setValrepLandimpOtherLandImpValueDepreciatedValue(String valrepLandimpOtherLandImpValueDepreciatedValue) {
        this.valrepLandimpOtherLandImpValueDepreciatedValue = valrepLandimpOtherLandImpValueDepreciatedValue;
    }

    public String getValrepLandimpOtherLandImpValueDepreciation() {
        return NULL_STRING_SETTER(valrepLandimpOtherLandImpValueDepreciation);
    }

    public void setValrepLandimpOtherLandImpValueDepreciation(String valrepLandimpOtherLandImpValueDepreciation) {
        this.valrepLandimpOtherLandImpValueDepreciation = valrepLandimpOtherLandImpValueDepreciation;
    }

    public String getValrepLandimpOtherLandImpValueDesc() {
        return NULL_STRING_SETTER(valrepLandimpOtherLandImpValueDesc);
    }

    public void setValrepLandimpOtherLandImpValueDesc(String valrepLandimpOtherLandImpValueDesc) {
        this.valrepLandimpOtherLandImpValueDesc = valrepLandimpOtherLandImpValueDesc;
    }

    public String getValrepLandimpOtherLandImpValueEconomicLife() {
        return NULL_STRING_SETTER(valrepLandimpOtherLandImpValueEconomicLife);
    }

    public void setValrepLandimpOtherLandImpValueEconomicLife(String valrepLandimpOtherLandImpValueEconomicLife) {
        this.valrepLandimpOtherLandImpValueEconomicLife = valrepLandimpOtherLandImpValueEconomicLife;
    }

    public String getValrepLandimpOtherLandImpValueEffectiveAge() {
        return NULL_STRING_SETTER(valrepLandimpOtherLandImpValueEffectiveAge);
    }

    public void setValrepLandimpOtherLandImpValueEffectiveAge(String valrepLandimpOtherLandImpValueEffectiveAge) {
        this.valrepLandimpOtherLandImpValueEffectiveAge = valrepLandimpOtherLandImpValueEffectiveAge;
    }

    public String getValrepLandimpOtherLandImpValuePercentage() {
        return NULL_STRING_SETTER(valrepLandimpOtherLandImpValuePercentage);
    }

    public void setValrepLandimpOtherLandImpValuePercentage(String valrepLandimpOtherLandImpValuePercentage) {
        this.valrepLandimpOtherLandImpValuePercentage = valrepLandimpOtherLandImpValuePercentage;
    }

    public String getValrepLandimpOtherLandImpValueRcn() {
        return NULL_STRING_SETTER(valrepLandimpOtherLandImpValueRcn);
    }

    public void setValrepLandimpOtherLandImpValueRcn(String valrepLandimpOtherLandImpValueRcn) {
        this.valrepLandimpOtherLandImpValueRcn = valrepLandimpOtherLandImpValueRcn;
    }

    public String getValrepLandimpOtherLandImpValueRemainLife() {
        return NULL_STRING_SETTER(valrepLandimpOtherLandImpValueRemainLife);
    }

    public void setValrepLandimpOtherLandImpValueRemainLife(String valrepLandimpOtherLandImpValueRemainLife) {
        this.valrepLandimpOtherLandImpValueRemainLife = valrepLandimpOtherLandImpValueRemainLife;
    }

    public String getValrepLandimpOtherLandImpValueType() {
        return NULL_STRING_SETTER(valrepLandimpOtherLandImpValueType);
    }

    public void setValrepLandimpOtherLandImpValueType(String valrepLandimpOtherLandImpValueType) {
        this.valrepLandimpOtherLandImpValueType = valrepLandimpOtherLandImpValueType;
    }

    public String getValrepLandimpOtherLandImpValueUnitValue() {
        return NULL_STRING_SETTER(valrepLandimpOtherLandImpValueUnitValue);
    }

    public void setValrepLandimpOtherLandImpValueUnitValue(String valrepLandimpOtherLandImpValueUnitValue) {
        this.valrepLandimpOtherLandImpValueUnitValue = valrepLandimpOtherLandImpValueUnitValue;
    }

}
