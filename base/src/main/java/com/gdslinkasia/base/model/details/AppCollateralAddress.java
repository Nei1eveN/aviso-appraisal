package com.gdslinkasia.base.model.details;

import androidx.room.ColumnInfo;
import androidx.room.Entity;
import androidx.room.ForeignKey;
import androidx.room.PrimaryKey;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import static androidx.room.ForeignKey.CASCADE;
import static com.gdslinkasia.base.database.DatabaseUtils.ADDR_TYPE;
import static com.gdslinkasia.base.database.DatabaseUtils.APPRAISAL_REQUEST_ID;
import static com.gdslinkasia.base.database.DatabaseUtils.BLDG;
import static com.gdslinkasia.base.database.DatabaseUtils.BLOCK_NO;
import static com.gdslinkasia.base.database.DatabaseUtils.BRGY;
import static com.gdslinkasia.base.database.DatabaseUtils.CITY;
import static com.gdslinkasia.base.database.DatabaseUtils.COUNTRY;
import static com.gdslinkasia.base.database.DatabaseUtils.DISTRICT;
import static com.gdslinkasia.base.database.DatabaseUtils.LATITUDE;
import static com.gdslinkasia.base.database.DatabaseUtils.LOCATION_LAT;
import static com.gdslinkasia.base.database.DatabaseUtils.LOCATION_LONG;
import static com.gdslinkasia.base.database.DatabaseUtils.LONGITUDE;
import static com.gdslinkasia.base.database.DatabaseUtils.LOT_NO;
import static com.gdslinkasia.base.database.DatabaseUtils.PROVINCE;
import static com.gdslinkasia.base.database.DatabaseUtils.REGION;
import static com.gdslinkasia.base.database.DatabaseUtils.STREET_NAME;
import static com.gdslinkasia.base.database.DatabaseUtils.STREET_NO;
import static com.gdslinkasia.base.database.DatabaseUtils.TCT_NO;
import static com.gdslinkasia.base.database.DatabaseUtils.UNIQUE_ID;
import static com.gdslinkasia.base.database.DatabaseUtils.UNIT_NO;
import static com.gdslinkasia.base.database.DatabaseUtils.VILLAGE;
import static com.gdslinkasia.base.database.DatabaseUtils.ZIP;
import static com.gdslinkasia.base.utils.GlobalString.NULL_STRING_SETTER;

@Entity(foreignKeys = {
        @ForeignKey(entity = AppraisalRequest.class,
                parentColumns = UNIQUE_ID,
                childColumns = APPRAISAL_REQUEST_ID,
                onDelete = CASCADE, onUpdate = CASCADE)})
public class AppCollateralAddress {

//    @Expose(serialize = false, deserialize = false)
    @PrimaryKey(autoGenerate = true)
    @ColumnInfo(name = UNIQUE_ID)
    private transient long uniqueId;

//    @Expose(serialize = false, deserialize = false)
    @ColumnInfo(name = APPRAISAL_REQUEST_ID, index = true)
    private transient long appraisalRequestId;

    @ColumnInfo(name = ADDR_TYPE)
    @SerializedName("app_address_type")
    @Expose
    private String appAddressType;

    @ColumnInfo(name = BLDG)
    @SerializedName("app_bldg")
    @Expose
    private String appBldg;

    @ColumnInfo(name = BLOCK_NO)
    @SerializedName("app_block_no")
    @Expose
    private String appBlockNo;

    @ColumnInfo(name = BRGY)
    @SerializedName("app_brgy")
    @Expose
    private String appBrgy;

    @ColumnInfo(name = CITY)
    @SerializedName("app_city")
    @Expose
    private String appCity;

    @ColumnInfo(name = COUNTRY)
    @SerializedName("app_country")
    @Expose
    private String appCountry;

    @ColumnInfo(name = DISTRICT)
    @SerializedName("app_district")
    @Expose
    private String appDistrict;

    @ColumnInfo(name = LATITUDE)
    @SerializedName("app_latitude")
    @Expose
    private String appLatitude;

    @ColumnInfo(name = LOCATION_LAT)
    @SerializedName("app_location_latitude")
    @Expose
    private String appLocationLatitude;

    @ColumnInfo(name = LOCATION_LONG)
    @SerializedName("app_location_longitude")
    @Expose
    private String appLocationLongitude;

    @ColumnInfo(name = LONGITUDE)
    @SerializedName("app_longitude")
    @Expose
    private String appLongitude;

    @ColumnInfo(name = LOT_NO)
    @SerializedName("app_lot_no")
    @Expose
    private String appLotNo;

    @ColumnInfo(name = PROVINCE)
    @SerializedName("app_province")
    @Expose
    private String appProvince;

    @ColumnInfo(name = REGION)
    @SerializedName("app_region")
    @Expose
    private String appRegion;

    @ColumnInfo(name = STREET_NAME)
    @SerializedName("app_street_name")
    @Expose
    private String appStreetName;

    @ColumnInfo(name = STREET_NO)
    @SerializedName("app_street_no")
    @Expose
    private String appStreetNo;

    @ColumnInfo(name = TCT_NO)
    @SerializedName("app_tct_no")
    @Expose
    private String appTctNo;

    @ColumnInfo(name = UNIT_NO)
    @SerializedName("app_unit_no")
    @Expose
    private String appUnitNo;

    @ColumnInfo(name = VILLAGE)
    @SerializedName("app_village")
    @Expose
    private String appVillage;

    @ColumnInfo(name = ZIP)
    @SerializedName("app_zip")
    @Expose
    private String appZip;

    public long getUniqueId() {
        return uniqueId;
    }

    public void setUniqueId(long uniqueId) {
        this.uniqueId = uniqueId;
    }

    public long getAppraisalRequestId() {
        return appraisalRequestId;
    }

    public void setAppraisalRequestId(long appraisalRequestId) {
        this.appraisalRequestId = appraisalRequestId;
    }

    public String getAppAddressType() {
        return NULL_STRING_SETTER(appAddressType);
    }

    public void setAppAddressType(String appAddressType) {
        this.appAddressType = appAddressType;
    }

    public String getAppBldg() {
        return NULL_STRING_SETTER(appBldg);
    }

    public void setAppBldg(String appBldg) {
        this.appBldg = appBldg;
    }

    public String getAppBlockNo() {
        return NULL_STRING_SETTER(appBlockNo);
    }

    public void setAppBlockNo(String appBlockNo) {
        this.appBlockNo = appBlockNo;
    }

    public String getAppBrgy() {
        return NULL_STRING_SETTER(appBrgy);
    }

    public void setAppBrgy(String appBrgy) {
        this.appBrgy = appBrgy;
    }

    public String getAppCity() {
        return NULL_STRING_SETTER(appCity);
    }

    public void setAppCity(String appCity) {
        this.appCity = appCity;
    }

    public String getAppCountry() {
        return NULL_STRING_SETTER(appCountry);
    }

    public void setAppCountry(String appCountry) {
        this.appCountry = appCountry;
    }

    public String getAppDistrict() {
        return NULL_STRING_SETTER(appDistrict);
    }

    public void setAppDistrict(String appDistrict) {
        this.appDistrict = appDistrict;
    }

    public String getAppLatitude() {
        return NULL_STRING_SETTER(appLatitude);
    }

    public void setAppLatitude(String appLatitude) {
        this.appLatitude = appLatitude;
    }

    public String getAppLocationLatitude() {
        return NULL_STRING_SETTER(appLocationLatitude);
    }

    public void setAppLocationLatitude(String appLocationLatitude) {
        this.appLocationLatitude = appLocationLatitude;
    }

    public String getAppLocationLongitude() {
        return NULL_STRING_SETTER(appLocationLongitude);
    }

    public void setAppLocationLongitude(String appLocationLongitude) {
        this.appLocationLongitude = appLocationLongitude;
    }

    public String getAppLongitude() {
        return NULL_STRING_SETTER(appLongitude);
    }

    public void setAppLongitude(String appLongitude) {
        this.appLongitude = appLongitude;
    }

    public String getAppLotNo() {
        return NULL_STRING_SETTER(appLotNo);
    }

    public void setAppLotNo(String appLotNo) {
        this.appLotNo = appLotNo;
    }

    public String getAppProvince() {
        return NULL_STRING_SETTER(appProvince);
    }

    public void setAppProvince(String appProvince) {
        this.appProvince = appProvince;
    }

    public String getAppRegion() {
        return NULL_STRING_SETTER(appRegion);
    }

    public void setAppRegion(String appRegion) {
        this.appRegion = appRegion;
    }

    public String getAppStreetName() {
        return NULL_STRING_SETTER(appStreetName);
    }

    public void setAppStreetName(String appStreetName) {
        this.appStreetName = appStreetName;
    }

    public String getAppStreetNo() {
        return NULL_STRING_SETTER(appStreetNo);
    }

    public void setAppStreetNo(String appStreetNo) {
        this.appStreetNo = appStreetNo;
    }

    public String getAppTctNo() {
        return NULL_STRING_SETTER(appTctNo);
    }

    public void setAppTctNo(String appTctNo) {
        this.appTctNo = appTctNo;
    }

    public String getAppUnitNo() {
        return NULL_STRING_SETTER(appUnitNo);
    }

    public void setAppUnitNo(String appUnitNo) {
        this.appUnitNo = appUnitNo;
    }

    public String getAppVillage() {
        return NULL_STRING_SETTER(appVillage);
    }

    public void setAppVillage(String appVillage) {
        this.appVillage = appVillage;
    }

    public String getAppZip() {
        return NULL_STRING_SETTER(appZip);
    }

    public void setAppZip(String appZip) {
        this.appZip = appZip;
    }
}
