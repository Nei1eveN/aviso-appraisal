
package com.gdslinkasia.base.model.details;

import androidx.room.ColumnInfo;
import androidx.room.Entity;
import androidx.room.ForeignKey;
import androidx.room.PrimaryKey;

import com.gdslinkasia.base.model.job.Record;
import com.google.gson.annotations.SerializedName;

import static androidx.room.ForeignKey.CASCADE;
import static com.gdslinkasia.base.database.DatabaseUtils.RECORD_ID;
import static com.gdslinkasia.base.database.DatabaseUtils.RECORD_UID;
import static com.gdslinkasia.base.database.DatabaseUtils.UNIQUE_ID;
import static com.gdslinkasia.base.utils.GlobalString.NULL_STRING_SETTER;

@SuppressWarnings("all")
@Entity(foreignKeys = {@ForeignKey(
        entity = Record.class,
        parentColumns = UNIQUE_ID,
        childColumns = RECORD_UID,
        onDelete = CASCADE)})
public class ValrepLandimpImpDetail {

//    @Expose(serialize = false, deserialize = false)
    @PrimaryKey(autoGenerate = true)
    @ColumnInfo(name = UNIQUE_ID)
    private transient long uniqueId;

    //TODO NON-SERIALIZED
//    @Expose(serialize = false, deserialize = false)
    @ColumnInfo(name = RECORD_ID, index = true)
    private transient String recordId;

//    @Expose(serialize = false, deserialize = false)
    @ColumnInfo(name = RECORD_UID, index = true)
    private transient long recordUId;

    @SerializedName("valrep_landimp_desc_beams")
    private String valrepLandimpDescBeams;
    @SerializedName("valrep_landimp_desc_category_class")
    private String valrepLandimpDescCategoryClass;
    @SerializedName("valrep_landimp_desc_ceiling")
    private String valrepLandimpDescCeiling;
    @SerializedName("valrep_landimp_desc_columns_posts")
    private String valrepLandimpDescColumnsPosts;
    @SerializedName("valrep_landimp_desc_compartments")
    private String valrepLandimpDescCompartments;
    @SerializedName("valrep_landimp_desc_confirmed_thru")
    private String valrepLandimpDescConfirmedThru;
    @SerializedName("valrep_landimp_desc_construction_feature")
    private String valrepLandimpDescConstructionFeature;
    @SerializedName("valrep_landimp_desc_designed_utilization")
    private String valrepLandimpDescDesignedUtilization;
    @SerializedName("valrep_landimp_desc_doors")
    private String valrepLandimpDescDoors;
    @SerializedName("valrep_landimp_desc_economic_life")
    private String valrepLandimpDescEconomicLife;
    @SerializedName("valrep_landimp_desc_effective_age")
    private String valrepLandimpDescEffectiveAge;
    @SerializedName("valrep_landimp_desc_est_life")
    private String valrepLandimpDescEstLife;
    @SerializedName("valrep_landimp_desc_exterior_walls")
    private String valrepLandimpDescExteriorWalls;
    @SerializedName("valrep_landimp_desc_features_2f")
    private String valrepLandimpDescFeatures2F;
    @SerializedName("valrep_landimp_desc_features_3f")
    private String valrepLandimpDescFeatures3F;
    @SerializedName("valrep_landimp_desc_features_4f")
    private String valrepLandimpDescFeatures4F;
    @SerializedName("valrep_landimp_desc_features_gf")
    private String valrepLandimpDescFeaturesGf;
    @SerializedName("valrep_landimp_desc_foundation")
    private String valrepLandimpDescFoundation;
    @SerializedName("valrep_landimp_desc_imp_cost_grade")
    private String valrepLandimpDescImpCostGrade;
    @SerializedName("valrep_landimp_desc_imp_desc")
    private String valrepLandimpDescImpDesc;
    @SerializedName("valrep_landimp_desc_imp_fence")
    private String valrepLandimpDescImpFence;
    @SerializedName("valrep_landimp_desc_imp_floor_area")
    private String valrepLandimpDescImpFloorArea;
    @SerializedName("valrep_landimp_desc_imp_flooring")
    private String valrepLandimpDescImpFlooring;
    @SerializedName("valrep_landimp_desc_imp_framing")
    private String valrepLandimpDescImpFraming;
    @SerializedName("valrep_landimp_desc_imp_garage")
    private String valrepLandimpDescImpGarage;
    @SerializedName("valrep_landimp_desc_imp_no_storey")
    private String valrepLandimpDescImpNoStorey;
    @SerializedName("valrep_landimp_desc_imp_others")
    private String valrepLandimpDescImpOthers;
    @SerializedName("valrep_landimp_desc_imp_remain_life")
    private String valrepLandimpDescImpRemainLife;
    @SerializedName("valrep_landimp_desc_imp_remarks")
    private String valrepLandimpDescImpRemarks;
    @SerializedName("valrep_landimp_desc_imp_roofing")
    private String valrepLandimpDescImpRoofing;
    @SerializedName("valrep_landimp_desc_imp_rooms")
    private String valrepLandimpDescImpRooms;
    @SerializedName("valrep_landimp_desc_imp_t_and_b")
    private String valrepLandimpDescImpTAndB;
    @SerializedName("valrep_landimp_desc_imp_use")
    private String valrepLandimpDescImpUse;
    @SerializedName("valrep_landimp_desc_imp_walls")
    private String valrepLandimpDescImpWalls;
    @SerializedName("valrep_landimp_desc_imp_windows")
    private String valrepLandimpDescImpWindows;
    @SerializedName("valrep_landimp_desc_impsummary1_no_of_t_b")
    private String valrepLandimpDescImpsummary1NoOfTB;
    @SerializedName("valrep_landimp_desc_interior_walls")
    private String valrepLandimpDescInteriorWalls;
    @SerializedName("valrep_landimp_desc_kitchen")
    private String valrepLandimpDescKitchen;
    @SerializedName("valrep_landimp_desc_observed_condition")
    private String valrepLandimpDescObservedCondition;
    @SerializedName("valrep_landimp_desc_occupants")
    private String valrepLandimpDescOccupants;
    @SerializedName("valrep_landimp_desc_others")
    private String valrepLandimpDescOthers;
    @SerializedName("valrep_landimp_desc_owned_or_leased")
    private String valrepLandimpDescOwnedOrLeased;
    @SerializedName("valrep_landimp_desc_partition")
    private String valrepLandimpDescPartition;
    @SerializedName("valrep_landimp_desc_partitions")
    private String valrepLandimpDescPartitions;
    @SerializedName("valrep_landimp_desc_percent_completed")
    private String valrepLandimpDescPercentCompleted;
    @SerializedName("valrep_landimp_desc_property_type")
    private String valrepLandimpDescPropertyType;
    @SerializedName("valrep_landimp_desc_roof")
    private String valrepLandimpDescRoof;
    @SerializedName("valrep_landimp_desc_stairs")
    private String valrepLandimpDescStairs;
    @SerializedName("valrep_landimp_desc_tb")
    private String valrepLandimpDescTb;
    @SerializedName("valrep_landimp_desc_trusses")
    private String valrepLandimpDescTrusses;
    @SerializedName("valrep_landimp_desc_valuation_approach")
    private String valrepLandimpDescValuationApproach;
    @SerializedName("valrep_landimp_desc_year_built")
    private String valrepLandimpDescYearBuilt;
    @SerializedName("valrep_landimp_impsummary1_actual_utilization")
    private String valrepLandimpImpsummary1ActualUtilization;
    @SerializedName("valrep_landimp_impsummary1_building_desc")
    private String valrepLandimpImpsummary1BuildingDesc;
    @SerializedName("valrep_landimp_impsummary1_erected_on_lot")
    private String valrepLandimpImpsummary1ErectedOnLot;
    @SerializedName("valrep_landimp_impsummary1_fa")
    private String valrepLandimpImpsummary1Fa;
    @SerializedName("valrep_landimp_impsummary1_fa_per_td")
    private String valrepLandimpImpsummary1FaPerTd;
    @SerializedName("valrep_landimp_impsummary1_index")
    private String valrepLandimpImpsummary1Index;
    @SerializedName("valrep_landimp_impsummary1_la")
    private String valrepLandimpImpsummary1La;
    @SerializedName("valrep_landimp_impsummary1_no_of_bedrooms")
    private String valrepLandimpImpsummary1NoOfBedrooms;
    @SerializedName("valrep_landimp_impsummary1_no_of_floors")
    private String valrepLandimpImpsummary1NoOfFloors;
    @SerializedName("valrep_landimp_impsummary1_owner")
    private String valrepLandimpImpsummary1Owner;
    @SerializedName("valrep_landimp_impsummary1_ownership_of_property")
    private String valrepLandimpImpsummary1OwnershipOfProperty;
    @SerializedName("valrep_landimp_impsummary1_socialized_housing")
    private String valrepLandimpImpsummary1SocializedHousing;
    @SerializedName("valrep_landimp_impsummary1_standard_features")
    private String valrepLandimpImpsummary1StandardFeatures;
    @SerializedName("valrep_landimp_impsummary1_usage_declaration")
    private String valrepLandimpImpsummary1UsageDeclaration;
    @SerializedName("valrep_landimp_tax_dec_imp_no")
    private String valrepLandimpTaxDecImpNo;
    @SerializedName("valrep_landimp_tax_dec_imp_td_classification")
    private String valrepLandimpTaxDecImpTdClassification;
    @SerializedName("valrep_landimp_tax_dec_imp_td_declarant")
    private String valrepLandimpTaxDecImpTdDeclarant;

    public long getUniqueId() {
        return uniqueId;
    }

    public void setUniqueId(long uniqueId) {
        this.uniqueId = uniqueId;
    }

    public String getRecordId() {
        return recordId;
    }

    public void setRecordId(String recordId) {
        this.recordId = recordId;
    }

    public long getRecordUId() {
        return recordUId;
    }

    public void setRecordUId(long recordUId) {
        this.recordUId = recordUId;
    }

    public String getValrepLandimpDescBeams() {
        return NULL_STRING_SETTER(valrepLandimpDescBeams);
    }

    public void setValrepLandimpDescBeams(String valrepLandimpDescBeams) {
        this.valrepLandimpDescBeams = valrepLandimpDescBeams;
    }

    public String getValrepLandimpDescCategoryClass() {
        return NULL_STRING_SETTER(valrepLandimpDescCategoryClass);
    }

    public void setValrepLandimpDescCategoryClass(String valrepLandimpDescCategoryClass) {
        this.valrepLandimpDescCategoryClass = valrepLandimpDescCategoryClass;
    }

    public String getValrepLandimpDescCeiling() {
        return NULL_STRING_SETTER(valrepLandimpDescCeiling);
    }

    public void setValrepLandimpDescCeiling(String valrepLandimpDescCeiling) {
        this.valrepLandimpDescCeiling = valrepLandimpDescCeiling;
    }

    public String getValrepLandimpDescColumnsPosts() {
        return NULL_STRING_SETTER(valrepLandimpDescColumnsPosts);
    }

    public void setValrepLandimpDescColumnsPosts(String valrepLandimpDescColumnsPosts) {
        this.valrepLandimpDescColumnsPosts = valrepLandimpDescColumnsPosts;
    }

    public String getValrepLandimpDescCompartments() {
        return NULL_STRING_SETTER(valrepLandimpDescCompartments);
    }

    public void setValrepLandimpDescCompartments(String valrepLandimpDescCompartments) {
        this.valrepLandimpDescCompartments = valrepLandimpDescCompartments;
    }

    public String getValrepLandimpDescConfirmedThru() {
        return NULL_STRING_SETTER(valrepLandimpDescConfirmedThru);
    }

    public void setValrepLandimpDescConfirmedThru(String valrepLandimpDescConfirmedThru) {
        this.valrepLandimpDescConfirmedThru = valrepLandimpDescConfirmedThru;
    }

    public String getValrepLandimpDescConstructionFeature() {
        return NULL_STRING_SETTER(valrepLandimpDescConstructionFeature);
    }

    public void setValrepLandimpDescConstructionFeature(String valrepLandimpDescConstructionFeature) {
        this.valrepLandimpDescConstructionFeature = valrepLandimpDescConstructionFeature;
    }

    public String getValrepLandimpDescDesignedUtilization() {
        return NULL_STRING_SETTER(valrepLandimpDescDesignedUtilization);
    }

    public void setValrepLandimpDescDesignedUtilization(String valrepLandimpDescDesignedUtilization) {
        this.valrepLandimpDescDesignedUtilization = valrepLandimpDescDesignedUtilization;
    }

    public String getValrepLandimpDescDoors() {
        return NULL_STRING_SETTER(valrepLandimpDescDoors);
    }

    public void setValrepLandimpDescDoors(String valrepLandimpDescDoors) {
        this.valrepLandimpDescDoors = valrepLandimpDescDoors;
    }

    public String getValrepLandimpDescEconomicLife() {
        return NULL_STRING_SETTER(valrepLandimpDescEconomicLife);
    }

    public void setValrepLandimpDescEconomicLife(String valrepLandimpDescEconomicLife) {
        this.valrepLandimpDescEconomicLife = valrepLandimpDescEconomicLife;
    }

    public String getValrepLandimpDescEffectiveAge() {
        return NULL_STRING_SETTER(valrepLandimpDescEffectiveAge);
    }

    public void setValrepLandimpDescEffectiveAge(String valrepLandimpDescEffectiveAge) {
        this.valrepLandimpDescEffectiveAge = valrepLandimpDescEffectiveAge;
    }

    public String getValrepLandimpDescEstLife() {
        return NULL_STRING_SETTER(valrepLandimpDescEstLife);
    }

    public void setValrepLandimpDescEstLife(String valrepLandimpDescEstLife) {
        this.valrepLandimpDescEstLife = valrepLandimpDescEstLife;
    }

    public String getValrepLandimpDescExteriorWalls() {
        return NULL_STRING_SETTER(valrepLandimpDescExteriorWalls);
    }

    public void setValrepLandimpDescExteriorWalls(String valrepLandimpDescExteriorWalls) {
        this.valrepLandimpDescExteriorWalls = valrepLandimpDescExteriorWalls;
    }

    public String getValrepLandimpDescFeatures2F() {
        return NULL_STRING_SETTER(valrepLandimpDescFeatures2F);
    }

    public void setValrepLandimpDescFeatures2F(String valrepLandimpDescFeatures2F) {
        this.valrepLandimpDescFeatures2F = valrepLandimpDescFeatures2F;
    }

    public String getValrepLandimpDescFeatures3F() {
        return NULL_STRING_SETTER(valrepLandimpDescFeatures3F);
    }

    public void setValrepLandimpDescFeatures3F(String valrepLandimpDescFeatures3F) {
        this.valrepLandimpDescFeatures3F = valrepLandimpDescFeatures3F;
    }

    public String getValrepLandimpDescFeatures4F() {
        return NULL_STRING_SETTER(valrepLandimpDescFeatures4F);
    }

    public void setValrepLandimpDescFeatures4F(String valrepLandimpDescFeatures4F) {
        this.valrepLandimpDescFeatures4F = valrepLandimpDescFeatures4F;
    }

    public String getValrepLandimpDescFeaturesGf() {
        return NULL_STRING_SETTER(valrepLandimpDescFeaturesGf);
    }

    public void setValrepLandimpDescFeaturesGf(String valrepLandimpDescFeaturesGf) {
        this.valrepLandimpDescFeaturesGf = valrepLandimpDescFeaturesGf;
    }

    public String getValrepLandimpDescFoundation() {
        return NULL_STRING_SETTER(valrepLandimpDescFoundation);
    }

    public void setValrepLandimpDescFoundation(String valrepLandimpDescFoundation) {
        this.valrepLandimpDescFoundation = valrepLandimpDescFoundation;
    }

    public String getValrepLandimpDescImpCostGrade() {
        return NULL_STRING_SETTER(valrepLandimpDescImpCostGrade);
    }

    public void setValrepLandimpDescImpCostGrade(String valrepLandimpDescImpCostGrade) {
        this.valrepLandimpDescImpCostGrade = valrepLandimpDescImpCostGrade;
    }

    public String getValrepLandimpDescImpDesc() {
        return NULL_STRING_SETTER(valrepLandimpDescImpDesc);
    }

    public void setValrepLandimpDescImpDesc(String valrepLandimpDescImpDesc) {
        this.valrepLandimpDescImpDesc = valrepLandimpDescImpDesc;
    }

    public String getValrepLandimpDescImpFence() {
        return NULL_STRING_SETTER(valrepLandimpDescImpFence);
    }

    public void setValrepLandimpDescImpFence(String valrepLandimpDescImpFence) {
        this.valrepLandimpDescImpFence = valrepLandimpDescImpFence;
    }

    public String getValrepLandimpDescImpFloorArea() {
        return NULL_STRING_SETTER(valrepLandimpDescImpFloorArea);
    }

    public void setValrepLandimpDescImpFloorArea(String valrepLandimpDescImpFloorArea) {
        this.valrepLandimpDescImpFloorArea = valrepLandimpDescImpFloorArea;
    }

    public String getValrepLandimpDescImpFlooring() {
        return NULL_STRING_SETTER(valrepLandimpDescImpFlooring);
    }

    public void setValrepLandimpDescImpFlooring(String valrepLandimpDescImpFlooring) {
        this.valrepLandimpDescImpFlooring = valrepLandimpDescImpFlooring;
    }

    public String getValrepLandimpDescImpFraming() {
        return NULL_STRING_SETTER(valrepLandimpDescImpFraming);
    }

    public void setValrepLandimpDescImpFraming(String valrepLandimpDescImpFraming) {
        this.valrepLandimpDescImpFraming = valrepLandimpDescImpFraming;
    }

    public String getValrepLandimpDescImpGarage() {
        return NULL_STRING_SETTER(valrepLandimpDescImpGarage);
    }

    public void setValrepLandimpDescImpGarage(String valrepLandimpDescImpGarage) {
        this.valrepLandimpDescImpGarage = valrepLandimpDescImpGarage;
    }

    public String getValrepLandimpDescImpNoStorey() {
        return NULL_STRING_SETTER(valrepLandimpDescImpNoStorey);
    }

    public void setValrepLandimpDescImpNoStorey(String valrepLandimpDescImpNoStorey) {
        this.valrepLandimpDescImpNoStorey = valrepLandimpDescImpNoStorey;
    }

    public String getValrepLandimpDescImpOthers() {
        return NULL_STRING_SETTER(valrepLandimpDescImpOthers);
    }

    public void setValrepLandimpDescImpOthers(String valrepLandimpDescImpOthers) {
        this.valrepLandimpDescImpOthers = valrepLandimpDescImpOthers;
    }

    public String getValrepLandimpDescImpRemainLife() {
        return NULL_STRING_SETTER(valrepLandimpDescImpRemainLife);
    }

    public void setValrepLandimpDescImpRemainLife(String valrepLandimpDescImpRemainLife) {
        this.valrepLandimpDescImpRemainLife = valrepLandimpDescImpRemainLife;
    }

    public String getValrepLandimpDescImpRemarks() {
        return NULL_STRING_SETTER(valrepLandimpDescImpRemarks);
    }

    public void setValrepLandimpDescImpRemarks(String valrepLandimpDescImpRemarks) {
        this.valrepLandimpDescImpRemarks = valrepLandimpDescImpRemarks;
    }

    public String getValrepLandimpDescImpRoofing() {
        return NULL_STRING_SETTER(valrepLandimpDescImpRoofing);
    }

    public void setValrepLandimpDescImpRoofing(String valrepLandimpDescImpRoofing) {
        this.valrepLandimpDescImpRoofing = valrepLandimpDescImpRoofing;
    }

    public String getValrepLandimpDescImpRooms() {
        return NULL_STRING_SETTER(valrepLandimpDescImpRooms);
    }

    public void setValrepLandimpDescImpRooms(String valrepLandimpDescImpRooms) {
        this.valrepLandimpDescImpRooms = valrepLandimpDescImpRooms;
    }

    public String getValrepLandimpDescImpTAndB() {
        return NULL_STRING_SETTER(valrepLandimpDescImpTAndB);
    }

    public void setValrepLandimpDescImpTAndB(String valrepLandimpDescImpTAndB) {
        this.valrepLandimpDescImpTAndB = valrepLandimpDescImpTAndB;
    }

    public String getValrepLandimpDescImpUse() {
        return NULL_STRING_SETTER(valrepLandimpDescImpUse);
    }

    public void setValrepLandimpDescImpUse(String valrepLandimpDescImpUse) {
        this.valrepLandimpDescImpUse = valrepLandimpDescImpUse;
    }

    public String getValrepLandimpDescImpWalls() {
        return NULL_STRING_SETTER(valrepLandimpDescImpWalls);
    }

    public void setValrepLandimpDescImpWalls(String valrepLandimpDescImpWalls) {
        this.valrepLandimpDescImpWalls = valrepLandimpDescImpWalls;
    }

    public String getValrepLandimpDescImpWindows() {
        return NULL_STRING_SETTER(valrepLandimpDescImpWindows);
    }

    public void setValrepLandimpDescImpWindows(String valrepLandimpDescImpWindows) {
        this.valrepLandimpDescImpWindows = valrepLandimpDescImpWindows;
    }

    public String getValrepLandimpDescImpsummary1NoOfTB() {
        return NULL_STRING_SETTER(valrepLandimpDescImpsummary1NoOfTB);
    }

    public void setValrepLandimpDescImpsummary1NoOfTB(String valrepLandimpDescImpsummary1NoOfTB) {
        this.valrepLandimpDescImpsummary1NoOfTB = valrepLandimpDescImpsummary1NoOfTB;
    }

    public String getValrepLandimpDescInteriorWalls() {
        return NULL_STRING_SETTER(valrepLandimpDescInteriorWalls);
    }

    public void setValrepLandimpDescInteriorWalls(String valrepLandimpDescInteriorWalls) {
        this.valrepLandimpDescInteriorWalls = valrepLandimpDescInteriorWalls;
    }

    public String getValrepLandimpDescKitchen() {
        return NULL_STRING_SETTER(valrepLandimpDescKitchen);
    }

    public void setValrepLandimpDescKitchen(String valrepLandimpDescKitchen) {
        this.valrepLandimpDescKitchen = valrepLandimpDescKitchen;
    }

    public String getValrepLandimpDescObservedCondition() {
        return NULL_STRING_SETTER(valrepLandimpDescObservedCondition);
    }

    public void setValrepLandimpDescObservedCondition(String valrepLandimpDescObservedCondition) {
        this.valrepLandimpDescObservedCondition = valrepLandimpDescObservedCondition;
    }

    public String getValrepLandimpDescOccupants() {
        return NULL_STRING_SETTER(valrepLandimpDescOccupants);
    }

    public void setValrepLandimpDescOccupants(String valrepLandimpDescOccupants) {
        this.valrepLandimpDescOccupants = valrepLandimpDescOccupants;
    }

    public String getValrepLandimpDescOthers() {
        return NULL_STRING_SETTER(valrepLandimpDescOthers);
    }

    public void setValrepLandimpDescOthers(String valrepLandimpDescOthers) {
        this.valrepLandimpDescOthers = valrepLandimpDescOthers;
    }

    public String getValrepLandimpDescOwnedOrLeased() {
        return NULL_STRING_SETTER(valrepLandimpDescOwnedOrLeased);
    }

    public void setValrepLandimpDescOwnedOrLeased(String valrepLandimpDescOwnedOrLeased) {
        this.valrepLandimpDescOwnedOrLeased = valrepLandimpDescOwnedOrLeased;
    }

    public String getValrepLandimpDescPartition() {
        return NULL_STRING_SETTER(valrepLandimpDescPartition);
    }

    public void setValrepLandimpDescPartition(String valrepLandimpDescPartition) {
        this.valrepLandimpDescPartition = valrepLandimpDescPartition;
    }

    public String getValrepLandimpDescPartitions() {
        return NULL_STRING_SETTER(valrepLandimpDescPartitions);
    }

    public void setValrepLandimpDescPartitions(String valrepLandimpDescPartitions) {
        this.valrepLandimpDescPartitions = valrepLandimpDescPartitions;
    }

    public String getValrepLandimpDescPercentCompleted() {
        return NULL_STRING_SETTER(valrepLandimpDescPercentCompleted);
    }

    public void setValrepLandimpDescPercentCompleted(String valrepLandimpDescPercentCompleted) {
        this.valrepLandimpDescPercentCompleted = valrepLandimpDescPercentCompleted;
    }

    public String getValrepLandimpDescPropertyType() {
        return NULL_STRING_SETTER(valrepLandimpDescPropertyType);
    }

    public void setValrepLandimpDescPropertyType(String valrepLandimpDescPropertyType) {
        this.valrepLandimpDescPropertyType = valrepLandimpDescPropertyType;
    }

    public String getValrepLandimpDescRoof() {
        return NULL_STRING_SETTER(valrepLandimpDescRoof);
    }

    public void setValrepLandimpDescRoof(String valrepLandimpDescRoof) {
        this.valrepLandimpDescRoof = valrepLandimpDescRoof;
    }

    public String getValrepLandimpDescStairs() {
        return NULL_STRING_SETTER(valrepLandimpDescStairs);
    }

    public void setValrepLandimpDescStairs(String valrepLandimpDescStairs) {
        this.valrepLandimpDescStairs = valrepLandimpDescStairs;
    }

    public String getValrepLandimpDescTb() {
        return NULL_STRING_SETTER(valrepLandimpDescTb);
    }

    public void setValrepLandimpDescTb(String valrepLandimpDescTb) {
        this.valrepLandimpDescTb = valrepLandimpDescTb;
    }

    public String getValrepLandimpDescTrusses() {
        return NULL_STRING_SETTER(valrepLandimpDescTrusses);
    }

    public void setValrepLandimpDescTrusses(String valrepLandimpDescTrusses) {
        this.valrepLandimpDescTrusses = valrepLandimpDescTrusses;
    }

    public String getValrepLandimpDescValuationApproach() {
        return NULL_STRING_SETTER(valrepLandimpDescValuationApproach);
    }

    public void setValrepLandimpDescValuationApproach(String valrepLandimpDescValuationApproach) {
        this.valrepLandimpDescValuationApproach = valrepLandimpDescValuationApproach;
    }

    public String getValrepLandimpDescYearBuilt() {
        return NULL_STRING_SETTER(valrepLandimpDescYearBuilt);
    }

    public void setValrepLandimpDescYearBuilt(String valrepLandimpDescYearBuilt) {
        this.valrepLandimpDescYearBuilt = valrepLandimpDescYearBuilt;
    }

    public String getValrepLandimpImpsummary1ActualUtilization() {
        return NULL_STRING_SETTER(valrepLandimpImpsummary1ActualUtilization);
    }

    public void setValrepLandimpImpsummary1ActualUtilization(String valrepLandimpImpsummary1ActualUtilization) {
        this.valrepLandimpImpsummary1ActualUtilization = valrepLandimpImpsummary1ActualUtilization;
    }

    public String getValrepLandimpImpsummary1BuildingDesc() {
        return NULL_STRING_SETTER(valrepLandimpImpsummary1BuildingDesc);
    }

    public void setValrepLandimpImpsummary1BuildingDesc(String valrepLandimpImpsummary1BuildingDesc) {
        this.valrepLandimpImpsummary1BuildingDesc = valrepLandimpImpsummary1BuildingDesc;
    }

    public String getValrepLandimpImpsummary1ErectedOnLot() {
        return NULL_STRING_SETTER(valrepLandimpImpsummary1ErectedOnLot);
    }

    public void setValrepLandimpImpsummary1ErectedOnLot(String valrepLandimpImpsummary1ErectedOnLot) {
        this.valrepLandimpImpsummary1ErectedOnLot = valrepLandimpImpsummary1ErectedOnLot;
    }

    public String getValrepLandimpImpsummary1Fa() {
        return NULL_STRING_SETTER(valrepLandimpImpsummary1Fa);
    }

    public void setValrepLandimpImpsummary1Fa(String valrepLandimpImpsummary1Fa) {
        this.valrepLandimpImpsummary1Fa = valrepLandimpImpsummary1Fa;
    }

    public String getValrepLandimpImpsummary1FaPerTd() {
        return NULL_STRING_SETTER(valrepLandimpImpsummary1FaPerTd);
    }

    public void setValrepLandimpImpsummary1FaPerTd(String valrepLandimpImpsummary1FaPerTd) {
        this.valrepLandimpImpsummary1FaPerTd = valrepLandimpImpsummary1FaPerTd;
    }

    public String getValrepLandimpImpsummary1Index() {
        return NULL_STRING_SETTER(valrepLandimpImpsummary1Index);
    }

    public void setValrepLandimpImpsummary1Index(String valrepLandimpImpsummary1Index) {
        this.valrepLandimpImpsummary1Index = valrepLandimpImpsummary1Index;
    }

    public String getValrepLandimpImpsummary1La() {
        return NULL_STRING_SETTER(valrepLandimpImpsummary1La);
    }

    public void setValrepLandimpImpsummary1La(String valrepLandimpImpsummary1La) {
        this.valrepLandimpImpsummary1La = valrepLandimpImpsummary1La;
    }

    public String getValrepLandimpImpsummary1NoOfBedrooms() {
        return NULL_STRING_SETTER(valrepLandimpImpsummary1NoOfBedrooms);
    }

    public void setValrepLandimpImpsummary1NoOfBedrooms(String valrepLandimpImpsummary1NoOfBedrooms) {
        this.valrepLandimpImpsummary1NoOfBedrooms = valrepLandimpImpsummary1NoOfBedrooms;
    }

    public String getValrepLandimpImpsummary1NoOfFloors() {
        return NULL_STRING_SETTER(valrepLandimpImpsummary1NoOfFloors);
    }

    public void setValrepLandimpImpsummary1NoOfFloors(String valrepLandimpImpsummary1NoOfFloors) {
        this.valrepLandimpImpsummary1NoOfFloors = valrepLandimpImpsummary1NoOfFloors;
    }

    public String getValrepLandimpImpsummary1Owner() {
        return NULL_STRING_SETTER(valrepLandimpImpsummary1Owner);
    }

    public void setValrepLandimpImpsummary1Owner(String valrepLandimpImpsummary1Owner) {
        this.valrepLandimpImpsummary1Owner = valrepLandimpImpsummary1Owner;
    }

    public String getValrepLandimpImpsummary1OwnershipOfProperty() {
        return NULL_STRING_SETTER(valrepLandimpImpsummary1OwnershipOfProperty);
    }

    public void setValrepLandimpImpsummary1OwnershipOfProperty(String valrepLandimpImpsummary1OwnershipOfProperty) {
        this.valrepLandimpImpsummary1OwnershipOfProperty = valrepLandimpImpsummary1OwnershipOfProperty;
    }

    public String getValrepLandimpImpsummary1SocializedHousing() {
        return NULL_STRING_SETTER(valrepLandimpImpsummary1SocializedHousing);
    }

    public void setValrepLandimpImpsummary1SocializedHousing(String valrepLandimpImpsummary1SocializedHousing) {
        this.valrepLandimpImpsummary1SocializedHousing = valrepLandimpImpsummary1SocializedHousing;
    }

    public String getValrepLandimpImpsummary1StandardFeatures() {
        return NULL_STRING_SETTER(valrepLandimpImpsummary1StandardFeatures);
    }

    public void setValrepLandimpImpsummary1StandardFeatures(String valrepLandimpImpsummary1StandardFeatures) {
        this.valrepLandimpImpsummary1StandardFeatures = valrepLandimpImpsummary1StandardFeatures;
    }

    public String getValrepLandimpImpsummary1UsageDeclaration() {
        return NULL_STRING_SETTER(valrepLandimpImpsummary1UsageDeclaration);
    }

    public void setValrepLandimpImpsummary1UsageDeclaration(String valrepLandimpImpsummary1UsageDeclaration) {
        this.valrepLandimpImpsummary1UsageDeclaration = valrepLandimpImpsummary1UsageDeclaration;
    }

    public String getValrepLandimpTaxDecImpNo() {
        return NULL_STRING_SETTER(valrepLandimpTaxDecImpNo);
    }

    public void setValrepLandimpTaxDecImpNo(String valrepLandimpTaxDecImpNo) {
        this.valrepLandimpTaxDecImpNo = valrepLandimpTaxDecImpNo;
    }

    public String getValrepLandimpTaxDecImpTdClassification() {
        return NULL_STRING_SETTER(valrepLandimpTaxDecImpTdClassification);
    }

    public void setValrepLandimpTaxDecImpTdClassification(String valrepLandimpTaxDecImpTdClassification) {
        this.valrepLandimpTaxDecImpTdClassification = valrepLandimpTaxDecImpTdClassification;
    }

    public String getValrepLandimpTaxDecImpTdDeclarant() {
        return NULL_STRING_SETTER(valrepLandimpTaxDecImpTdDeclarant);
    }

    public void setValrepLandimpTaxDecImpTdDeclarant(String valrepLandimpTaxDecImpTdDeclarant) {
        this.valrepLandimpTaxDecImpTdDeclarant = valrepLandimpTaxDecImpTdDeclarant;
    }

}
