
package com.gdslinkasia.base.model.job;

import androidx.room.ColumnInfo;
import androidx.room.Entity;
import androidx.room.ForeignKey;
import androidx.room.PrimaryKey;
import androidx.room.TypeConverters;

import com.gdslinkasia.base.database.converters.AppraisalRequestTypeConverter;
import com.gdslinkasia.base.database.converters.JobSystemTypeConverter;
import com.gdslinkasia.base.database.converters.ValrepLandimpAssumptionsRemarkTypeConverter;
import com.gdslinkasia.base.database.converters.ValrepLandimpImpDetailTypeConverter;
import com.gdslinkasia.base.database.converters.ValrepLandimpImpValuationTypeConverter;
import com.gdslinkasia.base.database.converters.ValrepLandimpLotDetailTypeConverter;
import com.gdslinkasia.base.database.converters.ValrepLandimpLotValuationTypeConverter;
import com.gdslinkasia.base.database.converters.ValrepLandimpOtherLandImpDetailTypeConverter;
import com.gdslinkasia.base.database.converters.ValrepLandimpOtherLandImpValueTypeConverter;
import com.gdslinkasia.base.database.converters.ValrepLandimpParkingValuationDetailTypeConverter;
import com.gdslinkasia.base.database.converters.ValrepLcIvsQualifyingStatementTypeConverter;
import com.gdslinkasia.base.model.details.AppraisalRequest;
import com.gdslinkasia.base.model.details.JobSystem;
import com.gdslinkasia.base.model.details.ValrepLandimpAssumptionsRemark;
import com.gdslinkasia.base.model.details.ValrepLandimpImpDetail;
import com.gdslinkasia.base.model.details.ValrepLandimpImpValuation;
import com.gdslinkasia.base.model.details.ValrepLandimpLotDetail;
import com.gdslinkasia.base.model.details.ValrepLandimpLotValuation;
import com.gdslinkasia.base.model.details.ValrepLandimpOtherLandImpDetail;
import com.gdslinkasia.base.model.details.ValrepLandimpOtherLandImpValue;
import com.gdslinkasia.base.model.details.ValrepLandimpParkingValuationDetail;
import com.gdslinkasia.base.model.details.ValrepLcIvsQualifyingStatement;
import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.List;

import static androidx.room.ForeignKey.CASCADE;
import static com.gdslinkasia.base.database.DatabaseUtils.APPLICATION_STATUS;
import static com.gdslinkasia.base.database.DatabaseUtils.APPRAISAL_TYPE;
import static com.gdslinkasia.base.database.DatabaseUtils.APPRAISER_USERNAME;
import static com.gdslinkasia.base.database.DatabaseUtils.RECORD_ID;
import static com.gdslinkasia.base.database.DatabaseUtils.UNIQUE_ID;
import static com.gdslinkasia.base.utils.GlobalString.NULL_LIST_SETTER;
import static com.gdslinkasia.base.utils.GlobalString.NULL_STRING_SETTER;


@SuppressWarnings("all")
@Entity(foreignKeys = @ForeignKey(entity = JobSystem.class,
        parentColumns = RECORD_ID,
        childColumns = RECORD_ID,
        onDelete = CASCADE))
public class Record {

//    @Expose(serialize = false, deserialize = false)
    @PrimaryKey(autoGenerate = true)
    @ColumnInfo(name = UNIQUE_ID)
    private transient long uniqueId;

    //TODO NON-SERIALIZED
//    @Expose(serialize = false, deserialize = false)
    @ColumnInfo(name = RECORD_ID, index = true)
    private transient String recordId;

    //TODO NON-SERIALIZED
//    @Expose(serialize = false, deserialize = false)
    @ColumnInfo(name = APPRAISER_USERNAME)
    private transient String appraiserUsername;

    @Expose(serialize = false, deserialize = false)
    @ColumnInfo(name = APPRAISAL_TYPE)
    private transient String appraisalType;

    @ColumnInfo(name = APPLICATION_STATUS)
    @SerializedName("application_status")
    private String applicationStatus;

    @SerializedName("app_account_authroizing_officer")
    private String appAccountAuthroizingOfficer;
    @SerializedName("app_account_business_address")
    private String appAccountBusinessAddress;
    @SerializedName("app_account_business_contact")
    private String appAccountBusinessContact;
    @SerializedName("app_account_business_email")
    private String appAccountBusinessEmail;
    @SerializedName("app_account_company")
    private String appAccountCompany;
    @SerializedName("app_account_designation")
    private String appAccountDesignation;
    @SerializedName("app_account_first_name")
    private String appAccountFirstName;
    @SerializedName("app_account_last_name")
    private String appAccountLastName;
    @SerializedName("app_account_middle_name")
    private String appAccountMiddleName;
    @SerializedName("app_account_name_title")
    private String appAccountNameTitle;
    @SerializedName("app_appraisal_manager")
    private String appAppraisalManager;
    @SerializedName("app_appraiser_code")
    private String appAppraiserCode;
    @SerializedName("app_appraiser_type")
    private String appAppraiserType;
    @SerializedName("app_borrower_name")
    private String appBorrowerName;
    @SerializedName("app_borrower_type")
    private String appBorrowerType;
    @SerializedName("app_contact_no")
    private String appContactNo;
    @SerializedName("app_cross_reference_first_name")
    private String appCrossReferenceFirstName;
    @SerializedName("app_cross_reference_last_name")
    private String appCrossReferenceLastName;
    @SerializedName("app_cross_reference_middle_name")
    private String appCrossReferenceMiddleName;
    @SerializedName("app_cross_reference_relation")
    private String appCrossReferenceRelation;
    @SerializedName("app_date_contract_day")
    private String appDateContractDay;
    @SerializedName("app_date_contract_month")
    private String appDateContractMonth;
    @SerializedName("app_date_contract_year")
    private String appDateContractYear;
    @SerializedName("app_date_today")
    private String appDateToday;
    @SerializedName("app_daterequested_day")
    private String appDaterequestedDay;
    @SerializedName("app_daterequested_month")
    private String appDaterequestedMonth;
    @SerializedName("app_daterequested_year")
    private String appDaterequestedYear;
    @SerializedName("app_declined_reason")
    private String appDeclinedReason;
    @SerializedName("app_desired_loan_amount")
    private String appDesiredLoanAmount;
    @SerializedName("app_intended_users_address")
    private String appIntendedUsersAddress;
    @SerializedName("app_intended_users_address_type")
    private String appIntendedUsersAddressType;
    @SerializedName("app_intended_users_name")
    private String appIntendedUsersName;
    @SerializedName("app_intended_users_others")
    private String appIntendedUsersOthers;
    @SerializedName("app_job_acceptance_remarks")
    private String appJobAcceptanceRemarks;
    @SerializedName("app_manager_username")
    private String appManagerUsername;
    @SerializedName("app_requestor_username")
    private String appRequestorUsername;
    @SerializedName("app_simul_type")
    private String appSimulType;
    @SerializedName("app_type_of_loan")
    private String appTypeOfLoan;
    @SerializedName("app_valuer_contact_number")
    private String appValuerContactNumber;
    @SerializedName("app_valuer_email_address")
    private String appValuerEmailAddress;
    @SerializedName("app_valuer_name")
    private String appValuerName;
    @SerializedName("app_valuer_officer_type")
    private String appValuerOfficerType;
    @SerializedName("application_source")
    private String applicationSource;



    @TypeConverters(AppraisalRequestTypeConverter.class)
    @SerializedName("appraisal_request")
    private List<AppraisalRequest> appraisalRequest;

    @SerializedName("asset_neighborhood_automotive_centers")
    private String assetNeighborhoodAutomotiveCenters;
    @SerializedName("asset_neighborhood_banks")
    private String assetNeighborhoodBanks;
    @SerializedName("asset_neighborhood_establishments")
    private String assetNeighborhoodEstablishments;
    @SerializedName("asset_neighborhood_government_facilities")
    private String assetNeighborhoodGovernmentFacilities;
    @SerializedName("asset_neighborhood_hospitals")
    private String assetNeighborhoodHospitals;
    @SerializedName("asset_neighborhood_hotels")
    private String assetNeighborhoodHotels;
    @SerializedName("asset_neighborhood_industrial_indicators")
    private String assetNeighborhoodIndustrialIndicators;
    @SerializedName("asset_neighborhood_schools")
    private String assetNeighborhoodSchools;
    @SerializedName("asset_neighborhood_shopping_malls")
    private String assetNeighborhoodShoppingMalls;
    @SerializedName("asset_neighborhood_subdivisions")
    private String assetNeighborhoodSubdivisions;
    @SerializedName("asset_neighborhood_worship")
    private String assetNeighborhoodWorship;
    @SerializedName("asset_technical_desc")
    private String assetTechnicalDesc;
    @SerializedName("bov_source_info_buyer_seller")
    private String bovSourceInfoBuyerSeller;
    @SerializedName("bov_source_info_buyer_sellerbroker")
    private String bovSourceInfoBuyerSellerbroker;
    @SerializedName("bov_source_info_contractor")
    private String bovSourceInfoContractor;
    @SerializedName("bov_source_info_government")
    private String bovSourceInfoGovernment;
    @SerializedName("bov_source_info_hoa_officer")
    private String bovSourceInfoHoaOfficer;
    @SerializedName("bov_source_info_lgu_official")
    private String bovSourceInfoLguOfficial;
    @SerializedName("bov_source_info_other")
    private String bovSourceInfoOther;
    @SerializedName("bov_source_info_private_website")
    private String bovSourceInfoPrivateWebsite;
    @SerializedName("bov_source_info_property_manager")
    private String bovSourceInfoPropertyManager;
    @SerializedName("bov_source_info_publication")
    private String bovSourceInfoPublication;
    @SerializedName("bov_source_info_simulation")
    private String bovSourceInfoSimulation;
    @SerializedName("bov_source_info_unaffiliated_broker")
    private String bovSourceInfoUnaffiliatedBroker;
    @SerializedName("lc_ivs_complied")
    private String lcIvsComplied;
    @SerializedName("lc_ivs_complied_summary")
    private String lcIvsCompliedSummary;
    @SerializedName("lc_ivs_qualifying_statements_desc")
    private String lcIvsQualifyingStatementsDesc;
    @SerializedName("lc_ivs_reason1")
    private String lcIvsReason1;
    @SerializedName("lc_ivs_reason2")
    private String lcIvsReason2;
    @SerializedName("lc_ivs_reason3")
    private String lcIvsReason3;
    @SerializedName("lc_ivs_reason4")
    private String lcIvsReason4;
    @SerializedName("lc_ivs_reason5")
    private String lcIvsReason5;
    @SerializedName("lc_ivs_reason_others")
    private String lcIvsReasonOthers;
    @SerializedName("lc_valuation_approach")
    private String lcValuationApproach;
    @SerializedName("lc_valuation_approach_reason")
    private String lcValuationApproachReason;
    @SerializedName("lc_valuation_key_input")
    private String lcValuationKeyInput;
    @SerializedName("lv_ivs_qualifying_statements_1")
    private String lvIvsQualifyingStatements1;
    @SerializedName("lv_ivs_qualifying_statements_2")
    private String lvIvsQualifyingStatements2;
    @SerializedName("lv_ivs_qualifying_statements_3")
    private String lvIvsQualifyingStatements3;
    @SerializedName("lv_ivs_qualifying_statements_4")
    private String lvIvsQualifyingStatements4;
    @SerializedName("lv_ivs_qualifying_statements_5")
    private String lvIvsQualifyingStatements5;
    @SerializedName("lv_ivs_qualifying_statements_6")
    private String lvIvsQualifyingStatements6;
    @SerializedName("paf_landimp_developer_name")
    private String pafLandimpDeveloperName;

    @TypeConverters(JobSystemTypeConverter.class)
    @SerializedName("system")
    @Expose
    private JobSystem jobSystem;

    @SerializedName("valrep_appraisal_basis_of_value")
    private String valrepAppraisalBasisOfValue;
    @SerializedName("valrep_land_imp_comp1_comp_type")
    private String valrepLandImpComp1CompType;
    @SerializedName("valrep_land_imp_comp2_comp_type")
    private String valrepLandImpComp2CompType;
    @SerializedName("valrep_land_imp_comp3_comp_type")
    private String valrepLandImpComp3CompType;
    @SerializedName("valrep_land_imp_comp4_comp_type")
    private String valrepLandImpComp4CompType;
    @SerializedName("valrep_land_imp_comp5_comp_type")
    private String valrepLandImpComp5CompType;
    @SerializedName("valrep_landimp_assumptions_1")
    private String valrepLandimpAssumptions1;
    @SerializedName("valrep_landimp_assumptions_2")
    private String valrepLandimpAssumptions2;
    @SerializedName("valrep_landimp_assumptions_3")
    private String valrepLandimpAssumptions3;
    @SerializedName("valrep_landimp_assumptions_4")
    private String valrepLandimpAssumptions4;
    @SerializedName("valrep_landimp_assumptions_5")
    private String valrepLandimpAssumptions5;

    @TypeConverters(ValrepLandimpAssumptionsRemarkTypeConverter.class)
    @SerializedName("valrep_landimp_assumptions_remarks")
    private List<ValrepLandimpAssumptionsRemark> valrepLandimpAssumptionsRemarks;

    @SerializedName("valrep_landimp_basic_description")
    private String valrepLandimpBasicDescription;
    @SerializedName("valrep_landimp_bound1_orientation")
    private String valrepLandimpBound1Orientation;
    @SerializedName("valrep_landimp_bound2_orientation")
    private String valrepLandimpBound2Orientation;
    @SerializedName("valrep_landimp_bound3_orientation")
    private String valrepLandimpBound3Orientation;
    @SerializedName("valrep_landimp_bound4_orientation")
    private String valrepLandimpBound4Orientation;
    @SerializedName("valrep_landimp_comp1_adjustable_price_sqm")
    private String valrepLandimpComp1AdjustablePriceSqm;
    @SerializedName("valrep_landimp_comp1_adjusted_value")
    private String valrepLandimpComp1AdjustedValue;
    @SerializedName("valrep_landimp_comp1_approx_age")
    private String valrepLandimpComp1ApproxAge;
    @SerializedName("valrep_landimp_comp1_area")
    private String valrepLandimpComp1Area;
    @SerializedName("valrep_landimp_comp1_base_price")
    private String valrepLandimpComp1BasePrice;
    @SerializedName("valrep_landimp_comp1_base_price_sqm")
    private String valrepLandimpComp1BasePriceSqm;
    @SerializedName("valrep_landimp_comp1_bldg_area")
    private String valrepLandimpComp1BldgArea;
    @SerializedName("valrep_landimp_comp1_bldg_depreciation")
    private String valrepLandimpComp1BldgDepreciation;
    @SerializedName("valrep_landimp_comp1_bldg_depreciation_val")
    private String valrepLandimpComp1BldgDepreciationVal;
    @SerializedName("valrep_landimp_comp1_bldg_replacement_unit_value")
    private String valrepLandimpComp1BldgReplacementUnitValue;
    @SerializedName("valrep_landimp_comp1_bldg_replacement_value")
    private String valrepLandimpComp1BldgReplacementValue;
    @SerializedName("valrep_landimp_comp1_characteristic")
    private String valrepLandimpComp1Characteristic;
    @SerializedName("valrep_landimp_comp1_characteristics")
    private String valrepLandimpComp1Characteristics;
    @SerializedName("valrep_landimp_comp1_concluded_adjustment")
    private String valrepLandimpComp1ConcludedAdjustment;
    @SerializedName("valrep_landimp_comp1_contact_no")
    private String valrepLandimpComp1ContactNo;
    @SerializedName("valrep_landimp_comp1_date_day")
    private String valrepLandimpComp1DateDay;
    @SerializedName("valrep_landimp_comp1_date_month")
    private String valrepLandimpComp1DateMonth;
    @SerializedName("valrep_landimp_comp1_date_year")
    private String valrepLandimpComp1DateYear;
    @SerializedName("valrep_landimp_comp1_discount_rate")
    private String valrepLandimpComp1DiscountRate;
    @SerializedName("valrep_landimp_comp1_discounts")
    private String valrepLandimpComp1Discounts;
    @SerializedName("valrep_landimp_comp1_effective_date")
    private String valrepLandimpComp1EffectiveDate;
    @SerializedName("valrep_landimp_comp1_land_residual_value")
    private String valrepLandimpComp1LandResidualValue;
    @SerializedName("valrep_landimp_comp1_location")
    private String valrepLandimpComp1Location;
    @SerializedName("valrep_landimp_comp1_net_bldg_value")
    private String valrepLandimpComp1NetBldgValue;
    @SerializedName("valrep_landimp_comp1_net_land_valuation")
    private String valrepLandimpComp1NetLandValuation;
    @SerializedName("valrep_landimp_comp1_price_sqm")
    private String valrepLandimpComp1PriceSqm;
    @SerializedName("valrep_landimp_comp1_price_value")
    private String valrepLandimpComp1PriceValue;
    @SerializedName("valrep_landimp_comp1_pricing_circumstance")
    private String valrepLandimpComp1PricingCircumstance;
    @SerializedName("valrep_landimp_comp1_prop_interest")
    private String valrepLandimpComp1PropInterest;
    @SerializedName("valrep_landimp_comp1_property_type")
    private String valrepLandimpComp1PropertyType;
    @SerializedName("valrep_landimp_comp1_rec_accessibility")
    private String valrepLandimpComp1RecAccessibility;
    @SerializedName("valrep_landimp_comp1_rec_accessibility_desc")
    private String valrepLandimpComp1RecAccessibilityDesc;
    @SerializedName("valrep_landimp_comp1_rec_accessibility_description")
    private String valrepLandimpComp1RecAccessibilityDescription;
    @SerializedName("valrep_landimp_comp1_rec_accessibility_subprop")
    private String valrepLandimpComp1RecAccessibilitySubprop;
    @SerializedName("valrep_landimp_comp1_rec_accessibility_type")
    private String valrepLandimpComp1RecAccessibilityType;
    @SerializedName("valrep_landimp_comp1_rec_amenities")
    private String valrepLandimpComp1RecAmenities;
    @SerializedName("valrep_landimp_comp1_rec_bargaining_allow")
    private String valrepLandimpComp1RecBargainingAllow;
    @SerializedName("valrep_landimp_comp1_rec_bargaining_allow_adjustment_price")
    private String valrepLandimpComp1RecBargainingAllowAdjustmentPrice;
    @SerializedName("valrep_landimp_comp1_rec_bargaining_allow_desc")
    private String valrepLandimpComp1RecBargainingAllowDesc;
    @SerializedName("valrep_landimp_comp1_rec_bargaining_allow_description")
    private String valrepLandimpComp1RecBargainingAllowDescription;
    @SerializedName("valrep_landimp_comp1_rec_bargaining_allow_subprop")
    private String valrepLandimpComp1RecBargainingAllowSubprop;
    @SerializedName("valrep_landimp_comp1_rec_bargaining_allow_type")
    private String valrepLandimpComp1RecBargainingAllowType;
    @SerializedName("valrep_landimp_comp1_rec_bargaining_allowance")
    private String valrepLandimpComp1RecBargainingAllowance;
    @SerializedName("valrep_landimp_comp1_rec_bargaining_allowance_desc")
    private String valrepLandimpComp1RecBargainingAllowanceDesc;
    @SerializedName("valrep_landimp_comp1_rec_corner_influence")
    private String valrepLandimpComp1RecCornerInfluence;
    @SerializedName("valrep_landimp_comp1_rec_degree_of_devt")
    private String valrepLandimpComp1RecDegreeOfDevt;
    @SerializedName("valrep_landimp_comp1_rec_elevation")
    private String valrepLandimpComp1RecElevation;
    @SerializedName("valrep_landimp_comp1_rec_elevation_desc")
    private String valrepLandimpComp1RecElevationDesc;
    @SerializedName("valrep_landimp_comp1_rec_elevation_description")
    private String valrepLandimpComp1RecElevationDescription;
    @SerializedName("valrep_landimp_comp1_rec_elevation_subprop")
    private String valrepLandimpComp1RecElevationSubprop;
    @SerializedName("valrep_landimp_comp1_rec_elevation_type")
    private String valrepLandimpComp1RecElevationType;
    @SerializedName("valrep_landimp_comp1_rec_location")
    private String valrepLandimpComp1RecLocation;
    @SerializedName("valrep_landimp_comp1_rec_location_desc")
    private String valrepLandimpComp1RecLocationDesc;
    @SerializedName("valrep_landimp_comp1_rec_location_description")
    private String valrepLandimpComp1RecLocationDescription;
    @SerializedName("valrep_landimp_comp1_rec_location_subprop")
    private String valrepLandimpComp1RecLocationSubprop;
    @SerializedName("valrep_landimp_comp1_rec_location_type")
    private String valrepLandimpComp1RecLocationType;
    @SerializedName("valrep_landimp_comp1_rec_lot_type")
    private String valrepLandimpComp1RecLotType;
    @SerializedName("valrep_landimp_comp1_rec_lot_type_desc")
    private String valrepLandimpComp1RecLotTypeDesc;
    @SerializedName("valrep_landimp_comp1_rec_lot_type_description")
    private String valrepLandimpComp1RecLotTypeDescription;
    @SerializedName("valrep_landimp_comp1_rec_lot_type_subprop")
    private String valrepLandimpComp1RecLotTypeSubprop;
    @SerializedName("valrep_landimp_comp1_rec_lot_type_type")
    private String valrepLandimpComp1RecLotTypeType;
    @SerializedName("valrep_landimp_comp1_rec_neighborhood")
    private String valrepLandimpComp1RecNeighborhood;
    @SerializedName("valrep_landimp_comp1_rec_neighborhood_desc")
    private String valrepLandimpComp1RecNeighborhoodDesc;
    @SerializedName("valrep_landimp_comp1_rec_neighborhood_description")
    private String valrepLandimpComp1RecNeighborhoodDescription;
    @SerializedName("valrep_landimp_comp1_rec_neighborhood_subprop")
    private String valrepLandimpComp1RecNeighborhoodSubprop;
    @SerializedName("valrep_landimp_comp1_rec_neighborhood_type")
    private String valrepLandimpComp1RecNeighborhoodType;
    @SerializedName("valrep_landimp_comp1_rec_others")
    private String valrepLandimpComp1RecOthers;
    @SerializedName("valrep_landimp_comp1_rec_others2")
    private String valrepLandimpComp1RecOthers2;
    @SerializedName("valrep_landimp_comp1_rec_others2_desc")
    private String valrepLandimpComp1RecOthers2Desc;
    @SerializedName("valrep_landimp_comp1_rec_others2_description")
    private String valrepLandimpComp1RecOthers2Description;
    @SerializedName("valrep_landimp_comp1_rec_others2_subprop")
    private String valrepLandimpComp1RecOthers2Subprop;
    @SerializedName("valrep_landimp_comp1_rec_others2_type")
    private String valrepLandimpComp1RecOthers2Type;
    @SerializedName("valrep_landimp_comp1_rec_others3")
    private String valrepLandimpComp1RecOthers3;
    @SerializedName("valrep_landimp_comp1_rec_others3_desc")
    private String valrepLandimpComp1RecOthers3Desc;
    @SerializedName("valrep_landimp_comp1_rec_others3_description")
    private String valrepLandimpComp1RecOthers3Description;
    @SerializedName("valrep_landimp_comp1_rec_others3_subprop")
    private String valrepLandimpComp1RecOthers3Subprop;
    @SerializedName("valrep_landimp_comp1_rec_others3_type")
    private String valrepLandimpComp1RecOthers3Type;
    @SerializedName("valrep_landimp_comp1_rec_others_desc")
    private String valrepLandimpComp1RecOthersDesc;
    @SerializedName("valrep_landimp_comp1_rec_others_description")
    private String valrepLandimpComp1RecOthersDescription;
    @SerializedName("valrep_landimp_comp1_rec_others_name")
    private String valrepLandimpComp1RecOthersName;
    @SerializedName("valrep_landimp_comp1_rec_others_subprop")
    private String valrepLandimpComp1RecOthersSubprop;
    @SerializedName("valrep_landimp_comp1_rec_others_type")
    private String valrepLandimpComp1RecOthersType;
    @SerializedName("valrep_landimp_comp1_rec_prop_rights")
    private String valrepLandimpComp1RecPropRights;
    @SerializedName("valrep_landimp_comp1_rec_prop_rights_desc")
    private String valrepLandimpComp1RecPropRightsDesc;
    @SerializedName("valrep_landimp_comp1_rec_property_rights_conveyed")
    private String valrepLandimpComp1RecPropertyRightsConveyed;
    @SerializedName("valrep_landimp_comp1_rec_property_rights_conveyed_adjustment_price")
    private String valrepLandimpComp1RecPropertyRightsConveyedAdjustmentPrice;
    @SerializedName("valrep_landimp_comp1_rec_property_rights_conveyed_desc")
    private String valrepLandimpComp1RecPropertyRightsConveyedDesc;
    @SerializedName("valrep_landimp_comp1_rec_property_rights_conveyed_description")
    private String valrepLandimpComp1RecPropertyRightsConveyedDescription;
    @SerializedName("valrep_landimp_comp1_rec_property_rights_conveyed_subprop")
    private String valrepLandimpComp1RecPropertyRightsConveyedSubprop;
    @SerializedName("valrep_landimp_comp1_rec_property_rights_conveyed_type")
    private String valrepLandimpComp1RecPropertyRightsConveyedType;
    @SerializedName("valrep_landimp_comp1_rec_road")
    private String valrepLandimpComp1RecRoad;
    @SerializedName("valrep_landimp_comp1_rec_road_desc")
    private String valrepLandimpComp1RecRoadDesc;
    @SerializedName("valrep_landimp_comp1_rec_road_description")
    private String valrepLandimpComp1RecRoadDescription;
    @SerializedName("valrep_landimp_comp1_rec_road_subprop")
    private String valrepLandimpComp1RecRoadSubprop;
    @SerializedName("valrep_landimp_comp1_rec_road_type")
    private String valrepLandimpComp1RecRoadType;
    @SerializedName("valrep_landimp_comp1_rec_shape")
    private String valrepLandimpComp1RecShape;
    @SerializedName("valrep_landimp_comp1_rec_shape_desc")
    private String valrepLandimpComp1RecShapeDesc;
    @SerializedName("valrep_landimp_comp1_rec_shape_description")
    private String valrepLandimpComp1RecShapeDescription;
    @SerializedName("valrep_landimp_comp1_rec_shape_subprop")
    private String valrepLandimpComp1RecShapeSubprop;
    @SerializedName("valrep_landimp_comp1_rec_shape_type")
    private String valrepLandimpComp1RecShapeType;
    @SerializedName("valrep_landimp_comp1_rec_size")
    private String valrepLandimpComp1RecSize;
    @SerializedName("valrep_landimp_comp1_rec_size_desc")
    private String valrepLandimpComp1RecSizeDesc;
    @SerializedName("valrep_landimp_comp1_rec_size_description")
    private String valrepLandimpComp1RecSizeDescription;
    @SerializedName("valrep_landimp_comp1_rec_size_subprop")
    private String valrepLandimpComp1RecSizeSubprop;
    @SerializedName("valrep_landimp_comp1_rec_size_type")
    private String valrepLandimpComp1RecSizeType;
    @SerializedName("valrep_landimp_comp1_rec_terrain")
    private String valrepLandimpComp1RecTerrain;
    @SerializedName("valrep_landimp_comp1_rec_terrain_desc")
    private String valrepLandimpComp1RecTerrainDesc;
    @SerializedName("valrep_landimp_comp1_rec_terrain_description")
    private String valrepLandimpComp1RecTerrainDescription;
    @SerializedName("valrep_landimp_comp1_rec_terrain_subprop")
    private String valrepLandimpComp1RecTerrainSubprop;
    @SerializedName("valrep_landimp_comp1_rec_terrain_type")
    private String valrepLandimpComp1RecTerrainType;
    @SerializedName("valrep_landimp_comp1_rec_time")
    private String valrepLandimpComp1RecTime;
    @SerializedName("valrep_landimp_comp1_rec_time1_subprop")
    private String valrepLandimpComp1RecTime1Subprop;
    @SerializedName("valrep_landimp_comp1_rec_time1_type")
    private String valrepLandimpComp1RecTime1Type;
    @SerializedName("valrep_landimp_comp1_rec_time_adjustment_price")
    private String valrepLandimpComp1RecTimeAdjustmentPrice;
    @SerializedName("valrep_landimp_comp1_rec_time_desc")
    private String valrepLandimpComp1RecTimeDesc;
    @SerializedName("valrep_landimp_comp1_rec_time_description")
    private String valrepLandimpComp1RecTimeDescription;
    @SerializedName("valrep_landimp_comp1_rec_time_element")
    private String valrepLandimpComp1RecTimeElement;
    @SerializedName("valrep_landimp_comp1_rec_time_element_desc")
    private String valrepLandimpComp1RecTimeElementDesc;
    @SerializedName("valrep_landimp_comp1_rec_tru_lot")
    private String valrepLandimpComp1RecTruLot;
    @SerializedName("valrep_landimp_comp1_remarks")
    private String valrepLandimpComp1Remarks;
    @SerializedName("valrep_landimp_comp1_selling_price")
    private String valrepLandimpComp1SellingPrice;
    @SerializedName("valrep_landimp_comp1_shape")
    private String valrepLandimpComp1Shape;
    @SerializedName("valrep_landimp_comp1_source")
    private String valrepLandimpComp1Source;
    @SerializedName("valrep_landimp_comp1_total_adj")
    private String valrepLandimpComp1TotalAdj;
    @SerializedName("valrep_landimp_comp1_totalgross_adj")
    private String valrepLandimpComp1TotalgrossAdj;
    @SerializedName("valrep_landimp_comp1_totalnet_adj")
    private String valrepLandimpComp1TotalnetAdj;
    @SerializedName("valrep_landimp_comp1_weight")
    private String valrepLandimpComp1Weight;
    @SerializedName("valrep_landimp_comp1_weight_equivalent")
    private String valrepLandimpComp1WeightEquivalent;
    @SerializedName("valrep_landimp_comp1_weighted_value")
    private String valrepLandimpComp1WeightedValue;
    @SerializedName("valrep_landimp_comp1_wgt_dist")
    private String valrepLandimpComp1WgtDist;
    @SerializedName("valrep_landimp_comp1_zoning")
    private String valrepLandimpComp1Zoning;
    @SerializedName("valrep_landimp_comp2_adjustable_price_sqm")
    private String valrepLandimpComp2AdjustablePriceSqm;
    @SerializedName("valrep_landimp_comp2_adjusted_value")
    private String valrepLandimpComp2AdjustedValue;
    @SerializedName("valrep_landimp_comp2_approx_age")
    private String valrepLandimpComp2ApproxAge;
    @SerializedName("valrep_landimp_comp2_area")
    private String valrepLandimpComp2Area;
    @SerializedName("valrep_landimp_comp2_base_price")
    private String valrepLandimpComp2BasePrice;
    @SerializedName("valrep_landimp_comp2_base_price_sqm")
    private String valrepLandimpComp2BasePriceSqm;
    @SerializedName("valrep_landimp_comp2_bldg_area")
    private String valrepLandimpComp2BldgArea;
    @SerializedName("valrep_landimp_comp2_bldg_depreciation")
    private String valrepLandimpComp2BldgDepreciation;
    @SerializedName("valrep_landimp_comp2_bldg_depreciation_val")
    private String valrepLandimpComp2BldgDepreciationVal;
    @SerializedName("valrep_landimp_comp2_bldg_replacement_unit_value")
    private String valrepLandimpComp2BldgReplacementUnitValue;
    @SerializedName("valrep_landimp_comp2_bldg_replacement_value")
    private String valrepLandimpComp2BldgReplacementValue;
    @SerializedName("valrep_landimp_comp2_characteristic")
    private String valrepLandimpComp2Characteristic;
    @SerializedName("valrep_landimp_comp2_characteristics")
    private String valrepLandimpComp2Characteristics;
    @SerializedName("valrep_landimp_comp2_concluded_adjustment")
    private String valrepLandimpComp2ConcludedAdjustment;
    @SerializedName("valrep_landimp_comp2_contact_no")
    private String valrepLandimpComp2ContactNo;
    @SerializedName("valrep_landimp_comp2_date_day")
    private String valrepLandimpComp2DateDay;
    @SerializedName("valrep_landimp_comp2_date_month")
    private String valrepLandimpComp2DateMonth;
    @SerializedName("valrep_landimp_comp2_date_year")
    private String valrepLandimpComp2DateYear;
    @SerializedName("valrep_landimp_comp2_discount_rate")
    private String valrepLandimpComp2DiscountRate;
    @SerializedName("valrep_landimp_comp2_discounts")
    private String valrepLandimpComp2Discounts;
    @SerializedName("valrep_landimp_comp2_effective_date")
    private String valrepLandimpComp2EffectiveDate;
    @SerializedName("valrep_landimp_comp2_land_residual_value")
    private String valrepLandimpComp2LandResidualValue;
    @SerializedName("valrep_landimp_comp2_location")
    private String valrepLandimpComp2Location;
    @SerializedName("valrep_landimp_comp2_net_bldg_value")
    private String valrepLandimpComp2NetBldgValue;
    @SerializedName("valrep_landimp_comp2_net_land_valuation")
    private String valrepLandimpComp2NetLandValuation;
    @SerializedName("valrep_landimp_comp2_price_sqm")
    private String valrepLandimpComp2PriceSqm;
    @SerializedName("valrep_landimp_comp2_price_value")
    private String valrepLandimpComp2PriceValue;
    @SerializedName("valrep_landimp_comp2_pricing_circumstance")
    private String valrepLandimpComp2PricingCircumstance;
    @SerializedName("valrep_landimp_comp2_prop_interest")
    private String valrepLandimpComp2PropInterest;
    @SerializedName("valrep_landimp_comp2_property_type")
    private String valrepLandimpComp2PropertyType;
    @SerializedName("valrep_landimp_comp2_rec_accessibility")
    private String valrepLandimpComp2RecAccessibility;
    @SerializedName("valrep_landimp_comp2_rec_accessibility_desc")
    private String valrepLandimpComp2RecAccessibilityDesc;
    @SerializedName("valrep_landimp_comp2_rec_accessibility_description")
    private String valrepLandimpComp2RecAccessibilityDescription;
    @SerializedName("valrep_landimp_comp2_rec_amenities")
    private String valrepLandimpComp2RecAmenities;
    @SerializedName("valrep_landimp_comp2_rec_bargaining_allow")
    private String valrepLandimpComp2RecBargainingAllow;
    @SerializedName("valrep_landimp_comp2_rec_bargaining_allow_adjustment_price")
    private String valrepLandimpComp2RecBargainingAllowAdjustmentPrice;
    @SerializedName("valrep_landimp_comp2_rec_bargaining_allow_desc")
    private String valrepLandimpComp2RecBargainingAllowDesc;
    @SerializedName("valrep_landimp_comp2_rec_bargaining_allow_description")
    private String valrepLandimpComp2RecBargainingAllowDescription;
    @SerializedName("valrep_landimp_comp2_rec_bargaining_allowance")
    private String valrepLandimpComp2RecBargainingAllowance;
    @SerializedName("valrep_landimp_comp2_rec_bargaining_allowance_desc")
    private String valrepLandimpComp2RecBargainingAllowanceDesc;
    @SerializedName("valrep_landimp_comp2_rec_corner_influence")
    private String valrepLandimpComp2RecCornerInfluence;
    @SerializedName("valrep_landimp_comp2_rec_degree_of_devt")
    private String valrepLandimpComp2RecDegreeOfDevt;
    @SerializedName("valrep_landimp_comp2_rec_elevation")
    private String valrepLandimpComp2RecElevation;
    @SerializedName("valrep_landimp_comp2_rec_elevation_desc")
    private String valrepLandimpComp2RecElevationDesc;
    @SerializedName("valrep_landimp_comp2_rec_elevation_description")
    private String valrepLandimpComp2RecElevationDescription;
    @SerializedName("valrep_landimp_comp2_rec_location")
    private String valrepLandimpComp2RecLocation;
    @SerializedName("valrep_landimp_comp2_rec_location_desc")
    private String valrepLandimpComp2RecLocationDesc;
    @SerializedName("valrep_landimp_comp2_rec_location_description")
    private String valrepLandimpComp2RecLocationDescription;
    @SerializedName("valrep_landimp_comp2_rec_lot_type")
    private String valrepLandimpComp2RecLotType;
    @SerializedName("valrep_landimp_comp2_rec_lot_type_desc")
    private String valrepLandimpComp2RecLotTypeDesc;
    @SerializedName("valrep_landimp_comp2_rec_lot_type_description")
    private String valrepLandimpComp2RecLotTypeDescription;
    @SerializedName("valrep_landimp_comp2_rec_neighborhood")
    private String valrepLandimpComp2RecNeighborhood;
    @SerializedName("valrep_landimp_comp2_rec_neighborhood_desc")
    private String valrepLandimpComp2RecNeighborhoodDesc;
    @SerializedName("valrep_landimp_comp2_rec_neighborhood_description")
    private String valrepLandimpComp2RecNeighborhoodDescription;
    @SerializedName("valrep_landimp_comp2_rec_neighborhood_type")
    private String valrepLandimpComp2RecNeighborhoodType;
    @SerializedName("valrep_landimp_comp2_rec_others")
    private String valrepLandimpComp2RecOthers;
    @SerializedName("valrep_landimp_comp2_rec_others2")
    private String valrepLandimpComp2RecOthers2;
    @SerializedName("valrep_landimp_comp2_rec_others2_desc")
    private String valrepLandimpComp2RecOthers2Desc;
    @SerializedName("valrep_landimp_comp2_rec_others2_description")
    private String valrepLandimpComp2RecOthers2Description;
    @SerializedName("valrep_landimp_comp2_rec_others3")
    private String valrepLandimpComp2RecOthers3;
    @SerializedName("valrep_landimp_comp2_rec_others3_desc")
    private String valrepLandimpComp2RecOthers3Desc;
    @SerializedName("valrep_landimp_comp2_rec_others3_description")
    private String valrepLandimpComp2RecOthers3Description;
    @SerializedName("valrep_landimp_comp2_rec_others_desc")
    private String valrepLandimpComp2RecOthersDesc;
    @SerializedName("valrep_landimp_comp2_rec_others_description")
    private String valrepLandimpComp2RecOthersDescription;
    @SerializedName("valrep_landimp_comp2_rec_others_name")
    private String valrepLandimpComp2RecOthersName;
    @SerializedName("valrep_landimp_comp2_rec_prop_rights")
    private String valrepLandimpComp2RecPropRights;
    @SerializedName("valrep_landimp_comp2_rec_prop_rights_desc")
    private String valrepLandimpComp2RecPropRightsDesc;
    @SerializedName("valrep_landimp_comp2_rec_property_rights_conveyed")
    private String valrepLandimpComp2RecPropertyRightsConveyed;
    @SerializedName("valrep_landimp_comp2_rec_property_rights_conveyed_adjustment_price")
    private String valrepLandimpComp2RecPropertyRightsConveyedAdjustmentPrice;
    @SerializedName("valrep_landimp_comp2_rec_property_rights_conveyed_desc")
    private String valrepLandimpComp2RecPropertyRightsConveyedDesc;
    @SerializedName("valrep_landimp_comp2_rec_property_rights_conveyed_description")
    private String valrepLandimpComp2RecPropertyRightsConveyedDescription;
    @SerializedName("valrep_landimp_comp2_rec_road")
    private String valrepLandimpComp2RecRoad;
    @SerializedName("valrep_landimp_comp2_rec_road_desc")
    private String valrepLandimpComp2RecRoadDesc;
    @SerializedName("valrep_landimp_comp2_rec_road_description")
    private String valrepLandimpComp2RecRoadDescription;
    @SerializedName("valrep_landimp_comp2_rec_shape")
    private String valrepLandimpComp2RecShape;
    @SerializedName("valrep_landimp_comp2_rec_shape_desc")
    private String valrepLandimpComp2RecShapeDesc;
    @SerializedName("valrep_landimp_comp2_rec_shape_description")
    private String valrepLandimpComp2RecShapeDescription;
    @SerializedName("valrep_landimp_comp2_rec_size")
    private String valrepLandimpComp2RecSize;
    @SerializedName("valrep_landimp_comp2_rec_size_desc")
    private String valrepLandimpComp2RecSizeDesc;
    @SerializedName("valrep_landimp_comp2_rec_size_description")
    private String valrepLandimpComp2RecSizeDescription;
    @SerializedName("valrep_landimp_comp2_rec_terrain")
    private String valrepLandimpComp2RecTerrain;
    @SerializedName("valrep_landimp_comp2_rec_terrain_desc")
    private String valrepLandimpComp2RecTerrainDesc;
    @SerializedName("valrep_landimp_comp2_rec_terrain_description")
    private String valrepLandimpComp2RecTerrainDescription;
    @SerializedName("valrep_landimp_comp2_rec_time")
    private String valrepLandimpComp2RecTime;
    @SerializedName("valrep_landimp_comp2_rec_time_adjustment_price")
    private String valrepLandimpComp2RecTimeAdjustmentPrice;
    @SerializedName("valrep_landimp_comp2_rec_time_desc")
    private String valrepLandimpComp2RecTimeDesc;
    @SerializedName("valrep_landimp_comp2_rec_time_description")
    private String valrepLandimpComp2RecTimeDescription;
    @SerializedName("valrep_landimp_comp2_rec_time_element")
    private String valrepLandimpComp2RecTimeElement;
    @SerializedName("valrep_landimp_comp2_rec_time_element_desc")
    private String valrepLandimpComp2RecTimeElementDesc;
    @SerializedName("valrep_landimp_comp2_rec_tru_lot")
    private String valrepLandimpComp2RecTruLot;
    @SerializedName("valrep_landimp_comp2_remarks")
    private String valrepLandimpComp2Remarks;
    @SerializedName("valrep_landimp_comp2_selling_price")
    private String valrepLandimpComp2SellingPrice;
    @SerializedName("valrep_landimp_comp2_shape")
    private String valrepLandimpComp2Shape;
    @SerializedName("valrep_landimp_comp2_source")
    private String valrepLandimpComp2Source;
    @SerializedName("valrep_landimp_comp2_total_adj")
    private String valrepLandimpComp2TotalAdj;
    @SerializedName("valrep_landimp_comp2_totalgross_adj")
    private String valrepLandimpComp2TotalgrossAdj;
    @SerializedName("valrep_landimp_comp2_totalnet_adj")
    private String valrepLandimpComp2TotalnetAdj;
    @SerializedName("valrep_landimp_comp2_weight")
    private String valrepLandimpComp2Weight;
    @SerializedName("valrep_landimp_comp2_weight_equivalent")
    private String valrepLandimpComp2WeightEquivalent;
    @SerializedName("valrep_landimp_comp2_weighted_value")
    private String valrepLandimpComp2WeightedValue;
    @SerializedName("valrep_landimp_comp2_wgt_dist")
    private String valrepLandimpComp2WgtDist;
    @SerializedName("valrep_landimp_comp2_zoning")
    private String valrepLandimpComp2Zoning;
    @SerializedName("valrep_landimp_comp3_adjustable_price_sqm")
    private String valrepLandimpComp3AdjustablePriceSqm;
    @SerializedName("valrep_landimp_comp3_adjusted_value")
    private String valrepLandimpComp3AdjustedValue;
    @SerializedName("valrep_landimp_comp3_approx_age")
    private String valrepLandimpComp3ApproxAge;
    @SerializedName("valrep_landimp_comp3_area")
    private String valrepLandimpComp3Area;
    @SerializedName("valrep_landimp_comp3_base_price")
    private String valrepLandimpComp3BasePrice;
    @SerializedName("valrep_landimp_comp3_base_price_sqm")
    private String valrepLandimpComp3BasePriceSqm;
    @SerializedName("valrep_landimp_comp3_bldg_area")
    private String valrepLandimpComp3BldgArea;
    @SerializedName("valrep_landimp_comp3_bldg_depreciation")
    private String valrepLandimpComp3BldgDepreciation;
    @SerializedName("valrep_landimp_comp3_bldg_depreciation_val")
    private String valrepLandimpComp3BldgDepreciationVal;
    @SerializedName("valrep_landimp_comp3_bldg_replacement_unit_value")
    private String valrepLandimpComp3BldgReplacementUnitValue;
    @SerializedName("valrep_landimp_comp3_bldg_replacement_value")
    private String valrepLandimpComp3BldgReplacementValue;
    @SerializedName("valrep_landimp_comp3_characteristic")
    private String valrepLandimpComp3Characteristic;
    @SerializedName("valrep_landimp_comp3_characteristics")
    private String valrepLandimpComp3Characteristics;
    @SerializedName("valrep_landimp_comp3_concluded_adjustment")
    private String valrepLandimpComp3ConcludedAdjustment;
    @SerializedName("valrep_landimp_comp3_contact_no")
    private String valrepLandimpComp3ContactNo;
    @SerializedName("valrep_landimp_comp3_date_day")
    private String valrepLandimpComp3DateDay;
    @SerializedName("valrep_landimp_comp3_date_month")
    private String valrepLandimpComp3DateMonth;
    @SerializedName("valrep_landimp_comp3_date_year")
    private String valrepLandimpComp3DateYear;
    @SerializedName("valrep_landimp_comp3_discount_rate")
    private String valrepLandimpComp3DiscountRate;
    @SerializedName("valrep_landimp_comp3_discounts")
    private String valrepLandimpComp3Discounts;
    @SerializedName("valrep_landimp_comp3_effective_date")
    private String valrepLandimpComp3EffectiveDate;
    @SerializedName("valrep_landimp_comp3_land_residual_value")
    private String valrepLandimpComp3LandResidualValue;
    @SerializedName("valrep_landimp_comp3_location")
    private String valrepLandimpComp3Location;
    @SerializedName("valrep_landimp_comp3_net_bldg_value")
    private String valrepLandimpComp3NetBldgValue;
    @SerializedName("valrep_landimp_comp3_net_land_valuation")
    private String valrepLandimpComp3NetLandValuation;
    @SerializedName("valrep_landimp_comp3_price_sqm")
    private String valrepLandimpComp3PriceSqm;
    @SerializedName("valrep_landimp_comp3_price_value")
    private String valrepLandimpComp3PriceValue;
    @SerializedName("valrep_landimp_comp3_pricing_circumstance")
    private String valrepLandimpComp3PricingCircumstance;
    @SerializedName("valrep_landimp_comp3_prop_interest")
    private String valrepLandimpComp3PropInterest;
    @SerializedName("valrep_landimp_comp3_property_type")
    private String valrepLandimpComp3PropertyType;
    @SerializedName("valrep_landimp_comp3_rec_accessibility")
    private String valrepLandimpComp3RecAccessibility;
    @SerializedName("valrep_landimp_comp3_rec_accessibility_desc")
    private String valrepLandimpComp3RecAccessibilityDesc;
    @SerializedName("valrep_landimp_comp3_rec_accessibility_description")
    private String valrepLandimpComp3RecAccessibilityDescription;
    @SerializedName("valrep_landimp_comp3_rec_amenities")
    private String valrepLandimpComp3RecAmenities;
    @SerializedName("valrep_landimp_comp3_rec_bargaining_allow")
    private String valrepLandimpComp3RecBargainingAllow;
    @SerializedName("valrep_landimp_comp3_rec_bargaining_allow_adjustment_price")
    private String valrepLandimpComp3RecBargainingAllowAdjustmentPrice;
    @SerializedName("valrep_landimp_comp3_rec_bargaining_allow_desc")
    private String valrepLandimpComp3RecBargainingAllowDesc;
    @SerializedName("valrep_landimp_comp3_rec_bargaining_allow_description")
    private String valrepLandimpComp3RecBargainingAllowDescription;
    @SerializedName("valrep_landimp_comp3_rec_bargaining_allowance")
    private String valrepLandimpComp3RecBargainingAllowance;
    @SerializedName("valrep_landimp_comp3_rec_bargaining_allowance_desc")
    private String valrepLandimpComp3RecBargainingAllowanceDesc;
    @SerializedName("valrep_landimp_comp3_rec_corner_influence")
    private String valrepLandimpComp3RecCornerInfluence;
    @SerializedName("valrep_landimp_comp3_rec_degree_of_devt")
    private String valrepLandimpComp3RecDegreeOfDevt;
    @SerializedName("valrep_landimp_comp3_rec_elevation")
    private String valrepLandimpComp3RecElevation;
    @SerializedName("valrep_landimp_comp3_rec_elevation_desc")
    private String valrepLandimpComp3RecElevationDesc;
    @SerializedName("valrep_landimp_comp3_rec_elevation_description")
    private String valrepLandimpComp3RecElevationDescription;
    @SerializedName("valrep_landimp_comp3_rec_location")
    private String valrepLandimpComp3RecLocation;
    @SerializedName("valrep_landimp_comp3_rec_location_desc")
    private String valrepLandimpComp3RecLocationDesc;
    @SerializedName("valrep_landimp_comp3_rec_location_description")
    private String valrepLandimpComp3RecLocationDescription;
    @SerializedName("valrep_landimp_comp3_rec_lot_type")
    private String valrepLandimpComp3RecLotType;
    @SerializedName("valrep_landimp_comp3_rec_lot_type_desc")
    private String valrepLandimpComp3RecLotTypeDesc;
    @SerializedName("valrep_landimp_comp3_rec_lot_type_description")
    private String valrepLandimpComp3RecLotTypeDescription;
    @SerializedName("valrep_landimp_comp3_rec_neighborhood")
    private String valrepLandimpComp3RecNeighborhood;
    @SerializedName("valrep_landimp_comp3_rec_neighborhood_desc")
    private String valrepLandimpComp3RecNeighborhoodDesc;
    @SerializedName("valrep_landimp_comp3_rec_neighborhood_description")
    private String valrepLandimpComp3RecNeighborhoodDescription;
    @SerializedName("valrep_landimp_comp3_rec_neighborhood_type")
    private String valrepLandimpComp3RecNeighborhoodType;
    @SerializedName("valrep_landimp_comp3_rec_others")
    private String valrepLandimpComp3RecOthers;
    @SerializedName("valrep_landimp_comp3_rec_others2")
    private String valrepLandimpComp3RecOthers2;
    @SerializedName("valrep_landimp_comp3_rec_others2_desc")
    private String valrepLandimpComp3RecOthers2Desc;
    @SerializedName("valrep_landimp_comp3_rec_others2_description")
    private String valrepLandimpComp3RecOthers2Description;
    @SerializedName("valrep_landimp_comp3_rec_others3")
    private String valrepLandimpComp3RecOthers3;
    @SerializedName("valrep_landimp_comp3_rec_others3_desc")
    private String valrepLandimpComp3RecOthers3Desc;
    @SerializedName("valrep_landimp_comp3_rec_others3_description")
    private String valrepLandimpComp3RecOthers3Description;
    @SerializedName("valrep_landimp_comp3_rec_others_desc")
    private String valrepLandimpComp3RecOthersDesc;
    @SerializedName("valrep_landimp_comp3_rec_others_description")
    private String valrepLandimpComp3RecOthersDescription;
    @SerializedName("valrep_landimp_comp3_rec_others_name")
    private String valrepLandimpComp3RecOthersName;
    @SerializedName("valrep_landimp_comp3_rec_prop_rights")
    private String valrepLandimpComp3RecPropRights;
    @SerializedName("valrep_landimp_comp3_rec_prop_rights_desc")
    private String valrepLandimpComp3RecPropRightsDesc;
    @SerializedName("valrep_landimp_comp3_rec_property_rights_conveyed")
    private String valrepLandimpComp3RecPropertyRightsConveyed;
    @SerializedName("valrep_landimp_comp3_rec_property_rights_conveyed_adjustment_price")
    private String valrepLandimpComp3RecPropertyRightsConveyedAdjustmentPrice;
    @SerializedName("valrep_landimp_comp3_rec_property_rights_conveyed_desc")
    private String valrepLandimpComp3RecPropertyRightsConveyedDesc;
    @SerializedName("valrep_landimp_comp3_rec_property_rights_conveyed_description")
    private String valrepLandimpComp3RecPropertyRightsConveyedDescription;
    @SerializedName("valrep_landimp_comp3_rec_road")
    private String valrepLandimpComp3RecRoad;
    @SerializedName("valrep_landimp_comp3_rec_road_desc")
    private String valrepLandimpComp3RecRoadDesc;
    @SerializedName("valrep_landimp_comp3_rec_road_description")
    private String valrepLandimpComp3RecRoadDescription;
    @SerializedName("valrep_landimp_comp3_rec_shape")
    private String valrepLandimpComp3RecShape;
    @SerializedName("valrep_landimp_comp3_rec_shape_desc")
    private String valrepLandimpComp3RecShapeDesc;
    @SerializedName("valrep_landimp_comp3_rec_shape_description")
    private String valrepLandimpComp3RecShapeDescription;
    @SerializedName("valrep_landimp_comp3_rec_size")
    private String valrepLandimpComp3RecSize;
    @SerializedName("valrep_landimp_comp3_rec_size_desc")
    private String valrepLandimpComp3RecSizeDesc;
    @SerializedName("valrep_landimp_comp3_rec_size_description")
    private String valrepLandimpComp3RecSizeDescription;
    @SerializedName("valrep_landimp_comp3_rec_terrain")
    private String valrepLandimpComp3RecTerrain;
    @SerializedName("valrep_landimp_comp3_rec_terrain_desc")
    private String valrepLandimpComp3RecTerrainDesc;
    @SerializedName("valrep_landimp_comp3_rec_terrain_description")
    private String valrepLandimpComp3RecTerrainDescription;
    @SerializedName("valrep_landimp_comp3_rec_time")
    private String valrepLandimpComp3RecTime;
    @SerializedName("valrep_landimp_comp3_rec_time_adjustment_price")
    private String valrepLandimpComp3RecTimeAdjustmentPrice;
    @SerializedName("valrep_landimp_comp3_rec_time_desc")
    private String valrepLandimpComp3RecTimeDesc;
    @SerializedName("valrep_landimp_comp3_rec_time_description")
    private String valrepLandimpComp3RecTimeDescription;
    @SerializedName("valrep_landimp_comp3_rec_time_element")
    private String valrepLandimpComp3RecTimeElement;
    @SerializedName("valrep_landimp_comp3_rec_time_element_desc")
    private String valrepLandimpComp3RecTimeElementDesc;
    @SerializedName("valrep_landimp_comp3_rec_tru_lot")
    private String valrepLandimpComp3RecTruLot;
    @SerializedName("valrep_landimp_comp3_remarks")
    private String valrepLandimpComp3Remarks;
    @SerializedName("valrep_landimp_comp3_selling_price")
    private String valrepLandimpComp3SellingPrice;
    @SerializedName("valrep_landimp_comp3_shape")
    private String valrepLandimpComp3Shape;
    @SerializedName("valrep_landimp_comp3_source")
    private String valrepLandimpComp3Source;
    @SerializedName("valrep_landimp_comp3_total_adj")
    private String valrepLandimpComp3TotalAdj;
    @SerializedName("valrep_landimp_comp3_totalgross_adj")
    private String valrepLandimpComp3TotalgrossAdj;
    @SerializedName("valrep_landimp_comp3_totalnet_adj")
    private String valrepLandimpComp3TotalnetAdj;
    @SerializedName("valrep_landimp_comp3_weight")
    private String valrepLandimpComp3Weight;
    @SerializedName("valrep_landimp_comp3_weight_equivalent")
    private String valrepLandimpComp3WeightEquivalent;
    @SerializedName("valrep_landimp_comp3_weighted_value")
    private String valrepLandimpComp3WeightedValue;
    @SerializedName("valrep_landimp_comp3_wgt_dist")
    private String valrepLandimpComp3WgtDist;
    @SerializedName("valrep_landimp_comp3_zoning")
    private String valrepLandimpComp3Zoning;
    @SerializedName("valrep_landimp_comp4_adjustable_price_sqm")
    private String valrepLandimpComp4AdjustablePriceSqm;
    @SerializedName("valrep_landimp_comp4_adjusted_value")
    private String valrepLandimpComp4AdjustedValue;
    @SerializedName("valrep_landimp_comp4_approx_age")
    private String valrepLandimpComp4ApproxAge;
    @SerializedName("valrep_landimp_comp4_area")
    private String valrepLandimpComp4Area;
    @SerializedName("valrep_landimp_comp4_base_price")
    private String valrepLandimpComp4BasePrice;
    @SerializedName("valrep_landimp_comp4_base_price_sqm")
    private String valrepLandimpComp4BasePriceSqm;
    @SerializedName("valrep_landimp_comp4_bldg_area")
    private String valrepLandimpComp4BldgArea;
    @SerializedName("valrep_landimp_comp4_bldg_depreciation")
    private String valrepLandimpComp4BldgDepreciation;
    @SerializedName("valrep_landimp_comp4_bldg_depreciation_val")
    private String valrepLandimpComp4BldgDepreciationVal;
    @SerializedName("valrep_landimp_comp4_bldg_replacement_unit_value")
    private String valrepLandimpComp4BldgReplacementUnitValue;
    @SerializedName("valrep_landimp_comp4_bldg_replacement_value")
    private String valrepLandimpComp4BldgReplacementValue;
    @SerializedName("valrep_landimp_comp4_characteristic")
    private String valrepLandimpComp4Characteristic;
    @SerializedName("valrep_landimp_comp4_characteristics")
    private String valrepLandimpComp4Characteristics;
    @SerializedName("valrep_landimp_comp4_concluded_adjustment")
    private String valrepLandimpComp4ConcludedAdjustment;
    @SerializedName("valrep_landimp_comp4_contact_no")
    private String valrepLandimpComp4ContactNo;
    @SerializedName("valrep_landimp_comp4_date_day")
    private String valrepLandimpComp4DateDay;
    @SerializedName("valrep_landimp_comp4_date_month")
    private String valrepLandimpComp4DateMonth;
    @SerializedName("valrep_landimp_comp4_date_year")
    private String valrepLandimpComp4DateYear;
    @SerializedName("valrep_landimp_comp4_discount_rate")
    private String valrepLandimpComp4DiscountRate;
    @SerializedName("valrep_landimp_comp4_discounts")
    private String valrepLandimpComp4Discounts;
    @SerializedName("valrep_landimp_comp4_effective_date")
    private String valrepLandimpComp4EffectiveDate;
    @SerializedName("valrep_landimp_comp4_land_residual_value")
    private String valrepLandimpComp4LandResidualValue;
    @SerializedName("valrep_landimp_comp4_location")
    private String valrepLandimpComp4Location;
    @SerializedName("valrep_landimp_comp4_net_bldg_value")
    private String valrepLandimpComp4NetBldgValue;
    @SerializedName("valrep_landimp_comp4_net_land_valuation")
    private String valrepLandimpComp4NetLandValuation;
    @SerializedName("valrep_landimp_comp4_price_sqm")
    private String valrepLandimpComp4PriceSqm;
    @SerializedName("valrep_landimp_comp4_price_value")
    private String valrepLandimpComp4PriceValue;
    @SerializedName("valrep_landimp_comp4_pricing_circumstance")
    private String valrepLandimpComp4PricingCircumstance;
    @SerializedName("valrep_landimp_comp4_prop_interest")
    private String valrepLandimpComp4PropInterest;
    @SerializedName("valrep_landimp_comp4_property_type")
    private String valrepLandimpComp4PropertyType;
    @SerializedName("valrep_landimp_comp4_rec_accessibility")
    private String valrepLandimpComp4RecAccessibility;
    @SerializedName("valrep_landimp_comp4_rec_accessibility_desc")
    private String valrepLandimpComp4RecAccessibilityDesc;
    @SerializedName("valrep_landimp_comp4_rec_accessibility_description")
    private String valrepLandimpComp4RecAccessibilityDescription;
    @SerializedName("valrep_landimp_comp4_rec_amenities")
    private String valrepLandimpComp4RecAmenities;
    @SerializedName("valrep_landimp_comp4_rec_bargaining_allow")
    private String valrepLandimpComp4RecBargainingAllow;
    @SerializedName("valrep_landimp_comp4_rec_bargaining_allow_adjustment_price")
    private String valrepLandimpComp4RecBargainingAllowAdjustmentPrice;
    @SerializedName("valrep_landimp_comp4_rec_bargaining_allow_desc")
    private String valrepLandimpComp4RecBargainingAllowDesc;
    @SerializedName("valrep_landimp_comp4_rec_bargaining_allow_description")
    private String valrepLandimpComp4RecBargainingAllowDescription;
    @SerializedName("valrep_landimp_comp4_rec_bargaining_allowance")
    private String valrepLandimpComp4RecBargainingAllowance;
    @SerializedName("valrep_landimp_comp4_rec_bargaining_allowance_desc")
    private String valrepLandimpComp4RecBargainingAllowanceDesc;
    @SerializedName("valrep_landimp_comp4_rec_corner_influence")
    private String valrepLandimpComp4RecCornerInfluence;
    @SerializedName("valrep_landimp_comp4_rec_degree_of_devt")
    private String valrepLandimpComp4RecDegreeOfDevt;
    @SerializedName("valrep_landimp_comp4_rec_elevation")
    private String valrepLandimpComp4RecElevation;
    @SerializedName("valrep_landimp_comp4_rec_elevation_desc")
    private String valrepLandimpComp4RecElevationDesc;
    @SerializedName("valrep_landimp_comp4_rec_elevation_description")
    private String valrepLandimpComp4RecElevationDescription;
    @SerializedName("valrep_landimp_comp4_rec_location")
    private String valrepLandimpComp4RecLocation;
    @SerializedName("valrep_landimp_comp4_rec_location_desc")
    private String valrepLandimpComp4RecLocationDesc;
    @SerializedName("valrep_landimp_comp4_rec_location_description")
    private String valrepLandimpComp4RecLocationDescription;
    @SerializedName("valrep_landimp_comp4_rec_lot_type")
    private String valrepLandimpComp4RecLotType;
    @SerializedName("valrep_landimp_comp4_rec_lot_type_desc")
    private String valrepLandimpComp4RecLotTypeDesc;
    @SerializedName("valrep_landimp_comp4_rec_lot_type_description")
    private String valrepLandimpComp4RecLotTypeDescription;
    @SerializedName("valrep_landimp_comp4_rec_neighborhood")
    private String valrepLandimpComp4RecNeighborhood;
    @SerializedName("valrep_landimp_comp4_rec_neighborhood_desc")
    private String valrepLandimpComp4RecNeighborhoodDesc;
    @SerializedName("valrep_landimp_comp4_rec_neighborhood_description")
    private String valrepLandimpComp4RecNeighborhoodDescription;
    @SerializedName("valrep_landimp_comp4_rec_neighborhood_type")
    private String valrepLandimpComp4RecNeighborhoodType;
    @SerializedName("valrep_landimp_comp4_rec_others")
    private String valrepLandimpComp4RecOthers;
    @SerializedName("valrep_landimp_comp4_rec_others2")
    private String valrepLandimpComp4RecOthers2;
    @SerializedName("valrep_landimp_comp4_rec_others2_desc")
    private String valrepLandimpComp4RecOthers2Desc;
    @SerializedName("valrep_landimp_comp4_rec_others2_description")
    private String valrepLandimpComp4RecOthers2Description;
    @SerializedName("valrep_landimp_comp4_rec_others3")
    private String valrepLandimpComp4RecOthers3;
    @SerializedName("valrep_landimp_comp4_rec_others3_desc")
    private String valrepLandimpComp4RecOthers3Desc;
    @SerializedName("valrep_landimp_comp4_rec_others3_description")
    private String valrepLandimpComp4RecOthers3Description;
    @SerializedName("valrep_landimp_comp4_rec_others_desc")
    private String valrepLandimpComp4RecOthersDesc;
    @SerializedName("valrep_landimp_comp4_rec_others_description")
    private String valrepLandimpComp4RecOthersDescription;
    @SerializedName("valrep_landimp_comp4_rec_others_name")
    private String valrepLandimpComp4RecOthersName;
    @SerializedName("valrep_landimp_comp4_rec_prop_rights")
    private String valrepLandimpComp4RecPropRights;
    @SerializedName("valrep_landimp_comp4_rec_prop_rights_desc")
    private String valrepLandimpComp4RecPropRightsDesc;
    @SerializedName("valrep_landimp_comp4_rec_property_rights_conveyed")
    private String valrepLandimpComp4RecPropertyRightsConveyed;
    @SerializedName("valrep_landimp_comp4_rec_property_rights_conveyed_adjustment_price")
    private String valrepLandimpComp4RecPropertyRightsConveyedAdjustmentPrice;
    @SerializedName("valrep_landimp_comp4_rec_property_rights_conveyed_desc")
    private String valrepLandimpComp4RecPropertyRightsConveyedDesc;
    @SerializedName("valrep_landimp_comp4_rec_property_rights_conveyed_description")
    private String valrepLandimpComp4RecPropertyRightsConveyedDescription;
    @SerializedName("valrep_landimp_comp4_rec_road")
    private String valrepLandimpComp4RecRoad;
    @SerializedName("valrep_landimp_comp4_rec_road_desc")
    private String valrepLandimpComp4RecRoadDesc;
    @SerializedName("valrep_landimp_comp4_rec_road_description")
    private String valrepLandimpComp4RecRoadDescription;
    @SerializedName("valrep_landimp_comp4_rec_shape")
    private String valrepLandimpComp4RecShape;
    @SerializedName("valrep_landimp_comp4_rec_shape_desc")
    private String valrepLandimpComp4RecShapeDesc;
    @SerializedName("valrep_landimp_comp4_rec_shape_description")
    private String valrepLandimpComp4RecShapeDescription;
    @SerializedName("valrep_landimp_comp4_rec_size")
    private String valrepLandimpComp4RecSize;
    @SerializedName("valrep_landimp_comp4_rec_size_desc")
    private String valrepLandimpComp4RecSizeDesc;
    @SerializedName("valrep_landimp_comp4_rec_size_description")
    private String valrepLandimpComp4RecSizeDescription;
    @SerializedName("valrep_landimp_comp4_rec_terrain")
    private String valrepLandimpComp4RecTerrain;
    @SerializedName("valrep_landimp_comp4_rec_terrain_desc")
    private String valrepLandimpComp4RecTerrainDesc;
    @SerializedName("valrep_landimp_comp4_rec_terrain_description")
    private String valrepLandimpComp4RecTerrainDescription;
    @SerializedName("valrep_landimp_comp4_rec_time")
    private String valrepLandimpComp4RecTime;
    @SerializedName("valrep_landimp_comp4_rec_time_adjustment_price")
    private String valrepLandimpComp4RecTimeAdjustmentPrice;
    @SerializedName("valrep_landimp_comp4_rec_time_desc")
    private String valrepLandimpComp4RecTimeDesc;
    @SerializedName("valrep_landimp_comp4_rec_time_description")
    private String valrepLandimpComp4RecTimeDescription;
    @SerializedName("valrep_landimp_comp4_rec_time_element")
    private String valrepLandimpComp4RecTimeElement;
    @SerializedName("valrep_landimp_comp4_rec_time_element_desc")
    private String valrepLandimpComp4RecTimeElementDesc;
    @SerializedName("valrep_landimp_comp4_rec_tru_lot")
    private String valrepLandimpComp4RecTruLot;
    @SerializedName("valrep_landimp_comp4_remarks")
    private String valrepLandimpComp4Remarks;
    @SerializedName("valrep_landimp_comp4_selling_price")
    private String valrepLandimpComp4SellingPrice;
    @SerializedName("valrep_landimp_comp4_shape")
    private String valrepLandimpComp4Shape;
    @SerializedName("valrep_landimp_comp4_source")
    private String valrepLandimpComp4Source;
    @SerializedName("valrep_landimp_comp4_total_adj")
    private String valrepLandimpComp4TotalAdj;
    @SerializedName("valrep_landimp_comp4_totalgross_adj")
    private String valrepLandimpComp4TotalgrossAdj;
    @SerializedName("valrep_landimp_comp4_totalnet_adj")
    private String valrepLandimpComp4TotalnetAdj;
    @SerializedName("valrep_landimp_comp4_weight")
    private String valrepLandimpComp4Weight;
    @SerializedName("valrep_landimp_comp4_weight_equivalent")
    private String valrepLandimpComp4WeightEquivalent;
    @SerializedName("valrep_landimp_comp4_weighted_value")
    private String valrepLandimpComp4WeightedValue;
    @SerializedName("valrep_landimp_comp4_wgt_dist")
    private String valrepLandimpComp4WgtDist;
    @SerializedName("valrep_landimp_comp4_zoning")
    private String valrepLandimpComp4Zoning;
    @SerializedName("valrep_landimp_comp5_adjustable_price_sqm")
    private String valrepLandimpComp5AdjustablePriceSqm;
    @SerializedName("valrep_landimp_comp5_adjusted_value")
    private String valrepLandimpComp5AdjustedValue;
    @SerializedName("valrep_landimp_comp5_approx_age")
    private String valrepLandimpComp5ApproxAge;
    @SerializedName("valrep_landimp_comp5_area")
    private String valrepLandimpComp5Area;
    @SerializedName("valrep_landimp_comp5_base_price")
    private String valrepLandimpComp5BasePrice;
    @SerializedName("valrep_landimp_comp5_base_price_sqm")
    private String valrepLandimpComp5BasePriceSqm;
    @SerializedName("valrep_landimp_comp5_bldg_area")
    private String valrepLandimpComp5BldgArea;
    @SerializedName("valrep_landimp_comp5_bldg_depreciation")
    private String valrepLandimpComp5BldgDepreciation;
    @SerializedName("valrep_landimp_comp5_bldg_depreciation_val")
    private String valrepLandimpComp5BldgDepreciationVal;
    @SerializedName("valrep_landimp_comp5_bldg_replacement_unit_value")
    private String valrepLandimpComp5BldgReplacementUnitValue;
    @SerializedName("valrep_landimp_comp5_bldg_replacement_value")
    private String valrepLandimpComp5BldgReplacementValue;
    @SerializedName("valrep_landimp_comp5_characteristic")
    private String valrepLandimpComp5Characteristic;
    @SerializedName("valrep_landimp_comp5_characteristics")
    private String valrepLandimpComp5Characteristics;
    @SerializedName("valrep_landimp_comp5_concluded_adjustment")
    private String valrepLandimpComp5ConcludedAdjustment;
    @SerializedName("valrep_landimp_comp5_contact_no")
    private String valrepLandimpComp5ContactNo;
    @SerializedName("valrep_landimp_comp5_date_day")
    private String valrepLandimpComp5DateDay;
    @SerializedName("valrep_landimp_comp5_date_month")
    private String valrepLandimpComp5DateMonth;
    @SerializedName("valrep_landimp_comp5_date_year")
    private String valrepLandimpComp5DateYear;
    @SerializedName("valrep_landimp_comp5_discount_rate")
    private String valrepLandimpComp5DiscountRate;
    @SerializedName("valrep_landimp_comp5_discounts")
    private String valrepLandimpComp5Discounts;
    @SerializedName("valrep_landimp_comp5_effective_date")
    private String valrepLandimpComp5EffectiveDate;
    @SerializedName("valrep_landimp_comp5_land_residual_value")
    private String valrepLandimpComp5LandResidualValue;
    @SerializedName("valrep_landimp_comp5_location")
    private String valrepLandimpComp5Location;
    @SerializedName("valrep_landimp_comp5_net_bldg_value")
    private String valrepLandimpComp5NetBldgValue;
    @SerializedName("valrep_landimp_comp5_net_land_valuation")
    private String valrepLandimpComp5NetLandValuation;
    @SerializedName("valrep_landimp_comp5_price_sqm")
    private String valrepLandimpComp5PriceSqm;
    @SerializedName("valrep_landimp_comp5_price_value")
    private String valrepLandimpComp5PriceValue;
    @SerializedName("valrep_landimp_comp5_pricing_circumstance")
    private String valrepLandimpComp5PricingCircumstance;
    @SerializedName("valrep_landimp_comp5_prop_interest")
    private String valrepLandimpComp5PropInterest;
    @SerializedName("valrep_landimp_comp5_property_type")
    private String valrepLandimpComp5PropertyType;
    @SerializedName("valrep_landimp_comp5_rec_accessibility")
    private String valrepLandimpComp5RecAccessibility;
    @SerializedName("valrep_landimp_comp5_rec_accessibility_desc")
    private String valrepLandimpComp5RecAccessibilityDesc;
    @SerializedName("valrep_landimp_comp5_rec_accessibility_description")
    private String valrepLandimpComp5RecAccessibilityDescription;
    @SerializedName("valrep_landimp_comp5_rec_amenities")
    private String valrepLandimpComp5RecAmenities;
    @SerializedName("valrep_landimp_comp5_rec_bargaining_allow")
    private String valrepLandimpComp5RecBargainingAllow;
    @SerializedName("valrep_landimp_comp5_rec_bargaining_allow_adjustment_price")
    private String valrepLandimpComp5RecBargainingAllowAdjustmentPrice;
    @SerializedName("valrep_landimp_comp5_rec_bargaining_allow_desc")
    private String valrepLandimpComp5RecBargainingAllowDesc;
    @SerializedName("valrep_landimp_comp5_rec_bargaining_allow_description")
    private String valrepLandimpComp5RecBargainingAllowDescription;
    @SerializedName("valrep_landimp_comp5_rec_bargaining_allowance")
    private String valrepLandimpComp5RecBargainingAllowance;
    @SerializedName("valrep_landimp_comp5_rec_bargaining_allowance_desc")
    private String valrepLandimpComp5RecBargainingAllowanceDesc;
    @SerializedName("valrep_landimp_comp5_rec_corner_influence")
    private String valrepLandimpComp5RecCornerInfluence;
    @SerializedName("valrep_landimp_comp5_rec_degree_of_devt")
    private String valrepLandimpComp5RecDegreeOfDevt;
    @SerializedName("valrep_landimp_comp5_rec_elevation")
    private String valrepLandimpComp5RecElevation;
    @SerializedName("valrep_landimp_comp5_rec_elevation_desc")
    private String valrepLandimpComp5RecElevationDesc;
    @SerializedName("valrep_landimp_comp5_rec_elevation_description")
    private String valrepLandimpComp5RecElevationDescription;
    @SerializedName("valrep_landimp_comp5_rec_location")
    private String valrepLandimpComp5RecLocation;
    @SerializedName("valrep_landimp_comp5_rec_location_desc")
    private String valrepLandimpComp5RecLocationDesc;
    @SerializedName("valrep_landimp_comp5_rec_location_description")
    private String valrepLandimpComp5RecLocationDescription;
    @SerializedName("valrep_landimp_comp5_rec_lot_type")
    private String valrepLandimpComp5RecLotType;
    @SerializedName("valrep_landimp_comp5_rec_lot_type_desc")
    private String valrepLandimpComp5RecLotTypeDesc;
    @SerializedName("valrep_landimp_comp5_rec_lot_type_description")
    private String valrepLandimpComp5RecLotTypeDescription;
    @SerializedName("valrep_landimp_comp5_rec_neighborhood")
    private String valrepLandimpComp5RecNeighborhood;
    @SerializedName("valrep_landimp_comp5_rec_neighborhood_desc")
    private String valrepLandimpComp5RecNeighborhoodDesc;
    @SerializedName("valrep_landimp_comp5_rec_neighborhood_description")
    private String valrepLandimpComp5RecNeighborhoodDescription;
    @SerializedName("valrep_landimp_comp5_rec_neighborhood_type")
    private String valrepLandimpComp5RecNeighborhoodType;
    @SerializedName("valrep_landimp_comp5_rec_others")
    private String valrepLandimpComp5RecOthers;
    @SerializedName("valrep_landimp_comp5_rec_others2")
    private String valrepLandimpComp5RecOthers2;
    @SerializedName("valrep_landimp_comp5_rec_others2_desc")
    private String valrepLandimpComp5RecOthers2Desc;
    @SerializedName("valrep_landimp_comp5_rec_others2_description")
    private String valrepLandimpComp5RecOthers2Description;
    @SerializedName("valrep_landimp_comp5_rec_others3")
    private String valrepLandimpComp5RecOthers3;
    @SerializedName("valrep_landimp_comp5_rec_others3_desc")
    private String valrepLandimpComp5RecOthers3Desc;
    @SerializedName("valrep_landimp_comp5_rec_others3_description")
    private String valrepLandimpComp5RecOthers3Description;
    @SerializedName("valrep_landimp_comp5_rec_others_desc")
    private String valrepLandimpComp5RecOthersDesc;
    @SerializedName("valrep_landimp_comp5_rec_others_description")
    private String valrepLandimpComp5RecOthersDescription;
    @SerializedName("valrep_landimp_comp5_rec_others_name")
    private String valrepLandimpComp5RecOthersName;
    @SerializedName("valrep_landimp_comp5_rec_prop_rights")
    private String valrepLandimpComp5RecPropRights;
    @SerializedName("valrep_landimp_comp5_rec_prop_rights_desc")
    private String valrepLandimpComp5RecPropRightsDesc;
    @SerializedName("valrep_landimp_comp5_rec_property_rights_conveyed")
    private String valrepLandimpComp5RecPropertyRightsConveyed;
    @SerializedName("valrep_landimp_comp5_rec_property_rights_conveyed_adjustment_price")
    private String valrepLandimpComp5RecPropertyRightsConveyedAdjustmentPrice;
    @SerializedName("valrep_landimp_comp5_rec_property_rights_conveyed_desc")
    private String valrepLandimpComp5RecPropertyRightsConveyedDesc;
    @SerializedName("valrep_landimp_comp5_rec_property_rights_conveyed_description")
    private String valrepLandimpComp5RecPropertyRightsConveyedDescription;
    @SerializedName("valrep_landimp_comp5_rec_road")
    private String valrepLandimpComp5RecRoad;
    @SerializedName("valrep_landimp_comp5_rec_road_desc")
    private String valrepLandimpComp5RecRoadDesc;
    @SerializedName("valrep_landimp_comp5_rec_road_description")
    private String valrepLandimpComp5RecRoadDescription;
    @SerializedName("valrep_landimp_comp5_rec_shape")
    private String valrepLandimpComp5RecShape;
    @SerializedName("valrep_landimp_comp5_rec_shape_desc")
    private String valrepLandimpComp5RecShapeDesc;
    @SerializedName("valrep_landimp_comp5_rec_shape_description")
    private String valrepLandimpComp5RecShapeDescription;
    @SerializedName("valrep_landimp_comp5_rec_size")
    private String valrepLandimpComp5RecSize;
    @SerializedName("valrep_landimp_comp5_rec_size_desc")
    private String valrepLandimpComp5RecSizeDesc;
    @SerializedName("valrep_landimp_comp5_rec_size_description")
    private String valrepLandimpComp5RecSizeDescription;
    @SerializedName("valrep_landimp_comp5_rec_terrain")
    private String valrepLandimpComp5RecTerrain;
    @SerializedName("valrep_landimp_comp5_rec_terrain_desc")
    private String valrepLandimpComp5RecTerrainDesc;
    @SerializedName("valrep_landimp_comp5_rec_terrain_description")
    private String valrepLandimpComp5RecTerrainDescription;
    @SerializedName("valrep_landimp_comp5_rec_time")
    private String valrepLandimpComp5RecTime;
    @SerializedName("valrep_landimp_comp5_rec_time_adjustment_price")
    private String valrepLandimpComp5RecTimeAdjustmentPrice;
    @SerializedName("valrep_landimp_comp5_rec_time_desc")
    private String valrepLandimpComp5RecTimeDesc;
    @SerializedName("valrep_landimp_comp5_rec_time_description")
    private String valrepLandimpComp5RecTimeDescription;
    @SerializedName("valrep_landimp_comp5_rec_time_element")
    private String valrepLandimpComp5RecTimeElement;
    @SerializedName("valrep_landimp_comp5_rec_time_element_desc")
    private String valrepLandimpComp5RecTimeElementDesc;
    @SerializedName("valrep_landimp_comp5_rec_tru_lot")
    private String valrepLandimpComp5RecTruLot;
    @SerializedName("valrep_landimp_comp5_remarks")
    private String valrepLandimpComp5Remarks;
    @SerializedName("valrep_landimp_comp5_selling_price")
    private String valrepLandimpComp5SellingPrice;
    @SerializedName("valrep_landimp_comp5_shape")
    private String valrepLandimpComp5Shape;
    @SerializedName("valrep_landimp_comp5_source")
    private String valrepLandimpComp5Source;
    @SerializedName("valrep_landimp_comp5_total_adj")
    private String valrepLandimpComp5TotalAdj;
    @SerializedName("valrep_landimp_comp5_totalgross_adj")
    private String valrepLandimpComp5TotalgrossAdj;
    @SerializedName("valrep_landimp_comp5_totalnet_adj")
    private String valrepLandimpComp5TotalnetAdj;
    @SerializedName("valrep_landimp_comp5_weight")
    private String valrepLandimpComp5Weight;
    @SerializedName("valrep_landimp_comp5_weight_equivalent")
    private String valrepLandimpComp5WeightEquivalent;
    @SerializedName("valrep_landimp_comp5_weighted_value")
    private String valrepLandimpComp5WeightedValue;
    @SerializedName("valrep_landimp_comp5_wgt_dist")
    private String valrepLandimpComp5WgtDist;
    @SerializedName("valrep_landimp_comp5_zoning")
    private String valrepLandimpComp5Zoning;
    @SerializedName("valrep_landimp_comp_rounded_to")
    private String valrepLandimpCompRoundedTo;
    @SerializedName("valrep_landimp_comp_rounded_to_nearest")
    private String valrepLandimpCompRoundedToNearest;
    @SerializedName("valrep_landimp_comp_with_comp_maps")
    private String valrepLandimpCompWithCompMaps;
    @SerializedName("valrep_landimp_date_inspected_day")
    private String valrepLandimpDateInspectedDay;
    @SerializedName("valrep_landimp_date_inspected_month")
    private String valrepLandimpDateInspectedMonth;
    @SerializedName("valrep_landimp_date_inspected_year")
    private String valrepLandimpDateInspectedYear;
    @SerializedName("valrep_landimp_desc_town_class")
    private String valrepLandimpDescTownClass;
    @SerializedName("valrep_landimp_final_value_improvements_appraised_mv")
    private String valrepLandimpFinalValueImprovementsAppraisedMv;
    @SerializedName("valrep_landimp_final_value_land_appraised_mv")
    private String valrepLandimpFinalValueLandAppraisedMv;
    @SerializedName("valrep_landimp_final_value_net_appraised_value")
    private String valrepLandimpFinalValueNetAppraisedValue;
    @SerializedName("valrep_landimp_final_value_net_appraised_value_words")
    private String valrepLandimpFinalValueNetAppraisedValueWords;
    @SerializedName("valrep_landimp_final_value_total_other_land_imp")
    private String valrepLandimpFinalValueTotalOtherLandImp;
    @SerializedName("valrep_landimp_id_bldg_valuation")
    private String valrepLandimpIdBldgValuation;
    @SerializedName("valrep_landimp_id_building_components")
    private String valrepLandimpIdBuildingComponents;
    @SerializedName("valrep_landimp_id_building_inspection")
    private String valrepLandimpIdBuildingInspection;
    @SerializedName("valrep_landimp_id_comparison")
    private String valrepLandimpIdComparison;
    @SerializedName("valrep_landimp_id_cost_estimate")
    private String valrepLandimpIdCostEstimate;
    @SerializedName("valrep_landimp_id_deeds_sale")
    private String valrepLandimpIdDeedsSale;
    @SerializedName("valrep_landimp_id_depreciated_analysis")
    private String valrepLandimpDepreciatedAnalysis;
    @SerializedName("valrep_landimp_id_engineering_tests")
    private String valrepLandimpIdEngineeringTests;
    @SerializedName("valrep_landimp_id_geodetic_plan")
    private String valrepLandimpIdGeodeticPlan;
    @SerializedName("valrep_landimp_id_geodetic_survey")
    private String valrepLandimpIdGeodeticSurvey;
    @SerializedName("valrep_landimp_id_land_valuation")
    private String valrepLandimpIdLandValuation;
    @SerializedName("valrep_landimp_id_local_brokers")
    private String valrepLandimpIdLocalBrokers;
    @SerializedName("valrep_landimp_id_locational_verification")
    private String valrepLandimpIdLocationalVerification;
    @SerializedName("valrep_landimp_id_locational_verification_subd")
    private String valrepLandimpIdLocationalVerificationSubd;
    @SerializedName("valrep_landimp_id_lot_config")
    private String valrepLandimpIdLotConfig;
    @SerializedName("valrep_landimp_id_market_analysis")
    private String valrepLandimpIdMarketAnalysis;
    @SerializedName("valrep_landimp_id_measurement_verification")
    private String valrepLandimpIdMeasurementVerification;
    @SerializedName("valrep_landimp_id_mls")
    private String valrepLandimpIdMls;
    @SerializedName("valrep_landimp_id_natural_hazards")
    private String valrepLandimpIdNaturalHazards;
    @SerializedName("valrep_landimp_id_neighborhood_research")
    private String valrepLandimpIdNeighborhoodResearch;
    @SerializedName("valrep_landimp_id_ocular_inspection")
    private String valrepLandimpIdOcularInspection;
    @SerializedName("valrep_landimp_id_others")
    private String valrepLandimpIdOthers;
    @SerializedName("valrep_landimp_id_plotting")
    private String valrepLandimpIdPlotting;
    @SerializedName("valrep_landimp_id_quantity_survey")
    private String valrepLandimpIdQuantitySurvey;
    @SerializedName("valrep_landimp_id_sampling_verification")
    private String valrepLandimpIdSamplingVerification;
    @SerializedName("valrep_landimp_id_site_assessment")
    private String valrepLandimpIdSiteAssessment;
    @SerializedName("valrep_landimp_id_socio_physical_profile")
    private String valrepLandimpIdSocioPhysicalProfile;
    @SerializedName("valrep_landimp_id_special_laws")
    private String valrepLandimpIdSpecialLaws;
    @SerializedName("valrep_landimp_id_statutory_constraints")
    private String valrepLandimpIdStatutoryConstraints;
    @SerializedName("valrep_landimp_id_statutory_constraints_desc")
    private String valrepLandimpIdStatutoryConstraintsDesc;
    @SerializedName("valrep_landimp_id_subd_map")
    private String valrepLandimpIdSubdMap;
    @SerializedName("valrep_landimp_id_survey_hazardous")
    private String valrepLandimpIdSurveyHazardous;
    @SerializedName("valrep_landimp_id_tax")
    private String valrepLandimpIdTax;
    @SerializedName("valrep_landimp_id_tax_declaration")
    private String valrepLandimpIdTaxDeclaration;
    @SerializedName("valrep_landimp_id_tax_map")
    private String valrepLandimpIdTaxMap;
    @SerializedName("valrep_landimp_id_traffic_count")
    private String valrepLandimpIdTrafficCount;
    @SerializedName("valrep_landimp_id_type_of_interest")
    private String valrepLandimpIdTypeOfInterest;
    @SerializedName("valrep_landimp_id_type_of_interest_desc")
    private String valrepLandimpIdTypeOfInterestDesc;
    @SerializedName("valrep_landimp_id_typical_participants")
    private String valrepLandimpIdTypicalParticipants;
    @SerializedName("valrep_landimp_id_unit_inspection")
    private String valrepLandimpIdUnitInspection;
    @SerializedName("valrep_landimp_id_unit_sampling_verification")
    private String valrepLandimpIdUnitSamplingVerification;
    @SerializedName("valrep_landimp_id_use_analysis")
    private String valrepLandimpIdUseAnalysis;
    @SerializedName("valrep_landimp_id_utilities_services")
    private String valrepLandimpIdUtilitiesServices;
    @SerializedName("valrep_landimp_id_value_research")
    private String valrepLandimpIdValueResearch;
    @SerializedName("valrep_landimp_id_zonal_value")
    private String valrepLandimpIdZonalValue;
    @SerializedName("valrep_landimp_id_zoning_certification")
    private String valrepLandimpIdZoningCertification;
    @SerializedName("valrep_landimp_id_zoning_verification")
    private String valrepLandimpIdZoningVerification;

    @TypeConverters(ValrepLandimpImpDetailTypeConverter.class)
    @SerializedName("valrep_landimp_imp_details")
    private List<ValrepLandimpImpDetail> valrepLandimpImpDetails;

    @SerializedName("valrep_landimp_imp_drainage")
    private String valrepLandimpImpDrainage;
    @SerializedName("valrep_landimp_imp_roads")
    private String valrepLandimpImpRoads;
    @SerializedName("valrep_landimp_imp_sidewalk")
    private String valrepLandimpImpSidewalk;

    @TypeConverters(ValrepLandimpImpValuationTypeConverter.class)
    @SerializedName("valrep_landimp_imp_valuation")
    private List<ValrepLandimpImpValuation> valrepLandimpImpValuation;

    @SerializedName("valrep_landimp_imp_valuation_remarks")
    private String valrepLandimpImpValuationRemarks;
    @SerializedName("valrep_landimp_imp_value_total_imp_value")
    private String valrepLandimpImpValueTotalImpValue;
    @SerializedName("valrep_landimp_imp_value_total_other_imp_value")
    private String valrepLandimpImpValueTotalOtherImpValue;

    @TypeConverters(ValrepLandimpLotDetailTypeConverter.class)
    @SerializedName("valrep_landimp_lot_details")
    private List<ValrepLandimpLotDetail> valrepLandimpLotDetails;

    @TypeConverters(ValrepLandimpLotValuationTypeConverter.class)
    @SerializedName("valrep_landimp_lot_valuation")
    private List<ValrepLandimpLotValuation> valrepLandimpLotValuation;

    @SerializedName("valrep_landimp_lotclass_actual_usage")
    private String valrepLandimpLotclassActualUsage;
    @SerializedName("valrep_landimp_lotclass_highest_best_use")
    private String valrepLandimpLotclassHighestBestUse;
    @SerializedName("valrep_landimp_lotclass_neighborhood")
    private String valrepLandimpLotclassNeighborhood;
    @SerializedName("valrep_landimp_lotclass_per_tax_dec")
    private String valrepLandimpLotclassPerTaxDec;
    @SerializedName("valrep_landimp_lotclass_prop_type")
    private String valrepLandimpLotclassPropType;
    @SerializedName("valrep_landimp_market_prop_market")
    private String valrepLandimpMarketPropMarket;
    @SerializedName("valrep_landimp_mcc_distance_1")
    private String valrepLandimpMccDistance1;
    @SerializedName("valrep_landimp_mcc_distance_2")
    private String valrepLandimpMccDistance2;
    @SerializedName("valrep_landimp_mcc_distance_3")
    private String valrepLandimpMccDistance3;
    @SerializedName("valrep_landimp_mcc_distance_4")
    private String valrepLandimpMccDistance4;
    @SerializedName("valrep_landimp_mcc_distance_5")
    private String valrepLandimpMccDistance5;
    @SerializedName("valrep_landimp_mcc_location_1")
    private String valrepLandimpMccLocation1;
    @SerializedName("valrep_landimp_mcc_location_2")
    private String valrepLandimpMccLocation2;
    @SerializedName("valrep_landimp_mcc_location_3")
    private String valrepLandimpMccLocation3;
    @SerializedName("valrep_landimp_mcc_location_4")
    private String valrepLandimpMccLocation4;
    @SerializedName("valrep_landimp_mcc_location_5")
    private String valrepLandimpMccLocation5;
    @SerializedName("valrep_landimp_mr_distance_1")
    private String valrepLandimpMrDistance1;
    @SerializedName("valrep_landimp_mr_distance_2")
    private String valrepLandimpMrDistance2;
    @SerializedName("valrep_landimp_mr_distance_3")
    private String valrepLandimpMrDistance3;
    @SerializedName("valrep_landimp_mr_distance_4")
    private String valrepLandimpMrDistance4;
    @SerializedName("valrep_landimp_mr_distance_5")
    private String valrepLandimpMrDistance5;
    @SerializedName("valrep_landimp_mr_location_1")
    private String valrepLandimpMrLocation1;
    @SerializedName("valrep_landimp_mr_location_2")
    private String valrepLandimpMrLocation2;
    @SerializedName("valrep_landimp_mr_location_3")
    private String valrepLandimpMrLocation3;
    @SerializedName("valrep_landimp_mr_location_4")
    private String valrepLandimpMrLocation4;
    @SerializedName("valrep_landimp_mr_location_5")
    private String valrepLandimpMrLocation5;

    @TypeConverters(ValrepLandimpOtherLandImpDetailTypeConverter.class)
    @SerializedName("valrep_landimp_other_land_imp_details")
    private List<ValrepLandimpOtherLandImpDetail> valrepLandimpOtherLandImpDetails;

    @TypeConverters(ValrepLandimpOtherLandImpValueTypeConverter.class)
    @SerializedName("valrep_landimp_other_land_imp_value")
    private List<ValrepLandimpOtherLandImpValue> valrepLandimpOtherLandImpValue;

    @SerializedName("valrep_landimp_other_prop_comments")
    private String valrepLandimpOtherPropComments;

    @TypeConverters(ValrepLandimpParkingValuationDetailTypeConverter.class)
    @SerializedName("valrep_landimp_parking_valuation_details")
    private List<ValrepLandimpParkingValuationDetail> valrepLandimpParkingValuationDetails;

    @SerializedName("valrep_landimp_physical_depth")
    private String valrepLandimpPhysicalDepth;
    @SerializedName("valrep_landimp_physical_elevation")
    private String valrepLandimpPhysicalElevation;
    @SerializedName("valrep_landimp_physical_frontage")
    private String valrepLandimpPhysicalFrontage;
    @SerializedName("valrep_landimp_physical_lot_type")
    private String valrepLandimpPhysicalLotType;
    @SerializedName("valrep_landimp_physical_pub_trans_desc")
    private String valrepLandimpPhysicalPubTransDesc;
    @SerializedName("valrep_landimp_physical_road")
    private String valrepLandimpPhysicalRoad;
    @SerializedName("valrep_landimp_physical_shape")
    private String valrepLandimpPhysicalShape;
    @SerializedName("valrep_landimp_physical_surfacing")
    private String valrepLandimpPhysicalSurfacing;
    @SerializedName("valrep_landimp_physical_terrain")
    private String valrepLandimpPhysicalTerrain;
    @SerializedName("valrep_landimp_special_assumptions_check")
    private String valrepLandimpSpecialAssumptionsCheck;
    @SerializedName("valrep_landimp_supplyanddemand_1")
    private String valrepLandimpSupplyanddemand1;
    @SerializedName("valrep_landimp_supplyanddemand_10")
    private String valrepLandimpSupplyanddemand10;
    @SerializedName("valrep_landimp_supplyanddemand_2")
    private String valrepLandimpSupplyanddemand2;
    @SerializedName("valrep_landimp_supplyanddemand_3")
    private String valrepLandimpSupplyanddemand3;
    @SerializedName("valrep_landimp_supplyanddemand_4")
    private String valrepLandimpSupplyanddemand4;
    @SerializedName("valrep_landimp_supplyanddemand_5")
    private String valrepLandimpSupplyanddemand5;
    @SerializedName("valrep_landimp_supplyanddemand_6")
    private String valrepLandimpSupplyanddemand6;
    @SerializedName("valrep_landimp_supplyanddemand_7")
    private String valrepLandimpSupplyanddemand7;
    @SerializedName("valrep_landimp_supplyanddemand_8")
    private String valrepLandimpSupplyanddemand8;
    @SerializedName("valrep_landimp_supplyanddemand_9")
    private String valrepLandimpSupplyanddemand9;
    @SerializedName("valrep_landimp_tax_dec_town_type")
    private String valrepLandimpTaxDecTownType;
    @SerializedName("valrep_landimp_util_electricity")
    private String valrepLandimpUtilElectricity;
    @SerializedName("valrep_landimp_util_telephone")
    private String valrepLandimpUtilTelephone;
    @SerializedName("valrep_landimp_util_water")
    private String valrepLandimpUtilWater;
    @SerializedName("valrep_landimp_valuation_date_month")
    private String valrepLandimpValuationDateMonth;
    @SerializedName("valrep_landimp_value_total_area")
    private String valrepLandimpValueTotalArea;
    @SerializedName("valrep_landimp_value_total_deduction")
    private String valrepLandimpValueTotalDeduction;
    @SerializedName("valrep_landimp_value_total_landimp_value")
    private String valrepLandimpValueTotalLandimpValue;
    @SerializedName("valrep_landimp_value_total_net_area")
    private String valrepLandimpValueTotalNetArea;
    @SerializedName("valrep_landimp_weighted_unit_value")
    private String valrepLandimpWeightedUnitValue;
    @SerializedName("valrep_landimp_zonal_value")
    private String valrepLandimpZonalValue;

    @TypeConverters(ValrepLcIvsQualifyingStatementTypeConverter.class)
    @SerializedName("valrep_lc_ivs_qualifying_statements")
    private List<ValrepLcIvsQualifyingStatement> valrepLcIvsQualifyingStatements;

    @SerializedName("valrep_lc_ivs_qualifying_statements_check")
    private String valrepLcIvsQualifyingStatementsCheck;
    @SerializedName("valrep_marketphase_1")
    private String valrepMarketphase1;
    @SerializedName("valrep_marketphase_2")
    private String valrepMarketphase2;
    @SerializedName("valrep_marketphase_3")
    private String valrepMarketphase3;
    @SerializedName("valrep_marketphase_4")
    private String valrepMarketphase4;
    @SerializedName("valrep_marketphase_5")
    private String valrepMarketphase5;
    @SerializedName("valrep_supplyanddemand_1")
    private String valrepSupplyanddemand1;
    @SerializedName("valrep_supplyanddemand_2")
    private String valrepSupplyanddemand2;
    @SerializedName("valrep_supplyanddemand_3")
    private String valrepSupplyanddemand3;
    @SerializedName("valrep_supplyanddemand_4")
    private String valrepSupplyanddemand4;
    @SerializedName("valrep_supplyanddemand_5")
    private String valrepSupplyanddemand5;
    @SerializedName("valrep_supplyanddemand_6")
    private String valrepSupplyanddemand6;
    @SerializedName("valrep_supplyanddemand_7")
    private String valrepSupplyanddemand7;
    @SerializedName("valrep_supplyanddemand_8")
    private String valrepSupplyanddemand8;
    @SerializedName("valrep_supplyanddemand_9")
    private String valrepSupplyanddemand9;
    @SerializedName("valrep_typicalbuyer_1")
    private String valrepTypicalbuyer1;

    public long getUniqueId() {
        return uniqueId;
    }

    public void setUniqueId(long uniqueId) {
        this.uniqueId = uniqueId;
    }

    public String getRecordId() {
        return recordId;
    }

    public void setRecordId(String recordId) {
        this.recordId = recordId;
    }

    public String getAppraiserUsername() {
        return appraiserUsername;
    }

    public void setAppraiserUsername(String appraiserUsername) {
        this.appraiserUsername = appraiserUsername;
    }

    public String getAppraisalType() {
        return appraisalType;
    }

    public void setAppraisalType(String appraisalType) {
        this.appraisalType = appraisalType;
    }

    public String getAppAccountAuthroizingOfficer() {
        return NULL_STRING_SETTER(appAccountAuthroizingOfficer);
    }

    public void setAppAccountAuthroizingOfficer(String appAccountAuthroizingOfficer) {
        this.appAccountAuthroizingOfficer = appAccountAuthroizingOfficer;
    }

    public String getAppAccountBusinessAddress() {
        return NULL_STRING_SETTER(appAccountBusinessAddress);
    }

    public void setAppAccountBusinessAddress(String appAccountBusinessAddress) {
        this.appAccountBusinessAddress = appAccountBusinessAddress;
    }

    public String getAppAccountBusinessContact() {
        return NULL_STRING_SETTER(appAccountBusinessContact);
    }

    public void setAppAccountBusinessContact(String appAccountBusinessContact) {
        this.appAccountBusinessContact = appAccountBusinessContact;
    }

    public String getAppAccountBusinessEmail() {
        return NULL_STRING_SETTER(appAccountBusinessEmail);
    }

    public void setAppAccountBusinessEmail(String appAccountBusinessEmail) {
        this.appAccountBusinessEmail = appAccountBusinessEmail;
    }

    public String getAppAccountCompany() {
        return NULL_STRING_SETTER(appAccountCompany);
    }

    public void setAppAccountCompany(String appAccountCompany) {
        this.appAccountCompany = appAccountCompany;
    }

    public String getAppAccountDesignation() {
        return NULL_STRING_SETTER(appAccountDesignation);
    }

    public void setAppAccountDesignation(String appAccountDesignation) {
        this.appAccountDesignation = appAccountDesignation;
    }

    public String getAppAccountFirstName() {
        return NULL_STRING_SETTER(appAccountFirstName);
    }

    public void setAppAccountFirstName(String appAccountFirstName) {
        this.appAccountFirstName = appAccountFirstName;
    }

    public String getAppAccountLastName() {
        return NULL_STRING_SETTER(appAccountLastName);
    }

    public void setAppAccountLastName(String appAccountLastName) {
        this.appAccountLastName = appAccountLastName;
    }

    public String getAppAccountMiddleName() {
        return NULL_STRING_SETTER(appAccountMiddleName);
    }

    public void setAppAccountMiddleName(String appAccountMiddleName) {
        this.appAccountMiddleName = appAccountMiddleName;
    }

    public String getAppAccountNameTitle() {
        return NULL_STRING_SETTER(appAccountNameTitle);
    }

    public void setAppAccountNameTitle(String appAccountNameTitle) {
        this.appAccountNameTitle = appAccountNameTitle;
    }

    public String getAppAppraisalManager() {
        return NULL_STRING_SETTER(appAppraisalManager);
    }

    public void setAppAppraisalManager(String appAppraisalManager) {
        this.appAppraisalManager = appAppraisalManager;
    }

    public String getAppAppraiserCode() {
        return NULL_STRING_SETTER(appAppraiserCode);
    }

    public void setAppAppraiserCode(String appAppraiserCode) {
        this.appAppraiserCode = appAppraiserCode;
    }

    public String getAppAppraiserType() {
        return NULL_STRING_SETTER(appAppraiserType);
    }

    public void setAppAppraiserType(String appAppraiserType) {
        this.appAppraiserType = appAppraiserType;
    }

    public String getAppBorrowerName() {
        return NULL_STRING_SETTER(appBorrowerName);
    }

    public void setAppBorrowerName(String appBorrowerName) {
        this.appBorrowerName = appBorrowerName;
    }

    public String getAppBorrowerType() {
        return NULL_STRING_SETTER(appBorrowerType);
    }

    public void setAppBorrowerType(String appBorrowerType) {
        this.appBorrowerType = appBorrowerType;
    }

    public String getAppContactNo() {
        return NULL_STRING_SETTER(appContactNo);
    }

    public void setAppContactNo(String appContactNo) {
        this.appContactNo = appContactNo;
    }

    public String getAppCrossReferenceFirstName() {
        return NULL_STRING_SETTER(appCrossReferenceFirstName);
    }

    public void setAppCrossReferenceFirstName(String appCrossReferenceFirstName) {
        this.appCrossReferenceFirstName = appCrossReferenceFirstName;
    }

    public String getAppCrossReferenceLastName() {
        return NULL_STRING_SETTER(appCrossReferenceLastName);
    }

    public void setAppCrossReferenceLastName(String appCrossReferenceLastName) {
        this.appCrossReferenceLastName = appCrossReferenceLastName;
    }

    public String getAppCrossReferenceMiddleName() {
        return NULL_STRING_SETTER(appCrossReferenceMiddleName);
    }

    public void setAppCrossReferenceMiddleName(String appCrossReferenceMiddleName) {
        this.appCrossReferenceMiddleName = appCrossReferenceMiddleName;
    }

    public String getAppCrossReferenceRelation() {
        return NULL_STRING_SETTER(appCrossReferenceRelation);
    }

    public void setAppCrossReferenceRelation(String appCrossReferenceRelation) {
        this.appCrossReferenceRelation = appCrossReferenceRelation;
    }

    public String getAppDateContractDay() {
        return NULL_STRING_SETTER(appDateContractDay);
    }

    public void setAppDateContractDay(String appDateContractDay) {
        this.appDateContractDay = appDateContractDay;
    }

    public String getAppDateContractMonth() {
        return NULL_STRING_SETTER(appDateContractMonth);
    }

    public void setAppDateContractMonth(String appDateContractMonth) {
        this.appDateContractMonth = appDateContractMonth;
    }

    public String getAppDateContractYear() {
        return NULL_STRING_SETTER(appDateContractYear);
    }

    public void setAppDateContractYear(String appDateContractYear) {
        this.appDateContractYear = appDateContractYear;
    }

    public String getAppDateToday() {
        return NULL_STRING_SETTER(appDateToday);
    }

    public void setAppDateToday(String appDateToday) {
        this.appDateToday = appDateToday;
    }

    public String getAppDaterequestedDay() {
        return NULL_STRING_SETTER(appDaterequestedDay);
    }

    public void setAppDaterequestedDay(String appDaterequestedDay) {
        this.appDaterequestedDay = appDaterequestedDay;
    }

    public String getAppDaterequestedMonth() {
        return NULL_STRING_SETTER(appDaterequestedMonth);
    }

    public void setAppDaterequestedMonth(String appDaterequestedMonth) {
        this.appDaterequestedMonth = appDaterequestedMonth;
    }

    public String getAppDaterequestedYear() {
        return NULL_STRING_SETTER(appDaterequestedYear);
    }

    public void setAppDaterequestedYear(String appDaterequestedYear) {
        this.appDaterequestedYear = appDaterequestedYear;
    }

    public String getAppDeclinedReason() {
        return NULL_STRING_SETTER(appDeclinedReason);
    }

    public void setAppDeclinedReason(String appDeclinedReason) {
        this.appDeclinedReason = appDeclinedReason;
    }

    public String getAppDesiredLoanAmount() {
        return NULL_STRING_SETTER(appDesiredLoanAmount);
    }

    public void setAppDesiredLoanAmount(String appDesiredLoanAmount) {
        this.appDesiredLoanAmount = appDesiredLoanAmount;
    }

    public String getAppIntendedUsersAddress() {
        return NULL_STRING_SETTER(appIntendedUsersAddress);
    }

    public void setAppIntendedUsersAddress(String appIntendedUsersAddress) {
        this.appIntendedUsersAddress = appIntendedUsersAddress;
    }

    public String getAppIntendedUsersAddressType() {
        return NULL_STRING_SETTER(appIntendedUsersAddressType);
    }

    public void setAppIntendedUsersAddressType(String appIntendedUsersAddressType) {
        this.appIntendedUsersAddressType = appIntendedUsersAddressType;
    }

    public String getAppIntendedUsersName() {
        return NULL_STRING_SETTER(appIntendedUsersName);
    }

    public void setAppIntendedUsersName(String appIntendedUsersName) {
        this.appIntendedUsersName = appIntendedUsersName;
    }

    public String getAppIntendedUsersOthers() {
        return NULL_STRING_SETTER(appIntendedUsersOthers);
    }

    public void setAppIntendedUsersOthers(String appIntendedUsersOthers) {
        this.appIntendedUsersOthers = appIntendedUsersOthers;
    }

    public String getAppJobAcceptanceRemarks() {
        return NULL_STRING_SETTER(appJobAcceptanceRemarks);
    }

    public void setAppJobAcceptanceRemarks(String appJobAcceptanceRemarks) {
        this.appJobAcceptanceRemarks = appJobAcceptanceRemarks;
    }

    public String getAppManagerUsername() {
        return NULL_STRING_SETTER(appManagerUsername);
    }

    public void setAppManagerUsername(String appManagerUsername) {
        this.appManagerUsername = appManagerUsername;
    }

    public String getAppRequestorUsername() {
        return NULL_STRING_SETTER(appRequestorUsername);
    }

    public void setAppRequestorUsername(String appRequestorUsername) {
        this.appRequestorUsername = appRequestorUsername;
    }

    public String getAppSimulType() {
        return NULL_STRING_SETTER(appSimulType);
    }

    public void setAppSimulType(String appSimulType) {
        this.appSimulType = appSimulType;
    }

    public String getAppTypeOfLoan() {
        return NULL_STRING_SETTER(appTypeOfLoan);
    }

    public void setAppTypeOfLoan(String appTypeOfLoan) {
        this.appTypeOfLoan = appTypeOfLoan;
    }

    public String getAppValuerContactNumber() {
        return NULL_STRING_SETTER(appValuerContactNumber);
    }

    public void setAppValuerContactNumber(String appValuerContactNumber) {
        this.appValuerContactNumber = appValuerContactNumber;
    }

    public String getAppValuerEmailAddress() {
        return NULL_STRING_SETTER(appValuerEmailAddress);
    }

    public void setAppValuerEmailAddress(String appValuerEmailAddress) {
        this.appValuerEmailAddress = appValuerEmailAddress;
    }

    public String getAppValuerName() {
        return NULL_STRING_SETTER(appValuerName);
    }

    public void setAppValuerName(String appValuerName) {
        this.appValuerName = appValuerName;
    }

    public String getAppValuerOfficerType() {
        return NULL_STRING_SETTER(appValuerOfficerType);
    }

    public void setAppValuerOfficerType(String appValuerOfficerType) {
        this.appValuerOfficerType = appValuerOfficerType;
    }

    public String getApplicationSource() {
        return NULL_STRING_SETTER(applicationSource);
    }

    public void setApplicationSource(String applicationSource) {
        this.applicationSource = applicationSource;
    }

    public String getApplicationStatus() {
        return NULL_STRING_SETTER(applicationStatus);
    }

    public void setApplicationStatus(String applicationStatus) {
        this.applicationStatus = applicationStatus;
    }

    public List<AppraisalRequest> getAppraisalRequest() {
        return NULL_LIST_SETTER(appraisalRequest);
    }

    public void setAppraisalRequest(List<AppraisalRequest> appraisalRequest) {
        this.appraisalRequest = appraisalRequest;
    }

    public String getAssetNeighborhoodAutomotiveCenters() {
        return NULL_STRING_SETTER(assetNeighborhoodAutomotiveCenters);
    }

    public void setAssetNeighborhoodAutomotiveCenters(String assetNeighborhoodAutomotiveCenters) {
        this.assetNeighborhoodAutomotiveCenters = assetNeighborhoodAutomotiveCenters;
    }

    public String getAssetNeighborhoodBanks() {
        return NULL_STRING_SETTER(assetNeighborhoodBanks);
    }

    public void setAssetNeighborhoodBanks(String assetNeighborhoodBanks) {
        this.assetNeighborhoodBanks = assetNeighborhoodBanks;
    }

    public String getAssetNeighborhoodEstablishments() {
        return NULL_STRING_SETTER(assetNeighborhoodEstablishments);
    }

    public void setAssetNeighborhoodEstablishments(String assetNeighborhoodEstablishments) {
        this.assetNeighborhoodEstablishments = assetNeighborhoodEstablishments;
    }

    public String getAssetNeighborhoodGovernmentFacilities() {
        return NULL_STRING_SETTER(assetNeighborhoodGovernmentFacilities);
    }

    public void setAssetNeighborhoodGovernmentFacilities(String assetNeighborhoodGovernmentFacilities) {
        this.assetNeighborhoodGovernmentFacilities = assetNeighborhoodGovernmentFacilities;
    }

    public String getAssetNeighborhoodHospitals() {
        return NULL_STRING_SETTER(assetNeighborhoodHospitals);
    }

    public void setAssetNeighborhoodHospitals(String assetNeighborhoodHospitals) {
        this.assetNeighborhoodHospitals = assetNeighborhoodHospitals;
    }

    public String getAssetNeighborhoodHotels() {
        return NULL_STRING_SETTER(assetNeighborhoodHotels);
    }

    public void setAssetNeighborhoodHotels(String assetNeighborhoodHotels) {
        this.assetNeighborhoodHotels = assetNeighborhoodHotels;
    }

    public String getAssetNeighborhoodIndustrialIndicators() {
        return NULL_STRING_SETTER(assetNeighborhoodIndustrialIndicators);
    }

    public void setAssetNeighborhoodIndustrialIndicators(String assetNeighborhoodIndustrialIndicators) {
        this.assetNeighborhoodIndustrialIndicators = assetNeighborhoodIndustrialIndicators;
    }

    public String getAssetNeighborhoodSchools() {
        return NULL_STRING_SETTER(assetNeighborhoodSchools);
    }

    public void setAssetNeighborhoodSchools(String assetNeighborhoodSchools) {
        this.assetNeighborhoodSchools = assetNeighborhoodSchools;
    }

    public String getAssetNeighborhoodShoppingMalls() {
        return NULL_STRING_SETTER(assetNeighborhoodShoppingMalls);
    }

    public void setAssetNeighborhoodShoppingMalls(String assetNeighborhoodShoppingMalls) {
        this.assetNeighborhoodShoppingMalls = assetNeighborhoodShoppingMalls;
    }

    public String getAssetNeighborhoodSubdivisions() {
        return NULL_STRING_SETTER(assetNeighborhoodSubdivisions);
    }

    public void setAssetNeighborhoodSubdivisions(String assetNeighborhoodSubdivisions) {
        this.assetNeighborhoodSubdivisions = assetNeighborhoodSubdivisions;
    }

    public String getAssetNeighborhoodWorship() {
        return NULL_STRING_SETTER(assetNeighborhoodWorship);
    }

    public void setAssetNeighborhoodWorship(String assetNeighborhoodWorship) {
        this.assetNeighborhoodWorship = assetNeighborhoodWorship;
    }

    public String getAssetTechnicalDesc() {
        return NULL_STRING_SETTER(assetTechnicalDesc);
    }

    public void setAssetTechnicalDesc(String assetTechnicalDesc) {
        this.assetTechnicalDesc = assetTechnicalDesc;
    }

    public String getBovSourceInfoBuyerSeller() {
        return NULL_STRING_SETTER(bovSourceInfoBuyerSeller);
    }

    public void setBovSourceInfoBuyerSeller(String bovSourceInfoBuyerSeller) {
        this.bovSourceInfoBuyerSeller = bovSourceInfoBuyerSeller;
    }

    public String getBovSourceInfoBuyerSellerbroker() {
        return NULL_STRING_SETTER(bovSourceInfoBuyerSellerbroker);
    }

    public void setBovSourceInfoBuyerSellerbroker(String bovSourceInfoBuyerSellerbroker) {
        this.bovSourceInfoBuyerSellerbroker = bovSourceInfoBuyerSellerbroker;
    }

    public String getBovSourceInfoContractor() {
        return NULL_STRING_SETTER(bovSourceInfoContractor);
    }

    public void setBovSourceInfoContractor(String bovSourceInfoContractor) {
        this.bovSourceInfoContractor = bovSourceInfoContractor;
    }

    public String getBovSourceInfoGovernment() {
        return NULL_STRING_SETTER(bovSourceInfoGovernment);
    }

    public void setBovSourceInfoGovernment(String bovSourceInfoGovernment) {
        this.bovSourceInfoGovernment = bovSourceInfoGovernment;
    }

    public String getBovSourceInfoHoaOfficer() {
        return NULL_STRING_SETTER(bovSourceInfoHoaOfficer);
    }

    public void setBovSourceInfoHoaOfficer(String bovSourceInfoHoaOfficer) {
        this.bovSourceInfoHoaOfficer = bovSourceInfoHoaOfficer;
    }

    public String getBovSourceInfoLguOfficial() {
        return NULL_STRING_SETTER(bovSourceInfoLguOfficial);
    }

    public void setBovSourceInfoLguOfficial(String bovSourceInfoLguOfficial) {
        this.bovSourceInfoLguOfficial = bovSourceInfoLguOfficial;
    }

    public String getBovSourceInfoOther() {
        return NULL_STRING_SETTER(bovSourceInfoOther);
    }

    public void setBovSourceInfoOther(String bovSourceInfoOther) {
        this.bovSourceInfoOther = bovSourceInfoOther;
    }

    public String getBovSourceInfoPrivateWebsite() {
        return NULL_STRING_SETTER(bovSourceInfoPrivateWebsite);
    }

    public void setBovSourceInfoPrivateWebsite(String bovSourceInfoPrivateWebsite) {
        this.bovSourceInfoPrivateWebsite = bovSourceInfoPrivateWebsite;
    }

    public String getBovSourceInfoPropertyManager() {
        return NULL_STRING_SETTER(bovSourceInfoPropertyManager);
    }

    public void setBovSourceInfoPropertyManager(String bovSourceInfoPropertyManager) {
        this.bovSourceInfoPropertyManager = bovSourceInfoPropertyManager;
    }

    public String getBovSourceInfoPublication() {
        return NULL_STRING_SETTER(bovSourceInfoPublication);
    }

    public void setBovSourceInfoPublication(String bovSourceInfoPublication) {
        this.bovSourceInfoPublication = bovSourceInfoPublication;
    }

    public String getBovSourceInfoSimulation() {
        return NULL_STRING_SETTER(bovSourceInfoSimulation);
    }

    public void setBovSourceInfoSimulation(String bovSourceInfoSimulation) {
        this.bovSourceInfoSimulation = bovSourceInfoSimulation;
    }

    public String getBovSourceInfoUnaffiliatedBroker() {
        return NULL_STRING_SETTER(bovSourceInfoUnaffiliatedBroker);
    }

    public void setBovSourceInfoUnaffiliatedBroker(String bovSourceInfoUnaffiliatedBroker) {
        this.bovSourceInfoUnaffiliatedBroker = bovSourceInfoUnaffiliatedBroker;
    }

    public String getLcIvsComplied() {
        return NULL_STRING_SETTER(lcIvsComplied);
    }

    public void setLcIvsComplied(String lcIvsComplied) {
        this.lcIvsComplied = lcIvsComplied;
    }

    public String getLcIvsCompliedSummary() {
        return NULL_STRING_SETTER(lcIvsCompliedSummary);
    }

    public void setLcIvsCompliedSummary(String lcIvsCompliedSummary) {
        this.lcIvsCompliedSummary = lcIvsCompliedSummary;
    }

    public String getLcIvsQualifyingStatementsDesc() {
        return NULL_STRING_SETTER(lcIvsQualifyingStatementsDesc);
    }

    public void setLcIvsQualifyingStatementsDesc(String lcIvsQualifyingStatementsDesc) {
        this.lcIvsQualifyingStatementsDesc = lcIvsQualifyingStatementsDesc;
    }

    public String getLcIvsReason1() {
        return NULL_STRING_SETTER(lcIvsReason1);
    }

    public void setLcIvsReason1(String lcIvsReason1) {
        this.lcIvsReason1 = lcIvsReason1;
    }

    public String getLcIvsReason2() {
        return NULL_STRING_SETTER(lcIvsReason2);
    }

    public void setLcIvsReason2(String lcIvsReason2) {
        this.lcIvsReason2 = lcIvsReason2;
    }

    public String getLcIvsReason3() {
        return NULL_STRING_SETTER(lcIvsReason3);
    }

    public void setLcIvsReason3(String lcIvsReason3) {
        this.lcIvsReason3 = lcIvsReason3;
    }

    public String getLcIvsReason4() {
        return NULL_STRING_SETTER(lcIvsReason4);
    }

    public void setLcIvsReason4(String lcIvsReason4) {
        this.lcIvsReason4 = lcIvsReason4;
    }

    public String getLcIvsReason5() {
        return NULL_STRING_SETTER(lcIvsReason5);
    }

    public void setLcIvsReason5(String lcIvsReason5) {
        this.lcIvsReason5 = lcIvsReason5;
    }

    public String getLcIvsReasonOthers() {
        return NULL_STRING_SETTER(lcIvsReasonOthers);
    }

    public void setLcIvsReasonOthers(String lcIvsReasonOthers) {
        this.lcIvsReasonOthers = lcIvsReasonOthers;
    }

    public String getLcValuationApproach() {
        return NULL_STRING_SETTER(lcValuationApproach);
    }

    public void setLcValuationApproach(String lcValuationApproach) {
        this.lcValuationApproach = lcValuationApproach;
    }

    public String getLcValuationApproachReason() {
        return NULL_STRING_SETTER(lcValuationApproachReason);
    }

    public void setLcValuationApproachReason(String lcValuationApproachReason) {
        this.lcValuationApproachReason = lcValuationApproachReason;
    }

    public String getLcValuationKeyInput() {
        return NULL_STRING_SETTER(lcValuationKeyInput);
    }

    public void setLcValuationKeyInput(String lcValuationKeyInput) {
        this.lcValuationKeyInput = lcValuationKeyInput;
    }

    public String getLvIvsQualifyingStatements1() {
        return NULL_STRING_SETTER(lvIvsQualifyingStatements1);
    }

    public void setLvIvsQualifyingStatements1(String lvIvsQualifyingStatements1) {
        this.lvIvsQualifyingStatements1 = lvIvsQualifyingStatements1;
    }

    public String getLvIvsQualifyingStatements2() {
        return NULL_STRING_SETTER(lvIvsQualifyingStatements2);
    }

    public void setLvIvsQualifyingStatements2(String lvIvsQualifyingStatements2) {
        this.lvIvsQualifyingStatements2 = lvIvsQualifyingStatements2;
    }

    public String getLvIvsQualifyingStatements3() {
        return NULL_STRING_SETTER(lvIvsQualifyingStatements3);
    }

    public void setLvIvsQualifyingStatements3(String lvIvsQualifyingStatements3) {
        this.lvIvsQualifyingStatements3 = lvIvsQualifyingStatements3;
    }

    public String getLvIvsQualifyingStatements4() {
        return NULL_STRING_SETTER(lvIvsQualifyingStatements4);
    }

    public void setLvIvsQualifyingStatements4(String lvIvsQualifyingStatements4) {
        this.lvIvsQualifyingStatements4 = lvIvsQualifyingStatements4;
    }

    public String getLvIvsQualifyingStatements5() {
        return NULL_STRING_SETTER(lvIvsQualifyingStatements5);
    }

    public void setLvIvsQualifyingStatements5(String lvIvsQualifyingStatements5) {
        this.lvIvsQualifyingStatements5 = lvIvsQualifyingStatements5;
    }

    public String getLvIvsQualifyingStatements6() {
        return NULL_STRING_SETTER(lvIvsQualifyingStatements6);
    }

    public void setLvIvsQualifyingStatements6(String lvIvsQualifyingStatements6) {
        this.lvIvsQualifyingStatements6 = lvIvsQualifyingStatements6;
    }

    public String getPafLandimpDeveloperName() {
        return NULL_STRING_SETTER(pafLandimpDeveloperName);
    }

    public void setPafLandimpDeveloperName(String pafLandimpDeveloperName) {
        this.pafLandimpDeveloperName = pafLandimpDeveloperName;
    }

    public JobSystem getJobSystem() {
        return jobSystem;
    }

    public void setJobSystem(JobSystem jobSystem) {
        this.jobSystem = jobSystem;
    }

    public String getValrepAppraisalBasisOfValue() {
        return NULL_STRING_SETTER(valrepAppraisalBasisOfValue);
    }

    public void setValrepAppraisalBasisOfValue(String valrepAppraisalBasisOfValue) {
        this.valrepAppraisalBasisOfValue = valrepAppraisalBasisOfValue;
    }

    public String getValrepLandImpComp1CompType() {
        return NULL_STRING_SETTER(valrepLandImpComp1CompType);
    }

    public void setValrepLandImpComp1CompType(String valrepLandImpComp1CompType) {
        this.valrepLandImpComp1CompType = valrepLandImpComp1CompType;
    }

    public String getValrepLandImpComp2CompType() {
        return NULL_STRING_SETTER(valrepLandImpComp2CompType);
    }

    public void setValrepLandImpComp2CompType(String valrepLandImpComp2CompType) {
        this.valrepLandImpComp2CompType = valrepLandImpComp2CompType;
    }

    public String getValrepLandImpComp3CompType() {
        return NULL_STRING_SETTER(valrepLandImpComp3CompType);
    }

    public void setValrepLandImpComp3CompType(String valrepLandImpComp3CompType) {
        this.valrepLandImpComp3CompType = valrepLandImpComp3CompType;
    }

    public String getValrepLandImpComp4CompType() {
        return NULL_STRING_SETTER(valrepLandImpComp4CompType);
    }

    public void setValrepLandImpComp4CompType(String valrepLandImpComp4CompType) {
        this.valrepLandImpComp4CompType = valrepLandImpComp4CompType;
    }

    public String getValrepLandImpComp5CompType() {
        return NULL_STRING_SETTER(valrepLandImpComp5CompType);
    }

    public void setValrepLandImpComp5CompType(String valrepLandImpComp5CompType) {
        this.valrepLandImpComp5CompType = valrepLandImpComp5CompType;
    }

    public String getValrepLandimpAssumptions1() {
        return NULL_STRING_SETTER(valrepLandimpAssumptions1);
    }

    public void setValrepLandimpAssumptions1(String valrepLandimpAssumptions1) {
        this.valrepLandimpAssumptions1 = valrepLandimpAssumptions1;
    }

    public String getValrepLandimpAssumptions2() {
        return NULL_STRING_SETTER(valrepLandimpAssumptions2);
    }

    public void setValrepLandimpAssumptions2(String valrepLandimpAssumptions2) {
        this.valrepLandimpAssumptions2 = valrepLandimpAssumptions2;
    }

    public String getValrepLandimpAssumptions3() {
        return NULL_STRING_SETTER(valrepLandimpAssumptions3);
    }

    public void setValrepLandimpAssumptions3(String valrepLandimpAssumptions3) {
        this.valrepLandimpAssumptions3 = valrepLandimpAssumptions3;
    }

    public String getValrepLandimpAssumptions4() {
        return NULL_STRING_SETTER(valrepLandimpAssumptions4);
    }

    public void setValrepLandimpAssumptions4(String valrepLandimpAssumptions4) {
        this.valrepLandimpAssumptions4 = valrepLandimpAssumptions4;
    }

    public String getValrepLandimpAssumptions5() {
        return NULL_STRING_SETTER(valrepLandimpAssumptions5);
    }

    public void setValrepLandimpAssumptions5(String valrepLandimpAssumptions5) {
        this.valrepLandimpAssumptions5 = valrepLandimpAssumptions5;
    }

    public List<ValrepLandimpAssumptionsRemark> getValrepLandimpAssumptionsRemarks() {
        return NULL_LIST_SETTER(valrepLandimpAssumptionsRemarks);
    }

    public void setValrepLandimpAssumptionsRemarks(List<ValrepLandimpAssumptionsRemark> valrepLandimpAssumptionsRemarks) {
        this.valrepLandimpAssumptionsRemarks = valrepLandimpAssumptionsRemarks;
    }

    public String getValrepLandimpBasicDescription() {
        return NULL_STRING_SETTER(valrepLandimpBasicDescription);
    }

    public void setValrepLandimpBasicDescription(String valrepLandimpBasicDescription) {
        this.valrepLandimpBasicDescription = valrepLandimpBasicDescription;
    }

    public String getValrepLandimpBound1Orientation() {
        return NULL_STRING_SETTER(valrepLandimpBound1Orientation);
    }

    public void setValrepLandimpBound1Orientation(String valrepLandimpBound1Orientation) {
        this.valrepLandimpBound1Orientation = valrepLandimpBound1Orientation;
    }

    public String getValrepLandimpBound2Orientation() {
        return NULL_STRING_SETTER(valrepLandimpBound2Orientation);
    }

    public void setValrepLandimpBound2Orientation(String valrepLandimpBound2Orientation) {
        this.valrepLandimpBound2Orientation = valrepLandimpBound2Orientation;
    }

    public String getValrepLandimpBound3Orientation() {
        return NULL_STRING_SETTER(valrepLandimpBound3Orientation);
    }

    public void setValrepLandimpBound3Orientation(String valrepLandimpBound3Orientation) {
        this.valrepLandimpBound3Orientation = valrepLandimpBound3Orientation;
    }

    public String getValrepLandimpBound4Orientation() {
        return NULL_STRING_SETTER(valrepLandimpBound4Orientation);
    }

    public void setValrepLandimpBound4Orientation(String valrepLandimpBound4Orientation) {
        this.valrepLandimpBound4Orientation = valrepLandimpBound4Orientation;
    }

    public String getValrepLandimpComp1AdjustablePriceSqm() {
        return NULL_STRING_SETTER(valrepLandimpComp1AdjustablePriceSqm);
    }

    public void setValrepLandimpComp1AdjustablePriceSqm(String valrepLandimpComp1AdjustablePriceSqm) {
        this.valrepLandimpComp1AdjustablePriceSqm = valrepLandimpComp1AdjustablePriceSqm;
    }

    public String getValrepLandimpComp1AdjustedValue() {
        return NULL_STRING_SETTER(valrepLandimpComp1AdjustedValue);
    }

    public void setValrepLandimpComp1AdjustedValue(String valrepLandimpComp1AdjustedValue) {
        this.valrepLandimpComp1AdjustedValue = valrepLandimpComp1AdjustedValue;
    }

    public String getValrepLandimpComp1ApproxAge() {
        return NULL_STRING_SETTER(valrepLandimpComp1ApproxAge);
    }

    public void setValrepLandimpComp1ApproxAge(String valrepLandimpComp1ApproxAge) {
        this.valrepLandimpComp1ApproxAge = valrepLandimpComp1ApproxAge;
    }

    public String getValrepLandimpComp1Area() {
        return NULL_STRING_SETTER(valrepLandimpComp1Area);
    }

    public void setValrepLandimpComp1Area(String valrepLandimpComp1Area) {
        this.valrepLandimpComp1Area = valrepLandimpComp1Area;
    }

    public String getValrepLandimpComp1BasePrice() {
        return NULL_STRING_SETTER(valrepLandimpComp1BasePrice);
    }

    public void setValrepLandimpComp1BasePrice(String valrepLandimpComp1BasePrice) {
        this.valrepLandimpComp1BasePrice = valrepLandimpComp1BasePrice;
    }

    public String getValrepLandimpComp1BasePriceSqm() {
        return NULL_STRING_SETTER(valrepLandimpComp1BasePriceSqm);
    }

    public void setValrepLandimpComp1BasePriceSqm(String valrepLandimpComp1BasePriceSqm) {
        this.valrepLandimpComp1BasePriceSqm = valrepLandimpComp1BasePriceSqm;
    }

    public String getValrepLandimpComp1BldgArea() {
        return NULL_STRING_SETTER(valrepLandimpComp1BldgArea);
    }

    public void setValrepLandimpComp1BldgArea(String valrepLandimpComp1BldgArea) {
        this.valrepLandimpComp1BldgArea = valrepLandimpComp1BldgArea;
    }

    public String getValrepLandimpComp1BldgDepreciation() {
        return NULL_STRING_SETTER(valrepLandimpComp1BldgDepreciation);
    }

    public void setValrepLandimpComp1BldgDepreciation(String valrepLandimpComp1BldgDepreciation) {
        this.valrepLandimpComp1BldgDepreciation = valrepLandimpComp1BldgDepreciation;
    }

    public String getValrepLandimpComp1BldgDepreciationVal() {
        return NULL_STRING_SETTER(valrepLandimpComp1BldgDepreciationVal);
    }

    public void setValrepLandimpComp1BldgDepreciationVal(String valrepLandimpComp1BldgDepreciationVal) {
        this.valrepLandimpComp1BldgDepreciationVal = valrepLandimpComp1BldgDepreciationVal;
    }

    public String getValrepLandimpComp1BldgReplacementUnitValue() {
        return NULL_STRING_SETTER(valrepLandimpComp1BldgReplacementUnitValue);
    }

    public void setValrepLandimpComp1BldgReplacementUnitValue(String valrepLandimpComp1BldgReplacementUnitValue) {
        this.valrepLandimpComp1BldgReplacementUnitValue = valrepLandimpComp1BldgReplacementUnitValue;
    }

    public String getValrepLandimpComp1BldgReplacementValue() {
        return NULL_STRING_SETTER(valrepLandimpComp1BldgReplacementValue);
    }

    public void setValrepLandimpComp1BldgReplacementValue(String valrepLandimpComp1BldgReplacementValue) {
        this.valrepLandimpComp1BldgReplacementValue = valrepLandimpComp1BldgReplacementValue;
    }

    public String getValrepLandimpComp1Characteristic() {
        return NULL_STRING_SETTER(valrepLandimpComp1Characteristic);
    }

    public void setValrepLandimpComp1Characteristic(String valrepLandimpComp1Characteristic) {
        this.valrepLandimpComp1Characteristic = valrepLandimpComp1Characteristic;
    }

    public String getValrepLandimpComp1Characteristics() {
        return NULL_STRING_SETTER(valrepLandimpComp1Characteristics);
    }

    public void setValrepLandimpComp1Characteristics(String valrepLandimpComp1Characteristics) {
        this.valrepLandimpComp1Characteristics = valrepLandimpComp1Characteristics;
    }

    public String getValrepLandimpComp1ConcludedAdjustment() {
        return NULL_STRING_SETTER(valrepLandimpComp1ConcludedAdjustment);
    }

    public void setValrepLandimpComp1ConcludedAdjustment(String valrepLandimpComp1ConcludedAdjustment) {
        this.valrepLandimpComp1ConcludedAdjustment = valrepLandimpComp1ConcludedAdjustment;
    }

    public String getValrepLandimpComp1ContactNo() {
        return NULL_STRING_SETTER(valrepLandimpComp1ContactNo);
    }

    public void setValrepLandimpComp1ContactNo(String valrepLandimpComp1ContactNo) {
        this.valrepLandimpComp1ContactNo = valrepLandimpComp1ContactNo;
    }

    public String getValrepLandimpComp1DateDay() {
        return NULL_STRING_SETTER(valrepLandimpComp1DateDay);
    }

    public void setValrepLandimpComp1DateDay(String valrepLandimpComp1DateDay) {
        this.valrepLandimpComp1DateDay = valrepLandimpComp1DateDay;
    }

    public String getValrepLandimpComp1DateMonth() {
        return NULL_STRING_SETTER(valrepLandimpComp1DateMonth);
    }

    public void setValrepLandimpComp1DateMonth(String valrepLandimpComp1DateMonth) {
        this.valrepLandimpComp1DateMonth = valrepLandimpComp1DateMonth;
    }

    public String getValrepLandimpComp1DateYear() {
        return NULL_STRING_SETTER(valrepLandimpComp1DateYear);
    }

    public void setValrepLandimpComp1DateYear(String valrepLandimpComp1DateYear) {
        this.valrepLandimpComp1DateYear = valrepLandimpComp1DateYear;
    }

    public String getValrepLandimpComp1DiscountRate() {
        return NULL_STRING_SETTER(valrepLandimpComp1DiscountRate);
    }

    public void setValrepLandimpComp1DiscountRate(String valrepLandimpComp1DiscountRate) {
        this.valrepLandimpComp1DiscountRate = valrepLandimpComp1DiscountRate;
    }

    public String getValrepLandimpComp1Discounts() {
        return NULL_STRING_SETTER(valrepLandimpComp1Discounts);
    }

    public void setValrepLandimpComp1Discounts(String valrepLandimpComp1Discounts) {
        this.valrepLandimpComp1Discounts = valrepLandimpComp1Discounts;
    }

    public String getValrepLandimpComp1EffectiveDate() {
        return NULL_STRING_SETTER(valrepLandimpComp1EffectiveDate);
    }

    public void setValrepLandimpComp1EffectiveDate(String valrepLandimpComp1EffectiveDate) {
        this.valrepLandimpComp1EffectiveDate = valrepLandimpComp1EffectiveDate;
    }

    public String getValrepLandimpComp1LandResidualValue() {
        return NULL_STRING_SETTER(valrepLandimpComp1LandResidualValue);
    }

    public void setValrepLandimpComp1LandResidualValue(String valrepLandimpComp1LandResidualValue) {
        this.valrepLandimpComp1LandResidualValue = valrepLandimpComp1LandResidualValue;
    }

    public String getValrepLandimpComp1Location() {
        return NULL_STRING_SETTER(valrepLandimpComp1Location);
    }

    public void setValrepLandimpComp1Location(String valrepLandimpComp1Location) {
        this.valrepLandimpComp1Location = valrepLandimpComp1Location;
    }

    public String getValrepLandimpComp1NetBldgValue() {
        return NULL_STRING_SETTER(valrepLandimpComp1NetBldgValue);
    }

    public void setValrepLandimpComp1NetBldgValue(String valrepLandimpComp1NetBldgValue) {
        this.valrepLandimpComp1NetBldgValue = valrepLandimpComp1NetBldgValue;
    }

    public String getValrepLandimpComp1NetLandValuation() {
        return NULL_STRING_SETTER(valrepLandimpComp1NetLandValuation);
    }

    public void setValrepLandimpComp1NetLandValuation(String valrepLandimpComp1NetLandValuation) {
        this.valrepLandimpComp1NetLandValuation = valrepLandimpComp1NetLandValuation;
    }

    public String getValrepLandimpComp1PriceSqm() {
        return NULL_STRING_SETTER(valrepLandimpComp1PriceSqm);
    }

    public void setValrepLandimpComp1PriceSqm(String valrepLandimpComp1PriceSqm) {
        this.valrepLandimpComp1PriceSqm = valrepLandimpComp1PriceSqm;
    }

    public String getValrepLandimpComp1PriceValue() {
        return NULL_STRING_SETTER(valrepLandimpComp1PriceValue);
    }

    public void setValrepLandimpComp1PriceValue(String valrepLandimpComp1PriceValue) {
        this.valrepLandimpComp1PriceValue = valrepLandimpComp1PriceValue;
    }

    public String getValrepLandimpComp1PricingCircumstance() {
        return NULL_STRING_SETTER(valrepLandimpComp1PricingCircumstance);
    }

    public void setValrepLandimpComp1PricingCircumstance(String valrepLandimpComp1PricingCircumstance) {
        this.valrepLandimpComp1PricingCircumstance = valrepLandimpComp1PricingCircumstance;
    }

    public String getValrepLandimpComp1PropInterest() {
        return NULL_STRING_SETTER(valrepLandimpComp1PropInterest);
    }

    public void setValrepLandimpComp1PropInterest(String valrepLandimpComp1PropInterest) {
        this.valrepLandimpComp1PropInterest = valrepLandimpComp1PropInterest;
    }

    public String getValrepLandimpComp1PropertyType() {
        return NULL_STRING_SETTER(valrepLandimpComp1PropertyType);
    }

    public void setValrepLandimpComp1PropertyType(String valrepLandimpComp1PropertyType) {
        this.valrepLandimpComp1PropertyType = valrepLandimpComp1PropertyType;
    }

    public String getValrepLandimpComp1RecAccessibility() {
        return NULL_STRING_SETTER(valrepLandimpComp1RecAccessibility);
    }

    public void setValrepLandimpComp1RecAccessibility(String valrepLandimpComp1RecAccessibility) {
        this.valrepLandimpComp1RecAccessibility = valrepLandimpComp1RecAccessibility;
    }

    public String getValrepLandimpComp1RecAccessibilityDesc() {
        return NULL_STRING_SETTER(valrepLandimpComp1RecAccessibilityDesc);
    }

    public void setValrepLandimpComp1RecAccessibilityDesc(String valrepLandimpComp1RecAccessibilityDesc) {
        this.valrepLandimpComp1RecAccessibilityDesc = valrepLandimpComp1RecAccessibilityDesc;
    }

    public String getValrepLandimpComp1RecAccessibilityDescription() {
        return NULL_STRING_SETTER(valrepLandimpComp1RecAccessibilityDescription);
    }

    public void setValrepLandimpComp1RecAccessibilityDescription(String valrepLandimpComp1RecAccessibilityDescription) {
        this.valrepLandimpComp1RecAccessibilityDescription = valrepLandimpComp1RecAccessibilityDescription;
    }

    public String getValrepLandimpComp1RecAccessibilitySubprop() {
        return NULL_STRING_SETTER(valrepLandimpComp1RecAccessibilitySubprop);
    }

    public void setValrepLandimpComp1RecAccessibilitySubprop(String valrepLandimpComp1RecAccessibilitySubprop) {
        this.valrepLandimpComp1RecAccessibilitySubprop = valrepLandimpComp1RecAccessibilitySubprop;
    }

    public String getValrepLandimpComp1RecAccessibilityType() {
        return NULL_STRING_SETTER(valrepLandimpComp1RecAccessibilityType);
    }

    public void setValrepLandimpComp1RecAccessibilityType(String valrepLandimpComp1RecAccessibilityType) {
        this.valrepLandimpComp1RecAccessibilityType = valrepLandimpComp1RecAccessibilityType;
    }

    public String getValrepLandimpComp1RecAmenities() {
        return NULL_STRING_SETTER(valrepLandimpComp1RecAmenities);
    }

    public void setValrepLandimpComp1RecAmenities(String valrepLandimpComp1RecAmenities) {
        this.valrepLandimpComp1RecAmenities = valrepLandimpComp1RecAmenities;
    }

    public String getValrepLandimpComp1RecBargainingAllow() {
        return NULL_STRING_SETTER(valrepLandimpComp1RecBargainingAllow);
    }

    public void setValrepLandimpComp1RecBargainingAllow(String valrepLandimpComp1RecBargainingAllow) {
        this.valrepLandimpComp1RecBargainingAllow = valrepLandimpComp1RecBargainingAllow;
    }

    public String getValrepLandimpComp1RecBargainingAllowAdjustmentPrice() {
        return NULL_STRING_SETTER(valrepLandimpComp1RecBargainingAllowAdjustmentPrice);
    }

    public void setValrepLandimpComp1RecBargainingAllowAdjustmentPrice(String valrepLandimpComp1RecBargainingAllowAdjustmentPrice) {
        this.valrepLandimpComp1RecBargainingAllowAdjustmentPrice = valrepLandimpComp1RecBargainingAllowAdjustmentPrice;
    }

    public String getValrepLandimpComp1RecBargainingAllowDesc() {
        return NULL_STRING_SETTER(valrepLandimpComp1RecBargainingAllowDesc);
    }

    public void setValrepLandimpComp1RecBargainingAllowDesc(String valrepLandimpComp1RecBargainingAllowDesc) {
        this.valrepLandimpComp1RecBargainingAllowDesc = valrepLandimpComp1RecBargainingAllowDesc;
    }

    public String getValrepLandimpComp1RecBargainingAllowDescription() {
        return NULL_STRING_SETTER(valrepLandimpComp1RecBargainingAllowDescription);
    }

    public void setValrepLandimpComp1RecBargainingAllowDescription(String valrepLandimpComp1RecBargainingAllowDescription) {
        this.valrepLandimpComp1RecBargainingAllowDescription = valrepLandimpComp1RecBargainingAllowDescription;
    }

    public String getValrepLandimpComp1RecBargainingAllowSubprop() {
        return NULL_STRING_SETTER(valrepLandimpComp1RecBargainingAllowSubprop);
    }

    public void setValrepLandimpComp1RecBargainingAllowSubprop(String valrepLandimpComp1RecBargainingAllowSubprop) {
        this.valrepLandimpComp1RecBargainingAllowSubprop = valrepLandimpComp1RecBargainingAllowSubprop;
    }

    public String getValrepLandimpComp1RecBargainingAllowType() {
        return NULL_STRING_SETTER(valrepLandimpComp1RecBargainingAllowType);
    }

    public void setValrepLandimpComp1RecBargainingAllowType(String valrepLandimpComp1RecBargainingAllowType) {
        this.valrepLandimpComp1RecBargainingAllowType = valrepLandimpComp1RecBargainingAllowType;
    }

    public String getValrepLandimpComp1RecBargainingAllowance() {
        return NULL_STRING_SETTER(valrepLandimpComp1RecBargainingAllowance);
    }

    public void setValrepLandimpComp1RecBargainingAllowance(String valrepLandimpComp1RecBargainingAllowance) {
        this.valrepLandimpComp1RecBargainingAllowance = valrepLandimpComp1RecBargainingAllowance;
    }

    public String getValrepLandimpComp1RecBargainingAllowanceDesc() {
        return NULL_STRING_SETTER(valrepLandimpComp1RecBargainingAllowanceDesc);
    }

    public void setValrepLandimpComp1RecBargainingAllowanceDesc(String valrepLandimpComp1RecBargainingAllowanceDesc) {
        this.valrepLandimpComp1RecBargainingAllowanceDesc = valrepLandimpComp1RecBargainingAllowanceDesc;
    }

    public String getValrepLandimpComp1RecCornerInfluence() {
        return NULL_STRING_SETTER(valrepLandimpComp1RecCornerInfluence);
    }

    public void setValrepLandimpComp1RecCornerInfluence(String valrepLandimpComp1RecCornerInfluence) {
        this.valrepLandimpComp1RecCornerInfluence = valrepLandimpComp1RecCornerInfluence;
    }

    public String getValrepLandimpComp1RecDegreeOfDevt() {
        return NULL_STRING_SETTER(valrepLandimpComp1RecDegreeOfDevt);
    }

    public void setValrepLandimpComp1RecDegreeOfDevt(String valrepLandimpComp1RecDegreeOfDevt) {
        this.valrepLandimpComp1RecDegreeOfDevt = valrepLandimpComp1RecDegreeOfDevt;
    }

    public String getValrepLandimpComp1RecElevation() {
        return NULL_STRING_SETTER(valrepLandimpComp1RecElevation);
    }

    public void setValrepLandimpComp1RecElevation(String valrepLandimpComp1RecElevation) {
        this.valrepLandimpComp1RecElevation = valrepLandimpComp1RecElevation;
    }

    public String getValrepLandimpComp1RecElevationDesc() {
        return NULL_STRING_SETTER(valrepLandimpComp1RecElevationDesc);
    }

    public void setValrepLandimpComp1RecElevationDesc(String valrepLandimpComp1RecElevationDesc) {
        this.valrepLandimpComp1RecElevationDesc = valrepLandimpComp1RecElevationDesc;
    }

    public String getValrepLandimpComp1RecElevationDescription() {
        return NULL_STRING_SETTER(valrepLandimpComp1RecElevationDescription);
    }

    public void setValrepLandimpComp1RecElevationDescription(String valrepLandimpComp1RecElevationDescription) {
        this.valrepLandimpComp1RecElevationDescription = valrepLandimpComp1RecElevationDescription;
    }

    public String getValrepLandimpComp1RecElevationSubprop() {
        return NULL_STRING_SETTER(valrepLandimpComp1RecElevationSubprop);
    }

    public void setValrepLandimpComp1RecElevationSubprop(String valrepLandimpComp1RecElevationSubprop) {
        this.valrepLandimpComp1RecElevationSubprop = valrepLandimpComp1RecElevationSubprop;
    }

    public String getValrepLandimpComp1RecElevationType() {
        return NULL_STRING_SETTER(valrepLandimpComp1RecElevationType);
    }

    public void setValrepLandimpComp1RecElevationType(String valrepLandimpComp1RecElevationType) {
        this.valrepLandimpComp1RecElevationType = valrepLandimpComp1RecElevationType;
    }

    public String getValrepLandimpComp1RecLocation() {
        return NULL_STRING_SETTER(valrepLandimpComp1RecLocation);
    }

    public void setValrepLandimpComp1RecLocation(String valrepLandimpComp1RecLocation) {
        this.valrepLandimpComp1RecLocation = valrepLandimpComp1RecLocation;
    }

    public String getValrepLandimpComp1RecLocationDesc() {
        return NULL_STRING_SETTER(valrepLandimpComp1RecLocationDesc);
    }

    public void setValrepLandimpComp1RecLocationDesc(String valrepLandimpComp1RecLocationDesc) {
        this.valrepLandimpComp1RecLocationDesc = valrepLandimpComp1RecLocationDesc;
    }

    public String getValrepLandimpComp1RecLocationDescription() {
        return NULL_STRING_SETTER(valrepLandimpComp1RecLocationDescription);
    }

    public void setValrepLandimpComp1RecLocationDescription(String valrepLandimpComp1RecLocationDescription) {
        this.valrepLandimpComp1RecLocationDescription = valrepLandimpComp1RecLocationDescription;
    }

    public String getValrepLandimpComp1RecLocationSubprop() {
        return NULL_STRING_SETTER(valrepLandimpComp1RecLocationSubprop);
    }

    public void setValrepLandimpComp1RecLocationSubprop(String valrepLandimpComp1RecLocationSubprop) {
        this.valrepLandimpComp1RecLocationSubprop = valrepLandimpComp1RecLocationSubprop;
    }

    public String getValrepLandimpComp1RecLocationType() {
        return NULL_STRING_SETTER(valrepLandimpComp1RecLocationType);
    }

    public void setValrepLandimpComp1RecLocationType(String valrepLandimpComp1RecLocationType) {
        this.valrepLandimpComp1RecLocationType = valrepLandimpComp1RecLocationType;
    }

    public String getValrepLandimpComp1RecLotType() {
        return NULL_STRING_SETTER(valrepLandimpComp1RecLotType);
    }

    public void setValrepLandimpComp1RecLotType(String valrepLandimpComp1RecLotType) {
        this.valrepLandimpComp1RecLotType = valrepLandimpComp1RecLotType;
    }

    public String getValrepLandimpComp1RecLotTypeDesc() {
        return NULL_STRING_SETTER(valrepLandimpComp1RecLotTypeDesc);
    }

    public void setValrepLandimpComp1RecLotTypeDesc(String valrepLandimpComp1RecLotTypeDesc) {
        this.valrepLandimpComp1RecLotTypeDesc = valrepLandimpComp1RecLotTypeDesc;
    }

    public String getValrepLandimpComp1RecLotTypeDescription() {
        return NULL_STRING_SETTER(valrepLandimpComp1RecLotTypeDescription);
    }

    public void setValrepLandimpComp1RecLotTypeDescription(String valrepLandimpComp1RecLotTypeDescription) {
        this.valrepLandimpComp1RecLotTypeDescription = valrepLandimpComp1RecLotTypeDescription;
    }

    public String getValrepLandimpComp1RecLotTypeSubprop() {
        return NULL_STRING_SETTER(valrepLandimpComp1RecLotTypeSubprop);
    }

    public void setValrepLandimpComp1RecLotTypeSubprop(String valrepLandimpComp1RecLotTypeSubprop) {
        this.valrepLandimpComp1RecLotTypeSubprop = valrepLandimpComp1RecLotTypeSubprop;
    }

    public String getValrepLandimpComp1RecLotTypeType() {
        return NULL_STRING_SETTER(valrepLandimpComp1RecLotTypeType);
    }

    public void setValrepLandimpComp1RecLotTypeType(String valrepLandimpComp1RecLotTypeType) {
        this.valrepLandimpComp1RecLotTypeType = valrepLandimpComp1RecLotTypeType;
    }

    public String getValrepLandimpComp1RecNeighborhood() {
        return NULL_STRING_SETTER(valrepLandimpComp1RecNeighborhood);
    }

    public void setValrepLandimpComp1RecNeighborhood(String valrepLandimpComp1RecNeighborhood) {
        this.valrepLandimpComp1RecNeighborhood = valrepLandimpComp1RecNeighborhood;
    }

    public String getValrepLandimpComp1RecNeighborhoodDesc() {
        return NULL_STRING_SETTER(valrepLandimpComp1RecNeighborhoodDesc);
    }

    public void setValrepLandimpComp1RecNeighborhoodDesc(String valrepLandimpComp1RecNeighborhoodDesc) {
        this.valrepLandimpComp1RecNeighborhoodDesc = valrepLandimpComp1RecNeighborhoodDesc;
    }

    public String getValrepLandimpComp1RecNeighborhoodDescription() {
        return NULL_STRING_SETTER(valrepLandimpComp1RecNeighborhoodDescription);
    }

    public void setValrepLandimpComp1RecNeighborhoodDescription(String valrepLandimpComp1RecNeighborhoodDescription) {
        this.valrepLandimpComp1RecNeighborhoodDescription = valrepLandimpComp1RecNeighborhoodDescription;
    }

    public String getValrepLandimpComp1RecNeighborhoodSubprop() {
        return NULL_STRING_SETTER(valrepLandimpComp1RecNeighborhoodSubprop);
    }

    public void setValrepLandimpComp1RecNeighborhoodSubprop(String valrepLandimpComp1RecNeighborhoodSubprop) {
        this.valrepLandimpComp1RecNeighborhoodSubprop = valrepLandimpComp1RecNeighborhoodSubprop;
    }

    public String getValrepLandimpComp1RecNeighborhoodType() {
        return NULL_STRING_SETTER(valrepLandimpComp1RecNeighborhoodType);
    }

    public void setValrepLandimpComp1RecNeighborhoodType(String valrepLandimpComp1RecNeighborhoodType) {
        this.valrepLandimpComp1RecNeighborhoodType = valrepLandimpComp1RecNeighborhoodType;
    }

    public String getValrepLandimpComp1RecOthers() {
        return NULL_STRING_SETTER(valrepLandimpComp1RecOthers);
    }

    public void setValrepLandimpComp1RecOthers(String valrepLandimpComp1RecOthers) {
        this.valrepLandimpComp1RecOthers = valrepLandimpComp1RecOthers;
    }

    public String getValrepLandimpComp1RecOthers2() {
        return NULL_STRING_SETTER(valrepLandimpComp1RecOthers2);
    }

    public void setValrepLandimpComp1RecOthers2(String valrepLandimpComp1RecOthers2) {
        this.valrepLandimpComp1RecOthers2 = valrepLandimpComp1RecOthers2;
    }

    public String getValrepLandimpComp1RecOthers2Desc() {
        return NULL_STRING_SETTER(valrepLandimpComp1RecOthers2Desc);
    }

    public void setValrepLandimpComp1RecOthers2Desc(String valrepLandimpComp1RecOthers2Desc) {
        this.valrepLandimpComp1RecOthers2Desc = valrepLandimpComp1RecOthers2Desc;
    }

    public String getValrepLandimpComp1RecOthers2Description() {
        return NULL_STRING_SETTER(valrepLandimpComp1RecOthers2Description);
    }

    public void setValrepLandimpComp1RecOthers2Description(String valrepLandimpComp1RecOthers2Description) {
        this.valrepLandimpComp1RecOthers2Description = valrepLandimpComp1RecOthers2Description;
    }

    public String getValrepLandimpComp1RecOthers2Subprop() {
        return NULL_STRING_SETTER(valrepLandimpComp1RecOthers2Subprop);
    }

    public void setValrepLandimpComp1RecOthers2Subprop(String valrepLandimpComp1RecOthers2Subprop) {
        this.valrepLandimpComp1RecOthers2Subprop = valrepLandimpComp1RecOthers2Subprop;
    }

    public String getValrepLandimpComp1RecOthers2Type() {
        return NULL_STRING_SETTER(valrepLandimpComp1RecOthers2Type);
    }

    public void setValrepLandimpComp1RecOthers2Type(String valrepLandimpComp1RecOthers2Type) {
        this.valrepLandimpComp1RecOthers2Type = valrepLandimpComp1RecOthers2Type;
    }

    public String getValrepLandimpComp1RecOthers3() {
        return NULL_STRING_SETTER(valrepLandimpComp1RecOthers3);
    }

    public void setValrepLandimpComp1RecOthers3(String valrepLandimpComp1RecOthers3) {
        this.valrepLandimpComp1RecOthers3 = valrepLandimpComp1RecOthers3;
    }

    public String getValrepLandimpComp1RecOthers3Desc() {
        return NULL_STRING_SETTER(valrepLandimpComp1RecOthers3Desc);
    }

    public void setValrepLandimpComp1RecOthers3Desc(String valrepLandimpComp1RecOthers3Desc) {
        this.valrepLandimpComp1RecOthers3Desc = valrepLandimpComp1RecOthers3Desc;
    }

    public String getValrepLandimpComp1RecOthers3Description() {
        return NULL_STRING_SETTER(valrepLandimpComp1RecOthers3Description);
    }

    public void setValrepLandimpComp1RecOthers3Description(String valrepLandimpComp1RecOthers3Description) {
        this.valrepLandimpComp1RecOthers3Description = valrepLandimpComp1RecOthers3Description;
    }

    public String getValrepLandimpComp1RecOthers3Subprop() {
        return NULL_STRING_SETTER(valrepLandimpComp1RecOthers3Subprop);
    }

    public void setValrepLandimpComp1RecOthers3Subprop(String valrepLandimpComp1RecOthers3Subprop) {
        this.valrepLandimpComp1RecOthers3Subprop = valrepLandimpComp1RecOthers3Subprop;
    }

    public String getValrepLandimpComp1RecOthers3Type() {
        return NULL_STRING_SETTER(valrepLandimpComp1RecOthers3Type);
    }

    public void setValrepLandimpComp1RecOthers3Type(String valrepLandimpComp1RecOthers3Type) {
        this.valrepLandimpComp1RecOthers3Type = valrepLandimpComp1RecOthers3Type;
    }

    public String getValrepLandimpComp1RecOthersDesc() {
        return NULL_STRING_SETTER(valrepLandimpComp1RecOthersDesc);
    }

    public void setValrepLandimpComp1RecOthersDesc(String valrepLandimpComp1RecOthersDesc) {
        this.valrepLandimpComp1RecOthersDesc = valrepLandimpComp1RecOthersDesc;
    }

    public String getValrepLandimpComp1RecOthersDescription() {
        return NULL_STRING_SETTER(valrepLandimpComp1RecOthersDescription);
    }

    public void setValrepLandimpComp1RecOthersDescription(String valrepLandimpComp1RecOthersDescription) {
        this.valrepLandimpComp1RecOthersDescription = valrepLandimpComp1RecOthersDescription;
    }

    public String getValrepLandimpComp1RecOthersName() {
        return NULL_STRING_SETTER(valrepLandimpComp1RecOthersName);
    }

    public void setValrepLandimpComp1RecOthersName(String valrepLandimpComp1RecOthersName) {
        this.valrepLandimpComp1RecOthersName = valrepLandimpComp1RecOthersName;
    }

    public String getValrepLandimpComp1RecOthersSubprop() {
        return NULL_STRING_SETTER(valrepLandimpComp1RecOthersSubprop);
    }

    public void setValrepLandimpComp1RecOthersSubprop(String valrepLandimpComp1RecOthersSubprop) {
        this.valrepLandimpComp1RecOthersSubprop = valrepLandimpComp1RecOthersSubprop;
    }

    public String getValrepLandimpComp1RecOthersType() {
        return NULL_STRING_SETTER(valrepLandimpComp1RecOthersType);
    }

    public void setValrepLandimpComp1RecOthersType(String valrepLandimpComp1RecOthersType) {
        this.valrepLandimpComp1RecOthersType = valrepLandimpComp1RecOthersType;
    }

    public String getValrepLandimpComp1RecPropRights() {
        return NULL_STRING_SETTER(valrepLandimpComp1RecPropRights);
    }

    public void setValrepLandimpComp1RecPropRights(String valrepLandimpComp1RecPropRights) {
        this.valrepLandimpComp1RecPropRights = valrepLandimpComp1RecPropRights;
    }

    public String getValrepLandimpComp1RecPropRightsDesc() {
        return NULL_STRING_SETTER(valrepLandimpComp1RecPropRightsDesc);
    }

    public void setValrepLandimpComp1RecPropRightsDesc(String valrepLandimpComp1RecPropRightsDesc) {
        this.valrepLandimpComp1RecPropRightsDesc = valrepLandimpComp1RecPropRightsDesc;
    }

    public String getValrepLandimpComp1RecPropertyRightsConveyed() {
        return NULL_STRING_SETTER(valrepLandimpComp1RecPropertyRightsConveyed);
    }

    public void setValrepLandimpComp1RecPropertyRightsConveyed(String valrepLandimpComp1RecPropertyRightsConveyed) {
        this.valrepLandimpComp1RecPropertyRightsConveyed = valrepLandimpComp1RecPropertyRightsConveyed;
    }

    public String getValrepLandimpComp1RecPropertyRightsConveyedAdjustmentPrice() {
        return NULL_STRING_SETTER(valrepLandimpComp1RecPropertyRightsConveyedAdjustmentPrice);
    }

    public void setValrepLandimpComp1RecPropertyRightsConveyedAdjustmentPrice(String valrepLandimpComp1RecPropertyRightsConveyedAdjustmentPrice) {
        this.valrepLandimpComp1RecPropertyRightsConveyedAdjustmentPrice = valrepLandimpComp1RecPropertyRightsConveyedAdjustmentPrice;
    }

    public String getValrepLandimpComp1RecPropertyRightsConveyedDesc() {
        return NULL_STRING_SETTER(valrepLandimpComp1RecPropertyRightsConveyedDesc);
    }

    public void setValrepLandimpComp1RecPropertyRightsConveyedDesc(String valrepLandimpComp1RecPropertyRightsConveyedDesc) {
        this.valrepLandimpComp1RecPropertyRightsConveyedDesc = valrepLandimpComp1RecPropertyRightsConveyedDesc;
    }

    public String getValrepLandimpComp1RecPropertyRightsConveyedDescription() {
        return NULL_STRING_SETTER(valrepLandimpComp1RecPropertyRightsConveyedDescription);
    }

    public void setValrepLandimpComp1RecPropertyRightsConveyedDescription(String valrepLandimpComp1RecPropertyRightsConveyedDescription) {
        this.valrepLandimpComp1RecPropertyRightsConveyedDescription = valrepLandimpComp1RecPropertyRightsConveyedDescription;
    }

    public String getValrepLandimpComp1RecPropertyRightsConveyedSubprop() {
        return NULL_STRING_SETTER(valrepLandimpComp1RecPropertyRightsConveyedSubprop);
    }

    public void setValrepLandimpComp1RecPropertyRightsConveyedSubprop(String valrepLandimpComp1RecPropertyRightsConveyedSubprop) {
        this.valrepLandimpComp1RecPropertyRightsConveyedSubprop = valrepLandimpComp1RecPropertyRightsConveyedSubprop;
    }

    public String getValrepLandimpComp1RecPropertyRightsConveyedType() {
        return NULL_STRING_SETTER(valrepLandimpComp1RecPropertyRightsConveyedType);
    }

    public void setValrepLandimpComp1RecPropertyRightsConveyedType(String valrepLandimpComp1RecPropertyRightsConveyedType) {
        this.valrepLandimpComp1RecPropertyRightsConveyedType = valrepLandimpComp1RecPropertyRightsConveyedType;
    }

    public String getValrepLandimpComp1RecRoad() {
        return NULL_STRING_SETTER(valrepLandimpComp1RecRoad);
    }

    public void setValrepLandimpComp1RecRoad(String valrepLandimpComp1RecRoad) {
        this.valrepLandimpComp1RecRoad = valrepLandimpComp1RecRoad;
    }

    public String getValrepLandimpComp1RecRoadDesc() {
        return NULL_STRING_SETTER(valrepLandimpComp1RecRoadDesc);
    }

    public void setValrepLandimpComp1RecRoadDesc(String valrepLandimpComp1RecRoadDesc) {
        this.valrepLandimpComp1RecRoadDesc = valrepLandimpComp1RecRoadDesc;
    }

    public String getValrepLandimpComp1RecRoadDescription() {
        return NULL_STRING_SETTER(valrepLandimpComp1RecRoadDescription);
    }

    public void setValrepLandimpComp1RecRoadDescription(String valrepLandimpComp1RecRoadDescription) {
        this.valrepLandimpComp1RecRoadDescription = valrepLandimpComp1RecRoadDescription;
    }

    public String getValrepLandimpComp1RecRoadSubprop() {
        return NULL_STRING_SETTER(valrepLandimpComp1RecRoadSubprop);
    }

    public void setValrepLandimpComp1RecRoadSubprop(String valrepLandimpComp1RecRoadSubprop) {
        this.valrepLandimpComp1RecRoadSubprop = valrepLandimpComp1RecRoadSubprop;
    }

    public String getValrepLandimpComp1RecRoadType() {
        return NULL_STRING_SETTER(valrepLandimpComp1RecRoadType);
    }

    public void setValrepLandimpComp1RecRoadType(String valrepLandimpComp1RecRoadType) {
        this.valrepLandimpComp1RecRoadType = valrepLandimpComp1RecRoadType;
    }

    public String getValrepLandimpComp1RecShape() {
        return NULL_STRING_SETTER(valrepLandimpComp1RecShape);
    }

    public void setValrepLandimpComp1RecShape(String valrepLandimpComp1RecShape) {
        this.valrepLandimpComp1RecShape = valrepLandimpComp1RecShape;
    }

    public String getValrepLandimpComp1RecShapeDesc() {
        return NULL_STRING_SETTER(valrepLandimpComp1RecShapeDesc);
    }

    public void setValrepLandimpComp1RecShapeDesc(String valrepLandimpComp1RecShapeDesc) {
        this.valrepLandimpComp1RecShapeDesc = valrepLandimpComp1RecShapeDesc;
    }

    public String getValrepLandimpComp1RecShapeDescription() {
        return NULL_STRING_SETTER(valrepLandimpComp1RecShapeDescription);
    }

    public void setValrepLandimpComp1RecShapeDescription(String valrepLandimpComp1RecShapeDescription) {
        this.valrepLandimpComp1RecShapeDescription = valrepLandimpComp1RecShapeDescription;
    }

    public String getValrepLandimpComp1RecShapeSubprop() {
        return NULL_STRING_SETTER(valrepLandimpComp1RecShapeSubprop);
    }

    public void setValrepLandimpComp1RecShapeSubprop(String valrepLandimpComp1RecShapeSubprop) {
        this.valrepLandimpComp1RecShapeSubprop = valrepLandimpComp1RecShapeSubprop;
    }

    public String getValrepLandimpComp1RecShapeType() {
        return NULL_STRING_SETTER(valrepLandimpComp1RecShapeType);
    }

    public void setValrepLandimpComp1RecShapeType(String valrepLandimpComp1RecShapeType) {
        this.valrepLandimpComp1RecShapeType = valrepLandimpComp1RecShapeType;
    }

    public String getValrepLandimpComp1RecSize() {
        return NULL_STRING_SETTER(valrepLandimpComp1RecSize);
    }

    public void setValrepLandimpComp1RecSize(String valrepLandimpComp1RecSize) {
        this.valrepLandimpComp1RecSize = valrepLandimpComp1RecSize;
    }

    public String getValrepLandimpComp1RecSizeDesc() {
        return NULL_STRING_SETTER(valrepLandimpComp1RecSizeDesc);
    }

    public void setValrepLandimpComp1RecSizeDesc(String valrepLandimpComp1RecSizeDesc) {
        this.valrepLandimpComp1RecSizeDesc = valrepLandimpComp1RecSizeDesc;
    }

    public String getValrepLandimpComp1RecSizeDescription() {
        return NULL_STRING_SETTER(valrepLandimpComp1RecSizeDescription);
    }

    public void setValrepLandimpComp1RecSizeDescription(String valrepLandimpComp1RecSizeDescription) {
        this.valrepLandimpComp1RecSizeDescription = valrepLandimpComp1RecSizeDescription;
    }

    public String getValrepLandimpComp1RecSizeSubprop() {
        return NULL_STRING_SETTER(valrepLandimpComp1RecSizeSubprop);
    }

    public void setValrepLandimpComp1RecSizeSubprop(String valrepLandimpComp1RecSizeSubprop) {
        this.valrepLandimpComp1RecSizeSubprop = valrepLandimpComp1RecSizeSubprop;
    }

    public String getValrepLandimpComp1RecSizeType() {
        return NULL_STRING_SETTER(valrepLandimpComp1RecSizeType);
    }

    public void setValrepLandimpComp1RecSizeType(String valrepLandimpComp1RecSizeType) {
        this.valrepLandimpComp1RecSizeType = valrepLandimpComp1RecSizeType;
    }

    public String getValrepLandimpComp1RecTerrain() {
        return NULL_STRING_SETTER(valrepLandimpComp1RecTerrain);
    }

    public void setValrepLandimpComp1RecTerrain(String valrepLandimpComp1RecTerrain) {
        this.valrepLandimpComp1RecTerrain = valrepLandimpComp1RecTerrain;
    }

    public String getValrepLandimpComp1RecTerrainDesc() {
        return NULL_STRING_SETTER(valrepLandimpComp1RecTerrainDesc);
    }

    public void setValrepLandimpComp1RecTerrainDesc(String valrepLandimpComp1RecTerrainDesc) {
        this.valrepLandimpComp1RecTerrainDesc = valrepLandimpComp1RecTerrainDesc;
    }

    public String getValrepLandimpComp1RecTerrainDescription() {
        return NULL_STRING_SETTER(valrepLandimpComp1RecTerrainDescription);
    }

    public void setValrepLandimpComp1RecTerrainDescription(String valrepLandimpComp1RecTerrainDescription) {
        this.valrepLandimpComp1RecTerrainDescription = valrepLandimpComp1RecTerrainDescription;
    }

    public String getValrepLandimpComp1RecTerrainSubprop() {
        return NULL_STRING_SETTER(valrepLandimpComp1RecTerrainSubprop);
    }

    public void setValrepLandimpComp1RecTerrainSubprop(String valrepLandimpComp1RecTerrainSubprop) {
        this.valrepLandimpComp1RecTerrainSubprop = valrepLandimpComp1RecTerrainSubprop;
    }

    public String getValrepLandimpComp1RecTerrainType() {
        return NULL_STRING_SETTER(valrepLandimpComp1RecTerrainType);
    }

    public void setValrepLandimpComp1RecTerrainType(String valrepLandimpComp1RecTerrainType) {
        this.valrepLandimpComp1RecTerrainType = valrepLandimpComp1RecTerrainType;
    }

    public String getValrepLandimpComp1RecTime() {
        return NULL_STRING_SETTER(valrepLandimpComp1RecTime);
    }

    public void setValrepLandimpComp1RecTime(String valrepLandimpComp1RecTime) {
        this.valrepLandimpComp1RecTime = valrepLandimpComp1RecTime;
    }

    public String getValrepLandimpComp1RecTime1Subprop() {
        return NULL_STRING_SETTER(valrepLandimpComp1RecTime1Subprop);
    }

    public void setValrepLandimpComp1RecTime1Subprop(String valrepLandimpComp1RecTime1Subprop) {
        this.valrepLandimpComp1RecTime1Subprop = valrepLandimpComp1RecTime1Subprop;
    }

    public String getValrepLandimpComp1RecTime1Type() {
        return NULL_STRING_SETTER(valrepLandimpComp1RecTime1Type);
    }

    public void setValrepLandimpComp1RecTime1Type(String valrepLandimpComp1RecTime1Type) {
        this.valrepLandimpComp1RecTime1Type = valrepLandimpComp1RecTime1Type;
    }

    public String getValrepLandimpComp1RecTimeAdjustmentPrice() {
        return NULL_STRING_SETTER(valrepLandimpComp1RecTimeAdjustmentPrice);
    }

    public void setValrepLandimpComp1RecTimeAdjustmentPrice(String valrepLandimpComp1RecTimeAdjustmentPrice) {
        this.valrepLandimpComp1RecTimeAdjustmentPrice = valrepLandimpComp1RecTimeAdjustmentPrice;
    }

    public String getValrepLandimpComp1RecTimeDesc() {
        return NULL_STRING_SETTER(valrepLandimpComp1RecTimeDesc);
    }

    public void setValrepLandimpComp1RecTimeDesc(String valrepLandimpComp1RecTimeDesc) {
        this.valrepLandimpComp1RecTimeDesc = valrepLandimpComp1RecTimeDesc;
    }

    public String getValrepLandimpComp1RecTimeDescription() {
        return NULL_STRING_SETTER(valrepLandimpComp1RecTimeDescription);
    }

    public void setValrepLandimpComp1RecTimeDescription(String valrepLandimpComp1RecTimeDescription) {
        this.valrepLandimpComp1RecTimeDescription = valrepLandimpComp1RecTimeDescription;
    }

    public String getValrepLandimpComp1RecTimeElement() {
        return NULL_STRING_SETTER(valrepLandimpComp1RecTimeElement);
    }

    public void setValrepLandimpComp1RecTimeElement(String valrepLandimpComp1RecTimeElement) {
        this.valrepLandimpComp1RecTimeElement = valrepLandimpComp1RecTimeElement;
    }

    public String getValrepLandimpComp1RecTimeElementDesc() {
        return NULL_STRING_SETTER(valrepLandimpComp1RecTimeElementDesc);
    }

    public void setValrepLandimpComp1RecTimeElementDesc(String valrepLandimpComp1RecTimeElementDesc) {
        this.valrepLandimpComp1RecTimeElementDesc = valrepLandimpComp1RecTimeElementDesc;
    }

    public String getValrepLandimpComp1RecTruLot() {
        return NULL_STRING_SETTER(valrepLandimpComp1RecTruLot);
    }

    public void setValrepLandimpComp1RecTruLot(String valrepLandimpComp1RecTruLot) {
        this.valrepLandimpComp1RecTruLot = valrepLandimpComp1RecTruLot;
    }

    public String getValrepLandimpComp1Remarks() {
        return NULL_STRING_SETTER(valrepLandimpComp1Remarks);
    }

    public void setValrepLandimpComp1Remarks(String valrepLandimpComp1Remarks) {
        this.valrepLandimpComp1Remarks = valrepLandimpComp1Remarks;
    }

    public String getValrepLandimpComp1SellingPrice() {
        return NULL_STRING_SETTER(valrepLandimpComp1SellingPrice);
    }

    public void setValrepLandimpComp1SellingPrice(String valrepLandimpComp1SellingPrice) {
        this.valrepLandimpComp1SellingPrice = valrepLandimpComp1SellingPrice;
    }

    public String getValrepLandimpComp1Shape() {
        return NULL_STRING_SETTER(valrepLandimpComp1Shape);
    }

    public void setValrepLandimpComp1Shape(String valrepLandimpComp1Shape) {
        this.valrepLandimpComp1Shape = valrepLandimpComp1Shape;
    }

    public String getValrepLandimpComp1Source() {
        return NULL_STRING_SETTER(valrepLandimpComp1Source);
    }

    public void setValrepLandimpComp1Source(String valrepLandimpComp1Source) {
        this.valrepLandimpComp1Source = valrepLandimpComp1Source;
    }

    public String getValrepLandimpComp1TotalAdj() {
        return NULL_STRING_SETTER(valrepLandimpComp1TotalAdj);
    }

    public void setValrepLandimpComp1TotalAdj(String valrepLandimpComp1TotalAdj) {
        this.valrepLandimpComp1TotalAdj = valrepLandimpComp1TotalAdj;
    }

    public String getValrepLandimpComp1TotalgrossAdj() {
        return NULL_STRING_SETTER(valrepLandimpComp1TotalgrossAdj);
    }

    public void setValrepLandimpComp1TotalgrossAdj(String valrepLandimpComp1TotalgrossAdj) {
        this.valrepLandimpComp1TotalgrossAdj = valrepLandimpComp1TotalgrossAdj;
    }

    public String getValrepLandimpComp1TotalnetAdj() {
        return NULL_STRING_SETTER(valrepLandimpComp1TotalnetAdj);
    }

    public void setValrepLandimpComp1TotalnetAdj(String valrepLandimpComp1TotalnetAdj) {
        this.valrepLandimpComp1TotalnetAdj = valrepLandimpComp1TotalnetAdj;
    }

    public String getValrepLandimpComp1Weight() {
        return NULL_STRING_SETTER(valrepLandimpComp1Weight);
    }

    public void setValrepLandimpComp1Weight(String valrepLandimpComp1Weight) {
        this.valrepLandimpComp1Weight = valrepLandimpComp1Weight;
    }

    public String getValrepLandimpComp1WeightEquivalent() {
        return NULL_STRING_SETTER(valrepLandimpComp1WeightEquivalent);
    }

    public void setValrepLandimpComp1WeightEquivalent(String valrepLandimpComp1WeightEquivalent) {
        this.valrepLandimpComp1WeightEquivalent = valrepLandimpComp1WeightEquivalent;
    }

    public String getValrepLandimpComp1WeightedValue() {
        return NULL_STRING_SETTER(valrepLandimpComp1WeightedValue);
    }

    public void setValrepLandimpComp1WeightedValue(String valrepLandimpComp1WeightedValue) {
        this.valrepLandimpComp1WeightedValue = valrepLandimpComp1WeightedValue;
    }

    public String getValrepLandimpComp1WgtDist() {
        return NULL_STRING_SETTER(valrepLandimpComp1WgtDist);
    }

    public void setValrepLandimpComp1WgtDist(String valrepLandimpComp1WgtDist) {
        this.valrepLandimpComp1WgtDist = valrepLandimpComp1WgtDist;
    }

    public String getValrepLandimpComp1Zoning() {
        return NULL_STRING_SETTER(valrepLandimpComp1Zoning);
    }

    public void setValrepLandimpComp1Zoning(String valrepLandimpComp1Zoning) {
        this.valrepLandimpComp1Zoning = valrepLandimpComp1Zoning;
    }

    public String getValrepLandimpComp2AdjustablePriceSqm() {
        return NULL_STRING_SETTER(valrepLandimpComp2AdjustablePriceSqm);
    }

    public void setValrepLandimpComp2AdjustablePriceSqm(String valrepLandimpComp2AdjustablePriceSqm) {
        this.valrepLandimpComp2AdjustablePriceSqm = valrepLandimpComp2AdjustablePriceSqm;
    }

    public String getValrepLandimpComp2AdjustedValue() {
        return NULL_STRING_SETTER(valrepLandimpComp2AdjustedValue);
    }

    public void setValrepLandimpComp2AdjustedValue(String valrepLandimpComp2AdjustedValue) {
        this.valrepLandimpComp2AdjustedValue = valrepLandimpComp2AdjustedValue;
    }

    public String getValrepLandimpComp2ApproxAge() {
        return NULL_STRING_SETTER(valrepLandimpComp2ApproxAge);
    }

    public void setValrepLandimpComp2ApproxAge(String valrepLandimpComp2ApproxAge) {
        this.valrepLandimpComp2ApproxAge = valrepLandimpComp2ApproxAge;
    }

    public String getValrepLandimpComp2Area() {
        return NULL_STRING_SETTER(valrepLandimpComp2Area);
    }

    public void setValrepLandimpComp2Area(String valrepLandimpComp2Area) {
        this.valrepLandimpComp2Area = valrepLandimpComp2Area;
    }

    public String getValrepLandimpComp2BasePrice() {
        return NULL_STRING_SETTER(valrepLandimpComp2BasePrice);
    }

    public void setValrepLandimpComp2BasePrice(String valrepLandimpComp2BasePrice) {
        this.valrepLandimpComp2BasePrice = valrepLandimpComp2BasePrice;
    }

    public String getValrepLandimpComp2BasePriceSqm() {
        return NULL_STRING_SETTER(valrepLandimpComp2BasePriceSqm);
    }

    public void setValrepLandimpComp2BasePriceSqm(String valrepLandimpComp2BasePriceSqm) {
        this.valrepLandimpComp2BasePriceSqm = valrepLandimpComp2BasePriceSqm;
    }

    public String getValrepLandimpComp2BldgArea() {
        return NULL_STRING_SETTER(valrepLandimpComp2BldgArea);
    }

    public void setValrepLandimpComp2BldgArea(String valrepLandimpComp2BldgArea) {
        this.valrepLandimpComp2BldgArea = valrepLandimpComp2BldgArea;
    }

    public String getValrepLandimpComp2BldgDepreciation() {
        return NULL_STRING_SETTER(valrepLandimpComp2BldgDepreciation);
    }

    public void setValrepLandimpComp2BldgDepreciation(String valrepLandimpComp2BldgDepreciation) {
        this.valrepLandimpComp2BldgDepreciation = valrepLandimpComp2BldgDepreciation;
    }

    public String getValrepLandimpComp2BldgDepreciationVal() {
        return NULL_STRING_SETTER(valrepLandimpComp2BldgDepreciationVal);
    }

    public void setValrepLandimpComp2BldgDepreciationVal(String valrepLandimpComp2BldgDepreciationVal) {
        this.valrepLandimpComp2BldgDepreciationVal = valrepLandimpComp2BldgDepreciationVal;
    }

    public String getValrepLandimpComp2BldgReplacementUnitValue() {
        return NULL_STRING_SETTER(valrepLandimpComp2BldgReplacementUnitValue);
    }

    public void setValrepLandimpComp2BldgReplacementUnitValue(String valrepLandimpComp2BldgReplacementUnitValue) {
        this.valrepLandimpComp2BldgReplacementUnitValue = valrepLandimpComp2BldgReplacementUnitValue;
    }

    public String getValrepLandimpComp2BldgReplacementValue() {
        return NULL_STRING_SETTER(valrepLandimpComp2BldgReplacementValue);
    }

    public void setValrepLandimpComp2BldgReplacementValue(String valrepLandimpComp2BldgReplacementValue) {
        this.valrepLandimpComp2BldgReplacementValue = valrepLandimpComp2BldgReplacementValue;
    }

    public String getValrepLandimpComp2Characteristic() {
        return NULL_STRING_SETTER(valrepLandimpComp2Characteristic);
    }

    public void setValrepLandimpComp2Characteristic(String valrepLandimpComp2Characteristic) {
        this.valrepLandimpComp2Characteristic = valrepLandimpComp2Characteristic;
    }

    public String getValrepLandimpComp2Characteristics() {
        return NULL_STRING_SETTER(valrepLandimpComp2Characteristics);
    }

    public void setValrepLandimpComp2Characteristics(String valrepLandimpComp2Characteristics) {
        this.valrepLandimpComp2Characteristics = valrepLandimpComp2Characteristics;
    }

    public String getValrepLandimpComp2ConcludedAdjustment() {
        return NULL_STRING_SETTER(valrepLandimpComp2ConcludedAdjustment);
    }

    public void setValrepLandimpComp2ConcludedAdjustment(String valrepLandimpComp2ConcludedAdjustment) {
        this.valrepLandimpComp2ConcludedAdjustment = valrepLandimpComp2ConcludedAdjustment;
    }

    public String getValrepLandimpComp2ContactNo() {
        return NULL_STRING_SETTER(valrepLandimpComp2ContactNo);
    }

    public void setValrepLandimpComp2ContactNo(String valrepLandimpComp2ContactNo) {
        this.valrepLandimpComp2ContactNo = valrepLandimpComp2ContactNo;
    }

    public String getValrepLandimpComp2DateDay() {
        return NULL_STRING_SETTER(valrepLandimpComp2DateDay);
    }

    public void setValrepLandimpComp2DateDay(String valrepLandimpComp2DateDay) {
        this.valrepLandimpComp2DateDay = valrepLandimpComp2DateDay;
    }

    public String getValrepLandimpComp2DateMonth() {
        return NULL_STRING_SETTER(valrepLandimpComp2DateMonth);
    }

    public void setValrepLandimpComp2DateMonth(String valrepLandimpComp2DateMonth) {
        this.valrepLandimpComp2DateMonth = valrepLandimpComp2DateMonth;
    }

    public String getValrepLandimpComp2DateYear() {
        return NULL_STRING_SETTER(valrepLandimpComp2DateYear);
    }

    public void setValrepLandimpComp2DateYear(String valrepLandimpComp2DateYear) {
        this.valrepLandimpComp2DateYear = valrepLandimpComp2DateYear;
    }

    public String getValrepLandimpComp2DiscountRate() {
        return NULL_STRING_SETTER(valrepLandimpComp2DiscountRate);
    }

    public void setValrepLandimpComp2DiscountRate(String valrepLandimpComp2DiscountRate) {
        this.valrepLandimpComp2DiscountRate = valrepLandimpComp2DiscountRate;
    }

    public String getValrepLandimpComp2Discounts() {
        return NULL_STRING_SETTER(valrepLandimpComp2Discounts);
    }

    public void setValrepLandimpComp2Discounts(String valrepLandimpComp2Discounts) {
        this.valrepLandimpComp2Discounts = valrepLandimpComp2Discounts;
    }

    public String getValrepLandimpComp2EffectiveDate() {
        return NULL_STRING_SETTER(valrepLandimpComp2EffectiveDate);
    }

    public void setValrepLandimpComp2EffectiveDate(String valrepLandimpComp2EffectiveDate) {
        this.valrepLandimpComp2EffectiveDate = valrepLandimpComp2EffectiveDate;
    }

    public String getValrepLandimpComp2LandResidualValue() {
        return NULL_STRING_SETTER(valrepLandimpComp2LandResidualValue);
    }

    public void setValrepLandimpComp2LandResidualValue(String valrepLandimpComp2LandResidualValue) {
        this.valrepLandimpComp2LandResidualValue = valrepLandimpComp2LandResidualValue;
    }

    public String getValrepLandimpComp2Location() {
        return NULL_STRING_SETTER(valrepLandimpComp2Location);
    }

    public void setValrepLandimpComp2Location(String valrepLandimpComp2Location) {
        this.valrepLandimpComp2Location = valrepLandimpComp2Location;
    }

    public String getValrepLandimpComp2NetBldgValue() {
        return NULL_STRING_SETTER(valrepLandimpComp2NetBldgValue);
    }

    public void setValrepLandimpComp2NetBldgValue(String valrepLandimpComp2NetBldgValue) {
        this.valrepLandimpComp2NetBldgValue = valrepLandimpComp2NetBldgValue;
    }

    public String getValrepLandimpComp2NetLandValuation() {
        return NULL_STRING_SETTER(valrepLandimpComp2NetLandValuation);
    }

    public void setValrepLandimpComp2NetLandValuation(String valrepLandimpComp2NetLandValuation) {
        this.valrepLandimpComp2NetLandValuation = valrepLandimpComp2NetLandValuation;
    }

    public String getValrepLandimpComp2PriceSqm() {
        return NULL_STRING_SETTER(valrepLandimpComp2PriceSqm);
    }

    public void setValrepLandimpComp2PriceSqm(String valrepLandimpComp2PriceSqm) {
        this.valrepLandimpComp2PriceSqm = valrepLandimpComp2PriceSqm;
    }

    public String getValrepLandimpComp2PriceValue() {
        return NULL_STRING_SETTER(valrepLandimpComp2PriceValue);
    }

    public void setValrepLandimpComp2PriceValue(String valrepLandimpComp2PriceValue) {
        this.valrepLandimpComp2PriceValue = valrepLandimpComp2PriceValue;
    }

    public String getValrepLandimpComp2PricingCircumstance() {
        return NULL_STRING_SETTER(valrepLandimpComp2PricingCircumstance);
    }

    public void setValrepLandimpComp2PricingCircumstance(String valrepLandimpComp2PricingCircumstance) {
        this.valrepLandimpComp2PricingCircumstance = valrepLandimpComp2PricingCircumstance;
    }

    public String getValrepLandimpComp2PropInterest() {
        return NULL_STRING_SETTER(valrepLandimpComp2PropInterest);
    }

    public void setValrepLandimpComp2PropInterest(String valrepLandimpComp2PropInterest) {
        this.valrepLandimpComp2PropInterest = valrepLandimpComp2PropInterest;
    }

    public String getValrepLandimpComp2PropertyType() {
        return NULL_STRING_SETTER(valrepLandimpComp2PropertyType);
    }

    public void setValrepLandimpComp2PropertyType(String valrepLandimpComp2PropertyType) {
        this.valrepLandimpComp2PropertyType = valrepLandimpComp2PropertyType;
    }

    public String getValrepLandimpComp2RecAccessibility() {
        return NULL_STRING_SETTER(valrepLandimpComp2RecAccessibility);
    }

    public void setValrepLandimpComp2RecAccessibility(String valrepLandimpComp2RecAccessibility) {
        this.valrepLandimpComp2RecAccessibility = valrepLandimpComp2RecAccessibility;
    }

    public String getValrepLandimpComp2RecAccessibilityDesc() {
        return NULL_STRING_SETTER(valrepLandimpComp2RecAccessibilityDesc);
    }

    public void setValrepLandimpComp2RecAccessibilityDesc(String valrepLandimpComp2RecAccessibilityDesc) {
        this.valrepLandimpComp2RecAccessibilityDesc = valrepLandimpComp2RecAccessibilityDesc;
    }

    public String getValrepLandimpComp2RecAccessibilityDescription() {
        return NULL_STRING_SETTER(valrepLandimpComp2RecAccessibilityDescription);
    }

    public void setValrepLandimpComp2RecAccessibilityDescription(String valrepLandimpComp2RecAccessibilityDescription) {
        this.valrepLandimpComp2RecAccessibilityDescription = valrepLandimpComp2RecAccessibilityDescription;
    }

    public String getValrepLandimpComp2RecAmenities() {
        return NULL_STRING_SETTER(valrepLandimpComp2RecAmenities);
    }

    public void setValrepLandimpComp2RecAmenities(String valrepLandimpComp2RecAmenities) {
        this.valrepLandimpComp2RecAmenities = valrepLandimpComp2RecAmenities;
    }

    public String getValrepLandimpComp2RecBargainingAllow() {
        return NULL_STRING_SETTER(valrepLandimpComp2RecBargainingAllow);
    }

    public void setValrepLandimpComp2RecBargainingAllow(String valrepLandimpComp2RecBargainingAllow) {
        this.valrepLandimpComp2RecBargainingAllow = valrepLandimpComp2RecBargainingAllow;
    }

    public String getValrepLandimpComp2RecBargainingAllowAdjustmentPrice() {
        return NULL_STRING_SETTER(valrepLandimpComp2RecBargainingAllowAdjustmentPrice);
    }

    public void setValrepLandimpComp2RecBargainingAllowAdjustmentPrice(String valrepLandimpComp2RecBargainingAllowAdjustmentPrice) {
        this.valrepLandimpComp2RecBargainingAllowAdjustmentPrice = valrepLandimpComp2RecBargainingAllowAdjustmentPrice;
    }

    public String getValrepLandimpComp2RecBargainingAllowDesc() {
        return NULL_STRING_SETTER(valrepLandimpComp2RecBargainingAllowDesc);
    }

    public void setValrepLandimpComp2RecBargainingAllowDesc(String valrepLandimpComp2RecBargainingAllowDesc) {
        this.valrepLandimpComp2RecBargainingAllowDesc = valrepLandimpComp2RecBargainingAllowDesc;
    }

    public String getValrepLandimpComp2RecBargainingAllowDescription() {
        return NULL_STRING_SETTER(valrepLandimpComp2RecBargainingAllowDescription);
    }

    public void setValrepLandimpComp2RecBargainingAllowDescription(String valrepLandimpComp2RecBargainingAllowDescription) {
        this.valrepLandimpComp2RecBargainingAllowDescription = valrepLandimpComp2RecBargainingAllowDescription;
    }

    public String getValrepLandimpComp2RecBargainingAllowance() {
        return NULL_STRING_SETTER(valrepLandimpComp2RecBargainingAllowance);
    }

    public void setValrepLandimpComp2RecBargainingAllowance(String valrepLandimpComp2RecBargainingAllowance) {
        this.valrepLandimpComp2RecBargainingAllowance = valrepLandimpComp2RecBargainingAllowance;
    }

    public String getValrepLandimpComp2RecBargainingAllowanceDesc() {
        return NULL_STRING_SETTER(valrepLandimpComp2RecBargainingAllowanceDesc);
    }

    public void setValrepLandimpComp2RecBargainingAllowanceDesc(String valrepLandimpComp2RecBargainingAllowanceDesc) {
        this.valrepLandimpComp2RecBargainingAllowanceDesc = valrepLandimpComp2RecBargainingAllowanceDesc;
    }

    public String getValrepLandimpComp2RecCornerInfluence() {
        return NULL_STRING_SETTER(valrepLandimpComp2RecCornerInfluence);
    }

    public void setValrepLandimpComp2RecCornerInfluence(String valrepLandimpComp2RecCornerInfluence) {
        this.valrepLandimpComp2RecCornerInfluence = valrepLandimpComp2RecCornerInfluence;
    }

    public String getValrepLandimpComp2RecDegreeOfDevt() {
        return NULL_STRING_SETTER(valrepLandimpComp2RecDegreeOfDevt);
    }

    public void setValrepLandimpComp2RecDegreeOfDevt(String valrepLandimpComp2RecDegreeOfDevt) {
        this.valrepLandimpComp2RecDegreeOfDevt = valrepLandimpComp2RecDegreeOfDevt;
    }

    public String getValrepLandimpComp2RecElevation() {
        return NULL_STRING_SETTER(valrepLandimpComp2RecElevation);
    }

    public void setValrepLandimpComp2RecElevation(String valrepLandimpComp2RecElevation) {
        this.valrepLandimpComp2RecElevation = valrepLandimpComp2RecElevation;
    }

    public String getValrepLandimpComp2RecElevationDesc() {
        return NULL_STRING_SETTER(valrepLandimpComp2RecElevationDesc);
    }

    public void setValrepLandimpComp2RecElevationDesc(String valrepLandimpComp2RecElevationDesc) {
        this.valrepLandimpComp2RecElevationDesc = valrepLandimpComp2RecElevationDesc;
    }

    public String getValrepLandimpComp2RecElevationDescription() {
        return NULL_STRING_SETTER(valrepLandimpComp2RecElevationDescription);
    }

    public void setValrepLandimpComp2RecElevationDescription(String valrepLandimpComp2RecElevationDescription) {
        this.valrepLandimpComp2RecElevationDescription = valrepLandimpComp2RecElevationDescription;
    }

    public String getValrepLandimpComp2RecLocation() {
        return NULL_STRING_SETTER(valrepLandimpComp2RecLocation);
    }

    public void setValrepLandimpComp2RecLocation(String valrepLandimpComp2RecLocation) {
        this.valrepLandimpComp2RecLocation = valrepLandimpComp2RecLocation;
    }

    public String getValrepLandimpComp2RecLocationDesc() {
        return NULL_STRING_SETTER(valrepLandimpComp2RecLocationDesc);
    }

    public void setValrepLandimpComp2RecLocationDesc(String valrepLandimpComp2RecLocationDesc) {
        this.valrepLandimpComp2RecLocationDesc = valrepLandimpComp2RecLocationDesc;
    }

    public String getValrepLandimpComp2RecLocationDescription() {
        return NULL_STRING_SETTER(valrepLandimpComp2RecLocationDescription);
    }

    public void setValrepLandimpComp2RecLocationDescription(String valrepLandimpComp2RecLocationDescription) {
        this.valrepLandimpComp2RecLocationDescription = valrepLandimpComp2RecLocationDescription;
    }

    public String getValrepLandimpComp2RecLotType() {
        return NULL_STRING_SETTER(valrepLandimpComp2RecLotType);
    }

    public void setValrepLandimpComp2RecLotType(String valrepLandimpComp2RecLotType) {
        this.valrepLandimpComp2RecLotType = valrepLandimpComp2RecLotType;
    }

    public String getValrepLandimpComp2RecLotTypeDesc() {
        return NULL_STRING_SETTER(valrepLandimpComp2RecLotTypeDesc);
    }

    public void setValrepLandimpComp2RecLotTypeDesc(String valrepLandimpComp2RecLotTypeDesc) {
        this.valrepLandimpComp2RecLotTypeDesc = valrepLandimpComp2RecLotTypeDesc;
    }

    public String getValrepLandimpComp2RecLotTypeDescription() {
        return NULL_STRING_SETTER(valrepLandimpComp2RecLotTypeDescription);
    }

    public void setValrepLandimpComp2RecLotTypeDescription(String valrepLandimpComp2RecLotTypeDescription) {
        this.valrepLandimpComp2RecLotTypeDescription = valrepLandimpComp2RecLotTypeDescription;
    }

    public String getValrepLandimpComp2RecNeighborhood() {
        return NULL_STRING_SETTER(valrepLandimpComp2RecNeighborhood);
    }

    public void setValrepLandimpComp2RecNeighborhood(String valrepLandimpComp2RecNeighborhood) {
        this.valrepLandimpComp2RecNeighborhood = valrepLandimpComp2RecNeighborhood;
    }

    public String getValrepLandimpComp2RecNeighborhoodDesc() {
        return NULL_STRING_SETTER(valrepLandimpComp2RecNeighborhoodDesc);
    }

    public void setValrepLandimpComp2RecNeighborhoodDesc(String valrepLandimpComp2RecNeighborhoodDesc) {
        this.valrepLandimpComp2RecNeighborhoodDesc = valrepLandimpComp2RecNeighborhoodDesc;
    }

    public String getValrepLandimpComp2RecNeighborhoodDescription() {
        return NULL_STRING_SETTER(valrepLandimpComp2RecNeighborhoodDescription);
    }

    public void setValrepLandimpComp2RecNeighborhoodDescription(String valrepLandimpComp2RecNeighborhoodDescription) {
        this.valrepLandimpComp2RecNeighborhoodDescription = valrepLandimpComp2RecNeighborhoodDescription;
    }

    public String getValrepLandimpComp2RecNeighborhoodType() {
        return NULL_STRING_SETTER(valrepLandimpComp2RecNeighborhoodType);
    }

    public void setValrepLandimpComp2RecNeighborhoodType(String valrepLandimpComp2RecNeighborhoodType) {
        this.valrepLandimpComp2RecNeighborhoodType = valrepLandimpComp2RecNeighborhoodType;
    }

    public String getValrepLandimpComp2RecOthers() {
        return NULL_STRING_SETTER(valrepLandimpComp2RecOthers);
    }

    public void setValrepLandimpComp2RecOthers(String valrepLandimpComp2RecOthers) {
        this.valrepLandimpComp2RecOthers = valrepLandimpComp2RecOthers;
    }

    public String getValrepLandimpComp2RecOthers2() {
        return NULL_STRING_SETTER(valrepLandimpComp2RecOthers2);
    }

    public void setValrepLandimpComp2RecOthers2(String valrepLandimpComp2RecOthers2) {
        this.valrepLandimpComp2RecOthers2 = valrepLandimpComp2RecOthers2;
    }

    public String getValrepLandimpComp2RecOthers2Desc() {
        return NULL_STRING_SETTER(valrepLandimpComp2RecOthers2Desc);
    }

    public void setValrepLandimpComp2RecOthers2Desc(String valrepLandimpComp2RecOthers2Desc) {
        this.valrepLandimpComp2RecOthers2Desc = valrepLandimpComp2RecOthers2Desc;
    }

    public String getValrepLandimpComp2RecOthers2Description() {
        return NULL_STRING_SETTER(valrepLandimpComp2RecOthers2Description);
    }

    public void setValrepLandimpComp2RecOthers2Description(String valrepLandimpComp2RecOthers2Description) {
        this.valrepLandimpComp2RecOthers2Description = valrepLandimpComp2RecOthers2Description;
    }

    public String getValrepLandimpComp2RecOthers3() {
        return NULL_STRING_SETTER(valrepLandimpComp2RecOthers3);
    }

    public void setValrepLandimpComp2RecOthers3(String valrepLandimpComp2RecOthers3) {
        this.valrepLandimpComp2RecOthers3 = valrepLandimpComp2RecOthers3;
    }

    public String getValrepLandimpComp2RecOthers3Desc() {
        return NULL_STRING_SETTER(valrepLandimpComp2RecOthers3Desc);
    }

    public void setValrepLandimpComp2RecOthers3Desc(String valrepLandimpComp2RecOthers3Desc) {
        this.valrepLandimpComp2RecOthers3Desc = valrepLandimpComp2RecOthers3Desc;
    }

    public String getValrepLandimpComp2RecOthers3Description() {
        return NULL_STRING_SETTER(valrepLandimpComp2RecOthers3Description);
    }

    public void setValrepLandimpComp2RecOthers3Description(String valrepLandimpComp2RecOthers3Description) {
        this.valrepLandimpComp2RecOthers3Description = valrepLandimpComp2RecOthers3Description;
    }

    public String getValrepLandimpComp2RecOthersDesc() {
        return NULL_STRING_SETTER(valrepLandimpComp2RecOthersDesc);
    }

    public void setValrepLandimpComp2RecOthersDesc(String valrepLandimpComp2RecOthersDesc) {
        this.valrepLandimpComp2RecOthersDesc = valrepLandimpComp2RecOthersDesc;
    }

    public String getValrepLandimpComp2RecOthersDescription() {
        return NULL_STRING_SETTER(valrepLandimpComp2RecOthersDescription);
    }

    public void setValrepLandimpComp2RecOthersDescription(String valrepLandimpComp2RecOthersDescription) {
        this.valrepLandimpComp2RecOthersDescription = valrepLandimpComp2RecOthersDescription;
    }

    public String getValrepLandimpComp2RecOthersName() {
        return NULL_STRING_SETTER(valrepLandimpComp2RecOthersName);
    }

    public void setValrepLandimpComp2RecOthersName(String valrepLandimpComp2RecOthersName) {
        this.valrepLandimpComp2RecOthersName = valrepLandimpComp2RecOthersName;
    }

    public String getValrepLandimpComp2RecPropRights() {
        return NULL_STRING_SETTER(valrepLandimpComp2RecPropRights);
    }

    public void setValrepLandimpComp2RecPropRights(String valrepLandimpComp2RecPropRights) {
        this.valrepLandimpComp2RecPropRights = valrepLandimpComp2RecPropRights;
    }

    public String getValrepLandimpComp2RecPropRightsDesc() {
        return NULL_STRING_SETTER(valrepLandimpComp2RecPropRightsDesc);
    }

    public void setValrepLandimpComp2RecPropRightsDesc(String valrepLandimpComp2RecPropRightsDesc) {
        this.valrepLandimpComp2RecPropRightsDesc = valrepLandimpComp2RecPropRightsDesc;
    }

    public String getValrepLandimpComp2RecPropertyRightsConveyed() {
        return NULL_STRING_SETTER(valrepLandimpComp2RecPropertyRightsConveyed);
    }

    public void setValrepLandimpComp2RecPropertyRightsConveyed(String valrepLandimpComp2RecPropertyRightsConveyed) {
        this.valrepLandimpComp2RecPropertyRightsConveyed = valrepLandimpComp2RecPropertyRightsConveyed;
    }

    public String getValrepLandimpComp2RecPropertyRightsConveyedAdjustmentPrice() {
        return NULL_STRING_SETTER(valrepLandimpComp2RecPropertyRightsConveyedAdjustmentPrice);
    }

    public void setValrepLandimpComp2RecPropertyRightsConveyedAdjustmentPrice(String valrepLandimpComp2RecPropertyRightsConveyedAdjustmentPrice) {
        this.valrepLandimpComp2RecPropertyRightsConveyedAdjustmentPrice = valrepLandimpComp2RecPropertyRightsConveyedAdjustmentPrice;
    }

    public String getValrepLandimpComp2RecPropertyRightsConveyedDesc() {
        return NULL_STRING_SETTER(valrepLandimpComp2RecPropertyRightsConveyedDesc);
    }

    public void setValrepLandimpComp2RecPropertyRightsConveyedDesc(String valrepLandimpComp2RecPropertyRightsConveyedDesc) {
        this.valrepLandimpComp2RecPropertyRightsConveyedDesc = valrepLandimpComp2RecPropertyRightsConveyedDesc;
    }

    public String getValrepLandimpComp2RecPropertyRightsConveyedDescription() {
        return NULL_STRING_SETTER(valrepLandimpComp2RecPropertyRightsConveyedDescription);
    }

    public void setValrepLandimpComp2RecPropertyRightsConveyedDescription(String valrepLandimpComp2RecPropertyRightsConveyedDescription) {
        this.valrepLandimpComp2RecPropertyRightsConveyedDescription = valrepLandimpComp2RecPropertyRightsConveyedDescription;
    }

    public String getValrepLandimpComp2RecRoad() {
        return NULL_STRING_SETTER(valrepLandimpComp2RecRoad);
    }

    public void setValrepLandimpComp2RecRoad(String valrepLandimpComp2RecRoad) {
        this.valrepLandimpComp2RecRoad = valrepLandimpComp2RecRoad;
    }

    public String getValrepLandimpComp2RecRoadDesc() {
        return NULL_STRING_SETTER(valrepLandimpComp2RecRoadDesc);
    }

    public void setValrepLandimpComp2RecRoadDesc(String valrepLandimpComp2RecRoadDesc) {
        this.valrepLandimpComp2RecRoadDesc = valrepLandimpComp2RecRoadDesc;
    }

    public String getValrepLandimpComp2RecRoadDescription() {
        return NULL_STRING_SETTER(valrepLandimpComp2RecRoadDescription);
    }

    public void setValrepLandimpComp2RecRoadDescription(String valrepLandimpComp2RecRoadDescription) {
        this.valrepLandimpComp2RecRoadDescription = valrepLandimpComp2RecRoadDescription;
    }

    public String getValrepLandimpComp2RecShape() {
        return NULL_STRING_SETTER(valrepLandimpComp2RecShape);
    }

    public void setValrepLandimpComp2RecShape(String valrepLandimpComp2RecShape) {
        this.valrepLandimpComp2RecShape = valrepLandimpComp2RecShape;
    }

    public String getValrepLandimpComp2RecShapeDesc() {
        return NULL_STRING_SETTER(valrepLandimpComp2RecShapeDesc);
    }

    public void setValrepLandimpComp2RecShapeDesc(String valrepLandimpComp2RecShapeDesc) {
        this.valrepLandimpComp2RecShapeDesc = valrepLandimpComp2RecShapeDesc;
    }

    public String getValrepLandimpComp2RecShapeDescription() {
        return NULL_STRING_SETTER(valrepLandimpComp2RecShapeDescription);
    }

    public void setValrepLandimpComp2RecShapeDescription(String valrepLandimpComp2RecShapeDescription) {
        this.valrepLandimpComp2RecShapeDescription = valrepLandimpComp2RecShapeDescription;
    }

    public String getValrepLandimpComp2RecSize() {
        return NULL_STRING_SETTER(valrepLandimpComp2RecSize);
    }

    public void setValrepLandimpComp2RecSize(String valrepLandimpComp2RecSize) {
        this.valrepLandimpComp2RecSize = valrepLandimpComp2RecSize;
    }

    public String getValrepLandimpComp2RecSizeDesc() {
        return NULL_STRING_SETTER(valrepLandimpComp2RecSizeDesc);
    }

    public void setValrepLandimpComp2RecSizeDesc(String valrepLandimpComp2RecSizeDesc) {
        this.valrepLandimpComp2RecSizeDesc = valrepLandimpComp2RecSizeDesc;
    }

    public String getValrepLandimpComp2RecSizeDescription() {
        return NULL_STRING_SETTER(valrepLandimpComp2RecSizeDescription);
    }

    public void setValrepLandimpComp2RecSizeDescription(String valrepLandimpComp2RecSizeDescription) {
        this.valrepLandimpComp2RecSizeDescription = valrepLandimpComp2RecSizeDescription;
    }

    public String getValrepLandimpComp2RecTerrain() {
        return NULL_STRING_SETTER(valrepLandimpComp2RecTerrain);
    }

    public void setValrepLandimpComp2RecTerrain(String valrepLandimpComp2RecTerrain) {
        this.valrepLandimpComp2RecTerrain = valrepLandimpComp2RecTerrain;
    }

    public String getValrepLandimpComp2RecTerrainDesc() {
        return NULL_STRING_SETTER(valrepLandimpComp2RecTerrainDesc);
    }

    public void setValrepLandimpComp2RecTerrainDesc(String valrepLandimpComp2RecTerrainDesc) {
        this.valrepLandimpComp2RecTerrainDesc = valrepLandimpComp2RecTerrainDesc;
    }

    public String getValrepLandimpComp2RecTerrainDescription() {
        return NULL_STRING_SETTER(valrepLandimpComp2RecTerrainDescription);
    }

    public void setValrepLandimpComp2RecTerrainDescription(String valrepLandimpComp2RecTerrainDescription) {
        this.valrepLandimpComp2RecTerrainDescription = valrepLandimpComp2RecTerrainDescription;
    }

    public String getValrepLandimpComp2RecTime() {
        return NULL_STRING_SETTER(valrepLandimpComp2RecTime);
    }

    public void setValrepLandimpComp2RecTime(String valrepLandimpComp2RecTime) {
        this.valrepLandimpComp2RecTime = valrepLandimpComp2RecTime;
    }

    public String getValrepLandimpComp2RecTimeAdjustmentPrice() {
        return NULL_STRING_SETTER(valrepLandimpComp2RecTimeAdjustmentPrice);
    }

    public void setValrepLandimpComp2RecTimeAdjustmentPrice(String valrepLandimpComp2RecTimeAdjustmentPrice) {
        this.valrepLandimpComp2RecTimeAdjustmentPrice = valrepLandimpComp2RecTimeAdjustmentPrice;
    }

    public String getValrepLandimpComp2RecTimeDesc() {
        return NULL_STRING_SETTER(valrepLandimpComp2RecTimeDesc);
    }

    public void setValrepLandimpComp2RecTimeDesc(String valrepLandimpComp2RecTimeDesc) {
        this.valrepLandimpComp2RecTimeDesc = valrepLandimpComp2RecTimeDesc;
    }

    public String getValrepLandimpComp2RecTimeDescription() {
        return NULL_STRING_SETTER(valrepLandimpComp2RecTimeDescription);
    }

    public void setValrepLandimpComp2RecTimeDescription(String valrepLandimpComp2RecTimeDescription) {
        this.valrepLandimpComp2RecTimeDescription = valrepLandimpComp2RecTimeDescription;
    }

    public String getValrepLandimpComp2RecTimeElement() {
        return NULL_STRING_SETTER(valrepLandimpComp2RecTimeElement);
    }

    public void setValrepLandimpComp2RecTimeElement(String valrepLandimpComp2RecTimeElement) {
        this.valrepLandimpComp2RecTimeElement = valrepLandimpComp2RecTimeElement;
    }

    public String getValrepLandimpComp2RecTimeElementDesc() {
        return NULL_STRING_SETTER(valrepLandimpComp2RecTimeElementDesc);
    }

    public void setValrepLandimpComp2RecTimeElementDesc(String valrepLandimpComp2RecTimeElementDesc) {
        this.valrepLandimpComp2RecTimeElementDesc = valrepLandimpComp2RecTimeElementDesc;
    }

    public String getValrepLandimpComp2RecTruLot() {
        return NULL_STRING_SETTER(valrepLandimpComp2RecTruLot);
    }

    public void setValrepLandimpComp2RecTruLot(String valrepLandimpComp2RecTruLot) {
        this.valrepLandimpComp2RecTruLot = valrepLandimpComp2RecTruLot;
    }

    public String getValrepLandimpComp2Remarks() {
        return NULL_STRING_SETTER(valrepLandimpComp2Remarks);
    }

    public void setValrepLandimpComp2Remarks(String valrepLandimpComp2Remarks) {
        this.valrepLandimpComp2Remarks = valrepLandimpComp2Remarks;
    }

    public String getValrepLandimpComp2SellingPrice() {
        return NULL_STRING_SETTER(valrepLandimpComp2SellingPrice);
    }

    public void setValrepLandimpComp2SellingPrice(String valrepLandimpComp2SellingPrice) {
        this.valrepLandimpComp2SellingPrice = valrepLandimpComp2SellingPrice;
    }

    public String getValrepLandimpComp2Shape() {
        return NULL_STRING_SETTER(valrepLandimpComp2Shape);
    }

    public void setValrepLandimpComp2Shape(String valrepLandimpComp2Shape) {
        this.valrepLandimpComp2Shape = valrepLandimpComp2Shape;
    }

    public String getValrepLandimpComp2Source() {
        return NULL_STRING_SETTER(valrepLandimpComp2Source);
    }

    public void setValrepLandimpComp2Source(String valrepLandimpComp2Source) {
        this.valrepLandimpComp2Source = valrepLandimpComp2Source;
    }

    public String getValrepLandimpComp2TotalAdj() {
        return NULL_STRING_SETTER(valrepLandimpComp2TotalAdj);
    }

    public void setValrepLandimpComp2TotalAdj(String valrepLandimpComp2TotalAdj) {
        this.valrepLandimpComp2TotalAdj = valrepLandimpComp2TotalAdj;
    }

    public String getValrepLandimpComp2TotalgrossAdj() {
        return NULL_STRING_SETTER(valrepLandimpComp2TotalgrossAdj);
    }

    public void setValrepLandimpComp2TotalgrossAdj(String valrepLandimpComp2TotalgrossAdj) {
        this.valrepLandimpComp2TotalgrossAdj = valrepLandimpComp2TotalgrossAdj;
    }

    public String getValrepLandimpComp2TotalnetAdj() {
        return NULL_STRING_SETTER(valrepLandimpComp2TotalnetAdj);
    }

    public void setValrepLandimpComp2TotalnetAdj(String valrepLandimpComp2TotalnetAdj) {
        this.valrepLandimpComp2TotalnetAdj = valrepLandimpComp2TotalnetAdj;
    }

    public String getValrepLandimpComp2Weight() {
        return NULL_STRING_SETTER(valrepLandimpComp2Weight);
    }

    public void setValrepLandimpComp2Weight(String valrepLandimpComp2Weight) {
        this.valrepLandimpComp2Weight = valrepLandimpComp2Weight;
    }

    public String getValrepLandimpComp2WeightEquivalent() {
        return NULL_STRING_SETTER(valrepLandimpComp2WeightEquivalent);
    }

    public void setValrepLandimpComp2WeightEquivalent(String valrepLandimpComp2WeightEquivalent) {
        this.valrepLandimpComp2WeightEquivalent = valrepLandimpComp2WeightEquivalent;
    }

    public String getValrepLandimpComp2WeightedValue() {
        return NULL_STRING_SETTER(valrepLandimpComp2WeightedValue);
    }

    public void setValrepLandimpComp2WeightedValue(String valrepLandimpComp2WeightedValue) {
        this.valrepLandimpComp2WeightedValue = valrepLandimpComp2WeightedValue;
    }

    public String getValrepLandimpComp2WgtDist() {
        return NULL_STRING_SETTER(valrepLandimpComp2WgtDist);
    }

    public void setValrepLandimpComp2WgtDist(String valrepLandimpComp2WgtDist) {
        this.valrepLandimpComp2WgtDist = valrepLandimpComp2WgtDist;
    }

    public String getValrepLandimpComp2Zoning() {
        return NULL_STRING_SETTER(valrepLandimpComp2Zoning);
    }

    public void setValrepLandimpComp2Zoning(String valrepLandimpComp2Zoning) {
        this.valrepLandimpComp2Zoning = valrepLandimpComp2Zoning;
    }

    public String getValrepLandimpComp3AdjustablePriceSqm() {
        return NULL_STRING_SETTER(valrepLandimpComp3AdjustablePriceSqm);
    }

    public void setValrepLandimpComp3AdjustablePriceSqm(String valrepLandimpComp3AdjustablePriceSqm) {
        this.valrepLandimpComp3AdjustablePriceSqm = valrepLandimpComp3AdjustablePriceSqm;
    }

    public String getValrepLandimpComp3AdjustedValue() {
        return NULL_STRING_SETTER(valrepLandimpComp3AdjustedValue);
    }

    public void setValrepLandimpComp3AdjustedValue(String valrepLandimpComp3AdjustedValue) {
        this.valrepLandimpComp3AdjustedValue = valrepLandimpComp3AdjustedValue;
    }

    public String getValrepLandimpComp3ApproxAge() {
        return NULL_STRING_SETTER(valrepLandimpComp3ApproxAge);
    }

    public void setValrepLandimpComp3ApproxAge(String valrepLandimpComp3ApproxAge) {
        this.valrepLandimpComp3ApproxAge = valrepLandimpComp3ApproxAge;
    }

    public String getValrepLandimpComp3Area() {
        return NULL_STRING_SETTER(valrepLandimpComp3Area);
    }

    public void setValrepLandimpComp3Area(String valrepLandimpComp3Area) {
        this.valrepLandimpComp3Area = valrepLandimpComp3Area;
    }

    public String getValrepLandimpComp3BasePrice() {
        return NULL_STRING_SETTER(valrepLandimpComp3BasePrice);
    }

    public void setValrepLandimpComp3BasePrice(String valrepLandimpComp3BasePrice) {
        this.valrepLandimpComp3BasePrice = valrepLandimpComp3BasePrice;
    }

    public String getValrepLandimpComp3BasePriceSqm() {
        return NULL_STRING_SETTER(valrepLandimpComp3BasePriceSqm);
    }

    public void setValrepLandimpComp3BasePriceSqm(String valrepLandimpComp3BasePriceSqm) {
        this.valrepLandimpComp3BasePriceSqm = valrepLandimpComp3BasePriceSqm;
    }

    public String getValrepLandimpComp3BldgArea() {
        return NULL_STRING_SETTER(valrepLandimpComp3BldgArea);
    }

    public void setValrepLandimpComp3BldgArea(String valrepLandimpComp3BldgArea) {
        this.valrepLandimpComp3BldgArea = valrepLandimpComp3BldgArea;
    }

    public String getValrepLandimpComp3BldgDepreciation() {
        return NULL_STRING_SETTER(valrepLandimpComp3BldgDepreciation);
    }

    public void setValrepLandimpComp3BldgDepreciation(String valrepLandimpComp3BldgDepreciation) {
        this.valrepLandimpComp3BldgDepreciation = valrepLandimpComp3BldgDepreciation;
    }

    public String getValrepLandimpComp3BldgDepreciationVal() {
        return NULL_STRING_SETTER(valrepLandimpComp3BldgDepreciationVal);
    }

    public void setValrepLandimpComp3BldgDepreciationVal(String valrepLandimpComp3BldgDepreciationVal) {
        this.valrepLandimpComp3BldgDepreciationVal = valrepLandimpComp3BldgDepreciationVal;
    }

    public String getValrepLandimpComp3BldgReplacementUnitValue() {
        return NULL_STRING_SETTER(valrepLandimpComp3BldgReplacementUnitValue);
    }

    public void setValrepLandimpComp3BldgReplacementUnitValue(String valrepLandimpComp3BldgReplacementUnitValue) {
        this.valrepLandimpComp3BldgReplacementUnitValue = valrepLandimpComp3BldgReplacementUnitValue;
    }

    public String getValrepLandimpComp3BldgReplacementValue() {
        return NULL_STRING_SETTER(valrepLandimpComp3BldgReplacementValue);
    }

    public void setValrepLandimpComp3BldgReplacementValue(String valrepLandimpComp3BldgReplacementValue) {
        this.valrepLandimpComp3BldgReplacementValue = valrepLandimpComp3BldgReplacementValue;
    }

    public String getValrepLandimpComp3Characteristic() {
        return NULL_STRING_SETTER(valrepLandimpComp3Characteristic);
    }

    public void setValrepLandimpComp3Characteristic(String valrepLandimpComp3Characteristic) {
        this.valrepLandimpComp3Characteristic = valrepLandimpComp3Characteristic;
    }

    public String getValrepLandimpComp3Characteristics() {
        return NULL_STRING_SETTER(valrepLandimpComp3Characteristics);
    }

    public void setValrepLandimpComp3Characteristics(String valrepLandimpComp3Characteristics) {
        this.valrepLandimpComp3Characteristics = valrepLandimpComp3Characteristics;
    }

    public String getValrepLandimpComp3ConcludedAdjustment() {
        return NULL_STRING_SETTER(valrepLandimpComp3ConcludedAdjustment);
    }

    public void setValrepLandimpComp3ConcludedAdjustment(String valrepLandimpComp3ConcludedAdjustment) {
        this.valrepLandimpComp3ConcludedAdjustment = valrepLandimpComp3ConcludedAdjustment;
    }

    public String getValrepLandimpComp3ContactNo() {
        return NULL_STRING_SETTER(valrepLandimpComp3ContactNo);
    }

    public void setValrepLandimpComp3ContactNo(String valrepLandimpComp3ContactNo) {
        this.valrepLandimpComp3ContactNo = valrepLandimpComp3ContactNo;
    }

    public String getValrepLandimpComp3DateDay() {
        return NULL_STRING_SETTER(valrepLandimpComp3DateDay);
    }

    public void setValrepLandimpComp3DateDay(String valrepLandimpComp3DateDay) {
        this.valrepLandimpComp3DateDay = valrepLandimpComp3DateDay;
    }

    public String getValrepLandimpComp3DateMonth() {
        return NULL_STRING_SETTER(valrepLandimpComp3DateMonth);
    }

    public void setValrepLandimpComp3DateMonth(String valrepLandimpComp3DateMonth) {
        this.valrepLandimpComp3DateMonth = valrepLandimpComp3DateMonth;
    }

    public String getValrepLandimpComp3DateYear() {
        return NULL_STRING_SETTER(valrepLandimpComp3DateYear);
    }

    public void setValrepLandimpComp3DateYear(String valrepLandimpComp3DateYear) {
        this.valrepLandimpComp3DateYear = valrepLandimpComp3DateYear;
    }

    public String getValrepLandimpComp3DiscountRate() {
        return NULL_STRING_SETTER(valrepLandimpComp3DiscountRate);
    }

    public void setValrepLandimpComp3DiscountRate(String valrepLandimpComp3DiscountRate) {
        this.valrepLandimpComp3DiscountRate = valrepLandimpComp3DiscountRate;
    }

    public String getValrepLandimpComp3Discounts() {
        return NULL_STRING_SETTER(valrepLandimpComp3Discounts);
    }

    public void setValrepLandimpComp3Discounts(String valrepLandimpComp3Discounts) {
        this.valrepLandimpComp3Discounts = valrepLandimpComp3Discounts;
    }

    public String getValrepLandimpComp3EffectiveDate() {
        return NULL_STRING_SETTER(valrepLandimpComp3EffectiveDate);
    }

    public void setValrepLandimpComp3EffectiveDate(String valrepLandimpComp3EffectiveDate) {
        this.valrepLandimpComp3EffectiveDate = valrepLandimpComp3EffectiveDate;
    }

    public String getValrepLandimpComp3LandResidualValue() {
        return NULL_STRING_SETTER(valrepLandimpComp3LandResidualValue);
    }

    public void setValrepLandimpComp3LandResidualValue(String valrepLandimpComp3LandResidualValue) {
        this.valrepLandimpComp3LandResidualValue = valrepLandimpComp3LandResidualValue;
    }

    public String getValrepLandimpComp3Location() {
        return NULL_STRING_SETTER(valrepLandimpComp3Location);
    }

    public void setValrepLandimpComp3Location(String valrepLandimpComp3Location) {
        this.valrepLandimpComp3Location = valrepLandimpComp3Location;
    }

    public String getValrepLandimpComp3NetBldgValue() {
        return NULL_STRING_SETTER(valrepLandimpComp3NetBldgValue);
    }

    public void setValrepLandimpComp3NetBldgValue(String valrepLandimpComp3NetBldgValue) {
        this.valrepLandimpComp3NetBldgValue = valrepLandimpComp3NetBldgValue;
    }

    public String getValrepLandimpComp3NetLandValuation() {
        return NULL_STRING_SETTER(valrepLandimpComp3NetLandValuation);
    }

    public void setValrepLandimpComp3NetLandValuation(String valrepLandimpComp3NetLandValuation) {
        this.valrepLandimpComp3NetLandValuation = valrepLandimpComp3NetLandValuation;
    }

    public String getValrepLandimpComp3PriceSqm() {
        return NULL_STRING_SETTER(valrepLandimpComp3PriceSqm);
    }

    public void setValrepLandimpComp3PriceSqm(String valrepLandimpComp3PriceSqm) {
        this.valrepLandimpComp3PriceSqm = valrepLandimpComp3PriceSqm;
    }

    public String getValrepLandimpComp3PriceValue() {
        return NULL_STRING_SETTER(valrepLandimpComp3PriceValue);
    }

    public void setValrepLandimpComp3PriceValue(String valrepLandimpComp3PriceValue) {
        this.valrepLandimpComp3PriceValue = valrepLandimpComp3PriceValue;
    }

    public String getValrepLandimpComp3PricingCircumstance() {
        return NULL_STRING_SETTER(valrepLandimpComp3PricingCircumstance);
    }

    public void setValrepLandimpComp3PricingCircumstance(String valrepLandimpComp3PricingCircumstance) {
        this.valrepLandimpComp3PricingCircumstance = valrepLandimpComp3PricingCircumstance;
    }

    public String getValrepLandimpComp3PropInterest() {
        return NULL_STRING_SETTER(valrepLandimpComp3PropInterest);
    }

    public void setValrepLandimpComp3PropInterest(String valrepLandimpComp3PropInterest) {
        this.valrepLandimpComp3PropInterest = valrepLandimpComp3PropInterest;
    }

    public String getValrepLandimpComp3PropertyType() {
        return NULL_STRING_SETTER(valrepLandimpComp3PropertyType);
    }

    public void setValrepLandimpComp3PropertyType(String valrepLandimpComp3PropertyType) {
        this.valrepLandimpComp3PropertyType = valrepLandimpComp3PropertyType;
    }

    public String getValrepLandimpComp3RecAccessibility() {
        return NULL_STRING_SETTER(valrepLandimpComp3RecAccessibility);
    }

    public void setValrepLandimpComp3RecAccessibility(String valrepLandimpComp3RecAccessibility) {
        this.valrepLandimpComp3RecAccessibility = valrepLandimpComp3RecAccessibility;
    }

    public String getValrepLandimpComp3RecAccessibilityDesc() {
        return NULL_STRING_SETTER(valrepLandimpComp3RecAccessibilityDesc);
    }

    public void setValrepLandimpComp3RecAccessibilityDesc(String valrepLandimpComp3RecAccessibilityDesc) {
        this.valrepLandimpComp3RecAccessibilityDesc = valrepLandimpComp3RecAccessibilityDesc;
    }

    public String getValrepLandimpComp3RecAccessibilityDescription() {
        return NULL_STRING_SETTER(valrepLandimpComp3RecAccessibilityDescription);
    }

    public void setValrepLandimpComp3RecAccessibilityDescription(String valrepLandimpComp3RecAccessibilityDescription) {
        this.valrepLandimpComp3RecAccessibilityDescription = valrepLandimpComp3RecAccessibilityDescription;
    }

    public String getValrepLandimpComp3RecAmenities() {
        return NULL_STRING_SETTER(valrepLandimpComp3RecAmenities);
    }

    public void setValrepLandimpComp3RecAmenities(String valrepLandimpComp3RecAmenities) {
        this.valrepLandimpComp3RecAmenities = valrepLandimpComp3RecAmenities;
    }

    public String getValrepLandimpComp3RecBargainingAllow() {
        return NULL_STRING_SETTER(valrepLandimpComp3RecBargainingAllow);
    }

    public void setValrepLandimpComp3RecBargainingAllow(String valrepLandimpComp3RecBargainingAllow) {
        this.valrepLandimpComp3RecBargainingAllow = valrepLandimpComp3RecBargainingAllow;
    }

    public String getValrepLandimpComp3RecBargainingAllowAdjustmentPrice() {
        return NULL_STRING_SETTER(valrepLandimpComp3RecBargainingAllowAdjustmentPrice);
    }

    public void setValrepLandimpComp3RecBargainingAllowAdjustmentPrice(String valrepLandimpComp3RecBargainingAllowAdjustmentPrice) {
        this.valrepLandimpComp3RecBargainingAllowAdjustmentPrice = valrepLandimpComp3RecBargainingAllowAdjustmentPrice;
    }

    public String getValrepLandimpComp3RecBargainingAllowDesc() {
        return NULL_STRING_SETTER(valrepLandimpComp3RecBargainingAllowDesc);
    }

    public void setValrepLandimpComp3RecBargainingAllowDesc(String valrepLandimpComp3RecBargainingAllowDesc) {
        this.valrepLandimpComp3RecBargainingAllowDesc = valrepLandimpComp3RecBargainingAllowDesc;
    }

    public String getValrepLandimpComp3RecBargainingAllowDescription() {
        return NULL_STRING_SETTER(valrepLandimpComp3RecBargainingAllowDescription);
    }

    public void setValrepLandimpComp3RecBargainingAllowDescription(String valrepLandimpComp3RecBargainingAllowDescription) {
        this.valrepLandimpComp3RecBargainingAllowDescription = valrepLandimpComp3RecBargainingAllowDescription;
    }

    public String getValrepLandimpComp3RecBargainingAllowance() {
        return NULL_STRING_SETTER(valrepLandimpComp3RecBargainingAllowance);
    }

    public void setValrepLandimpComp3RecBargainingAllowance(String valrepLandimpComp3RecBargainingAllowance) {
        this.valrepLandimpComp3RecBargainingAllowance = valrepLandimpComp3RecBargainingAllowance;
    }

    public String getValrepLandimpComp3RecBargainingAllowanceDesc() {
        return NULL_STRING_SETTER(valrepLandimpComp3RecBargainingAllowanceDesc);
    }

    public void setValrepLandimpComp3RecBargainingAllowanceDesc(String valrepLandimpComp3RecBargainingAllowanceDesc) {
        this.valrepLandimpComp3RecBargainingAllowanceDesc = valrepLandimpComp3RecBargainingAllowanceDesc;
    }

    public String getValrepLandimpComp3RecCornerInfluence() {
        return NULL_STRING_SETTER(valrepLandimpComp3RecCornerInfluence);
    }

    public void setValrepLandimpComp3RecCornerInfluence(String valrepLandimpComp3RecCornerInfluence) {
        this.valrepLandimpComp3RecCornerInfluence = valrepLandimpComp3RecCornerInfluence;
    }

    public String getValrepLandimpComp3RecDegreeOfDevt() {
        return NULL_STRING_SETTER(valrepLandimpComp3RecDegreeOfDevt);
    }

    public void setValrepLandimpComp3RecDegreeOfDevt(String valrepLandimpComp3RecDegreeOfDevt) {
        this.valrepLandimpComp3RecDegreeOfDevt = valrepLandimpComp3RecDegreeOfDevt;
    }

    public String getValrepLandimpComp3RecElevation() {
        return NULL_STRING_SETTER(valrepLandimpComp3RecElevation);
    }

    public void setValrepLandimpComp3RecElevation(String valrepLandimpComp3RecElevation) {
        this.valrepLandimpComp3RecElevation = valrepLandimpComp3RecElevation;
    }

    public String getValrepLandimpComp3RecElevationDesc() {
        return NULL_STRING_SETTER(valrepLandimpComp3RecElevationDesc);
    }

    public void setValrepLandimpComp3RecElevationDesc(String valrepLandimpComp3RecElevationDesc) {
        this.valrepLandimpComp3RecElevationDesc = valrepLandimpComp3RecElevationDesc;
    }

    public String getValrepLandimpComp3RecElevationDescription() {
        return NULL_STRING_SETTER(valrepLandimpComp3RecElevationDescription);
    }

    public void setValrepLandimpComp3RecElevationDescription(String valrepLandimpComp3RecElevationDescription) {
        this.valrepLandimpComp3RecElevationDescription = valrepLandimpComp3RecElevationDescription;
    }

    public String getValrepLandimpComp3RecLocation() {
        return NULL_STRING_SETTER(valrepLandimpComp3RecLocation);
    }

    public void setValrepLandimpComp3RecLocation(String valrepLandimpComp3RecLocation) {
        this.valrepLandimpComp3RecLocation = valrepLandimpComp3RecLocation;
    }

    public String getValrepLandimpComp3RecLocationDesc() {
        return NULL_STRING_SETTER(valrepLandimpComp3RecLocationDesc);
    }

    public void setValrepLandimpComp3RecLocationDesc(String valrepLandimpComp3RecLocationDesc) {
        this.valrepLandimpComp3RecLocationDesc = valrepLandimpComp3RecLocationDesc;
    }

    public String getValrepLandimpComp3RecLocationDescription() {
        return NULL_STRING_SETTER(valrepLandimpComp3RecLocationDescription);
    }

    public void setValrepLandimpComp3RecLocationDescription(String valrepLandimpComp3RecLocationDescription) {
        this.valrepLandimpComp3RecLocationDescription = valrepLandimpComp3RecLocationDescription;
    }

    public String getValrepLandimpComp3RecLotType() {
        return NULL_STRING_SETTER(valrepLandimpComp3RecLotType);
    }

    public void setValrepLandimpComp3RecLotType(String valrepLandimpComp3RecLotType) {
        this.valrepLandimpComp3RecLotType = valrepLandimpComp3RecLotType;
    }

    public String getValrepLandimpComp3RecLotTypeDesc() {
        return NULL_STRING_SETTER(valrepLandimpComp3RecLotTypeDesc);
    }

    public void setValrepLandimpComp3RecLotTypeDesc(String valrepLandimpComp3RecLotTypeDesc) {
        this.valrepLandimpComp3RecLotTypeDesc = valrepLandimpComp3RecLotTypeDesc;
    }

    public String getValrepLandimpComp3RecLotTypeDescription() {
        return NULL_STRING_SETTER(valrepLandimpComp3RecLotTypeDescription);
    }

    public void setValrepLandimpComp3RecLotTypeDescription(String valrepLandimpComp3RecLotTypeDescription) {
        this.valrepLandimpComp3RecLotTypeDescription = valrepLandimpComp3RecLotTypeDescription;
    }

    public String getValrepLandimpComp3RecNeighborhood() {
        return NULL_STRING_SETTER(valrepLandimpComp3RecNeighborhood);
    }

    public void setValrepLandimpComp3RecNeighborhood(String valrepLandimpComp3RecNeighborhood) {
        this.valrepLandimpComp3RecNeighborhood = valrepLandimpComp3RecNeighborhood;
    }

    public String getValrepLandimpComp3RecNeighborhoodDesc() {
        return NULL_STRING_SETTER(valrepLandimpComp3RecNeighborhoodDesc);
    }

    public void setValrepLandimpComp3RecNeighborhoodDesc(String valrepLandimpComp3RecNeighborhoodDesc) {
        this.valrepLandimpComp3RecNeighborhoodDesc = valrepLandimpComp3RecNeighborhoodDesc;
    }

    public String getValrepLandimpComp3RecNeighborhoodDescription() {
        return NULL_STRING_SETTER(valrepLandimpComp3RecNeighborhoodDescription);
    }

    public void setValrepLandimpComp3RecNeighborhoodDescription(String valrepLandimpComp3RecNeighborhoodDescription) {
        this.valrepLandimpComp3RecNeighborhoodDescription = valrepLandimpComp3RecNeighborhoodDescription;
    }

    public String getValrepLandimpComp3RecNeighborhoodType() {
        return NULL_STRING_SETTER(valrepLandimpComp3RecNeighborhoodType);
    }

    public void setValrepLandimpComp3RecNeighborhoodType(String valrepLandimpComp3RecNeighborhoodType) {
        this.valrepLandimpComp3RecNeighborhoodType = valrepLandimpComp3RecNeighborhoodType;
    }

    public String getValrepLandimpComp3RecOthers() {
        return NULL_STRING_SETTER(valrepLandimpComp3RecOthers);
    }

    public void setValrepLandimpComp3RecOthers(String valrepLandimpComp3RecOthers) {
        this.valrepLandimpComp3RecOthers = valrepLandimpComp3RecOthers;
    }

    public String getValrepLandimpComp3RecOthers2() {
        return NULL_STRING_SETTER(valrepLandimpComp3RecOthers2);
    }

    public void setValrepLandimpComp3RecOthers2(String valrepLandimpComp3RecOthers2) {
        this.valrepLandimpComp3RecOthers2 = valrepLandimpComp3RecOthers2;
    }

    public String getValrepLandimpComp3RecOthers2Desc() {
        return NULL_STRING_SETTER(valrepLandimpComp3RecOthers2Desc);
    }

    public void setValrepLandimpComp3RecOthers2Desc(String valrepLandimpComp3RecOthers2Desc) {
        this.valrepLandimpComp3RecOthers2Desc = valrepLandimpComp3RecOthers2Desc;
    }

    public String getValrepLandimpComp3RecOthers2Description() {
        return NULL_STRING_SETTER(valrepLandimpComp3RecOthers2Description);
    }

    public void setValrepLandimpComp3RecOthers2Description(String valrepLandimpComp3RecOthers2Description) {
        this.valrepLandimpComp3RecOthers2Description = valrepLandimpComp3RecOthers2Description;
    }

    public String getValrepLandimpComp3RecOthers3() {
        return NULL_STRING_SETTER(valrepLandimpComp3RecOthers3);
    }

    public void setValrepLandimpComp3RecOthers3(String valrepLandimpComp3RecOthers3) {
        this.valrepLandimpComp3RecOthers3 = valrepLandimpComp3RecOthers3;
    }

    public String getValrepLandimpComp3RecOthers3Desc() {
        return NULL_STRING_SETTER(valrepLandimpComp3RecOthers3Desc);
    }

    public void setValrepLandimpComp3RecOthers3Desc(String valrepLandimpComp3RecOthers3Desc) {
        this.valrepLandimpComp3RecOthers3Desc = valrepLandimpComp3RecOthers3Desc;
    }

    public String getValrepLandimpComp3RecOthers3Description() {
        return NULL_STRING_SETTER(valrepLandimpComp3RecOthers3Description);
    }

    public void setValrepLandimpComp3RecOthers3Description(String valrepLandimpComp3RecOthers3Description) {
        this.valrepLandimpComp3RecOthers3Description = valrepLandimpComp3RecOthers3Description;
    }

    public String getValrepLandimpComp3RecOthersDesc() {
        return NULL_STRING_SETTER(valrepLandimpComp3RecOthersDesc);
    }

    public void setValrepLandimpComp3RecOthersDesc(String valrepLandimpComp3RecOthersDesc) {
        this.valrepLandimpComp3RecOthersDesc = valrepLandimpComp3RecOthersDesc;
    }

    public String getValrepLandimpComp3RecOthersDescription() {
        return NULL_STRING_SETTER(valrepLandimpComp3RecOthersDescription);
    }

    public void setValrepLandimpComp3RecOthersDescription(String valrepLandimpComp3RecOthersDescription) {
        this.valrepLandimpComp3RecOthersDescription = valrepLandimpComp3RecOthersDescription;
    }

    public String getValrepLandimpComp3RecOthersName() {
        return NULL_STRING_SETTER(valrepLandimpComp3RecOthersName);
    }

    public void setValrepLandimpComp3RecOthersName(String valrepLandimpComp3RecOthersName) {
        this.valrepLandimpComp3RecOthersName = valrepLandimpComp3RecOthersName;
    }

    public String getValrepLandimpComp3RecPropRights() {
        return NULL_STRING_SETTER(valrepLandimpComp3RecPropRights);
    }

    public void setValrepLandimpComp3RecPropRights(String valrepLandimpComp3RecPropRights) {
        this.valrepLandimpComp3RecPropRights = valrepLandimpComp3RecPropRights;
    }

    public String getValrepLandimpComp3RecPropRightsDesc() {
        return NULL_STRING_SETTER(valrepLandimpComp3RecPropRightsDesc);
    }

    public void setValrepLandimpComp3RecPropRightsDesc(String valrepLandimpComp3RecPropRightsDesc) {
        this.valrepLandimpComp3RecPropRightsDesc = valrepLandimpComp3RecPropRightsDesc;
    }

    public String getValrepLandimpComp3RecPropertyRightsConveyed() {
        return NULL_STRING_SETTER(valrepLandimpComp3RecPropertyRightsConveyed);
    }

    public void setValrepLandimpComp3RecPropertyRightsConveyed(String valrepLandimpComp3RecPropertyRightsConveyed) {
        this.valrepLandimpComp3RecPropertyRightsConveyed = valrepLandimpComp3RecPropertyRightsConveyed;
    }

    public String getValrepLandimpComp3RecPropertyRightsConveyedAdjustmentPrice() {
        return NULL_STRING_SETTER(valrepLandimpComp3RecPropertyRightsConveyedAdjustmentPrice);
    }

    public void setValrepLandimpComp3RecPropertyRightsConveyedAdjustmentPrice(String valrepLandimpComp3RecPropertyRightsConveyedAdjustmentPrice) {
        this.valrepLandimpComp3RecPropertyRightsConveyedAdjustmentPrice = valrepLandimpComp3RecPropertyRightsConveyedAdjustmentPrice;
    }

    public String getValrepLandimpComp3RecPropertyRightsConveyedDesc() {
        return NULL_STRING_SETTER(valrepLandimpComp3RecPropertyRightsConveyedDesc);
    }

    public void setValrepLandimpComp3RecPropertyRightsConveyedDesc(String valrepLandimpComp3RecPropertyRightsConveyedDesc) {
        this.valrepLandimpComp3RecPropertyRightsConveyedDesc = valrepLandimpComp3RecPropertyRightsConveyedDesc;
    }

    public String getValrepLandimpComp3RecPropertyRightsConveyedDescription() {
        return NULL_STRING_SETTER(valrepLandimpComp3RecPropertyRightsConveyedDescription);
    }

    public void setValrepLandimpComp3RecPropertyRightsConveyedDescription(String valrepLandimpComp3RecPropertyRightsConveyedDescription) {
        this.valrepLandimpComp3RecPropertyRightsConveyedDescription = valrepLandimpComp3RecPropertyRightsConveyedDescription;
    }

    public String getValrepLandimpComp3RecRoad() {
        return NULL_STRING_SETTER(valrepLandimpComp3RecRoad);
    }

    public void setValrepLandimpComp3RecRoad(String valrepLandimpComp3RecRoad) {
        this.valrepLandimpComp3RecRoad = valrepLandimpComp3RecRoad;
    }

    public String getValrepLandimpComp3RecRoadDesc() {
        return NULL_STRING_SETTER(valrepLandimpComp3RecRoadDesc);
    }

    public void setValrepLandimpComp3RecRoadDesc(String valrepLandimpComp3RecRoadDesc) {
        this.valrepLandimpComp3RecRoadDesc = valrepLandimpComp3RecRoadDesc;
    }

    public String getValrepLandimpComp3RecRoadDescription() {
        return NULL_STRING_SETTER(valrepLandimpComp3RecRoadDescription);
    }

    public void setValrepLandimpComp3RecRoadDescription(String valrepLandimpComp3RecRoadDescription) {
        this.valrepLandimpComp3RecRoadDescription = valrepLandimpComp3RecRoadDescription;
    }

    public String getValrepLandimpComp3RecShape() {
        return NULL_STRING_SETTER(valrepLandimpComp3RecShape);
    }

    public void setValrepLandimpComp3RecShape(String valrepLandimpComp3RecShape) {
        this.valrepLandimpComp3RecShape = valrepLandimpComp3RecShape;
    }

    public String getValrepLandimpComp3RecShapeDesc() {
        return NULL_STRING_SETTER(valrepLandimpComp3RecShapeDesc);
    }

    public void setValrepLandimpComp3RecShapeDesc(String valrepLandimpComp3RecShapeDesc) {
        this.valrepLandimpComp3RecShapeDesc = valrepLandimpComp3RecShapeDesc;
    }

    public String getValrepLandimpComp3RecShapeDescription() {
        return NULL_STRING_SETTER(valrepLandimpComp3RecShapeDescription);
    }

    public void setValrepLandimpComp3RecShapeDescription(String valrepLandimpComp3RecShapeDescription) {
        this.valrepLandimpComp3RecShapeDescription = valrepLandimpComp3RecShapeDescription;
    }

    public String getValrepLandimpComp3RecSize() {
        return NULL_STRING_SETTER(valrepLandimpComp3RecSize);
    }

    public void setValrepLandimpComp3RecSize(String valrepLandimpComp3RecSize) {
        this.valrepLandimpComp3RecSize = valrepLandimpComp3RecSize;
    }

    public String getValrepLandimpComp3RecSizeDesc() {
        return NULL_STRING_SETTER(valrepLandimpComp3RecSizeDesc);
    }

    public void setValrepLandimpComp3RecSizeDesc(String valrepLandimpComp3RecSizeDesc) {
        this.valrepLandimpComp3RecSizeDesc = valrepLandimpComp3RecSizeDesc;
    }

    public String getValrepLandimpComp3RecSizeDescription() {
        return NULL_STRING_SETTER(valrepLandimpComp3RecSizeDescription);
    }

    public void setValrepLandimpComp3RecSizeDescription(String valrepLandimpComp3RecSizeDescription) {
        this.valrepLandimpComp3RecSizeDescription = valrepLandimpComp3RecSizeDescription;
    }

    public String getValrepLandimpComp3RecTerrain() {
        return NULL_STRING_SETTER(valrepLandimpComp3RecTerrain);
    }

    public void setValrepLandimpComp3RecTerrain(String valrepLandimpComp3RecTerrain) {
        this.valrepLandimpComp3RecTerrain = valrepLandimpComp3RecTerrain;
    }

    public String getValrepLandimpComp3RecTerrainDesc() {
        return NULL_STRING_SETTER(valrepLandimpComp3RecTerrainDesc);
    }

    public void setValrepLandimpComp3RecTerrainDesc(String valrepLandimpComp3RecTerrainDesc) {
        this.valrepLandimpComp3RecTerrainDesc = valrepLandimpComp3RecTerrainDesc;
    }

    public String getValrepLandimpComp3RecTerrainDescription() {
        return NULL_STRING_SETTER(valrepLandimpComp3RecTerrainDescription);
    }

    public void setValrepLandimpComp3RecTerrainDescription(String valrepLandimpComp3RecTerrainDescription) {
        this.valrepLandimpComp3RecTerrainDescription = valrepLandimpComp3RecTerrainDescription;
    }

    public String getValrepLandimpComp3RecTime() {
        return NULL_STRING_SETTER(valrepLandimpComp3RecTime);
    }

    public void setValrepLandimpComp3RecTime(String valrepLandimpComp3RecTime) {
        this.valrepLandimpComp3RecTime = valrepLandimpComp3RecTime;
    }

    public String getValrepLandimpComp3RecTimeAdjustmentPrice() {
        return NULL_STRING_SETTER(valrepLandimpComp3RecTimeAdjustmentPrice);
    }

    public void setValrepLandimpComp3RecTimeAdjustmentPrice(String valrepLandimpComp3RecTimeAdjustmentPrice) {
        this.valrepLandimpComp3RecTimeAdjustmentPrice = valrepLandimpComp3RecTimeAdjustmentPrice;
    }

    public String getValrepLandimpComp3RecTimeDesc() {
        return NULL_STRING_SETTER(valrepLandimpComp3RecTimeDesc);
    }

    public void setValrepLandimpComp3RecTimeDesc(String valrepLandimpComp3RecTimeDesc) {
        this.valrepLandimpComp3RecTimeDesc = valrepLandimpComp3RecTimeDesc;
    }

    public String getValrepLandimpComp3RecTimeDescription() {
        return NULL_STRING_SETTER(valrepLandimpComp3RecTimeDescription);
    }

    public void setValrepLandimpComp3RecTimeDescription(String valrepLandimpComp3RecTimeDescription) {
        this.valrepLandimpComp3RecTimeDescription = valrepLandimpComp3RecTimeDescription;
    }

    public String getValrepLandimpComp3RecTimeElement() {
        return NULL_STRING_SETTER(valrepLandimpComp3RecTimeElement);
    }

    public void setValrepLandimpComp3RecTimeElement(String valrepLandimpComp3RecTimeElement) {
        this.valrepLandimpComp3RecTimeElement = valrepLandimpComp3RecTimeElement;
    }

    public String getValrepLandimpComp3RecTimeElementDesc() {
        return NULL_STRING_SETTER(valrepLandimpComp3RecTimeElementDesc);
    }

    public void setValrepLandimpComp3RecTimeElementDesc(String valrepLandimpComp3RecTimeElementDesc) {
        this.valrepLandimpComp3RecTimeElementDesc = valrepLandimpComp3RecTimeElementDesc;
    }

    public String getValrepLandimpComp3RecTruLot() {
        return NULL_STRING_SETTER(valrepLandimpComp3RecTruLot);
    }

    public void setValrepLandimpComp3RecTruLot(String valrepLandimpComp3RecTruLot) {
        this.valrepLandimpComp3RecTruLot = valrepLandimpComp3RecTruLot;
    }

    public String getValrepLandimpComp3Remarks() {
        return NULL_STRING_SETTER(valrepLandimpComp3Remarks);
    }

    public void setValrepLandimpComp3Remarks(String valrepLandimpComp3Remarks) {
        this.valrepLandimpComp3Remarks = valrepLandimpComp3Remarks;
    }

    public String getValrepLandimpComp3SellingPrice() {
        return NULL_STRING_SETTER(valrepLandimpComp3SellingPrice);
    }

    public void setValrepLandimpComp3SellingPrice(String valrepLandimpComp3SellingPrice) {
        this.valrepLandimpComp3SellingPrice = valrepLandimpComp3SellingPrice;
    }

    public String getValrepLandimpComp3Shape() {
        return NULL_STRING_SETTER(valrepLandimpComp3Shape);
    }

    public void setValrepLandimpComp3Shape(String valrepLandimpComp3Shape) {
        this.valrepLandimpComp3Shape = valrepLandimpComp3Shape;
    }

    public String getValrepLandimpComp3Source() {
        return NULL_STRING_SETTER(valrepLandimpComp3Source);
    }

    public void setValrepLandimpComp3Source(String valrepLandimpComp3Source) {
        this.valrepLandimpComp3Source = valrepLandimpComp3Source;
    }

    public String getValrepLandimpComp3TotalAdj() {
        return NULL_STRING_SETTER(valrepLandimpComp3TotalAdj);
    }

    public void setValrepLandimpComp3TotalAdj(String valrepLandimpComp3TotalAdj) {
        this.valrepLandimpComp3TotalAdj = valrepLandimpComp3TotalAdj;
    }

    public String getValrepLandimpComp3TotalgrossAdj() {
        return NULL_STRING_SETTER(valrepLandimpComp3TotalgrossAdj);
    }

    public void setValrepLandimpComp3TotalgrossAdj(String valrepLandimpComp3TotalgrossAdj) {
        this.valrepLandimpComp3TotalgrossAdj = valrepLandimpComp3TotalgrossAdj;
    }

    public String getValrepLandimpComp3TotalnetAdj() {
        return NULL_STRING_SETTER(valrepLandimpComp3TotalnetAdj);
    }

    public void setValrepLandimpComp3TotalnetAdj(String valrepLandimpComp3TotalnetAdj) {
        this.valrepLandimpComp3TotalnetAdj = valrepLandimpComp3TotalnetAdj;
    }

    public String getValrepLandimpComp3Weight() {
        return NULL_STRING_SETTER(valrepLandimpComp3Weight);
    }

    public void setValrepLandimpComp3Weight(String valrepLandimpComp3Weight) {
        this.valrepLandimpComp3Weight = valrepLandimpComp3Weight;
    }

    public String getValrepLandimpComp3WeightEquivalent() {
        return NULL_STRING_SETTER(valrepLandimpComp3WeightEquivalent);
    }

    public void setValrepLandimpComp3WeightEquivalent(String valrepLandimpComp3WeightEquivalent) {
        this.valrepLandimpComp3WeightEquivalent = valrepLandimpComp3WeightEquivalent;
    }

    public String getValrepLandimpComp3WeightedValue() {
        return NULL_STRING_SETTER(valrepLandimpComp3WeightedValue);
    }

    public void setValrepLandimpComp3WeightedValue(String valrepLandimpComp3WeightedValue) {
        this.valrepLandimpComp3WeightedValue = valrepLandimpComp3WeightedValue;
    }

    public String getValrepLandimpComp3WgtDist() {
        return NULL_STRING_SETTER(valrepLandimpComp3WgtDist);
    }

    public void setValrepLandimpComp3WgtDist(String valrepLandimpComp3WgtDist) {
        this.valrepLandimpComp3WgtDist = valrepLandimpComp3WgtDist;
    }

    public String getValrepLandimpComp3Zoning() {
        return NULL_STRING_SETTER(valrepLandimpComp3Zoning);
    }

    public void setValrepLandimpComp3Zoning(String valrepLandimpComp3Zoning) {
        this.valrepLandimpComp3Zoning = valrepLandimpComp3Zoning;
    }

    public String getValrepLandimpComp4AdjustablePriceSqm() {
        return NULL_STRING_SETTER(valrepLandimpComp4AdjustablePriceSqm);
    }

    public void setValrepLandimpComp4AdjustablePriceSqm(String valrepLandimpComp4AdjustablePriceSqm) {
        this.valrepLandimpComp4AdjustablePriceSqm = valrepLandimpComp4AdjustablePriceSqm;
    }

    public String getValrepLandimpComp4AdjustedValue() {
        return NULL_STRING_SETTER(valrepLandimpComp4AdjustedValue);
    }

    public void setValrepLandimpComp4AdjustedValue(String valrepLandimpComp4AdjustedValue) {
        this.valrepLandimpComp4AdjustedValue = valrepLandimpComp4AdjustedValue;
    }

    public String getValrepLandimpComp4ApproxAge() {
        return NULL_STRING_SETTER(valrepLandimpComp4ApproxAge);
    }

    public void setValrepLandimpComp4ApproxAge(String valrepLandimpComp4ApproxAge) {
        this.valrepLandimpComp4ApproxAge = valrepLandimpComp4ApproxAge;
    }

    public String getValrepLandimpComp4Area() {
        return NULL_STRING_SETTER(valrepLandimpComp4Area);
    }

    public void setValrepLandimpComp4Area(String valrepLandimpComp4Area) {
        this.valrepLandimpComp4Area = valrepLandimpComp4Area;
    }

    public String getValrepLandimpComp4BasePrice() {
        return NULL_STRING_SETTER(valrepLandimpComp4BasePrice);
    }

    public void setValrepLandimpComp4BasePrice(String valrepLandimpComp4BasePrice) {
        this.valrepLandimpComp4BasePrice = valrepLandimpComp4BasePrice;
    }

    public String getValrepLandimpComp4BasePriceSqm() {
        return NULL_STRING_SETTER(valrepLandimpComp4BasePriceSqm);
    }

    public void setValrepLandimpComp4BasePriceSqm(String valrepLandimpComp4BasePriceSqm) {
        this.valrepLandimpComp4BasePriceSqm = valrepLandimpComp4BasePriceSqm;
    }

    public String getValrepLandimpComp4BldgArea() {
        return NULL_STRING_SETTER(valrepLandimpComp4BldgArea);
    }

    public void setValrepLandimpComp4BldgArea(String valrepLandimpComp4BldgArea) {
        this.valrepLandimpComp4BldgArea = valrepLandimpComp4BldgArea;
    }

    public String getValrepLandimpComp4BldgDepreciation() {
        return NULL_STRING_SETTER(valrepLandimpComp4BldgDepreciation);
    }

    public void setValrepLandimpComp4BldgDepreciation(String valrepLandimpComp4BldgDepreciation) {
        this.valrepLandimpComp4BldgDepreciation = valrepLandimpComp4BldgDepreciation;
    }

    public String getValrepLandimpComp4BldgDepreciationVal() {
        return NULL_STRING_SETTER(valrepLandimpComp4BldgDepreciationVal);
    }

    public void setValrepLandimpComp4BldgDepreciationVal(String valrepLandimpComp4BldgDepreciationVal) {
        this.valrepLandimpComp4BldgDepreciationVal = valrepLandimpComp4BldgDepreciationVal;
    }

    public String getValrepLandimpComp4BldgReplacementUnitValue() {
        return NULL_STRING_SETTER(valrepLandimpComp4BldgReplacementUnitValue);
    }

    public void setValrepLandimpComp4BldgReplacementUnitValue(String valrepLandimpComp4BldgReplacementUnitValue) {
        this.valrepLandimpComp4BldgReplacementUnitValue = valrepLandimpComp4BldgReplacementUnitValue;
    }

    public String getValrepLandimpComp4BldgReplacementValue() {
        return NULL_STRING_SETTER(valrepLandimpComp4BldgReplacementValue);
    }

    public void setValrepLandimpComp4BldgReplacementValue(String valrepLandimpComp4BldgReplacementValue) {
        this.valrepLandimpComp4BldgReplacementValue = valrepLandimpComp4BldgReplacementValue;
    }

    public String getValrepLandimpComp4Characteristic() {
        return NULL_STRING_SETTER(valrepLandimpComp4Characteristic);
    }

    public void setValrepLandimpComp4Characteristic(String valrepLandimpComp4Characteristic) {
        this.valrepLandimpComp4Characteristic = valrepLandimpComp4Characteristic;
    }

    public String getValrepLandimpComp4Characteristics() {
        return NULL_STRING_SETTER(valrepLandimpComp4Characteristics);
    }

    public void setValrepLandimpComp4Characteristics(String valrepLandimpComp4Characteristics) {
        this.valrepLandimpComp4Characteristics = valrepLandimpComp4Characteristics;
    }

    public String getValrepLandimpComp4ConcludedAdjustment() {
        return NULL_STRING_SETTER(valrepLandimpComp4ConcludedAdjustment);
    }

    public void setValrepLandimpComp4ConcludedAdjustment(String valrepLandimpComp4ConcludedAdjustment) {
        this.valrepLandimpComp4ConcludedAdjustment = valrepLandimpComp4ConcludedAdjustment;
    }

    public String getValrepLandimpComp4ContactNo() {
        return NULL_STRING_SETTER(valrepLandimpComp4ContactNo);
    }

    public void setValrepLandimpComp4ContactNo(String valrepLandimpComp4ContactNo) {
        this.valrepLandimpComp4ContactNo = valrepLandimpComp4ContactNo;
    }

    public String getValrepLandimpComp4DateDay() {
        return NULL_STRING_SETTER(valrepLandimpComp4DateDay);
    }

    public void setValrepLandimpComp4DateDay(String valrepLandimpComp4DateDay) {
        this.valrepLandimpComp4DateDay = valrepLandimpComp4DateDay;
    }

    public String getValrepLandimpComp4DateMonth() {
        return NULL_STRING_SETTER(valrepLandimpComp4DateMonth);
    }

    public void setValrepLandimpComp4DateMonth(String valrepLandimpComp4DateMonth) {
        this.valrepLandimpComp4DateMonth = valrepLandimpComp4DateMonth;
    }

    public String getValrepLandimpComp4DateYear() {
        return NULL_STRING_SETTER(valrepLandimpComp4DateYear);
    }

    public void setValrepLandimpComp4DateYear(String valrepLandimpComp4DateYear) {
        this.valrepLandimpComp4DateYear = valrepLandimpComp4DateYear;
    }

    public String getValrepLandimpComp4DiscountRate() {
        return NULL_STRING_SETTER(valrepLandimpComp4DiscountRate);
    }

    public void setValrepLandimpComp4DiscountRate(String valrepLandimpComp4DiscountRate) {
        this.valrepLandimpComp4DiscountRate = valrepLandimpComp4DiscountRate;
    }

    public String getValrepLandimpComp4Discounts() {
        return NULL_STRING_SETTER(valrepLandimpComp4Discounts);
    }

    public void setValrepLandimpComp4Discounts(String valrepLandimpComp4Discounts) {
        this.valrepLandimpComp4Discounts = valrepLandimpComp4Discounts;
    }

    public String getValrepLandimpComp4EffectiveDate() {
        return NULL_STRING_SETTER(valrepLandimpComp4EffectiveDate);
    }

    public void setValrepLandimpComp4EffectiveDate(String valrepLandimpComp4EffectiveDate) {
        this.valrepLandimpComp4EffectiveDate = valrepLandimpComp4EffectiveDate;
    }

    public String getValrepLandimpComp4LandResidualValue() {
        return NULL_STRING_SETTER(valrepLandimpComp4LandResidualValue);
    }

    public void setValrepLandimpComp4LandResidualValue(String valrepLandimpComp4LandResidualValue) {
        this.valrepLandimpComp4LandResidualValue = valrepLandimpComp4LandResidualValue;
    }

    public String getValrepLandimpComp4Location() {
        return NULL_STRING_SETTER(valrepLandimpComp4Location);
    }

    public void setValrepLandimpComp4Location(String valrepLandimpComp4Location) {
        this.valrepLandimpComp4Location = valrepLandimpComp4Location;
    }

    public String getValrepLandimpComp4NetBldgValue() {
        return NULL_STRING_SETTER(valrepLandimpComp4NetBldgValue);
    }

    public void setValrepLandimpComp4NetBldgValue(String valrepLandimpComp4NetBldgValue) {
        this.valrepLandimpComp4NetBldgValue = valrepLandimpComp4NetBldgValue;
    }

    public String getValrepLandimpComp4NetLandValuation() {
        return NULL_STRING_SETTER(valrepLandimpComp4NetLandValuation);
    }

    public void setValrepLandimpComp4NetLandValuation(String valrepLandimpComp4NetLandValuation) {
        this.valrepLandimpComp4NetLandValuation = valrepLandimpComp4NetLandValuation;
    }

    public String getValrepLandimpComp4PriceSqm() {
        return NULL_STRING_SETTER(valrepLandimpComp4PriceSqm);
    }

    public void setValrepLandimpComp4PriceSqm(String valrepLandimpComp4PriceSqm) {
        this.valrepLandimpComp4PriceSqm = valrepLandimpComp4PriceSqm;
    }

    public String getValrepLandimpComp4PriceValue() {
        return NULL_STRING_SETTER(valrepLandimpComp4PriceValue);
    }

    public void setValrepLandimpComp4PriceValue(String valrepLandimpComp4PriceValue) {
        this.valrepLandimpComp4PriceValue = valrepLandimpComp4PriceValue;
    }

    public String getValrepLandimpComp4PricingCircumstance() {
        return NULL_STRING_SETTER(valrepLandimpComp4PricingCircumstance);
    }

    public void setValrepLandimpComp4PricingCircumstance(String valrepLandimpComp4PricingCircumstance) {
        this.valrepLandimpComp4PricingCircumstance = valrepLandimpComp4PricingCircumstance;
    }

    public String getValrepLandimpComp4PropInterest() {
        return NULL_STRING_SETTER(valrepLandimpComp4PropInterest);
    }

    public void setValrepLandimpComp4PropInterest(String valrepLandimpComp4PropInterest) {
        this.valrepLandimpComp4PropInterest = valrepLandimpComp4PropInterest;
    }

    public String getValrepLandimpComp4PropertyType() {
        return NULL_STRING_SETTER(valrepLandimpComp4PropertyType);
    }

    public void setValrepLandimpComp4PropertyType(String valrepLandimpComp4PropertyType) {
        this.valrepLandimpComp4PropertyType = valrepLandimpComp4PropertyType;
    }

    public String getValrepLandimpComp4RecAccessibility() {
        return NULL_STRING_SETTER(valrepLandimpComp4RecAccessibility);
    }

    public void setValrepLandimpComp4RecAccessibility(String valrepLandimpComp4RecAccessibility) {
        this.valrepLandimpComp4RecAccessibility = valrepLandimpComp4RecAccessibility;
    }

    public String getValrepLandimpComp4RecAccessibilityDesc() {
        return NULL_STRING_SETTER(valrepLandimpComp4RecAccessibilityDesc);
    }

    public void setValrepLandimpComp4RecAccessibilityDesc(String valrepLandimpComp4RecAccessibilityDesc) {
        this.valrepLandimpComp4RecAccessibilityDesc = valrepLandimpComp4RecAccessibilityDesc;
    }

    public String getValrepLandimpComp4RecAccessibilityDescription() {
        return NULL_STRING_SETTER(valrepLandimpComp4RecAccessibilityDescription);
    }

    public void setValrepLandimpComp4RecAccessibilityDescription(String valrepLandimpComp4RecAccessibilityDescription) {
        this.valrepLandimpComp4RecAccessibilityDescription = valrepLandimpComp4RecAccessibilityDescription;
    }

    public String getValrepLandimpComp4RecAmenities() {
        return NULL_STRING_SETTER(valrepLandimpComp4RecAmenities);
    }

    public void setValrepLandimpComp4RecAmenities(String valrepLandimpComp4RecAmenities) {
        this.valrepLandimpComp4RecAmenities = valrepLandimpComp4RecAmenities;
    }

    public String getValrepLandimpComp4RecBargainingAllow() {
        return NULL_STRING_SETTER(valrepLandimpComp4RecBargainingAllow);
    }

    public void setValrepLandimpComp4RecBargainingAllow(String valrepLandimpComp4RecBargainingAllow) {
        this.valrepLandimpComp4RecBargainingAllow = valrepLandimpComp4RecBargainingAllow;
    }

    public String getValrepLandimpComp4RecBargainingAllowAdjustmentPrice() {
        return NULL_STRING_SETTER(valrepLandimpComp4RecBargainingAllowAdjustmentPrice);
    }

    public void setValrepLandimpComp4RecBargainingAllowAdjustmentPrice(String valrepLandimpComp4RecBargainingAllowAdjustmentPrice) {
        this.valrepLandimpComp4RecBargainingAllowAdjustmentPrice = valrepLandimpComp4RecBargainingAllowAdjustmentPrice;
    }

    public String getValrepLandimpComp4RecBargainingAllowDesc() {
        return NULL_STRING_SETTER(valrepLandimpComp4RecBargainingAllowDesc);
    }

    public void setValrepLandimpComp4RecBargainingAllowDesc(String valrepLandimpComp4RecBargainingAllowDesc) {
        this.valrepLandimpComp4RecBargainingAllowDesc = valrepLandimpComp4RecBargainingAllowDesc;
    }

    public String getValrepLandimpComp4RecBargainingAllowDescription() {
        return NULL_STRING_SETTER(valrepLandimpComp4RecBargainingAllowDescription);
    }

    public void setValrepLandimpComp4RecBargainingAllowDescription(String valrepLandimpComp4RecBargainingAllowDescription) {
        this.valrepLandimpComp4RecBargainingAllowDescription = valrepLandimpComp4RecBargainingAllowDescription;
    }

    public String getValrepLandimpComp4RecBargainingAllowance() {
        return NULL_STRING_SETTER(valrepLandimpComp4RecBargainingAllowance);
    }

    public void setValrepLandimpComp4RecBargainingAllowance(String valrepLandimpComp4RecBargainingAllowance) {
        this.valrepLandimpComp4RecBargainingAllowance = valrepLandimpComp4RecBargainingAllowance;
    }

    public String getValrepLandimpComp4RecBargainingAllowanceDesc() {
        return NULL_STRING_SETTER(valrepLandimpComp4RecBargainingAllowanceDesc);
    }

    public void setValrepLandimpComp4RecBargainingAllowanceDesc(String valrepLandimpComp4RecBargainingAllowanceDesc) {
        this.valrepLandimpComp4RecBargainingAllowanceDesc = valrepLandimpComp4RecBargainingAllowanceDesc;
    }

    public String getValrepLandimpComp4RecCornerInfluence() {
        return NULL_STRING_SETTER(valrepLandimpComp4RecCornerInfluence);
    }

    public void setValrepLandimpComp4RecCornerInfluence(String valrepLandimpComp4RecCornerInfluence) {
        this.valrepLandimpComp4RecCornerInfluence = valrepLandimpComp4RecCornerInfluence;
    }

    public String getValrepLandimpComp4RecDegreeOfDevt() {
        return NULL_STRING_SETTER(valrepLandimpComp4RecDegreeOfDevt);
    }

    public void setValrepLandimpComp4RecDegreeOfDevt(String valrepLandimpComp4RecDegreeOfDevt) {
        this.valrepLandimpComp4RecDegreeOfDevt = valrepLandimpComp4RecDegreeOfDevt;
    }

    public String getValrepLandimpComp4RecElevation() {
        return NULL_STRING_SETTER(valrepLandimpComp4RecElevation);
    }

    public void setValrepLandimpComp4RecElevation(String valrepLandimpComp4RecElevation) {
        this.valrepLandimpComp4RecElevation = valrepLandimpComp4RecElevation;
    }

    public String getValrepLandimpComp4RecElevationDesc() {
        return NULL_STRING_SETTER(valrepLandimpComp4RecElevationDesc);
    }

    public void setValrepLandimpComp4RecElevationDesc(String valrepLandimpComp4RecElevationDesc) {
        this.valrepLandimpComp4RecElevationDesc = valrepLandimpComp4RecElevationDesc;
    }

    public String getValrepLandimpComp4RecElevationDescription() {
        return NULL_STRING_SETTER(valrepLandimpComp4RecElevationDescription);
    }

    public void setValrepLandimpComp4RecElevationDescription(String valrepLandimpComp4RecElevationDescription) {
        this.valrepLandimpComp4RecElevationDescription = valrepLandimpComp4RecElevationDescription;
    }

    public String getValrepLandimpComp4RecLocation() {
        return NULL_STRING_SETTER(valrepLandimpComp4RecLocation);
    }

    public void setValrepLandimpComp4RecLocation(String valrepLandimpComp4RecLocation) {
        this.valrepLandimpComp4RecLocation = valrepLandimpComp4RecLocation;
    }

    public String getValrepLandimpComp4RecLocationDesc() {
        return NULL_STRING_SETTER(valrepLandimpComp4RecLocationDesc);
    }

    public void setValrepLandimpComp4RecLocationDesc(String valrepLandimpComp4RecLocationDesc) {
        this.valrepLandimpComp4RecLocationDesc = valrepLandimpComp4RecLocationDesc;
    }

    public String getValrepLandimpComp4RecLocationDescription() {
        return NULL_STRING_SETTER(valrepLandimpComp4RecLocationDescription);
    }

    public void setValrepLandimpComp4RecLocationDescription(String valrepLandimpComp4RecLocationDescription) {
        this.valrepLandimpComp4RecLocationDescription = valrepLandimpComp4RecLocationDescription;
    }

    public String getValrepLandimpComp4RecLotType() {
        return NULL_STRING_SETTER(valrepLandimpComp4RecLotType);
    }

    public void setValrepLandimpComp4RecLotType(String valrepLandimpComp4RecLotType) {
        this.valrepLandimpComp4RecLotType = valrepLandimpComp4RecLotType;
    }

    public String getValrepLandimpComp4RecLotTypeDesc() {
        return NULL_STRING_SETTER(valrepLandimpComp4RecLotTypeDesc);
    }

    public void setValrepLandimpComp4RecLotTypeDesc(String valrepLandimpComp4RecLotTypeDesc) {
        this.valrepLandimpComp4RecLotTypeDesc = valrepLandimpComp4RecLotTypeDesc;
    }

    public String getValrepLandimpComp4RecLotTypeDescription() {
        return NULL_STRING_SETTER(valrepLandimpComp4RecLotTypeDescription);
    }

    public void setValrepLandimpComp4RecLotTypeDescription(String valrepLandimpComp4RecLotTypeDescription) {
        this.valrepLandimpComp4RecLotTypeDescription = valrepLandimpComp4RecLotTypeDescription;
    }

    public String getValrepLandimpComp4RecNeighborhood() {
        return NULL_STRING_SETTER(valrepLandimpComp4RecNeighborhood);
    }

    public void setValrepLandimpComp4RecNeighborhood(String valrepLandimpComp4RecNeighborhood) {
        this.valrepLandimpComp4RecNeighborhood = valrepLandimpComp4RecNeighborhood;
    }

    public String getValrepLandimpComp4RecNeighborhoodDesc() {
        return NULL_STRING_SETTER(valrepLandimpComp4RecNeighborhoodDesc);
    }

    public void setValrepLandimpComp4RecNeighborhoodDesc(String valrepLandimpComp4RecNeighborhoodDesc) {
        this.valrepLandimpComp4RecNeighborhoodDesc = valrepLandimpComp4RecNeighborhoodDesc;
    }

    public String getValrepLandimpComp4RecNeighborhoodDescription() {
        return NULL_STRING_SETTER(valrepLandimpComp4RecNeighborhoodDescription);
    }

    public void setValrepLandimpComp4RecNeighborhoodDescription(String valrepLandimpComp4RecNeighborhoodDescription) {
        this.valrepLandimpComp4RecNeighborhoodDescription = valrepLandimpComp4RecNeighborhoodDescription;
    }

    public String getValrepLandimpComp4RecNeighborhoodType() {
        return NULL_STRING_SETTER(valrepLandimpComp4RecNeighborhoodType);
    }

    public void setValrepLandimpComp4RecNeighborhoodType(String valrepLandimpComp4RecNeighborhoodType) {
        this.valrepLandimpComp4RecNeighborhoodType = valrepLandimpComp4RecNeighborhoodType;
    }

    public String getValrepLandimpComp4RecOthers() {
        return NULL_STRING_SETTER(valrepLandimpComp4RecOthers);
    }

    public void setValrepLandimpComp4RecOthers(String valrepLandimpComp4RecOthers) {
        this.valrepLandimpComp4RecOthers = valrepLandimpComp4RecOthers;
    }

    public String getValrepLandimpComp4RecOthers2() {
        return NULL_STRING_SETTER(valrepLandimpComp4RecOthers2);
    }

    public void setValrepLandimpComp4RecOthers2(String valrepLandimpComp4RecOthers2) {
        this.valrepLandimpComp4RecOthers2 = valrepLandimpComp4RecOthers2;
    }

    public String getValrepLandimpComp4RecOthers2Desc() {
        return NULL_STRING_SETTER(valrepLandimpComp4RecOthers2Desc);
    }

    public void setValrepLandimpComp4RecOthers2Desc(String valrepLandimpComp4RecOthers2Desc) {
        this.valrepLandimpComp4RecOthers2Desc = valrepLandimpComp4RecOthers2Desc;
    }

    public String getValrepLandimpComp4RecOthers2Description() {
        return NULL_STRING_SETTER(valrepLandimpComp4RecOthers2Description);
    }

    public void setValrepLandimpComp4RecOthers2Description(String valrepLandimpComp4RecOthers2Description) {
        this.valrepLandimpComp4RecOthers2Description = valrepLandimpComp4RecOthers2Description;
    }

    public String getValrepLandimpComp4RecOthers3() {
        return NULL_STRING_SETTER(valrepLandimpComp4RecOthers3);
    }

    public void setValrepLandimpComp4RecOthers3(String valrepLandimpComp4RecOthers3) {
        this.valrepLandimpComp4RecOthers3 = valrepLandimpComp4RecOthers3;
    }

    public String getValrepLandimpComp4RecOthers3Desc() {
        return NULL_STRING_SETTER(valrepLandimpComp4RecOthers3Desc);
    }

    public void setValrepLandimpComp4RecOthers3Desc(String valrepLandimpComp4RecOthers3Desc) {
        this.valrepLandimpComp4RecOthers3Desc = valrepLandimpComp4RecOthers3Desc;
    }

    public String getValrepLandimpComp4RecOthers3Description() {
        return NULL_STRING_SETTER(valrepLandimpComp4RecOthers3Description);
    }

    public void setValrepLandimpComp4RecOthers3Description(String valrepLandimpComp4RecOthers3Description) {
        this.valrepLandimpComp4RecOthers3Description = valrepLandimpComp4RecOthers3Description;
    }

    public String getValrepLandimpComp4RecOthersDesc() {
        return NULL_STRING_SETTER(valrepLandimpComp4RecOthersDesc);
    }

    public void setValrepLandimpComp4RecOthersDesc(String valrepLandimpComp4RecOthersDesc) {
        this.valrepLandimpComp4RecOthersDesc = valrepLandimpComp4RecOthersDesc;
    }

    public String getValrepLandimpComp4RecOthersDescription() {
        return NULL_STRING_SETTER(valrepLandimpComp4RecOthersDescription);
    }

    public void setValrepLandimpComp4RecOthersDescription(String valrepLandimpComp4RecOthersDescription) {
        this.valrepLandimpComp4RecOthersDescription = valrepLandimpComp4RecOthersDescription;
    }

    public String getValrepLandimpComp4RecOthersName() {
        return NULL_STRING_SETTER(valrepLandimpComp4RecOthersName);
    }

    public void setValrepLandimpComp4RecOthersName(String valrepLandimpComp4RecOthersName) {
        this.valrepLandimpComp4RecOthersName = valrepLandimpComp4RecOthersName;
    }

    public String getValrepLandimpComp4RecPropRights() {
        return NULL_STRING_SETTER(valrepLandimpComp4RecPropRights);
    }

    public void setValrepLandimpComp4RecPropRights(String valrepLandimpComp4RecPropRights) {
        this.valrepLandimpComp4RecPropRights = valrepLandimpComp4RecPropRights;
    }

    public String getValrepLandimpComp4RecPropRightsDesc() {
        return NULL_STRING_SETTER(valrepLandimpComp4RecPropRightsDesc);
    }

    public void setValrepLandimpComp4RecPropRightsDesc(String valrepLandimpComp4RecPropRightsDesc) {
        this.valrepLandimpComp4RecPropRightsDesc = valrepLandimpComp4RecPropRightsDesc;
    }

    public String getValrepLandimpComp4RecPropertyRightsConveyed() {
        return NULL_STRING_SETTER(valrepLandimpComp4RecPropertyRightsConveyed);
    }

    public void setValrepLandimpComp4RecPropertyRightsConveyed(String valrepLandimpComp4RecPropertyRightsConveyed) {
        this.valrepLandimpComp4RecPropertyRightsConveyed = valrepLandimpComp4RecPropertyRightsConveyed;
    }

    public String getValrepLandimpComp4RecPropertyRightsConveyedAdjustmentPrice() {
        return NULL_STRING_SETTER(valrepLandimpComp4RecPropertyRightsConveyedAdjustmentPrice);
    }

    public void setValrepLandimpComp4RecPropertyRightsConveyedAdjustmentPrice(String valrepLandimpComp4RecPropertyRightsConveyedAdjustmentPrice) {
        this.valrepLandimpComp4RecPropertyRightsConveyedAdjustmentPrice = valrepLandimpComp4RecPropertyRightsConveyedAdjustmentPrice;
    }

    public String getValrepLandimpComp4RecPropertyRightsConveyedDesc() {
        return NULL_STRING_SETTER(valrepLandimpComp4RecPropertyRightsConveyedDesc);
    }

    public void setValrepLandimpComp4RecPropertyRightsConveyedDesc(String valrepLandimpComp4RecPropertyRightsConveyedDesc) {
        this.valrepLandimpComp4RecPropertyRightsConveyedDesc = valrepLandimpComp4RecPropertyRightsConveyedDesc;
    }

    public String getValrepLandimpComp4RecPropertyRightsConveyedDescription() {
        return NULL_STRING_SETTER(valrepLandimpComp4RecPropertyRightsConveyedDescription);
    }

    public void setValrepLandimpComp4RecPropertyRightsConveyedDescription(String valrepLandimpComp4RecPropertyRightsConveyedDescription) {
        this.valrepLandimpComp4RecPropertyRightsConveyedDescription = valrepLandimpComp4RecPropertyRightsConveyedDescription;
    }

    public String getValrepLandimpComp4RecRoad() {
        return NULL_STRING_SETTER(valrepLandimpComp4RecRoad);
    }

    public void setValrepLandimpComp4RecRoad(String valrepLandimpComp4RecRoad) {
        this.valrepLandimpComp4RecRoad = valrepLandimpComp4RecRoad;
    }

    public String getValrepLandimpComp4RecRoadDesc() {
        return NULL_STRING_SETTER(valrepLandimpComp4RecRoadDesc);
    }

    public void setValrepLandimpComp4RecRoadDesc(String valrepLandimpComp4RecRoadDesc) {
        this.valrepLandimpComp4RecRoadDesc = valrepLandimpComp4RecRoadDesc;
    }

    public String getValrepLandimpComp4RecRoadDescription() {
        return NULL_STRING_SETTER(valrepLandimpComp4RecRoadDescription);
    }

    public void setValrepLandimpComp4RecRoadDescription(String valrepLandimpComp4RecRoadDescription) {
        this.valrepLandimpComp4RecRoadDescription = valrepLandimpComp4RecRoadDescription;
    }

    public String getValrepLandimpComp4RecShape() {
        return NULL_STRING_SETTER(valrepLandimpComp4RecShape);
    }

    public void setValrepLandimpComp4RecShape(String valrepLandimpComp4RecShape) {
        this.valrepLandimpComp4RecShape = valrepLandimpComp4RecShape;
    }

    public String getValrepLandimpComp4RecShapeDesc() {
        return NULL_STRING_SETTER(valrepLandimpComp4RecShapeDesc);
    }

    public void setValrepLandimpComp4RecShapeDesc(String valrepLandimpComp4RecShapeDesc) {
        this.valrepLandimpComp4RecShapeDesc = valrepLandimpComp4RecShapeDesc;
    }

    public String getValrepLandimpComp4RecShapeDescription() {
        return NULL_STRING_SETTER(valrepLandimpComp4RecShapeDescription);
    }

    public void setValrepLandimpComp4RecShapeDescription(String valrepLandimpComp4RecShapeDescription) {
        this.valrepLandimpComp4RecShapeDescription = valrepLandimpComp4RecShapeDescription;
    }

    public String getValrepLandimpComp4RecSize() {
        return NULL_STRING_SETTER(valrepLandimpComp4RecSize);
    }

    public void setValrepLandimpComp4RecSize(String valrepLandimpComp4RecSize) {
        this.valrepLandimpComp4RecSize = valrepLandimpComp4RecSize;
    }

    public String getValrepLandimpComp4RecSizeDesc() {
        return NULL_STRING_SETTER(valrepLandimpComp4RecSizeDesc);
    }

    public void setValrepLandimpComp4RecSizeDesc(String valrepLandimpComp4RecSizeDesc) {
        this.valrepLandimpComp4RecSizeDesc = valrepLandimpComp4RecSizeDesc;
    }

    public String getValrepLandimpComp4RecSizeDescription() {
        return NULL_STRING_SETTER(valrepLandimpComp4RecSizeDescription);
    }

    public void setValrepLandimpComp4RecSizeDescription(String valrepLandimpComp4RecSizeDescription) {
        this.valrepLandimpComp4RecSizeDescription = valrepLandimpComp4RecSizeDescription;
    }

    public String getValrepLandimpComp4RecTerrain() {
        return NULL_STRING_SETTER(valrepLandimpComp4RecTerrain);
    }

    public void setValrepLandimpComp4RecTerrain(String valrepLandimpComp4RecTerrain) {
        this.valrepLandimpComp4RecTerrain = valrepLandimpComp4RecTerrain;
    }

    public String getValrepLandimpComp4RecTerrainDesc() {
        return NULL_STRING_SETTER(valrepLandimpComp4RecTerrainDesc);
    }

    public void setValrepLandimpComp4RecTerrainDesc(String valrepLandimpComp4RecTerrainDesc) {
        this.valrepLandimpComp4RecTerrainDesc = valrepLandimpComp4RecTerrainDesc;
    }

    public String getValrepLandimpComp4RecTerrainDescription() {
        return NULL_STRING_SETTER(valrepLandimpComp4RecTerrainDescription);
    }

    public void setValrepLandimpComp4RecTerrainDescription(String valrepLandimpComp4RecTerrainDescription) {
        this.valrepLandimpComp4RecTerrainDescription = valrepLandimpComp4RecTerrainDescription;
    }

    public String getValrepLandimpComp4RecTime() {
        return NULL_STRING_SETTER(valrepLandimpComp4RecTime);
    }

    public void setValrepLandimpComp4RecTime(String valrepLandimpComp4RecTime) {
        this.valrepLandimpComp4RecTime = valrepLandimpComp4RecTime;
    }

    public String getValrepLandimpComp4RecTimeAdjustmentPrice() {
        return NULL_STRING_SETTER(valrepLandimpComp4RecTimeAdjustmentPrice);
    }

    public void setValrepLandimpComp4RecTimeAdjustmentPrice(String valrepLandimpComp4RecTimeAdjustmentPrice) {
        this.valrepLandimpComp4RecTimeAdjustmentPrice = valrepLandimpComp4RecTimeAdjustmentPrice;
    }

    public String getValrepLandimpComp4RecTimeDesc() {
        return NULL_STRING_SETTER(valrepLandimpComp4RecTimeDesc);
    }

    public void setValrepLandimpComp4RecTimeDesc(String valrepLandimpComp4RecTimeDesc) {
        this.valrepLandimpComp4RecTimeDesc = valrepLandimpComp4RecTimeDesc;
    }

    public String getValrepLandimpComp4RecTimeDescription() {
        return NULL_STRING_SETTER(valrepLandimpComp4RecTimeDescription);
    }

    public void setValrepLandimpComp4RecTimeDescription(String valrepLandimpComp4RecTimeDescription) {
        this.valrepLandimpComp4RecTimeDescription = valrepLandimpComp4RecTimeDescription;
    }

    public String getValrepLandimpComp4RecTimeElement() {
        return NULL_STRING_SETTER(valrepLandimpComp4RecTimeElement);
    }

    public void setValrepLandimpComp4RecTimeElement(String valrepLandimpComp4RecTimeElement) {
        this.valrepLandimpComp4RecTimeElement = valrepLandimpComp4RecTimeElement;
    }

    public String getValrepLandimpComp4RecTimeElementDesc() {
        return NULL_STRING_SETTER(valrepLandimpComp4RecTimeElementDesc);
    }

    public void setValrepLandimpComp4RecTimeElementDesc(String valrepLandimpComp4RecTimeElementDesc) {
        this.valrepLandimpComp4RecTimeElementDesc = valrepLandimpComp4RecTimeElementDesc;
    }

    public String getValrepLandimpComp4RecTruLot() {
        return NULL_STRING_SETTER(valrepLandimpComp4RecTruLot);
    }

    public void setValrepLandimpComp4RecTruLot(String valrepLandimpComp4RecTruLot) {
        this.valrepLandimpComp4RecTruLot = valrepLandimpComp4RecTruLot;
    }

    public String getValrepLandimpComp4Remarks() {
        return NULL_STRING_SETTER(valrepLandimpComp4Remarks);
    }

    public void setValrepLandimpComp4Remarks(String valrepLandimpComp4Remarks) {
        this.valrepLandimpComp4Remarks = valrepLandimpComp4Remarks;
    }

    public String getValrepLandimpComp4SellingPrice() {
        return NULL_STRING_SETTER(valrepLandimpComp4SellingPrice);
    }

    public void setValrepLandimpComp4SellingPrice(String valrepLandimpComp4SellingPrice) {
        this.valrepLandimpComp4SellingPrice = valrepLandimpComp4SellingPrice;
    }

    public String getValrepLandimpComp4Shape() {
        return NULL_STRING_SETTER(valrepLandimpComp4Shape);
    }

    public void setValrepLandimpComp4Shape(String valrepLandimpComp4Shape) {
        this.valrepLandimpComp4Shape = valrepLandimpComp4Shape;
    }

    public String getValrepLandimpComp4Source() {
        return NULL_STRING_SETTER(valrepLandimpComp4Source);
    }

    public void setValrepLandimpComp4Source(String valrepLandimpComp4Source) {
        this.valrepLandimpComp4Source = valrepLandimpComp4Source;
    }

    public String getValrepLandimpComp4TotalAdj() {
        return NULL_STRING_SETTER(valrepLandimpComp4TotalAdj);
    }

    public void setValrepLandimpComp4TotalAdj(String valrepLandimpComp4TotalAdj) {
        this.valrepLandimpComp4TotalAdj = valrepLandimpComp4TotalAdj;
    }

    public String getValrepLandimpComp4TotalgrossAdj() {
        return NULL_STRING_SETTER(valrepLandimpComp4TotalgrossAdj);
    }

    public void setValrepLandimpComp4TotalgrossAdj(String valrepLandimpComp4TotalgrossAdj) {
        this.valrepLandimpComp4TotalgrossAdj = valrepLandimpComp4TotalgrossAdj;
    }

    public String getValrepLandimpComp4TotalnetAdj() {
        return NULL_STRING_SETTER(valrepLandimpComp4TotalnetAdj);
    }

    public void setValrepLandimpComp4TotalnetAdj(String valrepLandimpComp4TotalnetAdj) {
        this.valrepLandimpComp4TotalnetAdj = valrepLandimpComp4TotalnetAdj;
    }

    public String getValrepLandimpComp4Weight() {
        return NULL_STRING_SETTER(valrepLandimpComp4Weight);
    }

    public void setValrepLandimpComp4Weight(String valrepLandimpComp4Weight) {
        this.valrepLandimpComp4Weight = valrepLandimpComp4Weight;
    }

    public String getValrepLandimpComp4WeightEquivalent() {
        return NULL_STRING_SETTER(valrepLandimpComp4WeightEquivalent);
    }

    public void setValrepLandimpComp4WeightEquivalent(String valrepLandimpComp4WeightEquivalent) {
        this.valrepLandimpComp4WeightEquivalent = valrepLandimpComp4WeightEquivalent;
    }

    public String getValrepLandimpComp4WeightedValue() {
        return NULL_STRING_SETTER(valrepLandimpComp4WeightedValue);
    }

    public void setValrepLandimpComp4WeightedValue(String valrepLandimpComp4WeightedValue) {
        this.valrepLandimpComp4WeightedValue = valrepLandimpComp4WeightedValue;
    }

    public String getValrepLandimpComp4WgtDist() {
        return NULL_STRING_SETTER(valrepLandimpComp4WgtDist);
    }

    public void setValrepLandimpComp4WgtDist(String valrepLandimpComp4WgtDist) {
        this.valrepLandimpComp4WgtDist = valrepLandimpComp4WgtDist;
    }

    public String getValrepLandimpComp4Zoning() {
        return NULL_STRING_SETTER(valrepLandimpComp4Zoning);
    }

    public void setValrepLandimpComp4Zoning(String valrepLandimpComp4Zoning) {
        this.valrepLandimpComp4Zoning = valrepLandimpComp4Zoning;
    }

    public String getValrepLandimpComp5AdjustablePriceSqm() {
        return NULL_STRING_SETTER(valrepLandimpComp5AdjustablePriceSqm);
    }

    public void setValrepLandimpComp5AdjustablePriceSqm(String valrepLandimpComp5AdjustablePriceSqm) {
        this.valrepLandimpComp5AdjustablePriceSqm = valrepLandimpComp5AdjustablePriceSqm;
    }

    public String getValrepLandimpComp5AdjustedValue() {
        return NULL_STRING_SETTER(valrepLandimpComp5AdjustedValue);
    }

    public void setValrepLandimpComp5AdjustedValue(String valrepLandimpComp5AdjustedValue) {
        this.valrepLandimpComp5AdjustedValue = valrepLandimpComp5AdjustedValue;
    }

    public String getValrepLandimpComp5ApproxAge() {
        return NULL_STRING_SETTER(valrepLandimpComp5ApproxAge);
    }

    public void setValrepLandimpComp5ApproxAge(String valrepLandimpComp5ApproxAge) {
        this.valrepLandimpComp5ApproxAge = valrepLandimpComp5ApproxAge;
    }

    public String getValrepLandimpComp5Area() {
        return NULL_STRING_SETTER(valrepLandimpComp5Area);
    }

    public void setValrepLandimpComp5Area(String valrepLandimpComp5Area) {
        this.valrepLandimpComp5Area = valrepLandimpComp5Area;
    }

    public String getValrepLandimpComp5BasePrice() {
        return NULL_STRING_SETTER(valrepLandimpComp5BasePrice);
    }

    public void setValrepLandimpComp5BasePrice(String valrepLandimpComp5BasePrice) {
        this.valrepLandimpComp5BasePrice = valrepLandimpComp5BasePrice;
    }

    public String getValrepLandimpComp5BasePriceSqm() {
        return NULL_STRING_SETTER(valrepLandimpComp5BasePriceSqm);
    }

    public void setValrepLandimpComp5BasePriceSqm(String valrepLandimpComp5BasePriceSqm) {
        this.valrepLandimpComp5BasePriceSqm = valrepLandimpComp5BasePriceSqm;
    }

    public String getValrepLandimpComp5BldgArea() {
        return NULL_STRING_SETTER(valrepLandimpComp5BldgArea);
    }

    public void setValrepLandimpComp5BldgArea(String valrepLandimpComp5BldgArea) {
        this.valrepLandimpComp5BldgArea = valrepLandimpComp5BldgArea;
    }

    public String getValrepLandimpComp5BldgDepreciation() {
        return NULL_STRING_SETTER(valrepLandimpComp5BldgDepreciation);
    }

    public void setValrepLandimpComp5BldgDepreciation(String valrepLandimpComp5BldgDepreciation) {
        this.valrepLandimpComp5BldgDepreciation = valrepLandimpComp5BldgDepreciation;
    }

    public String getValrepLandimpComp5BldgDepreciationVal() {
        return NULL_STRING_SETTER(valrepLandimpComp5BldgDepreciationVal);
    }

    public void setValrepLandimpComp5BldgDepreciationVal(String valrepLandimpComp5BldgDepreciationVal) {
        this.valrepLandimpComp5BldgDepreciationVal = valrepLandimpComp5BldgDepreciationVal;
    }

    public String getValrepLandimpComp5BldgReplacementUnitValue() {
        return NULL_STRING_SETTER(valrepLandimpComp5BldgReplacementUnitValue);
    }

    public void setValrepLandimpComp5BldgReplacementUnitValue(String valrepLandimpComp5BldgReplacementUnitValue) {
        this.valrepLandimpComp5BldgReplacementUnitValue = valrepLandimpComp5BldgReplacementUnitValue;
    }

    public String getValrepLandimpComp5BldgReplacementValue() {
        return NULL_STRING_SETTER(valrepLandimpComp5BldgReplacementValue);
    }

    public void setValrepLandimpComp5BldgReplacementValue(String valrepLandimpComp5BldgReplacementValue) {
        this.valrepLandimpComp5BldgReplacementValue = valrepLandimpComp5BldgReplacementValue;
    }

    public String getValrepLandimpComp5Characteristic() {
        return NULL_STRING_SETTER(valrepLandimpComp5Characteristic);
    }

    public void setValrepLandimpComp5Characteristic(String valrepLandimpComp5Characteristic) {
        this.valrepLandimpComp5Characteristic = valrepLandimpComp5Characteristic;
    }

    public String getValrepLandimpComp5Characteristics() {
        return NULL_STRING_SETTER(valrepLandimpComp5Characteristics);
    }

    public void setValrepLandimpComp5Characteristics(String valrepLandimpComp5Characteristics) {
        this.valrepLandimpComp5Characteristics = valrepLandimpComp5Characteristics;
    }

    public String getValrepLandimpComp5ConcludedAdjustment() {
        return NULL_STRING_SETTER(valrepLandimpComp5ConcludedAdjustment);
    }

    public void setValrepLandimpComp5ConcludedAdjustment(String valrepLandimpComp5ConcludedAdjustment) {
        this.valrepLandimpComp5ConcludedAdjustment = valrepLandimpComp5ConcludedAdjustment;
    }

    public String getValrepLandimpComp5ContactNo() {
        return NULL_STRING_SETTER(valrepLandimpComp5ContactNo);
    }

    public void setValrepLandimpComp5ContactNo(String valrepLandimpComp5ContactNo) {
        this.valrepLandimpComp5ContactNo = valrepLandimpComp5ContactNo;
    }

    public String getValrepLandimpComp5DateDay() {
        return NULL_STRING_SETTER(valrepLandimpComp5DateDay);
    }

    public void setValrepLandimpComp5DateDay(String valrepLandimpComp5DateDay) {
        this.valrepLandimpComp5DateDay = valrepLandimpComp5DateDay;
    }

    public String getValrepLandimpComp5DateMonth() {
        return NULL_STRING_SETTER(valrepLandimpComp5DateMonth);
    }

    public void setValrepLandimpComp5DateMonth(String valrepLandimpComp5DateMonth) {
        this.valrepLandimpComp5DateMonth = valrepLandimpComp5DateMonth;
    }

    public String getValrepLandimpComp5DateYear() {
        return NULL_STRING_SETTER(valrepLandimpComp5DateYear);
    }

    public void setValrepLandimpComp5DateYear(String valrepLandimpComp5DateYear) {
        this.valrepLandimpComp5DateYear = valrepLandimpComp5DateYear;
    }

    public String getValrepLandimpComp5DiscountRate() {
        return NULL_STRING_SETTER(valrepLandimpComp5DiscountRate);
    }

    public void setValrepLandimpComp5DiscountRate(String valrepLandimpComp5DiscountRate) {
        this.valrepLandimpComp5DiscountRate = valrepLandimpComp5DiscountRate;
    }

    public String getValrepLandimpComp5Discounts() {
        return NULL_STRING_SETTER(valrepLandimpComp5Discounts);
    }

    public void setValrepLandimpComp5Discounts(String valrepLandimpComp5Discounts) {
        this.valrepLandimpComp5Discounts = valrepLandimpComp5Discounts;
    }

    public String getValrepLandimpComp5EffectiveDate() {
        return NULL_STRING_SETTER(valrepLandimpComp5EffectiveDate);
    }

    public void setValrepLandimpComp5EffectiveDate(String valrepLandimpComp5EffectiveDate) {
        this.valrepLandimpComp5EffectiveDate = valrepLandimpComp5EffectiveDate;
    }

    public String getValrepLandimpComp5LandResidualValue() {
        return NULL_STRING_SETTER(valrepLandimpComp5LandResidualValue);
    }

    public void setValrepLandimpComp5LandResidualValue(String valrepLandimpComp5LandResidualValue) {
        this.valrepLandimpComp5LandResidualValue = valrepLandimpComp5LandResidualValue;
    }

    public String getValrepLandimpComp5Location() {
        return NULL_STRING_SETTER(valrepLandimpComp5Location);
    }

    public void setValrepLandimpComp5Location(String valrepLandimpComp5Location) {
        this.valrepLandimpComp5Location = valrepLandimpComp5Location;
    }

    public String getValrepLandimpComp5NetBldgValue() {
        return NULL_STRING_SETTER(valrepLandimpComp5NetBldgValue);
    }

    public void setValrepLandimpComp5NetBldgValue(String valrepLandimpComp5NetBldgValue) {
        this.valrepLandimpComp5NetBldgValue = valrepLandimpComp5NetBldgValue;
    }

    public String getValrepLandimpComp5NetLandValuation() {
        return NULL_STRING_SETTER(valrepLandimpComp5NetLandValuation);
    }

    public void setValrepLandimpComp5NetLandValuation(String valrepLandimpComp5NetLandValuation) {
        this.valrepLandimpComp5NetLandValuation = valrepLandimpComp5NetLandValuation;
    }

    public String getValrepLandimpComp5PriceSqm() {
        return NULL_STRING_SETTER(valrepLandimpComp5PriceSqm);
    }

    public void setValrepLandimpComp5PriceSqm(String valrepLandimpComp5PriceSqm) {
        this.valrepLandimpComp5PriceSqm = valrepLandimpComp5PriceSqm;
    }

    public String getValrepLandimpComp5PriceValue() {
        return NULL_STRING_SETTER(valrepLandimpComp5PriceValue);
    }

    public void setValrepLandimpComp5PriceValue(String valrepLandimpComp5PriceValue) {
        this.valrepLandimpComp5PriceValue = valrepLandimpComp5PriceValue;
    }

    public String getValrepLandimpComp5PricingCircumstance() {
        return NULL_STRING_SETTER(valrepLandimpComp5PricingCircumstance);
    }

    public void setValrepLandimpComp5PricingCircumstance(String valrepLandimpComp5PricingCircumstance) {
        this.valrepLandimpComp5PricingCircumstance = valrepLandimpComp5PricingCircumstance;
    }

    public String getValrepLandimpComp5PropInterest() {
        return NULL_STRING_SETTER(valrepLandimpComp5PropInterest);
    }

    public void setValrepLandimpComp5PropInterest(String valrepLandimpComp5PropInterest) {
        this.valrepLandimpComp5PropInterest = valrepLandimpComp5PropInterest;
    }

    public String getValrepLandimpComp5PropertyType() {
        return NULL_STRING_SETTER(valrepLandimpComp5PropertyType);
    }

    public void setValrepLandimpComp5PropertyType(String valrepLandimpComp5PropertyType) {
        this.valrepLandimpComp5PropertyType = valrepLandimpComp5PropertyType;
    }

    public String getValrepLandimpComp5RecAccessibility() {
        return NULL_STRING_SETTER(valrepLandimpComp5RecAccessibility);
    }

    public void setValrepLandimpComp5RecAccessibility(String valrepLandimpComp5RecAccessibility) {
        this.valrepLandimpComp5RecAccessibility = valrepLandimpComp5RecAccessibility;
    }

    public String getValrepLandimpComp5RecAccessibilityDesc() {
        return NULL_STRING_SETTER(valrepLandimpComp5RecAccessibilityDesc);
    }

    public void setValrepLandimpComp5RecAccessibilityDesc(String valrepLandimpComp5RecAccessibilityDesc) {
        this.valrepLandimpComp5RecAccessibilityDesc = valrepLandimpComp5RecAccessibilityDesc;
    }

    public String getValrepLandimpComp5RecAccessibilityDescription() {
        return NULL_STRING_SETTER(valrepLandimpComp5RecAccessibilityDescription);
    }

    public void setValrepLandimpComp5RecAccessibilityDescription(String valrepLandimpComp5RecAccessibilityDescription) {
        this.valrepLandimpComp5RecAccessibilityDescription = valrepLandimpComp5RecAccessibilityDescription;
    }

    public String getValrepLandimpComp5RecAmenities() {
        return NULL_STRING_SETTER(valrepLandimpComp5RecAmenities);
    }

    public void setValrepLandimpComp5RecAmenities(String valrepLandimpComp5RecAmenities) {
        this.valrepLandimpComp5RecAmenities = valrepLandimpComp5RecAmenities;
    }

    public String getValrepLandimpComp5RecBargainingAllow() {
        return NULL_STRING_SETTER(valrepLandimpComp5RecBargainingAllow);
    }

    public void setValrepLandimpComp5RecBargainingAllow(String valrepLandimpComp5RecBargainingAllow) {
        this.valrepLandimpComp5RecBargainingAllow = valrepLandimpComp5RecBargainingAllow;
    }

    public String getValrepLandimpComp5RecBargainingAllowAdjustmentPrice() {
        return NULL_STRING_SETTER(valrepLandimpComp5RecBargainingAllowAdjustmentPrice);
    }

    public void setValrepLandimpComp5RecBargainingAllowAdjustmentPrice(String valrepLandimpComp5RecBargainingAllowAdjustmentPrice) {
        this.valrepLandimpComp5RecBargainingAllowAdjustmentPrice = valrepLandimpComp5RecBargainingAllowAdjustmentPrice;
    }

    public String getValrepLandimpComp5RecBargainingAllowDesc() {
        return NULL_STRING_SETTER(valrepLandimpComp5RecBargainingAllowDesc);
    }

    public void setValrepLandimpComp5RecBargainingAllowDesc(String valrepLandimpComp5RecBargainingAllowDesc) {
        this.valrepLandimpComp5RecBargainingAllowDesc = valrepLandimpComp5RecBargainingAllowDesc;
    }

    public String getValrepLandimpComp5RecBargainingAllowDescription() {
        return NULL_STRING_SETTER(valrepLandimpComp5RecBargainingAllowDescription);
    }

    public void setValrepLandimpComp5RecBargainingAllowDescription(String valrepLandimpComp5RecBargainingAllowDescription) {
        this.valrepLandimpComp5RecBargainingAllowDescription = valrepLandimpComp5RecBargainingAllowDescription;
    }

    public String getValrepLandimpComp5RecBargainingAllowance() {
        return NULL_STRING_SETTER(valrepLandimpComp5RecBargainingAllowance);
    }

    public void setValrepLandimpComp5RecBargainingAllowance(String valrepLandimpComp5RecBargainingAllowance) {
        this.valrepLandimpComp5RecBargainingAllowance = valrepLandimpComp5RecBargainingAllowance;
    }

    public String getValrepLandimpComp5RecBargainingAllowanceDesc() {
        return NULL_STRING_SETTER(valrepLandimpComp5RecBargainingAllowanceDesc);
    }

    public void setValrepLandimpComp5RecBargainingAllowanceDesc(String valrepLandimpComp5RecBargainingAllowanceDesc) {
        this.valrepLandimpComp5RecBargainingAllowanceDesc = valrepLandimpComp5RecBargainingAllowanceDesc;
    }

    public String getValrepLandimpComp5RecCornerInfluence() {
        return NULL_STRING_SETTER(valrepLandimpComp5RecCornerInfluence);
    }

    public void setValrepLandimpComp5RecCornerInfluence(String valrepLandimpComp5RecCornerInfluence) {
        this.valrepLandimpComp5RecCornerInfluence = valrepLandimpComp5RecCornerInfluence;
    }

    public String getValrepLandimpComp5RecDegreeOfDevt() {
        return NULL_STRING_SETTER(valrepLandimpComp5RecDegreeOfDevt);
    }

    public void setValrepLandimpComp5RecDegreeOfDevt(String valrepLandimpComp5RecDegreeOfDevt) {
        this.valrepLandimpComp5RecDegreeOfDevt = valrepLandimpComp5RecDegreeOfDevt;
    }

    public String getValrepLandimpComp5RecElevation() {
        return NULL_STRING_SETTER(valrepLandimpComp5RecElevation);
    }

    public void setValrepLandimpComp5RecElevation(String valrepLandimpComp5RecElevation) {
        this.valrepLandimpComp5RecElevation = valrepLandimpComp5RecElevation;
    }

    public String getValrepLandimpComp5RecElevationDesc() {
        return NULL_STRING_SETTER(valrepLandimpComp5RecElevationDesc);
    }

    public void setValrepLandimpComp5RecElevationDesc(String valrepLandimpComp5RecElevationDesc) {
        this.valrepLandimpComp5RecElevationDesc = valrepLandimpComp5RecElevationDesc;
    }

    public String getValrepLandimpComp5RecElevationDescription() {
        return NULL_STRING_SETTER(valrepLandimpComp5RecElevationDescription);
    }

    public void setValrepLandimpComp5RecElevationDescription(String valrepLandimpComp5RecElevationDescription) {
        this.valrepLandimpComp5RecElevationDescription = valrepLandimpComp5RecElevationDescription;
    }

    public String getValrepLandimpComp5RecLocation() {
        return NULL_STRING_SETTER(valrepLandimpComp5RecLocation);
    }

    public void setValrepLandimpComp5RecLocation(String valrepLandimpComp5RecLocation) {
        this.valrepLandimpComp5RecLocation = valrepLandimpComp5RecLocation;
    }

    public String getValrepLandimpComp5RecLocationDesc() {
        return NULL_STRING_SETTER(valrepLandimpComp5RecLocationDesc);
    }

    public void setValrepLandimpComp5RecLocationDesc(String valrepLandimpComp5RecLocationDesc) {
        this.valrepLandimpComp5RecLocationDesc = valrepLandimpComp5RecLocationDesc;
    }

    public String getValrepLandimpComp5RecLocationDescription() {
        return NULL_STRING_SETTER(valrepLandimpComp5RecLocationDescription);
    }

    public void setValrepLandimpComp5RecLocationDescription(String valrepLandimpComp5RecLocationDescription) {
        this.valrepLandimpComp5RecLocationDescription = valrepLandimpComp5RecLocationDescription;
    }

    public String getValrepLandimpComp5RecLotType() {
        return NULL_STRING_SETTER(valrepLandimpComp5RecLotType);
    }

    public void setValrepLandimpComp5RecLotType(String valrepLandimpComp5RecLotType) {
        this.valrepLandimpComp5RecLotType = valrepLandimpComp5RecLotType;
    }

    public String getValrepLandimpComp5RecLotTypeDesc() {
        return NULL_STRING_SETTER(valrepLandimpComp5RecLotTypeDesc);
    }

    public void setValrepLandimpComp5RecLotTypeDesc(String valrepLandimpComp5RecLotTypeDesc) {
        this.valrepLandimpComp5RecLotTypeDesc = valrepLandimpComp5RecLotTypeDesc;
    }

    public String getValrepLandimpComp5RecLotTypeDescription() {
        return NULL_STRING_SETTER(valrepLandimpComp5RecLotTypeDescription);
    }

    public void setValrepLandimpComp5RecLotTypeDescription(String valrepLandimpComp5RecLotTypeDescription) {
        this.valrepLandimpComp5RecLotTypeDescription = valrepLandimpComp5RecLotTypeDescription;
    }

    public String getValrepLandimpComp5RecNeighborhood() {
        return NULL_STRING_SETTER(valrepLandimpComp5RecNeighborhood);
    }

    public void setValrepLandimpComp5RecNeighborhood(String valrepLandimpComp5RecNeighborhood) {
        this.valrepLandimpComp5RecNeighborhood = valrepLandimpComp5RecNeighborhood;
    }

    public String getValrepLandimpComp5RecNeighborhoodDesc() {
        return NULL_STRING_SETTER(valrepLandimpComp5RecNeighborhoodDesc);
    }

    public void setValrepLandimpComp5RecNeighborhoodDesc(String valrepLandimpComp5RecNeighborhoodDesc) {
        this.valrepLandimpComp5RecNeighborhoodDesc = valrepLandimpComp5RecNeighborhoodDesc;
    }

    public String getValrepLandimpComp5RecNeighborhoodDescription() {
        return NULL_STRING_SETTER(valrepLandimpComp5RecNeighborhoodDescription);
    }

    public void setValrepLandimpComp5RecNeighborhoodDescription(String valrepLandimpComp5RecNeighborhoodDescription) {
        this.valrepLandimpComp5RecNeighborhoodDescription = valrepLandimpComp5RecNeighborhoodDescription;
    }

    public String getValrepLandimpComp5RecNeighborhoodType() {
        return NULL_STRING_SETTER(valrepLandimpComp5RecNeighborhoodType);
    }

    public void setValrepLandimpComp5RecNeighborhoodType(String valrepLandimpComp5RecNeighborhoodType) {
        this.valrepLandimpComp5RecNeighborhoodType = valrepLandimpComp5RecNeighborhoodType;
    }

    public String getValrepLandimpComp5RecOthers() {
        return NULL_STRING_SETTER(valrepLandimpComp5RecOthers);
    }

    public void setValrepLandimpComp5RecOthers(String valrepLandimpComp5RecOthers) {
        this.valrepLandimpComp5RecOthers = valrepLandimpComp5RecOthers;
    }

    public String getValrepLandimpComp5RecOthers2() {
        return NULL_STRING_SETTER(valrepLandimpComp5RecOthers2);
    }

    public void setValrepLandimpComp5RecOthers2(String valrepLandimpComp5RecOthers2) {
        this.valrepLandimpComp5RecOthers2 = valrepLandimpComp5RecOthers2;
    }

    public String getValrepLandimpComp5RecOthers2Desc() {
        return NULL_STRING_SETTER(valrepLandimpComp5RecOthers2Desc);
    }

    public void setValrepLandimpComp5RecOthers2Desc(String valrepLandimpComp5RecOthers2Desc) {
        this.valrepLandimpComp5RecOthers2Desc = valrepLandimpComp5RecOthers2Desc;
    }

    public String getValrepLandimpComp5RecOthers2Description() {
        return NULL_STRING_SETTER(valrepLandimpComp5RecOthers2Description);
    }

    public void setValrepLandimpComp5RecOthers2Description(String valrepLandimpComp5RecOthers2Description) {
        this.valrepLandimpComp5RecOthers2Description = valrepLandimpComp5RecOthers2Description;
    }

    public String getValrepLandimpComp5RecOthers3() {
        return NULL_STRING_SETTER(valrepLandimpComp5RecOthers3);
    }

    public void setValrepLandimpComp5RecOthers3(String valrepLandimpComp5RecOthers3) {
        this.valrepLandimpComp5RecOthers3 = valrepLandimpComp5RecOthers3;
    }

    public String getValrepLandimpComp5RecOthers3Desc() {
        return NULL_STRING_SETTER(valrepLandimpComp5RecOthers3Desc);
    }

    public void setValrepLandimpComp5RecOthers3Desc(String valrepLandimpComp5RecOthers3Desc) {
        this.valrepLandimpComp5RecOthers3Desc = valrepLandimpComp5RecOthers3Desc;
    }

    public String getValrepLandimpComp5RecOthers3Description() {
        return NULL_STRING_SETTER(valrepLandimpComp5RecOthers3Description);
    }

    public void setValrepLandimpComp5RecOthers3Description(String valrepLandimpComp5RecOthers3Description) {
        this.valrepLandimpComp5RecOthers3Description = valrepLandimpComp5RecOthers3Description;
    }

    public String getValrepLandimpComp5RecOthersDesc() {
        return NULL_STRING_SETTER(valrepLandimpComp5RecOthersDesc);
    }

    public void setValrepLandimpComp5RecOthersDesc(String valrepLandimpComp5RecOthersDesc) {
        this.valrepLandimpComp5RecOthersDesc = valrepLandimpComp5RecOthersDesc;
    }

    public String getValrepLandimpComp5RecOthersDescription() {
        return NULL_STRING_SETTER(valrepLandimpComp5RecOthersDescription);
    }

    public void setValrepLandimpComp5RecOthersDescription(String valrepLandimpComp5RecOthersDescription) {
        this.valrepLandimpComp5RecOthersDescription = valrepLandimpComp5RecOthersDescription;
    }

    public String getValrepLandimpComp5RecOthersName() {
        return NULL_STRING_SETTER(valrepLandimpComp5RecOthersName);
    }

    public void setValrepLandimpComp5RecOthersName(String valrepLandimpComp5RecOthersName) {
        this.valrepLandimpComp5RecOthersName = valrepLandimpComp5RecOthersName;
    }

    public String getValrepLandimpComp5RecPropRights() {
        return NULL_STRING_SETTER(valrepLandimpComp5RecPropRights);
    }

    public void setValrepLandimpComp5RecPropRights(String valrepLandimpComp5RecPropRights) {
        this.valrepLandimpComp5RecPropRights = valrepLandimpComp5RecPropRights;
    }

    public String getValrepLandimpComp5RecPropRightsDesc() {
        return NULL_STRING_SETTER(valrepLandimpComp5RecPropRightsDesc);
    }

    public void setValrepLandimpComp5RecPropRightsDesc(String valrepLandimpComp5RecPropRightsDesc) {
        this.valrepLandimpComp5RecPropRightsDesc = valrepLandimpComp5RecPropRightsDesc;
    }

    public String getValrepLandimpComp5RecPropertyRightsConveyed() {
        return NULL_STRING_SETTER(valrepLandimpComp5RecPropertyRightsConveyed);
    }

    public void setValrepLandimpComp5RecPropertyRightsConveyed(String valrepLandimpComp5RecPropertyRightsConveyed) {
        this.valrepLandimpComp5RecPropertyRightsConveyed = valrepLandimpComp5RecPropertyRightsConveyed;
    }

    public String getValrepLandimpComp5RecPropertyRightsConveyedAdjustmentPrice() {
        return NULL_STRING_SETTER(valrepLandimpComp5RecPropertyRightsConveyedAdjustmentPrice);
    }

    public void setValrepLandimpComp5RecPropertyRightsConveyedAdjustmentPrice(String valrepLandimpComp5RecPropertyRightsConveyedAdjustmentPrice) {
        this.valrepLandimpComp5RecPropertyRightsConveyedAdjustmentPrice = valrepLandimpComp5RecPropertyRightsConveyedAdjustmentPrice;
    }

    public String getValrepLandimpComp5RecPropertyRightsConveyedDesc() {
        return NULL_STRING_SETTER(valrepLandimpComp5RecPropertyRightsConveyedDesc);
    }

    public void setValrepLandimpComp5RecPropertyRightsConveyedDesc(String valrepLandimpComp5RecPropertyRightsConveyedDesc) {
        this.valrepLandimpComp5RecPropertyRightsConveyedDesc = valrepLandimpComp5RecPropertyRightsConveyedDesc;
    }

    public String getValrepLandimpComp5RecPropertyRightsConveyedDescription() {
        return NULL_STRING_SETTER(valrepLandimpComp5RecPropertyRightsConveyedDescription);
    }

    public void setValrepLandimpComp5RecPropertyRightsConveyedDescription(String valrepLandimpComp5RecPropertyRightsConveyedDescription) {
        this.valrepLandimpComp5RecPropertyRightsConveyedDescription = valrepLandimpComp5RecPropertyRightsConveyedDescription;
    }

    public String getValrepLandimpComp5RecRoad() {
        return NULL_STRING_SETTER(valrepLandimpComp5RecRoad);
    }

    public void setValrepLandimpComp5RecRoad(String valrepLandimpComp5RecRoad) {
        this.valrepLandimpComp5RecRoad = valrepLandimpComp5RecRoad;
    }

    public String getValrepLandimpComp5RecRoadDesc() {
        return NULL_STRING_SETTER(valrepLandimpComp5RecRoadDesc);
    }

    public void setValrepLandimpComp5RecRoadDesc(String valrepLandimpComp5RecRoadDesc) {
        this.valrepLandimpComp5RecRoadDesc = valrepLandimpComp5RecRoadDesc;
    }

    public String getValrepLandimpComp5RecRoadDescription() {
        return NULL_STRING_SETTER(valrepLandimpComp5RecRoadDescription);
    }

    public void setValrepLandimpComp5RecRoadDescription(String valrepLandimpComp5RecRoadDescription) {
        this.valrepLandimpComp5RecRoadDescription = valrepLandimpComp5RecRoadDescription;
    }

    public String getValrepLandimpComp5RecShape() {
        return NULL_STRING_SETTER(valrepLandimpComp5RecShape);
    }

    public void setValrepLandimpComp5RecShape(String valrepLandimpComp5RecShape) {
        this.valrepLandimpComp5RecShape = valrepLandimpComp5RecShape;
    }

    public String getValrepLandimpComp5RecShapeDesc() {
        return NULL_STRING_SETTER(valrepLandimpComp5RecShapeDesc);
    }

    public void setValrepLandimpComp5RecShapeDesc(String valrepLandimpComp5RecShapeDesc) {
        this.valrepLandimpComp5RecShapeDesc = valrepLandimpComp5RecShapeDesc;
    }

    public String getValrepLandimpComp5RecShapeDescription() {
        return NULL_STRING_SETTER(valrepLandimpComp5RecShapeDescription);
    }

    public void setValrepLandimpComp5RecShapeDescription(String valrepLandimpComp5RecShapeDescription) {
        this.valrepLandimpComp5RecShapeDescription = valrepLandimpComp5RecShapeDescription;
    }

    public String getValrepLandimpComp5RecSize() {
        return NULL_STRING_SETTER(valrepLandimpComp5RecSize);
    }

    public void setValrepLandimpComp5RecSize(String valrepLandimpComp5RecSize) {
        this.valrepLandimpComp5RecSize = valrepLandimpComp5RecSize;
    }

    public String getValrepLandimpComp5RecSizeDesc() {
        return NULL_STRING_SETTER(valrepLandimpComp5RecSizeDesc);
    }

    public void setValrepLandimpComp5RecSizeDesc(String valrepLandimpComp5RecSizeDesc) {
        this.valrepLandimpComp5RecSizeDesc = valrepLandimpComp5RecSizeDesc;
    }

    public String getValrepLandimpComp5RecSizeDescription() {
        return NULL_STRING_SETTER(valrepLandimpComp5RecSizeDescription);
    }

    public void setValrepLandimpComp5RecSizeDescription(String valrepLandimpComp5RecSizeDescription) {
        this.valrepLandimpComp5RecSizeDescription = valrepLandimpComp5RecSizeDescription;
    }

    public String getValrepLandimpComp5RecTerrain() {
        return NULL_STRING_SETTER(valrepLandimpComp5RecTerrain);
    }

    public void setValrepLandimpComp5RecTerrain(String valrepLandimpComp5RecTerrain) {
        this.valrepLandimpComp5RecTerrain = valrepLandimpComp5RecTerrain;
    }

    public String getValrepLandimpComp5RecTerrainDesc() {
        return NULL_STRING_SETTER(valrepLandimpComp5RecTerrainDesc);
    }

    public void setValrepLandimpComp5RecTerrainDesc(String valrepLandimpComp5RecTerrainDesc) {
        this.valrepLandimpComp5RecTerrainDesc = valrepLandimpComp5RecTerrainDesc;
    }

    public String getValrepLandimpComp5RecTerrainDescription() {
        return NULL_STRING_SETTER(valrepLandimpComp5RecTerrainDescription);
    }

    public void setValrepLandimpComp5RecTerrainDescription(String valrepLandimpComp5RecTerrainDescription) {
        this.valrepLandimpComp5RecTerrainDescription = valrepLandimpComp5RecTerrainDescription;
    }

    public String getValrepLandimpComp5RecTime() {
        return NULL_STRING_SETTER(valrepLandimpComp5RecTime);
    }

    public void setValrepLandimpComp5RecTime(String valrepLandimpComp5RecTime) {
        this.valrepLandimpComp5RecTime = valrepLandimpComp5RecTime;
    }

    public String getValrepLandimpComp5RecTimeAdjustmentPrice() {
        return NULL_STRING_SETTER(valrepLandimpComp5RecTimeAdjustmentPrice);
    }

    public void setValrepLandimpComp5RecTimeAdjustmentPrice(String valrepLandimpComp5RecTimeAdjustmentPrice) {
        this.valrepLandimpComp5RecTimeAdjustmentPrice = valrepLandimpComp5RecTimeAdjustmentPrice;
    }

    public String getValrepLandimpComp5RecTimeDesc() {
        return NULL_STRING_SETTER(valrepLandimpComp5RecTimeDesc);
    }

    public void setValrepLandimpComp5RecTimeDesc(String valrepLandimpComp5RecTimeDesc) {
        this.valrepLandimpComp5RecTimeDesc = valrepLandimpComp5RecTimeDesc;
    }

    public String getValrepLandimpComp5RecTimeDescription() {
        return NULL_STRING_SETTER(valrepLandimpComp5RecTimeDescription);
    }

    public void setValrepLandimpComp5RecTimeDescription(String valrepLandimpComp5RecTimeDescription) {
        this.valrepLandimpComp5RecTimeDescription = valrepLandimpComp5RecTimeDescription;
    }

    public String getValrepLandimpComp5RecTimeElement() {
        return NULL_STRING_SETTER(valrepLandimpComp5RecTimeElement);
    }

    public void setValrepLandimpComp5RecTimeElement(String valrepLandimpComp5RecTimeElement) {
        this.valrepLandimpComp5RecTimeElement = valrepLandimpComp5RecTimeElement;
    }

    public String getValrepLandimpComp5RecTimeElementDesc() {
        return NULL_STRING_SETTER(valrepLandimpComp5RecTimeElementDesc);
    }

    public void setValrepLandimpComp5RecTimeElementDesc(String valrepLandimpComp5RecTimeElementDesc) {
        this.valrepLandimpComp5RecTimeElementDesc = valrepLandimpComp5RecTimeElementDesc;
    }

    public String getValrepLandimpComp5RecTruLot() {
        return NULL_STRING_SETTER(valrepLandimpComp5RecTruLot);
    }

    public void setValrepLandimpComp5RecTruLot(String valrepLandimpComp5RecTruLot) {
        this.valrepLandimpComp5RecTruLot = valrepLandimpComp5RecTruLot;
    }

    public String getValrepLandimpComp5Remarks() {
        return NULL_STRING_SETTER(valrepLandimpComp5Remarks);
    }

    public void setValrepLandimpComp5Remarks(String valrepLandimpComp5Remarks) {
        this.valrepLandimpComp5Remarks = valrepLandimpComp5Remarks;
    }

    public String getValrepLandimpComp5SellingPrice() {
        return NULL_STRING_SETTER(valrepLandimpComp5SellingPrice);
    }

    public void setValrepLandimpComp5SellingPrice(String valrepLandimpComp5SellingPrice) {
        this.valrepLandimpComp5SellingPrice = valrepLandimpComp5SellingPrice;
    }

    public String getValrepLandimpComp5Shape() {
        return NULL_STRING_SETTER(valrepLandimpComp5Shape);
    }

    public void setValrepLandimpComp5Shape(String valrepLandimpComp5Shape) {
        this.valrepLandimpComp5Shape = valrepLandimpComp5Shape;
    }

    public String getValrepLandimpComp5Source() {
        return NULL_STRING_SETTER(valrepLandimpComp5Source);
    }

    public void setValrepLandimpComp5Source(String valrepLandimpComp5Source) {
        this.valrepLandimpComp5Source = valrepLandimpComp5Source;
    }

    public String getValrepLandimpComp5TotalAdj() {
        return NULL_STRING_SETTER(valrepLandimpComp5TotalAdj);
    }

    public void setValrepLandimpComp5TotalAdj(String valrepLandimpComp5TotalAdj) {
        this.valrepLandimpComp5TotalAdj = valrepLandimpComp5TotalAdj;
    }

    public String getValrepLandimpComp5TotalgrossAdj() {
        return NULL_STRING_SETTER(valrepLandimpComp5TotalgrossAdj);
    }

    public void setValrepLandimpComp5TotalgrossAdj(String valrepLandimpComp5TotalgrossAdj) {
        this.valrepLandimpComp5TotalgrossAdj = valrepLandimpComp5TotalgrossAdj;
    }

    public String getValrepLandimpComp5TotalnetAdj() {
        return NULL_STRING_SETTER(valrepLandimpComp5TotalnetAdj);
    }

    public void setValrepLandimpComp5TotalnetAdj(String valrepLandimpComp5TotalnetAdj) {
        this.valrepLandimpComp5TotalnetAdj = valrepLandimpComp5TotalnetAdj;
    }

    public String getValrepLandimpComp5Weight() {
        return NULL_STRING_SETTER(valrepLandimpComp5Weight);
    }

    public void setValrepLandimpComp5Weight(String valrepLandimpComp5Weight) {
        this.valrepLandimpComp5Weight = valrepLandimpComp5Weight;
    }

    public String getValrepLandimpComp5WeightEquivalent() {
        return NULL_STRING_SETTER(valrepLandimpComp5WeightEquivalent);
    }

    public void setValrepLandimpComp5WeightEquivalent(String valrepLandimpComp5WeightEquivalent) {
        this.valrepLandimpComp5WeightEquivalent = valrepLandimpComp5WeightEquivalent;
    }

    public String getValrepLandimpComp5WeightedValue() {
        return NULL_STRING_SETTER(valrepLandimpComp5WeightedValue);
    }

    public void setValrepLandimpComp5WeightedValue(String valrepLandimpComp5WeightedValue) {
        this.valrepLandimpComp5WeightedValue = valrepLandimpComp5WeightedValue;
    }

    public String getValrepLandimpComp5WgtDist() {
        return NULL_STRING_SETTER(valrepLandimpComp5WgtDist);
    }

    public void setValrepLandimpComp5WgtDist(String valrepLandimpComp5WgtDist) {
        this.valrepLandimpComp5WgtDist = valrepLandimpComp5WgtDist;
    }

    public String getValrepLandimpComp5Zoning() {
        return NULL_STRING_SETTER(valrepLandimpComp5Zoning);
    }

    public void setValrepLandimpComp5Zoning(String valrepLandimpComp5Zoning) {
        this.valrepLandimpComp5Zoning = valrepLandimpComp5Zoning;
    }

    public String getValrepLandimpCompRoundedTo() {
        return NULL_STRING_SETTER(valrepLandimpCompRoundedTo);
    }

    public void setValrepLandimpCompRoundedTo(String valrepLandimpCompRoundedTo) {
        this.valrepLandimpCompRoundedTo = valrepLandimpCompRoundedTo;
    }

    public String getValrepLandimpCompRoundedToNearest() {
        return NULL_STRING_SETTER(valrepLandimpCompRoundedToNearest);
    }

    public void setValrepLandimpCompRoundedToNearest(String valrepLandimpCompRoundedToNearest) {
        this.valrepLandimpCompRoundedToNearest = valrepLandimpCompRoundedToNearest;
    }

    public String getValrepLandimpCompWithCompMaps() {
        return NULL_STRING_SETTER(valrepLandimpCompWithCompMaps);
    }

    public void setValrepLandimpCompWithCompMaps(String valrepLandimpCompWithCompMaps) {
        this.valrepLandimpCompWithCompMaps = valrepLandimpCompWithCompMaps;
    }

    public String getValrepLandimpDateInspectedDay() {
        return NULL_STRING_SETTER(valrepLandimpDateInspectedDay);
    }

    public void setValrepLandimpDateInspectedDay(String valrepLandimpDateInspectedDay) {
        this.valrepLandimpDateInspectedDay = valrepLandimpDateInspectedDay;
    }

    public String getValrepLandimpDateInspectedMonth() {
        return NULL_STRING_SETTER(valrepLandimpDateInspectedMonth);
    }

    public void setValrepLandimpDateInspectedMonth(String valrepLandimpDateInspectedMonth) {
        this.valrepLandimpDateInspectedMonth = valrepLandimpDateInspectedMonth;
    }

    public String getValrepLandimpDateInspectedYear() {
        return NULL_STRING_SETTER(valrepLandimpDateInspectedYear);
    }

    public void setValrepLandimpDateInspectedYear(String valrepLandimpDateInspectedYear) {
        this.valrepLandimpDateInspectedYear = valrepLandimpDateInspectedYear;
    }

    public String getValrepLandimpDescTownClass() {
        return NULL_STRING_SETTER(valrepLandimpDescTownClass);
    }

    public void setValrepLandimpDescTownClass(String valrepLandimpDescTownClass) {
        this.valrepLandimpDescTownClass = valrepLandimpDescTownClass;
    }

    public String getValrepLandimpFinalValueImprovementsAppraisedMv() {
        return NULL_STRING_SETTER(valrepLandimpFinalValueImprovementsAppraisedMv);
    }

    public void setValrepLandimpFinalValueImprovementsAppraisedMv(String valrepLandimpFinalValueImprovementsAppraisedMv) {
        this.valrepLandimpFinalValueImprovementsAppraisedMv = valrepLandimpFinalValueImprovementsAppraisedMv;
    }

    public String getValrepLandimpFinalValueLandAppraisedMv() {
        return NULL_STRING_SETTER(valrepLandimpFinalValueLandAppraisedMv);
    }

    public void setValrepLandimpFinalValueLandAppraisedMv(String valrepLandimpFinalValueLandAppraisedMv) {
        this.valrepLandimpFinalValueLandAppraisedMv = valrepLandimpFinalValueLandAppraisedMv;
    }

    public String getValrepLandimpFinalValueNetAppraisedValue() {
        return NULL_STRING_SETTER(valrepLandimpFinalValueNetAppraisedValue);
    }

    public void setValrepLandimpFinalValueNetAppraisedValue(String valrepLandimpFinalValueNetAppraisedValue) {
        this.valrepLandimpFinalValueNetAppraisedValue = valrepLandimpFinalValueNetAppraisedValue;
    }

    public String getValrepLandimpFinalValueNetAppraisedValueWords() {
        return NULL_STRING_SETTER(valrepLandimpFinalValueNetAppraisedValueWords);
    }

    public void setValrepLandimpFinalValueNetAppraisedValueWords(String valrepLandimpFinalValueNetAppraisedValueWords) {
        this.valrepLandimpFinalValueNetAppraisedValueWords = valrepLandimpFinalValueNetAppraisedValueWords;
    }

    public String getValrepLandimpFinalValueTotalOtherLandImp() {
        return NULL_STRING_SETTER(valrepLandimpFinalValueTotalOtherLandImp);
    }

    public void setValrepLandimpFinalValueTotalOtherLandImp(String valrepLandimpFinalValueTotalOtherLandImp) {
        this.valrepLandimpFinalValueTotalOtherLandImp = valrepLandimpFinalValueTotalOtherLandImp;
    }

    public String getValrepLandimpIdBldgValuation() {
        return NULL_STRING_SETTER(valrepLandimpIdBldgValuation);
    }

    public void setValrepLandimpIdBldgValuation(String valrepLandimpIdBldgValuation) {
        this.valrepLandimpIdBldgValuation = valrepLandimpIdBldgValuation;
    }

    public String getValrepLandimpIdBuildingComponents() {
        return NULL_STRING_SETTER(valrepLandimpIdBuildingComponents);
    }

    public void setValrepLandimpIdBuildingComponents(String valrepLandimpIdBuildingComponents) {
        this.valrepLandimpIdBuildingComponents = valrepLandimpIdBuildingComponents;
    }

    public String getValrepLandimpIdBuildingInspection() {
        return NULL_STRING_SETTER(valrepLandimpIdBuildingInspection);
    }

    public void setValrepLandimpIdBuildingInspection(String valrepLandimpIdBuildingInspection) {
        this.valrepLandimpIdBuildingInspection = valrepLandimpIdBuildingInspection;
    }

    public String getValrepLandimpIdComparison() {
        return NULL_STRING_SETTER(valrepLandimpIdComparison);
    }

    public void setValrepLandimpIdComparison(String valrepLandimpIdComparison) {
        this.valrepLandimpIdComparison = valrepLandimpIdComparison;
    }

    public String getValrepLandimpIdCostEstimate() {
        return NULL_STRING_SETTER(valrepLandimpIdCostEstimate);
    }

    public void setValrepLandimpIdCostEstimate(String valrepLandimpIdCostEstimate) {
        this.valrepLandimpIdCostEstimate = valrepLandimpIdCostEstimate;
    }

    public String getValrepLandimpIdDeedsSale() {
        return NULL_STRING_SETTER(valrepLandimpIdDeedsSale);
    }

    public void setValrepLandimpIdDeedsSale(String valrepLandimpIdDeedsSale) {
        this.valrepLandimpIdDeedsSale = valrepLandimpIdDeedsSale;
    }

    public String getValrepLandimpDepreciatedAnalysis() {
        return valrepLandimpDepreciatedAnalysis;
    }

    public void setValrepLandimpDepreciatedAnalysis(String valrepLandimpDepreciatedAnalysis) {
        this.valrepLandimpDepreciatedAnalysis = valrepLandimpDepreciatedAnalysis;
    }

    public String getValrepLandimpIdEngineeringTests() {
        return NULL_STRING_SETTER(valrepLandimpIdEngineeringTests);
    }

    public void setValrepLandimpIdEngineeringTests(String valrepLandimpIdEngineeringTests) {
        this.valrepLandimpIdEngineeringTests = valrepLandimpIdEngineeringTests;
    }

    public String getValrepLandimpIdGeodeticPlan() {
        return NULL_STRING_SETTER(valrepLandimpIdGeodeticPlan);
    }

    public void setValrepLandimpIdGeodeticPlan(String valrepLandimpIdGeodeticPlan) {
        this.valrepLandimpIdGeodeticPlan = valrepLandimpIdGeodeticPlan;
    }

    public String getValrepLandimpIdGeodeticSurvey() {
        return NULL_STRING_SETTER(valrepLandimpIdGeodeticSurvey);
    }

    public void setValrepLandimpIdGeodeticSurvey(String valrepLandimpIdGeodeticSurvey) {
        this.valrepLandimpIdGeodeticSurvey = valrepLandimpIdGeodeticSurvey;
    }

    public String getValrepLandimpIdLandValuation() {
        return NULL_STRING_SETTER(valrepLandimpIdLandValuation);
    }

    public void setValrepLandimpIdLandValuation(String valrepLandimpIdLandValuation) {
        this.valrepLandimpIdLandValuation = valrepLandimpIdLandValuation;
    }

    public String getValrepLandimpIdLocalBrokers() {
        return NULL_STRING_SETTER(valrepLandimpIdLocalBrokers);
    }

    public void setValrepLandimpIdLocalBrokers(String valrepLandimpIdLocalBrokers) {
        this.valrepLandimpIdLocalBrokers = valrepLandimpIdLocalBrokers;
    }

    public String getValrepLandimpIdLocationalVerification() {
        return NULL_STRING_SETTER(valrepLandimpIdLocationalVerification);
    }

    public void setValrepLandimpIdLocationalVerification(String valrepLandimpIdLocationalVerification) {
        this.valrepLandimpIdLocationalVerification = valrepLandimpIdLocationalVerification;
    }

    public String getValrepLandimpIdLocationalVerificationSubd() {
        return NULL_STRING_SETTER(valrepLandimpIdLocationalVerificationSubd);
    }

    public void setValrepLandimpIdLocationalVerificationSubd(String valrepLandimpIdLocationalVerificationSubd) {
        this.valrepLandimpIdLocationalVerificationSubd = valrepLandimpIdLocationalVerificationSubd;
    }

    public String getValrepLandimpIdLotConfig() {
        return NULL_STRING_SETTER(valrepLandimpIdLotConfig);
    }

    public void setValrepLandimpIdLotConfig(String valrepLandimpIdLotConfig) {
        this.valrepLandimpIdLotConfig = valrepLandimpIdLotConfig;
    }

    public String getValrepLandimpIdMarketAnalysis() {
        return NULL_STRING_SETTER(valrepLandimpIdMarketAnalysis);
    }

    public void setValrepLandimpIdMarketAnalysis(String valrepLandimpIdMarketAnalysis) {
        this.valrepLandimpIdMarketAnalysis = valrepLandimpIdMarketAnalysis;
    }

    public String getValrepLandimpIdMeasurementVerification() {
        return NULL_STRING_SETTER(valrepLandimpIdMeasurementVerification);
    }

    public void setValrepLandimpIdMeasurementVerification(String valrepLandimpIdMeasurementVerification) {
        this.valrepLandimpIdMeasurementVerification = valrepLandimpIdMeasurementVerification;
    }

    public String getValrepLandimpIdMls() {
        return NULL_STRING_SETTER(valrepLandimpIdMls);
    }

    public void setValrepLandimpIdMls(String valrepLandimpIdMls) {
        this.valrepLandimpIdMls = valrepLandimpIdMls;
    }

    public String getValrepLandimpIdNaturalHazards() {
        return NULL_STRING_SETTER(valrepLandimpIdNaturalHazards);
    }

    public void setValrepLandimpIdNaturalHazards(String valrepLandimpIdNaturalHazards) {
        this.valrepLandimpIdNaturalHazards = valrepLandimpIdNaturalHazards;
    }

    public String getValrepLandimpIdNeighborhoodResearch() {
        return NULL_STRING_SETTER(valrepLandimpIdNeighborhoodResearch);
    }

    public void setValrepLandimpIdNeighborhoodResearch(String valrepLandimpIdNeighborhoodResearch) {
        this.valrepLandimpIdNeighborhoodResearch = valrepLandimpIdNeighborhoodResearch;
    }

    public String getValrepLandimpIdOcularInspection() {
        return NULL_STRING_SETTER(valrepLandimpIdOcularInspection);
    }

    public void setValrepLandimpIdOcularInspection(String valrepLandimpIdOcularInspection) {
        this.valrepLandimpIdOcularInspection = valrepLandimpIdOcularInspection;
    }

    public String getValrepLandimpIdOthers() {
        return NULL_STRING_SETTER(valrepLandimpIdOthers);
    }

    public void setValrepLandimpIdOthers(String valrepLandimpIdOthers) {
        this.valrepLandimpIdOthers = valrepLandimpIdOthers;
    }

    public String getValrepLandimpIdPlotting() {
        return NULL_STRING_SETTER(valrepLandimpIdPlotting);
    }

    public void setValrepLandimpIdPlotting(String valrepLandimpIdPlotting) {
        this.valrepLandimpIdPlotting = valrepLandimpIdPlotting;
    }

    public String getValrepLandimpIdQuantitySurvey() {
        return NULL_STRING_SETTER(valrepLandimpIdQuantitySurvey);
    }

    public void setValrepLandimpIdQuantitySurvey(String valrepLandimpIdQuantitySurvey) {
        this.valrepLandimpIdQuantitySurvey = valrepLandimpIdQuantitySurvey;
    }

    public String getValrepLandimpIdSamplingVerification() {
        return NULL_STRING_SETTER(valrepLandimpIdSamplingVerification);
    }

    public void setValrepLandimpIdSamplingVerification(String valrepLandimpIdSamplingVerification) {
        this.valrepLandimpIdSamplingVerification = valrepLandimpIdSamplingVerification;
    }

    public String getValrepLandimpIdSiteAssessment() {
        return NULL_STRING_SETTER(valrepLandimpIdSiteAssessment);
    }

    public void setValrepLandimpIdSiteAssessment(String valrepLandimpIdSiteAssessment) {
        this.valrepLandimpIdSiteAssessment = valrepLandimpIdSiteAssessment;
    }

    public String getValrepLandimpIdSocioPhysicalProfile() {
        return NULL_STRING_SETTER(valrepLandimpIdSocioPhysicalProfile);
    }

    public void setValrepLandimpIdSocioPhysicalProfile(String valrepLandimpIdSocioPhysicalProfile) {
        this.valrepLandimpIdSocioPhysicalProfile = valrepLandimpIdSocioPhysicalProfile;
    }

    public String getValrepLandimpIdSpecialLaws() {
        return NULL_STRING_SETTER(valrepLandimpIdSpecialLaws);
    }

    public void setValrepLandimpIdSpecialLaws(String valrepLandimpIdSpecialLaws) {
        this.valrepLandimpIdSpecialLaws = valrepLandimpIdSpecialLaws;
    }

    public String getValrepLandimpIdStatutoryConstraints() {
        return NULL_STRING_SETTER(valrepLandimpIdStatutoryConstraints);
    }

    public void setValrepLandimpIdStatutoryConstraints(String valrepLandimpIdStatutoryConstraints) {
        this.valrepLandimpIdStatutoryConstraints = valrepLandimpIdStatutoryConstraints;
    }

    public String getValrepLandimpIdStatutoryConstraintsDesc() {
        return NULL_STRING_SETTER(valrepLandimpIdStatutoryConstraintsDesc);
    }

    public void setValrepLandimpIdStatutoryConstraintsDesc(String valrepLandimpIdStatutoryConstraintsDesc) {
        this.valrepLandimpIdStatutoryConstraintsDesc = valrepLandimpIdStatutoryConstraintsDesc;
    }

    public String getValrepLandimpIdSubdMap() {
        return NULL_STRING_SETTER(valrepLandimpIdSubdMap);
    }

    public void setValrepLandimpIdSubdMap(String valrepLandimpIdSubdMap) {
        this.valrepLandimpIdSubdMap = valrepLandimpIdSubdMap;
    }

    public String getValrepLandimpIdSurveyHazardous() {
        return NULL_STRING_SETTER(valrepLandimpIdSurveyHazardous);
    }

    public void setValrepLandimpIdSurveyHazardous(String valrepLandimpIdSurveyHazardous) {
        this.valrepLandimpIdSurveyHazardous = valrepLandimpIdSurveyHazardous;
    }

    public String getValrepLandimpIdTax() {
        return NULL_STRING_SETTER(valrepLandimpIdTax);
    }

    public void setValrepLandimpIdTax(String valrepLandimpIdTax) {
        this.valrepLandimpIdTax = valrepLandimpIdTax;
    }

    public String getValrepLandimpIdTaxDeclaration() {
        return NULL_STRING_SETTER(valrepLandimpIdTaxDeclaration);
    }

    public void setValrepLandimpIdTaxDeclaration(String valrepLandimpIdTaxDeclaration) {
        this.valrepLandimpIdTaxDeclaration = valrepLandimpIdTaxDeclaration;
    }

    public String getValrepLandimpIdTaxMap() {
        return NULL_STRING_SETTER(valrepLandimpIdTaxMap);
    }

    public void setValrepLandimpIdTaxMap(String valrepLandimpIdTaxMap) {
        this.valrepLandimpIdTaxMap = valrepLandimpIdTaxMap;
    }

    public String getValrepLandimpIdTrafficCount() {
        return NULL_STRING_SETTER(valrepLandimpIdTrafficCount);
    }

    public void setValrepLandimpIdTrafficCount(String valrepLandimpIdTrafficCount) {
        this.valrepLandimpIdTrafficCount = valrepLandimpIdTrafficCount;
    }

    public String getValrepLandimpIdTypeOfInterest() {
        return NULL_STRING_SETTER(valrepLandimpIdTypeOfInterest);
    }

    public void setValrepLandimpIdTypeOfInterest(String valrepLandimpIdTypeOfInterest) {
        this.valrepLandimpIdTypeOfInterest = valrepLandimpIdTypeOfInterest;
    }

    public String getValrepLandimpIdTypeOfInterestDesc() {
        return NULL_STRING_SETTER(valrepLandimpIdTypeOfInterestDesc);
    }

    public void setValrepLandimpIdTypeOfInterestDesc(String valrepLandimpIdTypeOfInterestDesc) {
        this.valrepLandimpIdTypeOfInterestDesc = valrepLandimpIdTypeOfInterestDesc;
    }

    public String getValrepLandimpIdTypicalParticipants() {
        return NULL_STRING_SETTER(valrepLandimpIdTypicalParticipants);
    }

    public void setValrepLandimpIdTypicalParticipants(String valrepLandimpIdTypicalParticipants) {
        this.valrepLandimpIdTypicalParticipants = valrepLandimpIdTypicalParticipants;
    }

    public String getValrepLandimpIdUnitInspection() {
        return NULL_STRING_SETTER(valrepLandimpIdUnitInspection);
    }

    public void setValrepLandimpIdUnitInspection(String valrepLandimpIdUnitInspection) {
        this.valrepLandimpIdUnitInspection = valrepLandimpIdUnitInspection;
    }

    public String getValrepLandimpIdUnitSamplingVerification() {
        return NULL_STRING_SETTER(valrepLandimpIdUnitSamplingVerification);
    }

    public void setValrepLandimpIdUnitSamplingVerification(String valrepLandimpIdUnitSamplingVerification) {
        this.valrepLandimpIdUnitSamplingVerification = valrepLandimpIdUnitSamplingVerification;
    }

    public String getValrepLandimpIdUseAnalysis() {
        return NULL_STRING_SETTER(valrepLandimpIdUseAnalysis);
    }

    public void setValrepLandimpIdUseAnalysis(String valrepLandimpIdUseAnalysis) {
        this.valrepLandimpIdUseAnalysis = valrepLandimpIdUseAnalysis;
    }

    public String getValrepLandimpIdUtilitiesServices() {
        return NULL_STRING_SETTER(valrepLandimpIdUtilitiesServices);
    }

    public void setValrepLandimpIdUtilitiesServices(String valrepLandimpIdUtilitiesServices) {
        this.valrepLandimpIdUtilitiesServices = valrepLandimpIdUtilitiesServices;
    }

    public String getValrepLandimpIdValueResearch() {
        return NULL_STRING_SETTER(valrepLandimpIdValueResearch);
    }

    public void setValrepLandimpIdValueResearch(String valrepLandimpIdValueResearch) {
        this.valrepLandimpIdValueResearch = valrepLandimpIdValueResearch;
    }

    public String getValrepLandimpIdZonalValue() {
        return NULL_STRING_SETTER(valrepLandimpIdZonalValue);
    }

    public void setValrepLandimpIdZonalValue(String valrepLandimpIdZonalValue) {
        this.valrepLandimpIdZonalValue = valrepLandimpIdZonalValue;
    }

    public String getValrepLandimpIdZoningCertification() {
        return NULL_STRING_SETTER(valrepLandimpIdZoningCertification);
    }

    public void setValrepLandimpIdZoningCertification(String valrepLandimpIdZoningCertification) {
        this.valrepLandimpIdZoningCertification = valrepLandimpIdZoningCertification;
    }

    public String getValrepLandimpIdZoningVerification() {
        return NULL_STRING_SETTER(valrepLandimpIdZoningVerification);
    }

    public void setValrepLandimpIdZoningVerification(String valrepLandimpIdZoningVerification) {
        this.valrepLandimpIdZoningVerification = valrepLandimpIdZoningVerification;
    }

    public List<ValrepLandimpImpDetail> getValrepLandimpImpDetails() {
        return NULL_LIST_SETTER(valrepLandimpImpDetails);
    }

    public void setValrepLandimpImpDetails(List<ValrepLandimpImpDetail> valrepLandimpImpDetails) {
        this.valrepLandimpImpDetails = valrepLandimpImpDetails;
    }

    public String getValrepLandimpImpDrainage() {
        return NULL_STRING_SETTER(valrepLandimpImpDrainage);
    }

    public void setValrepLandimpImpDrainage(String valrepLandimpImpDrainage) {
        this.valrepLandimpImpDrainage = valrepLandimpImpDrainage;
    }

    public String getValrepLandimpImpRoads() {
        return NULL_STRING_SETTER(valrepLandimpImpRoads);
    }

    public void setValrepLandimpImpRoads(String valrepLandimpImpRoads) {
        this.valrepLandimpImpRoads = valrepLandimpImpRoads;
    }

    public String getValrepLandimpImpSidewalk() {
        return NULL_STRING_SETTER(valrepLandimpImpSidewalk);
    }

    public void setValrepLandimpImpSidewalk(String valrepLandimpImpSidewalk) {
        this.valrepLandimpImpSidewalk = valrepLandimpImpSidewalk;
    }

    public List<ValrepLandimpImpValuation> getValrepLandimpImpValuation() {
        return NULL_LIST_SETTER(valrepLandimpImpValuation);
    }

    public void setValrepLandimpImpValuation(List<ValrepLandimpImpValuation> valrepLandimpImpValuation) {
        this.valrepLandimpImpValuation = valrepLandimpImpValuation;
    }

    public String getValrepLandimpImpValuationRemarks() {
        return NULL_STRING_SETTER(valrepLandimpImpValuationRemarks);
    }

    public void setValrepLandimpImpValuationRemarks(String valrepLandimpImpValuationRemarks) {
        this.valrepLandimpImpValuationRemarks = valrepLandimpImpValuationRemarks;
    }

    public String getValrepLandimpImpValueTotalImpValue() {
        return NULL_STRING_SETTER(valrepLandimpImpValueTotalImpValue);
    }

    public void setValrepLandimpImpValueTotalImpValue(String valrepLandimpImpValueTotalImpValue) {
        this.valrepLandimpImpValueTotalImpValue = valrepLandimpImpValueTotalImpValue;
    }

    public String getValrepLandimpImpValueTotalOtherImpValue() {
        return NULL_STRING_SETTER(valrepLandimpImpValueTotalOtherImpValue);
    }

    public void setValrepLandimpImpValueTotalOtherImpValue(String valrepLandimpImpValueTotalOtherImpValue) {
        this.valrepLandimpImpValueTotalOtherImpValue = valrepLandimpImpValueTotalOtherImpValue;
    }

    public List<ValrepLandimpLotDetail> getValrepLandimpLotDetails() {
        return NULL_LIST_SETTER(valrepLandimpLotDetails);
    }

    public void setValrepLandimpLotDetails(List<ValrepLandimpLotDetail> valrepLandimpLotDetails) {
        this.valrepLandimpLotDetails = valrepLandimpLotDetails;
    }

    public List<ValrepLandimpLotValuation> getValrepLandimpLotValuation() {
        return NULL_LIST_SETTER(valrepLandimpLotValuation);
    }

    public void setValrepLandimpLotValuation(List<ValrepLandimpLotValuation> valrepLandimpLotValuation) {
        this.valrepLandimpLotValuation = valrepLandimpLotValuation;
    }

    public String getValrepLandimpLotclassActualUsage() {
        return NULL_STRING_SETTER(valrepLandimpLotclassActualUsage);
    }

    public void setValrepLandimpLotclassActualUsage(String valrepLandimpLotclassActualUsage) {
        this.valrepLandimpLotclassActualUsage = valrepLandimpLotclassActualUsage;
    }

    public String getValrepLandimpLotclassHighestBestUse() {
        return NULL_STRING_SETTER(valrepLandimpLotclassHighestBestUse);
    }

    public void setValrepLandimpLotclassHighestBestUse(String valrepLandimpLotclassHighestBestUse) {
        this.valrepLandimpLotclassHighestBestUse = valrepLandimpLotclassHighestBestUse;
    }

    public String getValrepLandimpLotclassNeighborhood() {
        return NULL_STRING_SETTER(valrepLandimpLotclassNeighborhood);
    }

    public void setValrepLandimpLotclassNeighborhood(String valrepLandimpLotclassNeighborhood) {
        this.valrepLandimpLotclassNeighborhood = valrepLandimpLotclassNeighborhood;
    }

    public String getValrepLandimpLotclassPerTaxDec() {
        return NULL_STRING_SETTER(valrepLandimpLotclassPerTaxDec);
    }

    public void setValrepLandimpLotclassPerTaxDec(String valrepLandimpLotclassPerTaxDec) {
        this.valrepLandimpLotclassPerTaxDec = valrepLandimpLotclassPerTaxDec;
    }

    public String getValrepLandimpLotclassPropType() {
        return NULL_STRING_SETTER(valrepLandimpLotclassPropType);
    }

    public void setValrepLandimpLotclassPropType(String valrepLandimpLotclassPropType) {
        this.valrepLandimpLotclassPropType = valrepLandimpLotclassPropType;
    }

    public String getValrepLandimpMarketPropMarket() {
        return NULL_STRING_SETTER(valrepLandimpMarketPropMarket);
    }

    public void setValrepLandimpMarketPropMarket(String valrepLandimpMarketPropMarket) {
        this.valrepLandimpMarketPropMarket = valrepLandimpMarketPropMarket;
    }

    public String getValrepLandimpMccDistance1() {
        return NULL_STRING_SETTER(valrepLandimpMccDistance1);
    }

    public void setValrepLandimpMccDistance1(String valrepLandimpMccDistance1) {
        this.valrepLandimpMccDistance1 = valrepLandimpMccDistance1;
    }

    public String getValrepLandimpMccDistance2() {
        return NULL_STRING_SETTER(valrepLandimpMccDistance2);
    }

    public void setValrepLandimpMccDistance2(String valrepLandimpMccDistance2) {
        this.valrepLandimpMccDistance2 = valrepLandimpMccDistance2;
    }

    public String getValrepLandimpMccDistance3() {
        return NULL_STRING_SETTER(valrepLandimpMccDistance3);
    }

    public void setValrepLandimpMccDistance3(String valrepLandimpMccDistance3) {
        this.valrepLandimpMccDistance3 = valrepLandimpMccDistance3;
    }

    public String getValrepLandimpMccDistance4() {
        return NULL_STRING_SETTER(valrepLandimpMccDistance4);
    }

    public void setValrepLandimpMccDistance4(String valrepLandimpMccDistance4) {
        this.valrepLandimpMccDistance4 = valrepLandimpMccDistance4;
    }

    public String getValrepLandimpMccDistance5() {
        return NULL_STRING_SETTER(valrepLandimpMccDistance5);
    }

    public void setValrepLandimpMccDistance5(String valrepLandimpMccDistance5) {
        this.valrepLandimpMccDistance5 = valrepLandimpMccDistance5;
    }

    public String getValrepLandimpMccLocation1() {
        return NULL_STRING_SETTER(valrepLandimpMccLocation1);
    }

    public void setValrepLandimpMccLocation1(String valrepLandimpMccLocation1) {
        this.valrepLandimpMccLocation1 = valrepLandimpMccLocation1;
    }

    public String getValrepLandimpMccLocation2() {
        return NULL_STRING_SETTER(valrepLandimpMccLocation2);
    }

    public void setValrepLandimpMccLocation2(String valrepLandimpMccLocation2) {
        this.valrepLandimpMccLocation2 = valrepLandimpMccLocation2;
    }

    public String getValrepLandimpMccLocation3() {
        return NULL_STRING_SETTER(valrepLandimpMccLocation3);
    }

    public void setValrepLandimpMccLocation3(String valrepLandimpMccLocation3) {
        this.valrepLandimpMccLocation3 = valrepLandimpMccLocation3;
    }

    public String getValrepLandimpMccLocation4() {
        return NULL_STRING_SETTER(valrepLandimpMccLocation4);
    }

    public void setValrepLandimpMccLocation4(String valrepLandimpMccLocation4) {
        this.valrepLandimpMccLocation4 = valrepLandimpMccLocation4;
    }

    public String getValrepLandimpMccLocation5() {
        return NULL_STRING_SETTER(valrepLandimpMccLocation5);
    }

    public void setValrepLandimpMccLocation5(String valrepLandimpMccLocation5) {
        this.valrepLandimpMccLocation5 = valrepLandimpMccLocation5;
    }

    public String getValrepLandimpMrDistance1() {
        return NULL_STRING_SETTER(valrepLandimpMrDistance1);
    }

    public void setValrepLandimpMrDistance1(String valrepLandimpMrDistance1) {
        this.valrepLandimpMrDistance1 = valrepLandimpMrDistance1;
    }

    public String getValrepLandimpMrDistance2() {
        return NULL_STRING_SETTER(valrepLandimpMrDistance2);
    }

    public void setValrepLandimpMrDistance2(String valrepLandimpMrDistance2) {
        this.valrepLandimpMrDistance2 = valrepLandimpMrDistance2;
    }

    public String getValrepLandimpMrDistance3() {
        return NULL_STRING_SETTER(valrepLandimpMrDistance3);
    }

    public void setValrepLandimpMrDistance3(String valrepLandimpMrDistance3) {
        this.valrepLandimpMrDistance3 = valrepLandimpMrDistance3;
    }

    public String getValrepLandimpMrDistance4() {
        return NULL_STRING_SETTER(valrepLandimpMrDistance4);
    }

    public void setValrepLandimpMrDistance4(String valrepLandimpMrDistance4) {
        this.valrepLandimpMrDistance4 = valrepLandimpMrDistance4;
    }

    public String getValrepLandimpMrDistance5() {
        return NULL_STRING_SETTER(valrepLandimpMrDistance5);
    }

    public void setValrepLandimpMrDistance5(String valrepLandimpMrDistance5) {
        this.valrepLandimpMrDistance5 = valrepLandimpMrDistance5;
    }

    public String getValrepLandimpMrLocation1() {
        return NULL_STRING_SETTER(valrepLandimpMrLocation1);
    }

    public void setValrepLandimpMrLocation1(String valrepLandimpMrLocation1) {
        this.valrepLandimpMrLocation1 = valrepLandimpMrLocation1;
    }

    public String getValrepLandimpMrLocation2() {
        return NULL_STRING_SETTER(valrepLandimpMrLocation2);
    }

    public void setValrepLandimpMrLocation2(String valrepLandimpMrLocation2) {
        this.valrepLandimpMrLocation2 = valrepLandimpMrLocation2;
    }

    public String getValrepLandimpMrLocation3() {
        return NULL_STRING_SETTER(valrepLandimpMrLocation3);
    }

    public void setValrepLandimpMrLocation3(String valrepLandimpMrLocation3) {
        this.valrepLandimpMrLocation3 = valrepLandimpMrLocation3;
    }

    public String getValrepLandimpMrLocation4() {
        return NULL_STRING_SETTER(valrepLandimpMrLocation4);
    }

    public void setValrepLandimpMrLocation4(String valrepLandimpMrLocation4) {
        this.valrepLandimpMrLocation4 = valrepLandimpMrLocation4;
    }

    public String getValrepLandimpMrLocation5() {
        return NULL_STRING_SETTER(valrepLandimpMrLocation5);
    }

    public void setValrepLandimpMrLocation5(String valrepLandimpMrLocation5) {
        this.valrepLandimpMrLocation5 = valrepLandimpMrLocation5;
    }

    public List<ValrepLandimpOtherLandImpDetail> getValrepLandimpOtherLandImpDetails() {
        return NULL_LIST_SETTER(valrepLandimpOtherLandImpDetails);
    }

    public void setValrepLandimpOtherLandImpDetails(List<ValrepLandimpOtherLandImpDetail> valrepLandimpOtherLandImpDetails) {
        this.valrepLandimpOtherLandImpDetails = valrepLandimpOtherLandImpDetails;
    }

    public List<ValrepLandimpOtherLandImpValue> getValrepLandimpOtherLandImpValue() {
        return NULL_LIST_SETTER(valrepLandimpOtherLandImpValue);
    }

    public void setValrepLandimpOtherLandImpValue(List<ValrepLandimpOtherLandImpValue> valrepLandimpOtherLandImpValue) {
        this.valrepLandimpOtherLandImpValue = valrepLandimpOtherLandImpValue;
    }

    public String getValrepLandimpOtherPropComments() {
        return NULL_STRING_SETTER(valrepLandimpOtherPropComments);
    }

    public void setValrepLandimpOtherPropComments(String valrepLandimpOtherPropComments) {
        this.valrepLandimpOtherPropComments = valrepLandimpOtherPropComments;
    }

    public List<ValrepLandimpParkingValuationDetail> getValrepLandimpParkingValuationDetails() {
        return NULL_LIST_SETTER(valrepLandimpParkingValuationDetails);
    }

    public void setValrepLandimpParkingValuationDetails(List<ValrepLandimpParkingValuationDetail> valrepLandimpParkingValuationDetails) {
        this.valrepLandimpParkingValuationDetails = valrepLandimpParkingValuationDetails;
    }

    public String getValrepLandimpPhysicalDepth() {
        return NULL_STRING_SETTER(valrepLandimpPhysicalDepth);
    }

    public void setValrepLandimpPhysicalDepth(String valrepLandimpPhysicalDepth) {
        this.valrepLandimpPhysicalDepth = valrepLandimpPhysicalDepth;
    }

    public String getValrepLandimpPhysicalElevation() {
        return NULL_STRING_SETTER(valrepLandimpPhysicalElevation);
    }

    public void setValrepLandimpPhysicalElevation(String valrepLandimpPhysicalElevation) {
        this.valrepLandimpPhysicalElevation = valrepLandimpPhysicalElevation;
    }

    public String getValrepLandimpPhysicalFrontage() {
        return NULL_STRING_SETTER(valrepLandimpPhysicalFrontage);
    }

    public void setValrepLandimpPhysicalFrontage(String valrepLandimpPhysicalFrontage) {
        this.valrepLandimpPhysicalFrontage = valrepLandimpPhysicalFrontage;
    }

    public String getValrepLandimpPhysicalLotType() {
        return NULL_STRING_SETTER(valrepLandimpPhysicalLotType);
    }

    public void setValrepLandimpPhysicalLotType(String valrepLandimpPhysicalLotType) {
        this.valrepLandimpPhysicalLotType = valrepLandimpPhysicalLotType;
    }

    public String getValrepLandimpPhysicalPubTransDesc() {
        return NULL_STRING_SETTER(valrepLandimpPhysicalPubTransDesc);
    }

    public void setValrepLandimpPhysicalPubTransDesc(String valrepLandimpPhysicalPubTransDesc) {
        this.valrepLandimpPhysicalPubTransDesc = valrepLandimpPhysicalPubTransDesc;
    }

    public String getValrepLandimpPhysicalRoad() {
        return NULL_STRING_SETTER(valrepLandimpPhysicalRoad);
    }

    public void setValrepLandimpPhysicalRoad(String valrepLandimpPhysicalRoad) {
        this.valrepLandimpPhysicalRoad = valrepLandimpPhysicalRoad;
    }

    public String getValrepLandimpPhysicalShape() {
        return NULL_STRING_SETTER(valrepLandimpPhysicalShape);
    }

    public void setValrepLandimpPhysicalShape(String valrepLandimpPhysicalShape) {
        this.valrepLandimpPhysicalShape = valrepLandimpPhysicalShape;
    }

    public String getValrepLandimpPhysicalSurfacing() {
        return NULL_STRING_SETTER(valrepLandimpPhysicalSurfacing);
    }

    public void setValrepLandimpPhysicalSurfacing(String valrepLandimpPhysicalSurfacing) {
        this.valrepLandimpPhysicalSurfacing = valrepLandimpPhysicalSurfacing;
    }

    public String getValrepLandimpPhysicalTerrain() {
        return NULL_STRING_SETTER(valrepLandimpPhysicalTerrain);
    }

    public void setValrepLandimpPhysicalTerrain(String valrepLandimpPhysicalTerrain) {
        this.valrepLandimpPhysicalTerrain = valrepLandimpPhysicalTerrain;
    }

    public String getValrepLandimpSpecialAssumptionsCheck() {
        return NULL_STRING_SETTER(valrepLandimpSpecialAssumptionsCheck);
    }

    public void setValrepLandimpSpecialAssumptionsCheck(String valrepLandimpSpecialAssumptionsCheck) {
        this.valrepLandimpSpecialAssumptionsCheck = valrepLandimpSpecialAssumptionsCheck;
    }

    public String getValrepLandimpSupplyanddemand1() {
        return NULL_STRING_SETTER(valrepLandimpSupplyanddemand1);
    }

    public void setValrepLandimpSupplyanddemand1(String valrepLandimpSupplyanddemand1) {
        this.valrepLandimpSupplyanddemand1 = valrepLandimpSupplyanddemand1;
    }

    public String getValrepLandimpSupplyanddemand10() {
        return NULL_STRING_SETTER(valrepLandimpSupplyanddemand10);
    }

    public void setValrepLandimpSupplyanddemand10(String valrepLandimpSupplyanddemand10) {
        this.valrepLandimpSupplyanddemand10 = valrepLandimpSupplyanddemand10;
    }

    public String getValrepLandimpSupplyanddemand2() {
        return NULL_STRING_SETTER(valrepLandimpSupplyanddemand2);
    }

    public void setValrepLandimpSupplyanddemand2(String valrepLandimpSupplyanddemand2) {
        this.valrepLandimpSupplyanddemand2 = valrepLandimpSupplyanddemand2;
    }

    public String getValrepLandimpSupplyanddemand3() {
        return NULL_STRING_SETTER(valrepLandimpSupplyanddemand3);
    }

    public void setValrepLandimpSupplyanddemand3(String valrepLandimpSupplyanddemand3) {
        this.valrepLandimpSupplyanddemand3 = valrepLandimpSupplyanddemand3;
    }

    public String getValrepLandimpSupplyanddemand4() {
        return NULL_STRING_SETTER(valrepLandimpSupplyanddemand4);
    }

    public void setValrepLandimpSupplyanddemand4(String valrepLandimpSupplyanddemand4) {
        this.valrepLandimpSupplyanddemand4 = valrepLandimpSupplyanddemand4;
    }

    public String getValrepLandimpSupplyanddemand5() {
        return NULL_STRING_SETTER(valrepLandimpSupplyanddemand5);
    }

    public void setValrepLandimpSupplyanddemand5(String valrepLandimpSupplyanddemand5) {
        this.valrepLandimpSupplyanddemand5 = valrepLandimpSupplyanddemand5;
    }

    public String getValrepLandimpSupplyanddemand6() {
        return NULL_STRING_SETTER(valrepLandimpSupplyanddemand6);
    }

    public void setValrepLandimpSupplyanddemand6(String valrepLandimpSupplyanddemand6) {
        this.valrepLandimpSupplyanddemand6 = valrepLandimpSupplyanddemand6;
    }

    public String getValrepLandimpSupplyanddemand7() {
        return NULL_STRING_SETTER(valrepLandimpSupplyanddemand7);
    }

    public void setValrepLandimpSupplyanddemand7(String valrepLandimpSupplyanddemand7) {
        this.valrepLandimpSupplyanddemand7 = valrepLandimpSupplyanddemand7;
    }

    public String getValrepLandimpSupplyanddemand8() {
        return NULL_STRING_SETTER(valrepLandimpSupplyanddemand8);
    }

    public void setValrepLandimpSupplyanddemand8(String valrepLandimpSupplyanddemand8) {
        this.valrepLandimpSupplyanddemand8 = valrepLandimpSupplyanddemand8;
    }

    public String getValrepLandimpSupplyanddemand9() {
        return NULL_STRING_SETTER(valrepLandimpSupplyanddemand9);
    }

    public void setValrepLandimpSupplyanddemand9(String valrepLandimpSupplyanddemand9) {
        this.valrepLandimpSupplyanddemand9 = valrepLandimpSupplyanddemand9;
    }

    public String getValrepLandimpTaxDecTownType() {
        return NULL_STRING_SETTER(valrepLandimpTaxDecTownType);
    }

    public void setValrepLandimpTaxDecTownType(String valrepLandimpTaxDecTownType) {
        this.valrepLandimpTaxDecTownType = valrepLandimpTaxDecTownType;
    }

    public String getValrepLandimpUtilElectricity() {
        return NULL_STRING_SETTER(valrepLandimpUtilElectricity);
    }

    public void setValrepLandimpUtilElectricity(String valrepLandimpUtilElectricity) {
        this.valrepLandimpUtilElectricity = valrepLandimpUtilElectricity;
    }

    public String getValrepLandimpUtilTelephone() {
        return NULL_STRING_SETTER(valrepLandimpUtilTelephone);
    }

    public void setValrepLandimpUtilTelephone(String valrepLandimpUtilTelephone) {
        this.valrepLandimpUtilTelephone = valrepLandimpUtilTelephone;
    }

    public String getValrepLandimpUtilWater() {
        return NULL_STRING_SETTER(valrepLandimpUtilWater);
    }

    public void setValrepLandimpUtilWater(String valrepLandimpUtilWater) {
        this.valrepLandimpUtilWater = valrepLandimpUtilWater;
    }

    public String getValrepLandimpValuationDateMonth() {
        return NULL_STRING_SETTER(valrepLandimpValuationDateMonth);
    }

    public void setValrepLandimpValuationDateMonth(String valrepLandimpValuationDateMonth) {
        this.valrepLandimpValuationDateMonth = valrepLandimpValuationDateMonth;
    }

    public String getValrepLandimpValueTotalArea() {
        return NULL_STRING_SETTER(valrepLandimpValueTotalArea);
    }

    public void setValrepLandimpValueTotalArea(String valrepLandimpValueTotalArea) {
        this.valrepLandimpValueTotalArea = valrepLandimpValueTotalArea;
    }

    public String getValrepLandimpValueTotalDeduction() {
        return NULL_STRING_SETTER(valrepLandimpValueTotalDeduction);
    }

    public void setValrepLandimpValueTotalDeduction(String valrepLandimpValueTotalDeduction) {
        this.valrepLandimpValueTotalDeduction = valrepLandimpValueTotalDeduction;
    }

    public String getValrepLandimpValueTotalLandimpValue() {
        return NULL_STRING_SETTER(valrepLandimpValueTotalLandimpValue);
    }

    public void setValrepLandimpValueTotalLandimpValue(String valrepLandimpValueTotalLandimpValue) {
        this.valrepLandimpValueTotalLandimpValue = valrepLandimpValueTotalLandimpValue;
    }

    public String getValrepLandimpValueTotalNetArea() {
        return NULL_STRING_SETTER(valrepLandimpValueTotalNetArea);
    }

    public void setValrepLandimpValueTotalNetArea(String valrepLandimpValueTotalNetArea) {
        this.valrepLandimpValueTotalNetArea = valrepLandimpValueTotalNetArea;
    }

    public String getValrepLandimpWeightedUnitValue() {
        return NULL_STRING_SETTER(valrepLandimpWeightedUnitValue);
    }

    public void setValrepLandimpWeightedUnitValue(String valrepLandimpWeightedUnitValue) {
        this.valrepLandimpWeightedUnitValue = valrepLandimpWeightedUnitValue;
    }

    public String getValrepLandimpZonalValue() {
        return NULL_STRING_SETTER(valrepLandimpZonalValue);
    }

    public void setValrepLandimpZonalValue(String valrepLandimpZonalValue) {
        this.valrepLandimpZonalValue = valrepLandimpZonalValue;
    }

    public List<ValrepLcIvsQualifyingStatement> getValrepLcIvsQualifyingStatements() {
        return NULL_LIST_SETTER(valrepLcIvsQualifyingStatements);
    }

    public void setValrepLcIvsQualifyingStatements(List<ValrepLcIvsQualifyingStatement> valrepLcIvsQualifyingStatements) {
        this.valrepLcIvsQualifyingStatements = valrepLcIvsQualifyingStatements;
    }

    public String getValrepLcIvsQualifyingStatementsCheck() {
        return NULL_STRING_SETTER(valrepLcIvsQualifyingStatementsCheck);
    }

    public void setValrepLcIvsQualifyingStatementsCheck(String valrepLcIvsQualifyingStatementsCheck) {
        this.valrepLcIvsQualifyingStatementsCheck = valrepLcIvsQualifyingStatementsCheck;
    }

    public String getValrepMarketphase1() {
        return NULL_STRING_SETTER(valrepMarketphase1);
    }

    public void setValrepMarketphase1(String valrepMarketphase1) {
        this.valrepMarketphase1 = valrepMarketphase1;
    }

    public String getValrepMarketphase2() {
        return NULL_STRING_SETTER(valrepMarketphase2);
    }

    public void setValrepMarketphase2(String valrepMarketphase2) {
        this.valrepMarketphase2 = valrepMarketphase2;
    }

    public String getValrepMarketphase3() {
        return NULL_STRING_SETTER(valrepMarketphase3);
    }

    public void setValrepMarketphase3(String valrepMarketphase3) {
        this.valrepMarketphase3 = valrepMarketphase3;
    }

    public String getValrepMarketphase4() {
        return NULL_STRING_SETTER(valrepMarketphase4);
    }

    public void setValrepMarketphase4(String valrepMarketphase4) {
        this.valrepMarketphase4 = valrepMarketphase4;
    }

    public String getValrepMarketphase5() {
        return NULL_STRING_SETTER(valrepMarketphase5);
    }

    public void setValrepMarketphase5(String valrepMarketphase5) {
        this.valrepMarketphase5 = valrepMarketphase5;
    }

    public String getValrepSupplyanddemand1() {
        return NULL_STRING_SETTER(valrepSupplyanddemand1);
    }

    public void setValrepSupplyanddemand1(String valrepSupplyanddemand1) {
        this.valrepSupplyanddemand1 = valrepSupplyanddemand1;
    }

    public String getValrepSupplyanddemand2() {
        return NULL_STRING_SETTER(valrepSupplyanddemand2);
    }

    public void setValrepSupplyanddemand2(String valrepSupplyanddemand2) {
        this.valrepSupplyanddemand2 = valrepSupplyanddemand2;
    }

    public String getValrepSupplyanddemand3() {
        return NULL_STRING_SETTER(valrepSupplyanddemand3);
    }

    public void setValrepSupplyanddemand3(String valrepSupplyanddemand3) {
        this.valrepSupplyanddemand3 = valrepSupplyanddemand3;
    }

    public String getValrepSupplyanddemand4() {
        return NULL_STRING_SETTER(valrepSupplyanddemand4);
    }

    public void setValrepSupplyanddemand4(String valrepSupplyanddemand4) {
        this.valrepSupplyanddemand4 = valrepSupplyanddemand4;
    }

    public String getValrepSupplyanddemand5() {
        return NULL_STRING_SETTER(valrepSupplyanddemand5);
    }

    public void setValrepSupplyanddemand5(String valrepSupplyanddemand5) {
        this.valrepSupplyanddemand5 = valrepSupplyanddemand5;
    }

    public String getValrepSupplyanddemand6() {
        return NULL_STRING_SETTER(valrepSupplyanddemand6);
    }

    public void setValrepSupplyanddemand6(String valrepSupplyanddemand6) {
        this.valrepSupplyanddemand6 = valrepSupplyanddemand6;
    }

    public String getValrepSupplyanddemand7() {
        return NULL_STRING_SETTER(valrepSupplyanddemand7);
    }

    public void setValrepSupplyanddemand7(String valrepSupplyanddemand7) {
        this.valrepSupplyanddemand7 = valrepSupplyanddemand7;
    }

    public String getValrepSupplyanddemand8() {
        return NULL_STRING_SETTER(valrepSupplyanddemand8);
    }

    public void setValrepSupplyanddemand8(String valrepSupplyanddemand8) {
        this.valrepSupplyanddemand8 = valrepSupplyanddemand8;
    }

    public String getValrepSupplyanddemand9() {
        return NULL_STRING_SETTER(valrepSupplyanddemand9);
    }

    public void setValrepSupplyanddemand9(String valrepSupplyanddemand9) {
        this.valrepSupplyanddemand9 = valrepSupplyanddemand9;
    }

    public String getValrepTypicalbuyer1() {
        return NULL_STRING_SETTER(valrepTypicalbuyer1);
    }

    public void setValrepTypicalbuyer1(String valrepTypicalbuyer1) {
        this.valrepTypicalbuyer1 = valrepTypicalbuyer1;
    }

}
