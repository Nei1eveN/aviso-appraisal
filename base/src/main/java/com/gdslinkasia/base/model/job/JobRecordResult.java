package com.gdslinkasia.base.model.job;

import androidx.room.Embedded;
import androidx.room.Relation;

import com.gdslinkasia.base.model.details.JobSystem;
import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.List;

import static com.gdslinkasia.base.database.DatabaseUtils.RECORD_ID;
import static com.gdslinkasia.base.utils.GlobalString.NULL_LIST_SETTER;

@SuppressWarnings("unused")
public class JobRecordResult {

    @Embedded
    private JobSystem jobSystem;

    @Relation(parentColumn = RECORD_ID, entityColumn = RECORD_ID, entity = Record.class)
    @SerializedName("records")
    @Expose
    private List<Record> records = null;

    //TODO TO FILL-IN OTHER LISTS IF NEEDED

    public JobSystem getJobSystem() {
        return jobSystem;
    }

    public void setJobSystem(JobSystem jobSystem) {
        this.jobSystem = jobSystem;
    }

    public List<Record> getRecords() {
        return NULL_LIST_SETTER(records);
    }

    public void setRecords(List<Record> records) {
        this.records = records;
    }
}