
package com.gdslinkasia.base.model.details;

import androidx.room.ColumnInfo;
import androidx.room.Entity;
import androidx.room.ForeignKey;
import androidx.room.PrimaryKey;

import com.gdslinkasia.base.model.job.Record;
import com.google.gson.annotations.SerializedName;

import static androidx.room.ForeignKey.CASCADE;
import static com.gdslinkasia.base.database.DatabaseUtils.LANDIMP_DESC_IMP_ID;
import static com.gdslinkasia.base.database.DatabaseUtils.RECORD_ID;
import static com.gdslinkasia.base.database.DatabaseUtils.RECORD_UID;
import static com.gdslinkasia.base.database.DatabaseUtils.UNIQUE_ID;
import static com.gdslinkasia.base.utils.GlobalString.NULL_STRING_SETTER;

@SuppressWarnings("unused")
@Entity(foreignKeys = {
        @ForeignKey(
                entity = Record.class,
                parentColumns = UNIQUE_ID,
                childColumns = RECORD_UID,
                onDelete = CASCADE),
        @ForeignKey(
                entity = ValrepLandimpImpDetail.class,
                parentColumns = UNIQUE_ID,
                childColumns = LANDIMP_DESC_IMP_ID,
                onDelete = CASCADE)})
public class ValrepLandimpImpValuation {

    //    @Expose(serialize = false, deserialize = false)
    @PrimaryKey(autoGenerate = true)
    @ColumnInfo(name = UNIQUE_ID)
    private transient long uniqueId;

    //TODO NON-SERIALIZED
//    @Expose(serialize = false, deserialize = false)
    @ColumnInfo(name = RECORD_ID, index = true)
    private transient String recordId;

    //    @Expose(serialize = false, deserialize = false)
    @ColumnInfo(name = RECORD_UID, index = true)
    private transient long recordUId;

    @ColumnInfo(name = LANDIMP_DESC_IMP_ID, index = true)
    private transient long descId;

    @SerializedName("valrep_landimp_imp_value_appraised_mv")
    private String valrepLandimpImpValueAppraisedMv;
    @SerializedName("valrep_landimp_imp_value_depreciated_value")
    private String valrepLandimpImpValueDepreciatedValue;
    @SerializedName("valrep_landimp_imp_value_depreciation")
    private String valrepLandimpImpValueDepreciation;
    @SerializedName("valrep_landimp_imp_value_description")
    private String valrepLandimpImpValueDescription;
    @SerializedName("valrep_landimp_imp_value_economic_life")
    private String valrepLandimpImpValueEconomicLife;
    @SerializedName("valrep_landimp_imp_value_economic_val")
    private String valrepLandimpImpValueEconomicVal;
    @SerializedName("valrep_landimp_imp_value_effective_life")
    private String valrepLandimpImpValueEffectiveLife;
    @SerializedName("valrep_landimp_imp_value_est_life")
    private String valrepLandimpImpValueEstLife;
    @SerializedName("valrep_landimp_imp_value_functional_val")
    private String valrepLandimpImpValueFunctionalVal;
    @SerializedName("valrep_landimp_imp_value_insurable")
    private String valrepLandimpImpValueInsurable;
    @SerializedName("valrep_landimp_imp_value_insurance_value")
    private String valrepLandimpImpValueInsuranceValue;
    @SerializedName("valrep_landimp_imp_value_less_reproduction")
    private String valrepLandimpImpValueLessReproduction;
    @SerializedName("valrep_landimp_imp_value_loan_value")
    private String valrepLandimpImpValueLoanValue;
    @SerializedName("valrep_landimp_imp_value_per_of_mv")
    private String valrepLandimpImpValuePerOfMv;
    @SerializedName("valrep_landimp_imp_value_percent_completion")
    private String valrepLandimpImpValuePercentCompletion;
    @SerializedName("valrep_landimp_imp_value_physical_curable_per")
    private String valrepLandimpImpValuePhysicalCurablePer;
    @SerializedName("valrep_landimp_imp_value_physical_curable_val")
    private String valrepLandimpImpValuePhysicalCurableVal;
    @SerializedName("valrep_landimp_imp_value_physical_incurable_per")
    private String valrepLandimpImpValuePhysicalIncurablePer;
    @SerializedName("valrep_landimp_imp_value_physical_incurable_val")
    private String valrepLandimpImpValuePhysicalIncurableVal;
    @SerializedName("valrep_landimp_imp_value_rcn_new")
    private String valrepLandimpImpValueRcnNew;
    @SerializedName("valrep_landimp_imp_value_salvage_rate")
    private String valrepLandimpImpValueSalvageRate;
    @SerializedName("valrep_landimp_imp_value_total_area")
    private String valrepLandimpImpValueTotalArea;
    @SerializedName("valrep_landimp_imp_value_total_depreciation")
    private String valrepLandimpImpValueTotalDepreciation;
    @SerializedName("valrep_landimp_imp_value_total_reproduction_cost")
    private String valrepLandimpImpValueTotalReproductionCost;
    @SerializedName("valrep_landimp_imp_value_year_built")
    private String valrepLandimpImpValueYearBuilt;

    public long getUniqueId() {
        return uniqueId;
    }

    public void setUniqueId(long uniqueId) {
        this.uniqueId = uniqueId;
    }

    public String getRecordId() {
        return recordId;
    }

    public void setRecordId(String recordId) {
        this.recordId = recordId;
    }

    public long getRecordUId() {
        return recordUId;
    }

    public void setRecordUId(long recordUId) {
        this.recordUId = recordUId;
    }

    public long getDescId() {
        return descId;
    }

    public void setDescId(long descId) {
        this.descId = descId;
    }

    public String getValrepLandimpImpValueAppraisedMv() {
        return NULL_STRING_SETTER(valrepLandimpImpValueAppraisedMv);
    }

    public void setValrepLandimpImpValueAppraisedMv(String valrepLandimpImpValueAppraisedMv) {
        this.valrepLandimpImpValueAppraisedMv = valrepLandimpImpValueAppraisedMv;
    }

    public String getValrepLandimpImpValueDepreciatedValue() {
        return NULL_STRING_SETTER(valrepLandimpImpValueDepreciatedValue);
    }

    public void setValrepLandimpImpValueDepreciatedValue(String valrepLandimpImpValueDepreciatedValue) {
        this.valrepLandimpImpValueDepreciatedValue = valrepLandimpImpValueDepreciatedValue;
    }

    public String getValrepLandimpImpValueDepreciation() {
        return NULL_STRING_SETTER(valrepLandimpImpValueDepreciation);
    }

    public void setValrepLandimpImpValueDepreciation(String valrepLandimpImpValueDepreciation) {
        this.valrepLandimpImpValueDepreciation = valrepLandimpImpValueDepreciation;
    }

    public String getValrepLandimpImpValueDescription() {
        return NULL_STRING_SETTER(valrepLandimpImpValueDescription);
    }

    public void setValrepLandimpImpValueDescription(String valrepLandimpImpValueDescription) {
        this.valrepLandimpImpValueDescription = valrepLandimpImpValueDescription;
    }

    public String getValrepLandimpImpValueEconomicLife() {
        return NULL_STRING_SETTER(valrepLandimpImpValueEconomicLife);
    }

    public void setValrepLandimpImpValueEconomicLife(String valrepLandimpImpValueEconomicLife) {
        this.valrepLandimpImpValueEconomicLife = valrepLandimpImpValueEconomicLife;
    }

    public String getValrepLandimpImpValueEconomicVal() {
        return NULL_STRING_SETTER(valrepLandimpImpValueEconomicVal);
    }

    public void setValrepLandimpImpValueEconomicVal(String valrepLandimpImpValueEconomicVal) {
        this.valrepLandimpImpValueEconomicVal = valrepLandimpImpValueEconomicVal;
    }

    public String getValrepLandimpImpValueEffectiveLife() {
        return NULL_STRING_SETTER(valrepLandimpImpValueEffectiveLife);
    }

    public void setValrepLandimpImpValueEffectiveLife(String valrepLandimpImpValueEffectiveLife) {
        this.valrepLandimpImpValueEffectiveLife = valrepLandimpImpValueEffectiveLife;
    }

    public String getValrepLandimpImpValueEstLife() {
        return NULL_STRING_SETTER(valrepLandimpImpValueEstLife);
    }

    public void setValrepLandimpImpValueEstLife(String valrepLandimpImpValueEstLife) {
        this.valrepLandimpImpValueEstLife = valrepLandimpImpValueEstLife;
    }

    public String getValrepLandimpImpValueFunctionalVal() {
        return NULL_STRING_SETTER(valrepLandimpImpValueFunctionalVal);
    }

    public void setValrepLandimpImpValueFunctionalVal(String valrepLandimpImpValueFunctionalVal) {
        this.valrepLandimpImpValueFunctionalVal = valrepLandimpImpValueFunctionalVal;
    }

    public String getValrepLandimpImpValueInsurable() {
        return NULL_STRING_SETTER(valrepLandimpImpValueInsurable);
    }

    public void setValrepLandimpImpValueInsurable(String valrepLandimpImpValueInsurable) {
        this.valrepLandimpImpValueInsurable = valrepLandimpImpValueInsurable;
    }

    public String getValrepLandimpImpValueInsuranceValue() {
        return NULL_STRING_SETTER(valrepLandimpImpValueInsuranceValue);
    }

    public void setValrepLandimpImpValueInsuranceValue(String valrepLandimpImpValueInsuranceValue) {
        this.valrepLandimpImpValueInsuranceValue = valrepLandimpImpValueInsuranceValue;
    }

    public String getValrepLandimpImpValueLessReproduction() {
        return NULL_STRING_SETTER(valrepLandimpImpValueLessReproduction);
    }

    public void setValrepLandimpImpValueLessReproduction(String valrepLandimpImpValueLessReproduction) {
        this.valrepLandimpImpValueLessReproduction = valrepLandimpImpValueLessReproduction;
    }

    public String getValrepLandimpImpValueLoanValue() {
        return NULL_STRING_SETTER(valrepLandimpImpValueLoanValue);
    }

    public void setValrepLandimpImpValueLoanValue(String valrepLandimpImpValueLoanValue) {
        this.valrepLandimpImpValueLoanValue = valrepLandimpImpValueLoanValue;
    }

    public String getValrepLandimpImpValuePerOfMv() {
        return NULL_STRING_SETTER(valrepLandimpImpValuePerOfMv);
    }

    public void setValrepLandimpImpValuePerOfMv(String valrepLandimpImpValuePerOfMv) {
        this.valrepLandimpImpValuePerOfMv = valrepLandimpImpValuePerOfMv;
    }

    public String getValrepLandimpImpValuePercentCompletion() {
        return NULL_STRING_SETTER(valrepLandimpImpValuePercentCompletion);
    }

    public void setValrepLandimpImpValuePercentCompletion(String valrepLandimpImpValuePercentCompletion) {
        this.valrepLandimpImpValuePercentCompletion = valrepLandimpImpValuePercentCompletion;
    }

    public String getValrepLandimpImpValuePhysicalCurablePer() {
        return NULL_STRING_SETTER(valrepLandimpImpValuePhysicalCurablePer);
    }

    public void setValrepLandimpImpValuePhysicalCurablePer(String valrepLandimpImpValuePhysicalCurablePer) {
        this.valrepLandimpImpValuePhysicalCurablePer = valrepLandimpImpValuePhysicalCurablePer;
    }

    public String getValrepLandimpImpValuePhysicalCurableVal() {
        return NULL_STRING_SETTER(valrepLandimpImpValuePhysicalCurableVal);
    }

    public void setValrepLandimpImpValuePhysicalCurableVal(String valrepLandimpImpValuePhysicalCurableVal) {
        this.valrepLandimpImpValuePhysicalCurableVal = valrepLandimpImpValuePhysicalCurableVal;
    }

    public String getValrepLandimpImpValuePhysicalIncurablePer() {
        return NULL_STRING_SETTER(valrepLandimpImpValuePhysicalIncurablePer);
    }

    public void setValrepLandimpImpValuePhysicalIncurablePer(String valrepLandimpImpValuePhysicalIncurablePer) {
        this.valrepLandimpImpValuePhysicalIncurablePer = valrepLandimpImpValuePhysicalIncurablePer;
    }

    public String getValrepLandimpImpValuePhysicalIncurableVal() {
        return NULL_STRING_SETTER(valrepLandimpImpValuePhysicalIncurableVal);
    }

    public void setValrepLandimpImpValuePhysicalIncurableVal(String valrepLandimpImpValuePhysicalIncurableVal) {
        this.valrepLandimpImpValuePhysicalIncurableVal = valrepLandimpImpValuePhysicalIncurableVal;
    }

    public String getValrepLandimpImpValueRcnNew() {
        return NULL_STRING_SETTER(valrepLandimpImpValueRcnNew);
    }

    public void setValrepLandimpImpValueRcnNew(String valrepLandimpImpValueRcnNew) {
        this.valrepLandimpImpValueRcnNew = valrepLandimpImpValueRcnNew;
    }

    public String getValrepLandimpImpValueSalvageRate() {
        return NULL_STRING_SETTER(valrepLandimpImpValueSalvageRate);
    }

    public void setValrepLandimpImpValueSalvageRate(String valrepLandimpImpValueSalvageRate) {
        this.valrepLandimpImpValueSalvageRate = valrepLandimpImpValueSalvageRate;
    }

    public String getValrepLandimpImpValueTotalArea() {
        return NULL_STRING_SETTER(valrepLandimpImpValueTotalArea);
    }

    public void setValrepLandimpImpValueTotalArea(String valrepLandimpImpValueTotalArea) {
        this.valrepLandimpImpValueTotalArea = valrepLandimpImpValueTotalArea;
    }

    public String getValrepLandimpImpValueTotalDepreciation() {
        return NULL_STRING_SETTER(valrepLandimpImpValueTotalDepreciation);
    }

    public void setValrepLandimpImpValueTotalDepreciation(String valrepLandimpImpValueTotalDepreciation) {
        this.valrepLandimpImpValueTotalDepreciation = valrepLandimpImpValueTotalDepreciation;
    }

    public String getValrepLandimpImpValueTotalReproductionCost() {
        return NULL_STRING_SETTER(valrepLandimpImpValueTotalReproductionCost);
    }

    public void setValrepLandimpImpValueTotalReproductionCost(String valrepLandimpImpValueTotalReproductionCost) {
        this.valrepLandimpImpValueTotalReproductionCost = valrepLandimpImpValueTotalReproductionCost;
    }

    public String getValrepLandimpImpValueYearBuilt() {
        return NULL_STRING_SETTER(valrepLandimpImpValueYearBuilt);
    }

    public void setValrepLandimpImpValueYearBuilt(String valrepLandimpImpValueYearBuilt) {
        this.valrepLandimpImpValueYearBuilt = valrepLandimpImpValueYearBuilt;
    }

}
