
package com.gdslinkasia.base.model.details;

import androidx.room.ColumnInfo;
import androidx.room.Entity;
import androidx.room.ForeignKey;
import androidx.room.PrimaryKey;

import com.gdslinkasia.base.model.job.Record;
import com.google.gson.annotations.SerializedName;

import static androidx.room.ForeignKey.CASCADE;
import static com.gdslinkasia.base.database.DatabaseUtils.RECORD_ID;
import static com.gdslinkasia.base.database.DatabaseUtils.RECORD_UID;
import static com.gdslinkasia.base.database.DatabaseUtils.UNIQUE_ID;
import static com.gdslinkasia.base.utils.GlobalString.NULL_STRING_SETTER;

@SuppressWarnings("unused")
@Entity(foreignKeys = {@ForeignKey(
        entity = Record.class,
        parentColumns = UNIQUE_ID,
        childColumns = RECORD_UID,
        onDelete = CASCADE)})
//@ForeignKey(entity = JobSystem.class,
//        parentColumns = RECORD_ID,
//        childColumns = RECORD_ID,
//        onDelete = CASCADE),
public class ValrepLandimpAssumptionsRemark {

//    @Expose(serialize = false, deserialize = false)
    @PrimaryKey(autoGenerate = true)
    @ColumnInfo(name = UNIQUE_ID)
    private transient long uniqueId;

    //TODO NON-SERIALIZED
//    @Expose(serialize = false, deserialize = false)
    @ColumnInfo(name = RECORD_ID, index = true)
    private transient String recordId;

//    @Expose(serialize = false, deserialize = false)
    @ColumnInfo(name = RECORD_UID, index = true)
    private transient long recordUId;

    @SerializedName("valrep_landimp_assumptions")
    private String valrepLandimpAssumptions;

    public long getUniqueId() {
        return uniqueId;
    }

    public void setUniqueId(long uniqueId) {
        this.uniqueId = uniqueId;
    }

    public String getRecordId() {
        return recordId;
    }

    public void setRecordId(String recordId) {
        this.recordId = recordId;
    }

    public long getRecordUId() {
        return recordUId;
    }

    public void setRecordUId(long recordUId) {
        this.recordUId = recordUId;
    }

    public String getValrepLandimpAssumptions() {
        return NULL_STRING_SETTER(valrepLandimpAssumptions);
    }

    public void setValrepLandimpAssumptions(String valrepLandimpAssumptions) {
        this.valrepLandimpAssumptions = valrepLandimpAssumptions;
    }
}
