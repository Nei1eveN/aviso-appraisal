package com.gdslinkasia.base.model.details;

import androidx.room.ColumnInfo;
import androidx.room.Entity;
import androidx.room.ForeignKey;
import androidx.room.PrimaryKey;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import static androidx.room.ForeignKey.CASCADE;
import static com.gdslinkasia.base.database.DatabaseUtils.APPRAISAL_REQUEST_ID;
import static com.gdslinkasia.base.database.DatabaseUtils.ATTACHMENT_CREATED_FROM;
import static com.gdslinkasia.base.database.DatabaseUtils.UNIQUE_ID;
import static com.gdslinkasia.base.utils.GlobalString.NULL_STRING_SETTER;

@Entity(foreignKeys = {
        @ForeignKey(entity = AppraisalRequest.class,
                parentColumns = UNIQUE_ID,
                childColumns = APPRAISAL_REQUEST_ID,
                onDelete = CASCADE)})
public class AppAttachment {

//    @Expose(serialize = false, deserialize = false)
    @PrimaryKey(autoGenerate = true)
    @ColumnInfo(name = UNIQUE_ID)
    private transient long uniqueId;

//    @Expose(serialize = false, deserialize = false)
    @ColumnInfo(name = APPRAISAL_REQUEST_ID, index = true)
    private transient long appraisalRequestId;

//    //TODO NON-SERIALIZED
//    @Expose(serialize = false, deserialize = false)
//    @ColumnInfo(name = APPLICATION_STATUS)
//    private String applicationStatus;
//
//    //TODO NON-SERIALIZED
//    @Expose(serialize = false, deserialize = false)
//    @ColumnInfo(name = APPRAISER_USERNAME)
//    private String appraiserUsername;

    @ColumnInfo(name = ATTACHMENT_CREATED_FROM, index = true)
    private transient String createdFrom;

    @ColumnInfo(name = "app_attachment_appraisal_type")
    @SerializedName("app_attachment_appraisal_type")
    @Expose
    private String appAttachmentAppraisalType;

    @ColumnInfo(name = "app_attachment_file")
    @SerializedName("app_attachment_file")
    @Expose
    private String appAttachmentFile;

    @ColumnInfo(name = "app_attachment_filename")
    @SerializedName("app_attachment_filename")
    @Expose
    private String appAttachmentFilename;

    public long getUniqueId() {
        return uniqueId;
    }

    public void setUniqueId(long uniqueId) {
        this.uniqueId = uniqueId;
    }

    public long getAppraisalRequestId() {
        return appraisalRequestId;
    }

    public void setAppraisalRequestId(long appraisalRequestId) {
        this.appraisalRequestId = appraisalRequestId;
    }

    public String getCreatedFrom() {
        return createdFrom;
    }

    public void setCreatedFrom(String createdFrom) {
        this.createdFrom = createdFrom;
    }

    //    public String getApplicationStatus() {
//        return applicationStatus;
//    }
//
//    public void setApplicationStatus(String applicationStatus) {
//        this.applicationStatus = applicationStatus;
//    }
//
//    public String getAppraiserUsername() {
//        return appraiserUsername;
//    }
//
//    public void setAppraiserUsername(String appraiserUsername) {
//        this.appraiserUsername = appraiserUsername;
//    }

    public String getAppAttachmentAppraisalType() {
        return NULL_STRING_SETTER(appAttachmentAppraisalType);
    }

    public void setAppAttachmentAppraisalType(String appAttachmentAppraisalType) {
        this.appAttachmentAppraisalType = appAttachmentAppraisalType;
    }

    public String getAppAttachmentFile() {
        return NULL_STRING_SETTER(appAttachmentFile);
    }

    public void setAppAttachmentFile(String appAttachmentFile) {
        this.appAttachmentFile = appAttachmentFile;
    }

    public String getAppAttachmentFilename() {
        return NULL_STRING_SETTER(appAttachmentFilename);
    }

    public void setAppAttachmentFilename(String appAttachmentFilename) {
        this.appAttachmentFilename = appAttachmentFilename;
    }

}