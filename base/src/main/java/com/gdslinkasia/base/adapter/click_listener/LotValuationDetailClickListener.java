package com.gdslinkasia.base.adapter.click_listener;

public interface LotValuationDetailClickListener {

    void onDetailItemClick(String recordId, long uniqueId);

    void onTotalValuationForLand(double totalLandAreaSqm, double lessRestrictionSqm, double netUsableArea, double marketValueForLand);

}
