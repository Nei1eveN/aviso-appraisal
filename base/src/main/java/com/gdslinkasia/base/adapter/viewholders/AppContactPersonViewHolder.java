package com.gdslinkasia.base.adapter.viewholders;

import android.view.View;

import androidx.annotation.NonNull;
import androidx.constraintlayout.widget.ConstraintLayout;
import androidx.recyclerview.widget.RecyclerView;

import com.gdslinkasia.commonresources.R2;
import com.google.android.material.textfield.TextInputEditText;

import butterknife.BindView;
import butterknife.ButterKnife;

public class AppContactPersonViewHolder extends RecyclerView.ViewHolder {

    @BindView(R2.id.constraintLayout) public ConstraintLayout constraintLayout;
    @BindView(R2.id.etContactName) public TextInputEditText contactName;
    @BindView(R2.id.etPrefix) public TextInputEditText prefix;
    @BindView(R2.id.etMobileNo) public TextInputEditText mobileNo;
    @BindView(R2.id.etLandline) public TextInputEditText landline;

    public AppContactPersonViewHolder(@NonNull View itemView) {
        super(itemView);
        ButterKnife.bind(this, itemView);
    }
}
