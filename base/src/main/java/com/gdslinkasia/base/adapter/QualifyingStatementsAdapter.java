package com.gdslinkasia.base.adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.ViewGroup;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.DiffUtil;
import androidx.recyclerview.widget.ListAdapter;

import com.gdslinkasia.base.R;
import com.gdslinkasia.base.adapter.click_listener.OwnershipPropDescClickListener;
import com.gdslinkasia.base.adapter.viewholders.QualifyingStatementViewHolder;
import com.gdslinkasia.base.model.details.ValrepLcIvsQualifyingStatement;

import java.util.Locale;

public class QualifyingStatementsAdapter extends ListAdapter<ValrepLcIvsQualifyingStatement, QualifyingStatementViewHolder> {

    private Context context;
    private OwnershipPropDescClickListener listener;

    public QualifyingStatementsAdapter(Context context, OwnershipPropDescClickListener listener) {
        super(CALL_BACK);
        this.context = context;
        this.listener = listener;
    }

    @NonNull
    @Override
    public QualifyingStatementViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        return new QualifyingStatementViewHolder(LayoutInflater.from(context).inflate(R.layout.list_qualifying_statement_item, parent, false));
    }

    @Override
    public void onBindViewHolder(@NonNull QualifyingStatementViewHolder holder, int position) {
        ValrepLcIvsQualifyingStatement detail = getItem(position);

        holder.title.setText(String.format(Locale.ENGLISH,"Qualifying Statement %d", position+1));
        holder.description.setText(detail.getLcIvsQualifyingStatementsRemarks());

        holder.constraintLayout.setOnClickListener(v -> listener.onPropDescItemClick(detail.getRecordId(), detail.getUniqueId(), position+1));
    }

    private static DiffUtil.ItemCallback<ValrepLcIvsQualifyingStatement> CALL_BACK = new DiffUtil.ItemCallback<ValrepLcIvsQualifyingStatement>() {
        @Override
        public boolean areItemsTheSame(@NonNull ValrepLcIvsQualifyingStatement oldItem, @NonNull ValrepLcIvsQualifyingStatement newItem) {
            return oldItem.getUniqueId() == newItem.getUniqueId();
        }

        @Override
        public boolean areContentsTheSame(@NonNull ValrepLcIvsQualifyingStatement oldItem, @NonNull ValrepLcIvsQualifyingStatement newItem) {
            return oldItem.getUniqueId() == newItem.getUniqueId();
        }
    };
}
