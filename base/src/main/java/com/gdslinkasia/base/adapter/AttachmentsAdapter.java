package com.gdslinkasia.base.adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.ViewGroup;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.DiffUtil;
import androidx.recyclerview.widget.ListAdapter;

import com.gdslinkasia.base.R;
import com.gdslinkasia.base.adapter.click_listener.AttachmentClickListener;
import com.gdslinkasia.base.adapter.viewholders.AttachmentViewHolder;
import com.gdslinkasia.base.model.details.AppAttachment;

import java.util.Locale;

public class AttachmentsAdapter extends ListAdapter<AppAttachment, AttachmentViewHolder> {

    private Context context;
    private AttachmentClickListener listener;

    public AttachmentsAdapter(Context context, AttachmentClickListener listener) {
        super(CALL_BACK);
        this.context = context;
        this.listener = listener;
    }

    @NonNull
    @Override
    public AttachmentViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        return new AttachmentViewHolder(LayoutInflater.from(context).inflate(R.layout.list_attachment_item, parent, false));
    }

    @Override
    public void onBindViewHolder(@NonNull AttachmentViewHolder holder, int position) {
        AppAttachment detail = getItem(position);

        holder.title.setText(String.format(Locale.ENGLISH,"Attachments no: %d", position+1));
        holder.fileName.setText(detail.getAppAttachmentFilename());
        holder.fileType.setText(detail.getAppAttachmentFile());
//        holder.title.append("\n\nFile Type: \n"+detail.getAppAttachmentFile());
//        holder.title.append("\nFile Name: \n"+detail.getAppAttachmentFilename());

        holder.constraintLayout.setOnClickListener(v -> listener.onPropDescItemClick(
                String.valueOf(detail.getUniqueId()), detail.getUniqueId(), detail));
    }

    private static DiffUtil.ItemCallback<AppAttachment> CALL_BACK = new DiffUtil.ItemCallback<AppAttachment>() {
        @Override
        public boolean areItemsTheSame(@NonNull AppAttachment oldItem, @NonNull AppAttachment newItem) {
            return oldItem.getUniqueId() == newItem.getUniqueId();
        }

        @Override
        public boolean areContentsTheSame(@NonNull AppAttachment oldItem, @NonNull AppAttachment newItem) {
            return oldItem.getUniqueId() == newItem.getUniqueId();
        }
    };
}
