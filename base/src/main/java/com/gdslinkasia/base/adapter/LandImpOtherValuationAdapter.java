package com.gdslinkasia.base.adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.ViewGroup;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.DiffUtil;
import androidx.recyclerview.widget.ListAdapter;

import com.gdslinkasia.base.R;
import com.gdslinkasia.base.adapter.click_listener.ViewListClickListener;
import com.gdslinkasia.base.adapter.viewholders.OwnershipPropDescViewHolder;
import com.gdslinkasia.base.model.details.ValrepLandimpOtherLandImpValue;

import java.util.Locale;

public class LandImpOtherValuationAdapter extends ListAdapter<ValrepLandimpOtherLandImpValue, OwnershipPropDescViewHolder> {

    private Context context;
    private ViewListClickListener listener;

    public LandImpOtherValuationAdapter(Context context, ViewListClickListener listener) {
        super(CALL_BACK);
        this.context = context;
        this.listener = listener;
    }

    @NonNull
    @Override
    public OwnershipPropDescViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        return new OwnershipPropDescViewHolder(LayoutInflater.from(context).inflate(R.layout.list_own_prop_desc_item, parent, false));
    }

    @Override
    public void onBindViewHolder(@NonNull OwnershipPropDescViewHolder holder, int position) {
        ValrepLandimpOtherLandImpValue detail = getItem(position);

        holder.title.setText(String.format(Locale.ENGLISH,"Description %d", position+1));
        holder.title.append("\nType: "+detail.getValrepLandimpOtherLandImpValueType());

        holder.constraintLayout.setOnClickListener(v -> listener.onItemDetailClick(detail.getRecordId(), detail.getUniqueId()));
    }

    private static DiffUtil.ItemCallback<ValrepLandimpOtherLandImpValue> CALL_BACK = new DiffUtil.ItemCallback<ValrepLandimpOtherLandImpValue>() {
        @Override
        public boolean areItemsTheSame(@NonNull ValrepLandimpOtherLandImpValue oldItem, @NonNull ValrepLandimpOtherLandImpValue newItem) {
            return oldItem.getUniqueId() == newItem.getUniqueId();
        }

        @Override
        public boolean areContentsTheSame(@NonNull ValrepLandimpOtherLandImpValue oldItem, @NonNull ValrepLandimpOtherLandImpValue newItem) {
            return oldItem.getUniqueId() == newItem.getUniqueId();
        }
    };
}
