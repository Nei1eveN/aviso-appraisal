package com.gdslinkasia.base.adapter.viewholders;

import android.view.View;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.constraintlayout.widget.ConstraintLayout;
import androidx.recyclerview.widget.RecyclerView;


import com.gdslinkasia.commonresources.R2;

import butterknife.BindView;
import butterknife.ButterKnife;

public class LotValuationDetailViewHolder extends RecyclerView.ViewHolder {

    @BindView(R2.id.constraintLayout) public ConstraintLayout constraintLayout;
    @BindView(R2.id.tvTitleNo) public TextView titleNo;
    @BindView(R2.id.tvTotalVal) public TextView totalVal;

    public LotValuationDetailViewHolder(@NonNull View itemView) {
        super(itemView);
        ButterKnife.bind(this, itemView);
    }
}
