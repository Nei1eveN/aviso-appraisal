package com.gdslinkasia.base.adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.ViewGroup;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.DiffUtil;
import androidx.recyclerview.widget.ListAdapter;

import com.gdslinkasia.base.R;
import com.gdslinkasia.base.adapter.click_listener.LotValuationDetailClickListener;
import com.gdslinkasia.base.adapter.viewholders.LotValuationDetailViewHolder;
import com.gdslinkasia.base.model.details.ValrepLandimpLotValuationDetail;

public class LotValuationDetailAdapter extends ListAdapter<ValrepLandimpLotValuationDetail, LotValuationDetailViewHolder> {

    private Context context;
    private LotValuationDetailClickListener listener;

    public LotValuationDetailAdapter(Context context, LotValuationDetailClickListener listener) {
        super(CALL_BACK);
        this.context = context;
        this.listener = listener;
    }

    @NonNull
    @Override
    public LotValuationDetailViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        return new LotValuationDetailViewHolder(LayoutInflater.from(context).inflate(R.layout.list_lot_valuation_item, parent, false));
    }

    @Override
    public void onBindViewHolder(@NonNull LotValuationDetailViewHolder holder, int position) {
        ValrepLandimpLotValuationDetail detail = getItem(position);

        holder.titleNo.setText(detail.getValrepLandimpValueTctNo());
        holder.totalVal.setText(String.format("TOTAL LAND VALUE\n%s", detail.getValrepLandimpValueLandValue()));



        holder.constraintLayout.setOnClickListener(view -> listener.onDetailItemClick(detail.getRecordId(), detail.getUniqueId()));

    }

    private static DiffUtil.ItemCallback<ValrepLandimpLotValuationDetail> CALL_BACK = new DiffUtil.ItemCallback<ValrepLandimpLotValuationDetail>() {
        @Override
        public boolean areItemsTheSame(@NonNull ValrepLandimpLotValuationDetail oldItem, @NonNull ValrepLandimpLotValuationDetail newItem) {
            return oldItem.getUniqueId() == newItem.getUniqueId();
        }

        @Override
        public boolean areContentsTheSame(@NonNull ValrepLandimpLotValuationDetail oldItem, @NonNull ValrepLandimpLotValuationDetail newItem) {
            return oldItem.getUniqueId() == newItem.getUniqueId();
        }
    };
}
