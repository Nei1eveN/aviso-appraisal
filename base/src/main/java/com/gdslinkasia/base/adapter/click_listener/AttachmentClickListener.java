package com.gdslinkasia.base.adapter.click_listener;

import com.gdslinkasia.base.model.details.AppAttachment;

public interface AttachmentClickListener {
    void onPropDescItemClick(String recordId, long uniqueId, AppAttachment detail);
}
