package com.gdslinkasia.base.adapter;

import android.content.Context;
import android.content.Intent;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.DiffUtil;
import androidx.recyclerview.widget.ListAdapter;

import com.gdslinkasia.base.R;
import com.gdslinkasia.base.adapter.viewholders.JobViewHolder;
import com.gdslinkasia.base.model.job.Record;
import com.gdslinkasia.base.utils.ActivityIntent;

import static com.gdslinkasia.base.database.DatabaseUtils.APPRAISAL_TYPE;
import static com.gdslinkasia.base.database.DatabaseUtils.RECORD_ID;
import static com.gdslinkasia.base.utils.GlobalString.ACCEPTED_JOB;
import static com.gdslinkasia.base.utils.GlobalString.APPRAISAL_TYPE_SENTENCE_CHANGER;
import static com.gdslinkasia.base.utils.GlobalString.DATE_NUMBER_TO_MONTH_CONVERSION;
import static com.gdslinkasia.base.utils.GlobalString.FOR_ACCEPTANCE;
import static com.gdslinkasia.base.utils.GlobalString.FOR_REWORK;


public class ActiveJobAdapter extends ListAdapter<Record, JobViewHolder> {

    private Context context;

    public ActiveJobAdapter(@NonNull Context context) {
        super(CALL_BACK);
        this.context = context;
    }

    @NonNull
    @Override
    public JobViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        return new JobViewHolder(LayoutInflater.from(context).inflate(R.layout.list_job_item, parent, false));
    }

    @Override
    public void onBindViewHolder(@NonNull JobViewHolder holder, int position) {
        Record record = getItem(position);

        holder.appraisalType.setText(APPRAISAL_TYPE_SENTENCE_CHANGER(record.getAppraisalType()));

        holder.name.setText(String.format("%s %s %s", record.getAppAccountFirstName(), record.getAppAccountMiddleName(), record.getAppAccountLastName()));
//        holder.requestedDate.setText(String.format("%s %s %s", record.getAppDaterequestedMonth(), record.getAppDaterequestedDay(), record.getAppDaterequestedYear()));
        holder.requestedDate.setVisibility(View.INVISIBLE);

        holder.year.setText(record.getAppDaterequestedYear());
        holder.day.setText(record.getAppDaterequestedDay());
        holder.month.setText(DATE_NUMBER_TO_MONTH_CONVERSION(record.getAppDaterequestedMonth()).toUpperCase());

        holder.linearLayout.setOnClickListener(view -> {
            Log.d("int--AppraisalType", record.getAppraisalType());
            Log.d("int--ApplicationStatus", record.getApplicationStatus());
            try {
                switch (record.getApplicationStatus()) {
                    case FOR_ACCEPTANCE: {
                        Intent intent = new Intent(context, Class.forName(ActivityIntent.FOR_ACCEPTANCE_ACTIVITY));
                        intent.putExtra(RECORD_ID, record.getRecordId());
                        intent.putExtra(APPRAISAL_TYPE, APPRAISAL_TYPE_SENTENCE_CHANGER(record.getAppraisalType()));
                        context.startActivity(intent);
                        break;
                    }
                    case FOR_REWORK: {
                        Intent intent = new Intent(context, Class.forName(ActivityIntent.FOR_REWORK_ACTIVITY));
                        intent.putExtra(RECORD_ID, record.getRecordId());
                        intent.putExtra(APPRAISAL_TYPE, APPRAISAL_TYPE_SENTENCE_CHANGER(record.getAppraisalType()));
                        context.startActivity(intent);
                        break;
                    }
                    case ACCEPTED_JOB: {
                        Intent intent = new Intent(context, Class.forName(ActivityIntent.ACCEPTED_JOB_ACTIVITY));
                        intent.putExtra(RECORD_ID, record.getRecordId());
                        intent.putExtra(APPRAISAL_TYPE, APPRAISAL_TYPE_SENTENCE_CHANGER(record.getAppraisalType()));
                        context.startActivity(intent);
                        break;
                    }
                }
            } catch (ClassNotFoundException e) {
                e.printStackTrace();
            }
        });
    }

    private static DiffUtil.ItemCallback<Record> CALL_BACK = new DiffUtil.ItemCallback<Record>() {
        @Override
        public boolean areItemsTheSame(@NonNull Record oldItem, @NonNull Record newItem) {
            return oldItem.getRecordId().equals(newItem.getRecordId());
        }

        @Override
        public boolean areContentsTheSame(@NonNull Record oldItem, @NonNull Record newItem) {
            return oldItem.getRecordId().equals(newItem.getRecordId());
        }
    };
}
