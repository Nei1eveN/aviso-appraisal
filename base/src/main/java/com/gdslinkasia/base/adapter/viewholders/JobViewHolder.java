package com.gdslinkasia.base.adapter.viewholders;

import android.view.View;
import android.widget.LinearLayout;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.gdslinkasia.commonresources.R2;

import butterknife.BindView;
import butterknife.ButterKnife;


public class JobViewHolder extends RecyclerView.ViewHolder {

    @BindView(R2.id.linearLayout)
    public LinearLayout linearLayout;

    @BindView(R2.id.tvName)
    public TextView name;

    @BindView(R2.id.tvApprType)
    public TextView appraisalType;

    @BindView(R2.id.tvDate)
    public TextView requestedDate;

    @BindView(R2.id.tvYear) public TextView year;
    @BindView(R2.id.tvDay) public TextView day;
    @BindView(R2.id.tvMonth) public TextView month;

    public JobViewHolder(@NonNull View itemView) {
        super(itemView);
        ButterKnife.bind(this, itemView);
    }
}
