package com.gdslinkasia.base.adapter.click_listener;

public interface ViewListClickListener {
    void onItemDetailClick(String recordId, long uniqueId);
}
