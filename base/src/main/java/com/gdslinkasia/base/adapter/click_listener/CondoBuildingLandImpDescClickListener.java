package com.gdslinkasia.base.adapter.click_listener;

public interface CondoBuildingLandImpDescClickListener {
    void onPropDescItemClick(String recordId, long uniqueId);
}
