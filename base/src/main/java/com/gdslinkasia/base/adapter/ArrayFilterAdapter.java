package com.gdslinkasia.base.adapter;

import android.content.Context;
import android.util.Log;
import android.widget.ArrayAdapter;
import android.widget.Filter;
import android.widget.Filterable;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;

import java.util.ArrayList;
import java.util.List;

public class ArrayFilterAdapter extends ArrayAdapter<String> implements Filterable {

    private List<String> newItems = new ArrayList<>();
    private List<String> originalItems = new ArrayList<>();

    public ArrayFilterAdapter(@NonNull Context context, int resource, @NonNull List<String> objects) {
        super(context, resource, objects);
        originalItems.addAll(objects);
    }

    @Override
    public int getCount() {
        if (newItems != null)
            return newItems.size();
        else
            return 0;
    }

    @Nullable
    @Override
    public String getItem(int position) {
        return newItems.get(position);
    }

    @NonNull
    @Override
    public Filter getFilter() {

        return new Filter() {
            @Override
            protected FilterResults performFiltering(CharSequence constraint) {

                if (constraint != null)
                    Log.d("Constraints", constraint.toString());
                FilterResults oReturn = new FilterResults();

                /*  if (orig == null){
                    for (int i = 0; i < items.size(); i++) {
                        orig.add(items.get(i));
                    }
                  }*/
                String temp;
                int counters = 0;
                if (constraint != null) {

                    newItems.clear();
                    if (originalItems != null && originalItems.size() > 0) {
                        for (int i = 0; i < originalItems.size(); i++) {
                            temp = originalItems.get(i).toUpperCase();

                            if (temp.startsWith(constraint.toString().toUpperCase())) {

                                newItems.add(originalItems.get(i));
                                counters++;

                            }
                        }
                    }
                    Log.d("Result size:", String.valueOf(newItems.size()));
                    if (counters != 0) {
                        newItems.clear();
                        newItems = originalItems;
                    }
                    oReturn.values = newItems;
                    oReturn.count = newItems.size();
                }
                return oReturn;
            }


            @Override
            protected void publishResults(CharSequence constraint, FilterResults results) {
                if (results != null && results.count > 0) {
                    notifyDataSetChanged();
                }
                else {
                    notifyDataSetInvalidated();
                }

            }

        };
    }
}
