package com.gdslinkasia.base.adapter.viewholders;

import android.view.View;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.constraintlayout.widget.ConstraintLayout;
import androidx.recyclerview.widget.RecyclerView;

import com.gdslinkasia.commonresources.R2;

import butterknife.BindView;
import butterknife.ButterKnife;

public class ViewListViewHolder extends RecyclerView.ViewHolder {

    @BindView(R2.id.constraintLayout) public ConstraintLayout constraintLayout;
    @BindView(R2.id.tvTitleNo) public TextView title;

    public ViewListViewHolder(@NonNull View itemView) {
        super(itemView);
        ButterKnife.bind(this, itemView);
    }
}
