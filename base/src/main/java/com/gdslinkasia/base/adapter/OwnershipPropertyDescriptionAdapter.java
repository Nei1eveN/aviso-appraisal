package com.gdslinkasia.base.adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.ViewGroup;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.DiffUtil;
import androidx.recyclerview.widget.ListAdapter;

import com.gdslinkasia.base.R;
import com.gdslinkasia.base.adapter.click_listener.OwnershipPropDescClickListener;
import com.gdslinkasia.base.adapter.viewholders.OwnershipPropDescViewHolder;
import com.gdslinkasia.base.model.details.ValrepLandimpLotDetail;

import java.util.Locale;

public class OwnershipPropertyDescriptionAdapter extends ListAdapter<ValrepLandimpLotDetail, OwnershipPropDescViewHolder> {

    private Context context;
    private OwnershipPropDescClickListener listener;

    public OwnershipPropertyDescriptionAdapter(Context context, OwnershipPropDescClickListener listener) {
        super(CALL_BACK);
        this.context = context;
        this.listener = listener;
    }

    @NonNull
    @Override
    public OwnershipPropDescViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        return new OwnershipPropDescViewHolder(LayoutInflater.from(context).inflate(R.layout.list_own_prop_desc_item, parent, false));
    }

    @Override
    public void onBindViewHolder(@NonNull OwnershipPropDescViewHolder holder, int position) {
        ValrepLandimpLotDetail detail = getItem(position);

        holder.title.setText(String.format(Locale.ENGLISH,"Description %d", position+1));
        holder.title.append("\nTitle Type: "+detail.getValrepLandimpPropdescTitleType());

        holder.constraintLayout.setOnClickListener(v -> listener.onPropDescItemClick(detail.getRecordId(), detail.getUniqueId(), position+1));
    }

    private static DiffUtil.ItemCallback<ValrepLandimpLotDetail> CALL_BACK = new DiffUtil.ItemCallback<ValrepLandimpLotDetail>() {
        @Override
        public boolean areItemsTheSame(@NonNull ValrepLandimpLotDetail oldItem, @NonNull ValrepLandimpLotDetail newItem) {
            return oldItem.getUniqueId() == newItem.getUniqueId();
        }

        @Override
        public boolean areContentsTheSame(@NonNull ValrepLandimpLotDetail oldItem, @NonNull ValrepLandimpLotDetail newItem) {
            return oldItem.getUniqueId() == newItem.getUniqueId();
        }
    };
}
