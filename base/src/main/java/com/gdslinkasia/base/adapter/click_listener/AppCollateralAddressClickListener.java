package com.gdslinkasia.base.adapter.click_listener;

import com.gdslinkasia.base.model.details.AppCollateralAddress;

public interface AppCollateralAddressClickListener {
    void getAddressDetails(AppCollateralAddress address);
    void getAddressReferenceIds(long uniqueId); //String recordId,
}
