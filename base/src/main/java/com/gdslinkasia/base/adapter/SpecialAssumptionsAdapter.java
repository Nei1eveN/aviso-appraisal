package com.gdslinkasia.base.adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.ViewGroup;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.DiffUtil;
import androidx.recyclerview.widget.ListAdapter;

import com.gdslinkasia.base.R;
import com.gdslinkasia.base.adapter.click_listener.OwnershipPropDescClickListener;
import com.gdslinkasia.base.adapter.viewholders.QualifyingStatementViewHolder;
import com.gdslinkasia.base.model.details.ValrepLandimpAssumptionsRemark;

import java.util.Locale;

public class SpecialAssumptionsAdapter extends ListAdapter<ValrepLandimpAssumptionsRemark, QualifyingStatementViewHolder> {

    private Context context;
    private OwnershipPropDescClickListener listener;

    public SpecialAssumptionsAdapter(Context context, OwnershipPropDescClickListener listener) {
        super(CALL_BACK);
        this.context = context;
        this.listener = listener;
    }

    @NonNull
    @Override
    public QualifyingStatementViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        return new QualifyingStatementViewHolder(LayoutInflater.from(context).inflate(R.layout.list_qualifying_statement_item, parent, false));
    }

    @Override
    public void onBindViewHolder(@NonNull QualifyingStatementViewHolder holder, int position) {
        ValrepLandimpAssumptionsRemark detail = getItem(position);

        holder.title.setText(String.format(Locale.ENGLISH,"Special Assumption %d", position+1));
        holder.description.setText(detail.getValrepLandimpAssumptions());

        holder.constraintLayout.setOnClickListener(v -> listener.onPropDescItemClick(detail.getRecordId(), detail.getUniqueId(), position+1));
    }

    private static DiffUtil.ItemCallback<ValrepLandimpAssumptionsRemark> CALL_BACK = new DiffUtil.ItemCallback<ValrepLandimpAssumptionsRemark>() {
        @Override
        public boolean areItemsTheSame(@NonNull ValrepLandimpAssumptionsRemark oldItem, @NonNull ValrepLandimpAssumptionsRemark newItem) {
            return oldItem.getUniqueId() != newItem.getUniqueId();
        }

        @Override
        public boolean areContentsTheSame(@NonNull ValrepLandimpAssumptionsRemark oldItem, @NonNull ValrepLandimpAssumptionsRemark newItem) {
            return oldItem.getUniqueId() != newItem.getUniqueId();
        }
    };
}
