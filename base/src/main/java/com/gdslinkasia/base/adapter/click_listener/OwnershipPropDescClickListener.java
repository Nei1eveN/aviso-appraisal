package com.gdslinkasia.base.adapter.click_listener;

public interface OwnershipPropDescClickListener {
    void onPropDescItemClick(String recordId, long uniqueId, int position);
}
