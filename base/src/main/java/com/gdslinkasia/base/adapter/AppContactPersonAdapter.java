package com.gdslinkasia.base.adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.ViewGroup;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.DiffUtil;
import androidx.recyclerview.widget.ListAdapter;

import com.gdslinkasia.base.R;
import com.gdslinkasia.base.adapter.click_listener.AppContactPersonClickListener;
import com.gdslinkasia.base.adapter.viewholders.AppContactPersonViewHolder;
import com.gdslinkasia.base.model.details.AppContactPerson;

public class AppContactPersonAdapter extends ListAdapter<AppContactPerson, AppContactPersonViewHolder> {

    private Context context;
    private AppContactPersonClickListener listener;

    public AppContactPersonAdapter(Context context, AppContactPersonClickListener listener) {
        super(CALL_BACK);
        this.context = context;
        this.listener = listener;
    }

    @NonNull
    @Override
    public AppContactPersonViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        return new AppContactPersonViewHolder(LayoutInflater.from(context).inflate(R.layout.list_app_contact_person_item, parent, false));
    }

    @Override
    public void onBindViewHolder(@NonNull AppContactPersonViewHolder holder, int position) {
        AppContactPerson person = getItem(position);

        holder.contactName.setText(person.getAppContactPersonName());
        holder.prefix.setText(person.getAppMobilePrefix());
        holder.mobileNo.setText(person.getAppMobileNo());
        holder.landline.setText(String.format("%s %s", person.getAppTelephoneArea(), person.getAppTelephoneNo()));
        holder.constraintLayout.setOnClickListener(v -> listener.onContactItemClick(person.getUniqueId()));
    }

    private static DiffUtil.ItemCallback<AppContactPerson> CALL_BACK = new DiffUtil.ItemCallback<AppContactPerson>() {
        @Override
        public boolean areItemsTheSame(@NonNull AppContactPerson oldItem, @NonNull AppContactPerson newItem) {
            return oldItem.getUniqueId() == newItem.getUniqueId();
        }

        @Override
        public boolean areContentsTheSame(@NonNull AppContactPerson oldItem, @NonNull AppContactPerson newItem) {
            return oldItem.getUniqueId() == newItem.getUniqueId();
        }
    };
}
