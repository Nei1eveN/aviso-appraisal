package com.gdslinkasia.base.adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.ViewGroup;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.DiffUtil;
import androidx.recyclerview.widget.ListAdapter;

import com.gdslinkasia.base.R;
import com.gdslinkasia.base.adapter.click_listener.AppCollateralAddressClickListener;
import com.gdslinkasia.base.adapter.viewholders.SubjPropertyAddressViewHolder;
import com.gdslinkasia.base.model.details.AppCollateralAddress;

public class SubjPropertyAddressAdapter extends ListAdapter<AppCollateralAddress, SubjPropertyAddressViewHolder> {

    private Context context;
    private AppCollateralAddressClickListener listener;

    public SubjPropertyAddressAdapter(Context context, AppCollateralAddressClickListener listener) {
        super(CALL_BACK);
        this.context = context;
        this.listener = listener;
    }


    @NonNull
    @Override
    public SubjPropertyAddressViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        return new SubjPropertyAddressViewHolder(LayoutInflater.from(context).inflate(R.layout.list_subj_property_item, parent, false));
    }

    @Override
    public void onBindViewHolder(@NonNull SubjPropertyAddressViewHolder holder, int position) {
        AppCollateralAddress address = getItem(position);

        holder.subjPropType.setText(address.getAppAddressType());
        holder.address.setText(String.format("%s %s %s %s %s %s %s %s %s %s %s", address.getAppLotNo(), address.getAppBlockNo(), address.getAppStreetName(), address.getAppVillage(), address.getAppBrgy(), address.getAppZip(), address.getAppDistrict(), address.getAppCity(), address.getAppProvince(), address.getAppRegion(), address.getAppCountry()));
        holder.constraintLayout.setOnClickListener(v -> listener.getAddressReferenceIds(address.getUniqueId())); //address.getRecordId(),
    }

    private static DiffUtil.ItemCallback<AppCollateralAddress> CALL_BACK = new DiffUtil.ItemCallback<AppCollateralAddress>() {
        @Override
        public boolean areItemsTheSame(@NonNull AppCollateralAddress oldItem, @NonNull AppCollateralAddress newItem) {
            return oldItem.getUniqueId() == newItem.getUniqueId();
        }

        @Override
        public boolean areContentsTheSame(@NonNull AppCollateralAddress oldItem, @NonNull AppCollateralAddress newItem) {
            return oldItem.getUniqueId() == newItem.getUniqueId();
        }
    };
}
