package com.gdslinkasia.base.adapter.viewholders;

import android.view.View;

import androidx.annotation.NonNull;
import androidx.cardview.widget.CardView;
import androidx.constraintlayout.widget.ConstraintLayout;
import androidx.recyclerview.widget.RecyclerView;

import com.gdslinkasia.commonresources.R2;
import com.google.android.material.textfield.TextInputEditText;

import butterknife.BindView;
import butterknife.ButterKnife;

public class SubjPropertyAddressViewHolder extends RecyclerView.ViewHolder {

    @BindView(R2.id.cardView) public CardView cardView;
    @BindView(R2.id.constraintLayout) public ConstraintLayout constraintLayout;
    @BindView(R2.id.etSubjPropType) public TextInputEditText subjPropType;
    @BindView(R2.id.etAddress) public TextInputEditText address;

    public SubjPropertyAddressViewHolder(@NonNull View itemView) {
        super(itemView);
        ButterKnife.bind(this, itemView);
    }
}
