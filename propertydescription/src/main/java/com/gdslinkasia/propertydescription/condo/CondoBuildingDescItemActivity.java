package com.gdslinkasia.propertydescription.condo;

import android.app.ProgressDialog;
import android.content.Intent;
import android.os.Bundle;
import android.view.MenuItem;
import android.widget.AutoCompleteTextView;
import android.widget.MultiAutoCompleteTextView;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AlertDialog;
import androidx.appcompat.app.AppCompatActivity;

import com.gdslinkasia.base.model.details.ValrepLandimpImpDetail;
import com.gdslinkasia.commonresources.R2;
import com.gdslinkasia.propertydescription.R;
import com.gdslinkasia.propertydescription.property_description_contract.condo_building_land_imp_item.CondoBuildingLandImpItemContract;
import com.gdslinkasia.propertydescription.property_description_contract.condo_building_land_imp_item.CondoBuildingLandImpItemPresenterImpl;

import java.util.Objects;

import butterknife.BindView;
import butterknife.ButterKnife;

import static com.gdslinkasia.base.database.DatabaseUtils.*;
import static com.gdslinkasia.base.utils.GlobalFunctions.SET_CONTENT_DROPDOWN;

public class CondoBuildingDescItemActivity extends AppCompatActivity implements CondoBuildingLandImpItemContract.View {

    public final String TAG = this.getClass().getSimpleName();

    private long uniqueId;

    @BindView(R2.id.txtDialogTitle) TextView txtDialogTitle;

    @BindView(R2.id.acValApproach) AutoCompleteTextView acValApproach;
    @BindView(R2.id.acDescBuilding) AutoCompleteTextView acDescBuilding;
    @BindView(R2.id.acFloorArea) AutoCompleteTextView acFloorArea;
    @BindView(R2.id.acPresentUtil) AutoCompleteTextView acPresentUtil;
    @BindView(R2.id.acPresentCondition) AutoCompleteTextView acPresentCondition;
    @BindView(R2.id.acStructuralFraming) AutoCompleteTextView acStructuralFraming;
    @BindView(R2.id.acNumFloor) AutoCompleteTextView acNumFloor;
    @BindView(R2.id.acNumBedrooms) AutoCompleteTextView acNumBedrooms;
    @BindView(R2.id.acNumTB) AutoCompleteTextView acNumTB;
    @BindView(R2.id.acFoundation) AutoCompleteTextView acFoundation;

    @BindView(R2.id.acExteriorWalls) MultiAutoCompleteTextView acExteriorWalls;
    @BindView(R2.id.acInteriorWalls) MultiAutoCompleteTextView acInteriorWalls;
    @BindView(R2.id.acFlooring) MultiAutoCompleteTextView acFlooring;
    @BindView(R2.id.acRoofing) MultiAutoCompleteTextView acRoofing;
    @BindView(R2.id.acCeiling) MultiAutoCompleteTextView acCeiling;
    @BindView(R2.id.acDoors) MultiAutoCompleteTextView acDoors;
    @BindView(R2.id.acWindows) MultiAutoCompleteTextView acWindows;
    @BindView(R2.id.acPartition) MultiAutoCompleteTextView acPartition;
    @BindView(R2.id.acOthers) MultiAutoCompleteTextView acOthers;

    @BindView(R2.id.acEcoLife) AutoCompleteTextView acEcoLife;
    @BindView(R2.id.acEffectiveAge) AutoCompleteTextView acEffectiveAge;
    @BindView(R2.id.acRemLife) AutoCompleteTextView acRemLife;

    @BindView(R2.id.acRemarks) AutoCompleteTextView acRemarks;

    private ProgressDialog progressDialog;

    private CondoBuildingLandImpItemContract.Presenter presenter;
    private String appraisalType;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.content_condo_building_desc);
        ButterKnife.bind(this);

        Intent intent = getIntent();
        uniqueId = intent.getLongExtra(UNIQUE_ID, 0);
        appraisalType = intent.getStringExtra(APPRAISAL_TYPE);

        Objects.requireNonNull(getSupportActionBar()).setTitle("Property Details");
        getSupportActionBar().setSubtitle("Condominium Building Description "+uniqueId);
        getSupportActionBar().setIcon(R.drawable.ic_close_white_24dp);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);

        progressDialog = new ProgressDialog(this);
        presenter = new CondoBuildingLandImpItemPresenterImpl(this, getApplication());
    }

    @Override
    public boolean onOptionsItemSelected(@NonNull MenuItem item) {
        if (item.getItemId() == android.R.id.home) {
            finish();
        }
        return super.onOptionsItemSelected(item);
    }

    @Override
    protected void onResume() {
        super.onResume();
        presenter.onStart(uniqueId);
    }



    @Override
    protected void onPause() {
        super.onPause();
        presenter.updateItemDetails(
                uniqueId,
                Objects.requireNonNull(acValApproach.getText()).toString(),
                Objects.requireNonNull(acDescBuilding.getText()).toString(),
                Objects.requireNonNull(acFloorArea.getText()).toString(),
                Objects.requireNonNull(acPresentUtil.getText()).toString(),
                Objects.requireNonNull(acPresentCondition.getText()).toString(),
                Objects.requireNonNull(acStructuralFraming.getText()).toString(),
                Objects.requireNonNull(acNumFloor.getText()).toString(),
                Objects.requireNonNull(acNumBedrooms.getText()).toString(),
                Objects.requireNonNull(acNumTB.getText()).toString(),
                Objects.requireNonNull(acFoundation.getText()).toString(),
                Objects.requireNonNull(acExteriorWalls.getText()).toString(),
                Objects.requireNonNull(acInteriorWalls.getText()).toString(),
                Objects.requireNonNull(acFlooring.getText()).toString(),
                Objects.requireNonNull(acRoofing.getText()).toString(),
                Objects.requireNonNull(acCeiling.getText()).toString(),
                Objects.requireNonNull(acDoors.getText()).toString(),
                Objects.requireNonNull(acWindows.getText()).toString(),
                Objects.requireNonNull(acPartition.getText()).toString(),
                Objects.requireNonNull(acOthers.getText()).toString(),
                Objects.requireNonNull(acEcoLife.getText()).toString(),
                Objects.requireNonNull(acEffectiveAge.getText()).toString(),
                Objects.requireNonNull(acRemLife.getText()).toString(),
                Objects.requireNonNull(acRemarks.getText()).toString()
        );
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        presenter.onDestroy();
        progressDialog.dismiss();
    }

    @Override
    public void showProgress(String title, String message) {
        progressDialog.setCancelable(false);
        progressDialog.setIndeterminate(false);
        progressDialog.setTitle(title);
        progressDialog.setMessage(message);
        progressDialog.show();
    }

    @Override
    public void hideProgress() {
        progressDialog.hide();
    }

    @Override
    public void showDetails(ValrepLandimpImpDetail detail) {

        acValApproach.setText(detail.getValrepLandimpDescValuationApproach());
        SET_CONTENT_DROPDOWN(this,acValApproach, R.array.valuationApproachArray, true);
        acDescBuilding.setText(detail.getValrepLandimpImpsummary1BuildingDesc());
        acFloorArea.setText(detail.getValrepLandimpDescImpFloorArea());
        acPresentUtil.setText(detail.getValrepLandimpTaxDecImpTdClassification());
        acPresentCondition.setText(detail.getValrepLandimpDescObservedCondition());
        SET_CONTENT_DROPDOWN(this,acPresentCondition, R.array.presentConditionArray, true);

        acStructuralFraming.setText(detail.getValrepLandimpDescConstructionFeature());
        acNumFloor.setText(detail.getValrepLandimpImpsummary1NoOfFloors());
        acNumBedrooms.setText(detail.getValrepLandimpImpsummary1NoOfBedrooms());
        acNumTB.setText(detail.getValrepLandimpDescImpsummary1NoOfTB());
        acFoundation.setText(detail.getValrepLandimpDescFoundation());
        SET_CONTENT_DROPDOWN(this,acFoundation, R.array.foundationArray, false);

        //multi ACTV dropdowns
        acExteriorWalls.setText(detail.getValrepLandimpDescExteriorWalls());
        acInteriorWalls.setText(detail.getValrepLandimpDescInteriorWalls());
        acFlooring.setText(detail.getValrepLandimpDescImpFlooring());
        acRoofing.setText(detail.getValrepLandimpDescImpRoofing());
        acCeiling.setText(detail.getValrepLandimpDescCeiling());
        acDoors.setText(detail.getValrepLandimpDescDoors());
        acWindows.setText(detail.getValrepLandimpDescImpWindows());
        acPartition.setText(detail.getValrepLandimpDescPartitions());

        acOthers.setText(detail.getValrepLandimpDescOthers());
        acEcoLife.setText(detail.getValrepLandimpDescEconomicLife());
        acEffectiveAge.setText(detail.getValrepLandimpDescEffectiveAge());
        acRemLife.setText(detail.getValrepLandimpDescImpRemainLife());
        acRemarks.setText(detail.getValrepLandimpDescImpRemarks());

        //multi ACTV dropdown
        SET_CONTENT_DROPDOWN(this,acExteriorWalls, R.array.exteriorWallsArray, false);
        acExteriorWalls.setTokenizer(new MultiAutoCompleteTextView.CommaTokenizer());
        acExteriorWalls.setThreshold(1);

        SET_CONTENT_DROPDOWN(this,acInteriorWalls, R.array.interiorWallsArray, false);
        acInteriorWalls.setTokenizer(new MultiAutoCompleteTextView.CommaTokenizer());
        acInteriorWalls.setThreshold(1);

        SET_CONTENT_DROPDOWN(this,acFlooring, R.array.flooringsArray, false);
        acFlooring.setTokenizer(new MultiAutoCompleteTextView.CommaTokenizer());
        acFlooring.setThreshold(1);

        SET_CONTENT_DROPDOWN(this,acRoofing, R.array.roofingArray, false);
        acRoofing.setTokenizer(new MultiAutoCompleteTextView.CommaTokenizer());
        acRoofing.setThreshold(1);

        SET_CONTENT_DROPDOWN(this,acCeiling, R.array.ceilingArray, false);
        acCeiling.setTokenizer(new MultiAutoCompleteTextView.CommaTokenizer());
        acCeiling.setThreshold(1);

        SET_CONTENT_DROPDOWN(this,acDoors, R.array.doorsArray, false);
        acDoors.setTokenizer(new MultiAutoCompleteTextView.CommaTokenizer());
        acDoors.setThreshold(1);

        SET_CONTENT_DROPDOWN(this,acWindows, R.array.windowsArray, false);
        acWindows.setTokenizer(new MultiAutoCompleteTextView.CommaTokenizer());
        acWindows.setThreshold(1);

        SET_CONTENT_DROPDOWN(this,acPartition, R.array.partitionsArray, false);
        acPartition.setTokenizer(new MultiAutoCompleteTextView.CommaTokenizer());
        acPartition.setThreshold(1);


    }

    @Override
    public void showExitDialog(String title, String message) {
        new AlertDialog.Builder(this)
                .setCancelable(false)
                .setTitle(title)
                .setMessage(message)
                .setPositiveButton("CLOSE", (dialog, which) -> finish())
                .create()
                .show();
    }
}
