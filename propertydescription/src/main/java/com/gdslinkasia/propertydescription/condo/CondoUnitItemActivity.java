package com.gdslinkasia.propertydescription.condo;

import android.app.ProgressDialog;
import android.content.Intent;
import android.os.Bundle;
import android.view.MenuItem;
import android.widget.AutoCompleteTextView;
import android.widget.MultiAutoCompleteTextView;

import androidx.appcompat.app.AlertDialog;
import androidx.appcompat.app.AppCompatActivity;

import com.gdslinkasia.base.model.details.ValrepLandimpLotDetail;
import com.gdslinkasia.commonresources.R2;
import com.gdslinkasia.propertydescription.R;
import com.gdslinkasia.propertydescription.property_description_contract.condo_unit_item.CondoUnitItemContract;
import com.gdslinkasia.propertydescription.property_description_contract.condo_unit_item.CondoUnitItemPresenterImpl;

import java.util.Objects;

import butterknife.BindView;
import butterknife.ButterKnife;

import static com.gdslinkasia.base.database.DatabaseUtils.*;
import static com.gdslinkasia.base.utils.GlobalFunctions.SET_CONTENT_DROPDOWN;

public class CondoUnitItemActivity extends AppCompatActivity implements CondoUnitItemContract.View {

    public final String TAG = this.getClass().getSimpleName();

    private long uniqueId;

    @BindView(R2.id.acTitleType) AutoCompleteTextView acTitleType;
    @BindView(R2.id.acTitleNo) AutoCompleteTextView acTitleNo;
    @BindView(R2.id.acUnitNo) AutoCompleteTextView acUnitNo;
    @BindView(R2.id.acFloor) AutoCompleteTextView acFloor;
    @BindView(R2.id.acFloorArea) AutoCompleteTextView acFloorArea;
    @BindView(R2.id.acPartition) MultiAutoCompleteTextView acPartition;
    @BindView(R2.id.acFlooring) MultiAutoCompleteTextView acFlooring;
    @BindView(R2.id.acCeiling) MultiAutoCompleteTextView acCeiling;
    @BindView(R2.id.acDoors) MultiAutoCompleteTextView acDoors;
    @BindView(R2.id.acPresentCondition) AutoCompleteTextView acPresentCondition;
    @BindView(R2.id.acOthers) AutoCompleteTextView acOthers;

    private ProgressDialog progressDialog;

    private CondoUnitItemContract.Presenter presenter;

//    DatePickerDialog datePickerDialog = new DatePickerDialog();

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.content_condo_unit_desc);
        ButterKnife.bind(this);

        Intent intent = getIntent();
        uniqueId = intent.getLongExtra(UNIQUE_ID, 0);

        Objects.requireNonNull(getSupportActionBar()).setTitle("Property Details");
        getSupportActionBar().setSubtitle("Condominium Unit Description "+uniqueId);
        getSupportActionBar().setIcon(R.drawable.ic_close_white_24dp);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);

        progressDialog = new ProgressDialog(this);
        presenter = new CondoUnitItemPresenterImpl(this, getApplication());
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        if (item.getItemId() == android.R.id.home) {
            finish();
        }
        return super.onOptionsItemSelected(item);
    }

    @Override
    protected void onResume() {
        super.onResume();
        presenter.onStart(uniqueId);
    }

    @Override
    protected void onPause() {
        super.onPause();
        presenter.updateItemDetails(
                uniqueId,
                acTitleType.getText().toString(),
                Objects.requireNonNull(acTitleNo.getText()).toString(),
                Objects.requireNonNull(acUnitNo.getText()).toString(),
                Objects.requireNonNull(acFloor.getText()).toString(),
                Objects.requireNonNull(acFloorArea.getText()).toString(),

                Objects.requireNonNull(acPartition.getText()).toString(),
                Objects.requireNonNull(acFlooring.getText()).toString(),
                Objects.requireNonNull(acCeiling.getText()).toString(),
                Objects.requireNonNull(acDoors.getText()).toString(),
                Objects.requireNonNull(acPresentCondition.getText()).toString(),
                Objects.requireNonNull(acOthers.getText()).toString()
        );
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        presenter.onDestroy();
        progressDialog.dismiss();
    }

    @Override
    public void showProgress(String title, String message) {
        progressDialog.setCancelable(false);
        progressDialog.setIndeterminate(false);
        progressDialog.setTitle(title);
        progressDialog.setMessage(message);
        progressDialog.show();
    }

    @Override
    public void hideProgress() {
        progressDialog.hide();
    }

    @Override
    public void showDetails(ValrepLandimpLotDetail detail) {

        acTitleType.setText(detail.getValrepLandimpPropdescTitleType());
        SET_CONTENT_DROPDOWN(this,acTitleType, R.array.condoTitleType, true);

        acTitleNo.setText(detail.getValrepLandimpPropdescTctNo());
        acUnitNo.setText(detail.getValrepLandimpPropdescLot());
        acFloor.setText(detail.getValrepLandimpPropdescBlock());
        acFloorArea.setText(detail.getValrepLandimpPropdescArea());

        //multi ACTV dropdown
        acPartition.setText(detail.getValrepLandimpDescPartitionsUnit());
        SET_CONTENT_DROPDOWN(this,acPartition, R.array.partitionsArray, false);
        acPartition.setTokenizer(new MultiAutoCompleteTextView.CommaTokenizer());
        acPartition.setThreshold(1);

        acFlooring.setText(detail.getValrepLandimpDescImpFlooringUnit());
        SET_CONTENT_DROPDOWN(this,acFlooring, R.array.flooringsArray, false);
        acFlooring.setTokenizer(new MultiAutoCompleteTextView.CommaTokenizer());
        acFlooring.setThreshold(1);

        acCeiling.setText(detail.getValrepLandimpDescCeilingUntit());
        SET_CONTENT_DROPDOWN(this,acCeiling, R.array.ceilingArray, false);
        acCeiling.setTokenizer(new MultiAutoCompleteTextView.CommaTokenizer());
        acCeiling.setThreshold(1);

        acDoors.setText(detail.getValrepLandimpDescDoorsUnit());
        SET_CONTENT_DROPDOWN(this,acDoors, R.array.doorsArray, false);
        acDoors.setTokenizer(new MultiAutoCompleteTextView.CommaTokenizer());
        acDoors.setThreshold(1);

        acPresentCondition.setText(detail.getValrepLandimpDescObservedConditionUnit());
        SET_CONTENT_DROPDOWN(this,acFlooring, R.array.presentConditionArray, true);


        acOthers.setText(detail.getValrepLandimpDescOthersUnit());

    }

    @Override
    public void showExitDialog(String title, String message) {
        new AlertDialog.Builder(this)
                .setCancelable(false)
                .setTitle(title)
                .setMessage(message)
                .setPositiveButton("CLOSE", (dialog, which) -> finish())
                .create()
                .show();
    }
}
