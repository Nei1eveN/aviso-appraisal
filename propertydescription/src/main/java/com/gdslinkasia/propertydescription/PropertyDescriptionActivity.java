package com.gdslinkasia.propertydescription;

import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;
import android.view.MenuItem;
import android.view.View;
import android.widget.AutoCompleteTextView;
import android.widget.CheckBox;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;
import androidx.constraintlayout.widget.ConstraintLayout;
import androidx.fragment.app.FragmentTransaction;

import com.gdslinkasia.base.model.job.Record;
import com.gdslinkasia.commonresources.R2;
import com.gdslinkasia.propertydescription.condo.CondoBuildingDescFragment;
import com.gdslinkasia.propertydescription.condo.CondoUnitDescFragment;
import com.gdslinkasia.propertydescription.land_improvement.ImprovementDescFragment;
import com.gdslinkasia.propertydescription.land_improvement.ImprovementOtherDescFragment;
import com.gdslinkasia.propertydescription.land_only.OwnershipPropertyDescActivity;
import com.gdslinkasia.propertydescription.property_description_contract.UpdatePropertyDescContract;
import com.gdslinkasia.propertydescription.property_description_contract.UpdatePropertyDescPresenterImpl;
import com.google.android.material.textfield.TextInputLayout;

import java.util.Objects;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

import static com.gdslinkasia.base.database.DatabaseUtils.APPRAISAL_TYPE;
import static com.gdslinkasia.base.database.DatabaseUtils.RECORD_ID;
import static com.gdslinkasia.base.database.DatabaseUtils.VAR_CONDO;
import static com.gdslinkasia.base.database.DatabaseUtils.VAR_LAND_IMP;
import static com.gdslinkasia.base.database.DatabaseUtils.VAR_LAND_ONLY;
import static com.gdslinkasia.base.utils.GlobalFunctions.GET_CONTENT_ADAPTER;
import static com.gdslinkasia.base.utils.GlobalFunctions.SET_CONTENT_DROPDOWN;
import static com.gdslinkasia.base.utils.GlobalFunctions.getCheckboxBoolean;
import static com.gdslinkasia.base.utils.GlobalString.CHECKBOX_TO_STRING;

public class PropertyDescriptionActivity extends AppCompatActivity implements UpdatePropertyDescContract.View {

    @BindView(R2.id.ownershipPropDescLayout) ConstraintLayout ownershipPropDescLayout;

    @BindView(R2.id.txtOwnershipCondoDesc) TextView txtOwnershipCondoDesc;
    @BindView(R2.id.txtImprovementAndBuildingDesc) TextView txtImprovementAndBuildingDesc;
    @BindView(R2.id.llImprovementAndBuildingDesc) ConstraintLayout llImprovementAndBuildingDesc;
    @BindView(R2.id.llImprovementOtherDesc) ConstraintLayout llimprovementOtherDesc;

    @BindView(R2.id.txDeedRestrict) TextInputLayout txDeedRestrict;
    @BindView(R2.id.acDeedRestrict) AutoCompleteTextView acDeedRestrict;
    @BindView(R2.id.txDescEncumb) TextInputLayout txDescEncumb;
    @BindView(R2.id.acDescEncumb) AutoCompleteTextView acDescEncumb;

    /*TODO PHYSICAL FEATURES*/
    @BindView(R2.id.txPhysTypeLot) TextInputLayout txPhysTypeLot;
    @BindView(R2.id.acPhysTypeLot) AutoCompleteTextView acPhysTypeLot;
    @BindView(R2.id.txPhysShape) TextInputLayout txPhysShape;
    @BindView(R2.id.acPhysShape) AutoCompleteTextView acPhysShape;
    @BindView(R2.id.txPhysTopography) TextInputLayout txPhysTopography;
    @BindView(R2.id.acTopography) AutoCompleteTextView acTopography;
    @BindView(R2.id.txPhysElevation) TextInputLayout txPhysElevation;
    @BindView(R2.id.acPhysElevation) AutoCompleteTextView acPhysElevation;
    @BindView(R2.id.txPhysFrontage) TextInputLayout txPhysFrontage;
    @BindView(R2.id.acPhysFrontage) AutoCompleteTextView acPhysFrontage;
    @BindView(R2.id.txPhysDepth) TextInputLayout txPhysDepth;
    @BindView(R2.id.acPhysDepth) AutoCompleteTextView acPhysDepth;

    /*TODO BASIS OF IDENTIFICATION*/
    @BindView(R2.id.cbBasisPlot) CheckBox cbBasisPlot;
    @BindView(R2.id.cbBasisApprove) CheckBox cbBasisAprrove;
    @BindView(R2.id.cbBasisSurvey) CheckBox cbBasisSurvey;
    @BindView(R2.id.cbBasisTax) CheckBox cbBasisTax;
    @BindView(R2.id.cbBasisSubdiv) CheckBox cbBasisSubdiv;
    @BindView(R2.id.txBasisOthers) TextInputLayout txBasisOthers;
    @BindView(R2.id.acBasisOthers) AutoCompleteTextView acBasisOthers;

    /*TODO BOUNDARIES*/
    @BindView(R2.id.txBoundFront) TextInputLayout txBoundFront;
    @BindView(R2.id.acBoundFront) AutoCompleteTextView acBoundFront;
    @BindView(R2.id.txBoundRear) TextInputLayout txBoundRear;
    @BindView(R2.id.acBoundRear) AutoCompleteTextView acBoundRear;
    @BindView(R2.id.txBoundLeft) TextInputLayout txBoundLeft;
    @BindView(R2.id.acBoundLeft) AutoCompleteTextView acBoundLeft;
    @BindView(R2.id.txBoundRight) TextInputLayout txBoundRight;
    @BindView(R2.id.acBoundRight) AutoCompleteTextView acBoundRight;

    /*TODO ZONING ORDINANCE*/
    @BindView(R2.id.txZoning) TextInputLayout txZoning;
    @BindView(R2.id.acZoning) AutoCompleteTextView acZoning;

    /*TODO GENERAL*/
    @BindView(R2.id.txGenClass) TextInputLayout txGenClass;
    @BindView(R2.id.acGenClass) AutoCompleteTextView acGenClass;
    @BindView(R2.id.txGenMunicipality) TextInputLayout txGenMunicipality;
    @BindView(R2.id.acGenMunicipality) AutoCompleteTextView acGenMunicipality;
    @BindView(R2.id.txGenCommunity) TextInputLayout txGenCommunity;
    @BindView(R2.id.acGenCommunity) AutoCompleteTextView acGenCommunity;
    @BindView(R2.id.txGenPresent) TextInputLayout txGenPresent;
    @BindView(R2.id.acGenPresent) AutoCompleteTextView acGenPresent;
    @BindView(R2.id.txGenHighest) TextInputLayout txGenHighest;
    @BindView(R2.id.acGenHighest) AutoCompleteTextView acGenHighest;
    @BindView(R2.id.txGenRoad) TextInputLayout txGenRoad;
    @BindView(R2.id.acGenRoad) AutoCompleteTextView acGenRoad;
    @BindView(R2.id.txGenSurfacing) TextInputLayout txGenSurfacing;
    @BindView(R2.id.acGenSurfacing) AutoCompleteTextView acGenSurfacing;
    @BindView(R2.id.txGenTypeProp) TextInputLayout txGenTypeProp;
    @BindView(R2.id.acGenTypeProp) AutoCompleteTextView acGenTypeProp;
    @BindView(R2.id.txGenPublicTranspo) TextInputLayout txGenPublicTranspo;
    @BindView(R2.id.acGenPublicTranspo) AutoCompleteTextView acGenPublicTranspo;

    /*TODO MAJOR COMMERCIAL CENTER*/
    @BindView(R2.id.txCommercialLoc1) TextInputLayout txCommercialLoc1;
    @BindView(R2.id.acCommercialLoc1) AutoCompleteTextView acCommercialLoc1;
    @BindView(R2.id.txCommercialLoc2) TextInputLayout txCommercialLoc2;
    @BindView(R2.id.acCommercialLoc2) AutoCompleteTextView acCommercialLoc2;
    @BindView(R2.id.txCommercialLoc3) TextInputLayout txCommercialLoc3;
    @BindView(R2.id.acCommercialLoc3) AutoCompleteTextView acCommercialLoc3;
    @BindView(R2.id.txCommercialLoc4) TextInputLayout txCommercialLoc4;
    @BindView(R2.id.acCommercialLoc4) AutoCompleteTextView acCommercialLoc4;
    @BindView(R2.id.txCommercialLoc5) TextInputLayout txCommercialLoc5;
    @BindView(R2.id.acCommercialLoc5) AutoCompleteTextView acCommercialLoc5;

    @BindView(R2.id.txCommercialDistance1) TextInputLayout txCommercialDistance1;
    @BindView(R2.id.acCommercialDistance1) AutoCompleteTextView acCommercialDistance1;
    @BindView(R2.id.txCommercialDistance2) TextInputLayout txCommercialDistance2;
    @BindView(R2.id.acCommercialDistance2) AutoCompleteTextView acCommercialDistance2;
    @BindView(R2.id.txCommercialDistance3) TextInputLayout txCommercialDistance3;
    @BindView(R2.id.acCommercialDistance3) AutoCompleteTextView acCommercialDistance3;
    @BindView(R2.id.txCommercialDistance4) TextInputLayout txCommercialDistance4;
    @BindView(R2.id.acCommercialDistance4) AutoCompleteTextView acCommercialDistance4;
    @BindView(R2.id.txCommercialDistance5) TextInputLayout txCommercialDistance5;
    @BindView(R2.id.acCommercialDistance5) AutoCompleteTextView acCommercialDistance5;

    /*TODO MAJOR ROADS*/
    @BindView(R2.id.txRoadStreet1) TextInputLayout txRoadStreet1;
    @BindView(R2.id.acRoadStreet1) AutoCompleteTextView acRoadStreet1;
    @BindView(R2.id.txRoadStreet2) TextInputLayout txRoadStreet2;
    @BindView(R2.id.acRoadStreet2) AutoCompleteTextView acRoadStreet2;
    @BindView(R2.id.txRoadStreet3) TextInputLayout txRoadStreet3;
    @BindView(R2.id.acRoadStreet3) AutoCompleteTextView acRoadStreet3;
    @BindView(R2.id.txRoadStreet4) TextInputLayout txRoadStreet4;
    @BindView(R2.id.acRoadStreet4) AutoCompleteTextView acRoadStreet4;
    @BindView(R2.id.txRoadStreet5) TextInputLayout txRoadStreet5;
    @BindView(R2.id.acRoadStreet5) AutoCompleteTextView acRoadStreet5;

    @BindView(R2.id.txRoadDistance1) TextInputLayout txRoadDistance1;
    @BindView(R2.id.acRoadDistance1) AutoCompleteTextView acRoadDistance1;
    @BindView(R2.id.txRoadDistance2) TextInputLayout txRoadDistance2;
    @BindView(R2.id.acRoadDistance2) AutoCompleteTextView acRoadDistance2;
    @BindView(R2.id.txRoadDistance3) TextInputLayout txRoadDistance3;
    @BindView(R2.id.acRoadDistance3) AutoCompleteTextView acRoadDistance3;
    @BindView(R2.id.txRoadDistance4) TextInputLayout txRoadDistance4;
    @BindView(R2.id.acRoadDistance4) AutoCompleteTextView acRoadDistance4;
    @BindView(R2.id.txRoadDistance5) TextInputLayout txRoadDistance5;
    @BindView(R2.id.acRoadDistance5) AutoCompleteTextView acRoadDistance5;

    /*TODO COMMUNITY FACILITIES/UTILITIES*/
    @BindView(R2.id.txCommElectric) TextInputLayout txCommElectric;
    @BindView(R2.id.acCommElectric) AutoCompleteTextView acCommElectric;
    @BindView(R2.id.txCommWater) TextInputLayout txCommWater;
    @BindView(R2.id.acCommWater) AutoCompleteTextView acCommWater;
    @BindView(R2.id.txCommTelecom) TextInputLayout txCommTelecom;
    @BindView(R2.id.acCommTelecom) AutoCompleteTextView acCommTelecom;
    @BindView(R2.id.txCommRoad) TextInputLayout txCommRoad;
    @BindView(R2.id.acCommRoad) AutoCompleteTextView acCommRoad;
    @BindView(R2.id.txCommDrainage) TextInputLayout txCommDrainage;
    @BindView(R2.id.acCommDrainage) AutoCompleteTextView acCommDrainage;
    @BindView(R2.id.txCommSidewalk) TextInputLayout txCommSidewalk;
    @BindView(R2.id.acCommSidewalk) AutoCompleteTextView acCommSidewalk;

    /*TODO NEIGHBORHOOD DEVELOPMENT*/
    @BindView(R2.id.txNeighDevSchool) TextInputLayout txNeighDevSchool;
    @BindView(R2.id.acNeighDevSchool) AutoCompleteTextView acNeighDevSchool;
    @BindView(R2.id.txNeighDevGovFac) TextInputLayout txNeighDevGovFac;
    @BindView(R2.id.acNeighDevFac) AutoCompleteTextView acNeighDevFac;
    @BindView(R2.id.txNeighDevBank) TextInputLayout txNeighDevBank;
    @BindView(R2.id.acNeighDevBank) AutoCompleteTextView acNeighDevBank;
    @BindView(R2.id.txNeighDevWorship) TextInputLayout txNeighDevWorship;
    @BindView(R2.id.acNeighDevWorship) AutoCompleteTextView acNeighDevWorship;
    @BindView(R2.id.txNeighDevMall) TextInputLayout txNeighDevMall;
    @BindView(R2.id.acNeighDevMall) AutoCompleteTextView acNeighDevMall;
    @BindView(R2.id.txNeighDevCondo) TextInputLayout txNeighDevCondo;
    @BindView(R2.id.acNeighDevCondo) AutoCompleteTextView acNeighDevCondo;
    @BindView(R2.id.txNeighDevHotel) TextInputLayout txNeighDevHotel;
    @BindView(R2.id.acNeighDevHotel) AutoCompleteTextView acNeighDevHotel;
    @BindView(R2.id.txNeighDevCommercial) TextInputLayout txNeighDevCommercial;
    @BindView(R2.id.acNeighDevCommercial) AutoCompleteTextView acNeighDevCommercial;
    @BindView(R2.id.txNeighDevMed) TextInputLayout txNeighDevMed;
    @BindView(R2.id.acNeighDevMed) AutoCompleteTextView acNeighDevMed;
    @BindView(R2.id.txNeighDevAuto) TextInputLayout txNeighDevAuto;
    @BindView(R2.id.acNeighDevAuto) AutoCompleteTextView acNeighDevAuto;
    @BindView(R2.id.txNeighDevIndus) TextInputLayout txNeighDevIndus;
    @BindView(R2.id.acNeighDevIndus) AutoCompleteTextView acNeighDevIndus;

    private String recordId;
    private String appraisalType;
    private UpdatePropertyDescContract.Presenter presenter;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.form_property_description);
        ButterKnife.bind(this);

        Bundle putExtraBundle = getIntent().getExtras();
        if(putExtraBundle == null) {
            recordId= null;
            return;
        } else {
            recordId= putExtraBundle.getString(RECORD_ID);
            appraisalType= putExtraBundle.getString(APPRAISAL_TYPE);
        }

        Objects.requireNonNull(getSupportActionBar()).setTitle("Property Description");
        getSupportActionBar().setSubtitle(appraisalType);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);

        presenter = new UpdatePropertyDescPresenterImpl(this, getApplication());

        presenter.onStart(recordId);

    }

    @Override
    public boolean onOptionsItemSelected(@NonNull MenuItem item) {
        if (item.getItemId() == android.R.id.home) {
            finish();
        }
        return super.onOptionsItemSelected(item);
    }

    @OnClick(R2.id.ownershipPropDescLayout)
    void ownershipPropDescOnClick() {
        Bundle bundle = new Bundle();
        bundle.putString(RECORD_ID, recordId);
        bundle.putString(APPRAISAL_TYPE, appraisalType);

        if(appraisalType.equals(VAR_CONDO)){
            CondoUnitDescFragment dialogFragment = new CondoUnitDescFragment();
            dialogFragment.setArguments(bundle);
            FragmentTransaction fragmentTransaction = getSupportFragmentManager().beginTransaction();
            dialogFragment.show(fragmentTransaction, CondoUnitDescFragment.TAG);
        } else if (appraisalType.equals(VAR_LAND_IMP)) {
            ImprovementDescFragment improvementDescFragment = new ImprovementDescFragment();
            improvementDescFragment.setArguments(bundle);
            FragmentTransaction fragmentTransaction = getSupportFragmentManager().beginTransaction();
            improvementDescFragment.show(fragmentTransaction, ImprovementDescFragment.TAG);
        }
        else {
            Intent intent = new Intent(this, OwnershipPropertyDescActivity.class);
            intent.putExtra(RECORD_ID, recordId);

            startActivity(intent);
        }
    }

    @OnClick(R2.id.llImprovementAndBuildingDesc)
    void onClickImprovementAndBuildingDesc() {
        Bundle bundle = new Bundle();
        bundle.putString(RECORD_ID, recordId);
        bundle.putString(APPRAISAL_TYPE, appraisalType);

        if(appraisalType.equals(VAR_CONDO)){
            CondoBuildingDescFragment dialogFragment = new CondoBuildingDescFragment();
            dialogFragment.setArguments(bundle);
            FragmentTransaction fragmentTransaction = getSupportFragmentManager().beginTransaction();
            dialogFragment.show(fragmentTransaction, CondoBuildingDescFragment.TAG);
        }else {
            ImprovementDescFragment dialogFragment = new ImprovementDescFragment();
            dialogFragment.setArguments(bundle);
            FragmentTransaction fragmentTransaction = getSupportFragmentManager().beginTransaction();
            dialogFragment.show(fragmentTransaction, ImprovementDescFragment.TAG);
        }
    }

    @OnClick(R2.id.llImprovementOtherDesc)
    void onClickImprovementOtherDesc() {
        Bundle bundle = new Bundle();
        bundle.putString(RECORD_ID, recordId);
        bundle.putString(APPRAISAL_TYPE, appraisalType);

        ImprovementOtherDescFragment dialogFragment = new ImprovementOtherDescFragment();
        dialogFragment.setArguments(bundle);
        FragmentTransaction fragmentTransaction = getSupportFragmentManager().beginTransaction();
        dialogFragment.show(fragmentTransaction, ImprovementDescFragment.TAG);

    }

    @Override
    public void showProgress(String title, String message) {

    }

    @Override
    public void hideProgress() {

    }

    @Override
    public void showSnackMessage(String message) {



    }

    @Override
    protected void onPause() {
        super.onPause();
        new Handler().post(() -> presenter.updatePropertyDesc(
        recordId,
        acDeedRestrict.getText().toString(),
        acDescEncumb.getText().toString(),

        /*PHYSICAL FEATURES*/
        acPhysTypeLot.getText().toString(),
        acPhysShape.getText().toString(),
        acTopography.getText().toString(),
        acPhysElevation.getText().toString(),
        acPhysFrontage.getText().toString(),
        acPhysDepth.getText().toString(),

        /*BASIS OF IDENTIFICATION*/
        CHECKBOX_TO_STRING(cbBasisPlot),
        CHECKBOX_TO_STRING(cbBasisAprrove),
        CHECKBOX_TO_STRING(cbBasisSurvey),
        CHECKBOX_TO_STRING(cbBasisTax),
        CHECKBOX_TO_STRING(cbBasisSubdiv),
         acBasisOthers.getText().toString(),
        /*BOUNDARIES*/
        acBoundFront.getText().toString(),
        acBoundRear.getText().toString(),
        acBoundLeft.getText().toString(),
        acBoundRight.getText().toString(),

        /*ZONING ORDINANCE*/
        acZoning.getText().toString(),

        /*GENERAL*/
        acGenClass.getText().toString(),
        acGenMunicipality.getText().toString(),
        acGenCommunity.getText().toString(),
        acGenPresent.getText().toString(),
        acGenHighest.getText().toString(),
        acGenRoad.getText().toString(),
        acGenSurfacing.getText().toString(),
        acGenTypeProp.getText().toString(),
        acGenPublicTranspo.getText().toString(),

        /*MAJOR COMMERCIAL CENTER*/
        acCommercialLoc1.getText().toString(),
        acCommercialDistance1.getText().toString(),
        acCommercialLoc2.getText().toString(),
        acCommercialDistance2.getText().toString(),
        acCommercialLoc3.getText().toString(),
        acCommercialDistance3.getText().toString(),
        acCommercialLoc4.getText().toString(),
        acCommercialDistance4.getText().toString(),
        acCommercialLoc5.getText().toString(),
        acCommercialDistance5.getText().toString(),

        /*MAJOR ROADS*/
        acRoadStreet1.getText().toString(),
        acRoadDistance1.getText().toString(),
        acRoadStreet2.getText().toString(),
        acRoadDistance2.getText().toString(),
        acRoadStreet3.getText().toString(),
        acRoadDistance3.getText().toString(),
        acRoadStreet4.getText().toString(),
        acRoadDistance4.getText().toString(),
        acRoadStreet5.getText().toString(),
        acRoadDistance5.getText().toString(),

        /*COMMUNITY FACILITIES/UTILITIES*/
        acCommElectric.getText().toString(),
        acCommWater.getText().toString(),
        acCommTelecom.getText().toString(),
        acCommRoad.getText().toString(),
        acCommDrainage.getText().toString(),
        acCommSidewalk.getText().toString(),

        /*NEIGHBORHOOD DEVELOPMENT*/
        acNeighDevSchool.getText().toString(),
        acNeighDevFac.getText().toString(),
        acNeighDevBank.getText().toString(),
        acNeighDevWorship.getText().toString(),
        acNeighDevMall.getText().toString(),
        acNeighDevCondo.getText().toString(),
        acNeighDevHotel.getText().toString(),
        acNeighDevCommercial.getText().toString(),
        acNeighDevMed.getText().toString(),
        acNeighDevAuto.getText().toString(),
        acNeighDevIndus.getText().toString()
        ));


    }

    @Override
    public void showDetails(Record record) {

        if(appraisalType.equals(VAR_CONDO)){
            txtOwnershipCondoDesc.setText(getString(R.string.lbl_condoUnitDesc));
            txtImprovementAndBuildingDesc.setText(getString(R.string.lbl_condoBuildingDesc));
            llimprovementOtherDesc.setVisibility(View.GONE);
        }
        else if(appraisalType.equals(VAR_LAND_ONLY)){
            llImprovementAndBuildingDesc.setVisibility(View.GONE);
            llimprovementOtherDesc.setVisibility(View.GONE);
        }
        /*else if(appraisalType.equals(VAR_LAND_IMP)){
            //no condition yet
        }*/

        acDeedRestrict.setText(record.getValrepLandimpIdStatutoryConstraints());
        acDescEncumb.setText(record.getValrepLandimpIdStatutoryConstraintsDesc());

        /*TODO PHYSICAL FEATURES*/
        acPhysTypeLot.setText(record.getValrepLandimpPhysicalLotType());
        acPhysShape.setText(record.getValrepLandimpPhysicalShape());
        acTopography.setText(record.getValrepLandimpPhysicalTerrain());
        acPhysElevation.setText(record.getValrepLandimpPhysicalElevation());
        acPhysFrontage.setText(record.getValrepLandimpPhysicalFrontage());
        acPhysDepth.setText(record.getValrepLandimpPhysicalDepth());

        /*TODO BASIS OF IDENTIFICATION*/
        cbBasisPlot.setChecked(getCheckboxBoolean(record.getValrepLandimpIdPlotting()));
        cbBasisAprrove.setChecked(getCheckboxBoolean(record.getValrepLandimpIdLotConfig()));
        cbBasisSurvey.setChecked(getCheckboxBoolean(record.getValrepLandimpIdGeodeticPlan()));
        cbBasisTax.setChecked(getCheckboxBoolean(record.getValrepLandimpIdTax()));
        cbBasisSubdiv.setChecked(getCheckboxBoolean(record.getValrepLandimpIdSubdMap()));

        /*TODO BOUNDARIES*/
        acBoundFront.setText(record.getValrepLandimpBound1Orientation());
        acBoundRear.setText(record.getValrepLandimpBound2Orientation());
        acBoundLeft.setText(record.getValrepLandimpBound3Orientation());
        acBoundRight.setText(record.getValrepLandimpBound4Orientation());

        /*TODO ZONING ORDINANCE*/
        acZoning.setText(record.getValrepLandimpLotclassPerTaxDec());

        /*TODO GENERAL*/
        acGenClass.setText(record.getValrepLandimpDescTownClass());
        acGenMunicipality.setText(record.getValrepLandimpTaxDecTownType());
        acGenCommunity.setText(record.getValrepLandimpLotclassNeighborhood());
        acGenPresent.setText(record.getValrepLandimpLotclassActualUsage());
        acGenHighest.setText(record.getValrepLandimpLotclassHighestBestUse());
        acGenRoad.setText(record.getValrepLandimpPhysicalRoad());
        acGenSurfacing.setText(record.getValrepLandimpPhysicalSurfacing());
        acGenTypeProp.setText(record.getValrepLandimpLotclassPropType());

        acGenPublicTranspo.setText(record.getValrepLandimpPhysicalPubTransDesc());
        acGenPublicTranspo.setAdapter(GET_CONTENT_ADAPTER(record.getValrepLandimpPhysicalPubTransDesc(), R.array.publicTransportArray, this));
        acGenPublicTranspo.setOnClickListener(view -> acGenPublicTranspo.showDropDown());

        /*TODO MAJOR COMMERCIAL CENTER*/
        acCommercialLoc1.setText(record.getValrepLandimpMccLocation1());
        acCommercialDistance1.setText(record.getValrepLandimpMccDistance1());
        acCommercialLoc2.setText(record.getValrepLandimpMccLocation2());
        acCommercialDistance2.setText(record.getValrepLandimpMccDistance2());
        acCommercialLoc3.setText(record.getValrepLandimpMccLocation3());
        acCommercialDistance3.setText(record.getValrepLandimpMccDistance3());
        acCommercialLoc4.setText(record.getValrepLandimpMccLocation4());
        acCommercialDistance4.setText(record.getValrepLandimpMccDistance4());
        acCommercialLoc5.setText(record.getValrepLandimpMccLocation5());
        acCommercialDistance5.setText(record.getValrepLandimpMccDistance5());

        /*TODO MAJOR ROADS*/
        acRoadStreet1.setText(record.getValrepLandimpMrLocation1());
        acRoadDistance1.setText(record.getValrepLandimpMrDistance1());
        acRoadStreet2.setText(record.getValrepLandimpMrLocation2());
        acRoadDistance2.setText(record.getValrepLandimpMrDistance2());
        acRoadStreet3.setText(record.getValrepLandimpMrLocation3());
        acRoadDistance3.setText(record.getValrepLandimpMrDistance3());
        acRoadStreet4.setText(record.getValrepLandimpMrLocation4());
        acRoadDistance4.setText(record.getValrepLandimpMrDistance4());
        acRoadStreet5.setText(record.getValrepLandimpMrLocation5());
        acRoadDistance5.setText(record.getValrepLandimpMrDistance5());

        /*TODO COMMUNITY FACILITIES/UTILITIES*/
        acCommElectric.setText(record.getValrepLandimpUtilElectricity());
        acCommWater.setText(record.getValrepLandimpUtilWater());
        acCommTelecom.setText(record.getValrepLandimpUtilTelephone());
        acCommRoad.setText(record.getValrepLandimpImpRoads());
        acCommDrainage.setText(record.getValrepLandimpImpDrainage());
        acCommSidewalk.setText(record.getValrepLandimpImpSidewalk());

        /*TODO NEIGHBORHOOD DEVELOPMENT*/
        acNeighDevSchool.setText(record.getAssetNeighborhoodSchools());
        acNeighDevFac.setText(record.getAssetNeighborhoodGovernmentFacilities());
        acNeighDevBank.setText(record.getAssetNeighborhoodBanks());
        acNeighDevWorship.setText(record.getAssetNeighborhoodWorship());
        acNeighDevMall.setText(record.getAssetNeighborhoodShoppingMalls());
        acNeighDevCondo.setText(record.getAssetNeighborhoodSubdivisions());
        acNeighDevHotel.setText(record.getAssetNeighborhoodHotels());
        acNeighDevCommercial.setText(record.getAssetNeighborhoodEstablishments());
        acNeighDevMed.setText(record.getAssetNeighborhoodHospitals());
        acNeighDevAuto.setText(record.getAssetNeighborhoodAutomotiveCenters());
        acNeighDevIndus.setText(record.getAssetNeighborhoodIndustrialIndicators());


        //TODO setup dropdown and hint
        SET_CONTENT_DROPDOWN(this, acDeedRestrict, R.array.deedRestrictArray, false);//not disabled
        SET_CONTENT_DROPDOWN(this, acPhysTypeLot, R.array.typeLotArray, true);
        SET_CONTENT_DROPDOWN(this, acPhysShape, R.array.shapeArray, true);
        SET_CONTENT_DROPDOWN(this, acTopography, R.array.topographyArray, true);
        SET_CONTENT_DROPDOWN(this, acPhysElevation, R.array.elevationArray, true);

        SET_CONTENT_DROPDOWN(this, acGenClass, R.array.cityClassArray, true);
        SET_CONTENT_DROPDOWN(this, acGenMunicipality, R.array.townArray, true);
        SET_CONTENT_DROPDOWN(this, acGenCommunity, R.array.communityArray, true);
        SET_CONTENT_DROPDOWN(this, acGenPresent, R.array.presentArray, true);
        SET_CONTENT_DROPDOWN(this, acGenHighest, R.array.highestArray, true);
        SET_CONTENT_DROPDOWN(this, acGenSurfacing, R.array.surfacingArray, false);//not disabled
        SET_CONTENT_DROPDOWN(this, acGenTypeProp, R.array.propertyArray, true);

    }

    @Override
    public void showExitDialog(String title, String message) {

    }

    @Override
    public void showErrorDialog(String title, String message) {

    }
}
