package com.gdslinkasia.propertydescription.land_improvement;

import android.app.ProgressDialog;
import android.content.Intent;
import android.os.Bundle;
import android.view.MenuItem;
import android.widget.AutoCompleteTextView;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AlertDialog;
import androidx.appcompat.app.AppCompatActivity;

import com.gdslinkasia.base.model.details.ValrepLandimpOtherLandImpDetail;
import com.gdslinkasia.commonresources.R2;
import com.gdslinkasia.propertydescription.R;
import com.gdslinkasia.propertydescription.property_description_contract.land_other_imp_item.LandOtherImpItemContract;
import com.gdslinkasia.propertydescription.property_description_contract.land_other_imp_item.LandOtherImpItemPresenterImpl;

import java.util.Objects;

import butterknife.BindView;
import butterknife.ButterKnife;

import static com.gdslinkasia.base.database.DatabaseUtils.*;
import static com.gdslinkasia.base.utils.GlobalFunctions.SET_CONTENT_DROPDOWN;
import static com.gdslinkasia.base.utils.computation.OtherDescImprovementComputation.AC_ECONOMIC_LIFE;
import static com.gdslinkasia.base.utils.computation.OtherDescImprovementComputation.AC_EFFECTIVE_AGE;

public class ImprovementOtherDescItemActivity extends AppCompatActivity implements LandOtherImpItemContract.View {

    public final String TAG = this.getClass().getSimpleName();

    private long uniqueId;

//    @BindView(R2.id.txtDialogTitle) TextView txtDialogTitle;

    @BindView(R2.id.acTypeOtherImp) AutoCompleteTextView acTypeOtherImp;
    @BindView(R2.id.acDescription) AutoCompleteTextView acDescription;
    @BindView(R2.id.acArea) AutoCompleteTextView acArea;
    @BindView(R2.id.acUnit) AutoCompleteTextView acUnit;
    @BindView(R2.id.acOthers) AutoCompleteTextView acOthers;
    @BindView(R2.id.acEcoLife) AutoCompleteTextView acEcoLife;
    @BindView(R2.id.acEffectiveAge) AutoCompleteTextView acEffectiveAge;
    @BindView(R2.id.acRemLife) AutoCompleteTextView acRemLife;
    @BindView(R2.id.acRemarks) AutoCompleteTextView acRemarks;

    private ProgressDialog progressDialog;

    private LandOtherImpItemContract.Presenter presenter;
    private String appraisalType;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.content_imp_other_desc);
        ButterKnife.bind(this);

        Intent intent = getIntent();
        uniqueId = intent.getLongExtra(UNIQUE_ID, 0);
        appraisalType = intent.getStringExtra(APPRAISAL_TYPE);

        Objects.requireNonNull(getSupportActionBar()).setTitle("Property Details");
        getSupportActionBar().setSubtitle("Description of Other Improvements "+uniqueId);
        getSupportActionBar().setIcon(R.drawable.ic_close_white_24dp);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);

        progressDialog = new ProgressDialog(this);
        presenter = new LandOtherImpItemPresenterImpl(this, getApplication());
    }

    @Override
    public boolean onOptionsItemSelected(@NonNull MenuItem item) {
        if (item.getItemId() == android.R.id.home) {
            finish();
        }
        return super.onOptionsItemSelected(item);
    }

    @Override
    protected void onResume() {
        super.onResume();
        presenter.onStart(uniqueId);
    }



    @Override
    protected void onPause() {
        super.onPause();
        presenter.updateItemDetails(
                uniqueId,
                Objects.requireNonNull(acTypeOtherImp.getText()).toString(),
                Objects.requireNonNull(acDescription.getText()).toString(),
                Objects.requireNonNull(acArea.getText()).toString(),
                Objects.requireNonNull(acUnit.getText()).toString(),
                Objects.requireNonNull(acOthers.getText()).toString(),
                Objects.requireNonNull(acEcoLife.getText()).toString(),
                Objects.requireNonNull(acEffectiveAge.getText()).toString(),
                Objects.requireNonNull(acRemLife.getText()).toString(),
                Objects.requireNonNull(acRemarks.getText()).toString()
        );
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        presenter.onDestroy();
        progressDialog.dismiss();
    }

    @Override
    public void showProgress(String title, String message) {
        progressDialog.setCancelable(false);
        progressDialog.setIndeterminate(false);
        progressDialog.setTitle(title);
        progressDialog.setMessage(message);
        progressDialog.show();
    }

    @Override
    public void hideProgress() {
        progressDialog.hide();
    }

    @Override
    public void showDetails(ValrepLandimpOtherLandImpDetail detail) {

        acTypeOtherImp.setText(detail.getValrepLandimpOtherLandImpDetailsType());
        acDescription.setText(detail.getValrepLandimpOtherLandImpDetailsDesc());
        acArea.setText(detail.getValrepLandimpOtherLandImpDetailsArea());
        acUnit.setText(detail.getValrepLandimpOtherLandImpDetailsAreaUnit());
        SET_CONTENT_DROPDOWN(this, acUnit, R.array.unitArray, true);

        acOthers.setText(detail.getValrepLandimpOtherLandImpDetailsCondition());
        SET_CONTENT_DROPDOWN(this, acOthers, R.array.presentConditionArray, true);

        acEcoLife.setText(detail.getValrepLandimpOtherLandImpDetailsEconomicLife());
        acEffectiveAge.setText(detail.getValrepLandimpOtherLandImpDetailsEffectiveAge());
        acRemLife.setText(detail.getValrepLandimpOtherLandImpDetailsRemainLife());
        acRemarks.setText(detail.getValrepLandimpOtherLandImpDetailsRemarks());

        acEcoLife.addTextChangedListener(AC_ECONOMIC_LIFE(acEcoLife, acEffectiveAge, acRemLife));
        acEffectiveAge.addTextChangedListener(AC_EFFECTIVE_AGE(acEcoLife, acEffectiveAge, acRemLife));
    }

    @Override
    public void showExitDialog(String title, String message) {
        new AlertDialog.Builder(this)
                .setCancelable(false)
                .setTitle(title)
                .setMessage(message)
                .setPositiveButton("CLOSE", (dialog, which) -> finish())
                .create()
                .show();
    }
}
