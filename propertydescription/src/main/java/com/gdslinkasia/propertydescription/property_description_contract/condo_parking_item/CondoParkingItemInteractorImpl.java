package com.gdslinkasia.propertydescription.property_description_contract.condo_parking_item;

import android.app.Application;

import com.gdslinkasia.base.database.repository.CondoParkingValuationLocalRepository;
import com.gdslinkasia.base.model.details.ValrepLandimpParkingValuationDetail;

import io.reactivex.Single;

public class CondoParkingItemInteractorImpl implements CondoParkingItemContract.Interactor {

    private CondoParkingValuationLocalRepository localRepository;

    public CondoParkingItemInteractorImpl(Application application) {
        this.localRepository = new CondoParkingValuationLocalRepository(application);
    }

    @Override
    public Single<ValrepLandimpParkingValuationDetail> getDetails(long uniqueId) {
        return localRepository.getDetails(uniqueId);
    }

    @Override
    public void updateLotDetail(ValrepLandimpParkingValuationDetail detail) {
        localRepository.updateLotDetail(detail);
    }
}
