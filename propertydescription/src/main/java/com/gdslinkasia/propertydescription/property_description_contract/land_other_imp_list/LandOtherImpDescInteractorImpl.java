package com.gdslinkasia.propertydescription.property_description_contract.land_other_imp_list;

import android.app.Application;


import com.gdslinkasia.base.database.repository.LandImpOtherDetailLocalRepository;
import com.gdslinkasia.base.database.repository.JobDetailsLocalRepository;
import com.gdslinkasia.base.model.details.ValrepLandimpOtherLandImpDetail;
import com.gdslinkasia.base.model.details.ValrepLandimpOtherLandImpValue;
import com.gdslinkasia.base.model.job.Record;

import java.util.List;

import io.reactivex.Flowable;
import io.reactivex.Single;

public class LandOtherImpDescInteractorImpl implements LandOtherImpDescContract.Interactor {

    private LandImpOtherDetailLocalRepository localRepository;
    private JobDetailsLocalRepository jobDetailsLocalRepository;

    public LandOtherImpDescInteractorImpl(Application application) {
        //same table for condo Unit Desc
        localRepository = new LandImpOtherDetailLocalRepository(application);
        jobDetailsLocalRepository = new JobDetailsLocalRepository(application);
    }

    @Override
    public Single<List<ValrepLandimpOtherLandImpDetail>> getLandImpImpDetailsList(String recordId) {
        return localRepository.getLandImpImpDetailsList(recordId);
    }

    @Override
    public Flowable<List<ValrepLandimpOtherLandImpDetail>> getListFlowable(String recordId) {
        return localRepository.getListFlowable(recordId);
    }

    @Override
    public Single<Record> getRecordById(String recordId) {
        return jobDetailsLocalRepository.getRecordById(recordId);
    }

    @Override
    public long insertOtherLandimpImpDetail(ValrepLandimpOtherLandImpDetail detail) {
        return localRepository.insertOtherLandimpImpDetail(detail);
    }

    @Override
    public void insertOtherLandimpImpValue(ValrepLandimpOtherLandImpValue value) {
        localRepository.insertOtherLandimpValue(value);
    }
}
