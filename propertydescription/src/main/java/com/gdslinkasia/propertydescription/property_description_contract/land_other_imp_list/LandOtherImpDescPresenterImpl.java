package com.gdslinkasia.propertydescription.property_description_contract.land_other_imp_list;

import android.app.Application;
import android.util.Log;

import com.gdslinkasia.base.model.details.ValrepLandimpOtherLandImpDetail;
import com.gdslinkasia.base.model.details.ValrepLandimpOtherLandImpValue;
import com.gdslinkasia.base.model.job.Record;

import java.util.List;

import io.reactivex.Flowable;
import io.reactivex.Single;
import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.disposables.CompositeDisposable;
import io.reactivex.functions.Consumer;
import io.reactivex.observers.DisposableSingleObserver;
import io.reactivex.schedulers.Schedulers;

public class LandOtherImpDescPresenterImpl implements LandOtherImpDescContract.Presenter {

    private LandOtherImpDescContract.View view;
    private LandOtherImpDescContract.Interactor interactor;
    private CompositeDisposable compositeDisposable = new CompositeDisposable();

    public LandOtherImpDescPresenterImpl(LandOtherImpDescContract.View view, Application application) {
        this.view = view;
        this.interactor = new LandOtherImpDescInteractorImpl(application);
    }

    @Override
    public void onStart(String recordId) {
//        view.showProgress();
        compositeDisposable.add(getSingleListObservable(recordId).subscribeWith(getSingleListObserver(recordId)));
    }

    @Override
    public void onDestroy() {
        view = null;
        compositeDisposable.clear();
    }

    @Override
    public void insertOtherLandimpImpDetail(String recordId) {
        compositeDisposable.add(getRecordObservable(recordId).subscribeWith(getRecordObserver()));
    }

    //TODO ADD ValrepLandimpOtherLandImpDetail + ValrepLandimpOtherLandImpValue
    private Single<Record> getRecordObservable(String recordId) {
        return interactor.getRecordById(recordId)
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread());
    }

    private DisposableSingleObserver<Record> getRecordObserver() {
        return new DisposableSingleObserver<Record>() {
            @Override
            public void onSuccess(Record record) {
                ValrepLandimpOtherLandImpDetail detail = new ValrepLandimpOtherLandImpDetail();
                detail.setRecordId(record.getRecordId());
                detail.setRecordUId(record.getUniqueId());

                long detailUId = interactor.insertOtherLandimpImpDetail(detail);
                Log.d("int--detailUId", String.valueOf(detailUId));

                ValrepLandimpOtherLandImpValue valuation = new ValrepLandimpOtherLandImpValue();
                valuation.setRecordId(record.getRecordId());
                valuation.setRecordUId(record.getUniqueId());
                valuation.setOtherDescImpId(detailUId);

                interactor.insertOtherLandimpImpValue(valuation);
            }

            @Override
            public void onError(Throwable e) {
                e.printStackTrace();
                view.showErrorDialog("Something Went Wrong", e.getMessage());
            }
        };
    }

    private Single<List<ValrepLandimpOtherLandImpDetail>> getSingleListObservable(String recordId) {
        return interactor.getLandImpImpDetailsList(recordId)
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread());
    }

    private DisposableSingleObserver<List<ValrepLandimpOtherLandImpDetail>> getSingleListObserver(String recordId) {
        return new DisposableSingleObserver<List<ValrepLandimpOtherLandImpDetail>>() {
            @Override
            public void onSuccess(List<ValrepLandimpOtherLandImpDetail> valrepLandimpLotDetails) {
                if (!valrepLandimpLotDetails.isEmpty()) {
//                    view.showList(valrepLandimpLotDetails);
                    compositeDisposable.add(getListFlowable(recordId).subscribe(getFlowableObserver()));
                } else {
                    view.showEmptyState("There are no records available.");
                }
//                new Handler().postDelayed(() -> view.hideProgress(), 1000);
            }

            @Override
            public void onError(Throwable e) {
                view.showEmptyState(e.getMessage());
//                new Handler().postDelayed(() -> view.hideProgress(), 1000);
            }
        };
    }

    private Flowable<List<ValrepLandimpOtherLandImpDetail>> getListFlowable(String recordId) {
        return interactor.getListFlowable(recordId)
                .subscribeOn(Schedulers.computation())
                .observeOn(AndroidSchedulers.mainThread());
    }

    private Consumer<List<ValrepLandimpOtherLandImpDetail>> getFlowableObserver() {
        return valrepLandimpLotDetails -> {
            view.showList(valrepLandimpLotDetails);
            Log.d("int--flowableSize", String.valueOf(valrepLandimpLotDetails.size()));
        };
    }

}
