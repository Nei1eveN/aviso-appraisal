package com.gdslinkasia.propertydescription.property_description_contract.desc_of_improvement_list;

import com.gdslinkasia.base.model.details.ValrepLandimpImpDetail;
import com.gdslinkasia.base.model.details.ValrepLandimpImpValuation;
import com.gdslinkasia.base.model.job.Record;

import java.util.List;

import io.reactivex.Flowable;
import io.reactivex.Single;

public interface LandImpDescImprovementContract {

    interface View {
        void showProgress(String title, String message);

        void hideProgress();

        void showList(List<ValrepLandimpImpDetail> valrepLandimpLotDetails);

        void showEmptyState(String message);

        void showErrorDialog(String title, String message);
    }

    interface Presenter {
        void onStart(String recordId);

        void onDestroy();

        void insertLandimpImpDetail(String recordId, int recyclerViewVisibility, int tvNoRecordVisibility);
    }

    interface Interactor {
        Single<List<ValrepLandimpImpDetail>> getCondoBuildingLandImpDescDescList(String recordId);

        Flowable<List<ValrepLandimpImpDetail>> getListFlowable(String recordId);

        long insertLandimpImpDetail(ValrepLandimpImpDetail detail);

        void insertLandimpImpValuation(ValrepLandimpImpValuation valuation);

        Single<Record> getRecordById(String recordId);
    }

}
