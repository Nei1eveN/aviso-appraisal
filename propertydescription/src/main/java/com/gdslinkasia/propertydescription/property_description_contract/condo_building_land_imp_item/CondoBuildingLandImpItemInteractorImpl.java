package com.gdslinkasia.propertydescription.property_description_contract.condo_building_land_imp_item;

import android.app.Application;

import com.gdslinkasia.base.database.repository.LandImpImpDetailLocalRepository;
import com.gdslinkasia.base.model.details.ValrepLandimpImpDetail;

import io.reactivex.Single;

public class CondoBuildingLandImpItemInteractorImpl implements CondoBuildingLandImpItemContract.Interactor {

    private LandImpImpDetailLocalRepository localRepository;

    public CondoBuildingLandImpItemInteractorImpl(Application application) {
        this.localRepository = new LandImpImpDetailLocalRepository(application);
    }

    @Override
    public Single<ValrepLandimpImpDetail> getCondoBuildingLandImpItemDetails(long uniqueId) {
        return localRepository.getPropertyDescDetail(uniqueId);
    }

    @Override
    public void updateLotDetail(ValrepLandimpImpDetail detail) {
        localRepository.updateLotDetail(detail);
    }
}
