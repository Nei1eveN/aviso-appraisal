package com.gdslinkasia.propertydescription.property_description_contract.land_other_imp_list;

import com.gdslinkasia.base.model.details.ValrepLandimpOtherLandImpDetail;
import com.gdslinkasia.base.model.details.ValrepLandimpOtherLandImpValue;
import com.gdslinkasia.base.model.job.Record;

import java.util.List;

import io.reactivex.Flowable;
import io.reactivex.Single;

public interface LandOtherImpDescContract {

    interface View {
        void showProgress(String title, String message);

        void hideProgress();

        void showList(List<ValrepLandimpOtherLandImpDetail> valrepLandimpOtherLandImpDetail);

        void showEmptyState(String message);

        void showErrorDialog(String title, String message);
    }

    interface Presenter {
        void onStart(String recordId);

        void onDestroy();

        void insertOtherLandimpImpDetail(String recordId);
    }

    interface Interactor {
        Single<List<ValrepLandimpOtherLandImpDetail>> getLandImpImpDetailsList(String recordId);

        Flowable<List<ValrepLandimpOtherLandImpDetail>> getListFlowable(String recordId);

        Single<Record> getRecordById(String recordId);

        long insertOtherLandimpImpDetail(ValrepLandimpOtherLandImpDetail detail);

        void insertOtherLandimpImpValue(ValrepLandimpOtherLandImpValue valuation);
    }

}
