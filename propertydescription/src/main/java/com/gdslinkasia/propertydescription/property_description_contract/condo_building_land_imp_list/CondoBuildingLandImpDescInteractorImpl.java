package com.gdslinkasia.propertydescription.property_description_contract.condo_building_land_imp_list;

import android.app.Application;

import com.gdslinkasia.base.database.repository.LandImpImpDetailLocalRepository;
import com.gdslinkasia.base.model.details.ValrepLandimpImpDetail;

import java.util.List;

import io.reactivex.Flowable;
import io.reactivex.Single;

public class CondoBuildingLandImpDescInteractorImpl implements CondoBuildingLandImpDescContract.Interactor {

    private LandImpImpDetailLocalRepository localRepository;

    public CondoBuildingLandImpDescInteractorImpl(Application application) {
        //same table for condo Unit Desc
        localRepository = new LandImpImpDetailLocalRepository(application);
    }

    @Override
    public Single<List<ValrepLandimpImpDetail>> getCondoBuildingLandImpDescDescList(String recordId) {
        //same table for condo and land imp (improvements and buildings)
        return localRepository.getLandImpImpDetailsList(recordId);
    }

    @Override
    public Flowable<List<ValrepLandimpImpDetail>> getListFlowable(String recordId) {
        return localRepository.getListFlowable(recordId);
    }
}
