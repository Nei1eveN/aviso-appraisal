package com.gdslinkasia.propertydescription.property_description_contract.ownership_property_item;

import android.app.Application;

import com.gdslinkasia.base.database.repository.PropertyDescriptionLocalRepository;
import com.gdslinkasia.base.model.details.ValrepLandimpLotDetail;

import io.reactivex.Single;

public class OwnershipItemInteractorImpl implements OwnershipItemContract.Interactor {

    private PropertyDescriptionLocalRepository localRepository;

    public OwnershipItemInteractorImpl(Application application) {
        this.localRepository = new PropertyDescriptionLocalRepository(application);
    }

    @Override
    public Single<ValrepLandimpLotDetail> getOwnershipItemDetails(long uniqueId) {
        return localRepository.getPropertyDescDetail(uniqueId);
    }

    @Override
    public void updateLotDetail(ValrepLandimpLotDetail detail) {
        localRepository.updateLotDetail(detail);
    }

    @Override
    public void onDestroy() {
        localRepository.onDestroy();
    }

    @Override
    public void deleteItem(long uniqueId) {
        localRepository.deleteLotItem(uniqueId);
    }
}
