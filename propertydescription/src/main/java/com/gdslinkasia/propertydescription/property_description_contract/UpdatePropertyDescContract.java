package com.gdslinkasia.propertydescription.property_description_contract;

import com.gdslinkasia.base.model.job.Record;

import io.reactivex.Single;

public interface UpdatePropertyDescContract {
    interface View {
        void showProgress(String title, String message);
        void hideProgress();
        void showSnackMessage(String message);
        void showDetails(Record record);
        void showExitDialog(String title, String message);
        void showErrorDialog(String title, String message);
    }
    interface Presenter {
        void onStart(String recordId);
        void onDestroy();

        void updatePropertyDesc(
                String recordId,
                String valrep_landimp_id_statutory_constraints,
                String valrep_landimp_id_statutory_constraints_desc,
                /*PHYSICAL FEATURES*/
                String valrep_landimp_physical_lot_type,
                String valrep_landimp_physical_shape,
                String valrep_landimp_physical_terrain,
                String valrep_landimp_physical_elevation,
                String valrep_landimp_physical_frontage,
                String valrep_landimp_physical_depth,
                /*BASIS OF IDENTIFICATION*/
                String valrep_landimp_id_plotting,
                String valrep_landimp_id_lot_config,
                String valrep_landimp_id_geodetic_plan,
                String valrep_landimp_id_tax,
                String valrep_landimp_id_subd_map,
                String valrep_landimp_id_others,
                /*BOUNDARIES*/
                String valrep_landimp_bound1_orientation,
                String valrep_landimp_bound2_orientation,
                String valrep_landimp_bound3_orientation,
                String valrep_landimp_bound4_orientation,
                /*ZONING ORDINANCE*/
                String valrep_landimp_lotclass_per_tax_dec,
                /*GENERAL*/
                String valrep_landimp_desc_town_class,
                String valrep_landimp_tax_dec_town_type,
                String valrep_landimp_lotclass_neighborhood,
                String valrep_landimp_lotclass_actual_usage,
                String valrep_landimp_lotclass_highest_best_use,
                String valrep_landimp_physical_road,
                String valrep_landimp_physical_surfacing,
                String valrep_landimp_lotclass_prop_type,
                String valrep_landimp_physical_pub_trans_desc,
                /*MAJOR COMMERCIAL CENTER*/
                String valrep_landimp_mcc_location_1,
                String valrep_landimp_mcc_distance_1,
                String valrep_landimp_mcc_location_2,
                String valrep_landimp_mcc_distance_2,
                String valrep_landimp_mcc_location_3,
                String valrep_landimp_mcc_distance_3,
                String valrep_landimp_mcc_location_4,
                String valrep_landimp_mcc_distance_4,
                String valrep_landimp_mcc_location_5,
                String valrep_landimp_mcc_distance_5,
                /*MAJOR ROADS*/
                String valrep_landimp_mr_location_1,
                String valrep_landimp_mr_distance_1,
                String valrep_landimp_mr_location_2,
                String valrep_landimp_mr_distance_2,
                String valrep_landimp_mr_location_3,
                String valrep_landimp_mr_distance_3,
                String valrep_landimp_mr_location_4,
                String valrep_landimp_mr_distance_4,
                String valrep_landimp_mr_location_5,
                String valrep_landimp_mr_distance_5,
                /*COMMUNITY FACILITIES/UTILITIES*/
                String valrep_landimp_util_electricity,
                String valrep_landimp_util_water,
                String valrep_landimp_util_telephone,
                String valrep_landimp_imp_roads,
                String valrep_landimp_imp_drainage,
                String valrep_landimp_imp_sidewalk,
                /*NEIGHBORHOOD DEVELOPMENT*/
                String asset_neighborhood_schools,
                String asset_neighborhood_government_facilities,
                String asset_neighborhood_banks,
                String asset_neighborhood_worship,
                String asset_neighborhood_shopping_malls,
                String asset_neighborhood_subdivisions,
                String asset_neighborhood_hotels,
                String asset_neighborhood_establishments,
                String asset_neighborhood_hospitals,
                String asset_neighborhood_automotive_centers,
                String asset_neighborhood_industrial_indicators


        );
    }
    interface Interactor {
        Single<Record> getRecordById(String recordId);
        void updatePropertyDesc(Record record);
    }
}
