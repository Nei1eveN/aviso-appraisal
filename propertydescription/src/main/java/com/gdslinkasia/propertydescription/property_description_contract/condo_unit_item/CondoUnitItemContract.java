package com.gdslinkasia.propertydescription.property_description_contract.condo_unit_item;

import com.gdslinkasia.base.model.details.ValrepLandimpLotDetail;

import io.reactivex.Single;

public interface CondoUnitItemContract {
    interface View {
        void showProgress(String title, String message);
        void hideProgress();
        void showDetails(ValrepLandimpLotDetail detail);
        void showExitDialog(String title, String message);
    }
    interface Presenter {
        void onStart(long uniqueId);
        void onDestroy();

        void updateItemDetails(long uniqueId,
                               String valrep_landimp_propdesc_title_type,
                               String valrep_landimp_propdesc_tct_no,
                               String valrep_landimp_propdesc_lot,
                               String valrep_landimp_propdesc_block,
                               String valrep_landimp_propdesc_area,
                               String valrep_landimp_desc_partitions_unit,
                               String valrep_landimp_desc_imp_flooring_unit,
                               String valrep_landimp_desc_ceiling_untit,
                               String valrep_landimp_desc_doors_unit,
                               String valrep_landimp_desc_observed_condition_unit,
                               String valrep_landimp_desc_others_unit
        );



    }
    interface Interactor {
        Single<ValrepLandimpLotDetail> getCondoUnitItemDetails(long uniqueId);

        void updateLotDetail(ValrepLandimpLotDetail detail);
    }
}
