package com.gdslinkasia.propertydescription.property_description_contract.condo_building_land_imp_list;

import com.gdslinkasia.base.model.details.ValrepLandimpImpDetail;

import java.util.List;

import io.reactivex.Flowable;
import io.reactivex.Single;

public interface CondoBuildingLandImpDescContract {

    interface View {
        void showProgress(String title, String message);

        void hideProgress();

        void showList(List<ValrepLandimpImpDetail> valrepLandimpImpDetails);

        void showEmptyState(String message);

        void showErrorDialog(String title, String message);
    }

    interface Presenter {
        void onStart(String recordId);

        void onDestroy();
    }

    interface Interactor {
        Single<List<ValrepLandimpImpDetail>> getCondoBuildingLandImpDescDescList(String recordId);

        Flowable<List<ValrepLandimpImpDetail>> getListFlowable(String recordId);
    }

}
