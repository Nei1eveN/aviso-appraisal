package com.gdslinkasia.propertydescription.property_description_contract.condo_unit_desc_list;

import android.app.Application;

import com.gdslinkasia.base.database.repository.PropertyDescriptionLocalRepository;
import com.gdslinkasia.base.model.details.ValrepLandimpLotDetail;

import java.util.List;

import io.reactivex.Flowable;
import io.reactivex.Single;

public class CondoUnitDescInteractorImpl implements CondoUnitDescContract.Interactor {

    private PropertyDescriptionLocalRepository localRepository;

    public CondoUnitDescInteractorImpl(Application application) {
        //same table for condo Unit Desc
        localRepository = new PropertyDescriptionLocalRepository(application);
    }

    @Override
    public Single<List<ValrepLandimpLotDetail>> getCondoUnitDescDescList(String recordId) {
        //same table for condo Unit Desc
        return localRepository.getOwnershipPropertyDescList(recordId);
    }

    @Override
    public Flowable<List<ValrepLandimpLotDetail>> getListFlowable(String recordId) {
        return localRepository.getListFlowable(recordId);
    }
}
