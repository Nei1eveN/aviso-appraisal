package com.gdslinkasia.propertydescription.property_description_contract.desc_of_improvement_item;

import com.gdslinkasia.base.model.details.ValrepLandimpImpDetail;
import com.gdslinkasia.base.model.details.ValrepLandimpImpValuation;

import io.reactivex.Single;

public interface LandImpDescItemContract {
    interface View {
        void showProgress(String title, String message);
        void hideProgress();
        void showDetails(ValrepLandimpImpDetail detail);
        void showExitDialog(String title, String message);
    }
    interface Presenter {
        void onStart(long uniqueId);
        void onDestroy();

        void updateItemDetails(long uniqueId,
                               String valrep_landimp_desc_valuation_approach,
                               String valrep_landimp_impsummary1_building_desc,
                               String valrep_landimp_desc_imp_floor_area,
                               String valrep_landimp_tax_dec_imp_td_classification,
                               String valrep_landimp_desc_observed_condition,
                               String valrep_landimp_desc_construction_feature,
                               String valrep_landimp_impsummary1_no_of_floors,
                               String valrep_landimp_impsummary1_no_of_bedrooms,
                               String valrep_landimp_desc_impsummary1_no_of_t_b,
                               String valrep_landimp_desc_foundation,
                               String valrep_landimp_desc_exterior_walls,
                               String valrep_landimp_desc_interior_walls,
                               String valrep_landimp_desc_imp_flooring,
                               String valrep_landimp_desc_imp_roofing,
                               String valrep_landimp_desc_ceiling,
                               String valrep_landimp_desc_doors,
                               String valrep_landimp_desc_imp_windows,
                               String valrep_landimp_desc_partitions,
                               String valrep_landimp_desc_others,
                               String valrep_landimp_desc_economic_life,
                               String valrep_landimp_desc_effective_age,
                               String valrep_landimp_desc_imp_remain_life,
                               String valrep_landimp_desc_imp_remarks
        );



    }
    interface Interactor {
        Single<ValrepLandimpImpDetail> getItemDetails(long uniqueId);

        Single<ValrepLandimpImpValuation> getValuationDetails(long descId);

        void updateValrepLandimpImpDetail(ValrepLandimpImpDetail detail);

        void updateLandimpImpValuation(ValrepLandimpImpValuation valuation);
    }
}
