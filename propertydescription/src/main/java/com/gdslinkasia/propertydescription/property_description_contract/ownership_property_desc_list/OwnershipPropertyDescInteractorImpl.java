package com.gdslinkasia.propertydescription.property_description_contract.ownership_property_desc_list;

import android.app.Application;

import com.gdslinkasia.base.database.repository.PropertyDescriptionLocalRepository;
import com.gdslinkasia.base.model.details.ValrepLandimpLotDetail;
import com.gdslinkasia.base.model.details.ValrepLandimpLotValuation;
import com.gdslinkasia.base.model.details.ValrepLandimpLotValuationDetail;
import com.gdslinkasia.base.model.job.Record;

import java.util.List;

import io.reactivex.Flowable;
import io.reactivex.Single;

public class OwnershipPropertyDescInteractorImpl implements OwnershipPropertyDescContract.Interactor {

    private PropertyDescriptionLocalRepository localRepository;

    public OwnershipPropertyDescInteractorImpl(Application application) {
        localRepository = new PropertyDescriptionLocalRepository(application);
    }

    @Override
    public Single<List<ValrepLandimpLotDetail>> getOwnershipPropertyDescList(String recordId) {
        return localRepository.getOwnershipPropertyDescList(recordId);
    }

    @Override
    public Flowable<List<ValrepLandimpLotDetail>> getListFlowable(String recordId) {
        return localRepository.getListFlowable(recordId);
    }

    @Override
    public void insertLotDetail(ValrepLandimpLotDetail detail) {
        localRepository.insertLotDetail(detail);
    }

    @Override
    public Single<Record> getRecordByRecordId(String recordId) {
        return localRepository.getRecordByRecordId(recordId);
    }

    @Override
    public void onDestroy() {
        localRepository.onDestroy();
    }

    @Override
    public long insertOwnershipPropertyDesc(ValrepLandimpLotDetail detail) {
        return localRepository.insertOwnershipPropertyDesc(detail);
    }

    @Override
    public Single<ValrepLandimpLotValuation> getValrepLandimpLotValuationByUid(long recordUniqueId) {
        return localRepository.getValrepLandimpLotValuationByUid(recordUniqueId);
    }

    /*@Override
    public Single<List<ValrepLandimpLotValuationDetail>> getValrepLandimpLotValuationDetailsByUid(long lotValuationUniqueId) {
        return localRepository.getValrepLandimpLotValuationDetailsByUid(lotValuationUniqueId);
    }*/

    @Override
    public void insertLotValuationDetail(ValrepLandimpLotValuationDetail lotValuationDetail) {
        localRepository.insertLotValuationDetail(lotValuationDetail);
    }
}
