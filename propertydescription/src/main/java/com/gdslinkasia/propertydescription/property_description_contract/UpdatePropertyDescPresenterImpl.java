package com.gdslinkasia.propertydescription.property_description_contract;

import android.app.Application;
import android.os.Handler;
import android.util.Log;

import com.gdslinkasia.base.model.job.Record;

import io.reactivex.Single;
import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.disposables.CompositeDisposable;
import io.reactivex.observers.DisposableSingleObserver;
import io.reactivex.schedulers.Schedulers;

public class UpdatePropertyDescPresenterImpl implements UpdatePropertyDescContract.Presenter {

    private UpdatePropertyDescContract.View view;
    private UpdatePropertyDescContract.Interactor interactor;

    private CompositeDisposable compositeDisposable = new CompositeDisposable();

    public UpdatePropertyDescPresenterImpl(UpdatePropertyDescContract.View view, Application application) {
        this.view = view;
        this.interactor = new UpdatePropertyDescInteractorImpl(application);
    }


    @Override
    public void onStart(String recordId) {
        view.showProgress("Loading Record", "Please wait...");
        compositeDisposable.add(getLocalSingleObserver(recordId).subscribeWith(getLocalObserver()));
    }

    @Override
    public void onDestroy() {
        view = null;
        compositeDisposable.clear();
    }

    @Override
    public void updatePropertyDesc(String recordId, String valrep_landimp_id_statutory_constraints,
                                   String valrep_landimp_id_statutory_constraints_desc, String valrep_landimp_physical_lot_type,
                                   String valrep_landimp_physical_shape, String valrep_landimp_physical_terrain,
                                   String valrep_landimp_physical_elevation,
                                   String valrep_landimp_physical_frontage, String valrep_landimp_physical_depth,
                                   String valrep_landimp_id_plotting,
                                   String valrep_landimp_id_lot_config,
                                   String valrep_landimp_id_geodetic_plan, String valrep_landimp_id_tax,
                                   String valrep_landimp_id_subd_map, String valrep_landimp_id_others,
                                   String valrep_landimp_bound1_orientation, String valrep_landimp_bound2_orientation,
                                   String valrep_landimp_bound3_orientation, String valrep_landimp_bound4_orientation,
                                   String valrep_landimp_lotclass_per_tax_dec, String valrep_landimp_desc_town_class,
                                   String valrep_landimp_tax_dec_town_type, String valrep_landimp_lotclass_neighborhood,
                                   String valrep_landimp_lotclass_actual_usage, String valrep_landimp_lotclass_highest_best_use,
                                   String valrep_landimp_physical_road,
                                   String valrep_landimp_physical_surfacing, String valrep_landimp_lotclass_prop_type,
                                   String valrep_landimp_physical_pub_trans_desc, String valrep_landimp_mcc_location_1,
                                   String valrep_landimp_mcc_distance_1, String valrep_landimp_mcc_location_2,
                                   String valrep_landimp_mcc_distance_2,
                                   String valrep_landimp_mcc_location_3, String valrep_landimp_mcc_distance_3,
                                   String valrep_landimp_mcc_location_4,
                                   String valrep_landimp_mcc_distance_4, String valrep_landimp_mcc_location_5, String valrep_landimp_mcc_distance_5,
                                   String valrep_landimp_mr_location_1, String valrep_landimp_mr_distance_1, String valrep_landimp_mr_location_2,
                                   String valrep_landimp_mr_distance_2, String valrep_landimp_mr_location_3, String valrep_landimp_mr_distance_3,
                                   String valrep_landimp_mr_location_4, String valrep_landimp_mr_distance_4, String valrep_landimp_mr_location_5,
                                   String valrep_landimp_mr_distance_5, String valrep_landimp_util_electricity, String valrep_landimp_util_water,
                                   String valrep_landimp_util_telephone, String valrep_landimp_imp_roads, String valrep_landimp_imp_drainage,
                                   String valrep_landimp_imp_sidewalk,
                                   String asset_neighborhood_schools, String asset_neighborhood_government_facilities, String asset_neighborhood_banks,
                                   String asset_neighborhood_worship, String asset_neighborhood_shopping_malls, String asset_neighborhood_subdivisions,
                                   String asset_neighborhood_hotels, String asset_neighborhood_establishments, String asset_neighborhood_hospitals,
                                   String asset_neighborhood_automotive_centers, String asset_neighborhood_industrial_indicators) {

                compositeDisposable.add(getLocalSingleObserver(recordId).subscribeWith(
                        getRecordUpdate(
                                 valrep_landimp_id_statutory_constraints,
                                 valrep_landimp_id_statutory_constraints_desc,
                                /*PHYSICAL FEATURES*/
                                 valrep_landimp_physical_lot_type,
                                 valrep_landimp_physical_shape,
                                 valrep_landimp_physical_terrain,
                                 valrep_landimp_physical_elevation,
                                 valrep_landimp_physical_frontage,
                                 valrep_landimp_physical_depth,
                                /*BASIS OF IDENTIFICATION*/
                                 valrep_landimp_id_plotting,
                                 valrep_landimp_id_lot_config,
                                 valrep_landimp_id_geodetic_plan,
                                 valrep_landimp_id_tax,
                                 valrep_landimp_id_subd_map,
                                 valrep_landimp_id_others,
                                /*BOUNDARIES*/
                                 valrep_landimp_bound1_orientation,
                                 valrep_landimp_bound2_orientation,
                                 valrep_landimp_bound3_orientation,
                                 valrep_landimp_bound4_orientation,
                                /*ZONING ORDINANCE*/
                                 valrep_landimp_lotclass_per_tax_dec,
                                /*GENERAL*/
                                 valrep_landimp_desc_town_class,
                                 valrep_landimp_tax_dec_town_type,
                                 valrep_landimp_lotclass_neighborhood,
                                 valrep_landimp_lotclass_actual_usage,
                                 valrep_landimp_lotclass_highest_best_use,
                                 valrep_landimp_physical_road,
                                 valrep_landimp_physical_surfacing,
                                 valrep_landimp_lotclass_prop_type,
                                 valrep_landimp_physical_pub_trans_desc,
                                /*MAJOR COMMERCIAL CENTER*/
                                 valrep_landimp_mcc_location_1,
                                 valrep_landimp_mcc_distance_1,
                                 valrep_landimp_mcc_location_2,
                                 valrep_landimp_mcc_distance_2,
                                 valrep_landimp_mcc_location_3,
                                 valrep_landimp_mcc_distance_3,
                                 valrep_landimp_mcc_location_4,
                                 valrep_landimp_mcc_distance_4,
                                 valrep_landimp_mcc_location_5,
                                 valrep_landimp_mcc_distance_5,
                                /*MAJOR ROADS*/
                                 valrep_landimp_mr_location_1,
                                 valrep_landimp_mr_distance_1,
                                 valrep_landimp_mr_location_2,
                                 valrep_landimp_mr_distance_2,
                                 valrep_landimp_mr_location_3,
                                 valrep_landimp_mr_distance_3,
                                 valrep_landimp_mr_location_4,
                                 valrep_landimp_mr_distance_4,
                                 valrep_landimp_mr_location_5,
                                 valrep_landimp_mr_distance_5,
                                /*COMMUNITY FACILITIES/UTILITIES*/
                                 valrep_landimp_util_electricity,
                                 valrep_landimp_util_water,
                                 valrep_landimp_util_telephone,
                                 valrep_landimp_imp_roads,
                                 valrep_landimp_imp_drainage,
                                 valrep_landimp_imp_sidewalk,
                                /*NEIGHBORHOOD DEVELOPMENT*/
                                 asset_neighborhood_schools,
                                 asset_neighborhood_government_facilities,
                                 asset_neighborhood_banks,
                                 asset_neighborhood_worship,
                                 asset_neighborhood_shopping_malls,
                                 asset_neighborhood_subdivisions,
                                 asset_neighborhood_hotels,
                                 asset_neighborhood_establishments,
                                 asset_neighborhood_hospitals,
                                 asset_neighborhood_automotive_centers,
                                 asset_neighborhood_industrial_indicators
                        )
                ));

    }


    //TODO Single disposable for fetching 1 Record from database based on recordId
    private Single<Record> getLocalSingleObserver(String recordId) {
        return interactor.getRecordById(recordId)
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread());
    }

    //TODO Results from disposable will be fetched here
    private DisposableSingleObserver<Record> getLocalObserver() {
        return new DisposableSingleObserver<Record>() {
            @Override
            public void onSuccess(Record record) {
                Log.d("int--LocalRepo", "Logic Triggered: UpdateAppraisalDetailsPresenter");
                view.showDetails(record);
                view.showSnackMessage("Loading Successful");
                view.hideProgress();
                new Handler().postDelayed(() -> view.hideProgress(), 1000);
            }

            @Override
            public void onError(Throwable e) {
                view.hideProgress();
                view.showExitDialog("Record Error", e.getMessage());
            }
        };
    }

    //TODO Update Record with parameters passed from method updateScopeOfWork
    private DisposableSingleObserver<Record> getRecordUpdate(
                    String valrep_landimp_id_statutory_constraints,
                    String valrep_landimp_id_statutory_constraints_desc,
                /*PHYSICAL FEATURES*/
                    String valrep_landimp_physical_lot_type,
                    String valrep_landimp_physical_shape,
                    String valrep_landimp_physical_terrain,
                    String valrep_landimp_physical_elevation,
                    String valrep_landimp_physical_frontage,
                    String valrep_landimp_physical_depth,
                /*BASIS OF IDENTIFICATION*/
                    String valrep_landimp_id_plotting,
                    String valrep_landimp_id_lot_config,
                    String valrep_landimp_id_geodetic_plan,
                    String valrep_landimp_id_tax,
                    String valrep_landimp_id_subd_map,
                    String valrep_landimp_id_others,
                /*BOUNDARIES*/
                    String valrep_landimp_bound1_orientation,
                    String valrep_landimp_bound2_orientation,
                    String valrep_landimp_bound3_orientation,
                    String valrep_landimp_bound4_orientation,
                /*ZONING ORDINANCE*/
                    String valrep_landimp_lotclass_per_tax_dec,
                /*GENERAL*/
                    String valrep_landimp_desc_town_class,
                    String valrep_landimp_tax_dec_town_type,
                    String valrep_landimp_lotclass_neighborhood,
                    String valrep_landimp_lotclass_actual_usage,
                    String valrep_landimp_lotclass_highest_best_use,
                    String valrep_landimp_physical_road,
                    String valrep_landimp_physical_surfacing,
                    String valrep_landimp_lotclass_prop_type,
                    String valrep_landimp_physical_pub_trans_desc,
                /*MAJOR COMMERCIAL CENTER*/
                    String valrep_landimp_mcc_location_1,
                    String valrep_landimp_mcc_distance_1,
                    String valrep_landimp_mcc_location_2,
                    String valrep_landimp_mcc_distance_2,
                    String valrep_landimp_mcc_location_3,
                    String valrep_landimp_mcc_distance_3,
                    String valrep_landimp_mcc_location_4,
                    String valrep_landimp_mcc_distance_4,
                    String valrep_landimp_mcc_location_5,
                    String valrep_landimp_mcc_distance_5,
                /*MAJOR ROADS*/
                    String valrep_landimp_mr_location_1,
                    String valrep_landimp_mr_distance_1,
                    String valrep_landimp_mr_location_2,
                    String valrep_landimp_mr_distance_2,
                    String valrep_landimp_mr_location_3,
                    String valrep_landimp_mr_distance_3,
                    String valrep_landimp_mr_location_4,
                    String valrep_landimp_mr_distance_4,
                    String valrep_landimp_mr_location_5,
                    String valrep_landimp_mr_distance_5,
                /*COMMUNITY FACILITIES/UTILITIES*/
                    String valrep_landimp_util_electricity,
                    String valrep_landimp_util_water,
                    String valrep_landimp_util_telephone,
                    String valrep_landimp_imp_roads,
                    String valrep_landimp_imp_drainage,
                    String valrep_landimp_imp_sidewalk,
                /*NEIGHBORHOOD DEVELOPMENT*/
                    String asset_neighborhood_schools,
                    String asset_neighborhood_government_facilities,
                    String asset_neighborhood_banks,
                    String asset_neighborhood_worship,
                    String asset_neighborhood_shopping_malls,
                    String asset_neighborhood_subdivisions,
                    String asset_neighborhood_hotels,
                    String asset_neighborhood_establishments,
                    String asset_neighborhood_hospitals,
                    String asset_neighborhood_automotive_centers,
                    String asset_neighborhood_industrial_indicators
    ) {
        return new DisposableSingleObserver<Record>() {
            @Override
            public void onSuccess(Record record) {
                Log.d("int--RecordFlowable", "Logic Triggered: UpdatePropertyDescriptions");

                record.setValrepLandimpIdStatutoryConstraints(valrep_landimp_id_statutory_constraints);
                record.setValrepLandimpIdStatutoryConstraintsDesc(valrep_landimp_id_statutory_constraints_desc);

                /*PHYSICAL FEATURES*/
                record.setValrepLandimpPhysicalLotType(valrep_landimp_physical_lot_type);
                record.setValrepLandimpPhysicalShape(valrep_landimp_physical_shape);
                record.setValrepLandimpPhysicalTerrain(valrep_landimp_physical_terrain);
                record.setValrepLandimpPhysicalElevation(valrep_landimp_physical_elevation);
                record.setValrepLandimpPhysicalFrontage(valrep_landimp_physical_frontage);
                record.setValrepLandimpPhysicalDepth(valrep_landimp_physical_depth);

                /*BASIS OF IDENTIFICATION*/
                record.setValrepLandimpIdPlotting(valrep_landimp_id_plotting);
                record.setValrepLandimpIdLotConfig(valrep_landimp_id_lot_config);
                record.setValrepLandimpIdGeodeticPlan(valrep_landimp_id_geodetic_plan);
                record.setValrepLandimpIdTax(valrep_landimp_id_tax);
                record.setValrepLandimpIdSubdMap(valrep_landimp_id_subd_map);
                record.setValrepLandimpIdOthers(valrep_landimp_id_others);

                /*BOUNDARIES*/
                record.setValrepLandimpBound1Orientation(valrep_landimp_bound1_orientation);
                record.setValrepLandimpBound2Orientation(valrep_landimp_bound2_orientation);
                record.setValrepLandimpBound3Orientation(valrep_landimp_bound3_orientation);
                record.setValrepLandimpBound4Orientation(valrep_landimp_bound4_orientation);

                /*ZONING ORDINANCE*/
                record.setValrepLandimpLotclassPerTaxDec(valrep_landimp_lotclass_per_tax_dec);

                /*GENERAL*/
                record.setValrepLandimpDescTownClass(valrep_landimp_desc_town_class);
                record.setValrepLandimpTaxDecTownType(valrep_landimp_tax_dec_town_type);
                record.setValrepLandimpLotclassNeighborhood(valrep_landimp_lotclass_neighborhood);
                record.setValrepLandimpLotclassActualUsage(valrep_landimp_lotclass_actual_usage);
                record.setValrepLandimpLotclassHighestBestUse(valrep_landimp_lotclass_highest_best_use);
                record.setValrepLandimpPhysicalRoad(valrep_landimp_physical_road);
                record.setValrepLandimpPhysicalSurfacing(valrep_landimp_physical_surfacing);
                record.setValrepLandimpLotclassPropType(valrep_landimp_lotclass_prop_type);
                record.setValrepLandimpPhysicalPubTransDesc(valrep_landimp_physical_pub_trans_desc);

                /*MAJOR COMMERCIAL CENTER*/
                record.setValrepLandimpMccLocation1(valrep_landimp_mcc_location_1);
                record.setValrepLandimpMccDistance1(valrep_landimp_mcc_distance_1);
                record.setValrepLandimpMccLocation2(valrep_landimp_mcc_location_2);
                record.setValrepLandimpMccDistance2(valrep_landimp_mcc_distance_2);
                record.setValrepLandimpMccLocation3(valrep_landimp_mcc_location_3);
                record.setValrepLandimpMccDistance3(valrep_landimp_mcc_distance_3);
                record.setValrepLandimpMccLocation4(valrep_landimp_mcc_location_4);
                record.setValrepLandimpMccDistance4(valrep_landimp_mcc_distance_4);
                record.setValrepLandimpMccLocation5(valrep_landimp_mcc_location_5);
                record.setValrepLandimpMccDistance5(valrep_landimp_mcc_distance_5);


                /*MAJOR ROADS*/
                record.setValrepLandimpMrLocation1(valrep_landimp_mr_location_1);
                record.setValrepLandimpMrDistance1(valrep_landimp_mr_distance_1);
                record.setValrepLandimpMrLocation2(valrep_landimp_mr_location_2);
                record.setValrepLandimpMrDistance2(valrep_landimp_mr_distance_2);
                record.setValrepLandimpMrLocation3(valrep_landimp_mr_location_3);
                record.setValrepLandimpMrDistance3(valrep_landimp_mr_distance_3);
                record.setValrepLandimpMrLocation4(valrep_landimp_mr_location_4);
                record.setValrepLandimpMrDistance4(valrep_landimp_mr_distance_4);
                record.setValrepLandimpMrLocation5(valrep_landimp_mr_location_5);
                record.setValrepLandimpMrDistance5(valrep_landimp_mr_distance_5);

                /*COMMUNITY FACILITIES/UTILITIES*/
                record.setValrepLandimpUtilElectricity(valrep_landimp_util_electricity);
                record.setValrepLandimpUtilWater(valrep_landimp_util_water);
                record.setValrepLandimpUtilTelephone(valrep_landimp_util_telephone);
                record.setValrepLandimpImpRoads(valrep_landimp_imp_roads);
                record.setValrepLandimpImpDrainage(valrep_landimp_imp_drainage);
                record.setValrepLandimpImpSidewalk(valrep_landimp_imp_sidewalk);

                /*NEIGHBORHOOD DEVELOPMENT*/
                record.setAssetNeighborhoodSchools(asset_neighborhood_schools);
                record.setAssetNeighborhoodGovernmentFacilities(asset_neighborhood_government_facilities);
                record.setAssetNeighborhoodBanks(asset_neighborhood_banks);
                record.setAssetNeighborhoodWorship(asset_neighborhood_worship);
                record.setAssetNeighborhoodShoppingMalls(asset_neighborhood_shopping_malls);
                record.setAssetNeighborhoodSubdivisions(asset_neighborhood_subdivisions);
                record.setAssetNeighborhoodHotels(asset_neighborhood_hotels);
                record.setAssetNeighborhoodEstablishments(asset_neighborhood_establishments);
                record.setAssetNeighborhoodHospitals(asset_neighborhood_hospitals);
                record.setAssetNeighborhoodAutomotiveCenters(asset_neighborhood_automotive_centers);
                record.setAssetNeighborhoodIndustrialIndicators(asset_neighborhood_industrial_indicators);


                interactor.updatePropertyDesc(record);

                view.showSnackMessage("Saving Successful");

                new Handler().postDelayed(() -> view.hideProgress(), 2000);
            }

            @Override
            public void onError(Throwable e) {
                view.hideProgress();
                view.showExitDialog("Record Error", e.getMessage());
            }
        };
    }
}