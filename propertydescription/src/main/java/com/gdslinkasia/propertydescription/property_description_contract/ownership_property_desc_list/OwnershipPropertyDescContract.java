package com.gdslinkasia.propertydescription.property_description_contract.ownership_property_desc_list;

import com.gdslinkasia.base.model.details.ValrepLandimpLotDetail;
import com.gdslinkasia.base.model.details.ValrepLandimpLotValuation;
import com.gdslinkasia.base.model.details.ValrepLandimpLotValuationDetail;
import com.gdslinkasia.base.model.job.Record;

import java.util.List;

import io.reactivex.Flowable;
import io.reactivex.Single;
import io.reactivex.disposables.Disposable;

public interface OwnershipPropertyDescContract {

    interface View {
        void showProgress(String title, String message);

        void hideProgress();

        void showList(List<ValrepLandimpLotDetail> valrepLandimpLotDetails);

        void showEmptyState(String message);

        void showErrorDialog(String title, String message);
    }

    interface Presenter {
        void onStart(String recordId);

        void onResume(String recordId);

        void onDestroy();

        void addNewLotDetail(String recordId, int recyclerViewVisibility, int tvNoRecordVisibility);
    }

    interface Interactor {
        Single<List<ValrepLandimpLotDetail>> getOwnershipPropertyDescList(String recordId);

        Flowable<List<ValrepLandimpLotDetail>> getListFlowable(String recordId);

        void insertLotDetail(ValrepLandimpLotDetail detail);

        Single<Record> getRecordByRecordId(String recordId);

        void onDestroy();

        long insertOwnershipPropertyDesc(ValrepLandimpLotDetail detail);

        Single<ValrepLandimpLotValuation> getValrepLandimpLotValuationByUid(long recordUniqueId);

        void insertLotValuationDetail(ValrepLandimpLotValuationDetail lotValuationDetail);
    }

}
