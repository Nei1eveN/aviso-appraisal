package com.gdslinkasia.propertydescription.property_description_contract;

import android.app.Application;

import com.gdslinkasia.base.database.repository.JobDetailsLocalRepository;
import com.gdslinkasia.base.model.job.Record;

import io.reactivex.Single;

class UpdatePropertyDescInteractorImpl implements UpdatePropertyDescContract.Interactor {

    private JobDetailsLocalRepository localRepository;

    public UpdatePropertyDescInteractorImpl(Application application) {
        localRepository = new JobDetailsLocalRepository(application);
    }

    @Override
    public Single<Record> getRecordById(String recordId) {
        return localRepository.getRecordById(recordId);
    }

    @Override
    public void updatePropertyDesc(Record record) {
        localRepository.updateRecord(record);  //update all record: should be rename to updateRecordAll
    }
}
