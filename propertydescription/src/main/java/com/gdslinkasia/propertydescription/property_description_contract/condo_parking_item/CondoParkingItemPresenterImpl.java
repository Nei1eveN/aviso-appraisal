package com.gdslinkasia.propertydescription.property_description_contract.condo_parking_item;

import android.app.Application;
import android.os.Handler;
import android.util.Log;

import com.gdslinkasia.base.model.details.ValrepLandimpParkingValuationDetail;

import io.reactivex.Single;
import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.disposables.CompositeDisposable;
import io.reactivex.observers.DisposableSingleObserver;
import io.reactivex.schedulers.Schedulers;

public class CondoParkingItemPresenterImpl implements CondoParkingItemContract.Presenter {
    private CondoParkingItemContract.View view;
    private CondoParkingItemContract.Interactor interactor;
    private CompositeDisposable compositeDisposable = new CompositeDisposable();

    public CondoParkingItemPresenterImpl(CondoParkingItemContract.View view, Application application) {
        this.view = view;
        this.interactor = new CondoParkingItemInteractorImpl(application);
    }

    @Override
    public void onStart(long uniqueId) {
        view.showProgress("Loading Details", "Please wait...");
        compositeDisposable.add(getSingleObservable(uniqueId).subscribeWith(getSingleObserver()));
    }

    @Override
    public void onDestroy() {
        view = null;
        compositeDisposable.clear();
    }

    @Override
    public void updateItemDetails(long uniqueId,
                        String valrep_landimp_value_parking_no,
                        String valrep_landimp_value_parkinglot_area,
                        String valrep_landimp_value_parkinglot_value
    ) {

        compositeDisposable.add(getSingleObservable(uniqueId).subscribeWith(getRecordUpdate(
                 valrep_landimp_value_parking_no,
                 valrep_landimp_value_parkinglot_area,
                 valrep_landimp_value_parkinglot_value
        )));
    }


    private Single<ValrepLandimpParkingValuationDetail> getSingleObservable(long uniqueId) {
        return interactor.getDetails(uniqueId)
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread());
    }

    private DisposableSingleObserver<ValrepLandimpParkingValuationDetail> getSingleObserver() {
        return new DisposableSingleObserver<ValrepLandimpParkingValuationDetail>() {
            @Override
            public void onSuccess(ValrepLandimpParkingValuationDetail detail) {
                view.showDetails(detail);
                new Handler().postDelayed(() -> view.hideProgress(), 2000);
            }

            @Override
            public void onError(Throwable e) {
                view.showExitDialog("Record Error", e.getMessage());
                new Handler().postDelayed(() -> view.hideProgress(), 2000);
            }
        };
    }

    private DisposableSingleObserver<ValrepLandimpParkingValuationDetail> getRecordUpdate(
            String valrep_landimp_value_parking_no,
            String valrep_landimp_value_parkinglot_area,
            String valrep_landimp_value_parkinglot_value) {
        return new DisposableSingleObserver<ValrepLandimpParkingValuationDetail>() {
            @Override
            public void onSuccess(ValrepLandimpParkingValuationDetail detail) {
                detail.setValrepLandimpValueParkingNo(valrep_landimp_value_parking_no);
                detail.setValrepLandimpValueParkinglotArea(valrep_landimp_value_parkinglot_area);
                detail.setValrepLandimpValueParkinglotValue(valrep_landimp_value_parkinglot_value);

                interactor.updateLotDetail(detail);

                Log.d("int--lotDetailSave", "Logic triggered");
            }

            @Override
            public void onError(Throwable e) {

            }
        };
    }
}
