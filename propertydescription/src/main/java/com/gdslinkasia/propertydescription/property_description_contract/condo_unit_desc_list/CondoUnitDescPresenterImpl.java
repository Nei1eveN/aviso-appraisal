package com.gdslinkasia.propertydescription.property_description_contract.condo_unit_desc_list;

import android.app.Application;
import android.util.Log;

import com.gdslinkasia.base.model.details.ValrepLandimpLotDetail;

import java.util.List;

import io.reactivex.Flowable;
import io.reactivex.Single;
import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.disposables.CompositeDisposable;
import io.reactivex.functions.Consumer;
import io.reactivex.observers.DisposableSingleObserver;
import io.reactivex.schedulers.Schedulers;

public class CondoUnitDescPresenterImpl implements CondoUnitDescContract.Presenter {

    private CondoUnitDescContract.View view;
    private CondoUnitDescContract.Interactor interactor;
    private CompositeDisposable compositeDisposable = new CompositeDisposable();

    public CondoUnitDescPresenterImpl(CondoUnitDescContract.View view, Application application) {
        this.view = view;
        this.interactor = new CondoUnitDescInteractorImpl(application);
    }

    @Override
    public void onStart(String recordId) {
//        view.showProgress();
        compositeDisposable.add(getSingleListObservable(recordId).subscribeWith(getSingleListObserver(recordId)));
    }

    @Override
    public void onDestroy() {
        view = null;
        compositeDisposable.clear();
    }

    private Single<List<ValrepLandimpLotDetail>> getSingleListObservable(String recordId) {
        return interactor.getCondoUnitDescDescList(recordId)
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread());
    }

    private DisposableSingleObserver<List<ValrepLandimpLotDetail>> getSingleListObserver(String recordId) {
        return new DisposableSingleObserver<List<ValrepLandimpLotDetail>>() {
            @Override
            public void onSuccess(List<ValrepLandimpLotDetail> valrepLandimpLotDetails) {
                if (!valrepLandimpLotDetails.isEmpty()) {
//                    view.showList(valrepLandimpLotDetails);
                    compositeDisposable.add(getListFlowable(recordId).subscribe(getFlowableObserver()));
                } else {
                    view.showEmptyState("There are no records available.");
                }
//                new Handler().postDelayed(() -> view.hideProgress(), 1000);
            }

            @Override
            public void onError(Throwable e) {
                view.showEmptyState(e.getMessage());
//                new Handler().postDelayed(() -> view.hideProgress(), 1000);
            }
        };
    }

    private Flowable<List<ValrepLandimpLotDetail>> getListFlowable(String recordId) {
        return interactor.getListFlowable(recordId)
                .subscribeOn(Schedulers.computation())
                .observeOn(AndroidSchedulers.mainThread());
    }

    private Consumer<List<ValrepLandimpLotDetail>> getFlowableObserver() {
        return valrepLandimpLotDetails -> {
            view.showList(valrepLandimpLotDetails);
            Log.d("int--flowableSize", String.valueOf(valrepLandimpLotDetails.size()));
        };
    }

}
