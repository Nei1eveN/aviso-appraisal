package com.gdslinkasia.propertydescription.property_description_contract.desc_of_improvement_list;

import android.app.Application;

import com.gdslinkasia.base.database.repository.LandImpImpDetailLocalRepository;
import com.gdslinkasia.base.model.details.ValrepLandimpImpDetail;
import com.gdslinkasia.base.model.details.ValrepLandimpImpValuation;
import com.gdslinkasia.base.model.job.Record;

import java.util.List;

import io.reactivex.Flowable;
import io.reactivex.Single;

public class LandImpDescImprovementInteractorImpl implements LandImpDescImprovementContract.Interactor {

    private LandImpImpDetailLocalRepository localRepository;

    public LandImpDescImprovementInteractorImpl(Application application) {
        //same table for condo Unit Desc
        localRepository = new LandImpImpDetailLocalRepository(application);
    }

    @Override
    public Single<List<ValrepLandimpImpDetail>> getCondoBuildingLandImpDescDescList(String recordId) {
        //same table for condo and land imp (improvements and buildings)
        return localRepository.getLandImpImpDetailsList(recordId);
    }

    @Override
    public Flowable<List<ValrepLandimpImpDetail>> getListFlowable(String recordId) {
        return localRepository.getListFlowable(recordId);
    }

    @Override
    public long insertLandimpImpDetail(ValrepLandimpImpDetail detail) {
        return localRepository.insertLandimpImpDetail(detail);
    }

    @Override
    public void insertLandimpImpValuation(ValrepLandimpImpValuation valuation) {
        localRepository.insertLandimpImpValuation(valuation);
    }

    @Override
    public Single<Record> getRecordById(String recordId) {
        return localRepository.getRecordById(recordId);
    }
}
