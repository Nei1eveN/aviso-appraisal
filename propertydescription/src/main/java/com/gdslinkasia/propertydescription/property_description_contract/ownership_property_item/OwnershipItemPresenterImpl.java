package com.gdslinkasia.propertydescription.property_description_contract.ownership_property_item;

import android.app.Application;
import android.os.Handler;
import android.util.Log;

import com.gdslinkasia.base.model.details.ValrepLandimpLotDetail;

import io.reactivex.Single;
import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.disposables.CompositeDisposable;
import io.reactivex.observers.DisposableSingleObserver;
import io.reactivex.schedulers.Schedulers;

public class OwnershipItemPresenterImpl implements OwnershipItemContract.Presenter {
    private OwnershipItemContract.View view;
    private OwnershipItemContract.Interactor interactor;
    private CompositeDisposable compositeDisposable = new CompositeDisposable();

    public OwnershipItemPresenterImpl(OwnershipItemContract.View view, Application application) {
        this.view = view;
        this.interactor = new OwnershipItemInteractorImpl(application);
    }

    @Override
    public void onStart(long uniqueId) {
        view.showProgress("Loading Details", "Please wait...");
        compositeDisposable.add(getSingleObservable(uniqueId).subscribeWith(getSingleObserver()));
    }

    @Override
    public void onDestroy() {
        view = null;
        compositeDisposable.clear();
//        interactor.onDestroy();
    }

    @Override
    public void updateItemDetails(long uniqueId, String acTitle, String etTitleNo, String etLotNo, String etBlockNo, String etPlanNo, String etLandAreaSqm, String etLessSqm, String macDescription, String acDateRegMonth, String etDateRegDay, String etDateRegYear, String etRegOwner, String acRegDeeds, String etTdSubmitted, String acTdClassification) {
        compositeDisposable.add(getSingleObservable(uniqueId).subscribeWith(getRecordUpdate(acTitle, etTitleNo, etLotNo, etBlockNo, etPlanNo, etLandAreaSqm, etLessSqm, macDescription, acDateRegMonth, etDateRegDay, etDateRegYear, etRegOwner, acRegDeeds, etTdSubmitted, acTdClassification)));
    }

    @Override
    public void deleteItem(long uniqueId) {
        view.showProgress("Removing Item", "Please wait...");
        interactor.deleteItem(uniqueId);
        new Handler().postDelayed(() -> {
            view.hideProgress();
            view.showToastMessage("Item Successfully Removed.");
            view.finishActivity();
        }, 2500);
    }

    private Single<ValrepLandimpLotDetail> getSingleObservable(long uniqueId) {
        return interactor.getOwnershipItemDetails(uniqueId)
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread());
    }

    private DisposableSingleObserver<ValrepLandimpLotDetail> getSingleObserver() {
        return new DisposableSingleObserver<ValrepLandimpLotDetail>() {
            @Override
            public void onSuccess(ValrepLandimpLotDetail detail) {
                view.showDetails(detail);
                new Handler().postDelayed(() -> view.hideProgress(), 2000);
            }

            @Override
            public void onError(Throwable e) {
                view.showExitDialog("Record Error", e.getMessage());
                new Handler().postDelayed(() -> view.hideProgress(), 2000);
            }
        };
    }

    private DisposableSingleObserver<ValrepLandimpLotDetail> getRecordUpdate(
            String acTitle, String etTitleNo,
            String etLotNo, String etBlockNo,
            String etPlanNo, String etLandAreaSqm, String etLessSqm,
            String macDescription,
            String acDateRegMonth, String etDateRegDay, String etDateRegYear,
            String etRegOwner, String acRegDeeds,
            String etTdSubmitted, String acTdClassification) {
        return new DisposableSingleObserver<ValrepLandimpLotDetail>() {
            @Override
            public void onSuccess(ValrepLandimpLotDetail detail) {
                detail.setValrepLandimpPropdescTitleType(acTitle);
                detail.setValrepLandimpPropdescTctNo(etTitleNo);
                detail.setValrepLandimpPropdescLot(etLotNo);
                detail.setValrepLandimpPropdescBlock(etBlockNo);
                detail.setValrepLandimpPropdescSurveyNos(etPlanNo);
                detail.setValrepLandimpPropdescArea(etLandAreaSqm);
                detail.setValrepLandimpPropdescDeductions(etLessSqm);
                detail.setValrepLandimpPropdescDeductionsDesc(macDescription);
                detail.setValrepLandimpPropdescRegistryDateMonth(acDateRegMonth);
                detail.setValrepLandimpPropdescRegistryDateDay(etDateRegDay);
                detail.setValrepLandimpPropdescRegistryDateYear(etDateRegYear);
                detail.setValrepLandimpPropdescRegisteredOwner(etRegOwner);
                detail.setValrepLandimpPropdescRegistryOfDeeds(acRegDeeds);
                detail.setValrepLandimpTaxDecLotNo(etTdSubmitted);
                detail.setValrepLandimpTaxDecClassification(acTdClassification);

                interactor.updateLotDetail(detail);

                Log.d("int--lotDetailSave", "Logic triggered");
            }

            @Override
            public void onError(Throwable e) {

            }
        };
    }
}
