package com.gdslinkasia.propertydescription.property_description_contract.desc_of_improvement_item;

import android.app.Application;
import android.os.Handler;
import android.util.Log;

import com.gdslinkasia.base.model.details.ValrepLandimpImpDetail;
import com.gdslinkasia.base.model.details.ValrepLandimpImpValuation;

import io.reactivex.Single;
import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.disposables.CompositeDisposable;
import io.reactivex.observers.DisposableSingleObserver;
import io.reactivex.schedulers.Schedulers;

import static com.gdslinkasia.base.utils.GlobalFunctions.nullcheckInt;
import static com.gdslinkasia.base.utils.GlobalString.cleanFormat;
import static com.gdslinkasia.base.utils.GlobalString.getFormattedAmountDouble;

public class LandImpDescItemPresenterImpl implements LandImpDescItemContract.Presenter {
    private LandImpDescItemContract.View view;
    private LandImpDescItemContract.Interactor interactor;
    private CompositeDisposable compositeDisposable = new CompositeDisposable();

    public LandImpDescItemPresenterImpl(LandImpDescItemContract.View view, Application application) {
        this.view = view;
        this.interactor = new LandImpDescItemInteractorImpl(application);
    }

    @Override
    public void onStart(long uniqueId) {
        view.showProgress("Loading Details", "Please wait...");
        compositeDisposable.add(getSingleObservable(uniqueId).subscribeWith(getSingleObserver()));
    }

    @Override
    public void onDestroy() {
        view = null;
        compositeDisposable.clear();
    }

    @Override
    public void updateItemDetails(long uniqueId,
                                  String valrep_landimp_desc_valuation_approach,
                                  String valrep_landimp_impsummary1_building_desc,
                                  String valrep_landimp_desc_imp_floor_area,
                                  String valrep_landimp_tax_dec_imp_td_classification,
                                  String valrep_landimp_desc_observed_condition,
                                  String valrep_landimp_desc_construction_feature,
                                  String valrep_landimp_impsummary1_no_of_floors,
                                  String valrep_landimp_impsummary1_no_of_bedrooms,
                                  String valrep_landimp_desc_impsummary1_no_of_t_b,
                                  String valrep_landimp_desc_foundation,
                                  String valrep_landimp_desc_exterior_walls,
                                  String valrep_landimp_desc_interior_walls,
                                  String valrep_landimp_desc_imp_flooring,
                                  String valrep_landimp_desc_imp_roofing,
                                  String valrep_landimp_desc_ceiling,
                                  String valrep_landimp_desc_doors,
                                  String valrep_landimp_desc_imp_windows,
                                  String valrep_landimp_desc_partitions,
                                  String valrep_landimp_desc_others,
                                  String valrep_landimp_desc_economic_life,
                                  String valrep_landimp_desc_effective_age,
                                  String valrep_landimp_desc_imp_remain_life,
                                  String valrep_landimp_desc_imp_remarks) {

        compositeDisposable.add(getSingleObservable(uniqueId).subscribeWith(getRecordUpdate(
                valrep_landimp_desc_valuation_approach,
                valrep_landimp_impsummary1_building_desc,
                valrep_landimp_desc_imp_floor_area,
                valrep_landimp_tax_dec_imp_td_classification,
                valrep_landimp_desc_observed_condition,
                valrep_landimp_desc_construction_feature,
                valrep_landimp_impsummary1_no_of_floors,
                valrep_landimp_impsummary1_no_of_bedrooms,
                valrep_landimp_desc_impsummary1_no_of_t_b,
                valrep_landimp_desc_foundation,
                valrep_landimp_desc_exterior_walls,
                valrep_landimp_desc_interior_walls,
                valrep_landimp_desc_imp_flooring,
                valrep_landimp_desc_imp_roofing,
                valrep_landimp_desc_ceiling,
                valrep_landimp_desc_doors,
                valrep_landimp_desc_imp_windows,
                valrep_landimp_desc_partitions,
                valrep_landimp_desc_others,
                valrep_landimp_desc_economic_life,
                valrep_landimp_desc_effective_age,
                valrep_landimp_desc_imp_remain_life,
                valrep_landimp_desc_imp_remarks
        )));
    }


    private Single<ValrepLandimpImpDetail> getSingleObservable(long uniqueId) {
        return interactor.getItemDetails(uniqueId)
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread());
    }

    private DisposableSingleObserver<ValrepLandimpImpDetail> getSingleObserver() {
        return new DisposableSingleObserver<ValrepLandimpImpDetail>() {
            @Override
            public void onSuccess(ValrepLandimpImpDetail detail) {
                view.showDetails(detail);
                new Handler().postDelayed(() -> view.hideProgress(), 2000);
            }

            @Override
            public void onError(Throwable e) {
                view.showExitDialog("Record Error", e.getMessage());
                new Handler().postDelayed(() -> view.hideProgress(), 2000);
            }
        };
    }

    private DisposableSingleObserver<ValrepLandimpImpDetail> getRecordUpdate(
            String valrep_landimp_desc_valuation_approach,
            String valrep_landimp_impsummary1_building_desc,
            String valrep_landimp_desc_imp_floor_area,
            String valrep_landimp_tax_dec_imp_td_classification,
            String valrep_landimp_desc_observed_condition,
            String valrep_landimp_desc_construction_feature,
            String valrep_landimp_impsummary1_no_of_floors,
            String valrep_landimp_impsummary1_no_of_bedrooms,
            String valrep_landimp_desc_impsummary1_no_of_t_b,
            String valrep_landimp_desc_foundation,
            String valrep_landimp_desc_exterior_walls,
            String valrep_landimp_desc_interior_walls,
            String valrep_landimp_desc_imp_flooring,
            String valrep_landimp_desc_imp_roofing,
            String valrep_landimp_desc_ceiling,
            String valrep_landimp_desc_doors,
            String valrep_landimp_desc_imp_windows,
            String valrep_landimp_desc_partitions,
            String valrep_landimp_desc_others,
            String valrep_landimp_desc_economic_life,
            String valrep_landimp_desc_effective_age,
            String valrep_landimp_desc_imp_remain_life,
            String valrep_landimp_desc_imp_remarks) {
        return new DisposableSingleObserver<ValrepLandimpImpDetail>() {
            @Override
            public void onSuccess(ValrepLandimpImpDetail detail) {

                detail.setValrepLandimpDescValuationApproach(valrep_landimp_desc_valuation_approach);
                detail.setValrepLandimpImpsummary1BuildingDesc(valrep_landimp_impsummary1_building_desc);
                detail.setValrepLandimpDescImpFloorArea(valrep_landimp_desc_imp_floor_area);
                detail.setValrepLandimpTaxDecImpTdClassification(valrep_landimp_tax_dec_imp_td_classification);
                detail.setValrepLandimpDescObservedCondition(valrep_landimp_desc_observed_condition);
                detail.setValrepLandimpDescConstructionFeature(valrep_landimp_desc_construction_feature);
                detail.setValrepLandimpImpsummary1NoOfFloors(valrep_landimp_impsummary1_no_of_floors);
                detail.setValrepLandimpImpsummary1NoOfBedrooms(valrep_landimp_impsummary1_no_of_bedrooms);
                detail.setValrepLandimpDescImpsummary1NoOfTB(valrep_landimp_desc_impsummary1_no_of_t_b);

                //multi ACTV dropdowns
                detail.setValrepLandimpDescFoundation(valrep_landimp_desc_foundation);
                detail.setValrepLandimpDescExteriorWalls(valrep_landimp_desc_exterior_walls);
                detail.setValrepLandimpDescInteriorWalls(valrep_landimp_desc_interior_walls);
                detail.setValrepLandimpDescImpFlooring(valrep_landimp_desc_imp_flooring);
                detail.setValrepLandimpDescImpRoofing(valrep_landimp_desc_imp_roofing);
                detail.setValrepLandimpDescCeiling(valrep_landimp_desc_ceiling);
                detail.setValrepLandimpDescDoors(valrep_landimp_desc_doors);
                detail.setValrepLandimpDescImpWindows(valrep_landimp_desc_imp_windows);
                detail.setValrepLandimpDescPartitions(valrep_landimp_desc_partitions);

                detail.setValrepLandimpDescOthers(valrep_landimp_desc_others);
                detail.setValrepLandimpDescEconomicLife(valrep_landimp_desc_economic_life);
                detail.setValrepLandimpDescEffectiveAge(valrep_landimp_desc_effective_age);
                detail.setValrepLandimpDescImpRemainLife(valrep_landimp_desc_imp_remain_life);
                detail.setValrepLandimpDescImpRemarks(valrep_landimp_desc_imp_remarks);

                double cleanEffectiveAge = Double.valueOf(cleanFormat(nullcheckInt(valrep_landimp_desc_effective_age)));
                double cleanEconomicLife = Double.valueOf(cleanFormat(nullcheckInt(valrep_landimp_desc_economic_life)));

                double cleanFloorArea = Double.valueOf(cleanFormat(nullcheckInt(valrep_landimp_desc_imp_floor_area)));


                compositeDisposable.add(interactor.getValuationDetails(detail.getUniqueId()).subscribeWith(
                        new DisposableSingleObserver<ValrepLandimpImpValuation>() {
                            @Override
                            public void onSuccess(ValrepLandimpImpValuation valuation) {


                                double cleanRCNSQM = Double.valueOf(cleanFormat(nullcheckInt(valuation.getValrepLandimpImpValueTotalReproductionCost())));
                                double cleanRCN = cleanFloorArea * cleanRCNSQM;

                                double cleanIncurablePerc = (cleanEffectiveAge / cleanEconomicLife) * 100;
                                Log.d("int--cleanIncPer", String.valueOf(cleanIncurablePerc));
                                double cleanIncurable = cleanRCN * ((cleanEffectiveAge / cleanEconomicLife) * 100 / 100);

                                double cleanFunctionalObsolence = Double.valueOf(cleanFormat(nullcheckInt(valuation.getValrepLandimpImpValueFunctionalVal())));
                                double cleanEconomicObsolence = Double.valueOf(cleanFormat(nullcheckInt(valuation.getValrepLandimpImpValueEconomicVal())));

                                double cleanDepreciatedRCN = cleanRCN - cleanIncurable - cleanFunctionalObsolence - cleanEconomicObsolence;

                                valuation.setValrepLandimpImpValueDescription(valrep_landimp_impsummary1_building_desc);
                                valuation.setValrepLandimpImpValueTotalArea(getFormattedAmountDouble(String.valueOf(cleanFloorArea)));
                                valuation.setValrepLandimpImpValueTotalReproductionCost(getFormattedAmountDouble(String.valueOf(cleanRCNSQM)));
                                valuation.setValrepLandimpImpValueRcnNew(getFormattedAmountDouble(String.valueOf(cleanRCN)));

                                valuation.setValrepLandimpImpValuePhysicalIncurablePer(getFormattedAmountDouble(String.valueOf(cleanIncurablePerc)));
                                valuation.setValrepLandimpImpValuePhysicalIncurableVal(getFormattedAmountDouble(String.valueOf(cleanIncurable)));

                                valuation.setValrepLandimpImpValueFunctionalVal(getFormattedAmountDouble(String.valueOf(cleanFunctionalObsolence)));
                                valuation.setValrepLandimpImpValueEconomicVal(getFormattedAmountDouble(String.valueOf(cleanEconomicObsolence)));

                                valuation.setValrepLandimpImpValueDepreciatedValue(getFormattedAmountDouble(String.valueOf(cleanDepreciatedRCN)));

                                valuation.setValrepLandimpImpValueEconomicLife(getFormattedAmountDouble(String.valueOf(cleanEconomicLife)));
                                valuation.setValrepLandimpImpValueEffectiveLife(getFormattedAmountDouble(String.valueOf(cleanEffectiveAge)));

                                interactor.updateLandimpImpValuation(valuation);

                                Log.d("int--valuationSave", "Logic Triggered");
                            }

                            @Override
                            public void onError(Throwable e) {
                                e.printStackTrace();
                            }
                        }));

                interactor.updateValrepLandimpImpDetail(detail);

                Log.d("int--lotDetailSave", "Logic triggered");
            }

            @Override
            public void onError(Throwable e) {
                e.printStackTrace();

            }
        };
    }
}
