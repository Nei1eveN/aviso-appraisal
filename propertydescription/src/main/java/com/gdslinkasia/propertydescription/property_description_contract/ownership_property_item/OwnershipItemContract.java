package com.gdslinkasia.propertydescription.property_description_contract.ownership_property_item;

import com.gdslinkasia.base.model.details.ValrepLandimpLotDetail;

import io.reactivex.Single;

public interface OwnershipItemContract {
    interface View {
        void showProgress(String title, String message);
        void hideProgress();
        void showDetails(ValrepLandimpLotDetail detail);
        void showExitDialog(String title, String message);

        void finishActivity();

        void showToastMessage(String message);
    }
    interface Presenter {
        void onStart(long uniqueId);
        void onDestroy();

        void updateItemDetails(long uniqueId, String acTitle, String etTitleNo, String etLotNo, String etBlockNo, String etPlanNo, String etLandAreaSqm, String etLessSqm, String macDescription, String acDateRegMonth, String etDateRegDay, String etDateRegYear, String etRegOwner, String acRegDeeds, String etTdSubmitted, String acTdClassification);

        void deleteItem(long uniqueId);
    }
    interface Interactor {
        Single<ValrepLandimpLotDetail> getOwnershipItemDetails(long uniqueId);

        void updateLotDetail(ValrepLandimpLotDetail detail);

        void onDestroy();

        void deleteItem(long uniqueId);
    }
}
