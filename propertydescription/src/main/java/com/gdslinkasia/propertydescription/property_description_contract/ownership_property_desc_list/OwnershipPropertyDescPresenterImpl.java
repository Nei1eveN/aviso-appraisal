package com.gdslinkasia.propertydescription.property_description_contract.ownership_property_desc_list;

import android.app.Application;
import android.os.Handler;
import android.util.Log;
import android.view.View;

import com.gdslinkasia.base.model.details.ValrepLandimpLotDetail;
import com.gdslinkasia.base.model.details.ValrepLandimpLotValuationDetail;
import com.gdslinkasia.base.model.job.Record;

import java.util.List;

import io.reactivex.Flowable;
import io.reactivex.Single;
import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.disposables.CompositeDisposable;
import io.reactivex.functions.Consumer;
import io.reactivex.observers.DisposableSingleObserver;
import io.reactivex.schedulers.Schedulers;

public class OwnershipPropertyDescPresenterImpl implements OwnershipPropertyDescContract.Presenter {

    private OwnershipPropertyDescContract.View view;
    private OwnershipPropertyDescContract.Interactor interactor;
    private CompositeDisposable compositeDisposable = new CompositeDisposable();

    public OwnershipPropertyDescPresenterImpl(OwnershipPropertyDescContract.View view, Application application) {
        this.view = view;
        this.interactor = new OwnershipPropertyDescInteractorImpl(application);
    }

    @Override
    public void onStart(String recordId) {
//        view.showProgress();
        view.showEmptyState("Loading Items. Please wait...");
    }

    @Override
    public void onResume(String recordId) {
        view.showProgress("Loading Items", "Please wait...");
        compositeDisposable.add(getSingleListObservable(recordId).subscribeWith(getSingleListObserver(recordId)));
    }

    @Override
    public void onDestroy() {
        view = null;
        compositeDisposable.clear();
        interactor.onDestroy();
    }

    @Override
    public void addNewLotDetail(String recordId, int recyclerViewVisibility, int tvNoRecordVisibility) {
        compositeDisposable.add(getRecordByRecordId(recordId).subscribeWith(addLotDetailObservable(recyclerViewVisibility, tvNoRecordVisibility)));
    }

    private Single<List<ValrepLandimpLotDetail>> getSingleListObservable(String recordId) {
        return interactor.getOwnershipPropertyDescList(recordId)
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread());
    }

    private DisposableSingleObserver<List<ValrepLandimpLotDetail>> getSingleListObserver(String recordId) {
        return new DisposableSingleObserver<List<ValrepLandimpLotDetail>>() {
            @Override
            public void onSuccess(List<ValrepLandimpLotDetail> valrepLandimpLotDetails) {
                if (!valrepLandimpLotDetails.isEmpty()) {
//                    view.showList(valrepLandimpLotDetails);
                    compositeDisposable.add(getListFlowable(recordId).subscribe(getFlowableObserver()));
                } else {
                    view.showEmptyState("There are no records available.");
                }
                new Handler().postDelayed(() -> view.hideProgress(), 1000);
            }

            @Override
            public void onError(Throwable e) {
                view.showEmptyState(e.getMessage());
//                new Handler().postDelayed(() -> view.hideProgress(), 1000);
            }
        };
    }

    private Flowable<List<ValrepLandimpLotDetail>> getListFlowable(String recordId) {
        return interactor.getListFlowable(recordId)
                .onBackpressureLatest()
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                ;
    }

    private Consumer<List<ValrepLandimpLotDetail>> getFlowableObserver() {
        return valrepLandimpLotDetails -> {
            view.showList(valrepLandimpLotDetails);
            Log.d("int--flowableSize", String.valueOf(valrepLandimpLotDetails.size()));
        };
    }

    //TODO RECORD OBJECT FETCHING
    private Single<Record> getRecordByRecordId(String recordId) {
        return interactor.getRecordByRecordId(recordId)
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread());
    }

    private DisposableSingleObserver<Record> addLotDetailObservable(int recyclerViewVisibility, int tvNoRecordVisibility) {
        return new DisposableSingleObserver<Record>() {
            @Override
            public void onSuccess(Record record) {
                //TODO TO ADD: LAND (MARKET APPROACH) WHEN ADDING PROPERTY DESCRIPTION ITEM

                compositeDisposable.add(interactor.getValrepLandimpLotValuationByUid(record.getUniqueId())
                        .subscribeOn(Schedulers.io())
                        .observeOn(AndroidSchedulers.mainThread())
                        .subscribe(valrepLandimpLotValuation -> {
                            if (recyclerViewVisibility == View.VISIBLE && tvNoRecordVisibility == View.INVISIBLE) {
                                ValrepLandimpLotDetail detail = new ValrepLandimpLotDetail();
                                detail.setRecordId(record.getRecordId());
                                detail.setRecordUId(record.getUniqueId());

                                long ownershipPropertyDescId = interactor.insertOwnershipPropertyDesc(detail);

                                ValrepLandimpLotValuationDetail lotValuationDetail = new ValrepLandimpLotValuationDetail();
                                lotValuationDetail.setLotValuationId(valrepLandimpLotValuation.getUniqueId());
                                lotValuationDetail.setOwnershipPropertyDescId(ownershipPropertyDescId);
                                lotValuationDetail.setRecordId(record.getRecordId());

                                interactor.insertLotValuationDetail(lotValuationDetail);
                            } else {
                                ValrepLandimpLotDetail detail = new ValrepLandimpLotDetail();
                                detail.setRecordId(record.getRecordId());
                                detail.setRecordUId(record.getUniqueId());

                                long ownershipPropertyDescId = interactor.insertOwnershipPropertyDesc(detail);

                                ValrepLandimpLotValuationDetail lotValuationDetail = new ValrepLandimpLotValuationDetail();
                                lotValuationDetail.setLotValuationId(valrepLandimpLotValuation.getUniqueId());
                                lotValuationDetail.setOwnershipPropertyDescId(ownershipPropertyDescId);
                                lotValuationDetail.setRecordId(record.getRecordId());

                                interactor.insertLotValuationDetail(lotValuationDetail);

                                onResume(record.getRecordId());
                            }
                        }));
            }

            @Override
            public void onError(Throwable e) {
                e.printStackTrace();
                view.showErrorDialog("Something Went Wrong", e.getMessage());
            }
        };
    }

}
