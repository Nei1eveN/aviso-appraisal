package com.gdslinkasia.propertydescription.property_description_contract.land_other_imp_item;

import android.app.Application;

import com.gdslinkasia.base.database.repository.LandImpOtherDetailLocalRepository;
import com.gdslinkasia.base.model.details.ValrepLandimpOtherLandImpDetail;
import com.gdslinkasia.base.model.details.ValrepLandimpOtherLandImpValue;

import io.reactivex.Single;

public class LandOtherImpItemInteractorImpl implements LandOtherImpItemContract.Interactor {

    private LandImpOtherDetailLocalRepository localRepository;

    public LandOtherImpItemInteractorImpl(Application application) {
        this.localRepository = new LandImpOtherDetailLocalRepository(application);
    }

    @Override
    public Single<ValrepLandimpOtherLandImpDetail> getLandImpImpDetailsList(long uniqueId) {
        return localRepository.getDetails(uniqueId);
    }

    @Override
    public void updateLotDetail(ValrepLandimpOtherLandImpDetail detail) {
        localRepository.updateLotDetail(detail);
    }

    @Override
    public Single<ValrepLandimpOtherLandImpValue> getOtherLandImpValueDetails(long otherDescImpId) {
        return localRepository.getOtherLandImpValue(otherDescImpId);
    }

    @Override
    public void updateOtherLandImpValue(ValrepLandimpOtherLandImpValue value) {
        localRepository.updateOtherValue(value);
    }
}
