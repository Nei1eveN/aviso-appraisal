package com.gdslinkasia.propertydescription.property_description_contract.condo_unit_desc_list;

import com.gdslinkasia.base.model.details.ValrepLandimpLotDetail;

import java.util.List;

import io.reactivex.Flowable;
import io.reactivex.Single;

public interface CondoUnitDescContract {

    interface View {
        void showProgress(String title, String message);

        void hideProgress();

        void showList(List<ValrepLandimpLotDetail> valrepLandimpLotDetails);

        void showEmptyState(String message);

        void showErrorDialog(String title, String message);
    }

    interface Presenter {
        void onStart(String recordId);

        void onDestroy();
    }

    interface Interactor {
        Single<List<ValrepLandimpLotDetail>> getCondoUnitDescDescList(String recordId);

        Flowable<List<ValrepLandimpLotDetail>> getListFlowable(String recordId);
    }

}
