package com.gdslinkasia.propertydescription.property_description_contract.condo_unit_item;

import android.app.Application;

import com.gdslinkasia.base.database.repository.PropertyDescriptionLocalRepository;
import com.gdslinkasia.base.model.details.ValrepLandimpLotDetail;

import io.reactivex.Single;

public class CondoUnitItemInteractorImpl implements CondoUnitItemContract.Interactor {

    private PropertyDescriptionLocalRepository localRepository;

    public CondoUnitItemInteractorImpl(Application application) {
        this.localRepository = new PropertyDescriptionLocalRepository(application);
    }

    @Override
    public Single<ValrepLandimpLotDetail> getCondoUnitItemDetails(long uniqueId) {
        return localRepository.getPropertyDescDetail(uniqueId);
    }

    @Override
    public void updateLotDetail(ValrepLandimpLotDetail detail) {
        localRepository.updateLotDetail(detail);
    }
}
