package com.gdslinkasia.propertydescription.property_description_contract.desc_of_improvement_list;

import android.app.Application;
import android.util.Log;
import android.view.View;

import com.gdslinkasia.base.model.details.ValrepLandimpImpDetail;
import com.gdslinkasia.base.model.details.ValrepLandimpImpValuation;
import com.gdslinkasia.base.model.job.Record;

import java.util.List;

import io.reactivex.Flowable;
import io.reactivex.Single;
import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.disposables.CompositeDisposable;
import io.reactivex.functions.Consumer;
import io.reactivex.observers.DisposableSingleObserver;
import io.reactivex.schedulers.Schedulers;

public class LandImpDescImprovementPresenterImpl implements LandImpDescImprovementContract.Presenter {

    private LandImpDescImprovementContract.View view;
    private LandImpDescImprovementContract.Interactor interactor;
    private CompositeDisposable compositeDisposable = new CompositeDisposable();

    public LandImpDescImprovementPresenterImpl(LandImpDescImprovementContract.View view, Application application) {
        this.view = view;
        this.interactor = new LandImpDescImprovementInteractorImpl(application);
    }

    @Override
    public void onStart(String recordId) {
//        view.showProgress();
        compositeDisposable.add(getSingleListObservable(recordId).subscribeWith(getSingleListObserver(recordId)));
    }

    @Override
    public void onDestroy() {
        view = null;
        compositeDisposable.clear();
    }

    @Override
    public void insertLandimpImpDetail(String recordId, int recyclerViewVisibility, int tvNoRecordVisibility) {
        compositeDisposable.add(getRecordObservable(recordId).subscribeWith(getRecordObserver(recyclerViewVisibility, tvNoRecordVisibility)));
    }

    private Single<List<ValrepLandimpImpDetail>> getSingleListObservable(String recordId) {
        return interactor.getCondoBuildingLandImpDescDescList(recordId)
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread());
    }

    private DisposableSingleObserver<List<ValrepLandimpImpDetail>> getSingleListObserver(String recordId) {
        return new DisposableSingleObserver<List<ValrepLandimpImpDetail>>() {
            @Override
            public void onSuccess(List<ValrepLandimpImpDetail> valrepLandimpLotDetails) {
                if (!valrepLandimpLotDetails.isEmpty()) {
//                    view.showList(valrepLandimpLotDetails);
                    compositeDisposable.add(getListFlowable(recordId).subscribe(getFlowableObserver()));
                } else {
                    view.showEmptyState("There are no records available.");
                }
//                new Handler().postDelayed(() -> view.hideProgress(), 1000);
            }

            @Override
            public void onError(Throwable e) {
                view.showEmptyState(e.getMessage());
//                new Handler().postDelayed(() -> view.hideProgress(), 1000);
            }
        };
    }

    private Flowable<List<ValrepLandimpImpDetail>> getListFlowable(String recordId) {
        return interactor.getListFlowable(recordId)
                .subscribeOn(Schedulers.computation())
                .observeOn(AndroidSchedulers.mainThread());
    }

    private Consumer<List<ValrepLandimpImpDetail>> getFlowableObserver() {
        return valrepLandimpLotDetails -> {
            view.showList(valrepLandimpLotDetails);
            Log.d("int--flowableSize", String.valueOf(valrepLandimpLotDetails.size()));
        };
    }

    //TODO ADD ValrepLandimpImpDetail + ValrepLandimpImpValuation
    private Single<Record> getRecordObservable(String recordId) {
        return interactor.getRecordById(recordId)
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread());
    }

    private DisposableSingleObserver<Record> getRecordObserver(int recyclerViewVisibility, int tvNoRecordVisibility) {
        return new DisposableSingleObserver<Record>() {
            @Override
            public void onSuccess(Record record) {
                if (recyclerViewVisibility == View.VISIBLE && tvNoRecordVisibility == View.INVISIBLE) {
                    ValrepLandimpImpDetail detail = new ValrepLandimpImpDetail();
                    detail.setRecordId(record.getRecordId());
                    detail.setRecordUId(record.getUniqueId());

                    long detailUId = interactor.insertLandimpImpDetail(detail);
                    Log.d("int--detailUId", String.valueOf(detailUId));

                    ValrepLandimpImpValuation valuation = new ValrepLandimpImpValuation();
                    valuation.setRecordId(record.getRecordId());
                    valuation.setRecordUId(record.getUniqueId());
                    valuation.setDescId(detailUId);

                    interactor.insertLandimpImpValuation(valuation);
                } else {
                    ValrepLandimpImpDetail detail = new ValrepLandimpImpDetail();
                    detail.setRecordId(record.getRecordId());
                    detail.setRecordUId(record.getUniqueId());

                    long detailUId = interactor.insertLandimpImpDetail(detail);
                    Log.d("int--detailUId", String.valueOf(detailUId));

                    ValrepLandimpImpValuation valuation = new ValrepLandimpImpValuation();
                    valuation.setRecordId(record.getRecordId());
                    valuation.setRecordUId(record.getUniqueId());
                    valuation.setDescId(detailUId);

                    interactor.insertLandimpImpValuation(valuation);

                    LandImpDescImprovementPresenterImpl.this.onStart(record.getRecordId());
                }
            }

            @Override
            public void onError(Throwable e) {
                view.showErrorDialog("Something Went Wrong", e.getMessage());
            }
        };
    }

}
