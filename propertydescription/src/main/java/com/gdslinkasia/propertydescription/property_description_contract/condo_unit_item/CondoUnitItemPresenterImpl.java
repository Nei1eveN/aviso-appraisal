package com.gdslinkasia.propertydescription.property_description_contract.condo_unit_item;

import android.app.Application;
import android.os.Handler;
import android.util.Log;

import com.gdslinkasia.base.model.details.ValrepLandimpLotDetail;

import io.reactivex.Single;
import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.disposables.CompositeDisposable;
import io.reactivex.observers.DisposableSingleObserver;
import io.reactivex.schedulers.Schedulers;

public class CondoUnitItemPresenterImpl implements CondoUnitItemContract.Presenter {
    private CondoUnitItemContract.View view;
    private CondoUnitItemContract.Interactor interactor;
    private CompositeDisposable compositeDisposable = new CompositeDisposable();

    public CondoUnitItemPresenterImpl(CondoUnitItemContract.View view, Application application) {
        this.view = view;
        this.interactor = new CondoUnitItemInteractorImpl(application);
    }

    @Override
    public void onStart(long uniqueId) {
        view.showProgress("Loading Details", "Please wait...");
        compositeDisposable.add(getSingleObservable(uniqueId).subscribeWith(getSingleObserver()));
    }

    @Override
    public void onDestroy() {
        view = null;
        compositeDisposable.clear();
    }

    @Override
    public void updateItemDetails(long uniqueId,
                                  String valrep_landimp_propdesc_title_type,
                                  String valrep_landimp_propdesc_tct_no,
                                  String valrep_landimp_propdesc_lot,
                                  String valrep_landimp_propdesc_block,
                                  String valrep_landimp_propdesc_area,
                                  String valrep_landimp_desc_partitions_unit,
                                  String valrep_landimp_desc_imp_flooring_unit,
                                  String valrep_landimp_desc_ceiling_untit,
                                  String valrep_landimp_desc_doors_unit,
                                  String valrep_landimp_desc_observed_condition_unit,
                                  String valrep_landimp_desc_others_unit) {

        compositeDisposable.add(getSingleObservable(uniqueId).subscribeWith(getRecordUpdate(
                 valrep_landimp_propdesc_title_type,
                 valrep_landimp_propdesc_tct_no,
                 valrep_landimp_propdesc_lot,
                 valrep_landimp_propdesc_block,
                 valrep_landimp_propdesc_area,
                 valrep_landimp_desc_partitions_unit,
                 valrep_landimp_desc_imp_flooring_unit,
                 valrep_landimp_desc_ceiling_untit,
                 valrep_landimp_desc_doors_unit,
                 valrep_landimp_desc_observed_condition_unit,
                 valrep_landimp_desc_others_unit
        )));
    }


    private Single<ValrepLandimpLotDetail> getSingleObservable(long uniqueId) {
        return interactor.getCondoUnitItemDetails(uniqueId)
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread());
    }

    private DisposableSingleObserver<ValrepLandimpLotDetail> getSingleObserver() {
        return new DisposableSingleObserver<ValrepLandimpLotDetail>() {
            @Override
            public void onSuccess(ValrepLandimpLotDetail detail) {
                view.showDetails(detail);
                new Handler().postDelayed(() -> view.hideProgress(), 2000);
            }

            @Override
            public void onError(Throwable e) {
                view.showExitDialog("Record Error", e.getMessage());
                new Handler().postDelayed(() -> view.hideProgress(), 2000);
            }
        };
    }

    private DisposableSingleObserver<ValrepLandimpLotDetail> getRecordUpdate(
            String valrep_landimp_propdesc_title_type,
            String valrep_landimp_propdesc_tct_no,
            String valrep_landimp_propdesc_lot,
            String valrep_landimp_propdesc_block,
            String valrep_landimp_propdesc_area,
            String valrep_landimp_desc_partitions_unit,
            String valrep_landimp_desc_imp_flooring_unit,
            String valrep_landimp_desc_ceiling_untit,
            String valrep_landimp_desc_doors_unit,
            String valrep_landimp_desc_observed_condition_unit,
            String valrep_landimp_desc_others_unit) {
        return new DisposableSingleObserver<ValrepLandimpLotDetail>() {
            @Override
            public void onSuccess(ValrepLandimpLotDetail detail) {

                detail.setValrepLandimpPropdescTitleType(valrep_landimp_propdesc_title_type);
                detail.setValrepLandimpPropdescTctNo(valrep_landimp_propdesc_tct_no);
                detail.setValrepLandimpPropdescLot(valrep_landimp_propdesc_lot);
                detail.setValrepLandimpPropdescBlock(valrep_landimp_propdesc_block);
                detail.setValrepLandimpPropdescArea(valrep_landimp_propdesc_area);
                detail.setValrepLandimpDescPartitionsUnit(valrep_landimp_desc_partitions_unit);
                detail.setValrepLandimpDescImpFlooringUnit(valrep_landimp_desc_imp_flooring_unit);
                detail.setValrepLandimpDescCeilingUntit(valrep_landimp_desc_ceiling_untit);
                detail.setValrepLandimpDescDoorsUnit(valrep_landimp_desc_doors_unit);
                detail.setValrepLandimpDescObservedConditionUnit(valrep_landimp_desc_observed_condition_unit);
                detail.setValrepLandimpDescOthersUnit(valrep_landimp_desc_others_unit);

                interactor.updateLotDetail(detail);

                Log.d("int--lotDetailSave", "Logic triggered");
            }

            @Override
            public void onError(Throwable e) {

            }
        };
    }
}
