package com.gdslinkasia.propertydescription.property_description_contract.land_other_imp_item;

import android.app.Application;
import android.os.Handler;
import android.util.Log;

import com.gdslinkasia.base.model.details.ValrepLandimpOtherLandImpDetail;

import io.reactivex.Single;
import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.disposables.CompositeDisposable;
import io.reactivex.observers.DisposableSingleObserver;
import io.reactivex.schedulers.Schedulers;

import static com.gdslinkasia.base.utils.GlobalFunctions.nullcheckInt;
import static com.gdslinkasia.base.utils.GlobalString.cleanFormat;
import static com.gdslinkasia.base.utils.GlobalString.getFormattedAmountDouble;

public class LandOtherImpItemPresenterImpl implements LandOtherImpItemContract.Presenter {
    private LandOtherImpItemContract.View view;
    private LandOtherImpItemContract.Interactor interactor;
    private CompositeDisposable compositeDisposable = new CompositeDisposable();

    public LandOtherImpItemPresenterImpl(LandOtherImpItemContract.View view, Application application) {
        this.view = view;
        this.interactor = new LandOtherImpItemInteractorImpl(application);
    }

    @Override
    public void onStart(long uniqueId) {
        view.showProgress("Loading Details", "Please wait...");
        compositeDisposable.add(getSingleObservable(uniqueId).subscribeWith(getSingleObserver()));
    }

    @Override
    public void onDestroy() {
        view = null;
        compositeDisposable.clear();
    }

    @Override
    public void updateItemDetails(long uniqueId,
                                  String valrep_landimp_other_land_imp_details_type,
                                  String valrep_landimp_other_land_imp_details_desc,
                                  String valrep_landimp_other_land_imp_details_area,
                                  String valrep_landimp_other_land_imp_details_area_unit,
                                  String valrep_landimp_other_land_imp_details_condition,
                                  String valrep_landimp_other_land_imp_details_economic_life,
                                  String valrep_landimp_other_land_imp_details_effective_age,
                                  String valrep_landimp_other_land_imp_details_remain_life,
                                  String valrep_landimp_other_land_imp_details_remarks) {

        compositeDisposable.add(getSingleObservable(uniqueId).subscribeWith(getRecordUpdate(
                valrep_landimp_other_land_imp_details_type,
                valrep_landimp_other_land_imp_details_desc,
                valrep_landimp_other_land_imp_details_area,
                valrep_landimp_other_land_imp_details_area_unit,
                valrep_landimp_other_land_imp_details_condition,
                valrep_landimp_other_land_imp_details_economic_life,
                valrep_landimp_other_land_imp_details_effective_age,
                valrep_landimp_other_land_imp_details_remain_life,
                valrep_landimp_other_land_imp_details_remarks
        )));
    }


    private Single<ValrepLandimpOtherLandImpDetail> getSingleObservable(long uniqueId) {
        return interactor.getLandImpImpDetailsList(uniqueId)
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread());
    }

    private DisposableSingleObserver<ValrepLandimpOtherLandImpDetail> getSingleObserver() {
        return new DisposableSingleObserver<ValrepLandimpOtherLandImpDetail>() {
            @Override
            public void onSuccess(ValrepLandimpOtherLandImpDetail detail) {
                view.showDetails(detail);
                new Handler().postDelayed(() -> view.hideProgress(), 2000);
            }

            @Override
            public void onError(Throwable e) {
                view.showExitDialog("Record Error", e.getMessage());
                new Handler().postDelayed(() -> view.hideProgress(), 2000);
            }
        };
    }

    private DisposableSingleObserver<ValrepLandimpOtherLandImpDetail> getRecordUpdate(
            String valrep_landimp_other_land_imp_details_type,
            String valrep_landimp_other_land_imp_details_desc,
            String valrep_landimp_other_land_imp_details_area,
            String valrep_landimp_other_land_imp_details_area_unit,
            String valrep_landimp_other_land_imp_details_condition,
            String valrep_landimp_other_land_imp_details_economic_life,
            String valrep_landimp_other_land_imp_details_effective_age,
            String valrep_landimp_other_land_imp_details_remain_life,
            String valrep_landimp_other_land_imp_details_remarks) {
        return new DisposableSingleObserver<ValrepLandimpOtherLandImpDetail>() {
            @Override
            public void onSuccess(ValrepLandimpOtherLandImpDetail detail) {

                detail.setValrepLandimpOtherLandImpDetailsType(valrep_landimp_other_land_imp_details_type);
                detail.setValrepLandimpOtherLandImpDetailsDesc(valrep_landimp_other_land_imp_details_desc);
                detail.setValrepLandimpOtherLandImpDetailsArea(valrep_landimp_other_land_imp_details_area);
                detail.setValrepLandimpOtherLandImpDetailsAreaUnit(valrep_landimp_other_land_imp_details_area_unit);
                detail.setValrepLandimpOtherLandImpDetailsCondition(valrep_landimp_other_land_imp_details_condition);
                detail.setValrepLandimpOtherLandImpDetailsEconomicLife(valrep_landimp_other_land_imp_details_economic_life);
                detail.setValrepLandimpOtherLandImpDetailsEffectiveAge(valrep_landimp_other_land_imp_details_effective_age);
                detail.setValrepLandimpOtherLandImpDetailsRemainLife(valrep_landimp_other_land_imp_details_remain_life);
                detail.setValrepLandimpOtherLandImpDetailsRemarks(valrep_landimp_other_land_imp_details_remarks);

                compositeDisposable.add(interactor.getOtherLandImpValueDetails(detail.getUniqueId())
                        .subscribeOn(Schedulers.io())
                        .subscribe(value -> {
                            double cleanArea = Double.valueOf(cleanFormat(nullcheckInt(valrep_landimp_other_land_imp_details_area)));
                            double cleanUnitCost = Double.valueOf(cleanFormat(nullcheckInt(value.getValrepLandimpOtherLandImpValueUnitValue())));

                            Log.d("int--cleanArea", String.valueOf(cleanArea));
                            Log.d("int--cleanUnitCost", String.valueOf(cleanUnitCost));

                            double cleanRCN = cleanArea * cleanUnitCost;

                            Log.d("int--cleanRCN", String.valueOf(cleanRCN));

                            double cleanEconomicLife = Double.valueOf(cleanFormat(nullcheckInt(valrep_landimp_other_land_imp_details_economic_life)));
                            double cleanEffectiveAge = Double.valueOf(cleanFormat(nullcheckInt(valrep_landimp_other_land_imp_details_effective_age)));

                            double cleanPER = (cleanEffectiveAge / cleanEconomicLife) * 100;

                            Log.d("int--cleanPER", String.valueOf(cleanPER));

                            double cleanTotalDepreciation = cleanRCN * (cleanPER / 100);

                            double cleanDepreciatedRCN = cleanRCN - cleanTotalDepreciation;

                            value.setValrepLandimpOtherLandImpValueType(valrep_landimp_other_land_imp_details_type);
                            value.setValrepLandimpOtherLandImpValueDesc(valrep_landimp_other_land_imp_details_desc);
                            value.setValrepLandimpOtherLandImpValueEconomicLife(valrep_landimp_other_land_imp_details_economic_life);
                            value.setValrepLandimpOtherLandImpValueEffectiveAge(valrep_landimp_other_land_imp_details_effective_age);
                            value.setValrepLandimpOtherLandImpValueRemainLife(valrep_landimp_other_land_imp_details_remain_life);
                            value.setValrepLandimpOtherLandImpValueArea(valrep_landimp_other_land_imp_details_area);
                            value.setValrepLandimpOtherLandImpValueAreaUnit(valrep_landimp_other_land_imp_details_area_unit);
                            value.setValrepLandimpOtherLandImpValueUnitValue(getFormattedAmountDouble(String.valueOf(cleanUnitCost)));
                            value.setValrepLandimpOtherLandImpValueRcn(getFormattedAmountDouble(String.valueOf(cleanRCN)));
                            value.setValrepLandimpOtherLandImpValuePercentage(getFormattedAmountDouble(String.valueOf(cleanPER)));
                            value.setValrepLandimpOtherLandImpValueDepreciation(getFormattedAmountDouble(String.valueOf(cleanTotalDepreciation)));
                            value.setValrepLandimpOtherLandImpValueDepreciatedValue(getFormattedAmountDouble(String.valueOf(cleanDepreciatedRCN)));

                            interactor.updateOtherLandImpValue(value);

                            Log.d("int--lotOtherValueSave", "Logic Triggered");
                        }));

                interactor.updateLotDetail(detail);

                Log.d("int--lotOtherDetailSave", "Logic triggered");
            }

            @Override
            public void onError(Throwable e) {

            }
        };
    }
}
