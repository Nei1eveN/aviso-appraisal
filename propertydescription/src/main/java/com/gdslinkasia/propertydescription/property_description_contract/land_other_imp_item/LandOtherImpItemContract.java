package com.gdslinkasia.propertydescription.property_description_contract.land_other_imp_item;

import com.gdslinkasia.base.model.details.ValrepLandimpOtherLandImpDetail;
import com.gdslinkasia.base.model.details.ValrepLandimpOtherLandImpValue;

import io.reactivex.Single;

public interface LandOtherImpItemContract {
    interface View {
        void showProgress(String title, String message);
        void hideProgress();
        void showDetails(ValrepLandimpOtherLandImpDetail detail);
        void showExitDialog(String title, String message);
    }
    interface Presenter {
        void onStart(long uniqueId);
        void onDestroy();

        void updateItemDetails(long uniqueId,
                               String valrep_landimp_other_land_imp_details_type,
                               String valrep_landimp_other_land_imp_details_desc,
                               String valrep_landimp_other_land_imp_details_area,
                               String valrep_landimp_other_land_imp_details_area_unit,
                               String valrep_landimp_other_land_imp_details_condition,
                               String valrep_landimp_other_land_imp_details_economic_life,
                               String valrep_landimp_other_land_imp_details_effective_age,
                               String valrep_landimp_other_land_imp_details_remain_life,
                               String valrep_landimp_other_land_imp_details_remarks
        );



    }
    interface Interactor {
        Single<ValrepLandimpOtherLandImpDetail> getLandImpImpDetailsList(long uniqueId);

        void updateLotDetail(ValrepLandimpOtherLandImpDetail detail);

        Single<ValrepLandimpOtherLandImpValue> getOtherLandImpValueDetails(long otherDescImpId);

        void updateOtherLandImpValue(ValrepLandimpOtherLandImpValue value);
    }
}
