package com.gdslinkasia.propertydescription.property_description_contract.desc_of_improvement_item;

import android.app.Application;

import com.gdslinkasia.base.database.repository.LandImpImpDetailLocalRepository;
import com.gdslinkasia.base.model.details.ValrepLandimpImpDetail;
import com.gdslinkasia.base.model.details.ValrepLandimpImpValuation;

import io.reactivex.Single;

public class LandImpDescItemInteractorImpl implements LandImpDescItemContract.Interactor {

    private LandImpImpDetailLocalRepository localRepository;

    public LandImpDescItemInteractorImpl(Application application) {
        this.localRepository = new LandImpImpDetailLocalRepository(application);
    }

    @Override
    public Single<ValrepLandimpImpDetail> getItemDetails(long uniqueId) {
        return localRepository.getPropertyDescDetail(uniqueId);
    }

    @Override
    public Single<ValrepLandimpImpValuation> getValuationDetails(long descId) {
        return localRepository.getValuationDetails(descId);
    }

    @Override
    public void updateValrepLandimpImpDetail(ValrepLandimpImpDetail detail) {
        localRepository.updateLotDetail(detail);
    }

    @Override
    public void updateLandimpImpValuation(ValrepLandimpImpValuation valuation) {
        localRepository.updateLandimpImpValuation(valuation);
    }
}
