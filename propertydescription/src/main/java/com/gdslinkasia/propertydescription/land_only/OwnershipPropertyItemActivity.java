package com.gdslinkasia.propertydescription.land_only;

import android.app.DatePickerDialog;
import android.app.ProgressDialog;
import android.content.Intent;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuItem;
import android.widget.AutoCompleteTextView;
import android.widget.MultiAutoCompleteTextView;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AlertDialog;
import androidx.appcompat.app.AppCompatActivity;

import com.gdslinkasia.base.model.details.ValrepLandimpLotDetail;
import com.gdslinkasia.commonresources.R2;
import com.gdslinkasia.propertydescription.R;
import com.gdslinkasia.propertydescription.property_description_contract.ownership_property_item.OwnershipItemContract;
import com.gdslinkasia.propertydescription.property_description_contract.ownership_property_item.OwnershipItemPresenterImpl;
import com.google.android.material.textfield.TextInputEditText;

import java.util.Objects;

import butterknife.BindView;
import butterknife.ButterKnife;

import static com.gdslinkasia.base.database.DatabaseUtils.*;
import static com.gdslinkasia.base.utils.GlobalFunctions.*;

public class OwnershipPropertyItemActivity extends AppCompatActivity implements OwnershipItemContract.View {

    private long uniqueId;

    @BindView(R2.id.acTitleType) AutoCompleteTextView acTitleType;
    @BindView(R2.id.etTitleNo) TextInputEditText etTitleNo;
    @BindView(R2.id.etLotNo) TextInputEditText etLotNo;
    @BindView(R2.id.etBlock) TextInputEditText etBlock;
    @BindView(R2.id.etPlanNo) TextInputEditText etPlanNo;
    @BindView(R2.id.etLandAreaSqm) TextInputEditText etLandAreaSqm;
    @BindView(R2.id.etLessSqm) TextInputEditText etLessSqm;
    @BindView(R2.id.macDescription) MultiAutoCompleteTextView macDescription;
    @BindView(R2.id.acDateRegMonth) AutoCompleteTextView acDateRegMonth;
    @BindView(R2.id.etDateRegDay) TextInputEditText etDateRegDay;
    @BindView(R2.id.etDateRegYear) TextInputEditText etDateRegYear;
    @BindView(R2.id.etRegOwner) TextInputEditText etRegOwner;
    @BindView(R2.id.acRegDeeds) AutoCompleteTextView acRegDeeds;
    @BindView(R2.id.etTdSubmitted) TextInputEditText etTdSubmitted;
    @BindView(R2.id.acTdClassification) AutoCompleteTextView acTdClassification;

    private ProgressDialog progressDialog;

    private OwnershipItemContract.Presenter presenter;

//    DatePickerDialog datePickerDialog = new DatePickerDialog();

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.content_ownership_prop_desc);
        ButterKnife.bind(this);

        Intent intent = getIntent();
        uniqueId = intent.getLongExtra(UNIQUE_ID, 0);

        int itemPosition = intent.getIntExtra("item_position", 0);

        Objects.requireNonNull(getSupportActionBar()).setTitle("Property Details");
        getSupportActionBar().setSubtitle("Ownership and Property Description "+ itemPosition);
        getSupportActionBar().setIcon(R.drawable.ic_close_white_24dp);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);

        progressDialog = new ProgressDialog(this);
        presenter = new OwnershipItemPresenterImpl(this, getApplication());
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.menu_ownership_property_item, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(@NonNull MenuItem item) {
        if (item.getItemId() == android.R.id.home) {
            finish();
        } else if (item.getItemId() == R.id.delete) {
            new AlertDialog.Builder(this).setCancelable(false).setTitle("Delete Record").setMessage("You are about to remove this record. Shall we proceed?").setPositiveButton("DELETE", (dialog, which) -> presenter.deleteItem(uniqueId)).setNegativeButton("CANCEL", null).create().show();
        }
        return super.onOptionsItemSelected(item);
    }

    @Override
    protected void onResume() {
        super.onResume();
        presenter.onStart(uniqueId);
    }

    @Override
    protected void onPause() {
        super.onPause();
        presenter.updateItemDetails(
                uniqueId,
                acTitleType.getText().toString(),
                Objects.requireNonNull(etTitleNo.getText()).toString(), Objects.requireNonNull(etLotNo.getText()).toString(),
                Objects.requireNonNull(etBlock.getText()).toString(), Objects.requireNonNull(etPlanNo.getText()).toString(),
                Objects.requireNonNull(etLandAreaSqm.getText()).toString(), Objects.requireNonNull(etLessSqm.getText()).toString(),
                macDescription.getText().toString(),
                monthConversionFromLetters(acDateRegMonth.getText().toString()),
                Objects.requireNonNull(etDateRegDay.getText()).toString(),
                Objects.requireNonNull(etDateRegYear.getText()).toString(),
                Objects.requireNonNull(etRegOwner.getText()).toString(),
                acRegDeeds.getText().toString(),
                Objects.requireNonNull(etTdSubmitted.getText()).toString(),
                acTdClassification.getText().toString());
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        presenter.onDestroy();
        progressDialog.dismiss();
    }

    @Override
    public void showProgress(String title, String message) {
        progressDialog.setCancelable(false);
        progressDialog.setIndeterminate(false);
        progressDialog.setTitle(title);
        progressDialog.setMessage(message);
        progressDialog.show();
    }

    @Override
    public void hideProgress() {
        progressDialog.hide();
    }

    @Override
    public void showDetails(ValrepLandimpLotDetail detail) {
        acTitleType.setKeyListener(null);
        acTitleType.setAdapter(titleTypeAdapter(this, detail.getValrepLandimpPropdescTitleType(), acTitleType));
        acTitleType.setOnClickListener(v -> acTitleType.showDropDown());

        etTitleNo.setText(detail.getValrepLandimpPropdescTctNo());
        etLotNo.setText(detail.getValrepLandimpPropdescLot());

        etBlock.setText(detail.getValrepLandimpPropdescBlock());
        etPlanNo.setText(detail.getValrepLandimpPropdescSurveyNos());

        etLandAreaSqm.setText(detail.getValrepLandimpPropdescArea());
        etLessSqm.setText(detail.getValrepLandimpPropdescDeductions());

        macDescription.setText(detail.getValrepLandimpPropdescDeductionsDesc());
        macDescription.setAdapter(descriptionTypeAdapter(this));
        macDescription.setTokenizer(new MultiAutoCompleteTextView.CommaTokenizer());
        macDescription.setThreshold(1);

        acDateRegMonth.setKeyListener(null);
        acDateRegMonth.setText(monthSelected(detail.getValrepLandimpPropdescRegistryDateMonth()));

        etDateRegDay.setText(detail.getValrepLandimpPropdescRegistryDateDay());
        etDateRegYear.setText(detail.getValrepLandimpPropdescRegistryDateYear());

        DatePickerDialog datePickerDialog = setDialogDateWithAutoCompleteTextView(this,
                getInspectedOrValuationYear(Integer.valueOf(Objects.requireNonNull(etDateRegYear.getText()).toString().isEmpty()  ? "0" : etDateRegYear.getText().toString() )),
                getInspectedOrValuationMonth(Integer.valueOf(Objects.requireNonNull(acDateRegMonth.getText()).toString().isEmpty() || acDateRegMonth.getText().toString().equals("----") || acDateRegMonth.getText().toString().equals(String.valueOf(convertMonthWordToNumber(acDateRegMonth.getText().toString(), this))) || containsWordMonth(acDateRegMonth, this)  ? "0" : acDateRegMonth.getText().toString())), //-1
                getInspectedOrValuationDayOfMonth(Integer.valueOf(Objects.requireNonNull(etDateRegDay.getText()).toString().isEmpty() ? "0" : etDateRegDay.getText().toString())),
                etDateRegYear, acDateRegMonth, etDateRegDay);

        acDateRegMonth.setOnClickListener(v -> datePickerDialog.show());

        etRegOwner.setText(detail.getValrepLandimpPropdescRegisteredOwner());
        acRegDeeds.setText(detail.getValrepLandimpPropdescRegistryOfDeeds());
        acRegDeeds.setAdapter(registryDeedType(this, detail.getValrepLandimpPropdescRegistryOfDeeds()));
        acRegDeeds.setOnClickListener(v -> acRegDeeds.showDropDown());

        etTdSubmitted.setText(detail.getValrepLandimpTaxDecLotNo());
        acTdClassification.setText(detail.getValrepLandimpTaxDecClassification());
        acTdClassification.setKeyListener(null);
        acTdClassification.setAdapter(ownershipClassificationType(this, detail.getValrepLandimpTaxDecClassification()));
        acTdClassification.setOnClickListener(v -> acTdClassification.showDropDown());
    }

    @Override
    public void showExitDialog(String title, String message) {
        new AlertDialog.Builder(this).setCancelable(false).setTitle(title).setMessage(message).setPositiveButton("CLOSE", (dialog, which) -> finish()).create().show();
    }

    @Override
    public void finishActivity() {
        finish();
    }

    @Override
    public void showToastMessage(String message) {
        Toast.makeText(this, message, Toast.LENGTH_SHORT).show();
    }
}
