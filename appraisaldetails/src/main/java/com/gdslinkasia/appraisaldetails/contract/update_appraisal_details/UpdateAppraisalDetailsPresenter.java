package com.gdslinkasia.appraisaldetails.contract.update_appraisal_details;

import android.app.Application;
import android.os.Handler;
import android.util.Log;

import com.gdslinkasia.base.model.details.AppContactPerson;
import com.gdslinkasia.base.model.details.AppraisalRequest;
import com.gdslinkasia.base.model.job.Record;

import java.util.ArrayList;
import java.util.List;

import io.reactivex.Single;
import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.disposables.CompositeDisposable;
import io.reactivex.functions.Consumer;
import io.reactivex.observers.DisposableSingleObserver;
import io.reactivex.schedulers.Schedulers;

public class UpdateAppraisalDetailsPresenter implements UpdateAppraisalDetailContract.Presenter {

    private UpdateAppraisalDetailContract.View view;
    private UpdateAppraisalDetailContract.Interactor interactor;

    private CompositeDisposable compositeDisposable = new CompositeDisposable();

    public UpdateAppraisalDetailsPresenter(UpdateAppraisalDetailContract.View view, Application application) {
        this.view = view;
        this.interactor = new UpdateAppraisalDetailsInteractor(application);
    }


    @Override
    public void onStart(String recordId) {
        view.showProgress("Loading Record", "Please wait...");
        compositeDisposable.add(getLocalSingleObserver(recordId).subscribeWith(getLocalObserver()));
    }

    @Override
    public void onDestroy() {
        view = null;
        compositeDisposable.clear();
    }

    @Override
    public void updateAppraisalDetails(String recordId,
                                       String isCompany,
                                       String Title, String firstName, String middleName, String lastName,
                                       String companyName, String authOfficer,
                                       String clientDesignation, String clientAddress,
                                       String contactMobNo, String contactEmailAdd,
                                       String clientType,
                                       String intendedUsers, String otherIntendedUsers,
                                       String etInspectedDay, String etInspectedMonth, String etInspectedYear,
                                       String etValuationDay, String etValuationMonth, String etValuationYear,
                                       String acBasisOfValue, String etZonalValue,
                                       String etControlNo, String etPropertyType,
                                       String acNatureAppraisalType, String acPurposeOfValuation,
                                       String etAppNatureAppraisalOther,
                                       String etOwnerSubjProp) {
        compositeDisposable.add(getLocalSingleObserver(recordId).subscribe(
                getRecordUpdate(isCompany, Title, firstName, middleName, lastName,
                        companyName, authOfficer,
                        clientDesignation, clientAddress,
                        contactMobNo, contactEmailAdd,
                        clientType,
                        intendedUsers, otherIntendedUsers,
                        etInspectedDay, etInspectedMonth, etInspectedYear,
                        etValuationDay, etValuationMonth, etValuationYear,
                        acBasisOfValue, etZonalValue,
                        etControlNo, etPropertyType,
                        acNatureAppraisalType, acPurposeOfValuation,
                        etAppNatureAppraisalOther,
                        etOwnerSubjProp)));
    }

    //TODO Single disposable for fetching 1 Record from database based on recordId
    private Single<Record> getLocalSingleObserver(String recordId) {
        return interactor.getRecordById(recordId)
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread());
    }

    //TODO Results from disposable will be fetched here
    private DisposableSingleObserver<Record> getLocalObserver() {
        return new DisposableSingleObserver<Record>() {
            @Override
            public void onSuccess(Record record) {
                Log.d("int--LocalRepo", "Logic Triggered: UpdateAppraisalDetailsPresenter");

//                view.showDetails(record);

//                view.showSnackMessage("Loading Successful");

//                compositeDisposable.add(getAppraisalRequest(record.getUniqueId()).subscribeWith(getAppraisalRequestObserver()));

                compositeDisposable.add(interactor.getRecordWithAppraisalRequest(record.getUniqueId())
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(recordWithAppraisalRequest -> compositeDisposable.add(interactor.getAppraisalRequestChildObjects(recordWithAppraisalRequest.getAppraisalRequests().get(0).getUniqueId())
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(appraisalRequestWithChildObjects -> {
                    view.showDetails(recordWithAppraisalRequest.getRecord());

                    if (!appraisalRequestWithChildObjects.getAppContactPersonList().isEmpty()) {
                        view.showOtherContacts(appraisalRequestWithChildObjects.getAppContactPersonList());
                    } else {
                        view.showEmptyOtherContacts("There are no contacts available");
                    }

                    if (!appraisalRequestWithChildObjects.getAppCollateralAddresses().isEmpty()) {
                        view.showCollateralAddress(appraisalRequestWithChildObjects.getAppCollateralAddresses());
                    } else {
                        view.showNoCollateralAddress("There are address available");
                    }
                }, throwable -> {
                    throwable.printStackTrace();
                    view.showExitDialog("Something went wrong", throwable.getMessage());
                })), throwable -> {
                    throwable.printStackTrace();
                    view.showExitDialog("Something went wrong", throwable.getMessage());
                }));


//                view.hideProgress();
                new Handler().post(() -> view.hideProgress());
            }

            @Override
            public void onError(Throwable e) {
                view.hideProgress();
                view.showExitDialog("Record Error", e.getMessage());
            }
        };

    }

    //TODO Update Record with parameters passed from method updateRecord
    private Consumer<Record> getRecordUpdate(String cbCompany,
                                                             String title,
                                                             String firstName, String middleName, String lastName,
                                                             String companyName, String authOfficer,
                                                             String clientDesignation, String clientAddress,
                                                             String contactMobNo, String contactEmailAdd,
                                                             String clientType,
                                                             String intendedUsers, String otherIntendedUsers,
                                                             String etInspectedDay, String etInspectedMonth, String etInspectedYear,
                                                             String etValuationDay, String etValuationMonth, String etValuationYear,
                                                             String acBasisOfValue, String etZonalValue,
                                                             String etControlNo, String etPropertyType,
                                                             String acNatureAppraisalType, String acPurposeOfValuation,
                                                             String etAppNatureAppraisalOther,
                                                             String etOwnerSubjProp) {

        return record -> {
            Log.d("int--RecordFlowable", "Logic Triggered: UpdateAppraisalDetailsPresenter");
            record.setAppAccountCompany(cbCompany);
            record.setAppAccountNameTitle(title);
            record.setAppAccountFirstName(firstName);
            record.setAppAccountMiddleName(middleName);
            record.setAppAccountLastName(lastName);
            record.setAppAccountFirstName(companyName);
            record.setAppAccountAuthroizingOfficer(authOfficer);
            record.setAppAccountDesignation(clientDesignation);
            record.setAppAccountBusinessAddress(clientAddress);
            record.setAppAccountBusinessContact(contactMobNo);
            record.setAppAccountBusinessEmail(contactEmailAdd);
            record.setAppIntendedUsersName(clientType);
//                record.setAppIntendedUsersName(intendedUsers);
            record.setAppIntendedUsersOthers(otherIntendedUsers);
            record.setValrepLandimpDateInspectedDay(etInspectedDay);
            record.setValrepLandimpDateInspectedMonth(etInspectedMonth);
            record.setValrepLandimpDateInspectedYear(etInspectedYear);
            record.setValrepLandimpValuationDateMonth(etValuationMonth);
            record.setValrepAppraisalBasisOfValue(acBasisOfValue);
            record.setValrepLandimpZonalValue(etZonalValue);

            List<AppraisalRequest> appraisalRequests = new ArrayList<>();
            for (AppraisalRequest appraisalRequest : record.getAppraisalRequest()) {
                appraisalRequest.setAppControlNo(etControlNo);
                appraisalRequest.setAppRequestAppraisal(etPropertyType);
                appraisalRequest.setAppNatureAppraisal(acNatureAppraisalType);
                appraisalRequest.setAppPurposeAppraisal(acPurposeOfValuation);
                appraisalRequest.setAppPurposeAppraisalOther(etAppNatureAppraisalOther);

                appraisalRequests.add(appraisalRequest);
            }

            interactor.updateAppraisalRequests(appraisalRequests);

            record.setAppraisalRequest(appraisalRequests);

            record.setPafLandimpDeveloperName(etOwnerSubjProp);


            interactor.updateRecord(record);

            view.showSnackMessage("Saving Successful");

            new Handler().postDelayed(() -> view.hideProgress(), 2000);
        };
    }

    private Single<List<AppraisalRequest>> getAppraisalRequest(long recordUId) {
        return interactor.getAppraisalRequestByRecordUId(recordUId)
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread());
    }

    private DisposableSingleObserver<List<AppraisalRequest>> getAppraisalRequestObserver() {
        return new DisposableSingleObserver<List<AppraisalRequest>>() {
            @Override
            public void onSuccess(List<AppraisalRequest> appraisalRequests) {
                for (AppraisalRequest appraisalRequest : appraisalRequests) {
                    Log.d("int--RequestUId", String.valueOf(appraisalRequest.getUniqueId()));

                    List<AppContactPerson> contactPeople = appraisalRequest.getAppContactPerson();
                    if (contactPeople.isEmpty()) {
                        Log.d("int--EmptyContacts", "Logic Triggered");
                        view.showEmptyOtherContacts("There are no contacts available");
                    } else {
                        Log.d("int--NotEmptyContact", "Logic Triggered");
                        view.showOtherContacts(contactPeople);
                    }

                    if (appraisalRequest.getAppCollateralAddress().isEmpty()) {
                        view.showNoCollateralAddress("There are no address available");
                    } else {

                        view.showCollateralAddress(appraisalRequest.getAppCollateralAddress());
                    }
                }
            }

            @Override
            public void onError(Throwable e) {
                e.printStackTrace();
            }
        };
    }
}