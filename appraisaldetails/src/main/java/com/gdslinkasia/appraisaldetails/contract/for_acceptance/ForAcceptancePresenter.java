package com.gdslinkasia.appraisaldetails.contract.for_acceptance;

import android.app.Application;

import com.gdslinkasia.base.model.job.Record;

import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.disposables.CompositeDisposable;
import io.reactivex.observers.DisposableSingleObserver;
import io.reactivex.schedulers.Schedulers;

public class ForAcceptancePresenter implements ForAcceptanceContract.Presenter {

    private ForAcceptanceContract.View view;
    private ForAcceptanceContract.Interactor interactor;
    private CompositeDisposable compositeDisposable = new CompositeDisposable();

    public ForAcceptancePresenter(Application application) {
        this.interactor = new ForAcceptanceInteractor(application);
    }

    @Override
    public void requestAddresses(String recordId) {
        compositeDisposable.add(interactor.getRecordById(recordId)
        .subscribeOn(Schedulers.io())
        .observeOn(AndroidSchedulers.mainThread())
        .subscribeWith(new DisposableSingleObserver<Record>() {
            @Override
            public void onSuccess(Record record) {
                compositeDisposable.add(interactor.getRecordWithAppraisalRequest(record.getUniqueId())
                        .subscribeOn(Schedulers.io())
                        .observeOn(AndroidSchedulers.mainThread())
                        .subscribe(recordWithAppraisalRequest -> compositeDisposable.add(interactor.getAppraisalRequestChildObjects(recordWithAppraisalRequest.getAppraisalRequests().get(0).getUniqueId())
                                .subscribeOn(Schedulers.io())
                                .observeOn(AndroidSchedulers.mainThread())
                                .subscribe(appraisalRequestWithChildObjects -> {

                                    if (!appraisalRequestWithChildObjects.getAppCollateralAddresses().isEmpty()) {
                                        view.showCollateralAddress(appraisalRequestWithChildObjects.getAppCollateralAddresses());
                                    } else {
                                        view.showNoCollateralAddress("There are no address available");
                                    }
                                }, Throwable::printStackTrace)), Throwable::printStackTrace));
            }

            @Override
            public void onError(Throwable e) {
                e.printStackTrace();
            }
        }));
    }

    @Override
    public void takeView(ForAcceptanceContract.View view) {
        this.view = view;
    }

    @Override
    public void dropView() {
        view = null;
        compositeDisposable.clear();
    }
}
