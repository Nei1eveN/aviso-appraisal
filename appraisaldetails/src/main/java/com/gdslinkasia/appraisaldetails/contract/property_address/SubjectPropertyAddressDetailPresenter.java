package com.gdslinkasia.appraisaldetails.contract.property_address;

import android.app.Application;
import android.os.Handler;
import android.util.Log;

import com.gdslinkasia.base.model.details.AppCollateralAddress;

import io.reactivex.Single;
import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.disposables.CompositeDisposable;
import io.reactivex.observers.DisposableSingleObserver;
import io.reactivex.schedulers.Schedulers;

public class SubjectPropertyAddressDetailPresenter implements SubjectPropertyAddressDetailContract.Presenter {

    private SubjectPropertyAddressDetailContract.View view;
    private SubjectPropertyAddressDetailContract.Interactor interactor;

    private CompositeDisposable compositeDisposable = new CompositeDisposable();

    public SubjectPropertyAddressDetailPresenter(SubjectPropertyAddressDetailContract.View view, Application application) {
        this.view = view;
        this.interactor = new SubjectPropertyAddressDetailInteractor(application);
    }

    @Override
    public void onStart(long uniqueId) {
        view.showProgress("Loading Record", "Please wait...");
        compositeDisposable.add(getLocalSingleObserver(uniqueId).subscribeWith(getLocalObserver()));
    }

    @Override
    public void onDestroy() {
        view = null;
        compositeDisposable.clear();
    }

    @Override
    public void updateDetails(long uniqueId,
                              String subjPropType,
                              String etLotNo, String etBlockNo, String etStreetName, String etVillage,
                              String etBrgy, String etZipCode, String etDistrict, String etCity,
                              String etProvince, String etRegion, String etCountry) {
//        view.showProgress("Saving Record", "Please wait...");
        compositeDisposable.add(getLocalSingleObserver(uniqueId).subscribeWith(getRecordUpdate(subjPropType, etLotNo, etBlockNo, etStreetName, etVillage, etBrgy, etZipCode, etDistrict, etCity, etProvince, etRegion, etCountry)));
    }

    private Single<AppCollateralAddress> getLocalSingleObserver(long uniqueId) {
        return interactor.getRecordByRowId(uniqueId)
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread());
    }

    //TODO Results from disposable will be fetched here
    private DisposableSingleObserver<AppCollateralAddress> getLocalObserver() {
        return new DisposableSingleObserver<AppCollateralAddress>() {
            @Override
            public void onSuccess(AppCollateralAddress address) {
                Log.d("int--LocalRepo", "Logic Triggered: UpdateAppraisalDetailsPresenter");

                view.showDetails(address);

                view.showSnackMessage("Loading Successful");

                new Handler().post(() -> view.hideProgress());
            }

            @Override
            public void onError(Throwable e) {
                e.printStackTrace();
                view.hideProgress();
                view.showExitDialog("Record Error", e.getMessage());
            }
        };
    }

    //TODO Update Record with parameters passed from method updateValrepLandimpImpDetail
    private DisposableSingleObserver<AppCollateralAddress> getRecordUpdate(String subjPropType,
                                                                           String etLotNo, String etBlockNo, String etStreetName, String etVillage,
                                                                           String etBrgy, String etZipCode, String etDistrict, String etCity,
                                                                           String etProvince, String etRegion, String etCountry) {
        return new DisposableSingleObserver<AppCollateralAddress>() {
            @Override
            public void onSuccess(AppCollateralAddress address) {
                Log.d("int--RecordFlowable", "Logic Triggered: "+ SubjectPropertyAddressDetailPresenter.class.getCanonicalName());
                address.setAppAddressType(subjPropType);
                address.setAppLotNo(etLotNo);
                address.setAppBlockNo(etBlockNo);
                address.setAppStreetName(etStreetName);
                address.setAppVillage(etVillage);
                address.setAppBrgy(etBrgy);
                address.setAppZip(etZipCode);
                address.setAppDistrict(etDistrict);
                address.setAppCity(etCity);
                address.setAppProvince(etProvince);
                address.setAppRegion(etRegion);
                address.setAppCountry(etCountry);

                interactor.updateDetails(address);

                view.showSnackMessage("Saving Successful");

//                new Handler().postDelayed(() -> view.hideProgress(), 2000);
            }

            @Override
            public void onError(Throwable e) {
                e.printStackTrace();
                view.hideProgress();
                view.showExitDialog("Record Error", e.getMessage());
            }
        };
    }
}
