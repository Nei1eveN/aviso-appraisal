package com.gdslinkasia.appraisaldetails.contract.jobdetails;

import android.app.Application;

import com.gdslinkasia.base.database.repository.JobDetailsLocalRepository;
import com.gdslinkasia.base.database.repository.JobDetailsRemoteRepository;
import com.gdslinkasia.base.model.*;
import com.gdslinkasia.base.model.details.*;
import com.gdslinkasia.base.model.job.*;

import org.json.JSONException;

import java.util.List;

import io.reactivex.Flowable;
import io.reactivex.Single;

public class JobDetailInteractor implements JobDetailContract.Interactor {

    private JobDetailsLocalRepository localRepository;
    private JobDetailsRemoteRepository remoteRepository;

    public JobDetailInteractor(Application application) {
        this.localRepository = new JobDetailsLocalRepository(application);
        this.remoteRepository = new JobDetailsRemoteRepository(application);
    }

    @Override
    public Single<RecordIDPreference> getRecordByPreference(String recordId) {
        return localRepository.getRecordIdByPreference(recordId);
    }

    @Override
    public void insertRecordPreference(RecordIDPreference recordIDPreference) {
        localRepository.insertRecordPreference(recordIDPreference);
    }

    @Override
    public Single<Record> getLocalDetails(String recordId) {
        return localRepository.getRecordById(recordId);
    }

    @Override
    public Single<Record> getRemoteDetails(String record) throws JSONException {
        return remoteRepository.getRecordById(record);
    }

    @Override
    public Flowable<JobSubmitResult> saveDataToServer(Record record) throws JSONException {
        return remoteRepository.saveDataToServer(record);
    }

    @Override
    public Single<List<AppraisalRequest>> getAppraisalRequestsByRecordId(String recordId) {
        return localRepository.getAppraisalRequestByRecordId(recordId);
    }

    @Override
    public Single<List<AppraisalRequest>> getAppraisalRequests(long recordUId) {
        return localRepository.getAppraisalRequestByRecordUId(recordUId);
    }

    @Override
    public Single<List<AppAttachment>> getAttachmentsByAppReqId(long appraisalRequestId, String createdFrom) {
        return localRepository.getAttachmentsByAppReqId(appraisalRequestId, createdFrom);
    }

    @Override
    public Single<List<ValrepLandimpAssumptionsRemark>> getRemarks(String recordId) {
        return localRepository.getAssumptionsRemarksByRecordId(recordId);
    }

    @Override
    public Single<List<ValrepLandimpImpDetail>> getLandimpDetails(String recordId) {
        return localRepository.getValrepLandimpImpDetails(recordId);
    }

    @Override
    public Single<List<ValrepLandimpImpValuation>> getLandimpValuations(String recordId) {
        return localRepository.getLandimpImpValuations(recordId);
    }

    @Override
    public Single<List<ValrepLandimpLotDetail>> getLotDetails(String recordId) {
        return localRepository.getValrepLandimpLotDetails(recordId);
    }

    @Override
    public Single<List<ValrepLandimpLotValuation>> getLotValuations(String recordId) {
        return localRepository.getLotValuations(recordId);
    }

    @Override
    public Single<List<ValrepLandimpOtherLandImpDetail>> getOtherLandimpDetails(String recordId) {
        return localRepository.getValrepLandimpOtherLandImpDetails(recordId);
    }

    @Override
    public Single<List<ValrepLandimpOtherLandImpValue>> getOtherLandimpValues(String recordId) {
        return localRepository.getValrepLandimpOtherLandImpValue(recordId);
    }

    @Override
    public Single<List<ValrepLandimpParkingValuationDetail>> getLandimpParkingValuationDetails(String recordId) {
        return localRepository.getValrepLandimpParkingValuationDetails(recordId);
    }

    @Override
    public Single<List<ValrepLcIvsQualifyingStatement>> getQualifyingStatements(String recordId) {
        return localRepository.getValrepLcIvsQualifyingStatements(recordId);
    }

    @Override
    public void deleteAllAttachments(List<AppAttachment> appAttachments) {
        localRepository.deleteAllAttachments(appAttachments);
    }

    @Override
    public void onDestroy() {
        localRepository.onDestroy();
    }

    @Override
    public void updateRecord(Record record) {
        localRepository.updateRecord(record);
    }


//    @Override
//    public Single<Record> getSingleResultLocalDetails(String recordId) {
//        return localRepository.getRecordById(recordId);
//    }
}
