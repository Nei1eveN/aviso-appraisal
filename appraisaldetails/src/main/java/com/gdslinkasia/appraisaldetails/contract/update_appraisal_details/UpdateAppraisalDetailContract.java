package com.gdslinkasia.appraisaldetails.contract.update_appraisal_details;

import com.gdslinkasia.base.database.dao.AppraisalRequestWithChildObjects;
import com.gdslinkasia.base.database.dao.RecordWithAppraisalRequest;
import com.gdslinkasia.base.model.details.*;
import com.gdslinkasia.base.model.job.Record;

import java.util.List;

import io.reactivex.Flowable;
import io.reactivex.Single;

public interface UpdateAppraisalDetailContract {
    interface View {
        void showProgress(String title, String message);

        void hideProgress();

        void showSnackMessage(String message);

        void showDetails(Record record);

        void showExitDialog(String title, String message);

        void showErrorDialog(String title, String message);

        void showOtherContacts(List<AppContactPerson> contactPersonList);

        void showCollateralAddress(List<AppCollateralAddress> collateralAddresses);

        void showEmptyOtherContacts(String message);

        void showNoCollateralAddress(String message);
    }

    interface Presenter {
        void onStart(String recordId);

        void onDestroy();

        void updateAppraisalDetails(String recordId,
                                    String isCompany, String Title, String firstName, String middleName, String lastName,
                                    String companyName, String authOfficer,
                                    String clientDesignation, String clientAddress,
                                    String contactMobNo, String contactEmailAdd,
                                    String clientType, String intendedUsers, String otherIntendedUsers,
                                    String etInspectedDay, String etInspectedMonth, String etInspectedYear,
                                    String etValuationDay, String etValuationMonth, String etValuationYear,
                                    String acBasisOfValue, String etZonalValue,
                                    String etControlNo, String etPropertyType,
                                    String acNatureAppraisalType, String acPurposeOfValuation,
                                    String etAppNatureAppraisalOther, String etOwnerSubjProp);
    }

    interface Interactor {
        Single<RecordWithAppraisalRequest> getRecordWithAppraisalRequest(long recordUid);

        Flowable<AppraisalRequestWithChildObjects> getAppraisalRequestChildObjects(long appraisalRequestId);

        Single<Record> getRecordById(String recordId);

        Flowable<Record> getFlowableRecordById(String recordId);

        void updateRecord(Record record);

        Single<List<AppraisalRequest>> getAppraisalRequestByRecordUId(long recordUId);

        void updateAppraisalRequests(List<AppraisalRequest> appraisalRequests);
    }
}
