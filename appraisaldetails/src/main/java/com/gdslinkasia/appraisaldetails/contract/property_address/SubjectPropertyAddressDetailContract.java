package com.gdslinkasia.appraisaldetails.contract.property_address;

import com.gdslinkasia.base.model.details.AppCollateralAddress;

import io.reactivex.Single;

public interface SubjectPropertyAddressDetailContract {
    interface View {
        void showProgress(String title, String message);

        void hideProgress();

        void showSnackMessage(String message);

//        void showDetails(Record record);

        void showDetails(AppCollateralAddress address);

        void showExitDialog(String title, String message);

        void showErrorDialog(String title, String message);

//        void showOtherContacts(List<AppContactPerson> contactPersonList);

//        void showCollateralAddress(List<AppCollateralAddress> collateralAddresses);

//        void showEmptyOtherContacts(String message);

//        void showNoCollateralAddress(String message);
    }

    interface Presenter {
//        void onResume(String recordId);

        void onStart(long uniqueId);

        void onDestroy();

        void updateDetails(long uniqueId,
                           String subjPropType, String etLotNo, String etBlockNo,
                           String etStreetName, String etVillage,
                           String etBrgy, String etZipCode, String etDistrict, String etCity,
                           String etProvince, String etRegion, String etCountry);
    }

    interface Interactor {
        Single<AppCollateralAddress> getRecordByRowId(long uniqueId);

        void updateDetails(AppCollateralAddress address);
    }
}
