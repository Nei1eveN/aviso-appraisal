package com.gdslinkasia.appraisaldetails.contract.update_appraisal_details;

import android.app.Application;

import com.gdslinkasia.base.database.dao.AppraisalRequestWithChildObjects;
import com.gdslinkasia.base.database.dao.RecordWithAppraisalRequest;
import com.gdslinkasia.base.database.repository.JobDetailsLocalRepository;
import com.gdslinkasia.base.model.details.AppraisalRequest;
import com.gdslinkasia.base.model.job.Record;

import java.util.List;

import io.reactivex.Flowable;
import io.reactivex.Single;

class UpdateAppraisalDetailsInteractor implements UpdateAppraisalDetailContract.Interactor {

    private JobDetailsLocalRepository localRepository;

    public UpdateAppraisalDetailsInteractor(Application application) {
        localRepository = new JobDetailsLocalRepository(application);
    }

    @Override
    public Single<RecordWithAppraisalRequest> getRecordWithAppraisalRequest(long recordUid) {
        return localRepository.getRecordWithAppraisalRequest(recordUid);
    }

    @Override
    public Flowable<AppraisalRequestWithChildObjects> getAppraisalRequestChildObjects(long appraisalRequestId) {
        return localRepository.getAppraisalRequestChildObjects(appraisalRequestId);
    }

    @Override
    public Single<Record> getRecordById(String recordId) {
        return localRepository.getRecordById(recordId);
    }

    @Override
    public Flowable<Record> getFlowableRecordById(String recordId) {
        return localRepository.getFlowableRecord(recordId);
    }

    @Override
    public void updateRecord(Record record) {
        localRepository.updateRecord(record);
    }

    @Override
    public Single<List<AppraisalRequest>> getAppraisalRequestByRecordUId(long appraisalRequestId) {
        return localRepository.getAppraisalRequestByRecordUId(appraisalRequestId);
    }

    @Override
    public void updateAppraisalRequests(List<AppraisalRequest> appraisalRequests) {
        localRepository.updateAppraisalRequests(appraisalRequests);
    }
}
