package com.gdslinkasia.appraisaldetails.contract.for_acceptance;

import android.app.Application;

import com.gdslinkasia.base.database.dao.AppraisalRequestWithChildObjects;
import com.gdslinkasia.base.database.dao.RecordWithAppraisalRequest;
import com.gdslinkasia.base.database.repository.JobDetailsLocalRepository;
import com.gdslinkasia.base.model.job.Record;

import io.reactivex.Flowable;
import io.reactivex.Single;

public class ForAcceptanceInteractor implements ForAcceptanceContract.Interactor {

    private JobDetailsLocalRepository localRepository;

    public ForAcceptanceInteractor(Application application) {
        localRepository = new JobDetailsLocalRepository(application);
    }

    @Override
    public Single<Record> getRecordById(String recordId) {
        return localRepository.getRecordById(recordId);
    }

    @Override
    public Single<RecordWithAppraisalRequest> getRecordWithAppraisalRequest(long recordUid) {
        return localRepository.getRecordWithAppraisalRequest(recordUid);
    }

    @Override
    public Flowable<AppraisalRequestWithChildObjects> getAppraisalRequestChildObjects(long appraisalRequestId) {
        return localRepository.getAppraisalRequestChildObjects(appraisalRequestId);
    }
}
