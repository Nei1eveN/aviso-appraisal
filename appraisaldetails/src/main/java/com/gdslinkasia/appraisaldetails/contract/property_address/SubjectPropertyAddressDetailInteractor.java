package com.gdslinkasia.appraisaldetails.contract.property_address;

import android.app.Application;

import com.gdslinkasia.base.database.repository.JobDetailsLocalRepository;
import com.gdslinkasia.base.model.details.AppCollateralAddress;

import io.reactivex.Single;

public class SubjectPropertyAddressDetailInteractor implements SubjectPropertyAddressDetailContract.Interactor {

    private JobDetailsLocalRepository localRepository;

    public SubjectPropertyAddressDetailInteractor(Application application) {
        localRepository = new JobDetailsLocalRepository(application);
    }

    public Single<AppCollateralAddress> getRecordByRowId(long uniqueId) {
        return localRepository.getCollateralAddress(uniqueId);
    }

    @Override
    public void updateDetails(AppCollateralAddress address) {
        localRepository.updateCollateralAddress(address);
    }
}
