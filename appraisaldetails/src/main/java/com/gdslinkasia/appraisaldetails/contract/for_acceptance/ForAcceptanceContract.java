package com.gdslinkasia.appraisaldetails.contract.for_acceptance;

import com.gdslinkasia.base.BasePresenter;
import com.gdslinkasia.base.BaseView;
import com.gdslinkasia.base.database.dao.AppraisalRequestWithChildObjects;
import com.gdslinkasia.base.database.dao.RecordWithAppraisalRequest;
import com.gdslinkasia.base.model.details.AppCollateralAddress;
import com.gdslinkasia.base.model.job.Record;

import java.util.List;

import io.reactivex.Flowable;
import io.reactivex.Single;

public interface ForAcceptanceContract {

    interface View extends BaseView<ForAcceptanceContract.Presenter> {
        void showCollateralAddress(List<AppCollateralAddress> appCollateralAddresses);
        void showNoCollateralAddress(String message);
    }

    interface Presenter extends BasePresenter<ForAcceptanceContract.View> {
        void requestAddresses(String recordId);
    }

    interface Interactor {
        Single<Record> getRecordById(String recordId);

        Single<RecordWithAppraisalRequest> getRecordWithAppraisalRequest(long recordUid);

        Flowable<AppraisalRequestWithChildObjects> getAppraisalRequestChildObjects(long appraisalRequestId);
    }

}
