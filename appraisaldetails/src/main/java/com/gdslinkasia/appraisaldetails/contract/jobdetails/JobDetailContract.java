package com.gdslinkasia.appraisaldetails.contract.jobdetails;

import com.gdslinkasia.base.model.RecordIDPreference;
import com.gdslinkasia.base.model.details.*;
import com.gdslinkasia.base.model.job.JobSubmitResult;
import com.gdslinkasia.base.model.job.Record;

import org.json.JSONException;

import java.util.List;

import io.reactivex.Flowable;
import io.reactivex.Single;

public interface JobDetailContract {
    interface View {
        void showProgress(String title, String message);

        void hideProgress();

        void showSnackMessage(String message);

        void showJobDetails(Record record);

        void showSyncOptionDialog(String title, String message);

        void showExitDialog(String title, String message);
    }

    interface Presenter {
        void onStart(String recordId);

        void requestFirstTimeSyncJobDetails(String recordId);

        void requestJobDetails(String recordId);

        void saveDetailsToServer(String recordId, String buttonText, String applicationStatus);

        void onDestroy();
    }

    interface Interactor {
        Single<RecordIDPreference> getRecordByPreference(String recordId);

        void insertRecordPreference(RecordIDPreference recordIDPreference);

        Single<Record> getLocalDetails(String recordId);

        Single<Record> getRemoteDetails(String recordId) throws JSONException;

        Flowable<JobSubmitResult> saveDataToServer(Record record) throws JSONException;

        //TODO SINGLE FETCHING FOR RETURNING EMPTY RESULT IF RESULT IS NULL OR EMPTY
        //Single<Record> getSingleResultLocalDetails(String recordId);

        Single<List<AppraisalRequest>> getAppraisalRequestsByRecordId(String recordId);

        Single<List<AppraisalRequest>> getAppraisalRequests(long recordUId);

        Single<List<AppAttachment>> getAttachmentsByAppReqId(long appraisalRequestId, String createdFrom);

        Single<List<ValrepLandimpAssumptionsRemark>> getRemarks(String recordId);

        Single<List<ValrepLandimpImpDetail>> getLandimpDetails(String recordId);

        Single<List<ValrepLandimpImpValuation>> getLandimpValuations(String recordId);

        Single<List<ValrepLandimpLotDetail>> getLotDetails(String recordId);

        Single<List<ValrepLandimpLotValuation>> getLotValuations(String recordId);

        Single<List<ValrepLandimpOtherLandImpDetail>> getOtherLandimpDetails(String recordId);

        Single<List<ValrepLandimpOtherLandImpValue>> getOtherLandimpValues(String recordId);

        Single<List<ValrepLandimpParkingValuationDetail>> getLandimpParkingValuationDetails(String recordId);

        Single<List<ValrepLcIvsQualifyingStatement>> getQualifyingStatements(String recordId);

        void deleteAllAttachments(List<AppAttachment> appAttachments);

        void onDestroy();

        void updateRecord(Record record);
    }
}
