package com.gdslinkasia.appraisaldetails.contract.jobdetails;

import android.app.Application;
import android.os.Environment;
import android.os.Handler;
import android.util.Log;

import androidx.room.EmptyResultSetException;

import com.androidnetworking.AndroidNetworking;
import com.androidnetworking.common.Priority;
import com.androidnetworking.error.ANError;
import com.androidnetworking.interfaces.JSONObjectRequestListener;
import com.gdslinkasia.base.model.RecordIDPreference;
import com.gdslinkasia.base.model.details.AppAttachment;
import com.gdslinkasia.base.model.details.AppraisalRequest;
import com.gdslinkasia.base.model.details.JobSystem;
import com.gdslinkasia.base.model.job.JobSubmitResult;
import com.gdslinkasia.base.model.job.Record;
import com.gdslinkasia.base.utils.GlobalString;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.File;
import java.util.List;

import io.reactivex.Flowable;
import io.reactivex.Single;
import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.disposables.CompositeDisposable;
import io.reactivex.functions.Consumer;
import io.reactivex.observers.DisposableSingleObserver;
import io.reactivex.schedulers.Schedulers;

import static com.gdslinkasia.base.utils.GlobalFunctions.INSERT_APPRAISAL_ATTACHMENTS_JSON_BUILDER;
import static com.gdslinkasia.base.utils.GlobalString.SENTENCE_TO_APPRAISAL_TYPE_CHANGER;
import static com.gdslinkasia.base.utils.GlobalString.ATTACHMENTS_DIRECTORY;

public class JobDetailPresenter implements JobDetailContract.Presenter {

    private JobDetailContract.View view;
    private JobDetailContract.Interactor interactor;
    private CompositeDisposable compositeDisposable = new CompositeDisposable();

    private String recordId;

    public JobDetailPresenter(JobDetailContract.View view, Application application) {
        this.view = view;

        interactor = new JobDetailInteractor(application);
    }

    @Override
    public void onStart(String recordId) {
        this.recordId = recordId;
        compositeDisposable.add(getLocalRecordPreference(recordId).subscribeWith(getPreferenceObserver()));
    }

    @Override
    public void requestFirstTimeSyncJobDetails(String recordId) {
        view.showProgress("Syncing Job Details", "Please wait...");
        try {
            Log.d("int--sessionMgr", "Session Load Count Changed.");

            compositeDisposable.add(getRemoteSingleObserver(recordId)
                    .subscribeWith(getRemoteObserver()));
        } catch (JSONException e) {
            e.printStackTrace();
        }
    }

    @Override
    public void requestJobDetails(String recordId) {
        view.showProgress("Syncing Job Details", "Please wait...");
        try {
            compositeDisposable.add(getRemoteSingleObserver(recordId).subscribeWith(getRemoteObserver()));
        } catch (JSONException e) {
            e.printStackTrace();
        }
    }

    @Override
    public void saveDetailsToServer(String recordId, String buttonText, String applicationStatus) {
        view.showProgress("Saving Job Details", "Please wait...");
        compositeDisposable.add(getLocalSingleObserver(recordId).subscribeWith(getLocalDataSaveToServer(buttonText, applicationStatus)));
    }

    @Override
    public void onDestroy() {
        view = null;
        compositeDisposable.clear();
        interactor.onDestroy();
        Log.d("int--onDestroy", "Logic Triggered: " + JobDetailPresenter.class.getSimpleName());
    }

    private Single<RecordIDPreference> getLocalRecordPreference(String recordId) {
        return interactor.getRecordByPreference(recordId)
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread());
    }

    private DisposableSingleObserver<RecordIDPreference> getPreferenceObserver() {
        return new DisposableSingleObserver<RecordIDPreference>() {
            @Override
            public void onSuccess(RecordIDPreference recordIDPreference) {
                view.showProgress("Loading Job Details", "Please wait...");
                compositeDisposable.add(getLocalSingleObserver(recordIDPreference.getRecordId()).subscribeWith(getLocalObserver()));
            }

            @Override
            public void onError(Throwable e) {
                if (e instanceof EmptyResultSetException) {
                    view.showSyncOptionDialog("Job Details To Be Synced", "It seems that this record is not yet synced to your device." +
                            "\n\n" +
                            "Do you want to perform sync?" +
                            "\n\n" +
                            "Note: Your device must be connected to internet.");
                } else {
                    view.showExitDialog("Something Went Wrong", e.getMessage());
                }
            }
        };
    }

    private Single<Record> getRemoteSingleObserver(String recordId) throws JSONException {
        return interactor.getRemoteDetails(recordId)
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread());
    }

    private DisposableSingleObserver<Record> getRemoteObserver() {
        return new DisposableSingleObserver<Record>() {
            @Override
            public void onSuccess(Record record) {
                view.showJobDetails(record);
                view.showSnackMessage("Sync Successful");
                new Handler().postDelayed(() -> view.hideProgress(), 1000);
//                JobDetailPresenter.this.onStart(record.getRecordId());
            }

            @Override
            public void onError(Throwable e) {
                view.hideProgress();
                view.showExitDialog("Record Error", e.getMessage());
            }
        };
    }

    private Single<Record> getLocalSingleObserver(String recordId) {
        return interactor.getLocalDetails(recordId)
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread());
    }

    private DisposableSingleObserver<Record> getLocalObserver() {
        return new DisposableSingleObserver<Record>() {
            @Override
            public void onSuccess(Record record) {
                Log.d("int--LocalRepo", "Logic Triggered: JobDetailPresenter");
                view.showJobDetails(record);
                view.showSnackMessage("Loading Successful");
                new Handler().postDelayed(() -> view.hideProgress(), 1000);
            }

            @Override
            public void onError(Throwable e) {
                e.printStackTrace();
                view.hideProgress();
                view.showExitDialog("Record Error", e.getMessage());
            }
        };
    }

    //TODO SAVE DATA TO SERVER
    private DisposableSingleObserver<Record> getLocalDataSaveToServer(String buttonText, String applicationStatus) {
        return new DisposableSingleObserver<Record>() {
            @Override
            public void onSuccess(Record record) {
                compositeDisposable.add(interactor.getAppraisalRequests(record.getUniqueId()).subscribeOn(Schedulers.io()).subscribe(appraisalRequests -> {
                    Log.d("int--ApprSize1", String.valueOf(appraisalRequests.size()));
                    if (appraisalRequests.size() > 1) {
                        appraisalRequests.remove(0);
                    }
                    Log.d("int--ApprSize2", String.valueOf(appraisalRequests.size()));
                    record.setAppraisalRequest(appraisalRequests);
                    Log.d("int--addAppReq", "Logic Triggered");

                    compositeDisposable.add(interactor.getRemarks(record.getRecordId()).subscribeOn(Schedulers.io()).subscribe(valrepLandimpAssumptionsRemarks -> {
                        record.setValrepLandimpAssumptionsRemarks(valrepLandimpAssumptionsRemarks);
                        Log.d("int--addRemarks", "Logic Triggered");

                        compositeDisposable.add(interactor.getLandimpDetails(record.getRecordId()).subscribeOn(Schedulers.io()).subscribe(valrepLandimpImpDetails -> {
                            record.setValrepLandimpImpDetails(valrepLandimpImpDetails);
                            Log.d("int--addLandimpImpDet", "Logic Triggered");

                            compositeDisposable.add(interactor.getLandimpValuations(record.getRecordId()).subscribeOn(Schedulers.io()).subscribe(valrepLandimpImpValuations -> {
                                record.setValrepLandimpImpValuation(valrepLandimpImpValuations);
                                Log.d("int--addLandimpImpVal", "Logic Triggered");

                                compositeDisposable.add(interactor.getLotDetails(record.getRecordId()).subscribeOn(Schedulers.io()).subscribe(valrepLandimpLotDetails -> {
                                    record.setValrepLandimpLotDetails(valrepLandimpLotDetails);
                                    Log.d("int--addLandimpLotDet", "Logic Triggered");

                                    compositeDisposable.add(interactor.getLotValuations(record.getRecordId()).subscribeOn(Schedulers.io()).subscribe(valrepLandimpLotValuations -> {
                                        record.setValrepLandimpLotValuation(valrepLandimpLotValuations);
                                        Log.d("int--addLandimpLotVal", "Logic Triggered");

                                        compositeDisposable.add(interactor.getOtherLandimpDetails(record.getRecordId()).subscribeOn(Schedulers.io()).subscribe(valrepLandimpOtherLandImpDetails -> {
                                            record.setValrepLandimpOtherLandImpDetails(valrepLandimpOtherLandImpDetails);
                                            Log.d("int--addOtherLandimpDet", "Logic Triggered");

                                            compositeDisposable.add(interactor.getOtherLandimpValues(record.getRecordId()).subscribeOn(Schedulers.io()).subscribe(valrepLandimpOtherLandImpValues -> {
                                                record.setValrepLandimpOtherLandImpValue(valrepLandimpOtherLandImpValues);
                                                Log.d("int--addOtherLandimpVal", "Logic Triggered");

                                                compositeDisposable.add(interactor.getLandimpParkingValuationDetails(record.getRecordId()).subscribeOn(Schedulers.io()).subscribe(valrepLandimpParkingValuationDetails -> {
                                                    record.setValrepLandimpParkingValuationDetails(valrepLandimpParkingValuationDetails);
                                                    Log.d("int--addParkValDet", "Logic Triggered");

                                                    compositeDisposable.add(interactor.getQualifyingStatements(record.getRecordId()).subscribeOn(Schedulers.io()).subscribe(valrepLcIvsQualifyingStatements -> {
                                                        record.setValrepLcIvsQualifyingStatements(valrepLcIvsQualifyingStatements);
                                                        Log.d("int--addQualifyingStat", "Logic Triggered");

                                                        //TODO CHANGE APPLICATION STATUS BASED ON BUTTON TEXT
                                                        if (buttonText.equals("Submit For Review") || buttonText.equals("Accept Job") || buttonText.equals("Set As Active Job")) {
                                                            Log.d("int--applicationStatus", "Logic Triggered: " + applicationStatus);
                                                            record.setApplicationStatus(applicationStatus);
                                                        }

                                                        compositeDisposable.add(getSaveRemoteSingleObserver(record).subscribe(getSaveRemoteSuccess(buttonText, applicationStatus), getSaveRemoteError()));
                                                    }));
                                                }));
                                            }));
                                        }));
                                    }));
                                }));
                            }));
                        }));
                    }));
                }));
//                    compositeDisposable.add(getSaveRemoteSingleObserver(record).subscribe(getSaveRemoteSuccess(), getSaveRemoteError()));
            }

            @Override
            public void onError(Throwable e) {
                new Handler().postDelayed(() -> view.hideProgress(), 1000);
                view.showExitDialog("Sync Error", e.getMessage());
            }
        };
    }

    //TODO CHANGE APPLICATION STATUS TO SERVER

    private Flowable<JobSubmitResult> getSaveRemoteSingleObserver(Record record) throws JSONException {
        return interactor.saveDataToServer(record)
                .subscribeOn(Schedulers.computation())
                .observeOn(AndroidSchedulers.mainThread())
                ;
    }

    private Consumer<JobSubmitResult> getSaveRemoteSuccess(String buttonText, String applicationStatus) {
        return jobRecordResult -> compositeDisposable.add(interactor.getAppraisalRequestsByRecordId(recordId)
                .subscribeOn(Schedulers.io())
                .subscribe(appraisalRequests -> {
                    Log.d("int--finalApprSize1", String.valueOf(appraisalRequests.size()));
                    if (appraisalRequests.size() > 1) {
                        appraisalRequests.remove(0);
                    }
                    Log.d("int--finalApprSize2", String.valueOf(appraisalRequests.size()));

                    if (buttonText.equals("Submit For Review") || buttonText.equals("Accept Job") || buttonText.equals("Set As Rework")) {
                        JobSystem jobSystem = jobRecordResult.getRecord().getJobSystem();

                        compositeDisposable.add(
                                interactor.getLocalDetails(jobSystem.getRecordId())
                                        .subscribeOn(Schedulers.io())
                                        .doOnSuccess(record -> {
                                            record.setApplicationStatus(applicationStatus);

                                            interactor.updateRecord(record);

                                            Log.d("int--ChangeStatus", "onSuccess: Logic Triggered");
                                        })
                                        .doOnError(Throwable::printStackTrace)
                                        .subscribe());
                    }

                    for (AppraisalRequest appraisalRequest : appraisalRequests) {
                        compositeDisposable.add(interactor.getAttachmentsByAppReqId(appraisalRequest.getUniqueId(), "Local")
                                .subscribeOn(Schedulers.io())
                                .observeOn(AndroidSchedulers.mainThread())
                                .subscribe(appAttachments -> {
                                    if (appAttachments.isEmpty()) {
                                        Log.d("int--emptyAttachment", "Logic Triggered");
                                        view.showExitDialog("Saving Successful", "Job Details has been sent to the server.\n\nSaving Complete.");
                                        view.showSnackMessage("Saving Successful");


                                        new Handler().postDelayed(() -> view.hideProgress(), 1000);
                                    } else {
                                        Log.d("int--notEmptyAttachment", "Logic Triggered");
                                        for (int i = 0; i < appAttachments.size(); i++) {
                                            AppAttachment attachment = appAttachments.get(i);
                                            if (!attachment.getAppAttachmentFilename().equals("null")) {
                                                File file = new File(Environment.getExternalStorageDirectory() + "/" + ATTACHMENTS_DIRECTORY + "/" + attachment.getAppAttachmentFilename());

                                                if (file.exists()) {

                                                    uploadPhysicalFile(i, appAttachments.size(), attachment, appAttachments, file);
                                                } else {
                                                    if (i == (appAttachments.size() - 1)) {
                                                        view.showExitDialog("Saving Successful", "Job Details has been sent to the server.\n\nSaving Complete."); //However, there are no created attachments found.
                                                        view.showSnackMessage("Saving Successful");
                                                        new Handler().postDelayed(() -> view.hideProgress(), 1000);
                                                    }
                                                }
                                            }
                                        }
                                    }
                                }, throwable -> {
                                    if (throwable instanceof EmptyResultSetException) {
                                        view.showExitDialog("Saving Successful", "Job Details has been sent to the server.\n\nSaving Complete.");
                                        view.showSnackMessage("Saving Successful");
                                    } else {
                                        throwable.printStackTrace();
                                        view.showExitDialog("Saving Error", throwable.getMessage());
                                    }
                                    new Handler().postDelayed(() -> view.hideProgress(), 1000);
                                }));
                    }
                }));
    }

    private void uploadPhysicalFile(int index, int attachmentsSize, AppAttachment appAttachment, List<AppAttachment> appAttachments, File attachment_path) {
        AndroidNetworking.upload(GlobalString.pdf_upload_url)
                .addMultipartFile("uploaded_file", attachment_path)
                .setPriority(Priority.HIGH)
                .build()
                .setUploadProgressListener((bytesUploaded, totalBytes) -> Log.e("int--uploadingPercent", bytesUploaded + "/" + totalBytes))
                .getAsJSONObject(new JSONObjectRequestListener() {
                    @Override
                    public void onResponse(JSONObject response) {
                        insertToDatabaseAttachment(
                                recordId,
                                appAttachment.getAppAttachmentFile(),
                                appAttachment.getAppAttachmentFilename(),
                                appAttachment.getAppAttachmentAppraisalType());

                        if (index == (attachmentsSize - 1)) {

                            if (!appAttachments.isEmpty()) {
                                if (attachment_path.exists()) {
                                    if (attachment_path.delete()) {

                                        interactor.deleteAllAttachments(appAttachments);

                                        Log.d("int--deleteAttachments", "Attachments removed.");

                                        view.showExitDialog("Record Updated", "Record ID: " + recordId + " has been updated.");
                                        view.showSnackMessage("Saving Successful");
                                    }
                                }
                            }
                        }
                    }

                    @Override
                    public void onError(ANError anError) {
                        anError.printStackTrace();
                        view.showExitDialog("Networking Error", anError.getMessage());
                    }
                });

    }

    private void insertToDatabaseAttachment(String recordId,
                                            String attachmentFile,
                                            String attachmentFileName,
                                            String appraisalType) {

        AndroidNetworking.post(GlobalString.insert_data_attachment_url)
                .addBodyParameter("tag", "QFEgR3KsBJRKcYSy2nCp")
                .addBodyParameter("attachments_data", INSERT_APPRAISAL_ATTACHMENTS_JSON_BUILDER(recordId, attachmentFile, attachmentFileName, SENTENCE_TO_APPRAISAL_TYPE_CHANGER(appraisalType) + "_0").toString())
                .setPriority(Priority.HIGH)
                .build()
                .getAsJSONObject(new JSONObjectRequestListener() {
                    @Override
                    public void onResponse(JSONObject response) {
                        Log.e("int--AttachmentInsert", "Logic Triggered");
                    }

                    @Override
                    public void onError(ANError error) {
                        Log.e("int--AttachmentInsert", "False " + error.getErrorDetail() + error.getErrorCode());
                        error.printStackTrace();
                    }
                });
    }


    private Consumer<Throwable> getSaveRemoteError() {
        return throwable -> {
            throwable.printStackTrace();
            new Handler().postDelayed(() -> view.hideProgress(), 1000);
            view.showExitDialog("Saving Error", throwable.getMessage());
        };
    }
}
