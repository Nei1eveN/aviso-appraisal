package com.gdslinkasia.appraisaldetails;

import android.app.ProgressDialog;
import android.content.Intent;
import android.os.Bundle;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AlertDialog;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;
import androidx.cardview.widget.CardView;
import androidx.constraintlayout.widget.ConstraintLayout;
import androidx.coordinatorlayout.widget.CoordinatorLayout;
import androidx.fragment.app.FragmentTransaction;

import com.gdslinkasia.appraisaldetails.contract.jobdetails.JobDetailContract;
import com.gdslinkasia.appraisaldetails.contract.jobdetails.JobDetailPresenter;
import com.gdslinkasia.base.model.job.Record;
import com.gdslinkasia.base.utils.ActivityIntent;
import com.gdslinkasia.commonresources.R2;
import com.google.android.material.snackbar.Snackbar;

import java.util.Objects;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

import static com.gdslinkasia.base.database.DatabaseUtils.APPRAISAL_TYPE;
import static com.gdslinkasia.base.database.DatabaseUtils.RECORD_ID;
import static com.gdslinkasia.base.database.DatabaseUtils.VAR_CONDO;
import static com.gdslinkasia.base.database.DatabaseUtils.VAR_LAND_IMP;
import static com.gdslinkasia.base.utils.GlobalString.REPORT_REVIEW;
import static com.gdslinkasia.base.utils.GlobalString.SENTENCE_TO_APPRAISAL_TYPE_CHANGER;

public class ActiveJobDetails extends AppCompatActivity implements JobDetailContract.View, Toolbar.OnMenuItemClickListener {

    Intent intent;
    String recordId, appraisalType;

    //TODO SCOPE OF WORK
    @BindView(R2.id.scopeCard) CardView scopeCard;
    @BindView(R2.id.scopeOfWorkLayout) ConstraintLayout scopeOfWorkLayout;

    //TODO PROPERTY DESCRIPTION
    @BindView(R2.id.propertyCard) CardView propertyCard;

    //TODO LAND VALUATION
    @BindView(R2.id.landValuationCard) CardView landValuationCard;
    @BindView(R2.id.txtLandValTitle) TextView txtLandValTitle;

    //TODO IMPROVEMENT VALUATION
    @BindView(R2.id.improvementValuationCard) CardView improvementValuationCard;
    @BindView(R2.id.llImprovementValuation) ConstraintLayout improvementValuationLayout;

    @BindView(R2.id.coordinatorLayout) CoordinatorLayout coordinatorLayout;
//    @BindView(R2.id.toolbar) Toolbar toolbar;

    //TODO CLIENT DETAILS
    @BindView(R2.id.txt_name) TextView clientName;
    @BindView(R2.id.txt_client_designation) TextView clientDesignation;
    @BindView(R2.id.txt_email) TextView email;
    @BindView(R2.id.txt_phone) TextView phone;

    @BindView(R2.id.txt_request_date) TextView requestedDate;
    @BindView(R2.id.txt_contract_date) TextView contractDate;

    @BindView(R2.id.txt_address) TextView address;

    //TODO JOB DETAIL FUNCTIONAL BUTTONS
    @BindView(R2.id.btnApprDetailsUpdate) Button btnUpdate;
    @BindView(R2.id.btnSave) Button btnSave;
    @BindView(R2.id.btnSubmit) Button btnSubmit;
    @BindView(R2.id.btnSyncFromServer) Button btnSyncFromServer;

    private JobDetailContract.Presenter presenter;

    private ProgressDialog progressDialog;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_active_job_details);
        ButterKnife.bind(this);
//        setSupportActionBar(toolbar);

        intent = getIntent();
        recordId = intent.getStringExtra(RECORD_ID);
        appraisalType = intent.getStringExtra(APPRAISAL_TYPE);

        progressDialog = new ProgressDialog(this);

        Objects.requireNonNull(getSupportActionBar()).setTitle("Active Job Details");
        getSupportActionBar().setSubtitle(appraisalType);
        Objects.requireNonNull(getSupportActionBar()).setDisplayHomeAsUpEnabled(true);

//        toolbar.inflateMenu(R.menu.menu_appr_details);
//        toolbar.setOnMenuItemClickListener(this);

        //TODO show improvement valuations
        if(appraisalType.equals(VAR_LAND_IMP)) {
            improvementValuationLayout.setVisibility(View.VISIBLE);
            improvementValuationCard.setVisibility(View.VISIBLE);
        } else {
            improvementValuationLayout.setVisibility(View.GONE);
            improvementValuationCard.setVisibility(View.GONE);
        }

        if(appraisalType.equals(VAR_CONDO)){
            txtLandValTitle.setText(getString(R.string.lbl_unitValuation));
        }

        clientName.setVisibility(View.INVISIBLE);
        clientDesignation.setVisibility(View.INVISIBLE);
        email.setVisibility(View.INVISIBLE);
        phone.setVisibility(View.INVISIBLE);

        requestedDate.setVisibility(View.INVISIBLE);
        contractDate.setVisibility(View.INVISIBLE);

        address.setVisibility(View.INVISIBLE);

        presenter = new JobDetailPresenter(this, getApplication());

    }

    @Override
    public boolean onOptionsItemSelected(@NonNull MenuItem item) {
        if (item.getItemId() == android.R.id.home) {
            finish();
        }
        return super.onOptionsItemSelected(item);
    }

    @Override
    protected void onResume() {
        super.onResume();
        presenter.onStart(recordId);
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        progressDialog.dismiss();
        presenter.onDestroy();
    }

    @Override
    public void showProgress(String title, String message) {
        progressDialog.setCancelable(false);
        progressDialog.setIndeterminate(false);
        progressDialog.setTitle(title);
        progressDialog.setMessage(message);
        progressDialog.show();
    }

    @Override
    public void hideProgress() {
        progressDialog.hide();
    }

    @Override
    public void showSnackMessage(String message) {
        Snackbar.make(coordinatorLayout, message, Snackbar.LENGTH_SHORT).show();
    }

    @Override
    public void showJobDetails(Record record) {
        clientName.setText(String.format("%s %s %s", record.getAppAccountFirstName(), record.getAppAccountMiddleName(), record.getAppAccountLastName()));
        clientDesignation.setText(record.getAppAccountDesignation());
        email.setText(record.getAppAccountBusinessEmail());
        phone.setText(record.getAppAccountBusinessContact());

        clientName.setVisibility(View.VISIBLE);
        clientDesignation.setVisibility(View.VISIBLE);
        email.setVisibility(View.VISIBLE);
        phone.setVisibility(View.VISIBLE);

        requestedDate.setText(String.format("%s - %s - %s", record.getAppDaterequestedMonth(), record.getAppDaterequestedDay(), record.getAppDaterequestedYear()));
        contractDate.setText(String.format("%s - %s - %s", record.getAppDateContractMonth(), record.getAppDateContractDay(), record.getAppDateContractYear()));
        address.setText(String.format("%s %s %s %s %s %s %s %s %s %s %s", record.getAppraisalRequest().get(0).getAppCollateralAddress().get(0).getAppLotNo(), record.getAppraisalRequest().get(0).getAppCollateralAddress().get(0).getAppBlockNo(), record.getAppraisalRequest().get(0).getAppCollateralAddress().get(0).getAppStreetName(), record.getAppraisalRequest().get(0).getAppCollateralAddress().get(0).getAppVillage(), record.getAppraisalRequest().get(0).getAppCollateralAddress().get(0).getAppBrgy(), record.getAppraisalRequest().get(0).getAppCollateralAddress().get(0).getAppZip(), record.getAppraisalRequest().get(0).getAppCollateralAddress().get(0).getAppDistrict(), record.getAppraisalRequest().get(0).getAppCollateralAddress().get(0).getAppCity(), record.getAppraisalRequest().get(0).getAppCollateralAddress().get(0).getAppProvince(), record.getAppraisalRequest().get(0).getAppCollateralAddress().get(0).getAppRegion(), record.getAppraisalRequest().get(0).getAppCollateralAddress().get(0).getAppCountry()));

        requestedDate.setVisibility(View.VISIBLE);
        contractDate.setVisibility(View.VISIBLE);
        address.setVisibility(View.VISIBLE);

    }

    @Override
    public void showSyncOptionDialog(String title, String message) {
        new AlertDialog.Builder(this)
                .setCancelable(false)
                .setTitle(title)
                .setMessage(message)
                .setPositiveButton("SYNC", (dialogInterface, i) -> presenter.requestFirstTimeSyncJobDetails(recordId))
                .setNegativeButton("EXIT", (dialogInterface, i) -> finish())
                .create()
                .show();
    }

    @Override
    public void showExitDialog(String title, String message) {
        new AlertDialog.Builder(this)
                .setCancelable(false)
                .setTitle(title)
                .setMessage(message)
                .setPositiveButton("EXIT", (dialogInterface, i) -> finish())
                .create()
                .show();
    }

    @OnClick(R2.id.btnApprDetailsUpdate)
    void onClickUpdate() {
        Bundle bundle = new Bundle();
        bundle.putString(RECORD_ID, recordId);
        intent.putExtra(APPRAISAL_TYPE, appraisalType);
        UpdateAppraisalDetailDialogFragment dialogFragment = new UpdateAppraisalDetailDialogFragment();
        dialogFragment.setArguments(bundle);

        FragmentTransaction fragmentTransaction = getSupportFragmentManager().beginTransaction();
        dialogFragment.show(fragmentTransaction, UpdateAppraisalDetailDialogFragment.TAG);
    }

    @OnClick(R2.id.scopeOfWorkLayout)
    void onClickScopeOfWork() {
        try {
            Intent intent = new Intent(this, Class.forName(ActivityIntent.SCOPE_OF_WORK_ACTIVITY));
            intent.putExtra(RECORD_ID, recordId);
            intent.putExtra(APPRAISAL_TYPE, appraisalType);

            startActivity(intent);
        } catch (ClassNotFoundException e) {
            e.printStackTrace();
        }
    }

    @OnClick(R2.id.propertyDetails)
    void onClickPropertyDesc() {
        try {
            Intent intent = new Intent(this, Class.forName(ActivityIntent.PROPERTY_DESCRIPTION_ACTIVITY));
            intent.putExtra(RECORD_ID, recordId);
            intent.putExtra(APPRAISAL_TYPE, appraisalType);
            startActivity(intent);
        } catch (ClassNotFoundException e) {
            e.printStackTrace();
        }
    }

    @OnClick(R2.id.landValuation)
    void onClickLandValuation() {
        try {
            Intent intent = new Intent(this, Class.forName(ActivityIntent.LAND_VALUATION_ACTIVITY));
            intent.putExtra(RECORD_ID, recordId);
            intent.putExtra(APPRAISAL_TYPE, appraisalType);
            startActivity(intent);
        } catch (ClassNotFoundException e) {
            e.printStackTrace();
        }
    }

    @OnClick(R2.id.llImprovementValuation)
    void onClickllImprovementValuation() {
        try {
            Intent intent = new Intent(this, Class.forName(ActivityIntent.IMPROVEMENT_VALUATION_ACTIVITY));
            intent.putExtra(RECORD_ID, recordId);
            intent.putExtra(APPRAISAL_TYPE, appraisalType);
            startActivity(intent);
        } catch (ClassNotFoundException e) {
            e.printStackTrace();
        }
    }

    @OnClick(R2.id.qualifications)
    void onClickQualifications() {
        try {
            Intent intent = new Intent(this, Class.forName(ActivityIntent.QUALIFICATION_ACTIVITY));
            intent.putExtra(RECORD_ID, recordId);
            intent.putExtra(APPRAISAL_TYPE, appraisalType);
            startActivity(intent);
        } catch (ClassNotFoundException e) {
            e.printStackTrace();
        }
    }

    @OnClick(R2.id.valuationSummary)
    void onClickvaluationSummary() {
        try {
            Intent intent = new Intent(this, Class.forName(ActivityIntent.VALUATION_SUMMARY_ACTIVITY));
            intent.putExtra(RECORD_ID, recordId);
            intent.putExtra(APPRAISAL_TYPE, appraisalType);
            startActivity(intent);
        } catch (ClassNotFoundException e) {
            e.printStackTrace();
        }
    }

    @OnClick(R2.id.othersLayout)
    void onClickOthersLayout() {
        try {
            Intent intent = new Intent(this, Class.forName(ActivityIntent.OTHERS_ACTIVITY));
            intent.putExtra(RECORD_ID, recordId);
            intent.putExtra(APPRAISAL_TYPE, appraisalType);
            startActivity(intent);
        } catch (ClassNotFoundException e) {
            e.printStackTrace();
        }
    }

    @OnClick(R2.id.attachments)
    void onClickAttachments() {
        try {
            Intent intent = new Intent(this, Class.forName(ActivityIntent.ATTACHMENTS_ACTIVITY));

            String[] attachmentOptions =
                    {"Attachment From Request", "Created Attachments"};
            new AlertDialog.Builder(this)
//                .setCancelable(false)
                    .setTitle("Attachments")
                    .setItems(attachmentOptions, (dialog, which) -> {
                        switch (which) {
                            case 0:
                                intent.putExtra(RECORD_ID, recordId);
                                intent.putExtra(APPRAISAL_TYPE, SENTENCE_TO_APPRAISAL_TYPE_CHANGER(appraisalType));
                                intent.putExtra("createdFrom", "Server");
                                startActivity(intent);
                                break;
                            case 1:
                                intent.putExtra(RECORD_ID, recordId);
                                intent.putExtra(APPRAISAL_TYPE, SENTENCE_TO_APPRAISAL_TYPE_CHANGER(appraisalType));
                                intent.putExtra("createdFrom", "Local");
                                startActivity(intent);
                                break;
                        }
                    })
                    .create()
                    .show();
        } catch (ClassNotFoundException e) {
            e.printStackTrace();
        }

    }

    @OnClick(R2.id.btnSubmit)
    void onClickSubmitForReview() {
        new AlertDialog.Builder(this)
                .setCancelable(false)
                .setTitle("Submit For Review")
                .setMessage("You are about to submit this job for review.\n\nShall we proceed?")
                .setPositiveButton("PROCEED", (dialog, which) -> presenter.saveDetailsToServer(recordId, "Submit For Review", REPORT_REVIEW))
                .setNegativeButton("CANCEL", null)
                .create()
                .show();
    }

    @OnClick(R2.id.btnSave)
    void onClickSave() {
        new AlertDialog.Builder(this)
                .setCancelable(false)
                .setTitle("Save Data To Server")
                .setMessage("You are about to save your current data from the server.\n\nShall we proceed?")
                .setPositiveButton("PROCEED", (dialog, which) -> presenter.saveDetailsToServer(recordId, "Save", REPORT_REVIEW))
                .setNegativeButton("CANCEL", null)
                .create()
                .show();
    }

    @OnClick(R2.id.btnSyncFromServer)
    void onClickRefreshDetail() {
        new AlertDialog.Builder(this)
                .setTitle("Sync From Server")
                .setMessage("This function lets you retrieve details from the server. Shall we proceed?\n\nTo perform this action, your device must be connected to the internet.")
                .setPositiveButton("PROCEED", (dialogInterface, i) -> presenter.requestJobDetails(recordId))
                .setNegativeButton("CANCEL", null)
                .create()
                .show();
    }


    @Override
    public boolean onMenuItemClick(MenuItem item) {
        int i = item.getItemId();
        if (i == R.id.sync) {
            new AlertDialog.Builder(this)
                    .setCancelable(false)
                    .setTitle("Sync Data From Server")
                    .setMessage("You are about to retrieve data from the server. However, data on this device will be overwritten.\n\nShall we proceed?")
                    .setPositiveButton("PROCEED", (dialog, which) -> presenter.requestJobDetails(recordId))
                    .setNegativeButton("CANCEL", null)
                    .create()
                    .show();
        } else if (i == R.id.save) {
            new AlertDialog.Builder(this)
                    .setCancelable(false)
                    .setTitle("Save Data To Server")
                    .setMessage("You are about to save your current data from the server.\n\nShall we proceed?")
                    .setPositiveButton("PROCEED", (dialog, which) -> presenter.requestFirstTimeSyncJobDetails(recordId))
                    .setNegativeButton("CANCEL", null)
                    .create()
                    .show();
        }
        return false;
    }
}
