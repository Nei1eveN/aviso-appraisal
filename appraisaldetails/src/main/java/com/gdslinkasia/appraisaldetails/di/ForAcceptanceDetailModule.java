package com.gdslinkasia.appraisaldetails.di;

import android.app.Application;

import com.gdslinkasia.appraisaldetails.contract.for_acceptance.ForAcceptanceContract;
import com.gdslinkasia.appraisaldetails.contract.for_acceptance.ForAcceptancePresenter;

import dagger.Module;
import dagger.Provides;

@Module
public class ForAcceptanceDetailModule {

    @Provides
    static ForAcceptanceContract.Presenter presenter(Application application) {
        return new ForAcceptancePresenter(application);
    }

}
