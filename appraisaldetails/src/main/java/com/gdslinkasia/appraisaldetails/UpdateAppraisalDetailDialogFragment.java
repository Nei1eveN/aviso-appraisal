package com.gdslinkasia.appraisaldetails;

import android.app.DatePickerDialog;
import android.app.Dialog;
import android.app.ProgressDialog;
import android.os.Bundle;
import android.os.Handler;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AutoCompleteTextView;
import android.widget.CheckBox;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.appcompat.app.AlertDialog;
import androidx.appcompat.widget.Toolbar;
import androidx.coordinatorlayout.widget.CoordinatorLayout;
import androidx.fragment.app.DialogFragment;
import androidx.fragment.app.FragmentTransaction;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.gdslinkasia.appraisaldetails.contract.update_appraisal_details.UpdateAppraisalDetailContract;
import com.gdslinkasia.appraisaldetails.contract.update_appraisal_details.UpdateAppraisalDetailsPresenter;
import com.gdslinkasia.base.adapter.AppContactPersonAdapter;
import com.gdslinkasia.base.adapter.SubjPropertyAddressAdapter;
import com.gdslinkasia.base.adapter.click_listener.AppCollateralAddressClickListener;
import com.gdslinkasia.base.adapter.click_listener.AppContactPersonClickListener;
import com.gdslinkasia.base.model.details.AppCollateralAddress;
import com.gdslinkasia.base.model.details.AppContactPerson;
import com.gdslinkasia.base.model.job.Record;
import com.gdslinkasia.base.utils.GlobalFunctions;
import com.gdslinkasia.commonresources.R2;
import com.google.android.material.snackbar.Snackbar;
import com.google.android.material.textfield.TextInputEditText;
import com.google.android.material.textfield.TextInputLayout;

import java.text.ParseException;
import java.util.Calendar;
import java.util.Date;
import java.util.List;
import java.util.Objects;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.Unbinder;

import static com.gdslinkasia.base.database.DatabaseUtils.RECORD_ID;
import static com.gdslinkasia.base.database.DatabaseUtils.UNIQUE_ID;
import static com.gdslinkasia.base.utils.GlobalFunctions.*;
import static com.gdslinkasia.base.utils.GlobalString.APPRAISAL_TYPE_SENTENCE_CHANGER;
import static com.gdslinkasia.base.utils.GlobalString.CHECKBOX_TO_STRING;
import static com.gdslinkasia.base.utils.GlobalString.SENTENCE_TO_APPRAISAL_TYPE_CHANGER;

public class UpdateAppraisalDetailDialogFragment extends DialogFragment
        implements UpdateAppraisalDetailContract.View, Toolbar.OnMenuItemClickListener,
        AppContactPersonClickListener, AppCollateralAddressClickListener, View.OnClickListener {

    public static String TAG = "FullScreenDialog";

    private String recordId;

    private Unbinder unbinder;

    @BindView(R2.id.coordinatorLayout) CoordinatorLayout coordinatorLayout;
    @BindView(R2.id.toolbar) Toolbar toolbar;
    @BindView(R2.id.frameContainer) FrameLayout frameLayout;

    private TextInputLayout txAppNatureAppraisalOther;

    private TextInputEditText dateReqMonth, dateReqDay, dateReqYear;
    private TextInputEditText etContractDateMonth, etContractDay, etContractYear;
    private TextInputLayout txFirstName, txMiddleName, txLastName;
    private TextInputEditText etFirstName, etMiddleName, etLastName;
    private TextInputEditText etCompanyName, etAuthOfficer;
    private TextInputEditText etClientDesignation, etClientAddress;
    private TextInputEditText etContactMobNo, etContactEmailAdd;
    private TextInputEditText etIntendedUsers, etOtherIntendedUsers;
    private TextInputEditText etRequestorName, etReqContactNo, etRequestorDesignation, etReqEmailAdd;
    private TextInputEditText etInspectMonth, etInspectDay, etInspectYear;
    private TextInputEditText etValuationMonth, etValuationDay, etValuationYear;
    private TextInputEditText etZonalValue;
    private TextInputEditText etControlNo, etPropertyType;
    private TextInputEditText etAppNatureAppraisalOther;
    private TextInputEditText etOwnerSubjProp;
//    private TextInputEditText etLotNo, etBlockNo, etStreetName, etVillage, etBrgy, etZip, etDistrict, etCity, etProvince, etRegion, etCountry;

    private RecyclerView rvOtherContacts;
    private TextView tvOtherContacts;

    private RecyclerView rvAddresses;
    private TextView tvNoCollateralAddress;

    private AutoCompleteTextView acTitle, acClientType, acBasisOfValue, acAppraisalType, acPurposeOfValuation; //, acSubjPropType

    private CheckBox cbCompany;
    private LinearLayout containerClientName;
    private LinearLayout containerCompanyName;

    private UpdateAppraisalDetailContract.Presenter presenter;

    private ProgressDialog progressDialog;

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setStyle(DialogFragment.STYLE_NORMAL, R.style.FullScreenDialog);

        recordId = Objects.requireNonNull(getArguments()).getString(RECORD_ID);

        presenter = new UpdateAppraisalDetailsPresenter(this, Objects.requireNonNull(getActivity()).getApplication());
    }

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        return inflater.inflate(R.layout.layout_full_screen_dialog, container, false);
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        unbinder = ButterKnife.bind(this, view);

        progressDialog = new ProgressDialog(getActivity());

        toolbar.setNavigationIcon(R.drawable.ic_close_white_24dp);
        toolbar.setNavigationOnClickListener(view1 -> Objects.requireNonNull(getDialog()).dismiss());
        toolbar.setTitle("Update Appraisal Details");

//        toolbar.inflateMenu(R.menu.menu_appr_details);
//        toolbar.setOnMenuItemClickListener(this);

        //TODO Form Widgets
        View appraisalDetailView = LayoutInflater.from(getContext()).inflate(R.layout.form_appraisal_details, frameLayout, false);
        View constraintLayout = appraisalDetailView.findViewById(R.id.constraintLayout);

        //TODO Request Date + Contract Date
        dateReqMonth = appraisalDetailView.findViewById(R.id.dateReqMonth);
        dateReqDay = appraisalDetailView.findViewById(R.id.dateReqDay);
        dateReqYear = appraisalDetailView.findViewById(R.id.dateReqYear);
        etContractDateMonth = appraisalDetailView.findViewById(R.id.etContractDateMonth);
        etContractDay = appraisalDetailView.findViewById(R.id.etContractDay);
        etContractYear = appraisalDetailView.findViewById(R.id.etContractYear);

        etClientDesignation = appraisalDetailView.findViewById(R.id.etClientDesignation);
        etClientAddress = appraisalDetailView.findViewById(R.id.etClientAddress);

        etContactMobNo = appraisalDetailView.findViewById(R.id.etContactMobNo);
        etContactEmailAdd = appraisalDetailView.findViewById(R.id.etContactEmailAdd);

        //TODO Company Checkbox
        cbCompany = appraisalDetailView.findViewById(R.id.cbCompany);

        //TODO Client Container
        containerClientName = appraisalDetailView.findViewById(R.id.container_clientName);
        acTitle = containerClientName.findViewById(R.id.acTitle);
        txFirstName = containerClientName.findViewById(R.id.txFirstName);
        etFirstName = containerClientName.findViewById(R.id.etFirstName);

        txMiddleName = containerClientName.findViewById(R.id.txMiddleName);
        etMiddleName = containerClientName.findViewById(R.id.etMiddleName);

        txLastName = containerClientName.findViewById(R.id.txLastName);
        etLastName = containerClientName.findViewById(R.id.etLastName);

        //TODO Company Container
        containerCompanyName = appraisalDetailView.findViewById(R.id.container_companyName);
        etCompanyName = containerCompanyName.findViewById(R.id.etCompanyName);
        etAuthOfficer = containerCompanyName.findViewById(R.id.etAuthOfficer);

        //TODO Other Contacts Container
        rvOtherContacts = appraisalDetailView.findViewById(R.id.rvOtherContacts);
        tvOtherContacts = appraisalDetailView.findViewById(R.id.tvNoOtherContacts);

        acClientType = appraisalDetailView.findViewById(R.id.acClientType);
        etIntendedUsers = appraisalDetailView.findViewById(R.id.etIntendedUsers);
        etOtherIntendedUsers = appraisalDetailView.findViewById(R.id.etOtherIntendedUsers);
        //TODO REQUESTOR DETAILS
        etRequestorName = appraisalDetailView.findViewById(R.id.etRequestorName);
        etReqContactNo = appraisalDetailView.findViewById(R.id.etReqContactNo);
        etRequestorDesignation = appraisalDetailView.findViewById(R.id.etRequestorDesignation);
        etReqEmailAdd = appraisalDetailView.findViewById(R.id.etReqEmailAdd);

        //TODO INSPECTED DATE
        etInspectMonth = appraisalDetailView.findViewById(R.id.etInspectMonth);
        etInspectDay = appraisalDetailView.findViewById(R.id.etInspectDay);
        etInspectYear = appraisalDetailView.findViewById(R.id.etInspectYear);
        ImageView ivInspectDate = appraisalDetailView.findViewById(R.id.ivInspectDate);
        ivInspectDate.setOnClickListener(this);

        //TODO VALUATION DATE
        etValuationMonth = appraisalDetailView.findViewById(R.id.etValuationMonth);
        etValuationDay = appraisalDetailView.findViewById(R.id.etValuationDay);
        etValuationYear = appraisalDetailView.findViewById(R.id.etValuationYear);
        ImageView ivValuationDate = appraisalDetailView.findViewById(R.id.ivValuationDate);
        ivValuationDate.setOnClickListener(this);

        acBasisOfValue = appraisalDetailView.findViewById(R.id.acBasisOfValue);
        etZonalValue = appraisalDetailView.findViewById(R.id.etZonalValue);

        etControlNo = appraisalDetailView.findViewById(R.id.etControlNo);
        etPropertyType = appraisalDetailView.findViewById(R.id.etPropertyType);
        acAppraisalType = appraisalDetailView.findViewById(R.id.acAppraisalType);
        acPurposeOfValuation = appraisalDetailView.findViewById(R.id.acPurposeOfValuation);
        txAppNatureAppraisalOther = constraintLayout.findViewById(R.id.txAppNatureAppraisalOther);
        etAppNatureAppraisalOther = constraintLayout.findViewById(R.id.etAppNatureAppraisalOther);

        etOwnerSubjProp = appraisalDetailView.findViewById(R.id.etOwnerSubjProp);

        //TODO Subject Property Address
        rvAddresses = appraisalDetailView.findViewById(R.id.rvAddresses);
        tvNoCollateralAddress = appraisalDetailView.findViewById(R.id.tvNoAddresses);

        frameLayout.addView(appraisalDetailView);
        presenter.onStart(recordId);
    }

    @Override
    public void onStart() {
        super.onStart();
        Log.d("int--DialogOnStart", "Logic Triggered");
        Dialog dialog = getDialog();
        if (dialog != null) {
            int width = ViewGroup.LayoutParams.MATCH_PARENT;
            int height = ViewGroup.LayoutParams.MATCH_PARENT;
            Objects.requireNonNull(dialog.getWindow()).setLayout(width, height);
        }
    }

    @Override
    public void onPause() {
        super.onPause();
        new Handler().post(() ->
        {
            Log.d("int--onPause", "Logic Triggered");
            presenter.updateAppraisalDetails(
                    recordId,
                    CHECKBOX_TO_STRING(cbCompany),
                    acTitle.getText().toString(),
                    Objects.requireNonNull(etFirstName.getText()).toString(),
                    Objects.requireNonNull(etMiddleName.getText()).toString(),
                    Objects.requireNonNull(etLastName.getText()).toString(),
                    Objects.requireNonNull(etCompanyName.getText()).toString(),
                    Objects.requireNonNull(etAuthOfficer.getText()).toString(),
                    Objects.requireNonNull(etClientDesignation.getText()).toString(),
                    Objects.requireNonNull(etClientAddress.getText()).toString(),
                    Objects.requireNonNull(etContactMobNo.getText()).toString(),
                    Objects.requireNonNull(etContactEmailAdd.getText()).toString(),
                    acClientType.getText().toString(),
                    Objects.requireNonNull(etIntendedUsers.getText()).toString(),
                    Objects.requireNonNull(etOtherIntendedUsers.getText()).toString(),
                    Objects.requireNonNull(etInspectDay.getText()).toString(),
                    Objects.requireNonNull(etInspectYear.getText()).toString()+"-"+ Objects.requireNonNull(etInspectMonth.getText()).toString()+"-"+etInspectDay.getText().toString()+"T00:00:00+08:00",
                    etInspectYear.getText().toString(),
                    Objects.requireNonNull(etValuationDay.getText()).toString(),
                    Objects.requireNonNull(etValuationYear.getText()).toString()+"-"+ Objects.requireNonNull(etValuationMonth.getText()).toString()+"-"+etValuationDay.getText().toString()+"T00:00:00+08:00",
                    etValuationYear.getText().toString(),
                    acBasisOfValue.getText().toString(),
                    Objects.requireNonNull(etZonalValue.getText()).toString(),
                    Objects.requireNonNull(etControlNo.getText()).toString(),
                    SENTENCE_TO_APPRAISAL_TYPE_CHANGER(Objects.requireNonNull(etPropertyType.getText()).toString()),
                    acAppraisalType.getText().toString(),
                    acPurposeOfValuation.getText().toString(),
                    Objects.requireNonNull(etAppNatureAppraisalOther.getText()).toString(),
                    Objects.requireNonNull(etOwnerSubjProp.getText()).toString()
            );

            Log.d("int--DateCombined", Objects.requireNonNull(etInspectYear.getText()).toString()+"-"+ Objects.requireNonNull(etInspectMonth.getText()).toString()+"-"+etInspectDay.getText().toString()+"T00:00:00+08:00");
        });
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        presenter.onDestroy();
        progressDialog.dismiss();
    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();
        unbinder.unbind();
    }

    @Override
    public void showProgress(String title, String message) {
        progressDialog.setCancelable(false);
        progressDialog.setIndeterminate(false);
        progressDialog.setTitle(title);
        progressDialog.setMessage(message);
        progressDialog.show();
    }

    @Override
    public void hideProgress() {
        progressDialog.hide();
    }

    @Override
    public void showSnackMessage(String message) {
        Snackbar.make(coordinatorLayout, message, Snackbar.LENGTH_SHORT).show();
    }

    @Override
    public void showDetails(Record record) {
        dateReqMonth.setText(String.format("%s", record.getAppDaterequestedMonth()));
        dateReqDay.setText(record.getAppDaterequestedDay());
        dateReqYear.setText(record.getAppDaterequestedYear());

        etContractDateMonth.setText(record.getAppDateContractMonth());
        etContractDay.setText(record.getAppDateContractDay());
        etContractYear.setText(record.getAppDateContractYear());

        cbCompany.setChecked(getCheckboxBoolean(record.getAppAccountCompany()));
        containerClientName.setVisibility(GET_CLIENT_NAME_VISIBILITY_FROM_COMPANY_CHECKBOX(cbCompany));
        containerCompanyName.setVisibility(GET_COMPANY_VISIBILITY_FROM_COMPANY_CHECKBOX(cbCompany));
        ONCHANGE_VISIBILITY_COMPANY_CHECKBOX(cbCompany, containerClientName, containerCompanyName);

        acTitle.setText(record.getAppAccountNameTitle());
        acTitle.setEnabled(false);
//        acTitle.setFocusable(false);
        acTitle.setAdapter(GET_CLIENT_TITLE_ADAPTER(record.getAppAccountNameTitle(), Objects.requireNonNull(getActivity())));
        acTitle.setOnClickListener(v -> acTitle.showDropDown());

        txFirstName.setEnabled(false);
        etFirstName.setText(record.getAppAccountFirstName());
        etFirstName.setEnabled(false);

        txMiddleName.setEnabled(false);
        etMiddleName.setText(record.getAppAccountMiddleName());
        etMiddleName.setEnabled(false);

        txLastName.setEnabled(false);
        etLastName.setText(record.getAppAccountLastName());
        etLastName.setEnabled(false);

        etCompanyName.setText(record.getAppAccountFirstName());
        etAuthOfficer.setText(record.getAppAccountAuthroizingOfficer());

        etClientDesignation.setText(record.getAppAccountDesignation());
        etClientAddress.setText(record.getAppAccountBusinessAddress());

        etContactMobNo.setText(record.getAppAccountBusinessContact());
        etContactEmailAdd.setText(record.getAppAccountBusinessEmail());

        acClientType.setText(record.getAppIntendedUsersName());
        acClientType.setEnabled(false);
        acClientType.setAdapter(GlobalFunctions.GET_CLIENT_TYPE_ADAPTER(record.getAppIntendedUsersName(), Objects.requireNonNull(getActivity())));
        acClientType.setOnClickListener(view -> acClientType.showDropDown());
        acClientType.setOnItemClickListener(new GlobalFunctions.AutoCompleteClickListener(acClientType, txAppNatureAppraisalOther, etAppNatureAppraisalOther));

        //TODO options: getAppIntendedUsersName() or getAppAccountFirstName()
        etIntendedUsers.setText(record.getAppAccountFirstName());
        etIntendedUsers.setEnabled(false);
        etIntendedUsers.setFocusable(false);

        etOtherIntendedUsers.setText(record.getAppIntendedUsersOthers());

        etRequestorName.setText(record.getAppValuerName());
        etRequestorName.setKeyListener(null);
        etRequestorDesignation.setText(record.getAppValuerOfficerType());
        etRequestorDesignation.setKeyListener(null);
        etReqContactNo.setText(record.getAppValuerContactNumber());
        etReqContactNo.setKeyListener(null);
        etReqEmailAdd.setText(record.getAppValuerEmailAddress());
        etReqEmailAdd.setKeyListener(null);

        try {
            Date inspectionDate = fullFormat.parse(record.getValrepLandimpDateInspectedMonth());
            String inspectionMonthWord = monthNumberFormat.format(Objects.requireNonNull(inspectionDate));
            String inspectionDayNumber = dayNumberFormat.format(inspectionDate);
            String inspectionYearNumber = yearNumberFormat.format(inspectionDate);

            etInspectMonth.setText(inspectionMonthWord);
            etInspectDay.setText(inspectionDayNumber);
            etInspectYear.setText(inspectionYearNumber);

            Date valuationDate = fullFormat.parse(record.getValrepLandimpValuationDateMonth());
            String valuationMonth = monthNumberFormat.format(Objects.requireNonNull(valuationDate));
            String valuationDayNumber = dayNumberFormat.format(valuationDate);
            String valuationYearNumber = yearNumberFormat.format(valuationDate);
            etValuationMonth.setText(valuationMonth);
            etValuationDay.setText(valuationDayNumber);
            etValuationYear.setText(valuationYearNumber);
        } catch (ParseException e) {
            e.printStackTrace();
        }


//        acBasisOfValue.setText(record.getValrepAppraisalBasisOfValue());
        acBasisOfValue.setAdapter(GET_BASIS_OF_VALUE_ADAPTER(record.getValrepAppraisalBasisOfValue(), getActivity()));
        acBasisOfValue.setOnClickListener(view -> acBasisOfValue.showDropDown());
        acBasisOfValue.setEnabled(false);

        etZonalValue.setText(record.getValrepLandimpZonalValue());

        etControlNo.setText(record.getAppraisalRequest().get(0).getAppControlNo());
        etControlNo.setKeyListener(null);
        //String appraisalTypeSentenceOriginal = record.getAppraisalRequest().get(0).getAppRequestAppraisal();
        String appraisalTypeSentenceChanger = APPRAISAL_TYPE_SENTENCE_CHANGER(record.getAppraisalRequest().get(0).getAppRequestAppraisal());
        etPropertyType.setText(appraisalTypeSentenceChanger);
        etPropertyType.setEnabled(false);
        etPropertyType.setFocusable(false);

        acAppraisalType.setText(record.getAppraisalRequest().get(0).getAppNatureAppraisal());
        acAppraisalType.setEnabled(false);
        acAppraisalType.setFocusable(false);

        acPurposeOfValuation.setText(record.getAppraisalRequest().get(0).getAppPurposeAppraisal());
        acPurposeOfValuation.setAdapter(GET_PURPOSE_APPRAISAL_ADAPTER(record.getAppraisalRequest().get(0).getAppPurposeAppraisal(), getActivity(), etAppNatureAppraisalOther));
        acPurposeOfValuation.setOnClickListener(view -> acPurposeOfValuation.showDropDown());
        acPurposeOfValuation.setOnItemClickListener(new GlobalFunctions.AutoCompleteClickListener(acPurposeOfValuation, txAppNatureAppraisalOther, etAppNatureAppraisalOther));
//        onPurposeOfValuationItemClick(acPurposeOfValuation, txAppNatureAppraisalOther, etAppNatureAppraisalOther);
        acPurposeOfValuation.setKeyListener(null);
        acPurposeOfValuation.setFocusable(false);

        etAppNatureAppraisalOther.setText(record.getAppraisalRequest().get(0).getAppPurposeAppraisalOther());

        etOwnerSubjProp.setText(record.getPafLandimpDeveloperName());
        Log.d("int--pafDevName", record.getPafLandimpDeveloperName());
    }

    @Override
    public void showExitDialog(String title, String message) {
        new AlertDialog.Builder(Objects.requireNonNull(getContext()))
                .setCancelable(false)
                .setTitle(title)
                .setMessage(message)
                .setPositiveButton("EXIT", (dialogInterface, i) -> Objects.requireNonNull(getDialog()).dismiss())
                .create()
                .show();
    }

    @Override
    public void showErrorDialog(String title, String message) {
        new AlertDialog.Builder(Objects.requireNonNull(getContext()))
                .setCancelable(false)
                .setTitle(title)
                .setMessage(message)
                .setPositiveButton("CLOSE", null)
                .create()
                .show();
    }

    @Override
    public void showOtherContacts(List<AppContactPerson> contactPersonList) {
        tvOtherContacts.setVisibility(View.GONE);
        rvOtherContacts.setVisibility(View.VISIBLE);

        AppContactPersonAdapter adapter = new AppContactPersonAdapter(getContext(), this);
        rvOtherContacts.setLayoutManager(new LinearLayoutManager(getContext()));
        adapter.submitList(contactPersonList);

        rvOtherContacts.setNestedScrollingEnabled(false);

//        rvOtherContacts.setHasFixedSize(true);

        rvOtherContacts.setAdapter(adapter);
    }

    @Override
    public void showCollateralAddress(List<AppCollateralAddress> collateralAddresses) {
        tvNoCollateralAddress.setVisibility(View.GONE);
        rvAddresses.setVisibility(View.VISIBLE);

        SubjPropertyAddressAdapter adapter = new SubjPropertyAddressAdapter(getContext(), this);
        rvAddresses.setLayoutManager(new LinearLayoutManager(getContext()));
        adapter.submitList(collateralAddresses);

        rvAddresses.setNestedScrollingEnabled(false);

//        rvOtherContacts.setHasFixedSize(true);

        rvAddresses.setAdapter(adapter);
    }

    @Override
    public void showEmptyOtherContacts(String message) {
        rvOtherContacts.setVisibility(View.GONE);
        tvOtherContacts.setVisibility(View.VISIBLE);

        tvOtherContacts.setText(message);
    }

    @Override
    public void showNoCollateralAddress(String message) {
        rvAddresses.setVisibility(View.GONE);
        tvNoCollateralAddress.setVisibility(View.VISIBLE);

        tvNoCollateralAddress.setText(message);
    }

    @Override
    public boolean onMenuItemClick(MenuItem item) {
        if (item.getItemId() == R.id.save) {
            presenter.updateAppraisalDetails(
                    recordId,
                    CHECKBOX_TO_STRING(cbCompany),
                    acTitle.getText().toString(),
                    Objects.requireNonNull(etFirstName.getText()).toString(),
                    Objects.requireNonNull(etMiddleName.getText()).toString(),
                    Objects.requireNonNull(etLastName.getText()).toString(),
                    Objects.requireNonNull(etCompanyName.getText()).toString(),
                    Objects.requireNonNull(etAuthOfficer.getText()).toString(),
                    Objects.requireNonNull(etClientDesignation.getText()).toString(),
                    Objects.requireNonNull(etClientAddress.getText()).toString(),
                    Objects.requireNonNull(etContactMobNo.getText()).toString(),
                    Objects.requireNonNull(etContactEmailAdd.getText()).toString(),
                    acClientType.getText().toString(),
                    Objects.requireNonNull(etIntendedUsers.getText()).toString(),
                    Objects.requireNonNull(etOtherIntendedUsers.getText()).toString(),
                    Objects.requireNonNull(etInspectDay.getText()).toString(),
                    Objects.requireNonNull(etInspectYear.getText()).toString()+"-"+ Objects.requireNonNull(etInspectMonth.getText()).toString()+"-"+etInspectDay.getText().toString()+"'T'00:00:00+08:00",
                    etInspectYear.getText().toString(),
                    Objects.requireNonNull(etValuationDay.getText()).toString(),
                    Objects.requireNonNull(etValuationYear.getText()).toString()+"-"+ Objects.requireNonNull(etValuationMonth.getText()).toString()+"-"+etValuationDay.getText().toString()+"'T'00:00:00+08:00",
                    etValuationYear.getText().toString(),
                    acBasisOfValue.getText().toString(),
                    Objects.requireNonNull(etZonalValue.getText()).toString(),
                    Objects.requireNonNull(etControlNo.getText()).toString(),
                    Objects.requireNonNull(etPropertyType.getText()).toString(),
                    acAppraisalType.getText().toString(),
                    acPurposeOfValuation.getText().toString(),
                    Objects.requireNonNull(etAppNatureAppraisalOther.getText()).toString(),
                    Objects.requireNonNull(etOwnerSubjProp.getText()).toString()
                    );

            Log.d("int--DateCombined", Objects.requireNonNull(etInspectYear.getText()).toString()+"-"+ Objects.requireNonNull(etInspectMonth.getText()).toString()+"-"+etInspectDay.getText().toString()+"T00:00:00+08:00");
        }
        return false;
    }

    @Override
    public void onContactItemClick(long uniqueId) {
        Log.d("int--contactUniqueId", String.valueOf(uniqueId));
    }

    @Override
    public void onClick(View v) {
        int i = v.getId();
        if (i == R.id.ivInspectDate) {
            DatePickerDialog datePickerDialog = setDialogDate(getActivity(),
                    getInspectedOrValuationYear(Integer.valueOf(Objects.requireNonNull(etInspectYear.getText()).toString().isEmpty() ? "0" : etInspectYear.getText().toString())),
                    getInspectedOrValuationMonth(Integer.valueOf(Objects.requireNonNull(etInspectMonth.getText()).toString().isEmpty() ? "0" : etInspectMonth.getText().toString())), //-1
                    getInspectedOrValuationDayOfMonth(Integer.valueOf(Objects.requireNonNull(etInspectDay.getText()).toString().isEmpty() ? "0" : etInspectDay.getText().toString())),
                    etInspectYear, etInspectMonth, etInspectDay);
            datePickerDialog.show();
//                DatePickerDialog datePickerDialog = new DatePickerDialog(Objects.requireNonNull(getActivity()), new DatePickerDialog.OnDateSetListener() {
//                    @Override
//                    public void onDateSet(DatePicker view, int year, int month, int dayOfMonth) {
//                        etInspectYear.setText(String.valueOf(year));
//                        etInspectMonth.setText(String.valueOf(month+1));
//                        etInspectDay.setText(String.valueOf(dayOfMonth));
//                    }
//                }, getInspectedOrValuationYear(Integer.valueOf(Objects.requireNonNull(etInspectYear.getText()).toString())), getInspectedOrValuationMonth(Integer.valueOf(Objects.requireNonNull(etInspectMonth.getText()).toString()))-1, getInspectedOrValuationDayOfMonth(Integer.valueOf(Objects.requireNonNull(etInspectDay.getText()).toString())));
//                datePickerDialog.getDatePicker().setMaxDate(Calendar.getInstance().getTimeInMillis());
            Log.d("int--year", String.valueOf(Calendar.getInstance().get(Calendar.YEAR)));
            Log.d("int--Month", String.valueOf(Calendar.getInstance().get(Calendar.MONTH) + 1));
//                datePickerDialog.show();
        } else if (i == R.id.ivValuationDate) {
            DatePickerDialog datePickerDialog;
            datePickerDialog = setDialogDate(getActivity(),
                    getInspectedOrValuationYear(Integer.valueOf(Objects.requireNonNull(etValuationYear.getText()).toString().isEmpty() ? "0" : etValuationYear.getText().toString())),
                    getInspectedOrValuationMonth(Integer.valueOf(Objects.requireNonNull(etValuationMonth.getText()).toString().isEmpty() ? "0" : etValuationMonth.getText().toString())), //-1
                    getInspectedOrValuationDayOfMonth(Integer.valueOf(Objects.requireNonNull(etValuationDay.getText()).toString().isEmpty() ? "0" : etValuationDay.getText().toString())),
                    etValuationYear, etValuationMonth, etValuationDay);
            datePickerDialog.show();
        }
    }

    @Override
    public void getAddressDetails(AppCollateralAddress address) {

    }

    @Override
    public void getAddressReferenceIds(long uniqueId) {
        Log.d("int--addressUniqueId", String.valueOf(uniqueId));
        Bundle bundle = new Bundle();
        bundle.putLong(UNIQUE_ID, uniqueId);

        UpdateSubjPropAddressDialogFragment dialogFragment = new UpdateSubjPropAddressDialogFragment();
        dialogFragment.setArguments(bundle);

        FragmentTransaction fragmentTransaction = getChildFragmentManager().beginTransaction();
        dialogFragment.show(fragmentTransaction, UpdateSubjPropAddressDialogFragment.TAG);
    }
}
