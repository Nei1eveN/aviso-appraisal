package com.gdslinkasia.appraisaldetails;

import android.app.Dialog;
import android.app.ProgressDialog;
import android.os.Bundle;
import android.os.Handler;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AutoCompleteTextView;
import android.widget.FrameLayout;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.appcompat.app.AlertDialog;
import androidx.appcompat.widget.Toolbar;
import androidx.coordinatorlayout.widget.CoordinatorLayout;
import androidx.fragment.app.DialogFragment;

import com.gdslinkasia.appraisaldetails.contract.property_address.SubjectPropertyAddressDetailContract;
import com.gdslinkasia.appraisaldetails.contract.property_address.SubjectPropertyAddressDetailPresenter;
import com.gdslinkasia.base.model.details.AppCollateralAddress;
import com.gdslinkasia.base.utils.GlobalFunctions;
import com.gdslinkasia.commonresources.R2;
import com.google.android.material.snackbar.Snackbar;
import com.google.android.material.textfield.TextInputEditText;

import java.util.Objects;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.Unbinder;

import static com.gdslinkasia.base.database.DatabaseUtils.UNIQUE_ID;


public class UpdateSubjPropAddressDialogFragment extends DialogFragment
        implements SubjectPropertyAddressDetailContract.View, Toolbar.OnMenuItemClickListener {

    static String TAG = "FullScreenDialog";

//    private int appraisalRequestId;
    private long uniqueId;

    private Unbinder unbinder;

    @BindView(R2.id.coordinatorLayout)
    CoordinatorLayout coordinatorLayout;
    @BindView(R2.id.toolbar)
    Toolbar toolbar;
    @BindView(R2.id.frameContainer)
    FrameLayout frameLayout;

    private AutoCompleteTextView acSubjPropType;
    private TextInputEditText etLotNo, etBlockNo, etStreetName, etVillage, etBrgy, etZip, etDistrict, etCity, etProvince, etRegion, etCountry;

    private SubjectPropertyAddressDetailContract.Presenter presenter;

    private ProgressDialog progressDialog;

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setStyle(DialogFragment.STYLE_NORMAL, R.style.FullScreenDialog);

//        appraisalRequestId = Objects.requireNonNull(getArguments()).getInt(APPRAISAL_REQUEST_ID);
        uniqueId = Objects.requireNonNull(getArguments()).getLong(UNIQUE_ID);

//        Log.d("int--recordId", String.valueOf(appraisalRequestId));
        Log.d("int--uniqueId", String.valueOf(uniqueId));

        presenter = new SubjectPropertyAddressDetailPresenter(this, Objects.requireNonNull(getActivity()).getApplication());
    }

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        return inflater.inflate(R.layout.layout_full_screen_dialog, container, false);
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        unbinder = ButterKnife.bind(this, view);

        progressDialog = new ProgressDialog(getActivity());

        toolbar.setNavigationIcon(R.drawable.ic_close_white_24dp);
        toolbar.setNavigationOnClickListener(view1 -> Objects.requireNonNull(getDialog()).dismiss());
        toolbar.setTitle("Details");
        toolbar.setSubtitle("Subject Property Address");

//        toolbar.inflateMenu(R.menu.menu_appr_details);
//        toolbar.setOnMenuItemClickListener(this);

        //TODO Form Widgets
        View appraisalDetailView = LayoutInflater.from(getContext()).inflate(R.layout.form_subj_property_address, frameLayout, false);

        acSubjPropType = appraisalDetailView.findViewById(R.id.acSubjPropType);
        etLotNo = appraisalDetailView.findViewById(R.id.etLotNo);
        etBlockNo = appraisalDetailView.findViewById(R.id.etBlockNo);
        etStreetName = appraisalDetailView.findViewById(R.id.etStreetName);
        etVillage = appraisalDetailView.findViewById(R.id.etVillage);
        etBrgy = appraisalDetailView.findViewById(R.id.etBrgy);
        etZip = appraisalDetailView.findViewById(R.id.etZip);
        etDistrict = appraisalDetailView.findViewById(R.id.etDistrict);
        etCity = appraisalDetailView.findViewById(R.id.etCity);
        etProvince = appraisalDetailView.findViewById(R.id.etProvince);
        etRegion = appraisalDetailView.findViewById(R.id.etRegion);
        etCountry = appraisalDetailView.findViewById(R.id.etCountry);

        frameLayout.addView(appraisalDetailView);

        presenter.onStart(uniqueId);
    }

    @Override
    public void onStart() {
        super.onStart();
        Log.d("int--DialogOnStart", "Logic Triggered");
        Dialog dialog = getDialog();
        if (dialog != null) {
            int width = ViewGroup.LayoutParams.MATCH_PARENT;
            int height = ViewGroup.LayoutParams.MATCH_PARENT;
            Objects.requireNonNull(dialog.getWindow()).setLayout(width, height);
        }
    }

    @Override
    public void onPause() {
        super.onPause();
        new Handler().post(() -> presenter.updateDetails(
                uniqueId,
                acSubjPropType.getText().toString(),
                Objects.requireNonNull(etLotNo.getText()).toString(),
                Objects.requireNonNull(etBlockNo.getText()).toString(),
                Objects.requireNonNull(etStreetName.getText()).toString(),
                Objects.requireNonNull(etVillage.getText()).toString(),
                Objects.requireNonNull(etBrgy.getText()).toString(),
                Objects.requireNonNull(etZip.getText()).toString(),
                Objects.requireNonNull(etDistrict.getText()).toString(),
                Objects.requireNonNull(etCity.getText()).toString(),
                Objects.requireNonNull(etProvince.getText()).toString(),
                Objects.requireNonNull(etRegion.getText()).toString(),
                Objects.requireNonNull(etCountry.getText()).toString()));
    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();
        unbinder.unbind();
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        presenter.onDestroy();
        progressDialog.dismiss();
    }

    @Override
    public void showProgress(String title, String message) {
        progressDialog.setCancelable(false);
        progressDialog.setIndeterminate(false);
        progressDialog.setTitle(title);
        progressDialog.setMessage(message);
        progressDialog.show();
    }

    @Override
    public void hideProgress() {
        progressDialog.hide();
    }

    @Override
    public void showSnackMessage(String message) {
        Snackbar.make(coordinatorLayout, message, Snackbar.LENGTH_SHORT).show();
    }

    @Override
    public void showDetails(AppCollateralAddress address) {
        acSubjPropType.setAdapter(GlobalFunctions.GET_ADDRESS_TYPE_ADAPTER(acSubjPropType, address.getAppAddressType(), Objects.requireNonNull(getActivity())));
        acSubjPropType.setOnClickListener(view -> acSubjPropType.showDropDown());
        acSubjPropType.setEnabled(false);

        etLotNo.setText(address.getAppLotNo());
        etBlockNo.setText(address.getAppBlockNo());
        etStreetName.setText(address.getAppStreetName());
        etVillage.setText(address.getAppVillage());
        etBrgy.setText(address.getAppBrgy());
        etZip.setText(address.getAppZip());
        etDistrict.setText(address.getAppDistrict());
        etCity.setText(address.getAppCity());
        etProvince.setText(address.getAppProvince());
        etRegion.setText(address.getAppRegion());
        etCountry.setText(address.getAppCountry());
        etCountry.setEnabled(false);
        etCountry.setFocusable(false);
    }

    @Override
    public void showExitDialog(String title, String message) {
        new AlertDialog.Builder(Objects.requireNonNull(getContext()))
                .setCancelable(false)
                .setTitle(title)
                .setMessage(message)
                .setPositiveButton("EXIT", (dialogInterface, i) -> Objects.requireNonNull(getDialog()).dismiss())
                .create()
                .show();
    }

    @Override
    public void showErrorDialog(String title, String message) {
        new AlertDialog.Builder(Objects.requireNonNull(getContext()))
                .setCancelable(false)
                .setTitle(title)
                .setMessage(message)
                .setPositiveButton("CLOSE", null)
                .create()
                .show();
    }

    @Override
    public boolean onMenuItemClick(MenuItem item) {
        if (item.getItemId() == R.id.save) {
            presenter.updateDetails(uniqueId,
                    acSubjPropType.getText().toString(),
                    Objects.requireNonNull(etLotNo.getText()).toString(),
                    Objects.requireNonNull(etBlockNo.getText()).toString(),
                    Objects.requireNonNull(etStreetName.getText()).toString(),
                    Objects.requireNonNull(etVillage.getText()).toString(),
                    Objects.requireNonNull(etBrgy.getText()).toString(),
                    Objects.requireNonNull(etZip.getText()).toString(),
                    Objects.requireNonNull(etDistrict.getText()).toString(),
                    Objects.requireNonNull(etCity.getText()).toString(),
                    Objects.requireNonNull(etProvince.getText()).toString(),
                    Objects.requireNonNull(etRegion.getText()).toString(),
                    Objects.requireNonNull(etCountry.getText()).toString());
        }
        return false;
    }
}
