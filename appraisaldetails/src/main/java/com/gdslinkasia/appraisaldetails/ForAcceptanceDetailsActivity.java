package com.gdslinkasia.appraisaldetails;

import android.annotation.SuppressLint;
import android.app.ProgressDialog;
import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.MenuItem;
import android.view.View;
import android.widget.AutoCompleteTextView;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AlertDialog;
import androidx.coordinatorlayout.widget.CoordinatorLayout;
import androidx.fragment.app.FragmentTransaction;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.gdslinkasia.appraisaldetails.contract.for_acceptance.ForAcceptanceContract;
import com.gdslinkasia.appraisaldetails.contract.jobdetails.JobDetailContract;
import com.gdslinkasia.appraisaldetails.contract.jobdetails.JobDetailPresenter;
import com.gdslinkasia.base.adapter.SubjPropertyAddressAdapter;
import com.gdslinkasia.base.adapter.click_listener.AppCollateralAddressClickListener;
import com.gdslinkasia.base.model.details.AppCollateralAddress;
import com.gdslinkasia.base.model.job.Record;
import com.gdslinkasia.base.utils.GlobalFunctions;
import com.gdslinkasia.base.utils.GlobalString;
import com.gdslinkasia.commonresources.R2;
import com.google.android.material.snackbar.Snackbar;
import com.google.android.material.textfield.TextInputEditText;
import com.google.android.material.textfield.TextInputLayout;

import java.text.ParseException;
import java.util.Date;
import java.util.List;
import java.util.Objects;

import javax.inject.Inject;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import dagger.android.support.DaggerAppCompatActivity;

import static com.gdslinkasia.base.database.DatabaseUtils.APPRAISAL_TYPE;
import static com.gdslinkasia.base.database.DatabaseUtils.RECORD_ID;
import static com.gdslinkasia.base.database.DatabaseUtils.UNIQUE_ID;
import static com.gdslinkasia.base.utils.GlobalFunctions.GET_BASIS_OF_VALUE_ADAPTER;
import static com.gdslinkasia.base.utils.GlobalFunctions.GET_CLIENT_NAME_VISIBILITY_FROM_COMPANY_CHECKBOX;
import static com.gdslinkasia.base.utils.GlobalFunctions.GET_CLIENT_TITLE_ADAPTER;
import static com.gdslinkasia.base.utils.GlobalFunctions.GET_COMPANY_VISIBILITY_FROM_COMPANY_CHECKBOX;
import static com.gdslinkasia.base.utils.GlobalFunctions.GET_PURPOSE_APPRAISAL_ADAPTER;
import static com.gdslinkasia.base.utils.GlobalFunctions.ONCHANGE_VISIBILITY_COMPANY_CHECKBOX;
import static com.gdslinkasia.base.utils.GlobalFunctions.dayNumberFormat;
import static com.gdslinkasia.base.utils.GlobalFunctions.fullFormat;
import static com.gdslinkasia.base.utils.GlobalFunctions.getCheckboxBoolean;
import static com.gdslinkasia.base.utils.GlobalFunctions.monthNumberFormat;
import static com.gdslinkasia.base.utils.GlobalFunctions.yearNumberFormat;
import static com.gdslinkasia.base.utils.GlobalString.APPRAISAL_TYPE_SENTENCE_CHANGER;

public class ForAcceptanceDetailsActivity extends DaggerAppCompatActivity implements JobDetailContract.View, ForAcceptanceContract.View, AppCollateralAddressClickListener {

    Intent intent;
    String recordId, appraisalType;

    @BindView(R2.id.coordinatorLayout) CoordinatorLayout coordinatorLayout;

    @BindView(R2.id.appReqFormHeader) TextView appReqFormHeader;

    @BindView(R2.id.txDateReqMonth) TextInputLayout txDateReqMonth;
    @BindView(R2.id.dateReqMonth) TextInputEditText dateReqMonth;

    @BindView(R2.id.txDateReqDay) TextInputLayout txDateReqDay;
    @BindView(R2.id.dateReqDay) TextInputEditText dateReqDay;

    @BindView(R2.id.txDateReqYear) TextInputLayout txDateReqYear;
    @BindView(R2.id.dateReqYear) TextInputEditText dateReqYear;

    @BindView(R2.id.txContractMonth) TextInputLayout txContractMonth;
    @BindView(R2.id.etContractDateMonth) TextInputEditText etContractDateMonth;

    @BindView(R2.id.txContractDay) TextInputLayout txContractDay;
    @BindView(R2.id.etContractDay) TextInputEditText etContractDay;

    @BindView(R2.id.txContractYear) TextInputLayout txContractYear;
    @BindView(R2.id.etContractYear) TextInputEditText etContractYear;

    @BindView(R2.id.txFirstName) TextInputLayout txFirstName;
    @BindView(R2.id.etFirstName) TextInputEditText etFirstName;

    @BindView(R2.id.txMiddleName) TextInputLayout txMiddleName;
    @BindView(R2.id.etMiddleName) TextInputEditText etMiddleName;

    @BindView(R2.id.txLastName) TextInputLayout txLastName;
    @BindView(R2.id.etLastName) TextInputEditText etLastName;

    @BindView(R2.id.txCompanyName) TextInputLayout txCompanyName;
    @BindView(R2.id.etCompanyName) TextInputEditText etCompanyName;

    @BindView(R2.id.txAuthorizingOfficer) TextInputLayout txAuthOfficer;
    @BindView(R2.id.etAuthOfficer) TextInputEditText etAuthOfficer;

    @BindView(R2.id.txClientDesignation) TextInputLayout txClientDesignation;
    @BindView(R2.id.etClientDesignation) TextInputEditText etClientDesignation;

    @BindView(R2.id.txClientAddress) TextInputLayout txClientAddress;
    @BindView(R2.id.etClientAddress) TextInputEditText etClientAddress;

    @BindView(R2.id.txContactNo) TextInputLayout txContactMobNo;
    @BindView(R2.id.etContactMobNo) TextInputEditText etContactMobNo;

    @BindView(R2.id.txContactEmailAdd) TextInputLayout txContactEmailAdd;
    @BindView(R2.id.etContactEmailAdd) TextInputEditText etContactEmailAdd;

    @BindView(R2.id.txIntendedUsers) TextInputLayout txIntendedUsers;
    @BindView(R2.id.etIntendedUsers) TextInputEditText etIntendedUsers;

    @BindView(R2.id.txOtherIntended) TextInputLayout txOtherIntended;
    @BindView(R2.id.etOtherIntendedUsers) TextInputEditText etOtherIntendedUsers;

    @BindView(R2.id.txRequestorName) TextInputLayout txRequestorName;
    @BindView(R2.id.etRequestorName) TextInputEditText etRequestorName;

    @BindView(R2.id.txRequestorNumber) TextInputLayout txReqContactNo;
    @BindView(R2.id.etReqContactNo) TextInputEditText etReqContactNo;

    //TODO OTHER CONTACT HEADER + LINE BORDER
    @BindView(R2.id.otherContactHeader) TextView otherContactHeader;
    @BindView(R2.id.otherContactLine) View otherContactLine;

    @BindView(R2.id.txRequestorDesignation) TextInputLayout txRequestorDesignation;
    @BindView(R2.id.etRequestorDesignation) TextInputEditText etRequestorDesignation;

    @BindView(R2.id.txRequestorEmailAdd) TextInputLayout txReqEmailAdd;
    @BindView(R2.id.etReqEmailAdd) TextInputEditText etReqEmailAdd;

    @BindView(R2.id.basisOfValueHeader) TextView basisOfValueHeader;
    @BindView(R2.id.basisOfValueLine) View basisOfValueLine;

    @BindView(R2.id.txInspectMonth) TextInputLayout txInspectMonth;
    @BindView(R2.id.etInspectMonth) TextInputEditText etInspectMonth;

    @BindView(R2.id.txInspectDay) TextInputLayout txInspectDay;
    @BindView(R2.id.etInspectDay) TextInputEditText etInspectDay;

    @BindView(R2.id.txInspectYear) TextInputLayout txInspectYear;
    @BindView(R2.id.etInspectYear) TextInputEditText etInspectYear;

    @BindView(R2.id.ivInspectDate) ImageView ivInspectDate;

    @BindView(R2.id.valuationDateHeader) TextView valuationDateHeader;
    @BindView(R2.id.txValuationMonth) TextInputLayout txValuationMonth;
    @BindView(R2.id.etValuationMonth) TextInputEditText etValuationMonth;

    @BindView(R2.id.txValuationDay) TextInputLayout txValuationDay;
    @BindView(R2.id.etValuationDay) TextInputEditText etValuationDay;

    @BindView(R2.id.txValuationYear) TextInputLayout txValuationYear;
    @BindView(R2.id.etValuationYear) TextInputEditText etValuationYear;

    @BindView(R2.id.ivValuationDate) ImageView ivValuationDate;

    @BindView(R2.id.txZonalValue) TextInputLayout txZonalValue;
    @BindView(R2.id.etZonalValue) TextInputEditText etZonalValue;

    @BindView(R2.id.txControlNo) TextInputLayout txControlNo;
    @BindView(R2.id.etControlNo) TextInputEditText etControlNo;

    @BindView(R2.id.txPropertyType) TextInputLayout txPropertyType;
    @BindView(R2.id.etPropertyType) TextInputEditText etPropertyType;

    @BindView(R2.id.txAppNatureAppraisalOther) TextInputLayout txAppNatureAppraisalOther;
    @BindView(R2.id.etAppNatureAppraisalOther)  TextInputEditText etAppNatureAppraisalOther;

    @BindView(R2.id.txOwnerSubjectProp) TextInputLayout txOwnerSubjectProp;
    @BindView(R2.id.etOwnerSubjProp) TextInputEditText etOwnerSubjProp;

    @BindView(R2.id.rvOtherContacts) RecyclerView rvOtherContacts;
    @BindView(R2.id.tvNoOtherContacts) TextView tvOtherContacts;

    @BindView(R2.id.rvAddresses) RecyclerView rvAddresses;
    @BindView(R2.id.tvNoAddresses) TextView tvNoCollateralAddress;

    @BindView(R2.id.txTitle) TextInputLayout txTitle;
    @BindView(R2.id.acTitle) AutoCompleteTextView acTitle;

    @BindView(R2.id.txClientType) TextInputLayout txClientType;
    @BindView(R2.id.acClientType) AutoCompleteTextView acClientType;

    @BindView(R2.id.txBasisOfValue) TextInputLayout txBasisOfValue;
    @BindView(R2.id.acBasisOfValue) AutoCompleteTextView acBasisOfValue;

    @BindView(R2.id.txAppraisalType) TextInputLayout txAppraisalType;
    @BindView(R2.id.acAppraisalType) AutoCompleteTextView acAppraisalType;

    @BindView(R2.id.txPurposeVal) TextInputLayout txPurposeVal;
    @BindView(R2.id.acPurposeOfValuation) AutoCompleteTextView acPurposeOfValuation; //, acSubjPropType

    @BindView(R2.id.cbCompany) CheckBox cbCompany;

    @BindView(R2.id.container_clientName) LinearLayout containerClientName;
    @BindView(R2.id.container_companyName) LinearLayout containerCompanyName;

//    //TODO CLIENT DETAILS
//    @BindView(R2.id.txt_name)
//    TextView clientName;
//    @BindView(R2.id.txt_client_designation) TextView clientDesignation;
//    @BindView(R2.id.txt_email) TextView email;
//    @BindView(R2.id.txt_phone) TextView phone;
//
//    @BindView(R2.id.txt_request_date) TextView requestedDate;
//    @BindView(R2.id.txt_contract_date) TextView contractDate;
//
//    @BindView(R2.id.txt_address) TextView address;

    @BindView(R2.id.btnSubmit) Button btnSubmit;
    @BindView(R2.id.btnRefreshDetail) Button btnRefreshDetail;

    JobDetailContract.Presenter presenter;

    @Inject
    ForAcceptanceContract.Presenter forAcceptancePresenter;

    ProgressDialog progressDialog;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_for_acceptance);
        ButterKnife.bind(this);

        intent = getIntent();
        recordId = intent.getStringExtra(RECORD_ID);
        appraisalType = intent.getStringExtra(APPRAISAL_TYPE);

        Objects.requireNonNull(getSupportActionBar()).setTitle("Job Acceptance Details");
        getSupportActionBar().setSubtitle(appraisalType);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);

        progressDialog = new ProgressDialog(this);

        //TODO REQUESTED DATE
        txDateReqMonth.setEnabled(false);
        txDateReqDay.setEnabled(false);
        txDateReqYear.setEnabled(false);

        //TODO CONTRACT DATE
        txContractMonth.setEnabled(false);
        txContractDay.setEnabled(false);
        txContractYear.setEnabled(false);

        //TODO COMPANY CHECKBOX
        cbCompany.setEnabled(false);

        //TODO CLIENT NAME
        txTitle.setEnabled(false);
        acTitle.setEnabled(false);
        txFirstName.setEnabled(false);
        etFirstName.setEnabled(false);
        txMiddleName.setEnabled(false);
        etMiddleName.setEnabled(false);
        txLastName.setEnabled(false);
        etLastName.setEnabled(false);

        //TODO COMPANY NAME
        txCompanyName.setEnabled(false);
        etCompanyName.setEnabled(false);
        txAuthOfficer.setEnabled(false);
        etAuthOfficer.setEnabled(false);

        //TODO CLIENT DESIGNATION, CLIENT ADDRESS
        txClientDesignation.setEnabled(false);
        etClientDesignation.setEnabled(false);
        txClientAddress.setEnabled(false);
        etClientAddress.setEnabled(false);

        //TODO CONTACT MOBILE NO., CONTACT EMAIL ADDRESS
        txContactMobNo.setEnabled(false);
        etContactMobNo.setEnabled(false);
        txContactEmailAdd.setEnabled(false);
        etContactEmailAdd.setEnabled(false);

        //TODO OTHER CONTACTS
        otherContactHeader.setVisibility(View.GONE);
        otherContactLine.setVisibility(View.GONE);
        rvOtherContacts.setVisibility(View.GONE);
        tvOtherContacts.setVisibility(View.GONE);

        txClientType.setEnabled(false);
        acClientType.setEnabled(false);

        txIntendedUsers.setEnabled(false);
        etIntendedUsers.setEnabled(false);

        txOtherIntended.setEnabled(false);
        etOtherIntendedUsers.setEnabled(false);

        //TODO REQUESTOR DETAILS
        txRequestorName.setEnabled(false);
        etRequestorName.setEnabled(false);
        txRequestorDesignation.setEnabled(false);
        etRequestorDesignation.setEnabled(false);
        txReqContactNo.setEnabled(false);
        etReqContactNo.setEnabled(false);
        txReqEmailAdd.setEnabled(false);
        etReqEmailAdd.setEnabled(false);

        //TODO BASIS OF VALUE
        basisOfValueHeader.setVisibility(View.GONE);
        basisOfValueLine.setVisibility(View.GONE);
        //TODO INSPECTED DATE
        txInspectMonth.setEnabled(false);
        etInspectMonth.setEnabled(false);
        txInspectDay.setEnabled(false);
        etInspectDay.setEnabled(false);
        txInspectYear.setEnabled(false);
        etInspectYear.setEnabled(false);
        ivInspectDate.setEnabled(false);

        //TODO VALUATION DATE
        valuationDateHeader.setVisibility(View.GONE);
        txValuationMonth.setEnabled(false);
        txValuationMonth.setVisibility(View.GONE);
        etValuationMonth.setEnabled(false);
        etValuationMonth.setVisibility(View.GONE);
        txValuationDay.setEnabled(false);
        txValuationDay.setVisibility(View.GONE);
        etValuationDay.setEnabled(false);
        etValuationDay.setVisibility(View.GONE);
        txValuationYear.setEnabled(false);
        txValuationYear.setVisibility(View.GONE);
        etValuationYear.setEnabled(false);
        etValuationYear.setVisibility(View.GONE);
        ivValuationDate.setEnabled(false);
        ivValuationDate.setVisibility(View.GONE);

        txBasisOfValue.setEnabled(false);
        txBasisOfValue.setVisibility(View.GONE);
        acBasisOfValue.setEnabled(false);
        acBasisOfValue.setVisibility(View.GONE);
        txZonalValue.setEnabled(false);
        txZonalValue.setVisibility(View.GONE);
        etZonalValue.setEnabled(false);
        etZonalValue.setVisibility(View.GONE);

        //TODO CONTROL NUMBER, PROPERTY TYPE, APPRAISAL TYPE, PURPOSE OF VALUATION
        txControlNo.setEnabled(false);
        etControlNo.setEnabled(false);

        txPropertyType.setEnabled(false);
        etPropertyType.setEnabled(false);

        txAppraisalType.setEnabled(false);
        acAppraisalType.setEnabled(false);

        txPurposeVal.setEnabled(false);
        acPurposeOfValuation.setEnabled(false);

        txAppNatureAppraisalOther.setEnabled(false);
        etAppNatureAppraisalOther.setEnabled(false);

        txOwnerSubjectProp.setEnabled(false);
        etOwnerSubjProp.setEnabled(false);




//        clientName.setVisibility(View.INVISIBLE);
//        clientDesignation.setVisibility(View.INVISIBLE);
//        email.setVisibility(View.INVISIBLE);
//        phone.setVisibility(View.INVISIBLE);
//
//        requestedDate.setVisibility(View.INVISIBLE);
//        contractDate.setVisibility(View.INVISIBLE);
//
//        address.setVisibility(View.INVISIBLE);

        presenter = new JobDetailPresenter(this, getApplication());
        forAcceptancePresenter.takeView(this);
    }

    @Override
    public boolean onOptionsItemSelected(@NonNull MenuItem item) {
        if (item.getItemId() == android.R.id.home) {
            finish();
        }
        return super.onOptionsItemSelected(item);
    }

    @Override
    protected void onResume() {
        super.onResume();
        presenter.onStart(recordId);
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        progressDialog.dismiss();
        presenter.onDestroy();
    }

    @Override
    public void showProgress(String title, String message) {
        progressDialog.setCancelable(false);
        progressDialog.setIndeterminate(false);
        progressDialog.setTitle(title);
        progressDialog.setMessage(message);
        progressDialog.show();
    }

    @Override
    public void hideProgress() {
        progressDialog.hide();
    }

    @Override
    public void showSnackMessage(String message) {
        Snackbar.make(coordinatorLayout, message, Snackbar.LENGTH_SHORT).show();
    }

    @SuppressLint("SetTextI18n")
    @Override
    public void showJobDetails(Record record) {
        forAcceptancePresenter.requestAddresses(recordId);

//        clientName.setText(String.format("%s %s %s", record.getAppAccountFirstName(), record.getAppAccountMiddleName(), record.getAppAccountLastName()));
//        clientDesignation.setText(record.getAppAccountDesignation());
//        email.setText(record.getAppAccountBusinessEmail());
//        phone.setText(record.getAppAccountBusinessContact());
//
//        clientName.setVisibility(View.VISIBLE);
//        clientDesignation.setVisibility(View.VISIBLE);
//        email.setVisibility(View.VISIBLE);
//        phone.setVisibility(View.VISIBLE);
//
//        requestedDate.setText(String.format("%s - %s - %s", record.getAppDaterequestedMonth(), record.getAppDaterequestedDay(), record.getAppDaterequestedYear()));
//        contractDate.setText(String.format("%s - %s - %s", record.getAppDateContractMonth(), record.getAppDateContractDay(), record.getAppDateContractYear()));
//        address.setText(String.format("%s %s %s %s %s %s %s %s %s %s %s", record.getAppraisalRequest().get(0).getAppCollateralAddress().get(0).getAppLotNo(), record.getAppraisalRequest().get(0).getAppCollateralAddress().get(0).getAppBlockNo(), record.getAppraisalRequest().get(0).getAppCollateralAddress().get(0).getAppStreetName(), record.getAppraisalRequest().get(0).getAppCollateralAddress().get(0).getAppVillage(), record.getAppraisalRequest().get(0).getAppCollateralAddress().get(0).getAppBrgy(), record.getAppraisalRequest().get(0).getAppCollateralAddress().get(0).getAppZip(), record.getAppraisalRequest().get(0).getAppCollateralAddress().get(0).getAppDistrict(), record.getAppraisalRequest().get(0).getAppCollateralAddress().get(0).getAppCity(), record.getAppraisalRequest().get(0).getAppCollateralAddress().get(0).getAppProvince(), record.getAppraisalRequest().get(0).getAppCollateralAddress().get(0).getAppRegion(), record.getAppraisalRequest().get(0).getAppCollateralAddress().get(0).getAppCountry()));
//
//        requestedDate.setVisibility(View.VISIBLE);
//        contractDate.setVisibility(View.VISIBLE);
//        address.setVisibility(View.VISIBLE);

        appReqFormHeader.setText("APPRAISAL REQUEST FORM");

        dateReqMonth.setText(String.format("%s", record.getAppDaterequestedMonth()));
        dateReqDay.setText(record.getAppDaterequestedDay());
        dateReqYear.setText(record.getAppDaterequestedYear());

        etContractDateMonth.setText(record.getAppDateContractMonth());
        etContractDay.setText(record.getAppDateContractDay());
        etContractYear.setText(record.getAppDateContractYear());

        cbCompany.setChecked(getCheckboxBoolean(record.getAppAccountCompany()));
        containerClientName.setVisibility(GET_CLIENT_NAME_VISIBILITY_FROM_COMPANY_CHECKBOX(cbCompany));
        containerCompanyName.setVisibility(GET_COMPANY_VISIBILITY_FROM_COMPANY_CHECKBOX(cbCompany));
        ONCHANGE_VISIBILITY_COMPANY_CHECKBOX(cbCompany, containerClientName, containerCompanyName);

        acTitle.setText(record.getAppAccountNameTitle());
        acTitle.setEnabled(false);
//        acTitle.setFocusable(false);
        acTitle.setAdapter(GET_CLIENT_TITLE_ADAPTER(record.getAppAccountNameTitle(), this));
        acTitle.setOnClickListener(v -> acTitle.showDropDown());

        etFirstName.setText(record.getAppAccountFirstName());
        etMiddleName.setText(record.getAppAccountMiddleName());
        etLastName.setText(record.getAppAccountLastName());

        etCompanyName.setText(record.getAppAccountFirstName());
        etAuthOfficer.setText(record.getAppAccountAuthroizingOfficer());

        etClientDesignation.setText(record.getAppAccountDesignation());
        etClientAddress.setText(record.getAppAccountBusinessAddress());

        etContactMobNo.setText(record.getAppAccountBusinessContact());
        etContactEmailAdd.setText(record.getAppAccountBusinessEmail());

        acClientType.setText(record.getAppIntendedUsersName());
        acClientType.setEnabled(false);
        acClientType.setAdapter(GlobalFunctions.GET_CLIENT_TYPE_ADAPTER(record.getAppIntendedUsersName(), this));
        acClientType.setOnClickListener(view -> acClientType.showDropDown());
        acClientType.setOnItemClickListener(new GlobalFunctions.AutoCompleteClickListener(acClientType, txAppNatureAppraisalOther, etAppNatureAppraisalOther));

        //TODO options: getAppIntendedUsersName() or getAppAccountFirstName()
        etIntendedUsers.setText(record.getAppAccountFirstName());
        etIntendedUsers.setEnabled(false);
        etIntendedUsers.setFocusable(false);

        etOtherIntendedUsers.setText(record.getAppIntendedUsersOthers());

        etRequestorName.setText(record.getAppValuerName());
        etRequestorName.setKeyListener(null);
        etRequestorDesignation.setText(record.getAppValuerOfficerType());
        etRequestorDesignation.setKeyListener(null);
        etReqContactNo.setText(record.getAppValuerContactNumber());
        etReqContactNo.setKeyListener(null);
        etReqEmailAdd.setText(record.getAppValuerEmailAddress());
        etReqEmailAdd.setKeyListener(null);

        try {
            Date inspectionDate = fullFormat.parse(record.getValrepLandimpDateInspectedMonth());
            String inspectionMonthWord = monthNumberFormat.format(Objects.requireNonNull(inspectionDate));
            String inspectionDayNumber = dayNumberFormat.format(inspectionDate);
            String inspectionYearNumber = yearNumberFormat.format(inspectionDate);

            etInspectMonth.setText(inspectionMonthWord);
            etInspectDay.setText(inspectionDayNumber);
            etInspectYear.setText(inspectionYearNumber);

            Date valuationDate = fullFormat.parse(record.getValrepLandimpValuationDateMonth());
            String valuationMonth = monthNumberFormat.format(Objects.requireNonNull(valuationDate));
            String valuationDayNumber = dayNumberFormat.format(valuationDate);
            String valuationYearNumber = yearNumberFormat.format(valuationDate);
            etValuationMonth.setText(valuationMonth);
            etValuationDay.setText(valuationDayNumber);
            etValuationYear.setText(valuationYearNumber);
        } catch (ParseException e) {
            e.printStackTrace();
        }


//        acBasisOfValue.setText(record.getValrepAppraisalBasisOfValue());
        acBasisOfValue.setAdapter(GET_BASIS_OF_VALUE_ADAPTER(record.getValrepAppraisalBasisOfValue(), this));
        acBasisOfValue.setOnClickListener(view -> acBasisOfValue.showDropDown());
        acBasisOfValue.setEnabled(false);

        etZonalValue.setText(record.getValrepLandimpZonalValue());

        etControlNo.setText(record.getAppraisalRequest().get(0).getAppControlNo());
        etControlNo.setKeyListener(null);
        //String appraisalTypeSentenceOriginal = record.getAppraisalRequest().get(0).getAppRequestAppraisal();
        String appraisalTypeSentenceChanger = APPRAISAL_TYPE_SENTENCE_CHANGER(record.getAppraisalRequest().get(0).getAppRequestAppraisal());
        etPropertyType.setText(appraisalTypeSentenceChanger);
        etPropertyType.setEnabled(false);
        etPropertyType.setFocusable(false);

        acAppraisalType.setText(record.getAppraisalRequest().get(0).getAppNatureAppraisal());
        acAppraisalType.setEnabled(false);
        acAppraisalType.setFocusable(false);

        acPurposeOfValuation.setText(record.getAppraisalRequest().get(0).getAppPurposeAppraisal());
        acPurposeOfValuation.setAdapter(GET_PURPOSE_APPRAISAL_ADAPTER(record.getAppraisalRequest().get(0).getAppPurposeAppraisal(), this, etAppNatureAppraisalOther));
        acPurposeOfValuation.setOnClickListener(view -> acPurposeOfValuation.showDropDown());
        acPurposeOfValuation.setOnItemClickListener(new GlobalFunctions.AutoCompleteClickListener(acPurposeOfValuation, txAppNatureAppraisalOther, etAppNatureAppraisalOther));
        acPurposeOfValuation.setEnabled(false);
//        acPurposeOfValuation.setFocusable(false);

        etAppNatureAppraisalOther.setText(record.getAppraisalRequest().get(0).getAppPurposeAppraisalOther());

        etOwnerSubjProp.setText(record.getPafLandimpDeveloperName());
        Log.d("int--pafDevName", record.getPafLandimpDeveloperName());


    }

    @Override
    public void showSyncOptionDialog(String title, String message) {
        new AlertDialog.Builder(this)
                .setCancelable(false)
                .setTitle(title)
                .setMessage(message)
                .setPositiveButton("SYNC", (dialogInterface, i) -> presenter.requestFirstTimeSyncJobDetails(recordId))
                .setNegativeButton("EXIT", (dialogInterface, i) -> finish())
                .create()
                .show();
    }

    @Override
    public void showExitDialog(String title, String message) {
        new AlertDialog.Builder(this)
                .setCancelable(false)
                .setTitle(title)
                .setMessage(message)
                .setPositiveButton("EXIT", (dialogInterface, i) -> finish())
                .create()
                .show();
    }

    @OnClick(R2.id.btnSubmit)
    void onClickSubmitForReview() {
        new AlertDialog.Builder(this)
                .setCancelable(false)
                .setTitle("Change to Active Job")
                .setMessage("You are about to make this job as active.\n\nShall we proceed?")
                .setPositiveButton("PROCEED", (dialog, which) -> presenter.saveDetailsToServer(recordId, "Accept Job", GlobalString.ACCEPTED_JOB))
                .setNegativeButton("CANCEL", null)
                .create()
                .show();
    }

    @OnClick(R2.id.btnRefreshDetail)
    void onClickRefreshDetail() {
        new AlertDialog.Builder(this)
                .setTitle("Sync From Server")
                .setMessage("This function lets you retrieve details from the server. Shall we proceed?\n\nTo perform this action, your device must be connected to the internet.")
                .setPositiveButton("PROCEED", (dialogInterface, i) -> presenter.requestJobDetails(recordId))
                .setNegativeButton("CANCEL", null)
                .create()
                .show();
    }

    @Override
    public void showCollateralAddress(List<AppCollateralAddress> appCollateralAddresses) {
        tvNoCollateralAddress.setVisibility(View.GONE);
        rvAddresses.setVisibility(View.VISIBLE);

        SubjPropertyAddressAdapter adapter = new SubjPropertyAddressAdapter(this, this);
        rvAddresses.setLayoutManager(new LinearLayoutManager(this));
        adapter.submitList(appCollateralAddresses);

        rvAddresses.setNestedScrollingEnabled(false);

//        rvOtherContacts.setHasFixedSize(true);

        rvAddresses.setAdapter(adapter);
    }

    @Override
    public void showNoCollateralAddress(String message) {
        rvAddresses.setVisibility(View.GONE);
        tvNoCollateralAddress.setVisibility(View.VISIBLE);

        tvNoCollateralAddress.setText(message);
    }

    @Override
    public void getAddressDetails(AppCollateralAddress address) {

    }

    @Override
    public void getAddressReferenceIds(long uniqueId) {
        Log.d("int--addressUniqueId", String.valueOf(uniqueId));
        Bundle bundle = new Bundle();
        bundle.putLong(UNIQUE_ID, uniqueId);

        UpdateSubjPropAddressDialogFragment dialogFragment = new UpdateSubjPropAddressDialogFragment();
        dialogFragment.setArguments(bundle);

        FragmentTransaction fragmentTransaction = getSupportFragmentManager().beginTransaction();
        dialogFragment.show(fragmentTransaction, UpdateSubjPropAddressDialogFragment.TAG);
    }
}
