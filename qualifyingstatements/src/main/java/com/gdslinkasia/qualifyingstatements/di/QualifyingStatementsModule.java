package com.gdslinkasia.qualifyingstatements.di;

import android.app.Application;

import com.gdslinkasia.qualifyingstatements.contract.qualifying_statement_list.QualifyingStatementsContract;
import com.gdslinkasia.qualifyingstatements.contract.qualifying_statement_list.QualifyingStatementsPresenter;

import dagger.Module;
import dagger.Provides;

@Module
public class QualifyingStatementsModule {

    @Provides
    static QualifyingStatementsContract.Presenter presenter(Application application) {
        return new QualifyingStatementsPresenter(application);
    }

}
