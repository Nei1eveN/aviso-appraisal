package com.gdslinkasia.qualifyingstatements;

import android.app.ProgressDialog;
import android.content.Intent;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuItem;
import android.widget.AutoCompleteTextView;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AlertDialog;
import androidx.appcompat.app.AppCompatActivity;

import com.gdslinkasia.base.model.details.ValrepLcIvsQualifyingStatement;
import com.gdslinkasia.commonresources.R2;
import com.gdslinkasia.qualifyingstatements.contract.qualifying_statement_item.QualifyingStatementsItemContract;
import com.gdslinkasia.qualifyingstatements.contract.qualifying_statement_item.QualifyingStatementsItemPresenter;

import java.util.Objects;

import butterknife.BindView;
import butterknife.ButterKnife;

import static com.gdslinkasia.base.database.DatabaseUtils.UNIQUE_ID;

public class QualifyingStatementsItemActivity extends AppCompatActivity implements QualifyingStatementsItemContract.View {

    private long uniqueId;

    @BindView(R2.id.acAddNewFields) AutoCompleteTextView acAddNewFields;

    private ProgressDialog progressDialog;

    private QualifyingStatementsItemContract.Presenter presenter;

//    DatePickerDialog datePickerDialog = new DatePickerDialog();

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.content_add_textfield);
        ButterKnife.bind(this);

        Intent intent = getIntent();
        uniqueId = intent.getLongExtra(UNIQUE_ID, 0);
        int itemPosition = intent.getIntExtra("item_position", 0);

        Objects.requireNonNull(getSupportActionBar()).setTitle("Property Details");
        getSupportActionBar().setSubtitle("Qualifying Statement "+itemPosition);
        getSupportActionBar().setIcon(R.drawable.ic_close_white_24dp);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);

        progressDialog = new ProgressDialog(this);
        presenter = new QualifyingStatementsItemPresenter(this, getApplication());
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.menu_ownership_property_item, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(@NonNull MenuItem item) {
        if (item.getItemId() == android.R.id.home) {
            finish();
        } else if (item.getItemId() == R.id.delete) {
            new AlertDialog.Builder(this).setCancelable(false).setTitle("Delete Record").setMessage("You are about to remove this record. Shall we proceed?").setPositiveButton("DELETE", (dialog, which) -> presenter.deleteItem(uniqueId)).setNegativeButton("CANCEL", null).create().show();
        }
        return super.onOptionsItemSelected(item);
    }

    @Override
    protected void onResume() {
        super.onResume();
        presenter.onStart(uniqueId);
    }

    @Override
    protected void onPause() {
        super.onPause();
        presenter.updateItemDetails(
                uniqueId,
                acAddNewFields.getText().toString());
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        presenter.onDestroy();
        progressDialog.dismiss();
    }

    @Override
    public void showProgress(String title, String message) {
        progressDialog.setCancelable(false);
        progressDialog.setIndeterminate(false);
        progressDialog.setTitle(title);
        progressDialog.setMessage(message);
        progressDialog.show();
    }

    @Override
    public void hideProgress() {
        progressDialog.hide();
    }

    @Override
    public void showDetails(ValrepLcIvsQualifyingStatement detail) {
        acAddNewFields.setText(detail.getLcIvsQualifyingStatementsRemarks());
    }

    @Override
    public void showExitDialog(String title, String message) {
        new AlertDialog.Builder(this).setCancelable(false).setTitle(title).setMessage(message).setPositiveButton("CLOSE", (dialog, which) -> finish()).create().show();
    }

    @Override
    public void finishActivity() {
        finish();
    }

    @Override
    public void showToastMessage(String message) {
        Toast.makeText(this, message, Toast.LENGTH_SHORT).show();
    }
}
