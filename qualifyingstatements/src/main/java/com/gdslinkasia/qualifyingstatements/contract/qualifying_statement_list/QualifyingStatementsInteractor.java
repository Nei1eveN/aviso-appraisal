package com.gdslinkasia.qualifyingstatements.contract.qualifying_statement_list;

import android.app.Application;

import com.gdslinkasia.base.database.repository.QualifyingStatementLocalRepository;
import com.gdslinkasia.base.model.details.ValrepLcIvsQualifyingStatement;
import com.gdslinkasia.base.model.job.Record;

import java.util.List;

import io.reactivex.Flowable;
import io.reactivex.Single;

public class QualifyingStatementsInteractor implements QualifyingStatementsContract.Interactor {

    private QualifyingStatementLocalRepository localRepository;

    public QualifyingStatementsInteractor(Application application) {
        localRepository = new QualifyingStatementLocalRepository(application);
    }

    @Override
    public Single<List<ValrepLcIvsQualifyingStatement>> getOwnershipPropertyDescList(String recordId) {
        return localRepository.getOwnershipPropertyDescList(recordId);
    }

    @Override
    public Flowable<List<ValrepLcIvsQualifyingStatement>> getListFlowable(String recordId) {
        return localRepository.getListFlowable(recordId);
    }

    @Override
    public void insertLotDetail(ValrepLcIvsQualifyingStatement detail) {
        localRepository.insertLotDetail(detail);
    }

    @Override
    public Single<Record> getRecordByRecordId(String recordId) {
        return localRepository.getRecordByRecordId(recordId);
    }

    @Override
    public void onDestroy() {
        localRepository.onDestroy();
    }
}
