package com.gdslinkasia.qualifyingstatements.contract.qualifying_statement_list;

import android.app.Application;
import android.os.Handler;
import android.util.Log;
import android.view.View;

import com.gdslinkasia.base.model.details.ValrepLcIvsQualifyingStatement;
import com.gdslinkasia.base.model.job.Record;

import java.util.List;

import io.reactivex.Flowable;
import io.reactivex.Single;
import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.disposables.CompositeDisposable;
import io.reactivex.functions.Consumer;
import io.reactivex.observers.DisposableSingleObserver;
import io.reactivex.schedulers.Schedulers;

public class QualifyingStatementsPresenter implements QualifyingStatementsContract.Presenter {

    private QualifyingStatementsContract.View view;
    private QualifyingStatementsContract.Interactor interactor;
    private CompositeDisposable compositeDisposable = new CompositeDisposable();

    public QualifyingStatementsPresenter(Application application) {
        this.interactor = new QualifyingStatementsInteractor(application);
    }

    @Override
    public void onStart() {
        view.showEmptyState("Loading Qualifying Statements");
    }

    @Override
    public void onResume(String recordId) {
        view.showProgress("Loading Items", "Please wait...");
        compositeDisposable.add(getSingleListObservable(recordId).subscribeWith(getSingleListObserver(recordId)));
    }

    @Override
    public void addNewLotDetail(String recordId, int recyclerViewVisibility, int tvNoRecordVisibility) {
        compositeDisposable.add(getRecordByRecordId(recordId).subscribeWith(addLotDetailObservable(recyclerViewVisibility, tvNoRecordVisibility)));
    }

    private Single<List<ValrepLcIvsQualifyingStatement>> getSingleListObservable(String recordId) {
        return interactor.getOwnershipPropertyDescList(recordId)
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread());
    }

    private DisposableSingleObserver<List<ValrepLcIvsQualifyingStatement>> getSingleListObserver(String recordId) {
        return new DisposableSingleObserver<List<ValrepLcIvsQualifyingStatement>>() {
            @Override
            public void onSuccess(List<ValrepLcIvsQualifyingStatement> valrepLandimpLotDetails) {
                if (!valrepLandimpLotDetails.isEmpty()) {
//                    view.showList(valrepLandimpLotDetails);
                    Log.d("int--qualStatement", "Not Empty: Logic Triggered");
                    compositeDisposable.add(getListFlowable(recordId).subscribe(getFlowableObserver()));
                } else {
                    Log.d("int--qualStatement", "List Empty: Logic Triggered");
                    view.showEmptyState("There are no records available.");
                }
                new Handler().postDelayed(() -> view.hideProgress(), 1000);
            }

            @Override
            public void onError(Throwable e) {
                view.showEmptyState(e.getMessage());
                new Handler().postDelayed(() -> view.hideProgress(), 1000);
            }
        };
    }

    private Flowable<List<ValrepLcIvsQualifyingStatement>> getListFlowable(String recordId) {
        return interactor.getListFlowable(recordId)
                .subscribeOn(Schedulers.computation())
                .observeOn(AndroidSchedulers.mainThread());
    }

    private Consumer<List<ValrepLcIvsQualifyingStatement>> getFlowableObserver() {
        return valrepLandimpLotDetails -> {
            view.showList(valrepLandimpLotDetails);
            Log.d("int--flowableSize", String.valueOf(valrepLandimpLotDetails.size()));
        };
    }

    //TODO RECORD OBJECT FETCHING
    private Single<Record> getRecordByRecordId(String recordId) {
        return interactor.getRecordByRecordId(recordId)
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread());
    }

    private DisposableSingleObserver<Record> addLotDetailObservable(int recyclerViewVisibility, int tvNoRecordVisibility) {
        return new DisposableSingleObserver<Record>() {
            @Override
            public void onSuccess(Record record) {
                if (recyclerViewVisibility == View.VISIBLE && tvNoRecordVisibility == View.INVISIBLE) {
                    ValrepLcIvsQualifyingStatement detail = new ValrepLcIvsQualifyingStatement();
                    detail.setRecordId(record.getRecordId());
                    detail.setRecordUId(record.getUniqueId());

                    interactor.insertLotDetail(detail);
                } else {
                    Log.d("int--qual-presenter", "onResume called: Logic triggered");
                    ValrepLcIvsQualifyingStatement detail = new ValrepLcIvsQualifyingStatement();
                    detail.setRecordId(record.getRecordId());
                    detail.setRecordUId(record.getUniqueId());

                    interactor.insertLotDetail(detail);

                    //TODO CALL onResume if RecyclerView visibility is GONE
                    onResume(record.getRecordId());
                }
            }

            @Override
            public void onError(Throwable e) {
                e.printStackTrace();
                view.showErrorDialog("Something Went Wrong", e.getMessage());
            }
        };
    }

    @Override
    public void takeView(QualifyingStatementsContract.View view) {
        this.view = view;
    }

    @Override
    public void dropView() {
        view = null;
        compositeDisposable.clear();
        interactor.onDestroy();
    }
}
