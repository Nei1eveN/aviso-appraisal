package com.gdslinkasia.qualifyingstatements.contract.qualifying_statement_list;

import com.gdslinkasia.base.BasePresenter;
import com.gdslinkasia.base.BaseView;
import com.gdslinkasia.base.model.details.ValrepLcIvsQualifyingStatement;
import com.gdslinkasia.base.model.job.Record;

import java.util.List;

import io.reactivex.Flowable;
import io.reactivex.Single;

public interface QualifyingStatementsContract {

    interface View extends BaseView<Presenter> {
        void showProgress(String title, String message);

        void hideProgress();

        void showList(List<ValrepLcIvsQualifyingStatement> valrepLandimpLotDetails);

        void showEmptyState(String message);

        void showErrorDialog(String title, String message);
    }

    interface Presenter extends BasePresenter<View> {
        void onStart();

        void onResume(String recordId);

        void addNewLotDetail(String recordId, int recyclerViewVisibility, int tvNoRecordVisibility);
    }

    interface Interactor {
        Single<List<ValrepLcIvsQualifyingStatement>> getOwnershipPropertyDescList(String recordId);

        Flowable<List<ValrepLcIvsQualifyingStatement>> getListFlowable(String recordId);

        void insertLotDetail(ValrepLcIvsQualifyingStatement detail);

        Single<Record> getRecordByRecordId(String recordId);

        void onDestroy();
    }

}
