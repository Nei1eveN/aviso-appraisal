package com.gdslinkasia.qualifyingstatements.contract.qualifying_statement_item;

import com.gdslinkasia.base.model.details.ValrepLcIvsQualifyingStatement;

import io.reactivex.Single;

public interface QualifyingStatementsItemContract {
    interface View {
        void showProgress(String title, String message);

        void hideProgress();

        void showDetails(ValrepLcIvsQualifyingStatement detail);

        void showExitDialog(String title, String message);

        void finishActivity();

        void showToastMessage(String message);
    }

    interface Presenter {
        void onStart(long uniqueId);

        void onDestroy();

        void updateItemDetails(long uniqueId, String valrep_landimp_assumptions);

        void deleteItem(long uniqueId);
    }

    interface Interactor {
        Single<ValrepLcIvsQualifyingStatement> getSpecialAssumptionsItemDetails(long uniqueId);

        void updateLotDetail(ValrepLcIvsQualifyingStatement detail);

        void onDestroy();

        void deleteItem(long uniqueId);
    }
}
