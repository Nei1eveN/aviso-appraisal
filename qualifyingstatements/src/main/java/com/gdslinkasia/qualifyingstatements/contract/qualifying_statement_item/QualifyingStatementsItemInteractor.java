package com.gdslinkasia.qualifyingstatements.contract.qualifying_statement_item;

import android.app.Application;

import com.gdslinkasia.base.database.repository.QualifyingStatementLocalRepository;
import com.gdslinkasia.base.model.details.ValrepLcIvsQualifyingStatement;

import io.reactivex.Single;

public class QualifyingStatementsItemInteractor implements QualifyingStatementsItemContract.Interactor {

    private QualifyingStatementLocalRepository localRepository;

    public QualifyingStatementsItemInteractor(Application application) {
        this.localRepository = new QualifyingStatementLocalRepository(application);
    }

    @Override
    public Single<ValrepLcIvsQualifyingStatement> getSpecialAssumptionsItemDetails(long uniqueId) {
        return localRepository.getPropertyDescDetail(uniqueId);
    }

    @Override
    public void updateLotDetail(ValrepLcIvsQualifyingStatement detail) {
        localRepository.updateLotDetail(detail);
    }

    @Override
    public void onDestroy() {
        localRepository.onDestroy();
    }

    @Override
    public void deleteItem(long uniqueId) {
        localRepository.deleteLotItem(uniqueId);
    }
}
