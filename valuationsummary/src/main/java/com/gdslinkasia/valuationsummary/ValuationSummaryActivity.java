package com.gdslinkasia.valuationsummary;

import android.os.Bundle;
import android.view.View;
import android.widget.TextView;

import androidx.appcompat.app.AppCompatActivity;

import com.gdslinkasia.base.model.job.Record;
import com.gdslinkasia.commonresources.R2;

import butterknife.BindView;
import butterknife.ButterKnife;

import static com.gdslinkasia.base.database.DatabaseUtils.*;

public class ValuationSummaryActivity extends AppCompatActivity implements ValuationSummaryContract.View {

    @BindView(R2.id.acMarketValLand) TextView acMarketValLand;
    @BindView(R2.id.acTotalImp) TextView acTotalImp;
    @BindView(R2.id.acTotalOtherImp) TextView acTotalOtherImp;
    @BindView(R2.id.acTotalAppraised) TextView acTotalAppraised;
    @BindView(R2.id.acValueWords) TextView acValueWords;


    public static String TAG = "ValuationSummaryActivity";

    private String recordId;
    private ValuationSummaryContract.Presenter presenter;
    private String appraisalType;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.form_valuation_summary);

        ButterKnife.bind(this);

        Bundle putExtraBundle = getIntent().getExtras();
        if(putExtraBundle == null) {
            recordId= null;
            return;
        } else {
            recordId= putExtraBundle.getString(RECORD_ID);
            appraisalType= putExtraBundle.getString(APPRAISAL_TYPE);
        }


        if(appraisalType.equals(VAR_LAND_ONLY)){
            acTotalImp.setVisibility(View.GONE);
            acTotalOtherImp.setVisibility(View.GONE);
        }

        else if(appraisalType.equals(VAR_CONDO)){
            acTotalImp.setVisibility(View.GONE);
            acTotalOtherImp.setVisibility(View.GONE);
            acTotalAppraised.setVisibility(View.GONE);
        }

        presenter = new ValuationSummaryPresenterImpl(this, getApplication());

        presenter.onStart(recordId);

    }


    @Override
    public void showProgress(String title, String message) {

    }

    @Override
    public void hideProgress() {

    }

    @Override
    public void showSnackMessage(String message) {

    }

    @Override
    public void showDetails(Record record) {
        acMarketValLand.setText(record.getValrepLandimpFinalValueLandAppraisedMv());
        acTotalImp.setText(record.getValrepLandimpFinalValueImprovementsAppraisedMv());
        acTotalOtherImp.setText(record.getValrepLandimpFinalValueTotalOtherLandImp());
        acTotalAppraised.setText(record.getValrepLandimpFinalValueNetAppraisedValue());
        acValueWords.setText(record.getValrepLandimpFinalValueNetAppraisedValueWords());

    }

    @Override
    public void showExitDialog(String title, String message) {

    }

    @Override
    public void showErrorDialog(String title, String message) {

    }
}
