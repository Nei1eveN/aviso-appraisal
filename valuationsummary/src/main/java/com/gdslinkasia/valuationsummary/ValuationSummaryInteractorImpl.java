package com.gdslinkasia.valuationsummary;

import android.app.Application;

import com.gdslinkasia.base.database.repository.JobDetailsLocalRepository;
import com.gdslinkasia.base.model.job.Record;

import io.reactivex.Single;

class ValuationSummaryInteractorImpl implements ValuationSummaryContract.Interactor {

    private JobDetailsLocalRepository localRepository;

    public ValuationSummaryInteractorImpl(Application application) {
        localRepository = new JobDetailsLocalRepository(application);
    }

    @Override
    public Single<Record> getRecordById(String recordId) {
        return localRepository.getRecordById(recordId);
    }

    @Override
    public void updateDetails(Record record) {
        localRepository.updateRecord(record);  //update all record: should be rename to updateRecordAll
    }
}
