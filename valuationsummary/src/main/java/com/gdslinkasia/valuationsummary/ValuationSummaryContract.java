package com.gdslinkasia.valuationsummary;

import com.gdslinkasia.base.model.job.Record;

import io.reactivex.Single;

public interface ValuationSummaryContract {
    interface View {
        void showProgress(String title, String message);
        void hideProgress();
        void showSnackMessage(String message);
        void showDetails(Record record);
        void showExitDialog(String title, String message);
        void showErrorDialog(String title, String message);
    }
    interface Presenter {
        void onStart(String recordId);
        void onDestroy();
    }
    interface Interactor {
        Single<Record> getRecordById(String recordId);
        void updateDetails(Record record);
    }
}
